<?php
try {
    require_once('stm/init.php');
    $controller = new \STMInstall\InstallController();
    $controller->loadView();
} catch (Exception $e) {
    die($e->getMessage());
}
?>
<!DOCTYPE html>
<html>
    <head>  
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width">
        <title>STM | <?php echo stmLang('install', 'header'); ?></title>
        <meta name="robots" content="noindex, nofollow" />
        <meta name="author" content="Zdenek Drahos, https://bitbucket.org/zdenekdrahos" />
        <link href="../web/css/style.css" type="text/css" rel="stylesheet" />
        <script src="../web/scripts/jquery.js" type="text/javascript"></script>
        <link href="../favicon.ico" rel="shortcut icon" />
        <style type="text/css">body section div#content {margin-left: 0}</style>
        <!--[if lt IE 9]><script src="../web/scripts/html5.js"></script><![endif]-->
        <!--[if lt IE 8]><link href="../web/css/ie.css" type="text/css" rel="stylesheet" media="all" /><![endif]-->
    </head>
<body>    
    <header>
        <section>
            <?php
            ob_start();
            \STM\Libs\LanguageSwitch\LanguageHelper::printLinksOfAvailableLanguages(); 
            echo str_replace('src="web', 'src="../web', ob_get_clean());
            ?>
            <div>
                Sports Table Manager <span>© 2012 - <?php echo strftime("%Y"); ?></span>
            </div>
        </section>
        <div id="gradient">
            <nav>
                <a href="#"><img src="../web/images/logo.png" alt="STM" /></a>
                <ul><li><?php echo $controller->getCurrentStep(); ?> / 3</li></ul>
            </nav>
        </div>
    </header>

    <section>        
        
        <div id="content">

            <h1><?php echo stmLang('install', 'header'); ?></h1>     

            <?php $controller->display() ?>
﻿
            </div>
        </section>

    </body>
</html>
            