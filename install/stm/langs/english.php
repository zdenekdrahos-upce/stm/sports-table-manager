<?php
// Available languages (title of the image)
$lang['lang-titles'] = array(
    'czech' => 'Czech language',
    'english' => 'English language',
);

$lang['install'] = array(
    'header' => 'Install Sports Table Manager',
    'back-to-first' => 'Go back to first step',
    'next-step' => 'Go to next step',
    'dberror' => 'Database error',
);

$lang['step-1'] = array(
    'requirements' => array(
        'header' => 'Requirements',
        'minimal' => 'Minimal requirements',
        'your-system' => 'Your system',
        'database' => 'Database',
        'mysql-support' => 'MySQL support',
    ),
    'db' => array(
        'mysql-settings' => 'MySQL Database Settigs',
        'triggers' => 'Trigger support',
        'procedures' => 'Procedure support',
        'connection' => 'Database connection',
        'server' => 'Database server',
        'user' => 'Database user',
        'password' => 'Password for database user',
        'name' => 'Name of database',
    ),
    'stm' => array(        
        'header' => 'Administrator in STM',
        'admin' => 'Admin name',
    ),
);

$lang['step-2'] = array(
    'ddl-scripts' => 'Database install script',    
    'info' => 'Download the following files and import them into your database..<br />
               Continue to the next step, when you have created tables in your DB.',
);

$lang['step-3'] = array(
    'admin' => array(
        'header' => 'Administrator login information',
        'name' => 'Name',
        'password' => 'Password',
    ),
    'next' => array(
        'header' => 'What\'s next?',
        'login' => 'Copy password, otherwise you will loss your password!!! Login (link in top right corner). ',
        'password' => 'After login you can change generated password in your profile (link Profile in top right corder)',
        'settings' => 'In settings change default STM settings (Menu: Admin &rarr; STM Settings)',
        'delete' => 'Delete install folder',
    )
);

$lang['form-error'] = array(
    'infotext' => 'You cannot go to next step because one or more error(s) occurred',
    'empty-value' => '%s must be filled.',
    'string-length' => '%s string length must be from %d to %d',
    'config-writable' => 'Directory <strong>/stm/config/</strong> must be writable (add 777 permission to files stm.php and database.php)',
    'template-notexist' => 'Template doesn\t exist',
    'file-not-saved' => 'File with database connection data was not saved',
    'connection-exists' => 'Database connection is already configured',
    'connection-notexists' => 'Cannot connect to the database, probably wrong database user or password.',
    'existing-users' => 'There are existing users in DB',
);

?>