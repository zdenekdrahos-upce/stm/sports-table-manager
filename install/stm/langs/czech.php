<?php
// Available languages (title of the image)
$lang['lang-titles'] = array(
    'czech' => 'Čeština',
    'english' => 'Angličtina',
);

$lang['install'] = array(
    'header' => 'Instalace Sports Table Manageru',
    'back-to-first' => 'Návrat na první krok',
    'next-step' => 'Pokračovat na další krok',
    'dberror' => 'Databázová chyba',
);

$lang['step-1'] = array(
    'requirements' => array(
        'header' => 'Požadavky',
        'minimal' => 'Minimální požadavky',
        'your-system' => 'Váš systém',
        'database' => 'Databáze',
        'mysql-support' => 'MySQL podpora',
    ),
    'db' => array(
        'mysql-settings' => 'Nastavení MySQL databáze',
        'triggers' => 'Podpora triggerů',
        'procedures' => 'Podpora procedur',
        'connection' => 'Databázové připojení',
        'server' => 'Databázový server',
        'user' => 'Databázový uživatel',
        'password' => 'Heslo pro DB uživatele',
        'name' => 'Jméno databáze',
    ),
    'stm' => array(        
        'header' => 'Administrace v STM',
        'admin' => 'Jméno administrátora',
    ),
);

$lang['step-2'] = array(
    'ddl-scripts' => 'Databázové instalační skripty',
    'info' => 'Stáhněte si níže uvedené soubory a naimportujte je do Vaší databáze.<br />
               Až budete mít vytvořené tabulky v databázi, tak pokračujte na další krok.',
);

$lang['step-3'] = array(
    'admin' => array(
        'header' => 'Přihlašovací údaje administrátora',
        'name' => 'Jméno',
        'password' => 'Heslo',
    ),
    'next' => array(
        'header' => 'Co dál?',
        'login' => 'Nezapomeňte si zkopírovat heslo z této stránky, jinak ho ztratíte!!! Přihlaste se (odkaz je v pravém horním rohu).',
        'password' => 'Po přihlášení si ve svém profilu můžete změnit vygenerované heslo (odkaz v pravém horním rohu)',
        'settings' => 'V nastavení nastavte výchozí chování aplikace (Menu: Administrátor &rarr; Nastavení STM)',
        'delete' => 'Smažte instalační složku z webu',
    )
);

$lang['form-error'] = array(
    'infotext' => 'Zatím nelze pokračovat na další krok, protože se vyskytly následující chyby',
    'empty-value' => '%s musí být vyplněno.',
    'string-length' => '%s musí mít délku od %d do %d',
    'config-writable' => 'Musí být možno zapsat do složky <strong>/stm/config/</strong> (nastavte souborů stm.php a database.php ze složky oprávnění na 777)',
    'template-notexist' => 'Šablona souboru neexistuje',
    'file-not-saved' => 'Nepodařilo se uložit soubor s údaji k připojení k databázi',
    'connection-exists' => 'Připojení k databázi už je nakonfigurované',
    'connection-notexists' => 'Nelze se připojit k databázi, pravděpodobně špatné přihlašovací údaje',
    'existing-users' => 'V databázi už jsou vytvoření uživatelé',
);

?>