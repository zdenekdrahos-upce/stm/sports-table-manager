<?php
// install constants
require_once(__DIR__ . '/paths.php');

// Standard STM
require_once(STM_INSTALL_ROOT . '../../stm/bootstrap.php');

// Install classes
require_once(STM_INSTALL_ROOT . 'classes/InstallController.php');
require_once(STM_INSTALL_ROOT . 'classes/InstallValidator.php');
