<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

define('STM_INSTALL_ROOT', __DIR__ . '/');
define('STM_INSTALL_STEPS', STM_INSTALL_ROOT . '/steps/');
define('STM_LANG_ROOT', STM_INSTALL_ROOT . 'langs/');
