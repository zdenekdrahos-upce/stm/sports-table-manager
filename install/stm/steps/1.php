
            <h2><?php echo stmLang('step-1', 'requirements', 'header'); ?></h2>
            <table>
                <tr>
                    <th>&nbsp;</th>
                    <th><?php echo stmLang('step-1', 'requirements', 'minimal'); ?></th>
                    <th><?php echo stmLang('step-1', 'requirements', 'your-system'); ?></th>
                </tr>
                <tr>
                    <th>PHP</th>
                    <td>PHP >= 5.3</td>
                    <td><?php echo PHP_VERSION; ?></td>
                </tr>
                <tr>
                    <th><?php echo stmLang('step-1', 'requirements', 'database'); ?></th>
                    <td>MySQL >= 5.1</td>
                    <td><?php 
                        echo function_exists('mysql_query') ? stmLang('step-1', 'requirements', 'mysql-support') : ''; 
                    ?></td>
                </tr>
            </table>
            <hr />

            <form method="POST" action="index.php">
                
                <h2><?php echo stmLang('step-1', 'db', 'mysql-settings'); ?></h2>
                <label for="triggers" class="inline"><?php echo stmLang('step-1', 'db', 'triggers'); ?>:</label>
                <input type="checkbox" id="triggers" name="triggers" checked="checked" class="inline" /><br />
                <label for="procedures" class="inline"><?php echo stmLang('step-1', 'db', 'procedures'); ?>:</label>
                <input type="checkbox" id="procedures" name="procedures" checked="checked" class="inline" /><br />
                <hr />
            
                <h2><?php echo stmLang('step-1', 'db', 'connection'); ?></h2>
                <label for="server"><?php echo stmLang('step-1', 'db', 'server'); ?>: </label>
                <input type="text" id="server" name="server" size="40" maxlength="50" value="" />
                <label for="user"><?php echo stmLang('step-1', 'db', 'user'); ?>: </label>
                <input type="text" id="user" name="user" size="40" maxlength="50" value="" />
                <label for="password"><?php echo stmLang('step-1', 'db', 'password'); ?>: </label>
                <input type="text" id="admin" name="password" size="40" maxlength="50" value="" />
                <label for="dbname"><?php echo stmLang('step-1', 'db', 'name'); ?>: </label>
                <input type="text" id="dbname" name="dbname" size="40" maxlength="50" value="" class="inline" />
                <hr />

                <h2><?php echo stmLang('step-1', 'stm', 'header'); ?></h2>
                <label for="admin"><?php echo stmLang('step-1', 'stm', 'admin'); ?>: </label>
                <input type="text" id="admin" name="admin" size="40" maxlength="50" value="" /><br />                
                <hr />

                <input type="submit" name="step-2" value="<?php echo stmLang('install', 'next-step'); ?>" />
            </form>
