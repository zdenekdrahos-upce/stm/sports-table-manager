

                        <h2><?php echo stmLang('step-3', 'admin', 'header'); ?></h2>
                        <ul>
                            <li><?php echo stmLang('step-3', 'admin', 'name'); ?>: <strong><?php echo $admin_name; ?></strong></li>
                            <li><?php echo stmLang('step-3', 'admin', 'password'); ?>: <strong><?php echo $admin_password; ?></strong></li>
                        </ul>
                        
                        <h2><?php echo stmLang('step-3', 'next', 'header'); ?></h2>
                        <ul>
                            <li><a href="../index.php" target="_blank"><?php echo stmLang('step-3', 'next', 'login'); ?></a></li>
                            <li><?php echo stmLang('step-3', 'next', 'password'); ?></li>
                            <li><?php echo stmLang('step-3', 'next', 'settings'); ?></li>
                            <li><?php echo stmLang('step-3', 'next', 'delete'); ?></li>
                        </ul>