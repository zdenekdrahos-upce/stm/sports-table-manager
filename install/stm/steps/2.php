
            <p><?php echo stmLang('step-2', 'info'); ?></p>

            <h2><?php echo stmLang('step-2', 'ddl-scripts'); ?></h2>
            <p></p>
            <ul>
                <?php foreach($scripts as $link => $name): ?>
                <li><a href="<?php echo $link; ?>" target="_blank"><?php echo $name; ?></a></li>
                <?php endforeach; ?>
            </ul>

            <hr />
            <form method="POST" action="index.php">
                <input type="hidden" name="admin" value="<?php echo $_POST['admin']; ?>" />
                <input type="submit" name="step-3" value="<?php echo stmLang('install', 'next-step'); ?>" />
            </form>