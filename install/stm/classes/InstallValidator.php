<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STMInstall;

use STM\DB\Database;
use STM\DB\IDatabase;
use STM\DB\DatabaseEvent;
use STM\DB\DatabaseException;
use STM\Web\Message\FormError;
use STM\Helpers\ObjectValidator;
use STM\StmFactory;
use STM\Competition\CompetitionSQL;
use STM\Match\MatchSQL;
use STM\Team\TeamSQL;

final class InstallValidator
{
    /** @var ObjectValidator */
    private static $validator;

    /** @param \STM\Libs\FormProcessor $formProcessor */
    public static function init($formProcessor = false)
    {
        if (is_null(self::$validator)) {
            self::$validator = new ObjectValidator('\STM\User\User');
        }
        self::$validator->setFormProcessor($formProcessor);
    }

    public static function checkFirstStep()
    {
        // database connection
        self::$validator->checkString(
            $_POST['server'],
            array('min' => 1, 'max' => 1000),
            stmLang('step-1', 'db', 'server')
        );
        self::$validator->checkString(
            $_POST['user'],
            array('min' => 1, 'max' => 1000),
            stmLang('step-1', 'db', 'user')
        );
        self::$validator->checkString(
            $_POST['password'],
            array('min' => 1, 'max' => 1000),
            stmLang('step-1', 'db', 'password')
        );
        self::$validator->checkString(
            $_POST['dbname'],
            array('min' => 1, 'max' => 1000),
            stmLang('step-1', 'db', 'name')
        );
        // admin
        self::checkAdmin();
        // config folder
        self::$validator->getFormProcessor()->checkCondition(
            is_writable(STM_CONFIG . 'database.php') && is_writable(STM_CONFIG . 'stm.php'),
            FormError::get('config-writable')
        );
        // existing connection
        self::$validator->getFormProcessor()->checkCondition(
            !self::isDatabaseAlive(),
            FormError::get('connection-exists')
        );
        return self::$validator->getFormProcessor()->isValid();
    }

    public static function checkSecondStep()
    {
        self::checkAdmin();
        self::$validator->getFormProcessor()->checkCondition(
            self::isDatabaseAlive(),
            FormError::get('connection-notexists')
        );
        if (self::$validator->getFormProcessor()->isValid()) {
            try {
                self::checkExistingUsers();
                $db = Database::getDB();
                self::checkQueries($db);
            } catch (DatabaseException $e) {
                DatabaseEvent::log($e);
                $error = '<strong>' . stmLang('install', 'dberror') . ': </strong>';
                self::$validator->getFormProcessor()->addError($error . $e->getMessage());
            }
        }
        return self::$validator->getFormProcessor()->isValid();
    }

    private static function checkAdmin()
    {
        self::$validator->checkString(
            $_POST['admin'],
            array('min' => 2, 'max' => 30),
            stmLang('step-1', 'stm', 'admin')
        );
    }

    private static function isDatabaseAlive()
    {
        try {
            Database::getDB()->openConnection();
            return true;
        } catch (DatabaseException $e) {
            return false;
        }
    }

    private static function checkExistingUsers()
    {
        $usersCount = StmFactory::count()->User->findAll();
        self::$validator->getFormProcessor()->checkCondition($usersCount == 0, FormError::get('existing-users'));
    }

    private static function checkQueries(IDatabase $database)
    {
        $queries = array(
            CompetitionSQL::selectCompetitions(),
            MatchSQL::selectMatches(),
            TeamSQL::selectTeams()
        );
        foreach ($queries as $query) {
            $database->query($query);
        }

    }
}
