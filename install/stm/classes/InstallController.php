<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STMInstall;

use STM\User\User;
use STM\User\UserGroup;
use STM\User\UserValidator;
use STM\Template\Template;
use STM\Web\Message\FormError;
use STM\Web\View\View;
use STM\Web\View\ViewText;
use STM\Web\View\ViewTemplate;
use STM\Libs\FormProcessor;
use STM\Utils\Strings;

class InstallController
{
    /** @var View */
    private $view;
    /** @var \STM\Libs\FormProcessor */
    private $formProcessor;
    /** @var int */
    private $current_step;

    public function __construct()
    {
        $this->view = new View();
        $this->formProcessor = new FormProcessor(true);
        InstallValidator::init($this->formProcessor);
        UserValidator::init($this->formProcessor);
        $this->current_step = $this->loadCurrentStep();
    }

    public function display()
    {
        $this->view->display();
    }

    public function loadView()
    {
        $step_method = 'step' . $this->current_step;
        $this->setContent($this->$step_method());
    }

    public function getCurrentStep()
    {
        return $this->current_step;
    }

    private function step1()
    {
        return array();
    }

    private function step2()
    {
        if (InstallValidator::checkFirstStep() && $this->saveDatabaseSettings()) {
            return array('scripts' => $this->getScripts());
        }
        return false;
    }

    private function saveDatabaseSettings()
    {
        $template = Template::findByName('change_database', STM_ADMIN_TEMPLATE_ROOT . 'settings/');
        if ($template instanceof Template) {
            $text = Strings::replaceValues($template->getContent(), $this->getTemplateArray());
            $config_file = Template::findByName('database', STM_SITE_ROOT . '/stm/config/');
            if (Strings::isStringNonEmpty($text) && $config_file->save($text)) {
                return true;
            } else {
                $this->formProcessor->addError(FormError::get('file-not-saved'));
            }
        } else {
            $this->formProcessor->addError(FormError::get('template-notexist'));
        }
    }

    private function getTemplateArray()
    {
        return array(
            'SERVER' => $_POST['server'],
            'USER' => $_POST['user'],
            'PASSWORD' => $_POST['password'],
            'DBNAME' => $_POST['dbname'],
            'PROCEDURES' => isset($_POST['procedures']) ? 'true' : 'false',
            'TRIGGERS' => isset($_POST['triggers']) ? 'true' : 'false',
        );
    }

    private function getScripts()
    {
        $scripts = array();
        $base_link = str_replace('index.php', '', $_SERVER['REQUEST_URI']) . "database-scripts/mysql/";
        $scripts[$base_link . 'mysql-5-1.SQL'] = 'MySQL DDL script';
        if (isset($_POST['triggers'])) {
            $scripts[$base_link . 'mysql-triggers.SQL'] = 'MySQL Triggers';
        }
        if (isset($_POST['procedures'])) {
            $scripts[$base_link . 'mysql-procedures.SQL'] = 'MySQL Procedures';
        }
        return $scripts;
    }

    private function step3()
    {
        if (isset($_SESSION['install'])) {
            $data = $_SESSION['install'];
            unset($_SESSION['install']);
            return $data;
        } elseif (InstallValidator::checkSecondStep()) {
            $password = $this->createAdminAccount();
            if ($password) {
                $_SESSION['install'] = array(
                    'admin_name' => $_POST['admin'],
                    'admin_password' => $password,
                );
                redirect($_SERVER['REQUEST_URI']);
            }
        }
        return false;
    }

    private function createAdminAccount()
    {
        $password = User::create(
            array('name' => $_POST['admin'], 'id_user_group' => UserGroup::getIdGroup(UserGroup::ADMIN))
        );
        return $password ? $password : false;
    }

    private function setContent($data)
    {
        if ($this->formProcessor->isValid()) {
            $this->view->setContent(new ViewTemplate(STM_INSTALL_STEPS . "{$this->current_step}.php", $data));
        } else {
            $this->view->setContent(
                new ViewTemplate(
                    STM_ADMIN_TEMPLATE_ROOT . 'form/errors.php',
                    array('errors' => $this->formProcessor->getErrors())
                )
            );
            $this->view->setContent(
                new ViewText('<hr /><a href="index.php">' . stmLang('install', 'back-to-first') . '</a>')
            );
        }
    }

    private function loadCurrentStep()
    {
        if (isset($_SESSION['install'])) {
            return 3;
        } else {
            for ($i = 2; $i <= 3; $i++) {
                if (isset($_POST["step-{$i}"])) {
                    return $i;
                }
            }
        }
        return 1;
    }
}
