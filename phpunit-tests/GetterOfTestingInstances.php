<?php

use STM\Competition\CompetitionType;
use STM\Competition\CompetitionSelection;
use STM\StmFactory;

class GetterOfTestingInstances {

    public static function get_competition() {
        return self::find_one_instance(CompetitionType::COMPETITION);
    }
    
    public static function get_season() {
        return self::find_one_instance(CompetitionType::SEASON);
    }
    
    public static function getPlayoff() {
        return self::find_one_instance(CompetitionType::PLAYOFF);
    }
    
    public static function find_competitions() {
        return self::find_other_competition_systems(CompetitionType::COMPETITION);
    }
    
    public static function find_seasons() {
        return self::find_other_competition_systems(CompetitionType::SEASON);
    }
    
    public static function find_playoffs() {
        return self::find_other_competition_systems(CompetitionType::PLAYOFF);
    }
    
    private static function find_one_instance($type) {
        $instances = self::find_competition_instances($type, 1);
        if ($instances) {
            $class = self::getClass($type);
            $competition = array_shift($instances);
            if ($type != CompetitionType::COMPETITION) {
                return $class::findById($competition->getId());
            } else {
                return $competition;
            }
        }
        return false;
    }
    
    private static function find_other_competition_systems($type) {
        $class = self::getClass($type);
        $competitions = self::find_competition_instances($type, 2);
        $instances = array();
        if ($competitions) {
            foreach ($competitions as $instance) {
                if ($type != CompetitionType::COMPETITION) {
                    $instances[] = $class::findById($instance->getId());
                } else {
                    $instances[] = $instance;
                }
            }
        }
        return $instances;
    }

    private static function getClass($type) {
        switch ($type) {
            case CompetitionType::PLAYOFF: return '\STM\Competition\Playoff\Playoff';
            case CompetitionType::SEASON: return '\STM\Competition\Season\Season';
            default: return '\STM\Competition\Competition';
        }
    }

    private static function find_competition_instances($type, $max) {
        $cs = new CompetitionSelection();
        if ($type != false) {
            $cs->setCompetitionType($type);
        }
        $cs->setLimit($max);
        return StmFactory::find()->Competition->find($cs);
    }    
}

?>
