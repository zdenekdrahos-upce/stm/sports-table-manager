<?php
require_once(__DIR__ . '/../stm/bootstrap.php');
require_once(__DIR__ . '/GetterOfTestingInstances.php');

function getFirst_instances($array) {    
    return array_slice($array, 0, min(5, count($array)));
}

function getMatches($matchType, $load_scores = true, $loadPeriods = true) {
    $selection = getMatchSelection($matchType, $load_scores, $loadPeriods);
    return STM\StmFactory::find()->Match->find($selection);
}

function getMatchSelection($matchType, $load_scores = true, $loadPeriods = true) {
    return array(
        'matchType' => $matchType,
        'loadScores' => $load_scores,
        'loadPeriods' => $loadPeriods,
        'limit' => array('max' => 5),
    );
}

?>