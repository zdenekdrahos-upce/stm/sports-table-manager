<?php

namespace STM\Attribute;

use \Exception;
use STM\StmFactory;

/**
 * Test class for AttributeValidator.
 * Generated by PHPUnit on 2012-02-19 at 09:18:39.
 * @group person
 */
class AttributeValidatorTest extends \PHPUnit_Framework_TestCase {

    protected function setUp() {
        AttributeValidator::init();
    }

    public function testCheck_create() {
        // must be array with key attribute (string <2, 30>)
        parent::assertTrue(AttributeValidator::checkCreate(array('attribute' => 'testing')));
        $this->assert_bad_arguments('checkCreate', null);
        $this->assert_bad_arguments('checkCreate', array('attribute' => 'a'));
    }

    public function testCheck_attribute_name() {
        parent::assertTrue(AttributeValidator::checkAttributeName('AT'));
        // string 2,30
        $this->assert_bad_arguments('checkAttributeName', null);
        $this->assert_bad_arguments('checkAttributeName', array());
        $this->assert_bad_arguments('checkAttributeName', new Exception());
        $this->assert_bad_arguments('checkAttributeName', '1234567890123456789012345678901');
        
        // different from current name or existing country
        foreach (StmFactory::find()->Attribute->findAll() as $attribute) {
            AttributeValidator::setAttribute($attribute);
            $this->assert_bad_arguments('checkAttributeName', $attribute->__toString());
            AttributeValidator::resetAttribute();
        }
    }

        
    private function assert_bad_arguments($method, $data) {
        AttributeValidator::init();
        parent::assertFalse(AttributeValidator::$method($data));
        AttributeValidator::init();
    }
}

?>
