<?php

namespace STM\Attribute;

use \Exception;
use STM\StmFactory;  

/**
 * Test class for Attribute.
 * Generated by PHPUnit on 2012-02-19 at 09:18:27.
 * @group person
 */
class AttributeTest extends \PHPUnit_Framework_TestCase {


    /** @var array(Attribute) */
    private $all_attributes;

    protected function setUp() {
        $this->all_attributes = StmFactory::find()->Attribute->findAll();
    }

    public function testfindById() {
        foreach ($this->all_attributes as $attribute) {            
            parent::assertInstanceOf('\STM\Attribute\Attribute', StmFactory::find()->Attribute->findById($attribute->getId()));
        }
        // argument must be numeric
        parent::assertFalse(StmFactory::find()->Attribute->findById('attribute'));
        parent::assertFalse(StmFactory::find()->Attribute->findById(null));
        parent::assertFalse(StmFactory::find()->Attribute->findById(new Exception()));
    }

    public function testFind_all() {
        parent::assertTrue(is_array($this->all_attributes));
        foreach ($this->all_attributes as $attribute) {            
            parent::assertInstanceOf('\STM\Attribute\Attribute', $attribute);
        }
    }

    public function testCreate() {
        parent::markTestSkipped('test in AttributeValidatorTest');
    }

    public function testExists_attribute() {
        foreach ($this->all_attributes as $attribute) {            
            $name = $attribute->__toString();   
            // case-INsensitive
            parent::assertTrue(Attribute::exists($name));            
            parent::assertTrue(Attribute::exists(mb_strtolower($name, 'UTF-8')));
            parent::assertTrue(Attribute::exists(mb_strtoupper($name, 'UTF-8')));
            parent::assertFalse(Attribute::exists('_*A*_' . $name));
            // argumetn must be string
            parent::assertFalse(Attribute::exists($attribute));
            parent::assertFalse(Attribute::exists(null));
        }
    }

    public function testDelete() {
        parent::markTestSkipped('in testCan_be_deleted, otherwise only DB fail');
    }

    public function testCan_be_deleted() {
        foreach ($this->all_attributes as $attribute) {
            $array = $attribute->toArray();
            if ($array['usage'] == 0) {
                parent::assertTrue($attribute->canBeDeleted());
            } else {
                parent::assertFalse($attribute->canBeDeleted());
            }            
        }
    }

    public function testUpdate_attribute_name() {
        parent::markTestSkipped('test in AttributeValidatorTest');
    }

    public function testGet_id() {
        $this->assert_get_to_function('is_int', 'getId');
    }

    public function testTo_array() {
        $this->assert_get_to_function('is_array', 'toArray');
    }

    public function test__toString() {
        $this->assert_get_to_function('is_string', '__toString');
    }

    private function assert_get_to_function($function, $method_name) {
        foreach ($this->all_attributes as $attribute) {
            parent::assertTrue($function($attribute->$method_name()));
        }
    }
}

?>
