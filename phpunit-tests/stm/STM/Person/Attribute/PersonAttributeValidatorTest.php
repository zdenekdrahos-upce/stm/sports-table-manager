<?php

namespace STM\Person\Attribute;

use \Exception;

/**
 * Test class for PersonAttributeValidator.
 * Generated by PHPUnit on 2012-03-28 at 09:26:31.
 * @group person
 */
class PersonAttributeValidatorTest extends \PHPUnit_Framework_TestCase {

    protected function setUp() {
        PersonAttributeValidator::init();
    }

    public function testCheck_create() {
        parent::assertTrue(PersonAttributeValidator::checkAttributeValue('110'));
        $this->assert_bad_arguments('checkCreate', null);
        $this->assert_bad_arguments('checkCreate', array());
        $this->assert_bad_arguments('checkCreate', array('person' => 1, 'attribute' => 2, 'value' => 'hello'));
    }

    public function testCheck_attribute_value() {
        parent::assertTrue(PersonAttributeValidator::checkAttributeValue('110'));
        $this->assert_bad_arguments('checkAttributeValue', 110);
        $this->assert_bad_arguments('checkAttributeValue', null);
        $this->assert_bad_arguments('checkAttributeValue', new Exception());
    }
    
    private function assert_bad_arguments($method, $data) {
        PersonAttributeValidator::init();
        parent::assertFalse(PersonAttributeValidator::$method($data));
        PersonAttributeValidator::init();
    }
}

?>
