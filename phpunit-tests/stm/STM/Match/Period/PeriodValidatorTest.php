<?php

namespace STM\Match\Period;

use STM\Match\Match;
use STM\Match\MatchSelection;
use \Exception;
use STM\StmFactory;

/**
 * Test class for PeriodValidator.
 * Generated by PHPUnit on 2011-08-30 at 15:03:37.
 * @group match_period
 */
class PeriodValidatorTest extends \PHPUnit_Framework_TestCase {

    private $all_periods;
    private $valid_structure;

    protected function setUp() {
        $this->all_periods = array();
        $factory = new PeriodFactory();
        foreach (getMatches(MatchSelection::ALL_MATCHES) as $match) {
            $this->all_periods[] = array(
                'match' => $match,
                'periods' => $factory->findByMatch($match)
            );
        }
        $this->valid_structure = array(
            'match' => StmFactory::find()->Match->findById (1),
            'score_home' => '1',
            'score_away' => '2',
            'note' => '',
        );
        PeriodValidator::init();
    }

    public function testCheck_create() {
        // argument must be array with keys 'match', 'score_home', 'score_away', 'note'
        $this->assert_bad_arguments('checkCreate', null);
        $this->assert_bad_arguments('checkCreate', new Exception());
        // match must be instanceof Match
        $this->assert_bad_arguments('checkCreate', $this->get_modified_structure('match', null));
        // scores must be numeric <0, 100>
        $this->assert_bad_arguments('checkCreate', $this->get_modified_structure('score_home', -1));
        $this->assert_bad_arguments('checkCreate', $this->get_modified_structure('score_away', '101'));
        // note must be string <0, 10>
        $this->assert_bad_arguments('checkCreate', $this->get_modified_structure('note', '123456789012345'));
        $this->assert_bad_arguments('checkCreate', $this->get_modified_structure('note', 'čřžčřž'));
        // OK
        if ($this->valid_structure['match']) {
            parent::assertTrue(PeriodValidator::checkCreate($this->valid_structure));
        }
    }

    public function testCheck_update() {
        // compared object must be set, otherwise same as testCheck_create
        $this->assert_bad_arguments('checkUpdate', $this->valid_structure);
    }

    public function testCheck_change_in_periods() {
        unset($this->valid_structure['match']);
        foreach ($this->all_periods as $match_info) {
            $match = $match_info['match'];
            $periods = $this->getPeriods_for_change($match_info['periods']);
            // score or note must be changed at least in one period
            PeriodValidator::init();
            parent::assertFalse(PeriodValidator::checkChangeInPeriods($match, $periods));
            PeriodValidator::init();
            if ($periods) {
                parent::assertTrue(PeriodValidator::checkChangeInPeriods($match, $this->get_updated_periods($periods, 'score_home', 7)));            
                parent::assertTrue(PeriodValidator::checkChangeInPeriods($match, $this->get_updated_periods($periods, 'score_away', 7)));            
                parent::assertTrue(PeriodValidator::checkChangeInPeriods($match, $this->get_updated_periods($periods, 'note', '*#&65')));
            }
            PeriodValidator::resetPeriod();
        }
    }

    private function getPeriods_for_change($periods) {
        $result = array();
        foreach ($periods as $period) {
            $result[$period->getIdPeriod()] = $period->toArray();
            unset($result[$period->getIdPeriod()]['id_period']);
        }
        return $result;
    }

    // change value only in first value
    private function get_updated_periods($periods, $key, $new_value) {
        foreach (array_keys($periods) as $id_period) {
            if ($periods[$id_period][$key] == $new_value) {
                $new_value++;
            }
            $periods[$id_period][$key] = $new_value;            
            break;
        }
        return $periods;
    }

    private function get_modified_structure($key, $new_value) {
        $attributes = $this->valid_structure;
        $attributes[$key] = $new_value;
        return $attributes;
    }

    private function assert_bad_arguments($method, $data) {
        PeriodValidator::init();
        parent::assertFalse(PeriodValidator::$method($data));
        PeriodValidator::init();
    }

}

?>