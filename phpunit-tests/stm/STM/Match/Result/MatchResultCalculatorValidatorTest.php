<?php

namespace STM\Match\Result;

use STM\Match\Match;
use STM\Match\Action\MatchAction;
use STM\Match\MatchSelection;
use STM\Match\Stats\Action\StatisticCalculationType;
use STM\StmFactory;

/**
 * Test class for MatchResultCalculatorValidator.
 * Generated by PHPUnit on 2013-01-20 at 14:16:36.
 * @group match
 */
class MatchResultCalculatorValidatorTest extends \PHPUnit_Framework_TestCase {

    /** @var Match */
    private $valid_match;
    /** @var MatchAction */
    private $valid_match_action;
    /** @var StatisticCalculationType */
    private $calculation_method;

    protected function setUp() {
        $matches = getMatches(MatchSelection::PLAYED_MATCHES, false, false);
        $this->valid_match = array_shift($matches);
        $this->calculation_method = StatisticCalculationType::COUNT;
        $this->valid_match_action = StmFactory::find()->MatchAction->findById(1);

        MatchResultCalculatorValidator::init();
        MatchResultCalculatorValidator::setMatch($this->valid_match);
    }

    public function testCheck_valid_data() {
        if ($this->valid_match && $this->valid_match_action) {
            parent::assertTrue(MatchResultCalculatorValidator::check($this->valid_match_action, $this->calculation_method));
        }
    }

    public function testCheck_unsetMatch() {
        MatchResultCalculatorValidator::resetMatch();
        parent::assertFalse(MatchResultCalculatorValidator::check($this->valid_match_action, $this->calculation_method));        
    }

    public function testCheck_invalid_match_action() {
        parent::assertFalse(MatchResultCalculatorValidator::check(null, $this->calculation_method));        
    }

    public function testCheck_invalid_calulation_type() {
        parent::assertFalse(MatchResultCalculatorValidator::check($this->valid_match_action, 'invalid method'));
    }

}

?>
