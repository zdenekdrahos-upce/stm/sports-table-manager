<?php

namespace STM;

use \Datetime;
use \Exception;

/**
 * Test class for StmDatetime.
 * Generated by PHPUnit on 2012-08-22 at 17:04:53.
 * @group helpers
 */
class StmDatetimeTest extends \PHPUnit_Framework_TestCase {

    /** @var StmDatetime */
    private $valid_object;
    /** @var StmDatetime */
    private $invalid_object;

    protected function setUp() {
        $this->valid_object = new StmDatetime('now');
        $this->invalid_object = new StmDatetime(new Exception());
    }

    public function testGet_datetime() {
        parent::assertTrue($this->valid_object->getDatetime() instanceof Datetime);
        parent::assertFalse($this->invalid_object->getDatetime());
    }

    public function testisValid() {
        parent::assertTrue($this->valid_object->isValid());
        parent::assertFalse($this->invalid_object->isValid());
    }

    public function test__toString() {
        parent::assertTrue($this->valid_object->__toString() !== '');
        parent::assertFalse($this->invalid_object->__toString() !== '');
    }

}

?>