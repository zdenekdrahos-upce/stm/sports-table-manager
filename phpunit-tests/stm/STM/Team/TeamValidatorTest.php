<?php

namespace STM\Team;

use \Exception;
use STM\StmFactory;

/**
 * Test class for TeamValidator.
 * Generated by PHPUnit on 2012-02-14 at 16:08:05.
 * @group team
 */
class TeamValidatorTest extends \PHPUnit_Framework_TestCase {

    /** @var array(Team) */
    private $all_teams;
    /** @var array(Club) */
    private $clubs;

    protected function setUp() {
        TeamValidator::init();
        $this->all_teams = StmFactory::find()->Team->findAll();
        $this->clubs = StmFactory::find()->Club->findAll();        
    }

    public function testCheck_create() {
        // must array with key name and club
        $club = array_shift($this->clubs);
        if ($club) {
            parent::assertTrue(TeamValidator::checkCreate(array('name' => 'a*1test', 'club' => $club)));
        }
        $this->assert_bad_arguments('checkCreate', null);
        $this->assert_bad_arguments('checkCreate', array('name' => 'adsa', 'club' => 'Makov'));
    }

    public function testCheck_update() {
        foreach ($this->all_teams as $team) {
            TeamValidator::setTeam($team);
            $valid = array(
                'name' => $team->__toString(),
                'club' => StmFactory::find()->Club->findById($team->getIdClub())
            );
            // different from current name and club
            $this->assert_bad_arguments('checkUpdate', $valid);

            // team cannot be already under the club
            foreach ($this->clubs as $new_club) {
                if ($new_club->getId() != $valid['club']->getId()) {
                    $valid['club'] = $new_club;
                    if ($new_club->isTeamFromClub($valid['name'])) {
                        $this->assert_bad_arguments('checkUpdate', $valid);
                    } else {
                        parent::assertTrue(TeamValidator::checkUpdate($valid));
                    }
                }
            }
            foreach (StmFactory::find()->Team->findByClub($valid['club']) as $club_team) {
                if ($club_team->getId() != $team->getId()) {
                    $old_name = $club_team->__toString();
                    $valid['name'] = $old_name;
                    $this->assert_bad_arguments('checkUpdate', $valid);
                    $valid['name'] = mb_strtolower($club_team->__toString(), 'UTF-8');
                    $this->assert_bad_arguments('checkUpdate', $valid);
                    $valid['name'] = mb_strtoupper($club_team->__toString(), 'UTF-8');
                    $this->assert_bad_arguments('checkUpdate', $valid);
                }
            }
            
            // at least one change (in name or club)
            $valid['name'] .= '*x*';
            parent::assertTrue(TeamValidator::checkUpdate($valid));
            
            TeamValidator::resetTeam();
        }
    }

    public function testCheck_name() {
        parent::assertTrue(TeamValidator::checkName('a*1test'));
        // string 2,50
        $this->assert_bad_arguments('checkName', null);
        $this->assert_bad_arguments('checkName', new Exception());
        $this->assert_bad_arguments('checkName', 'a');        
        $this->assert_bad_arguments('checkName', '123456789012345678901234567890123456789012345678901');
    }

    public function testCheck_club() {
        // instance of Club class
        $this->assert_bad_arguments('checkClub', null);
        $this->assert_bad_arguments('checkClub', new Exception());
    }

    private function assert_bad_arguments($method, $data) {
        TeamValidator::init();
        parent::assertFalse(TeamValidator::$method($data));
        TeamValidator::init();
    }
}

?>