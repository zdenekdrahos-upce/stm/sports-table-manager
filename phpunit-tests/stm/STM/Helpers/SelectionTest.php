<?php

namespace STM\Helpers;

use STM\DB\SQL\SelectQuery;

/**
 * Test class for Selection.
 * Generated by PHPUnit on 2012-07-13 at 14:31:12.
 * @group helpers
 */
class SelectionTest extends \PHPUnit_Framework_TestCase {

    /** @var Selection */
    private $object;

    protected function setUp() {
        $this->object = new Selection();
    }

    public function testgetSelectQuery() {
        parent::assertFalse($this->object->getSelectQuery(null));
        parent::assertInstanceOf('\STM\DB\SQL\SelectQuery', $this->object->getSelectQuery(new SelectQuery(DT_CLUBS)));
    }

    public function testGet_escaped_condition() {
        parent::assertInstanceOf('\STM\DB\SQL\Condition', $this->object->getEscapedCondition());
    }

    public function testGet_limit() {
        parent::assertFalse($this->object->getLimit());
        $this->object->setRowsLimit(5, 0);
        parent::assertInstanceOf('\STM\DB\SQL\RowsLimit', $this->object->getLimit());
    }

    public function testsetRowsLimit() {
        // max >= 1, offset >= 0 (integers)
        $this->object->setRowsLimit(0, -1);
        parent::assertFalse($this->object->getLimit());
        $this->object->setRowsLimit('1', '0');
        parent::assertFalse($this->object->getLimit());
        $this->object->setRowsLimit(1, 0);
        parent::assertInstanceOf('\STM\DB\SQL\RowsLimit', $this->object->getLimit());
    }

    public function testSet_null_attribute() {
        $this->object->setNullAttribute('name', true);
        $this->assertTrue(is_int(strpos($this->object->getEscapedCondition()->__toString(), 'name IS NULL')));
        $this->object->setNullAttribute('name', false);
        $this->assertTrue(is_int(strpos($this->object->getEscapedCondition()->__toString(), 'name IS NOT NULL')));
    }

    public function testSet_attribute_with_value() {
        $this->object->setAttributeWithValue('name', 'Zdenda');
        $this->assertTrue(is_int(strpos($this->object->getEscapedCondition()->__toString(), "name = 'Zdenda'")));
    }

    public function testSet_attributes_in_multiple_columns() {
        $this->object->setAttributesInMultipleColumns(array('home', 'away'), array('Makov'), true);
        $this->assertTrue(is_int(strpos($this->object->getEscapedCondition()->__toString(), "home IN ('Makov') AND away IN ('Makov')")));
        $this->object->setAttributesInMultipleColumns(array('home', 'away'), array('Makov'), false);
        $this->assertTrue(is_int(strpos($this->object->getEscapedCondition()->__toString(), "home IN ('Makov') OR away IN ('Makov')")));
    }

}

?>