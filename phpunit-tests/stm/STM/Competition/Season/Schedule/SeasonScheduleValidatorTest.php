<?php

namespace STM\Competition\Season\Schedule;

use STM\Competition\Season\Season;
use \GetterOfTestingInstances;
use \Exception;

/**
 * Test class for SeasonScheduleValidator.
 * Generated by PHPUnit on 2012-02-02 at 13:30:15.
 * @group competition_season
 */
class SeasonScheduleValidatorTest extends \PHPUnit_Framework_TestCase {

    /** @var array */
    private $data;

    protected function setUp() {
        $this->data = array(
            'season' => GetterOfTestingInstances::get_season(),
            'matches' => array(),
            'dates' => array()
        );
        if ($this->data['season'] instanceof Season) {
            // add empty dates
            for ($i = 0; $i < $this->data['season']->getSeasonPeriods(); $i++) {
                $this->data['dates'][] = '';
            }
        }
    }

    public function test_bad_argument() {
        // argument must be array('season' => ..., 'matches' => ...);
        $this->assert_bad_arguments(null);
        $this->assert_bad_arguments(new Exception());
        $this->assert_bad_arguments(array());
        $this->assert_bad_arguments(array('season' => 1, 'match' => 2));
    }

    public function test_bad_season() {
        // season must be instanceof Season class
        $this->data['season'] = 1;
        $this->assert_bad_arguments($this->data);
        $this->data['season'] = null;
        $this->assert_bad_arguments($this->data);

        foreach (GetterOfTestingInstances::find_competitions() as $competition) {
            $this->data['season'] = $competition;
            $this->assert_bad_arguments($this->data);
        }
    }

    public function test_bad_matches() {
        // matches must be array and number of rounds and matches in round have to
        // correspond with instance attributes of the season
        $this->data['matches'] = 1;
        $this->assert_bad_arguments($this->data);
        $this->data['matches'] = array();
        $this->assert_bad_arguments($this->data);

        if ($this->data['season'] instanceof Season) {
            // invalid number of rounds
            $this->data['matches'] = $this->get_matching_schedule();
            array_shift($this->data['matches']);
            $this->assert_bad_arguments($this->data);

            // invalid number of matches in round
            $this->data['matches'] = $this->get_matching_schedule();
            $first_round = array_shift($this->data['matches']);
            $first_round[] = array();
            array_unshift($this->data['matches'], $first_round);
            $this->assert_bad_arguments($this->data);

            // invalid match array            
            $this->data['matches'] = $this->get_matching_schedule();
            $first_round = array_shift($this->data['matches']);
            if ($first_round) {
                $first_match = array_shift($first_round);
                $first_match = array('home' => 1, 'away' => 2);
                array_unshift($first_round, $first_match);
                array_unshift($this->data['matches'], $first_round);
                $this->assert_bad_arguments($this->data);
            }
        }
    }

    public function test_check_generated_schedule() {
        if ($this->data['season'] instanceof Season && $this->data['season']->getCountTeams()) {
            $this->data['matches'] = $this->get_matching_schedule();
            parent::assertTrue(SeasonScheduleValidator::checkGeneratedSchedule($this->data));
        }
    }

    private function get_matching_schedule() {
        $team_count = $this->data['season']->getCountTeams();
        $round_count = $team_count % 2 == 0 ? $team_count - 1 : $team_count;
        $matches_in_round = floor($team_count / 2);

        $schedule = array();
        for ($i = 1; $i <= $round_count; $i++) {
            for ($j = 1; $j <= $matches_in_round; $j++) {
                $schedule[$i][$j] = array('h' => 1, 'a' => 1);
            }
        }
        return $schedule;
    }

    private function assert_bad_arguments($data) {
        SeasonScheduleValidator::init();
        parent::assertFalse(SeasonScheduleValidator::checkGeneratedSchedule($data));
        SeasonScheduleValidator::init();
    }

}

?>
