<?php

namespace STM\Competition;

use STM\Utils\Classes;
use \Exception;

/**
 * Test class for CompetitionType.
 * Generated by PHPUnit on 2011-08-28 at 10:56:08.
 * @group competition
 */
class CompetitionTypeTest extends \PHPUnit_Framework_TestCase {

    private $valid_values;
    private $bad_values;

    protected function setUp() {
        $this->valid_values = array('S', 'P', 'X');
        $this->bad_values = array('dsadas', 'Season', 'Playoff', false, array(), 235, new Exception());
    }

    protected function tearDown() {}

    public function testGet_competition_type() {
        foreach ($this->valid_values as $value) {            
            $this->assertTrue(Classes::isClassConstant('\STM\Competition\CompetitionType', CompetitionType::getCompetitionType($value)));
        }

        foreach ($this->bad_values as $value) {
            $this->assertFalse(Classes::isClassConstant('\STM\Competition\CompetitionType', CompetitionType::getCompetitionType($value)));
        }
    }

}

?>
