<?php

namespace STM\Competition\Playoff\Serie;

use \GetterOfTestingInstances;
use STM\StmFactory;

/**
 * Test class for PlayoffSerie.
 * Generated by PHPUnit on 2012-02-05 at 21:10:29.
 * @group competition_playoff
 */
class PlayoffSerieTest extends \PHPUnit_Framework_TestCase {

    /** @var array(Playoff) */
    private $playoffs;
    /** @var Playoff */
    private $playoff;
    /** @var array(PlayoffSerie) */
    private $playoff_series;

    protected function setUp() {
        $this->playoffs = GetterOfTestingInstances::find_playoffs();
        $this->playoff = array_shift($this->playoffs);
        $this->playoff_series = StmFactory::find()->PlayoffSerie->findByPlayoff($this->playoff);
    }

    public function testfindById() {
        if ($this->playoff_series) {
            foreach ($this->playoff_series as $serie) {
                parent::assertInstanceOf(
                    '\STM\Competition\Playoff\Serie\PlayoffSerie', 
                    StmFactory::find()->PlayoffSerie->findById($serie->getId()));
            }
        }
        parent::assertFalse(StmFactory::find()->PlayoffSerie->findById(null));
        parent::assertFalse(StmFactory::find()->PlayoffSerie->findById(array()));
        parent::assertFalse(StmFactory::find()->PlayoffSerie->findById('my playoff'));
    }

    public function testFind_by_playoff() {
        if ($this->playoff) {
            parent::assertTrue(is_array($this->playoff_series));
        }
    }

    public function testFind_by_playoff_round() {
        if ($this->playoff) {
            for ($i = 1; $i <= $this->playoff->getPlayedRounds(); $i++) {
                parent::assertTrue(is_array(StmFactory::find()->PlayoffSerie->findByPlayoffRound($this->playoff, $i)));
            }
            $next_round_series = StmFactory::find()->PlayoffSerie->findByPlayoffRound($this->playoff, $this->playoff->getPlayedRounds() + 1);
            parent::assertTrue(is_array($next_round_series) && empty($next_round_series));
        }
    }

    public function testDelete_playoff_round() {
        foreach ($this->playoffs as $playoff) {
            if ($playoff->getPlayedRounds() == 0) {
                parent::assertFalse(PlayoffSerie::deletePlayoffRound($playoff));
            }
        }
    }

    public function testUpdate_seeded_type() {
        foreach ($this->playoffs as $playoff) {
            if ($playoff->getPlayedRounds() == 0) {
                parent::assertFalse(PlayoffSerie::updateSeededType($playoff, 'fixed'));
                parent::assertFalse(PlayoffSerie::updateSeededType($playoff, 'variable'));
            }
        }
    }

    public function testIs_finished() {
        if ($this->playoff) {
            foreach ($this->playoff_series as $serie) {
                // must be instance of Playoff
                parent::assertFalse($serie->isFinished(null));
                parent::assertFalse($serie->isFinished(array()));
                // must be parent playoff
                foreach ($this->playoffs as $playoff) {
                    if ($playoff->getId() != $this->playoff->getId()) {
                        parent::assertFalse($serie->isFinished($playoff));
                    }
                }
                // ok
                parent::assertTrue(is_bool($serie->isFinished($this->playoff)));
            }
        }
    }

    public function testGet_winner_id() {
        $this->assert_get_to_function('is_int', 'getIdWinner');
    }

    public function testGet_winner_name() {
        $this->assert_get_to_function('is_string', 'getNameWinner');
    }

    public function testGet_id() {
        $this->assert_get_to_function('is_int', 'getId');
    }

    public function testGet_id_team_1() {
        $this->assert_get_to_function('is_int', 'getIdTeam1');
    }

    public function testGet_id_team_2() {
        $this->assert_get_to_function('is_int', 'getIdTeam2');
    }

    public function testGet_id_next_serie() {
        if ($this->playoff) {
            foreach ($this->playoff_series as $serie) {
                $id = $serie->getIdNextSerie();
                parent::assertTrue(is_int($id) || is_null($id));
            }
        }
    }

    public function testGet_round() {
        $this->assert_get_to_function('is_int', 'getRound');
    }

    public function testGet_seeded() {
        $this->assert_get_to_function('is_int', 'getSeeded');
    }

    public function testTo_array() {
        $this->assert_get_to_function('is_array', 'toArray');
    }

    public function test__toString() {
        $this->assert_get_to_function('is_string', '__toString');
    }

    private function assert_get_to_function($function, $method_name) {
        if ($this->playoff) {
            foreach ($this->playoff_series as $serie) {
                parent::assertTrue($function($serie->$method_name()));
            }
        }
    }

}

?>
