<?php

namespace STM\DB\Object;

use STM\User\User;
use STM\DB\Database;
use STM\DB\SQL\SelectQuery;
use \Exception;
use STM\StmFactory;

/**
 * Test class for DatabaseObject.
 * Generated by PHPUnit on 2011-09-03 at 10:41:28.
 * @group database
 */
class DatabaseObjectTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var DatabaseObject
     */
    private $dbObject;
    private $badValues;

    protected function setUp() {
        $this->dbObject = new DatabaseObject(DT_USERS, array('id_user'));
        $this->dbObject->setClass('\STM\User\User');
        $this->dbObject->setDatabase(Database::getDB());
        $this->badValues = array('', null, false, array(), 0, 235, new Exception());
    }

    public function testInsert_values() {
        foreach ($this->badValues as $value) {
            $this->assertFalse($this->dbObject->insertValues($value));
            $this->assertFalse($this->dbObject->insertValues($value, true));
            $this->assertFalse($this->dbObject->insertValues($value, false));
            $this->assertFalse($this->dbObject->insertValues($value, $value));
        }
    }

    public function testUpdate_by_id() {
        foreach ($this->badValues as $value) {
            if (!is_scalar($value) && !is_array($value)) {
                $this->assertFalse($this->dbObject->updateById($value, array('name' => 'zdenda')));
            }
        }
        // number of attributes in PK - test in deleteById
    }

    public function testDelete_by_id() {
        foreach ($this->badValues as $value) {
            if (!is_scalar($value) && !is_array($value)) {
                $this->assertFalse($this->dbObject->deleteById($value));
            }
        }

        // tables has one attribute in PK -> ID must be one variable or array with one variable, 
        // otherwise error because number of attributes will not match
        $invalidId = array('id_user' => 5, 'name' => 'Zdenda'); // only values are used
        try {
            $this->dbObject->deleteById($invalidId);
            parent::fail('Delete should fail');
        } catch (Exception $e) {
            parent::assertInstanceOf('InvalidArgumentException', $e);
        }
    }

    public function testExists_item() {
        // arguments must be non empty strings
        foreach ($this->badValues as $value) {
            $this->assertFalse($this->dbObject->existsItem($value, $value));
            $this->assertFalse($this->dbObject->existsItem('          ', $value));
            $this->assertFalse($this->dbObject->existsItem($value, '            '));
        }

        foreach (StmFactory::find()->User->findAll() as $user) {
            $this->assertTrue($this->dbObject->existsItem('name', $user->getUsername()));
            $this->assertTrue($this->dbObject->existsItem('name', mb_strtolower($user->getUsername(), 'UTF-8')));
            $this->assertTrue($this->dbObject->existsItem('name', mb_strtoupper($user->getUsername(), 'UTF-8')));
            // tests only strings
            $this->assertFalse($this->dbObject->existsItem('id_user', $user->getId()));
        }

        // other test in UserTest, MatchTest, CompetitionTest
    }

    public function testSelect() {
        foreach ($this->badValues as $value) {
            // second/third argument are always in method sets to valid values
            // instance of SelectQuery must be valid
            $this->assertFalse($this->dbObject->select(new SelectQuery($value)));
        }
    }

    public function testSelect_all() {
        // other tests in QueryFactory::selectAll.
        $this->markTestIncomplete('Always valid');
    }

    public function testSelect_by_id() {
        foreach ($this->badValues as $value) {
            // id must be scalar or array
            if (!is_scalar($value) && !is_array($value)) {
                $this->assertFalse($this->dbObject->selectById($value));
            }
        }
    }

    public function testSelect_count() {
        // always OK if argument is Condition or null (counts all rows in table)
        // other tests in QueryFactory::selectCount
        $this->assertTrue(is_int($this->dbObject->selectCount(null)));
        $this->assertTrue(is_int($this->dbObject->selectCount(new \STM\DB\SQL\Condition('id_user = 5'))));
    }

}

?>
