<?php

namespace STM\Utils;

use STM\StmDatetime;
use \Exception;

/**
 * Test class for Dates.
 * Generated by PHPUnit on 2012-08-22 at 17:09:17.
 * @group utils
 */
class DatesTest extends \PHPUnit_Framework_TestCase {

    /** @var StmDatetime */
    private $now;
    /** @var StmDatetime */
    private $yesterday;

    protected function setUp() {
        $this->now = new StmDatetime('now');
        $this->yesterday = new StmDatetime('yesterday');
    }

    public function testconvertDatetimeToString() {
        parent::assertTrue(is_string(Dates::convertDatetimeToString($this->now->__toString())));
        // error message
        parent::assertEquals(Dates::convertDatetimeToString('dsadsa', 'error'), 'error');
        parent::assertFalse(is_string(Dates::convertDatetimeToString($this->now->getDatetime(), false)));
        // default error message 
        parent::assertEquals(Dates::convertDatetimeToString('dsadsa'), stmLang('not-set'));
        parent::assertEquals(Dates::convertDatetimeToString('dsadsa', 'not-set'), stmLang('not-set'));
    }

    public function testString_to_database_date() {
        parent::assertFalse(Dates::stringToDatabaseDate(array()) !== '');
        parent::assertFalse(Dates::stringToDatabaseDate('abcd') !== '');
        parent::assertTrue(Dates::stringToDatabaseDate('now') !== '');
    }

    public function testisDateInRange() {
        parent::assertFalse(Dates::isDateInRange(new Exception(), $this->yesterday, $this->now));
        parent::assertFalse(Dates::isDateInRange('now + 1 hour', $this->yesterday, $this->now));
        parent::assertFalse(Dates::isDateInRange('now - 2 days', $this->yesterday, $this->now));
        parent::assertTrue(Dates::isDateInRange('now - 1 hour', $this->yesterday, $this->now));
    }

    public function testAre_dates_in_order() {
        parent::assertFalse(Dates::areDatesInOrder(array(), new Exception()));
        parent::assertFalse(Dates::areDatesInOrder('dsadsads', $this->yesterday->__toString()));
        parent::assertFalse(Dates::areDatesInOrder($this->now->__toString(), ''));
        parent::assertFalse(Dates::areDatesInOrder($this->now->__toString(), $this->yesterday->__toString()));
        parent::assertTrue(Dates::areDatesInOrder($this->yesterday->__toString(), $this->now->__toString()));
    }

    public function testStrtotime() {
        parent::assertFalse(is_int(Dates::strtotime('dsadsa')));
        parent::assertFalse(is_int(Dates::strtotime($this->now->getDatetime())));
        parent::assertTrue(is_int(Dates::strtotime($this->now->__toString())));
    }

}

?>