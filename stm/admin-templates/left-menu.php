<ul>
    <li><a href="index.php?module=<?php echo $_GET['module']; ?>"><?php echo stmLang('layout', 'homepage'); ?></a></li>
    <?php if (\STM\Utils\Links::isSetPreviousPage()): ?>
    <li><a href="<?php echo $_SERVER['HTTP_REFERER']; ?>"><?php echo stmLang('layout', 'previous'); ?></a></li>
    <?php endif; ?>    
</ul>