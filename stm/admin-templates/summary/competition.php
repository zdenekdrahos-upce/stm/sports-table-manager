<?php
/**
 * Competition summary
 * $summary CompetitionSummary
 */
?>

<?php 
include(__DIR__ . '/competition/header.php');
if ($summary->teams !== false) {
    include(__DIR__ . '/competition/teams.php');    
}
if ($summary->schedule !== false) {
    include(__DIR__ . '/competition/schedule.php');    
}
if ($summary->statistics !== false) {
    include(__DIR__ . '/competition/statistics.php');
}
if ($summary->winners !== false) {
    include(__DIR__ . '/competition/winners.php');
}
if ($summary->tables !== false) {
    include(__DIR__ . '/competition/tables.php');
}
if ($summary->matches !== false) {
    include(__DIR__ . '/competition/matches.php');
}
if ($summary->playersActions !== false) {
    include(__DIR__ . '/competition/players.php');
}
?>