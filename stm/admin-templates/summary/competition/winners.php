
<h2 id="winners"><?php echo stmLang('competition', 'summary', 'winners', 'header'); ?></h2>
<?php if ($summary->winners['podium'] !== false): ?>
<?php 
echo '<h3>' . stmLang('competition', 'summary', 'winners', 'top-3') . '</h3>';
echo '<ol>';
foreach($summary->winners['podium'] as $position => $teams) {
    echo '<li>';
    echo $position == 1 ? '<strong>' : '';
    echo implode(', ', $teams);
    echo $position == 1 ? '</strong>' : '';
    echo '</li>';
}
echo '</ol>';
?>
<?php endif; ?>

<?php if ($summary->winners['topPlayers'] !== false): ?>
<?php 
echo '<h3>' . stmLang('competition', 'summary', 'winners', 'top-players') . '</h3>';
foreach (array_keys($summary->winners['topPlayers']) as $action) {
    $header = "<h4>{$action}</h4>";
    $players_data = $summary->winners['topPlayers'][$action];
    include(__DIR__ . '/players_template.php');
}
?>
<?php endif; ?>