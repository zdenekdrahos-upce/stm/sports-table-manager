
<ul>
    <?php
    if ($summary->teams['count'] !== false) {
        echo "<li>" . stmLang('competition', 'stats', 'team-count') . ": <strong>{$summary->teams['count']}</strong></li>";
    }
    if ($summary->teams['teams'] !== false) {
        echo "<li>" . stmLang('menu', 'teams') . ": <strong>" . implode(', ', $summary->teams['teams']) . "</strong></li>";
    }
    ?>
</ul>
