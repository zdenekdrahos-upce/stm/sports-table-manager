
<ul class="vertical">
    <?php
    echo '<li><a href="#detail">' . stmLang('header', 'competition', 'read', 'competition') . '</a></li>';    
    if ($summary->schedule !== false) {
        echo '<li><a href="#schedule">' . stmLang('competition', 'summary', 'schedule', 'header') . '</a></li>';
    }
    if ($summary->statistics !== false) {
        echo '<li><a href="#statistics">' . stmLang('header', 'competition', 'read', 'statistics') . '</a></li>';
    }
    if ($summary->winners !== false) {
        echo '<li><a href="#winners">' . stmLang('competition', 'summary', 'winners', 'header') . '</a></li>';
    }
    if ($summary->tables !== false) {        
        echo '<li><a href="#tables">' . stmLang('competition', 'summary', 'tables') . '</a></li>';
    }
    if ($summary->matches !== false) {
        echo '<li><a href="#matches">' . stmLang('competition', 'summary', 'matches', 'header') . '</a></li>';
    }
    if ($summary->playersActions !== false) {
        echo '<li><a href="#players">' . stmLang('header', 'match', 'read', 'action_player') . '</a></li>';
    }
    ?>
</ul>