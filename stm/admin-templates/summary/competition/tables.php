
<h2 id="tables"><?php echo stmLang('competition', 'summary', 'tables'); ?></h2>
<?php 
// attributes for classic table template
$table_name = '';
$display_wot = false;
$display_lot = false;
$your_teams = array();
$classic_table_types = array('full', 'home', 'away');
foreach ($classic_table_types as $table_type) {
    if ($summary->tables[$table_type] !== false) {
        echo '<h3>' . stmLang('competition', 'table', $table_type) . '</h3>';
        $table = $summary->tables[$table_type];
        include(STM_ADMIN_TEMPLATE_ROOT . 'result-table/table.php');
    }
}

if ($summary->tables['cross'] !== false) {
    echo '<h3>' . stmLang('header', 'season', 'read', 'cross_table') . '</h3>';
    $cross_table = $summary->tables['cross'];
    include(STM_MODULES_ROOT . 'Competition/Views/read/season/cross_table.php');
}
?>