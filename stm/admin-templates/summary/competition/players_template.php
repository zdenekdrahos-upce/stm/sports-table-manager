<?php 
/*
 * $header 
 * $players_data
 */
use STM\Match\Stats\Action\StatisticCalculationType;
?>
<?php 
echo $header;
$full_list_types = array(
    'sum' => StatisticCalculationType::SUM,
    'count' => StatisticCalculationType::COUNT
);
foreach ($full_list_types as $array_key => $translator_key) {
    echo '<table class="players_actions">';
    echo '<tr><th colspan="3">' . stmLang('constants', 'statisticcalculationtype', $translator_key) . '</td></tr>';
    foreach ($players_data[$array_key] as $value => $members) {
        echo '<tr>';
        $members_count = count($members);
        if ($members_count <= 1) {
            $member = array_shift($members);         
            $member_array = $member->toArray();
            echo <<<MEMBER
                <td class="value"><strong>{$value}</strong></td>
                <td>{$member_array['person']}</td>
                <td>{$member_array['team']}</td>   
MEMBER;
        } else {
            echo "<td class=\"value\" rowspan=\"{$members_count}\"><strong>{$value}</strong></td>";
            foreach ($members as $member) {
                $member_array = $member->toArray();            
                echo <<<MEMBER
                    <td>{$member_array['person']}</td>
                    <td>{$member_array['team']}</td>   
                </tr>
                <tr>
MEMBER;
            }
        }
        echo '</tr>';
    }
    echo '</table>';    
}
echo '<hr />';
?>