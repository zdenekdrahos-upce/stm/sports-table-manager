
<h2 id="players"><?php echo stmLang('header', 'match', 'read', 'action_player'); ?></h2>
<?php foreach ($summary->playersActions as $action => $action_array): ?>
<h3><?php echo $action; ?></h3>

<?php if ($action_array['top5'] !== false): ?>
<?php 
$header = '<h4>' . stmLang('competition', 'summary', 'player-actions', 'top-5') . '</h4>';
$players_data = $action_array['top5'];
include(__DIR__ . '/players_template.php');
?>
<?php endif; ?>

<?php if ($action_array['teams'] !== false): ?>
<?php 
echo '<h4>' . stmLang('menu', 'teams') . '</h4>';
echo '<table>';
foreach($action_array['teams'] as $team => $players_data) {
    $team_players = array(
        'sum' => array(),
        'count' => array()
    );
    foreach (array_keys($team_players) as $type) {
        foreach($players_data[$type] as $value => $players) {
            foreach ($players as $player) {
                $team_players[$type][] = "<strong>$value</strong> – $player";
            }
        }
    }
    foreach (array_keys($team_players) as $type) {
        $team_players[$type] = implode(', ', $team_players[$type]);
    }    
    echo <<<TEAM
    <tr>
        <th rowspan="2">{$team}</th>
        <td>SUM</td>
        <td class="left">{$team_players['sum']}</td>
    </tr>
    <tr>
        <td>COUNT</td>
        <td class="left">{$team_players['count']}</td>
    </tr>
TEAM;
}
echo '</table>';
echo '<hr />';
?>
<?php endif; ?>

<?php if ($action_array['fullList'] !== false): ?>
<?php 
$header = '<h4>' . stmLang('competition', 'summary', 'player-actions', 'full-list') . '</h4>';
$players_data = $action_array['fullList'];
include(__DIR__ . '/players_template.php');
?>
<?php 
endif; // if print full list
endforeach; // foreach match action
?>