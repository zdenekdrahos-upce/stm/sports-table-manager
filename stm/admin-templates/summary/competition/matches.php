
<h2 id="matches"><?php echo stmLang('competition', 'summary', 'matches', 'header'); ?></h2>
<table cellspacing="0" class="matches">
    <tr>
        <th><?php echo stmLang('match', 'home'); ?></th>
        <th colspan="3"><?php echo stmLang('match', 'score'); ?></th>
        <th><?php echo stmLang('match', 'away'); ?></th>
    </tr>
    <?php foreach ($summary->matches as $match_info): $match_array = $match_info['match']->toArray(); ?>
    <tr class="match">
        <td class="right"><?php echo $match_array['team_home']; ?></td>
        <td class="score"><?php echo $match_array['score_home']; ?></td>
        <td class="score">:</td>
        <td class="score"><?php echo $match_array['score_away']; ?></td>
        <td class="left"><?php echo $match_array['team_away']; ?></td>
    </tr>

    <?php if ($match_info['periods'] !== false && count($match_info['periods']) > 1): ?>
    <tr>
        <td>&nbsp;</td>
        <td colspan="3"><?php echo implode(', ', $match_info['periods']); ?></td>
        <td>&nbsp;</td>
    </tr>
    <?php endif; ?>
    
    <?php 
    if ($match_info['playersActions'] !== false):
      foreach ($match_info['playersActions'] as $action => $teams_info): 
    ?>
    <tr class="actions">
        <?php if ($teams_info['home']): ?>
        <td class="right"><?php echo implode('<br />', $teams_info['home']['players']); ?></td>
        <td><strong><?php echo implode('<br />', $teams_info['home']['minutes']); ?></strong></td>
        <?php else: ?>
        <td class="right">-</td>
        <td>-</td>
        <?php endif; ?>
        
        <td class="action"><?php echo $action; ?></td>
        
        <?php if ($teams_info['away']): ?>
        <td><strong><?php echo implode('<br />', $teams_info['away']['minutes']); ?></strong></td>
        <td class="left"><?php echo implode('<br />', $teams_info['away']['players']); ?></td>
        <?php else: ?>
        <td>-</td>
        <td class="left">-</td>
        <?php endif; ?>
    </tr>
    <?php 
      endforeach; 
    endif; 
    ?>

    <?php endforeach; ?>
</table>