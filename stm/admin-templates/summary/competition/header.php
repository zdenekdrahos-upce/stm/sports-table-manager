
<h1><?php echo $summary->competition['name']; ?></h1>
<?php include(__DIR__ . '/navigation.php'); ?>

<h2 id="detail"><?php echo stmLang('header', 'competition', 'read', 'competition'); ?></h2>
<ul>
    <li><?php echo stmLang('competition', 'date-start'); ?>: <strong><?php echo $summary->competition['dateStart']; ?></strong></li>
    <li><?php echo stmLang('competition', 'date-end'); ?>: <strong><?php echo $summary->competition['dateEnd']; ?></strong></li>
</ul>
