
<h2 id="schedule"><?php echo stmLang('competition', 'summary', 'schedule', 'header'); ?></h2>
<table>
    <?php 
    if ($summary->schedule['fieldsCount'] > 1) {
        echo '<tr>';
        for($i = 1; $i <= $summary->schedule['fieldsCount']; $i++) {
            echo "<th>{$i}</th>";
        }
        echo '</tr>';
    }
    $matches_count = 0;
    foreach($summary->schedule['matches'] as $match) {
        if ($matches_count++ % $summary->schedule['fieldsCount'] == 0) {
            echo '<tr>';
        }
        echo "<td>{$match}</td>";
        if ($matches_count % $summary->schedule['fieldsCount'] == 0) {
            echo '</tr>';
        }
    }
    ?>
</table>