
<h2 id="statistics"><?php echo stmLang('header', 'competition', 'read', 'statistics'); ?></h2>
<ul>
    <?php
    if ($summary->statistics['matchesCountAll'] !== false) {
        echo "<li>" . stmLang('competition', 'stats', 'all-matches') . ": <strong>{$summary->statistics['matchesCountAll']}</strong></li>";
    }
    if ($summary->statistics['matchesCountPlayed'] !== false) {
        echo "<li>" . stmLang('competition', 'stats', 'played-matches') . ": <strong>{$summary->statistics['matchesCountPlayed']}</strong></li>";
    }
    if ($summary->statistics['matchesCountUpcoming'] !== false) {
        echo "<li>" . stmLang('competition', 'stats', 'upcoming-matches') . ": <strong>{$summary->statistics['matchesCountUpcoming']}</strong></li>";
    }
    if ($summary->statistics['goalsCount'] !== false) {
        echo "<li>" . stmLang('competition', 'summary', 'statistics', 'goals-count') . ": <strong>{$summary->statistics['goalsCount']}</strong></li>";
    }
    if ($summary->statistics['goalsAverage'] !== false) {
        $rounded_average = round($summary->statistics['goalsAverage'], 2);
        echo "<li>" . stmLang('competition', 'summary', 'statistics', 'goals-average') . ": <strong>{$rounded_average}</strong></li>";
    }
    ?>
</ul>