<?php
/*
 * $display_wot     if display only WIN or WIN and WIN in OT
 * $display_lot     if display only LOSS or LOSS and LOSS in OT
 * $table_name
 * $table           array, item = \STM\Match\Table\TableRow
 * $your_teams      array, item = ID of your team
 */
?>
<h2><?php echo $table_name; ?></h2>
<table id="tablesorter">
    <thead>
        <tr>
            <th><a href=""><?php echo stmLang('competition', 'table', 'position'); ?></a></th>
            <th><a href=""><?php echo stmLang('competition', 'team'); ?></a></th>
            <th><a href=""><?php echo stmLang('competition', 'table', 'matches'); ?></a></th>
            <th><a href=""><?php echo stmLang('competition', 'table', 'win'); ?></a></th>
            <?php if ($display_wot): ?>
            <th><a href=""><?php echo stmLang('competition', 'table', 'winInOt'); ?></a></th>
            <?php endif; ?>
            <th><a href=""><?php echo stmLang('competition', 'table', 'draw'); ?></a></th>
            <?php if ($display_lot): ?>
            <th><a href=""><?php echo stmLang('competition', 'table', 'lossInOt'); ?></a></th>        
            <?php endif; ?>
            <th><a href=""><?php echo stmLang('competition', 'table', 'loss'); ?></a></th>
            <th><a href=""><?php echo stmLang('competition', 'table', 'goalFor'); ?></a></th>
            <th><a href=""><?php echo stmLang('competition', 'table', 'goalAgainst'); ?></a></th>        
            <th><a href=""><?php echo stmLang('competition', 'table', 'points'); ?></a></th>
            <th><a href=""><?php echo stmLang('competition', 'table', 'ext'); ?></a></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($table as $pos => $row): ?>
            <tr<?php echo in_array($row->idTeam, $your_teams) ? ' class="user"' : ''; ?>>
                <td><?php echo $pos + 1; ?>.</td>        
                <td>
                    <?php if (isset($_GET['c'])): ?>
                    <a href="?module=statistics&action=read&type=team&t=<?php echo $row->idTeam . '&c=' . $_GET['c']; ?>"><?php echo $row->name; ?></a>
                    <?php else: ?>
                    <strong><?php echo $row->name; ?></strong>
                    <?php endif; ?>
                </td>
                <td><?php echo $row->matches; ?></td>
                <?php if ($display_wot): ?>
                <td><?php echo $row->win; ?></td>
                <td><?php echo $row->winInOt; ?></td>        
                <?php else: ?>
                <td><?php echo $row->win + $row->winInOt; ?></td>
                <?php endif; ?>
                <td><?php echo $row->draw; ?></td>
                <?php if ($display_lot): ?>
                <td><?php echo $row->lossInOt; ?></td>        
                <td><?php echo $row->loss; ?></td>        
                <?php else: ?>
                <td><?php echo $row->loss + $row->lossInOt; ?></td>
                <?php endif; ?>
                <td><?php echo $row->goalFor; ?></td>        
                <td><?php echo $row->goalAgainst; ?></td>        
                <td><strong><?php echo $row->points; ?></strong></td>
                <td><?php echo $row->extraPoints; ?></td>        
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

