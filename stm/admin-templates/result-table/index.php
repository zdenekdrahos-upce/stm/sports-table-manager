<?php
/*
 * $display_wot     if display only WIN or WIN and WIN in OT
 * $display_lot     if display only LOSS or LOSS and LOSS in OT
 * $result_table    \STM\Match\Table\ResultTable
 * $your_teams      array, item = ID of your team
 */
?>

<ul class="vertical">
    <li><a href="javascript:toggleOneDiv('full', 'toogle');"><?php echo stmLang('competition', 'table', 'full'); ?></a></li>
    <li><a href="javascript:toggleOneDiv('home', 'toogle');"><?php echo stmLang('competition', 'table', 'home'); ?></a></li>
    <li><a href="javascript:toggleOneDiv('away', 'toogle');"><?php echo stmLang('competition', 'table', 'away'); ?></a></li>
</ul>

<div class="toogle" name="full" id="displayed">
<?php
$table_name = stmLang('competition', 'table', 'full');
$table = $result_table->getTableFull();
include(__DIR__ . '/table.php');
?>
</div>

<div class="toogle" name="home">
<?php
$table_name = stmLang('competition', 'table', 'home');
$table = $result_table->getTableHome();
include(__DIR__ . '/table.php');
?>
</div>

<div class="toogle" name="away">
<?php
$table_name = stmLang('competition', 'table', 'away');
$table = $result_table->getTableAway();
include(__DIR__ . '/table.php');
?>
</div>

<script type="text/javascript" src="web/scripts/jquery.tablesorter.min.js"></script>
<script type="text/javascript">
	$(function() {		
		$("#tablesorter").tablesorter();		
	});	
</script>