<?php
/*
 * $preview \STM\Match\Preview\MatchPreview
 */
$match_array = $preview->match->toArray(); 
static $result_classes = array('W' => 'exact', 'L' => 'fail', 'D' => 'success', '?' => 'unknown');
?>
    
    <ul>
        <li><?php echo stmLang('competition', 'competition'); ?>: <a href="?module=competition&action=read&type=competition&c=<?php echo $preview->match->getIdCompetition(); ?>"><?php echo $preview->competitionName; ?></a></li>
        <li><?php echo stmLang('match', 'home'); ?>: <a href="?module=team&action=read&type=team&t=<?php echo $preview->match->getIdTeamHome(); ?>"><?php echo $match_array['team_home']; ?></a></li>
        <li><?php echo stmLang('match', 'away'); ?>: <a href="?module=team&action=read&type=team&t=<?php echo $preview->match->getIdTeamAway(); ?>"><?php echo $match_array['team_away']; ?></a></li>
        <li><?php echo stmLang('match', 'date'); ?>: <strong><?php echo $preview->match->getDate(); ?></strong></li>
        <?php if ($preview->match->getRound() !== false): ?>
        <li><?php echo stmLang('match', 'season-round'); ?>: <strong><?php echo $preview->match->getRound(); ?></strong></li>
        <?php endif; ?>     
        <li><a href="?module=match&action=read&type=match&m=<?php echo $preview->match->getId(); ?>"><?php echo stmLang('header', 'match', 'read', 'match'); ?></a></li>
    </ul>
    <hr />

    <h2><?php echo stmLang('team', 'match-preview', 'head-to-head-matches'); ?></h2>
    <?php if ($preview->headToHeadMatches): ?>
    <table>
        <tr>               
            <th><?php echo stmLang('match', 'date'); ?></th>
            <th><?php echo stmLang('competition', 'match'); ?></th>
            <th><?php echo stmLang('match', 'score'); ?></th>
            <th>&nbsp;</th>
        </tr>
    <?php foreach ($preview->headToHeadMatches as $match): ?>
        <tr>
            <td><?php echo $match->match->getDate(); ?></td>
            <td><?php echo $match->match; ?></td>
            <td><?php echo $match->score; ?></td>
            <td class="<?php echo $result_classes[$match->result]; ?>">&nbsp;</td>
        </tr>
    <?php endforeach; ?>         
    </table>    
    <?php else: ?>
    <p><?php echo stmLang('no-content'); ?></p>
    <?php endif; ?>

    <h2><?php echo stmLang('team', 'match-preview', 'head-to-head-table'); ?></h2>
    <?php 
    $display_wot = false;
    $display_lot = false;
    $result_table = $preview->headToHeadTable;
    $your_teams = array($preview->idTeam);
    include(STM_ADMIN_TEMPLATE_ROOT . 'result-table/index.php');
    ?>
    <hr />

    <h2><?php echo stmLang('team', 'match-preview', 'last-matches'); ?> <strong><?php echo $match_array['team_home']; ?></strong></h2>
    <?php if ($preview->lastMatches->home): ?>
    <table>
        <tr>               
            <th><?php echo stmLang('match', 'date'); ?></th>
            <th><?php echo stmLang('competition', 'match'); ?></th>
            <th><?php echo stmLang('match', 'score'); ?></th>
            <th>&nbsp;</th>
        </tr>
    <?php foreach ($preview->lastMatches->home as $match): ?>
        <tr>
            <td><?php echo $match->match->getDate(); ?></td>
            <td><?php echo $match->match; ?></td>
            <td><?php echo $match->score; ?></td>
            <td class="<?php echo $result_classes[$match->result]; ?>">&nbsp;</td>
        </tr>
    <?php endforeach; ?>        
    </table>    
    <?php else: ?>
    <p><?php echo stmLang('no-content'); ?></p>
    <?php endif; ?>

    <h2><?php echo stmLang('team', 'match-preview', 'last-matches'); ?> <strong><?php echo $match_array['team_away']; ?></strong></h2>
    <?php if ($preview->lastMatches->away): ?>
    <table>
        <tr>               
            <th><?php echo stmLang('match', 'date'); ?></th>
            <th><?php echo stmLang('competition', 'match'); ?></th>
            <th><?php echo stmLang('match', 'score'); ?></th>
            <th>&nbsp;</th>
        </tr>
    <?php foreach ($preview->lastMatches->away as $match): ?>
        <tr>
            <td><?php echo $match->match->getDate(); ?></td>
            <td><?php echo $match->match; ?></td>
            <td><?php echo $match->score; ?></td>
            <td class="<?php echo $result_classes[$match->result]; ?>">&nbsp;</td>
        </tr>
    <?php endforeach; ?>        
    </table>    
    <?php else: ?>
    <p><?php echo stmLang('no-content'); ?></p>
    <?php endif; ?>
    <hr />

    <?php if ($preview->seasonStatistics): ?>
    <h2><?php echo stmLang('team', 'match-preview', 'season-stats'); ?></h2>
    <table>
        <tr>
            <th class="width20">&nbsp;</th>
            <th><strong><?php echo $match_array['team_home']; ?></strong></th>
            <th><strong><?php echo $match_array['team_away']; ?></strong></th>
        </tr>
        <?php foreach(get_object_vars($preview->seasonStatistics->home) as $key => $home_value):  ?>
        <tr>
            <th><?php echo stmLang('competition', 'table', $key); ?></th>
            <td><?php echo $home_value->getTotal(); ?></td>
            <td><?php echo $preview->seasonStatistics->away->$key->getTotal(); ?></td>
        </tr>
        <?php endforeach; ?>
    </table>
    
    <?php endif; ?>