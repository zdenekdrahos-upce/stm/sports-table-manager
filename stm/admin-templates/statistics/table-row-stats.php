<?php
/*
 * $table_row_stats \STM\Match\Stats\Table\TableRowsStats
 */
?>

<table>
    <tr>
        <th>&nbsp;</th>
        <th><?php echo stmLang('statistics', 'team-overall'); ?></th>
        <th><?php echo stmLang('statistics', 'team-home'); ?></th>
        <th><?php echo stmLang('statistics', 'team-away'); ?></th>
        <th><?php echo stmLang('statistics', 'team-difference'); ?></th>
    </tr>
    <?php foreach($table_row_stats as $key => $stat): ?>
    <tr>
        <th><?php echo stmLang('competition', 'table', $key); ?></th>
        <td><strong><?php echo $stat->getTotal(); ?></strong></td>
        <?php if (in_array($key, array('position', 'truth_points'))): ?>
        <td><?php echo "{$stat->home}"; ?></td>
        <td><?php echo "{$stat->away}"; ?></td>
        <td>&nbsp;</td>
        <?php else: ?>
        <td><?php echo "{$stat->home} ({$stat->getPercentageHome()}%)"; ?></td>
        <td><?php echo "{$stat->away} ({$stat->getPercentageAway()}%)"; ?></td>
        <td><?php echo $stat->getDifference(); ?></td>
        <?php endif; ?>
    </tr>
    <?php endforeach; ?>
</table>