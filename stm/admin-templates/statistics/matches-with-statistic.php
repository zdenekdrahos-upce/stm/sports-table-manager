<?php
/*
 * Mmatches with match actions (statistic)
 * $actions \STM\Match\Action\Player\MatchPlayerAction
 */
?>

<?php if ($actions): ?>
    <table>
        <tr>
            <th><?php echo stmLang('competition', 'team'); ?></th>
            <th><?php echo stmLang('competition', 'match'); ?></th>
            <th><?php echo stmLang('match', 'player-action'); ?></th>
            <th><?php echo stmLang('match', 'player-action-minute'); ?></th>
        </tr>
        <?php foreach($actions as $match_player_action): extract($match_player_action->toArray()); ?>
        <tr>
            <td><a href="?module=team&action=read&type=team&t=<?php echo $match_player_action->getIdTeam();?>"><?php echo $name_team; ?></a></td>
            <td><a href="?module=match&action=read&type=action_player&m=<?php echo $match_player_action->getIdMatch();?>"><?php echo $name_match; ?></a></td>
            <td><a href="?module=action#<?php echo $match_player_action->getIdMatchAction();?>"><?php echo $name_action; ?></a></td>
            <td><?php echo $minute_action; ?></td>
        </tr>
        <?php if (\STM\Utils\Strings::isStringNonEmpty($action_comment)): ?>
        <tr class="action_comment">
            <th><?php echo stmLang('match', 'player-action-comment'); ?>:</th>
            <td colspan="2"><?php echo $action_comment; ?></td>
        </tr>
        <?php endif; ?>
        <?php endforeach; ?>
    </table>
<?php else: ?>
    <p><?php echo stmLang('no-content'); ?></p>
<?php endif; ?>
