<?php
/*
 * $stats       \STM\Match\Stats\TeamMatchesStats
 * $name        string
 * $statistic   \STM\Match\Stats\TopMatches
 */
?>

<h3><?php echo $name; ?></h3>
<ul>
    <li>
        <?php echo stmLang('statistics', 'team-home'); ?>: 
        <?php if (count($statistic->matchesHome) > 0): ?>
        <strong><?php echo "{$statistic->topValues->home}"; ?></strong> 
        (<?php 
        foreach($statistic->matchesHome as $id_match) {
            $match = $stats->matches[$id_match];
            echo "{$match->match} <a href=\"?module=match&action=read&type=match&m={$id_match}\">{$match->score}</a>";
            if ($id_match != end($statistic->matchesHome)) {
                echo ', ';
            }
        }
        ?>)
        <?php else: ?>
        <strong>-</strong>
        <?php endif; ?>
    </li>
    <li>
        <?php echo stmLang('statistics', 'team-away'); ?>: 
        <?php if (count($statistic->matchesAway) > 0): ?>
        <strong><?php echo "{$statistic->topValues->away}"; ?></strong> 
        (<?php 
        foreach($statistic->matchesAway as $id_match) {
            $match = $stats->matches[$id_match];
            echo "{$match->match} <a href=\"?module=match&action=read&type=match&m={$id_match}\">{$match->score}</a>";
            if ($id_match != end($statistic->matchesAway)) {
                echo ', ';
            }
        }
        ?>)
        <?php else: ?>
        <strong>-</strong>
        <?php endif; ?>
    </li>
</ul>
