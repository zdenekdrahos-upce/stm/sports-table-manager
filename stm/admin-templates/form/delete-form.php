<form method="post" action="<?php echo $url; ?>" class="delete">
    <?php \STM\Web\HTML\Forms::inputDeleteToken(); ?>
    <input type="submit" name="delete" value="<?php echo $submit_message; ?>" onclick="return confirm('<?php echo $question; ?>')" />
</form>