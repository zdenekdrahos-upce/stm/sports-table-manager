<?php
/**
 * $fields
 */
$filter_form_header = isset($filter_form_header) ? $filter_form_header : stmLang('match', 'filter', 'header');
?>

<form method="GET" action="index.php">
    <strong><?php echo $filter_form_header; ?>: </strong>    
    <?php \STM\Web\HTML\Forms::printCurrentGetFieldsAsHiddenInputs(array_keys($fields)); ?>
    <?php foreach($fields as $id => $field): ?>
    <label for="<?php echo $id; ?>" class="inline"><?php echo $field['name']; ?>:</label>
    <?php \STM\Web\HTML\Forms::select($id, $field['values'], true, 'inline');  ?>
    <?php endforeach; ?>
    <input type="submit" class="inline" value="<?php echo stmLang('form', 'show'); ?>" />
</form>