<?php
/**
 * Errors in form processing
 * $errors        arrays of errors (from \STM\Libs\FormProcessor::getErrors)
 */
?>
<?php if (is_array($errors) && count($errors) > 0): ?>    
    <aside class="errors">
        <h2><?php echo stmLang('form-error', 'infotext'); ?>:</h2>
        <ul>
            <?php foreach ($errors as $error): ?><li><?php echo $error; ?></li><?php endforeach; ?>            
        </ul>
    </aside>
<?php endif; ?>