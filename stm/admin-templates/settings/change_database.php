<?php
/** The hostname of the Database server */
define('STM_DB_SERVER', '{SERVER}');
/** Your username for the server / database */
define('STM_DB_USER', '{USER}');
/** Your password for the server/database */
define('STM_DB_PASS', '{PASSWORD}');
/** The name of your database - set only if you're using MySQL */
define('STM_DB_NAME', '{DBNAME}');
/** If procedures can be used in database */
define('STM_DB_PROCEDURES', {PROCEDURES});
/** If triggers can be used in database */
define('STM_DB_TRIGGERS', {TRIGGERS});
