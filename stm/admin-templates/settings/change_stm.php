<?php
// BASIC SETTINGS
/** Define if user registration is allowed */
define('STM_ALLOW_USER_REGISTRATION', {REGISTRATION});
/** Define if unregistered visitor has access to application (only read competitions) */
define('STM_ALLOW_VISITOR_ACCESS', {VISITORS});
/** Define if match score prediction is allowed */
define('STM_ALLOW_PREDICTIONS', {PREDICTIONS});
/** Available languages: english, czech */
define('STM_DEFAULT_LANGUAGE', '{DEFAULT_LANG}');
/** Default datetime format ('Y-m-d H:i:s' == YEAR-MONTH-DAY HOUR-MINUTE-SECOND) 
 * http://php.net/manual/en/datetime.formats.php */
define('STM_DEFAULT_DATETIME_FORMAT', '{DATETIME}');
/** Url to website used in Back to website */
define('STM_CLUB_WEBSITE', '{WEBSITE}');

// DEFAULT VALUES
# Season points
define('STM_POINT_WIN', {POINT_WIN});
define('STM_POINT_WIN_OT', {POINT_WIN_OT});
define('STM_POINT_DRAW', {POINT_DRAW});
define('STM_POINT_LOSS_OT', {POINT_LOSS_OT});
define('STM_POINT_LOSS', {POINT_LOSS});
define('STM_MATCH_PERIODS', {MATCH_PERIODS});
# Filtering in Matches
define('STM_MAX_MATCHES_FOR_TABLE', {MAX_TABLE});
define('STM_MAX_MATCHES_FOR_LISTING', {MAX_LISTING});
define('STM_MATCHES_ON_ONE_PAGE', {PER_PAGE});
# Statistics
define('STM_STATISTIC_CALCULATION', '{STATISTIC_CALCULATION}');
# Match players
define('STM_MINUTE_IN', '{MINUTE_IN}');
define('STM_MINUTE_OUT', '{MINUTE_OUT}');

// Cache
/** Id of seasons (separated by comma) for which season table is not deleted and updated (e.g. 4,8) */
define('STM_NOT_DELETED_SEASONS_TABLE', '{OLD_SEASONS}');
