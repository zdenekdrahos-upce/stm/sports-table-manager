<!DOCTYPE html>
<html>
    <head>  
        <meta charset="UTF-8">
        <title>STM | Error</title>
        <meta name="description" content="Sports Table Manager">
        <meta name="keywords" content="sport, sports, table, manager, php, mysql">        
        <meta name="robots" content="noindex, nofollow" />
        <meta name="author" content="Zdenek Drahos, UPCE" />
        <link href="favicon.ico" rel="shortcut icon" />
        <link href="web/css/error.css" type="text/css" rel="stylesheet" />
        <script src="web/scripts/jquery.js" type="text/javascript"></script>
        <script type="text/javascript">jQuery(document).ready(function(){$("code").after('<a href="#" onclick="selectCode(this); return false;"><?php echo stmLang('form', 'select_all'); ?></a>')});</script>
    </head>
    <body>

        <h1>Sports Table Manager</h1>
        
        <a href="https://bitbucket.org/stm-sport/sports-table-manager/issues/new" target="_blank" title="<?php echo stmLang('error', 'report'); ?>"><img src="web/images/icons/error.png" alt="Error" /></a>

        <a class="report" href="https://bitbucket.org/stm-sport/sports-table-manager/issues/new" target="_blank"><?php echo stmLang('error', 'report'); ?></a>
        
        <h2><?php echo stmLang('error', 'desc'); ?></h2>
        <code>
            <?php
            echo 'Error: ' . $exception->getMessage() . '<br />';
            echo 'User group: ' . (isset($_SESSION['stmUser']['user_group']) ? $_SESSION['stmUser']['user_group'] : 'Visitor') . '<br />';
            echo 'PHP version: ' . phpversion() . '<br />';
            echo 'Server name: ' . $_SERVER['SERVER_NAME'] . '<br />';
            echo 'Error at: index.php?' . $_SERVER['QUERY_STRING'] . '<br />';
            echo 'Previous page: ' . (isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : 'unknown') . '<br />';
            echo 'Browser: ' . $_SERVER['HTTP_USER_AGENT'] . '<br />';
            echo 'MySQL support: ' . (function_exists('mysql_query') ? 'true' : 'false') . '<br />';
            ?>
        </code>        

    </body>
</html>