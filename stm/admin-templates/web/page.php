<!DOCTYPE html>
<html>
    <head>  
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width">
        <title>STM | <?php echo $page->getHeader(); ?></title>
        <meta name="description" content="Sports Table Manager" />
        <meta name="keywords" content="sport, sports, table, manager, php, mysql" />
        <meta name="robots" content="noindex, nofollow" />
        <meta name="author" content="Zdenek Drahos, https://bitbucket.org/zdenekdrahos" />
        <link href="web/css/style.css" type="text/css" rel="stylesheet" />
        <script src="web/scripts/jquery.js" type="text/javascript"></script>
        <script type="text/javascript">jQuery(document).ready(function(){$("code").before('<a href="#" onclick="selectCode(this); return false;"><?php echo stmLang('form', 'select_all'); ?></a><br />')});</script>
        <link href="favicon.ico" rel="shortcut icon" />
        <?php if (!$controller->isSetLeftMenu()): ?><style type="text/css">body > section > div {margin-left: 0}</style><?php endif; ?>
        <!--[if lt IE 9]><script src="web/scripts/html5.js"></script><![endif]-->
        <!--[if lt IE 8]><link href="web/css/ie.css" type="text/css" rel="stylesheet" media="all" /><![endif]-->
    </head>
<body>    
    <header>
        <section>
            <?php \STM\Libs\LanguageSwitch\LanguageHelper::printLinksOfAvailableLanguages(); ?>            
            <div>
            <?php if ($session->isLoggedIn()): ?>
                <strong><?php echo $session->getUsername(); ?></strong>
                <a href="index.php?module=profile"><?php echo stmLang('layout', 'header', 'profile');?></a>
                <a href="index.php?type=logout"><?php echo stmLang('layout', 'header', 'logout');?></a>
            <?php else: ?>
                <strong><?php echo stmLang('layout', 'header', 'visitor');?></strong>
                <a href="index.php?type=login"><?php echo stmLang('layout', 'header', 'login');?></a>
                <?php if (STM_ALLOW_USER_REGISTRATION === true): ?><a href="index.php?type=registration"><?php echo stmLang('layout', 'header', 'register');?></a><?php endif; ?>
            <?php endif; ?>
                | <a href="<?php echo STM_CLUB_WEBSITE; ?>"><?php echo stmLang('layout', 'header', 'back-to-web');?></a>
            </div>
        </section>
        <div id="gradient">
            <nav>
                <a href="index.php"><img src="web/images/logo.png" alt="STM" /></a>
                <?php $menu->display(); ?>
            </nav>
        </div>
    </header>  

    <?php if ($session->hasMessageSuccess()): ?>
    <figure class="success">
        <span><?php echo $session->getMessageSuccess(); ?></span>
    </figure>      
    <?php endif; ?>

    <?php if ($session->hasMessageFail()): ?>
    <figure class="failure">
        <span><?php echo $session->getMessageFail(); ?></span>
    </figure>      
    <?php endif; ?>

    <section>        
        
        <?php if ($controller->isSetLeftMenu()): ?>
        <nav id="right"> 
            <?php echo $controller->displayLeftMenu(); ?>
        </nav>
        <?php endif; ?>
        
        <div id="content">

            <h1><?php echo $page->getHeader(); ?></h1>     

            <?php $controller->display(); ?>
﻿
        </div>
    </section>

    <footer>
        <?php if ($controller->isSetLeftMenu()): ?>
        <a href="javascript:toggleNav();" id="aTag"><?php echo stmLang('layout', 'footer', 'hide-left-menu');?></a> |
        <?php endif; ?>
        <a href="index.php?type=map" title="Map of the web"><?php echo stmLang('layout', 'footer', 'map');?></a> |
        <a href="index.php?type=about" title="Information about Sports Table Manager application">Sports Table Manager</a> |
        <span>© 2012 - <?php echo strftime("%Y"); ?></span>
    </footer>

  </body>
</html>