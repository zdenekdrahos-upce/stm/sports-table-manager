<h2>Co to je STM?</h2>
<p>
    STM slouží pro správu výsledků sportovních klubů,
    nebo pro sportovní informační weby. Charakteristika STM:
</p>
<ul>
    <li>jednoduchá správa týmů a klubů, sezón, zápasů,</li>
    <li>využitelné pro různá sportovní odvětví,</li>
    <li>zdarma (šířeno pod new BSD licencí),</li>
    <li>snadná integrace na Váš vlastní web,</li>
    <li>možnost tipování výsledků pro fanoušky,</li>
    <li>generování rozpisů sezóny i pavouka pro playoff.</li>
</ul>

<h2>Orientace v aplikaci</h2>
<p>
    Pro základní orientaci a pohyb v aplikaci slouží horní vysouvací menu a následně na jednotlivých stránkách i levé menu.
    Informace o aktuální verzi STM, kterou používate, naleznete na <a href="?type=about">informační stránce o STM</a>.
</p>
