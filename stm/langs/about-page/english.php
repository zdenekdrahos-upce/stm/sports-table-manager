<h2>What is STM?</h2>
<p>
    STM can be used for the management results of sport clubs, or
    for sports information webs. Characteristic:
</p>
<ul>
    <li>easy management of teams, clubs, seasons and matches,</li>
    <li>can be used for various sports,</li>
    <li>free (new BSD license),</li>
    <li>easy integration on your web,</li>
    <li>match predictions for fans,</li>
    <li>generating of season schedules and playoff trees.</li>
</ul>

<h2>Orientation in STM</h2>
<p>
    For basic orientation in the app there is top drop down menu and left menu on single pages.
    You can find information about used version of STM on <a href="?type=about">page about STM</a>.
</p>
