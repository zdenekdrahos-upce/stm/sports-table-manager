<?php
// Available languages (title of the image)
$lang['lang-titles'] = array(
    'czech' => 'Czech language',
    'english' => 'English language',
);

// Layout
$lang['layout'] = array(
    'header' => array(
        'profile' => 'Profile',
        'logout' => 'Logout',
        'visitor' => 'Visitor',
        'login' => 'Login',
        'register' => 'Register',
        'back-to-web' => 'Back to website'
    ),
    'footer' => array(
        'map' => 'Map of the web',
        // same in js files
        'hide-left-menu' => 'Hide left menu',
        'hide-left-menu' => 'Show left menu',
    ),
    'homepage' => 'Module Homepage',
    'previous' => 'Previous Page',
);

// Menu
$lang['menu'] = array(
    'competitions' => 'Competitions',
    'categories' => 'Categories',
    'matches' => 'Matches',
    'import-matches' => 'Import Matches',
    'predictions' => 'Predictions',
    'chart' => 'Chart',
    'my-predictions' => 'My Predictions',
    'my-stats' => 'My Statistics',
    'teams' => 'Teams',
    'clubs' => 'Clubs',
    'import-teams' => 'Import Teams and Clubs',
    'head-to-head' => 'Head To Head',
    'persons' => 'Persons',
    'positions' => 'Positions',
    'attributes' => 'Attributes',
    'import-persons' => 'Import Persons',
    'other' => 'Other',
    'sports' => 'Sports Categories',
    'actions' => 'Match Actions',
    'stadiums' => 'Stadiums',
    'countries' => 'Countries',
    'admin' => 'Admin',
    'users' => 'Users',
    'settings' => 'STM Settings',
    'export' => 'Export',
    'import' => 'Import',
    'help' => '<img src="web/images/icons/help.png" class="help" title="Go to help for current page" />'
);

// Page headers (h1)  ['header', 'module_name', 'action', 'type]
$lang['header']['index'] = array(
    'index' => 'Sports Table Manager',
    'read' => array(
        'login' => 'Login',
        'registration' => 'Registration',
        'about' => 'About STM',
        'map' => 'Map of the Web'
    )
);
$lang['header']['team'] = array(
    'index' => 'List of All Teams',
    'create' => array(
        'team' => 'Create New Team',
        'member' => 'Add Member'
    ),
    'read' => array(
        'team' => 'About Team',
        'competitions' => 'Team Competitions',
        'members' => 'Team Members',
        'upcoming_match' => 'Team\'s Upcoming Match',
    ),
    'update' => array(
        'team' => 'Change Team Details',
        'member' => 'Update Team Member',
    )
);
$lang['header']['club'] = array(
    'index' => 'List of All Clubs',
    'create' => array(
        'club' => 'Create New Club',
        'member' => 'Add Member'
    ),
    'read' => array(
        'club' => 'About Club',
        'teams' => 'Teams From Club',
        'members' => 'Club Members'
    ),
    'update' => array(
        'club' => 'Change Club Details',
        'member' => 'Update Club Member'
    )
);
$lang['header']['competition'] = array(
    'index' => 'List of All Competitions',
    'create' => array(
        'competition' => 'Create New Competition',
        'team' => 'Add teams to competition',
        'teams' => 'Add teams from different competition',
        'players' => 'Add all team\'s players to all competition matches',
        'summary' => 'Create competition summary'
    ),
    'read' => array(
        'competition' => 'Competition Detail',
        'matches' => 'Competition matches',
        'statistics' => 'Competition statistics',
        'teams' => 'Teams from competition',
        'advanced_actions' => 'Advanced Actions',
    ),
    'update' => array(
        'competition' => 'Change Competition Details',
    )
);
$lang['header']['season'] = array(
    'index' => 'List of All Seasons',
    'create' => array(
        'season' => 'Create New Season',
        'schedule' => 'Generate Season Schedule',
    ),
    'read' => array(
        'tables' => 'Season Tables',
        'rounds' => 'Season Rounds',
        'cross_table' => 'Cross Table',
    ),
    'update' => array(
        'season' => 'Change Season Details',
        'extra_points' => 'Team Extra Points',
        'round_date' => 'Change Date of Matches in Season Rounds',
        'table' => 'Sort teams with equal points in full table by head to head matches',
    )
);
$lang['header']['playoff'] = array(
    'index' => 'List of All Playoffs',
    'create' => array(
        'playoff' => 'Create New Playoff',
        'round' => 'Generate Playoff Round'
    ),
    'read' => array(
        'series' => 'Playoff Series',
        'serie' => 'Detail of playoff serie',
        'tree' => 'Playoff Tree'
    ),
    'update' => array(
        'playoff' => 'Change Playoff Details',
        'seeded' => 'Change Team\'s Seeded'
    )
);
$lang['header']['category'] = array(
    'index' => 'List of All Categories',
    'create' => array(
        'category' => 'Create New Category',
    ),
    'read' => array(
        'category' => 'Category Detail',
    ),
    'update' => array(
        'category' => 'Change Category Details',
    )
);
$lang['header']['match'] = array(
    'index' => 'List of All Matches',
    'create' => array(
        'match' => 'Create New Match',
        'period' => 'Add Period to Match',
        'result' => 'Add Result',
        'round_results' => 'Add Match Results',
        'referee' => 'Add Referee',
        'lineup_home' => 'Add Players To Home Team Line-up',
        'lineup_away' => 'Add Players To Away Team Line-up',
        'action_team' => 'Add Team\'s Statistics',
        'action_player' => 'Add Player\'s Statistics',
        'result_from_action' => 'Calculate Match Result According To Match Action',
    ),
    'read' => array(
        'match' => 'Basic Match Information',
        'detail' => 'Match Details',
        'referees' => 'Referees',
        'lineup_home' => 'Home Team Line-up',
        'lineup_away' => 'Away Team Line-up',
        'action_team' => 'Team Statistics',
        'action_player' => 'Player Statistics',
    ),
    'update' => array(
        'match' => 'Change Basic Match Details',
        'result' => 'Change Match Result',
        'detail' => 'Change Match Details',
        'referee' => 'Change Referee',
        'player' => 'Change Player From Line-up',
        'action_team' => 'Change Team Statistic',
        'action_player' => 'Change Player Statistic',
    )
);
$lang['header']['prediction'] = array(
    'index' => 'Matches Predictions',
    'create' => array(
        'prediction' => 'Create New Prediction'
    ),
    'read' => array(
        'predictions' => 'My Predictions',
        'user_stats' => 'My Prediction Statistics',
        'match_stats' => 'Match Prediction Statistics',
        'competition_stats' => 'Competition Prediction Statistics',
        'chart' => 'Prediction Chart'
    ),
    'update' => array(
        'prediction' => 'Change prediction score'
    ),
);
$lang['header']['statistics'] = array(
    'index' => 'Statistics',
    'read' => array(
        'person_players' => 'Player Statistics',
        'team_players' => 'Players Statistics in Team',
        'competition_players' => 'Players Statistics in Competition',
        'team' => 'Team Statistics',
        'head_to_head' => 'Head To Head',
        'person_matches' => 'Matches With Statistics for Person',
        'player_matches' => 'Matches With Statistics for Team Player',
    ),
);
$lang['header']['person'] = array(
    'index' => 'List of All Persons',
    'create' => array(
        'person' => 'Create New Person',
        'attribute' => 'Add attribute'
    ),
    'read' => array(
        'person' => 'About Person',
        'club' => 'Member of clubs',
        'team' => 'Member of teams',
        'referee' => 'Referee of matches',
    ),
    'update' => array(
        'person' => 'Change Person Details',
        'attribute' => 'Change Attribute Value'
    )
);
$lang['header']['position'] = array(
    'index' => 'List of All Persons Positions',
    'create' => array(
        'position' => 'Create New Persons Position'
    ),
    'read' => array(
        'position' => 'Person Position Detail',
    ),
    'update' => array(
        'position' => 'Change Person Position'
    )
);
$lang['header']['attribute'] = array(
    'index' => 'List of All Attributes',
    'create' => array(
        'attribute' => 'Create New Attribute'
    ),
    'read' => array(
        'attribute' => 'About Attribute',
    ),
    'update' => array(
        'name' => 'Change Attribute Name'
    )
);
$lang['header']['country'] = array(
    'index' => 'List of All Countries',
    'create' => array(
        'country' => 'Create New Country'
    ),
    'read' => array(
        'country' => 'About Country',
    ),
    'update' => array(
        'name' => 'Change Country Name'
    )
);
$lang['header']['stadium'] = array(
    'index' => 'List of All Stadiums',
    'create' => array(
        'stadium' => 'Create New Stadium'
    ),
    'read' => array(
        'stadium' => 'About Stadium',
    ),
    'update' => array(
        'stadium' => 'Change Stadium Details',
    )
);
$lang['header']['sport'] = array(
    'index' => 'List of All Sports Categories',
    'create' => array(
        'sport' => 'Create New Sport Category'
    ),
    'read' => array(
        'sport' => 'About Sport Category',
    ),
    'update' => array(
        'sport' => 'Change Sport Category Details'
    )
);
$lang['header']['action'] = array(
    'index' => 'List of All Match Actions',
    'create' => array(
        'action' => 'Create New Match Action'
    ),
    'read' => array(
        'action' => 'Match Action Detail',
    ),
    'update' => array(
        'action' => 'Change Match Action'
    )
);
$lang['header']['user'] = array(
    'index' => 'List of All Users',
    'create' => array(
        'user' => 'Create New User',
        'competition' => 'Grant Competition to User'
    ),
    'read' => array(
        'user' => 'User Detail for Admin',
        'inactive_users' => 'Never Logged Users',
        'user_groups' => 'User Groups Permissions',
        'logs' => 'User Activity (logs)',
    ),
    'update' => array(
        'user_group' => 'Change User Group',
    ),
    'delete' => array(
        'logs' => 'Delete Logs',
    )
);
$lang['header']['profile'] = array(
    'index' => 'User Profile',
    'update' => array(
        'email' => 'Change Email',
        'password' => 'Change Password',
    ),
);
$lang['header']['settings'] = array(
    'index' => 'Settings',
    'read' => array(
        'stm' => 'STM settings',
        'database' => 'Database settings',
    ),
    'update' => array(
        'stm' => 'Change STM settings',
    ),
);
$lang['header']['import'] = array(
    'index' => 'Import',
    'create' => array(
        'matches' => 'Import Matches',
        'teams' => 'Import Teams and Clubs',
        'persons' => 'Import Persons',
    ),
    'update' => array(
        'table' => 'Import Season Table',
    ),
);
$lang['header']['export'] = array(
    'index' => 'Export',
    'create' => array(
        'web_club' => 'Export club to web',
        'web_team' => 'Export team to web',
        'web_competition' => 'Export competition to web',
        'web_match' => 'Export match to web',
        'web_person' => 'Export person to web',
        'web_match_selection' => 'Export match selection to web',
        'file_teams' => 'Export teams and clubs',
        'file_competitions' => 'Export competitions',
        'file_matches' => 'Export matches',
        'file_users' => 'Export users',
    ),
);

// General
$lang['name'] = 'Name';
$lang['left-menu-header'] = 'Actions';
$lang['no-content'] = 'No Items';
$lang['not-set'] = 'Not Set';
$lang['forbidden'] = 'Forbidden';
$lang['number'] = 'Number';

// Forms
$lang['form'] = array(
    'create' => 'Create',
    'change' => 'Change',
    'delete' => 'Delete',
    'show' => 'Show',
    'empty' => 'None',
    'required' => 'Required fields',
    'optional' => 'Optional fields',
    'select_all' => 'Select All',
    'add_new_rows' => 'Add new rows',
);

// Index module
$lang['index'] = array(
    // homepage
    'homepage' => array(
        'competitions' => 'Current competitions',
        'teams' => 'Your teams in STM STM',
        'add-player' => 'Add player',
        'change-date' => 'Change match date',
        'no-competitions' => 'You can create new season, playoff or list of matches in section Competitions',
        'no-teams' => 'No team is marked as <strong>Your team</strong>. You can mark team in every competition in section Teams from competitions. Your teams will be highlighted in match schedules, tables etc.',
        'last-matches' => 'Last %d matches of your teams',
        'next-matches' => 'Next %d matches of your teams',
    ),
    // login
    'username' => 'Username',
    'password' => 'Password',
    'login' => 'Login',
    // registration
    'register' => 'Register',
    'instructions' => 'Instructions',
    'step-1' => 'Fill your name',
    'step-2' => 'After successful registration you will be redirected to login page where you will see your password',
    'step-3' => 'Copy password, otherwise you will loss your password',
    'step-4' => 'Login with generated password',
    'step-5' => 'Go to profile (link Profile in right corner)',
    'step-6' => 'Add your email (you will need it if you forgot password)',
    'step-7' => 'If you want change your password (you will need generated password)',
    'advantages' => 'Advantages of registration',
    'access-team' => 'Access to team statistics',
    'predictions' => 'Match scores predicting',
    // about
    'app' => 'Application',
    'version' => 'Version',
    'author' => 'Author',
    'licence' => 'Licence',
    'website' => 'Website',
    'src' => 'Source Code',
);

// Settings module
$lang['settings'] = array(
    // permissions
    'allowed' => 'ALLOWED',
    'denied' => 'DENIED',
    // read database
    'db-server' => 'Database server',
    'db-user' => 'Database user',
    'db-password' => 'Database password',
    'db-name' => 'Database name',
    'db-editing' => 'Edit DB settings',
    'db-web' => 'You cannot edit database in the web administration',
    'db-file' => 'You can only manually edit file <strong>include/config/database.php</strong> then copy file via FTP',
    'db-procedures' => 'Procedures',
    'db-triggers' => 'Triggers',
    // read stm
    'settings-basic' => 'Basic Settings',
    'registration' => 'User registration',
    'visitors' => 'Access of unregistered users',
    'predictions' => 'Matches predictions',
    'default-language' => 'Default language',
    'default-datetime-format' => 'Default format for date and time',
    'club-website' => 'Club website',
    'settings-defaultvalues' => 'Default Values',
    'settings-filtering' => 'Parameters For Match Filtering',
    'filter-max-table' => 'Maximum of matches in calculating table',
    'filter-max-listing' => 'Maximum of matches in match listing',
    'filter-per-page' => 'Number of matches on one page in match listing',
    'statistic-calculation' => 'Player statistic calculation',
    'old-seasons' => 'ID of seasons with table cache that will not be updated or deleted (comma separated)',
    'settings-lineups' => 'Match lineups',
    'minute-in' => 'Since (player in the match)',
    'minute-out' => 'To (player out of the match)',
    // update stm
    'change' => 'Change STM settings',
);

// Country module
$lang['country'] = array(
    'delete-msg' => 'Are you sure? Do you want to delete this country? All clubs and persons from this country will be set to No Coutry. Deleted country is '
);

// Attribute module
$lang['attribute'] = array(
    'usage' => 'Usage count',
    'delete' => 'Delete attribute',
    'delete-msg' => 'Are you sure? Do you want to delete the attribute? Deleted attribute is ',
    'delete-usage' => 'Delete attribute usage',
    'delete-usage-msg' => 'Are you sure? Do you want to delete this attribute from all persons? Usage is deleted for attribute '
);

// Person position module
$lang['position'] = array(
    'abbrevation' => 'Abbrevation',
    'delete-msg' => 'Are you sure? Do you want to delete this position? Deleted position is '
);

// Stadium module
$lang['stadium'] = array(
    'capacity' => 'Capacity',
    'map' => 'map',
    'map-link' => 'Map Link',
    'field-size' => 'Field Size',
    'field-width' => 'Field Width',
    'field-height' => 'Field Height',
    'delete' => 'Delete stadium',
    'delete-msg' => 'Are you sure? Do you want to delete this stadium? All clubs and matches played on this stadion will be set to No Stadium. Deleted stadium is '
);

// Sport module
$lang['sport'] = array(
    'sport' => 'Sport Category',
    'count_childs' => 'Descendants count',
    'count_positions' => 'Person position count',
    'count_match_actions' => 'Match actions count',
    'parent' => 'Parent Sport Category',
    'actions' => 'Match Actions From Sport Category',
    'positions' => 'Person Positions From Sport Category',
    'delete' => 'Delete Sport Category',
    'delete-msg' => 'Are you sure? Do you want to delete sport category '
);

// Category module
$lang['category'] = array(
    'category' => 'Category',
    'description' => 'Description',
    'parent' => 'Parent Category',
    'descendants-count' => 'Descendants Count',
    'descendants' => 'Descendants',
    'competitions-count' => 'Competitions Count',
    'competitions' => 'Competitions in Category',
    'delete' => 'Delete category',
    'delete-msg' => 'Are you sure? Do you want to delete this category? Deleted category is '
);

// Club module
$lang['club'] = array(
    // club
    'city' => 'City',
    'foundation' => 'Foundation date',
    'info' => 'Club info',
    'colors' => 'Club Colors',
    'website' => 'Website',
    'address' => 'Address',
    'email' => 'Email contact',
    'telephone' => 'Telephone contact',
    'stadium' => 'Stadium',
    'country' => 'Country',
    // club members
    'member' => 'Club Member',
    'person' => 'Person',
    'person-detail' => 'Person Detail',
    'position' => 'Position',
    'position-update' => 'Update Position',
    // delete
    'delete' => 'Delete club',
    'delete-msg' => 'Are you sure? Do you want to delete this club? Deleted club is ',
    'delete-member' => 'Delete member',
    'delete-member-msg' => 'Are you sure? Do you want to delete member of the club? Deleted member is ',
);

// Person module
$lang['person'] = array(
    // person
    'forename' => 'Forename',
    'surname' => 'Surname',
    'birth-date' => 'Birth date',
    'characteristic' => 'Characteristic',
    'country' => 'Country',
    // attribute
    'attribute' => 'Attribute',
    'attribute-value' => 'Value',
    'attributes' => 'Attributes',
    'attribute-update' => 'Update attribute value',
    // team
    'team' => 'Team',
    'position' => 'Position',
    'since' => 'Since',
    'to' => 'To',
    // filter form
    'filter' => array(
        'header' => 'Filtering Persons',
        'no-team' => 'Persons not in team',
    ),
    // delete
    'delete' => 'Delete person',
    'delete-msg' => 'Are you sure? Do you want to delete this person? Deleted person is ',
    'delete-attribute' => 'Delete attribute',
    'delete-attribute-msg' => 'Are you sure? Do you want to delete this attribute from person? Deleted attribute is ',
    'delete-attributes' => 'Delete all persons attributes',
    'delete-attributes-msg' => 'Are you sure? Do you want to delete all attributes from person? Person is ',
);

// Team module
$lang['team'] = array(
    'club' => 'Club',
    'last-match' => 'Last match',
    'next-match' => 'Next match',
    // members
    'person' => 'Person',
    'player' => 'Player',
    'links' => 'Links',
    'dress-number' => 'Dress Number',
    'is-player' => 'Is a player',
    // filter members
    'memberfilter-only-players' => 'Only players',
    'memberfilter-no-players' => 'Only other team members',
    'memberfilter-date' => 'Date',
    // upcoming match
    'match-preview' => array(
        'head-to-head-matches' => 'Last head to head matches',
        'head-to-head-table' => 'Overall table of head to head matches',
        'last-matches' => 'Last matches of the team ',
        'season-stats' => 'Statistics of actual season',
    ),
    // delete
    'delete' => 'Delete team',
    'delete-msg' => 'Are you sure? Do you want to delete this team? Deleted team is ',
    'delete-member' => 'Delete member',
    'delete-member-msg' => 'Are you sure? Do you want to delete this team member? All member entries IN MATCHES will be DELETED! Deleted member is ',
    'delete-player' => 'Delete player status',
    'delete-player-msg' => 'Are you sure? Do you want to delete this team player? Person still will be part of the team, but all member entries IN MATCHES will be DELETED! Deleted player is ',
);

// User module
$lang['user'] = array(
    'user' => 'User',
    'group' => 'User Group',
    'email' => 'Email',
    'date-reg' => 'Date Registration',
    'last-login' => 'Last Login',
    // groups
    'all' => 'All Users',
    'registered' => 'Registered Users',
    'comp-managers' => 'Competition Managers',
    'managers' => 'Managers',
    'admins' => 'Administrators',
    'visitors' => 'Visitors',
    // info group page
    'groupinfo' => array(
        'green' => 'GREEN = full access',
        'blue' => 'BLUE = read only',
        'red' => 'RED = no access',
        'competitions' => 'Competitions',
        'matches' => 'Matches',
        'predictions' => 'Matches Predictions',
        'teams' => 'Teams',
        'users' => 'Users',
    ),
    // password change
    'password' => array(
        'old' => 'Old password',
        'new' => 'New password',
        'confirm' => 'Confirm new password',
    ),
    // admin user
    'change-group' => 'Change User Group',
    'reset-pass' => 'Reset Password',
    'never-login' => 'Never',
    'managed-competitions' => 'Managed Competitions',
    'grant-competition' => 'Grant Competition',
    // logs
    'logs' => array(
        'filesize' => 'File Size',
        'competitions' => $lang['menu']['competitions'],
        'matches' => $lang['menu']['matches'],
        'filter' => 'Match filtering',
        'predictions' => $lang['menu']['predictions'],
        'teams' => $lang['menu']['teams'],
        'users' => $lang['menu']['users'],
        'database' => 'Database errors',
    ),
    // delete
    'delete-msg' => 'Are you sure? Do you want to delete user ',
    'delete-account' => 'Delete account',
    'delete-account-msg' => 'Are you sure? Do you want to delete your account? You will not have access to STM.',
    'delete-competition' => 'Revoke competition',
    'delete-competition-msg' => 'Are you sure? Do you want to revoke the competition ',
);

// Match module
$lang['match'] = array(
    'home' => 'Home Team',
    'away' => 'Away Team',
    'date' => 'Date',
    'score' => 'Score',
    'score-home' => 'Home Score',
    'score-away' => 'Away Score',
    'back-to-serie' => 'Back To Playoff Serie',
    // index - filtering
    'filter' => array(
        'header' => 'Filtering',
        'submit' => 'Display My Selection',
        'match-selection' => 'Match Selection',
        'all-matches' => 'All Matches',
        'played-matches' => 'Played Matches',
        'upcoming-matches' => 'Upcoming Matches',
        'load-scores' => 'Load Scores',
        'load-periods' => 'Load Periods',
        'result-table' => 'Calculate Result Table',
        'head-to-head' => 'Only Head-to-Head',
        'page' => 'Page',
    ),
    // create match
    'teams' => 'Teams',
    'season-fields' => 'Season Fields',
    'season-round' => 'Round',
    // predictions - left menu
    'predictions' => 'Predictions',
    'new-prediction' => 'Predicts Score',
    // periods
    'period-number' => 'Period',
    'periods' => 'Periods',
    'period' => array(
        'add' => 'Add Period',
        'note' => 'Note',
    ),
    // detail
    'detail-spectators' => 'Spectators',
    'detail-comment' => 'Match Comment',
    'detail-stadium' => 'Stadium',
    /// referee
    'match-referee' => 'Referee',
    /// lineups
    'lineup-substitute' => 'Substitution Player',
    /// team actions (statistics)
    'team-action' => 'Team Statistic',
    /// player actions
    'player-action' => 'Player Statistic',
    'player-action-minute' => 'When Action Happened',
    'player-action-comment' => 'Action Comment',
    // result from action
    'calculate-score' => array(
        'existing-score' => 'Current score of the match <strong>%s</strong> will be deleted.',
        'calculation' => 'Method of score calculation',
    ),
    // delete
    'delete' => 'Delete match',
    'delete-msg' => 'Are you sure? Do you want to delete this match? Deleted match is ',
    'delete-result' => 'Delete result',
    'delete-result-msg' => 'Are you sure? Do you want to delete result of the match ',
    'delete-period' => 'Delete period',
    'delete-period-msg' => 'Are you sure? Do you want to delete period n.',
    'delete-detail' => 'Delete detail',
    'delete-detail-msg' => 'Are you sure? Do you want to delete detail of the match ',
    'delete-referee' => 'Delete referee',
    'delete-referee-msg' => 'Are you sure? Do you want to delete referee ',
    'delete-player-msg' => 'Are you sure? Do you want to delete player ',
    'delete-teamaction-msg' => 'Are you sure? Do you want to delete team statistic ',
    'delete-playeraction-msg' => 'Are you sure? Do you want to delete player statistic ',
);

// Match action module
$lang['action'] = array(
    'name' => 'Match Action',
    'stats' => 'In Statistics',
    'count_in_teams' => 'Teams',
    'count_in_players' => 'Players',
    'delete-msg' => 'Are you sure? Do you want to delete this match action? Deleted action is ',
    'delete-teamaction' => 'Delete Teams Statistics',
    'delete-teamaction-msg' => 'Are you sure? Do you want to delete from teams statistics action ',
    'delete-playeraction' => 'Delete Players Statistics',
    'delete-playeraction-msg' => 'Are you sure? Do you want to delete from players statistic action ',
);

// Prediction module
$lang['prediction'] = array(
    'match' => 'Predicted Match',
    'result' => 'Match Result',
    'prediction' => 'My Prediction',
    'chart' => array(
        'username' => 'Username',
        'count' => 'Predictions Count',
        'cheat' => 'cheat',
        'exact' => 'Exact Predictions',
        'success' => 'Predictions Success',
        'points' => 'Points'
    ),
    'stats' => array(
        'played' => 'Number of matches that were played',
        'how-many' => 'How many times predicted result was ...',
        'win' => 'Win Home',
        'draw' => 'Draw',
        'loss' => 'Loss Home'
    ),
);

// Competition module
$lang['competition'] = array(
    // general
    'competition' => 'Competition',
    'season' => 'Season',
    'playoff' => 'Playoff',
    'category' => 'Category',
    'match' => 'Match',
    'team' => 'Team',
    'serie' => 'Playoff Serie',
    // detail
    'sections' => array(
        'last-matches' => 'Last matches',
        'next-matches' => 'Upcoming matches',
        'detail' => 'Information about competition',
    ),
    'name' => 'Name',
    'type' => 'Competition Type',
    'date-start' => 'Date Start',
    'date-end' => 'Date End',
    'min-match-periods' => 'Min Match Periods',
    'score-type' => 'Score Type',
    'status' => 'Competition Visibility',
    'periods' => 'Season Periods Count',
    'win' => 'Point Win',
    'win-ot' => 'Point Win in Overtime',
    'draw' => 'Point Draw',
    'loss' => 'Point Loss',
    'loss-ot' => 'Point Loss in Overtime',
    'serie-win-matches' => 'Serie Win Matches',
    // stats
    'stats' => array(
        'team-count' => 'Teams Count',
        'all-matches' => 'All Matches Count',
        'played-matches' => 'Played Matches Count',
        'upcoming-matches' => 'Upcoming Matches Count',
        'total-matches' => 'Total Number of Matches in Season',
        'percentage-matches' => 'Percentage of Played Matches in Season',
        'played-rounds' => 'Played Rounds',
        'total-rounds' => 'Total Rounds',
        'players-counts' => 'Players Count In Teams',
        'players-count' => 'Players Count',
    ),
    // teams
    'teams' => array(
        'mark' => 'Mark as Your Team',
        'unmark' => 'Unmark Your Team',
        'how-add' => 'How Add Team(s) to Competition',
    ),
    // season
    'table' => array(
        'full' => 'Full Table',
        'home' => 'Home Table',
        'away' => 'Away Table',
        'position' => 'Pos',
        'matches' => 'Pl',
        'win' => 'W',
        'winInOt' => 'WOT',
        'draw' => 'D',
        'lossInOt' => 'LOT',
        'loss' => 'L',
        'goalFor' => 'GF',
        'goalAgainst' => 'GA',
        'points' => 'Pts',
        'ext' => 'Extra',
        'truthPoints' => 'Points in truth table'
    ),
    'extra' => array(
        'current' => 'Current Extra Points',
        'add' => 'Add Extra Points',
        'added' => 'Added Points'
    ),
    'schedule' => array(
        'reload' => 'Reload schedule',
        'reload-help' => 'nothing saved to DB, use e.g. if you don\'t like current 1st round of the schedule',
        'save' => 'Save schedule to database',
        'start-date' => 'Start date of period n.',
        'preview' => 'Preview of one period from generated schedule of the season',
        'regenerate' => '<strong>Warning</strong>: season already contains matches, round will be changed in existing matches',
    ),
    'rounds' => array(
        'match-count' => 'Matches in round',
        'load-team-dates' => 'Load current round dates according to matches of team',
    ),
    // plaoyff
    'tree' => array(
        'no-round' => 'No rounds, at least one round has to be generated to display tree',
        'rounds' => 'Rounds',
        'round-n' => 'Round n.',
        'closed' => 'CLOSED',
        'actual' => 'Actual',
        'not-generated' => 'Not Generated',
        'round-select' => 'Round Selection',
        'display-round' => 'Display Series From Selected Round',
        'serie-n' => 'Series from round n.',
        'links' => 'Links',
        'link-detail' => 'Detail',
        'link-addmatch' => 'Add Match',
        'matches' => 'Matches in Serie',
    ),
    'seeded' => array(
        'current' => 'Current Seeded',
        'new' => 'New Seeded',
    ),
    'round' => array(
        'save' => 'Save round to database',
        'generate-n' => 'Generated round n.',
        'teams-seeded' => 'Teams order by seeded',
        'previous' => 'Previous round',
        'seeded' => 'Seeded',
        'seeded-fixed' => 'Minimal seeded from previous serie',
        'seeded-variable' => 'Seeded from winner of serie from this round',
        'change-seeded' => 'Change seeded style of generated round',
    ),
    // summary
    'summary' => array(
        'type' => 'Items to be included in summary',
        'optional-settings' => 'Optional settings',
        'schedule' => array(
            'header' => 'Matches Schedules',
            'fields' => 'Number of Fields',
        ),
        'teams' => array(
            'count' => 'Number of Teams',
            'list' => 'List of teams'
        ),
        'statistics' => array(
            'goals-count' => 'Number of goals',
            'goals-average' => 'Average goals in match',
        ),
        'winners' => array(
            'header' => 'Winners',
            'top-3' => 'Winners\' Podium',
            'top-players' => 'Top Player',
        ),
        'tables' => 'Tables',
        'player-actions' => array(
            'top-5' => 'Top 5 Players',
            'teams' => 'List of Team Players and Statistic',
            'full-list' => 'Complete list, e.g. top scorers',
        ),
        'matches' => array(
            'header' => 'Detailed Matches View',

        ),
    ),
    // delete
    'delete' => 'Delete competition',
    'delete-msg' => 'Are you sure? Do you want to delete the competition ',
    'delete-matches' => 'Delete matches',
    'delete-matches-msg' => 'Are you sure? Do you want to delete all matches from the competition ',
    'delete-team' => 'Delete Team From Competition',
    'delete-team-msg' => 'Are you sure? Do you want to delete team from the competition? Deleted team is ',
    'delete-playoffround' => 'DELETE ROUND',
    'delete-playoffround-msg' => 'Are you sure? Do you want to delete actual round from this playoff?',
    'delete-seasonround' => 'Delete Matches From Round',
    'delete-seasonround-msg' => 'Are you sure? Do you want to delete all matches from round n.',
);

// Import module
$lang['import'] = array(
    'step' => 'Step',
    'go-to-next' => 'Go To Next Step',
    'go-to-previous' => 'Back To Previous Step',
    'save' => 'Import (save to database)',
    'format' => 'Format',
    'remove-invalid' => 'Remove lines which don\'t match format',
    'format-symbols' => 'Format Symbols',
    'format-surrounding' => 'Surrounding text',
    'input' => 'Input text',
    'summary' => 'Import Summary',
    // matches
    'match-count' => 'Created matches',
    'match-count' => 'Created periods',
    'errors-match' => 'Not Created Matches',
    // teams
    'team-count' => 'Created teams',
    'club-count' => 'Created clubs',
    'errors-team' => 'Not Created Teams Or Clubs',
    // table
    'table-all' => 'Import home and away table too (otherwise full table will be used for home and away table)',
    // persons
    'person-count' => 'Created persons',
    'team-member-count' => 'Created team players',
);

// Statistics
$lang['statistics'] = array(
    'played-matches' => 'Played Matches',
    'played-time' => 'Played Time',
    'team-overall' => 'Overall',
    'team-home' => 'Home',
    'team-away' => 'Away',
    'team-difference' => 'Difference',
    'team-positions' => 'Team Positions In Table',
    'team-overall-results' => 'Overall results',
    'records' => 'Records',
    'highestAggregateScore' => 'Matches with highest aggregate score',
    'highestGoalFor' => 'The highest goal for in match',
    'highestGoalAgainst' => 'The highest foal against in match',
    'highestWin' => 'The highest win',
    'highestLoss' => 'The highest loss',
    'head-to-head' => array(
        'last-matches' => 'Last 5 matches',
    ),
);

// Export
$lang['export'] = array(
    'web' => 'Web Export (PHP Code)',
    'database' => 'Database Export (File)',
    'limit-max' => 'Max Matches',
    'limit-offset' => 'Start Offset',
    'order-priority' => 'Order Priority',
    'order-match-id' => 'Order by Match ID',
    'order-round' => 'Order by Round',
    'order-datetime' => 'Order by Date',
    'order-is-ascending' => 'Ascending',
);

// Error
$lang['error'] = array(
    'report' => 'Report Error',
    'desc' => 'Error Description',
);

// Constants
$lang['constants'] = array(
    'usergroup' => array(
        \STM\User\UserGroup::ADMIN => 'Admin',
        \STM\User\UserGroup::MANAGER => 'Manager',
        \STM\User\UserGroup::COMPETITION_MANAGER => 'Competition Manager',
        \STM\User\UserGroup::REGISTERED_USER => 'Registered User',
    ),
    'scoretype' => array(
        \STM\Match\ScoreType::FOOTBALL => 'Football Scores',
        \STM\Match\ScoreType::TENNIS => 'Tennis Scores'
    ),
    'competitiontype' => array(
        \STM\Competition\CompetitionType::COMPETITION => 'Matches',
        \STM\Competition\CompetitionType::SEASON => 'Season',
        \STM\Competition\CompetitionType::PLAYOFF => 'Playoff',
    ),
    'competitionstatus' => array(
        \STM\Competition\CompetitionStatus::PUBLICS => 'Public',
        \STM\Competition\CompetitionStatus::VISIBLE => 'Visible',
        \STM\Competition\CompetitionStatus::HIDDEN => 'Hidden',
    ),
    'statisticcalculationtype' => array(
        \STM\Match\Stats\Action\StatisticCalculationType::COUNT => 'Count',
        \STM\Match\Stats\Action\StatisticCalculationType::SUM => 'Sum',
    ),
);

// Form Errors
$lang['form-error'] = array(
    'infotext' => 'The operation could not be performed because one or more error(s) occurred',
    'db-fail' => 'Error during creating in DB, try it again',
    'invalid-input' => 'Missing required data',
    // formats for sprintf
    'empty-value' => '%s must be filled.',
    'no-change' => '%s - no change',
    'string-length' => '%s string length must be from %d to %d',
    'number-length' => '%s size must be from %d to %d',
    'invalid-value' => '%s must be %s',
    'invalid-date' => '%s - invalid date format',
    'invalid-date2' => '%s date must be before date %s',
    'invalid-website' => '%s must be valid internet address',
    'invalid-email' => '%s must be valid email address',
    'unique-value' => '%s already exists',
    // other
    'login' => 'Username/Password - incorrect combination',
    'date-integrity' => 'Dates must be in ascending order',
    'compare-birth-date' => 'Dates must be younger than birth date of the person',
    'invalid-filename' => 'Name is not valid filename. Name cannot contains space / ? * : ; { } \\',
    'password-same' => 'New and old are same passwords',
    'password-old' => 'Error in old password',
    'password-confirmation' => 'New and confirmation does not match',
    'last-admin' => 'You cannot change user group of last admin',
    'category-parent' => 'Category cannot be own parent',
    'team-club' => 'Team with this name is already in club',
    'seeded-unique' => 'Every team has to have unique seeded (integer)',
    'seeded-firstround' => 'Seeded can be only changed before the first round is generated',
    'no-teams' => 'No teams in selected competition',
    'point-integrity' => 'Points must be in order: WIN >= WIN IN OVERTIME >= DRAW >= LOSS IN OT >= LOSS',
    'nothing-checked' => 'No items were selected',
    'sport-unique' => 'Sport category with this name is already in selected category',
    // MatchValidator
    'match-teams' => 'Home and Away must be different teams',
    'match-playoff' => 'Team change in playoff is not allowed, you can only switch teams',
    'match-seasonround' => 'One of the team is already in other match of season round - team can be once in one season round',
    'match-seasonperiods' => 'Teams can play against each other only %d (number of season periods)',
    'match-serie' => 'You cannot add match to serie from closed round',
    'match-filter-maxteams' => 'Maximum is %d matches in filtering for selected action (calculating table or match listing)',
    'match-playoff-serie-seas' => 'Serie cannot be set in Season',
    'match-seasonround-play' => 'Match round cannot be set in Playoff',
    'match-seasonround-comp' => 'Match round cannot be set in ordinary Competition',
    'match-playoffserie-comp' => 'Serie cannot be set in ordinary Competition',
    'match-seasonround-set' => 'For season round must be set',
    // MatchPlayerValidator
    'match-substitution-team' => 'Substitution player must be from same team and match',
    'match-substitution-player' => 'Substitution player must be different than current player',
    'match-player' => 'Person must be player from the team, not only member of the team',
    // PredictionValidator
    'prediction-date' => 'You can predicts match score only if match was not played',
    'prediction-competition' => 'Predicting matches from the competition is not allowed',
    'prediction-score' => 'You cannot predict match with saved score',
    // PlayoffRoundValidator
    'playoffround-winner' => 'All series must have winner',
    'playoffround-seeded' => 'All teams must have seeded',
    // Templates
    'file-not-saved' => 'File was not saved, try it again',
    // Import Matches
    'import-playoff' => 'Playoff matches cannot be imported',
    'import-reguired-competition' => 'Symbols for home and away team are required in format',
    'import-reguired-season' => 'Round symbol is required if you import matches to season',
    'import-reguired-person' => 'Symbols for forename and surname are required in format',
    'import-invalid-team' => 'Team "%s" is not from selected competition',
    // Import Season Table
    'import-seasontable-teams' => '%s - all teams must be from selected season',
    'import-seasontable-numbers' => '%s - all values must be number from 0 to 10000',
    'import-seasontable-structure' => '%s - missing required values',
    // Import Persons
    'import-persons-team' => 'Unexisting team',
    'import-persons-position' => 'Unexisting position',
);

// Session message
$lang['session'] = array(
    // index
    'login' => 'Successful login. Welcome %s!',
    'registration' => 'Successful registration<br />Username: <strong>%s</strong>, password: <strong>%s</strong>',
    'create-user' => 'User %s created with password <strong>%s</strong>',
    'visitor-access' => 'Visitors don\'t have access to this page',
    // general
    'change' => '%s <em>%s</em> - successfully changed',
    'change-fail' => '%s <em>%s</em> - failed changing',
    'create' => '%s <em>%s</em> - successfully created',
    'create-fail' => '%s <em>%s</em> - failed creating',
    'delete-success' => '%s <em>%s</em> - successfully deleted',
    'delete-fail' => '%s <em>%s</em> - failed deleting, try it again',
    'invalid' => '%s - non-existent item was selected',
    // page exceptions
    'page-module' => 'Non-existing module',
    'page-action' => 'Invalid type of action (only create, read, update and delete)',
    'page-type' => 'Not allowed action in module',
    'allow-prediction' => 'Match predictions is not allowed',
    'only-logged-users' => 'Only logged users can access this module',
    // other
    'authorization' => 'You don\'t have authorization to this page',
    'delete-csrf' => 'Failed deleting, CSRF attack',
    'delete-input' => 'Failed deleting, bad input arguments',
    'undefined-error' => 'Undefined error, try it again',
    'attribute-delete' => 'Attribute cannot be deleted, if at least one person use this attribute',
    'attribute-usage' => 'No person has set this attribute',
    'sport-delete' => 'Sport category cannot be deleted if positions or match actions are under sport category',
    'position-delete' => 'Person position cannot be deleted if club or team member has this position',
    'action-delete' => 'Match action cannot be deleted if it\'s used in team or player statistics',
    'action-players-delete' => 'Match action is not used in players statistics',
    'action-teams-delete' => 'Match action is not used in teams statistics',
    // members and clubs/teams
    'invalid-member' => 'Non-existent member',
    'team-add-member' => 'You created %d new team members from %d selected persons',
    'club-add-member' => 'You created %d new club members from %d selected persons',
    'delete-club' => 'Club cannot be deleted, there are teams or members under the club',
    'delete-person' => 'Person cannot be deleted, persons is in club/team/referees',
    // match
    'match-create' => 'To add match competition has to contain at least 2 teams.',
    'match-competition' => 'You have to select one competition if you want to create new match',
    'match-round' => 'You can change round only for match from Season',
    'match-serie' => 'Deleting is not allowed in closed round of playoff',
    'match-result' => 'Match has not result',
    'match-create-result' => 'Result sucessfully created (number of created periods: %d)',
    'match-update-result' => 'Result sucessfully updated (number of updated periods: %d)',
    /// detail
    'match-detail-notset' => 'Match details are not set',
    /// referee
    'match-referee-add' => 'Successful add of new referee',
    'match-referee-invalid' => 'Invalid referee',
    /// players
    'match-player-create' => 'You created %d players from %d selected persons',
    'match-player-action-create' => 'You created %d match actions from %d selected actions',
    // prediction
    'prediction-existing' => 'You have already predicted this match. You cannot create new prediction, but you can update score of your prediction',
    'prediction-match' => 'You have to select one existing match if you want to create new prediction or view match statistics',
    'prediction-competition' => 'You have to select one existing competition if you want to view competition prediction statistics or chart',
    // team
    'create-player' => 'Member is already player',
    'delete-team' => 'Team cannot be deleted if it is assigned to a competition or members under the team',
    // user
    'user-grant' => 'You can add new competition only to CompetitionManager',
    'user-revoke' => 'You can revoke competition only from CompetitionManager',
    'user-add-competition' => 'Competition was successfully add to user',
    'user-reset' => 'Successful update for %s, new password = <strong>%s</strong>',
    'user-logout' => 'Successful logout',
    'user-change-password' => 'Password was changed. Log in with new password',
    'user-delete-account' => 'Your user account was deleted',
    // competition
    'competition-playoffround' => 'Action is allowed only before first round is generated',
    'competition-playoffmatches' => 'Delete matches is not allowed in playoff, in playoff you can only delete rounds and matches from actual round',
    'competition-addteams' => 'Successful add of team(s) to competition',
    'delete-competition' => 'Competition cannot be deleted, if matches are assigned to competition',
    'delete-competitionmatches' => 'Competition doesn\'t contain matches.',
    'competition-team' => 'Invalid team',
    'competition-import-players' => "%d players were added to %d existing matches. %d players already exist in lineup",
    'competition-import-players-fail' => "%d players weren't added to %d existing matches. %d players already exist in lineup",
    'competition-round-date-update' => 'You changed dates in %d rounds from %d selected rounds',
    // season
    'season-teamcount' => 'In season there has to be <2, %d> teams if you want to generate schedule.',
    'season-generated-schedule' => 'Season schedule was generated',
    'season-full-table-sort-success' => 'Teams with equal points were sorted in full table by head to head matches',
    'season-full-table-sort-fail' => 'Teams with equal points were not sorted in full table by head to head matches, try it again',
    // playoff
    'playoff-teamcount' => 'Invalid team count - count must be power of two and <= 128 if you want to generate playoff round',
    'playoff-generated' => 'Playoff is completely generated',
);
