<?php
// Available languages (title of the image)
$lang['lang-titles'] = array(
    'czech' => 'Čeština',
    'english' => 'Angličtina',
);

// Layout
$lang['layout'] = array(
    'header' => array(
        'profile' => 'Profil',
        'logout' => 'Odhlásit',
        'visitor' => 'Návštěvník',
        'login' => 'Přihlásit',
        'register' => 'Zaregistrovat',
        'back-to-web' => 'Zpět na web'
    ),
    'footer' => array(
        'map' => 'Mapa webu',
        // same in js files
        'hide-left-menu' => 'Skrýt levé menu',
        'show-left-menu' => 'Zobrazit levé menu',
    ),
    'homepage' => 'Hlavní stránka sekce',
    'previous' => 'Předchozí stránka',
);

// Menu
$lang['menu'] = array(
    'competitions' => 'Soutěže',
    'categories' => 'Kategorie',
    'matches' => 'Zápasy',
    'import-matches' => 'Import zápasů',
    'predictions' => 'Tipování výsledků',
    'chart' => 'Žebříček',
    'my-predictions' => 'Moje tipy',
    'my-stats' => 'Moje statistiky',
    'teams' => 'Týmy',
    'clubs' => 'Kluby',
    'import-teams' => 'Import týmů a klubů',
    'head-to-head' => 'Souboj dvou týmů',
    'persons' => 'Lidé',
    'positions' => 'Pozice',
    'attributes' => 'Atributy',
    'import-persons' => 'Import osob',
    'other' => 'Ostatní',
    'sports' => 'Sportovní kategorie',
    'actions' => 'Zápasové akce',
    'stadiums' => 'Stadiony',
    'countries' => 'Státy',
    'admin' => 'Administrátor',
    'users' => 'Uživatelé',
    'settings' => 'Nastavení STM',
    'export' => 'Export',
    'import' => 'Import',
    'help' => '<img src="web/images/icons/help.png" class="help" title="Přejít do nápovědy pro aktuální stránku" />'
);

// Page headers (h1)  ['header', 'module_name', 'action', 'type]
$lang['header']['index'] = array(
    'index' => 'Sports Table Manager',
    'read' => array(
        'login' => 'Přihlášení',
        'registration' => 'Registrace',
        'about' => 'O aplikaci STM',
        'map' => 'Mapa webu'
    )
);
$lang['header']['team'] = array(
    'index' => 'Seznam všech týmů',
    'create' => array(
        'team' => 'Vytvoření nového týmu',
        'member' => 'Přidání člena do týmu'
    ),
    'read' => array(
        'team' => 'Detail týmu',
        'competitions' => 'Soutěže, kterých se tým účastní',
        'members' => 'Týmoví členové',
        'upcoming_match' => 'Příští zápas týmu',
    ),
    'update' => array(
        'team' => 'Změna týmových údajů',
        'member' => 'Změna člena týmu',
    )
);
$lang['header']['club'] = array(
    'index' => 'Seznam všech klubů',
    'create' => array(
        'club' => 'Vytvoření nového klubu',
        'member' => 'Přidání člena klubu'
    ),
    'read' => array(
        'club' => 'Detail klubu',
        'teams' => 'Týmy patřící ke klubu',
        'members' => 'Kluboví členové'
    ),
    'update' => array(
        'club' => 'Změna údajů o klubu',
        'member' => 'Změna člena klubu'
    )
);
$lang['header']['competition'] = array(
    'index' => 'Seznam všech soutěží',
    'create' => array(
        'competition' => 'Vytvoření nové soutěže',
        'team' => 'Přidání týmů do soutěže',
        'teams' => 'Vložení týmů z jiné soutěže',
        'players' => 'Přidání všech týmových hráčů do všech zápasů v soutěži',
        'summary' => 'Vytvoření souhrnu soutěže'
    ),
    'read' => array(
        'competition' => 'Detail soutěže',
        'matches' => 'Zápasy v soutěži',
        'statistics' => 'Statistika soutěže',
        'teams' => 'Týmy hrající soutěž',
        'advanced_actions' => 'Pokročilá správa soutěže',
    ),
    'update' => array(
        'competition' => 'Změna údajů o soutěži',
    )
);
$lang['header']['season'] = array(
    'index' => 'Sezóny',
    'create' => array(
        'season' => 'Vytvoření nové sezóny',
        'schedule' => 'Generování rozpisu sezóny',
    ),
    'read' => array(
        'tables' => 'Tabulky sezóny',
        'rounds' => 'Kola sezóny',
        'cross_table' => 'Křížová tabulka',
    ),
    'update' => array(
        'season' => 'Změna sezónních údajů',
        'extra_points' => 'Extra body týmu',
        'round_date' => 'Změna data zápasů v kolech sezóny',
        'table' => 'Seřadit týmy se stejným počtem bodů v celkové tabulce podle vzájemných zápasů',
    )
);
$lang['header']['playoff'] = array(
    'index' => 'Playoff',
    'create' => array(
        'playoff' => 'Vytvoření nového playoff',
        'round' => 'Generování kola playoff'
    ),
    'read' => array(
        'series' => 'Série z playoff',
        'serie' => 'Detail playoff série',
        'tree' => 'Playoff pavouk'
    ),
    'update' => array(
        'playoff' => 'Změna playoff údajů',
        'seeded' => 'Změna nasazení týmů'
    )
);
$lang['header']['category'] = array(
    'index' => 'Seznam všech kategorií',
    'create' => array(
        'category' => 'Vytvoření nové kategorie',
    ),
    'read' => array(
        'category' => 'Detail kategorie',
    ),
    'update' => array(
        'category' => 'Změna údajů o kategorii',
    )
);
$lang['header']['match'] = array(
    'index' => 'Seznam všech zápasů',
    'create' => array(
        'match' => 'Vytvoření nového zápasu',
        'period' => 'Přidání periody zápasu',
        'result' => 'Přidat výsledek',
        'round_results' => 'Vložení výsledků zápasů',
        'referee' => 'Přidat rozhodčího',
        'lineup_home' => 'Přidat hráče do sestavy domácího týmu',
        'lineup_away' => 'Přidat hráče do sestavy hostujícího týmu',
        'action_team' => 'Přidat týmové statistiky',
        'action_player' => 'Přidat hráčské statistiky',
        'result_from_action' => 'Vypočítat výsledek zápasu podle zápasové akce',
    ),
    'read' => array(
        'match' => 'Základní informace o zápasu',
        'detail' => 'Detail zápasu',
        'referees' => 'Rozhodčí zápasu',
        'lineup_home' => 'Sestava domácího týmů',
        'lineup_away' => 'Sestava hostujícího týmů',
        'action_team' => 'Týmové statistiky',
        'action_player' => 'Hráčské statistiky',
    ),
    'update' => array(
        'match' => 'Změna zápasových údajů',
        'result' => 'Změna výsledku zápasu',
        'detail' => 'Změna detailu zápasu',
        'referee' => 'Změna rozhodčího',
        'player' => 'Změna hráče sestavy',
        'action_team' => 'Změna týmové statistiky',
        'action_player' => 'Změna hráčské statistiky',
    )
);
$lang['header']['prediction'] = array(
    'index' => 'Tipování zápasů',
    'create' => array(
        'prediction' => 'Vytvoření nového tipu'
    ),
    'read' => array(
        'predictions' => 'Moje tipy',
        'user_stats' => 'Statistiky mého tipování',
        'match_stats' => 'Statistiky tipování zápasu',
        'competition_stats' => 'Statistiky tipování soutěže',
        'chart' => 'Žebříček tipérů'
    ),
    'update' => array(
        'prediction' => 'Změna tipovaného skóre'
    ),
);
$lang['header']['statistics'] = array(
    'index' => 'Statistiky',
    'read' => array(
        'person_players' => 'Statistiky hráče',
        'team_players' => 'Hráčské statistiky týmu',
        'competition_players' => 'Hráčské statistiky soutěže',
        'team' => 'Statistiky týmu',
        'head_to_head' => 'Souboj dvou týmů',
        'person_matches' => 'Zápasy se zápisem do statistik osoby',
        'player_matches' => 'Zápasy se zápisem do statistik týmového hráče',
    ),
);
$lang['header']['person'] = array(
    'index' => 'Seznam všech lidí',
    'create' => array(
        'person' => 'Vytvoření nové osoby',
        'attribute' => 'Přidání atributu k osobě'
    ),
    'read' => array(
        'person' => 'Detail osoby',
        'club' => 'Člen klubu',
        'team' => 'Člen týmu',
        'referee' => 'Rozhodčí zápasů',
    ),
    'update' => array(
        'person' => 'Změna osobních údajů',
        'attribute' => 'Změna hodnoty atributu'
    )
);
$lang['header']['position'] = array(
    'index' => 'Seznam všech pozic osob',
    'create' => array(
        'position' => 'Vytvoření nové pozice'
    ),
    'read' => array(
        'position' => 'Detail pozice osoby',
    ),
    'update' => array(
        'position' => 'Změna pozice osoby'
    )
);
$lang['header']['attribute'] = array(
    'index' => 'Seznam všech atributů',
    'create' => array(
        'attribute' => 'Vytvoření nového atributu'
    ),
    'read' => array(
        'attribute' => 'Detail atributu',
    ),
    'update' => array(
        'name' => 'Změna jména atributu'
    )
);
$lang['header']['country'] = array(
    'index' => 'Seznam všech států',
    'create' => array(
        'country' => 'Vytvoření nového státu'
    ),
    'read' => array(
        'country' => 'Detail státu',
    ),
    'update' => array(
        'name' => 'Změna jména státu'
    )
);
$lang['header']['stadium'] = array(
    'index' => 'Seznam všech stadionů',
    'create' => array(
        'stadium' => 'Vytvoření nového stadionu'
    ),
    'read' => array(
        'stadium' => 'Detail stadionu',
    ),
    'update' => array(
        'stadium' => 'Změna údajů o stadionu',
    )
);
$lang['header']['sport'] = array(
    'index' => 'Seznam všech sportovních kategorií',
    'create' => array(
        'sport' => 'Vytvoření nové sportovní kategorie '
    ),
    'read' => array(
        'sport' => 'Detail sportovní kategorie',
    ),
    'update' => array(
        'sport' => 'Změna údajů sportovní kategorie'
    )
);
$lang['header']['action'] = array(
    'index' => 'Seznam všech zápasových akcí',
    'create' => array(
        'action' => 'Vytvoření nové zápasové akce'
    ),
    'read' => array(
        'action' => 'Detail zápasové akce',
    ),
    'update' => array(
        'action' => 'Změna zápasové akce'
    )
);
$lang['header']['user'] = array(
    'index' => 'Seznam všech uživatelů',
    'create' => array(
        'user' => 'Vytvoření nového uživatele',
        'competition' => 'Přidání práv uživateli k soutěži'
    ),
    'read' => array(
        'user' => 'Detail uživatele pro administrátora',
        'inactive_users' => 'Nikdy nepřihlášení uživatelé',
        'user_groups' => 'Práva uživatelských skupin',
        'logs' => 'Uživatelská aktivita (logy)',
    ),
    'update' => array(
        'user_group' => 'Změna uživatelské skupiny',
    ),
    'delete' => array(
        'logs' => 'Smazat logy',
    )
);
$lang['header']['profile'] = array(
    'index' => 'Uživatelský profil',
    'update' => array(
        'email' => 'Změna emailu',
        'password' => 'Změna hesla',
    ),
);
$lang['header']['settings'] = array(
    'index' => 'Nastavení',
    'read' => array(
        'stm' => 'Nastavení STM',
        'database' => 'Nastavení databáze',
    ),
    'update' => array(
        'stm' => 'Změna nastavení STM',
    ),
);
$lang['header']['import'] = array(
    'index' => 'Import',
    'create' => array(
        'matches' => 'Import zápasů',
        'teams' => 'Import týmů a klubů',
        'persons' => 'Import osob',
    ),
    'update' => array(
        'table' => 'Import tabulky sezóny',
    ),
);
$lang['header']['export'] = array(
    'index' => 'Export',
    'create' => array(
        'web_club' => 'Export klubu na web',
        'web_team' => 'Export týmu na web',
        'web_competition' => 'Export soutěže na web',
        'web_match' => 'Export zápasu na web',
        'web_person' => 'Export osoby na web',
        'web_match_selection' => 'Export výběru zápasů na web',
        'file_teams' => 'Export týmů a klubů',
        'file_competitions' => 'Export soutěží',
        'file_matches' => 'Export zápasů',
        'file_users' => 'Export uživatelů',
    ),
);

// General
$lang['name'] = 'Jméno';
$lang['left-menu-header'] = 'Akce';
$lang['no-content'] = 'Žádné položky';
$lang['not-set'] = 'Nenastaveno';
$lang['forbidden'] = 'Zakázáno';
$lang['number'] = 'Číslo';

// Forms
$lang['form'] = array(
    'create' => 'Vytvořit',
    'change' => 'Změnit',
    'delete' => 'Smazat',
    'show' => 'Zobrazit',
    'empty' => 'Nic',
    'required' => 'Povinná pole',
    'optional' => 'Nepovinná pole',
    'select_all' => 'Vybrat vše',
    'add_new_rows' => 'Přidat další řádky',
);

// Index module
$lang['index'] = array(
    // homepage
    'homepage' => array(
        'competitions' => 'Aktuálně rozehrané soutěže',
        'teams' => 'Vaše týmy v STM',
        'add-player' => 'Přidat hráče',
        'change-date' => 'Upravit datum zápasu',
        'no-competitions' => 'Žádná soutěž není vytvořena. Novou sezónu, playoff nebo obyčejný seznam zápasů můžete založit v sekci Soutěže',
        'no-teams' => 'Žádný tým není označen jako <strong>Váš tým</strong>. Tým můžete označit v každé soutěži v sekci Týmy hrající soutěže. Takové týmy pak budou zvýrazněny v rozpisech zápasů, tabulkách sezóny apod.',
        'last-matches' => 'Posledních %d zápasů vašich týmů',
        'next-matches' => 'Příštích %d zápasů vašich týmů',
    ),
    // login
    'username' => 'Jméno',
    'password' => 'Heslo',
    // registration
    'register' => 'Registrovat',
    'instructions' => 'Návod',
    'step-1' => 'Vyplňte jméno',
    'step-2' => 'Po úspěšné registraci budete přesměrování na přihlášení, kde uvidíte vaše heslo',
    'step-3' => 'Zkopírujte si heslo, jinak ho ztratíte',
    'step-4' => 'Přihlašte se s vygenerovaným heslem',
    'step-5' => 'Přejděte do profilu (odkaz v pravém horním rohu)',
    'step-6' => 'Přidejte svůj email (volitelné, používá se v případě zapomenutého hesla)',
    'step-7' => 'Změňte si vygenerované heslo',
    'advantages' => 'Výhody registrace',
    'access-team' => 'Přístup ke statistikám a detailům týmů',
    'predictions' => 'Možnost tipování výsledků zápasů',
    // about
    'login' => 'Přihlásit',
    'app' => 'Aplikace',
    'version' => 'Verze',
    'author' => 'Autor',
    'licence' => 'Licence',
    'website' => 'Web',
    'src' => 'Zdrojový kód',
);

// Settings module
$lang['settings'] = array(
    // permissions
    'allowed' => 'POVOLENO',
    'denied' => 'ZAKÁZÁNO',
    // read database
    'db-server' => 'Databázový server',
    'db-user' => 'Databázový uživatel',
    'db-password' => 'Databázové heslo',
    'db-name' => 'Jméno databáze',
    'db-editing' => 'Úpravy nastavení databáze',
    'db-web' => 'Přes webové rozhraní nelze editovat databázové nastavení',
    'db-file' => 'Pro editaci je nutno ručně upravit soubor <strong>include/config/database.php</strong> a ten následně zkopírovat na server přes FTP.',
    'db-procedures' => 'Procedury',
    'db-triggers' => 'Triggery',
    // read stm
    'settings-basic' => 'Základní nastavení aplikace',
    'registration' => 'Registrace uživatelů',
    'visitors' => 'Přístup nezaregistrovaných uživatelů',
    'predictions' => 'Tipování výsledků zápasů',
    'default-language' => 'Výchozí jazyk',
    'default-datetime-format' => 'Výchozí formát pro datum a čas',
    'club-website' => 'Web klubu',
    'settings-defaultvalues' => 'Výchozí hodnoty',
    'settings-filtering' => 'Parametry pro filtrování zápasů',
    'filter-max-table' => 'Maximální počet zápasů pro výpočet tabulky',
    'filter-max-listing' => 'Maximální počet zápasů pro výpis zápasů',
    'filter-per-page' => 'Počet zápasů na jedné stránce ve výpisu zápasů',
    'old-seasons' => 'ID sezón, jejichž cache tabulek nebude aktualizována ani mazána (oddělené čárkou)',
    'statistic-calculation' => 'Výpočet statistik hráčů',
    'settings-lineups' => 'Zápasové sestavy',
    'minute-in' => 'Od (vstup hráče do zápasu)',
    'minute-out' => 'Do (odchod hráče ze zápasu)',
    // update stm
    'change' => 'Změnit nastavení STM',
);

// Country module
$lang['country'] = array(
    'delete-msg' => 'Jste si jisti? Opravdu chcete smazat tento stát? Všem klubům a osobám z tohoto státu bude nastaven stát na nevyplněno. Mažete stát '
);

// Attribute module
$lang['attribute'] = array(
    'usage' => 'Počet použití',
    'delete' => 'Smazat atribut',
    'delete-msg' => 'Jste si jisti? Opravdu chcete smazat tento atribut? Mažete atribut ',
    'delete-usage' => 'Smazat přiřazení atributu',
    'delete-usage-msg' => 'Jste si jisti? Opravdu chcete odstranit přiřazení atributu od všech osob? Mažete přiřazení u osob, které mají atribut '
);

// Person position module
$lang['position'] = array(
    'abbrevation' => 'Zkratka',
    'delete-msg' => 'Jste si jisti? Opravdu chcete odstranit tuto pozici? Mažete pozici '
);

// Stadium module
$lang['stadium'] = array(
    'capacity' => 'Kapacita',
    'map' => 'mapa',
    'map-link' => 'Odkaz na mapu',
    'field-size' => 'Rozměry hřiště',
    'field-width' => 'Šířka hřiště',
    'field-height' => 'Délka hřiště',
    'delete' => 'Smazat stadion',
    'delete-msg' => 'Jste si jisti? Opravdu chcete odstranit tento stadion? Všem klubům a zápasům hraných na tomto stadionu bude nastaven stadion na nevyplněno. Mažete stadion '
);

// Sport module
$lang['sport'] = array(
    'sport' => 'Sportovní kategorie',
    'count_childs' => 'Počet podkategorií',
    'count_positions' => 'Počet pozic v kategorii',
    'count_match_actions' => 'Počet zápasových akcí v kategorii',
    'parent' => 'Rodičovský sport',
    'actions' => 'Zápasové akce patřící pod sportovní kategorii',
    'positions' => 'Pozice osob patřící pod sportovní kategorii',
    'delete' => 'Smazat sportovní kategorii',
    'delete-msg' => 'Jste si jisti? Opravdu chcete odstranit sportovní kategorii '
);

// Category module
$lang['category'] = array(
    'category' => 'Kategorie',
    'description' => 'Popis',
    'parent' => 'Rodičovská kategorie',
    'descendants-count' => 'Počet podkategorií',
    'descendants' => 'Podkategorie',
    'competitions-count' => 'Počet soutěží v kategorii',
    'competitions' => 'Soutěže v kategorii',
    'delete' => 'Smazat kategorii',
    'delete-msg' => 'Jste si jisti? Opravdu chcete odstranit tuto kategorii? Mažete kategorii '
);

// Club module
$lang['club'] = array(
    // club
    'city' => 'Město',
    'foundation' => 'Datum založení',
    'info' => 'O klubu',
    'colors' => 'Klubové barvy',
    'website' => 'Web',
    'address' => 'Adresa',
    'email' => 'Emailový kontakt',
    'telephone' => 'Telefonní kontakt',
    'stadium' => 'Stadion',
    'country' => 'Stát',
    // club members
    'member' => 'Člen klubu',
    'person' => 'Osoba',
    'person-detail' => 'Detail osoby',
    'position' => 'Pozice',
    'position-update' => 'Změnit pozici',
    // delete
    'delete' => 'Smazat klub',
    'delete-msg' => 'Jste si jisti? Opravdu chcete odstranit tento klub? Mažete klub ',
    'delete-member' => 'Smazat člena',
    'delete-member-msg' => 'Jste si jisti? Opravdu chcete odstranit člena z tohoto klubu? Mažete člena ',
);

// Person module
$lang['person'] = array(
    // person
    'forename' => 'Jméno',
    'surname' => 'Příjmení',
    'birth-date' => 'Datum narození',
    'characteristic' => 'Charakteristika',
    'country' => 'Stát',
    // attribute
    'attribute' => 'Atribut',
    'attribute-value' => 'Hodnota',
    'attributes' => 'Atributy',
    'attribute-update' => 'Změnit hodnotu atributu',
    // team
    'team' => 'Tým',
    'position' => 'Pozice',
    'since' => 'Od',
    'to' => 'Do',
    // filter form
    'filter' => array(
        'header' => 'Filtrovat osoby',
        'no-team' => 'Lidé bez týmu',
    ),
    // delete
    'delete' => 'Smazat osobu',
    'delete-msg' => 'Jste si jisti? Opravdu chcete odstranit tuto osobu? Mažete osobu ',
    'delete-attribute' => 'Smazat atribut',
    'delete-attribute-msg' => 'Jste si jisti? Opravdu chcete odstranit tento atribut od osoby? Mažete atribut ',
    'delete-attributes' => 'Smazat všechny osobní atributy',
    'delete-attributes-msg' => 'Jste si jisti? Opravdu chcete odstranit všechny hodnoty atributů od této osoby? Atributy mažete od osoby ',
);

// Team module
$lang['team'] = array(
    'club' => 'Klub',
    'last-match' => 'Poslední zápas',
    'next-match' => 'Příští zápas',
    // members
    'person' => 'Osoba',
    'player' => 'Hráč',
    'links' => 'Odkazy',
    'dress-number' => 'Číslo dresu',
    'is-player' => 'Je hráčem',
    // filter members
    'memberfilter-only-players' => 'Pouze hráči',
    'memberfilter-no-players' => 'Pouze realizační tým',
    'memberfilter-date' => 'Datum',
    // upcoming match
    'match-preview' => array(
        'head-to-head-matches' => 'Poslední vzájemné zápasy',
        'head-to-head-table' => 'Celková tabulka ze všech vzájemných zápasů',
        'last-matches' => 'Poslední zápasy týmu',
        'season-stats' => 'Statistiky z aktuální sezóny, odkud zápas pochází',
    ),
    // delete
    'delete' => 'Smazat tým',
    'delete-msg' => 'Jste si jisti? Opravdu chcete odstranit tento tým? Mažete tým ',
    'delete-member' => 'Smazat člena',
    'delete-member-msg' => 'Jste si jisti? Opravdu chcete smazat tohoto člena týmu? Všechny záznamy s hráčem v ZÁPASECH BUDOU SMAZÁNY! Mažete člena ',
    'delete-player' => 'Odebrat status hráče',
    'delete-player-msg' => 'Jste si jisti? Opravdu chcete smazat tohoto hráče z týmu? Osoba bude stále členem týmu, ale všechny záznamy s hráčem v ZÁPASECH BUDOU SMAZÁNY! Mažete hráče ',
);

// User module
$lang['user'] = array(
    'user' => 'Uživatel',
    'group' => 'Uživatelská skupina',
    'email' => 'Email',
    'date-reg' => 'Datum registrace',
    'last-login' => 'Poslední přihlášení',
    // groups
    'all' => 'Všichni',
    'registered' => 'Registrovaní',
    'comp-managers' => 'Správci soutěží',
    'managers' => 'Správci',
    'admins' => 'Administrátoři',
    'visitors' => 'Návštěvníci',
    // info group page
    'groupinfo' => array(
        'green' => 'ZELENÁ = plný přístup',
        'blue' => 'MODRÁ = pouze čtení',
        'red' => 'ČERVENÁ = bez přístupu',
        'competitions' => 'Soutěže',
        'matches' => 'Zápasy',
        'predictions' => 'Tipování',
        'teams' => 'Týmy',
        'users' => 'Uživatelé',
    ),
    // password change
    'password' => array(
        'old' => 'Staré heslo',
        'new' => 'Nové heslo',
        'confirm' => 'Potvrzení nového hesla',
    ),
    // admin user
    'change-group' => 'Změnit uživatelskou skupinu',
    'reset-pass' => 'Resetovat heslo',
    'never-login' => 'Nikdy',
    'managed-competitions' => 'Spravované soutěže',
    'grant-competition' => 'Přidat práva k další soutěži',
    // logs
    'logs' => array(
        'filesize' => 'Velikost souboru',
        'competitions' => $lang['menu']['competitions'],
        'matches' => $lang['menu']['matches'],
        'filter' => 'Filtrování zápasů',
        'predictions' => $lang['menu']['predictions'],
        'teams' => $lang['menu']['teams'],
        'users' => $lang['menu']['users'],
        'database' => 'Databázové chyby',
    ),
    // delete
    'delete-msg' => 'Jste si jisti? Opravdu chcete smazat uživatele ',
    'delete-account' => 'Smazat účet',
    'delete-account-msg' => 'Jste si jisti? Opravdu chcete smazat svůj účet? Poté již nebude mít přístup do STM.',
    'delete-competition' => 'Odebrat soutěž',
    'delete-competition-msg' => 'Jste si jisti? Opravdu chcete uživateli odebrat soutěž ',
);

// Match module
$lang['match'] = array(
    'home' => 'Domácí tým',
    'away' => 'Hostující tým',
    'date' => 'Datum konání',
    'score' => 'Skóre',
    'score-home' => 'Skóre domácí',
    'score-away' => 'Skóre hosté',
    'back-to-serie' => 'Zpět do playoff série',
    // index - filtering
    'filter' => array(
        'header' => 'Filtrování',
        'submit' => 'Zobrazit můj výběr',
        'match-selection' => 'Výběr zápasů',
        'all-matches' => 'Všechny zápasy',
        'played-matches' => 'Odehrané zápasy',
        'upcoming-matches' => 'Neodehrané zápasy',
        'load-scores' => 'Načítat skóre',
        'load-periods' => 'Načítat periody',
        'result-table' => 'Vypočítat tabulku',
        'head-to-head' => 'Pouze vzájemné zápasy',
        'page' => 'Stránka',
    ),
    // create match
    'teams' => 'Týmy',
    'season-fields' => 'Údaje k sezóně',
    'season-round' => 'Kolo',
    // predictions - left menu
    'predictions' => 'Tipování',
    'new-prediction' => 'Tipnout výsledek',
    // periods
    'period-number' => 'Perioda',
    'periods' => 'Periody',
    'period' => array(
        'add' => 'Přidat periodu',
        'note' => 'Poznámka',
    ),
    // detail
    'detail-spectators' => 'Počet diváků',
    'detail-comment' => 'Komentář zápasu',
    'detail-stadium' => 'Stadión',
    /// referee
    'match-referee' => 'Rozhodčí',
    /// lineups
    'lineup-substitute' => 'Střídající hráč',
    /// team actions (statistics)
    'team-action' => 'Týmová statistika',
    /// player actions
    'player-action' => 'Hráčská statistika',
    'player-action-minute' => 'Kdy se akce odehrála',
    'player-action-comment' => 'Komentář akce',
    // result from action
    'calculate-score' => array(
        'existing-score' => 'Pokud zvolíte výpočet skóre, tak aktuální výsledek zápasu <strong>%s</strong> bude smazán.',
        'calculation' => 'Způsob výpočtu výsledku',
    ),
    // delete
    'delete' => 'Smazat zápas',
    'delete-msg' => 'Jste si jisti? Opravdu chcete smazat tento zápas? Mažete zápas ',
    'delete-result' => 'Smazat výsledek',
    'delete-result-msg' => 'Jste si jisti? Opravdu chcete smazat výsledek zápasu ',
    'delete-period' => 'Smazat periodu',
    'delete-period-msg' => 'Jste si jisti? Opravdu chcete smazat periodu č.',
    'delete-detail' => 'Smazat detail zápasu',
    'delete-detail-msg' => 'Jste si jisti? Opravdu chcete smazat detail zápasu ',
    'delete-referee' => 'Smazat rozhodčího',
    'delete-referee-msg' => 'Jste si jisti? Opravdu chcete smazat rozhodčího ',
    'delete-player-msg' => 'Jste si jisti? Opravdu chcete smazat hráče ',
    'delete-teamaction-msg' => 'Jste si jisti? Opravdu chcete smazat týmovou statistiku ',
    'delete-playeraction-msg' => 'Jste si jisti? Opravdu chcete smazat hráčskou statistiku ',
);

// Match action module
$lang['action'] = array(
    'name' => 'Zápasová akce',
    'stats' => 'Ve statistikách',
    'count_in_teams' => 'Týmy',
    'count_in_players' => 'Hráči',
    'delete-msg' => 'Jste si jisti? Opravdu chcete odstranit tuto zápasovou akci? Mažete akci ',
    'delete-teamaction' => 'Smazat týmové statistiky',
    'delete-teamaction-msg' => 'Jste si jisti? Opravdu chcete smazat z týmových statistik akci ',
    'delete-playeraction' => 'Smazat hráčské statistiky',
    'delete-playeraction-msg' => 'Jste si jisti? Opravdu chcete smazat z hráčských statistik akci ',
);

// Prediction module
$lang['prediction'] = array(
    'match' => 'Tipovaný zápas',
    'result' => 'Výsledek zápasu',
    'prediction' => 'Můj tip',
    'chart' => array(
        'username' => 'Uživatel',
        'count' => 'Počet tipů',
        'cheat' => 'podvody',
        'exact' => 'Přesné tipy',
        'success' => 'Úspěšnost',
        'points' => 'Body'
    ),
    'stats' => array(
        'played' => 'Počet odehraných zápasů',
        'how-many' => 'Kolikrát byla tipována ...',
        'win' => 'Výhra domácích',
        'draw' => 'Remíza',
        'loss' => 'Prohra domácích'
    ),
);

// Competition module
$lang['competition'] = array(
    // general
    'competition' => 'Soutěž',
    'season' => 'Sezóna',
    'playoff' => 'Playoff',
    'category' => 'Kategorie',
    'match' => 'Zápas',
    'team' => 'Tým',
    'serie' => 'Playoff série',
    // detail
    'sections' => array(
        'last-matches' => 'Poslední zápasy',
        'next-matches' => 'Příští zápasy',
        'detail' => 'Základní informace o soutěži',
    ),
    'name' => 'Jméno',
    'type' => 'Typ soutěže',
    'date-start' => 'Datum začátku',
    'date-end' => 'Datum ukončení',
    'min-match-periods' => 'Minimální počet period zápasu',
    'score-type' => 'Typ počítání skóre',
    'status' => 'Viditelnost soutěže',
    'periods' => 'Počet period sezóny',
    'win' => 'Body za výhru',
    'win-ot' => 'Body za výhru v prodloužení',
    'draw' => 'Body za remízu',
    'loss' => 'Body za prohru',
    'loss-ot' => 'Body za prohru v prodloužení',
    'serie-win-matches' => 'Počet vítězných zápasů série',
    // stats
    'stats' => array(
        'team-count' => 'Počet týmů',
        'all-matches' => 'Celkový počet zápasů',
        'played-matches' => 'Počet odehraných zápasů',
        'upcoming-matches' => 'Počet nadcházejících zápasů',
        'total-matches' => 'Celkem zápasů v sezóně',
        'percentage-matches' => 'Procento odehraných zápasů sezóny',
        'played-rounds' => 'Počet odehraných kol',
        'total-rounds' => 'Celkový počet kol',
        'players-counts' => 'Počet hráčů v týmech',
        'players-count' => 'Počet hráčů',
    ),
    // teams
    'teams' => array(
        'mark' => 'Označit jako Váš tým',
        'unmark' => 'Odstranit značku Vašeho týmu',
        'how-add' => 'Jak přidat tým(y) do soutěže',
    ),
    // season
    'table' => array(
        'full' => 'Kompletní tabulka',
        'home' => 'Tabulka domácí',
        'away' => 'Tabulka hosté',
        'position' => 'Poz',
        'matches' => 'Z',
        'win' => 'V',
        'winInOt' => 'VOT',
        'draw' => 'R',
        'lossInOt' => 'POT',
        'loss' => 'P',
        'goalFor' => 'VG',
        'goalAgainst' => 'OG',
        'points' => 'Body',
        'ext' => 'Extra',
        'truthPoints' => 'Body v tabulce pravdy'
    ),
    'extra' => array(
        'current' => 'Současný počet extra bodů',
        'add' => 'Přidat extra body',
        'added' => 'Přidané body'
    ),
    'schedule' => array(
        'reload' => 'Znovu načíst rozpis',
        'reload-help' => 'nic není ukládáno do databáze, použijte např. pokud se Vám nelíbí vygenerované první kolo rozpisu',
        'save' => 'Uložit rozpis do databáze',
        'start-date' => 'Datum začátku periody č.',
        'preview' => 'Náhled první periody vygenerovaného rozpisu',
        'regenerate' => '<strong>Varování</strong>: sezóna obsahuje zápasy, u aktuálních zápasů dojde ke změně kola, ale nemusí sedět pořadí zápasů v kole, protože již existující zápasy budou vždy řazeny dříve než ty nově vytvořené',
    ),
    'rounds' => array(
        'match-count' => 'Zápasy v kole',
        'load-team-dates' => 'Načíst aktuální data kol podle zápasů týmu',
    ),
    // plaoyff
    'tree' => array(
        'no-round' => 'Žádné kolo, proto nejde zobrazit playoff pavouka',
        'rounds' => 'Kola',
        'round-n' => 'Kolo č.',
        'closed' => 'UZAVŘENO',
        'actual' => 'Aktuální',
        'not-generated' => 'Nevygenerováno',
        'round-select' => 'Výběr kola',
        'display-round' => 'Zobraz série ze zvoleného kola',
        'serie-n' => 'Série z kola č.',
        'links' => 'Odkazy',
        'link-detail' => 'Detail',
        'link-addmatch' => 'Přidat zápas',
        'matches' => 'Zápasy v sérii',
    ),
    'seeded' => array(
        'current' => 'Současné nasazení',
        'new' => 'Nové nasazení',
    ),
    'round' => array(
        'save' => 'Uložit kolo do databáze',
        'generate-n' => 'Vygenerované kolo č.',
        'teams-seeded' => 'Týmy seřazené podle nasazení',
        'previous' => 'Předchozí kolo',
        'seeded' => 'Nasazení',
        'seeded-fixed' => 'Menší nasazení z předchozí série',
        'seeded-variable' => 'Nasazení vítěze z tohoto kola',
        'change-seeded' => 'Změnit způsob nasazování v generovaném kole',
    ),
    // summary
    'summary' => array(
        'type' => 'Položky k zahrnutí do souhrnu',
        'optional-settings' => 'Nepovinné nastavení',
        'schedule' => array(
            'header' => 'Rozpis zápasů',
            'fields' => 'Počet hříšť',
        ),
        'teams' => array(
            'count' => 'Počet týmů',
            'list' => 'Seznam týmů'
        ),
        'statistics' => array(
            'goals-count' => 'Počet vstřelených gólů',
            'goals-average' => 'Průměrný počet gólů v zápase',
        ),
        'winners' => array(
            'header' => 'Vítězové',
            'top-3' => 'Stupně vítězů',
            'top-players' => 'Nejlepší hráč(i) z evidovaných hráčských statistik',
        ),
        'tables' => 'Tabulky',
        'player-actions' => array(
            'top-5' => 'Top 5 nejlepších hráčů u statistiky',
            'teams' => 'Týmy se seznamem hráčům a u hráče hodnota statistiky',
            'full-list' => 'Kompletní seznam, např. tabulka střelců',
        ),
        'matches' => array(
            'header' => 'Detailní výpis zápasů',

        ),
    ),
    // delete
    'delete' => 'Smazat soutěž',
    'delete-msg' => 'Jste si jisti? Opravdu chcete smazat soutěž ',
    'delete-matches' => 'Smazat zápasy',
    'delete-matches-msg' => 'Jste si jisti? Opravdu chcete smazat všechny zápasy ze soutěže ',
    'delete-team' => 'Smazat tým ze soutěže',
    'delete-team-msg' => 'Jste si jisti? Opravdu chcete smazat tento tým ze soutěže? Mažete tým ',
    'delete-playoffround' => 'SMAZAT KOLO',
    'delete-playoffround-msg' => 'Jste si jisti? Opravdu chcete smazat aktuální kolo z tohoto playoff?',
    'delete-seasonround' => 'Smazat zápasy z kola',
    'delete-seasonround-msg' => 'Jste si jisti? Opravdu chcete všechny zápasy z kola č.',
);

// Import module
$lang['import'] = array(
    'step' => 'Krok',
    'go-to-next' => 'Jít na další krok',
    'go-to-previous' => 'Zpátky na předchozí krok',
    'save' => 'Provést import (uložit do databáze)',
    'format' => 'Formát',
    'remove-invalid' => 'Odstranit řádky nevyhovující formátu',
    'format-symbols' => 'Formátovací značky',
    'format-surrounding' => 'Obklopující text',
    'input' => 'Vstupní text',
    'summary' => 'Shrnutí importu',
    // matches
    'match-count' => 'Vytvořeno zápasů',
    'period-count' => 'Vytvořeno period',
    'errors-match' => 'Zápasy, které nebyly vytvořeny',
    // teams
    'team-count' => 'Vytvořeno týmů',
    'club-count' => 'Vytvořeno klubů',
    'errors-team' => 'Týmy a kluby, které nebyly vytvořeny',
    // table
    'table-all' => 'Importovat také tabulku domácí a hosté (jinak bude kompletní tabulka použita pro tabulku domácí a hosté)',
    // persons
    'person-count' => 'Vytvořeno osob',
    'team-member-count' => 'Vytvořeno hráčů v týmech',
);

// Statistics
$lang['statistics'] = array(
    'played-matches' => 'Odehrané zápasy',
    'played-time' => 'Odehraný čas',
    'team-overall' => 'Celkem',
    'team-home' => 'Doma',
    'team-away' => 'Venku',
    'team-difference' => 'Rozdíl',
    'team-positions' => 'Postavení týmu v tabulce',
    'team-overall-results' => 'Celková bilance zápasů',
    'records' => 'Rekordy',
    'highestAggregateScore' => 'Zápasy s nejvíce brankami',
    'highestGoalFor' => 'Nejvíce vstřelených branek v zápase',
    'highestGoalAgainst' => 'Nejvíce obdržených branek v zápase',
    'highestWin' => 'Nejvyšší výhra',
    'highestLoss' => 'Nejvyšší prohra',
    'head-to-head' => array(
        'last-matches' => 'Posledních 5 vzájemných zápasů',
    ),
);

// Export
$lang['export'] = array(
    'web' => 'Web Export (PHP kód)',
    'database' => 'Databázový export (Soubor)',
    'limit-max' => 'Maximálně zápasů',
    'limit-offset' => 'Počáteční offset',
    'order-priority' => 'Priorita řazení',
    'order-match-id' => 'Řadit podle ID zápasu',
    'order-round' => 'Řadit podle kola',
    'order-datetime' => 'Řadit podle data konání',
    'order-is-ascending' => 'Vzestupně',
);

// Error
$lang['error'] = array(
    'report' => 'Nahlásit chybu',
    'desc' => 'Popis chyby',
);

// Constants
$lang['constants'] = array(
    'usergroup' => array(
        \STM\User\UserGroup::ADMIN => 'Administrátor',
        \STM\User\UserGroup::MANAGER => 'Správce',
        \STM\User\UserGroup::COMPETITION_MANAGER => 'Správce soutěže',
        \STM\User\UserGroup::REGISTERED_USER => 'Registrovaný uživatel',
    ),
    'scoretype' => array(
        \STM\Match\ScoreType::FOOTBALL => 'Fotbalová skóre',
        \STM\Match\ScoreType::TENNIS => 'Tenisová skóre'
    ),
    'competitiontype' => array(
        \STM\Competition\CompetitionType::COMPETITION => 'Zápasy',
        \STM\Competition\CompetitionType::SEASON => 'Sezóna',
        \STM\Competition\CompetitionType::PLAYOFF => 'Playoff',
    ),
    'competitionstatus' => array(
        \STM\Competition\CompetitionStatus::PUBLICS => 'Veřejná',
        \STM\Competition\CompetitionStatus::VISIBLE => 'Viditelná',
        \STM\Competition\CompetitionStatus::HIDDEN => 'Skrytá',
    ),
    'statisticcalculationtype' => array(
        \STM\Match\Stats\Action\StatisticCalculationType::COUNT => 'Počet',
        \STM\Match\Stats\Action\StatisticCalculationType::SUM => 'Součet',
    ),
);

// Form Errors
$lang['form-error'] = array(
    'infotext' => 'Operaci nelze provést, protože se vyskytly následující chyby',
    'db-fail' => 'Chyba během zpracování v databázi, zkuste to znovu',
    'invalid-input' => 'Záznam neobsahuje všechna potřebná data',
    // formats for sprintf
    'empty-value' => '%s musí být vyplněno.',
    'no-change' => '%s - beze změny',
    'string-length' => '%s musí mít délku od %d do %d',
    'number-length' => '%s musí být číslo od %d do %d',
    'invalid-value' => '%s musí být %s',
    'invalid-date' => '%s - neplatný formát data',
    'invalid-date2' => '%s datum musí být před datumem %s',
    'invalid-website' => '%s musí být platná internetová adresa',
    'invalid-email' => '%s musí být platný email',
    'unique-value' => '%s už existuje',
    // other
    'login' => 'Nesprávná kombinace jména a hesla',
    'date-integrity' => 'Data musí být ve vzestupném pořadí',
    'compare-birth-date' => 'Data musí být mladší než narození osoby',
    'invalid-filename' => 'Jméno není platné jméno souboru. Jméno nesmí obsahovat mezeru nebo znaky / ? * : ; { } \\',
    'password-same' => 'Nové a staré heslo jsou stejné',
    'password-old' => 'Nesouhlasí staré heslo',
    'password-confirmation' => 'Nové heslo a jeho potvrzení se neshodují',
    'last-admin' => 'Nemůžete změnit uživatelskou skupinu posledního administrátora',
    'category-parent' => 'Kategorie nemůže být svým vlastním rodičem',
    'team-club' => 'Tým s tímto jménem už je součástí klubu',
    'seeded-unique' => 'Nasazení každého týmu musí být unikátní a celé číslo',
    'seeded-firstround' => 'Nasazení lze změnit pouze před vygenerováním prvního kola',
    'no-teams' => 'Žádné týmy ve zvolené soutěži',
    'point-integrity' => 'Body musí splňovat: VÝHRA >= VÝHRA V PRODLOUŽENÍ >= REMÍZA >= PROHRA V PR. >= PROHRA',
    'nothing-checked' => 'Nebyly vybrány žádné položky',
    'sport-unique' => 'Sportovní kategorie s tímto jménem už existuje pod touto kategorií',
    // MatchValidator
    'match-teams' => 'Domácí a hostující tým musí být různé týmy',
    'match-playoff' => 'V playoff nelze změnit týmy, můžete pouze prohodit týmy',
    'match-seasonround' => 'Tým můžete odehrát v kole maximálně jeden zápas',
    'match-seasonperiods' => 'Týmy se spolu mohou utkat pouze %d (počet period sezóny)',
    'match-serie' => 'Nemůžete přidat zápas do série z uzavřeného kola',
    'match-filter-maxteams' => 'Filtr může vrátit maximálně %d zápasů pro zvolenou akci (výpočet tabulky nebo výpis zápasů)',
    'match-playoff-serie-seas' => 'Série nemůže být nastavena v sezóně',
    'match-seasonround-play' => 'Kolo nemůže být nastaveno v playoff',
    'match-seasonround-comp' => 'Kolo nemůže být nastaveno v obyčejné soutěži',
    'match-playoffserie-comp' => 'Série nemůže být nastavena v obyčejné soutěži',
    'match-seasonround-set' => 'U sezóny musí být vyplněno kolo',
    // MatchPlayerValidator
    'match-substitution-team' => 'Střídající hráč musí být ze stejného týmu a zápasu',
    'match-substitution-player' => 'Střídající hráč musí být jiný než střídaný hráč',
    'match-player' => 'Osoba musí být hráčem v týmů, nejenom členem týmu.',
    // PredictionValidator
    'prediction-date' => 'Můžete tipovat výsledky pouze neodehraných zápasů',
    'prediction-competition' => 'Tipování výsledků zápasů z tété soutěže není povoleno',
    'prediction-score' => 'Nemůžete tipovat zápasy s uloženým výsledkem',
    // PlayoffRoundValidator
    'playoffround-winner' => 'Všechny série musí mít vítěze',
    'playoffround-seeded' => 'Všechny týmy musí mít nasazení',
    // Template
    'file-not-saved' => 'Soubor nebyl uložen, zkuste to znovu',
    // Import Matches
    'import-playoff' => 'Zápasy playoff nemohou být importovány',
    'import-reguired-competition' => 'Symboly pro domácí a hostující tým jsou povinné ve formátu',
    'import-reguired-season' => 'Symbol pro kolo sezóny musí být ve formátu, pokud importujete zápasy do sezóny',
    'import-reguired-person' => 'Symboly pro jmémo a příjmení jsou povinné ve formátu',
    'import-invalid-team' => 'Tým "%s" není ze zvolené soutěže',
    // Import Season Table
    'import-seasontable-teams' => '%s - všechny týmy musí být ze zvolené sezóny',
    'import-seasontable-numbers' => '%s - všechny hodnoty musí být čísla od 0 do 10000',
    'import-seasontable-structure' => '%s - u některého týmu nejsou vyplněny všechny povinné hodnoty',
    // Import Persons
    'import-persons-team' => 'Neexistující tým',
    'import-persons-position' => 'Neexistující pozice',
);

// Session message
$lang['session'] = array(
    // index
    'login' => 'Úspěšné přihlášení. Vítejte %s!',
    'registration' => 'Úspěšná registrace<br />Jméno: <strong>%s</strong>, heslo: <strong>%s</strong>',
    'create-user' => 'Uživatel %s vytvořen s heslem <strong>%s</strong>',
    'visitor-access' => 'Návštěvníci nemají povolen přístup na tuto stránku',
    // general
    'change' => '%s <em>%s</em> - úspěšná úprava',
    'change-fail' => '%s <em>%s</em> - neúspěšná úprava',
    'create' => '%s <em>%s</em> - úspěšné vytvoření',
    'create-fail' => '%s <em>%s</em> - neúspěšné vytvoření',
    'delete-success' => '%s <em>%s</em> - úspěšné smazání',
    'delete-fail' => '%s <em>%s</em> - nesmazáno, zkuste to znovu',
    'invalid' => '%s - zvolena neexistující položka',
    // page exceptions
    'page-module' => 'Neexistující modul',
    'page-action' => 'Neplatný typ akce (povoleno pouze create, read, update a delete)',
    'page-type' => 'Nepovolená akce v modulu',
    'allow-prediction' => 'Tipování zápasů není povoleno',
    'only-logged-users' => 'Pouze přihlášení uživatelé mají povolen přístup do tohoto modulu',
    // other
    'authorization' => 'Nemáte povolený přístup na tuto stránku',
    'delete-csrf' => 'Nesmazáno, CSRF útok',
    'delete-input' => 'Nesmazáno, špatné vstupní argumenty',
    'undefined-error' => 'Nedefinovaná chyba, zkuste to znovu',
    'attribute-delete' => 'Atribut nejde smazat, pokud ho používá alespoň jedna osoba',
    'attribute-usage' => 'Žádná osoba nemá přiřazen tento atribut',
    'sport-delete' => 'Sportovní kategorii nejde smazat, pokud je k ní přiřazena alespoň jedna pozice nebo zápasová akce',
    'position-delete' => 'Pozici nejde smazat, pokud ji používá alespoň jedna osoba',
    'action-delete' => 'Zápasovou akci nejde smazat, pokud je použita alespoň jednou v hráčské nebo týmové statistice',
    'action-players-delete' => 'Zápasová akce se nepoužívá v hráčských statistikách',
    'action-teams-delete' => 'Zápasová akce se nepoužívá v týmových statistikách',
    // members and clubs/teams
    'invalid-member' => 'Neexistující člen',
    'team-add-member' => 'Do týmu bylo úspěšně přidáno %d osob z %d vybraných osob',
    'club-add-member' => 'Do klubu bylo úspěšně přidáno %d osob z %d vybraných osob',
    'delete-club' => 'Klub nejde smazat, pokud něj patří týmy nebo členové',
    'delete-person' => 'Osobu nejde smazat, pokud se nachází v týmech/klubech nebo mezi rozhodčími',
    // match
    'match-create' => 'Soutěž musí obsahovat alespoň 2 týmy, aby mohl být přidán zápas do soutěže',
    'match-competition' => 'Musíte zvolit jednu soutěž, pokud chcete vytvořit nový zápas',
    'match-round' => 'Kolo jde změnit jen u zápasů sezóny',
    'match-serie' => 'Mazání není povoleno u zápasů z uzavřených sérií',
    'match-result' => 'Zápas nemá výsledek',
    'match-create-result' => 'Úspěšné přidání výsledku zápasu (počet přidaných period: %d)',
    'match-update-result' => 'Úspěšná změna výsledku zápasu (počet upravených period: %d)',
    /// detail
    'match-detail-notset' => 'Detaily zápasu nejsou vyplněny',
    /// referee
    'match-referee-add' => 'Úspěšné přidání nového rozhodčího',
    'match-referee-invalid' => 'Neexistující rozhodčí',
    /// players
    'match-player-create' => 'Bylo přidáno %d hráčů z %d vybraných osob',
    'match-player-action-create' => 'Bylo přidáno %d zápasových akcí ze %d zadaných akcí',
    // prediction
    'prediction-existing' => 'Tento zápas už jste tipovali. Nemůžete vytvořit nový tip, ale můžete změnit skóre stávajícího tipu',
    'prediction-match' => 'Musíte zvolit existující zápas, pokud chcete vytvořit nový tip nebo prohlédnout statistiky zápasu',
    'prediction-competition' => 'Musíte zvolit existující soutěž, pokud chcete vidět statistiky soutěže',
    // team
    'create-player' => 'Osoba už je hráčem týmu',
    'delete-team' => 'Tým nejde smazat, pokud je součástí soutěže nebo má pod sebou nějaké členy',
    // user
    'user-grant' => 'Přidat soutěž lze jen správci soutěží',
    'user-revoke' => 'Odebrat soutěž lze jen správci soutěží',
    'user-add-competition' => 'Soutěž byla úspěšně přiřazena uživateli',
    'user-reset' => 'Úspěšný reset uživatele %s, nové heslo = <strong>%s</strong>',
    'user-logout' => 'Byli jste úspěšně odhlášeni',
    'user-change-password' => 'Úspěšná změna hesla, přihlaste se s novým heslem',
    'user-delete-account' => 'Váš uživatelský účet byl smazán',
    // competition
    'competition-playoffround' => 'Akce je povolená pouze, pokud není vygenerované první kolo playoff',
    'competition-playoffmatches' => 'Smazání všech zápasů není povolené v playoff',
    'competition-addteams' => 'Tým(y) byl(y) úspěšně vložen(y) do soutěže',
    'delete-competition' => 'Soutěž nejde smazat, pokud obsahuje zápasy',
    'delete-competitionmatches' => 'Soutěž neobsahuje žádné zápasy',
    'competition-team' => 'Neplatný tým',
    'competition-import-players' => "Do %d zápasů bylo celkem přidáno %d hráčů. %d hráčů už bylo předtím v sestavě",
    'competition-import-players-fail' => "Do %d zápasů se nepodařilo přidat %d hráčů. %d hráčů už bylo předtím v sestavě",
    'competition-round-date-update' => 'Bylo změněno datum v %d kolech z %d vybraných kol',
    // season
    'season-teamcount' => 'Sezóna musí obsahovat <2, %d> týmů, pokud chcete vygenerovat rozpis',
    'season-generated-schedule' => 'Rozpis sezóny byl vygenerován',
    'season-full-table-sort-success' => 'Týmy se stejným počtem bodů v celkové tabulce byly seřazeny podle vzájemných zápasů',
    'season-full-table-sort-fail' => 'Týmy se stejným počtem bodů v celkové tabulce se nepodačilo seřadit podle vzájemných zápasů, zkuste to znovu',
    // playoff
    'playoff-teamcount' => 'Neplatný počet týmů - počet musí být mocninou 2 a menší než 128, pokud chcete vygenerovat kolo playoff',
    'playoff-generated' => 'Playoff už je kompletně vygenerované',
);
