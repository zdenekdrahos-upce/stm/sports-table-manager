<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\User;

use STM\Utils\Classes;

/**
 * UserGroup class
 * - represents database table 'user_groups'
 * - used for simpler work with primary key (A, M, C, R) and name
 *   of the group (Admin, Manager, Competition Manager)
 */
final class UserGroup
{
    const ADMIN = 'Admin';
    const MANAGER = 'Manager';
    const COMPETITION_MANAGER = 'Competition Manager';
    const REGISTERED_USER = 'Registered User';
    const VISITOR = 'Visitor';

    private function __construct()
    {
    }

    /**
     * Returns ID group, if $constant is not valid, then returns the lowest
     * user level - H (Helper)
     * @param UserGroup $constant
     * @return string/false
     * Returns ID_GROUP (primary key in table 'user_groups') or false
     */
    public static function getIdGroup($constant)
    {
        if (Classes::isClassConstant('\STM\User\UserGroup', $constant)) {
            switch ($constant) {
                case self::ADMIN:
                    return 'A';
                case self::MANAGER:
                    return 'M';
                case self::COMPETITION_MANAGER:
                    return 'C';
                case self::REGISTERED_USER:
                    return 'R';
                case self::VISITOR:
                    return 'V';
            }
        }
        return false;
    }

    /**
     * Finds name of the group by id group
     * @param string $id_group
     * @return string
     */
    public static function getGroupname($id_group)
    {
        if (is_string($id_group)) {
            switch ($id_group) {
                case 'A':
                    return self::ADMIN;
                case 'M':
                    return self::MANAGER;
                case 'C':
                    return self::COMPETITION_MANAGER;
                case 'R':
                    return self::REGISTERED_USER;
            }
        }
        return 'Unexisting group';
    }
}
