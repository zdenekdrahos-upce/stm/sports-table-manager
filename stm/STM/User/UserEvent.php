<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\User;

use STM\Helpers\Event;

/**
 * UserEvent class
 * - for saving events related with User
 */
class UserEvent
{
    /** Creating new user */
    const CREATE = 'Create User';
    /** Updating user - update email, password, user group, reset password */
    const UPDATE = 'Update User';
    /** Deleting user */
    const DELETE = 'Delete User';
    /** User login into application */
    const LOGIN = 'Login User';
    /** User logout from application */
    const LOGOUT = 'Logout User';
    /** User registration */
    const REGISTER = 'Registration';
    /** CompetitionManager & competitions */
    const COMPETITION_MANAGER = 'Competition Manager';
    /** Delete Logs */
    const DELETE_LOGS = 'Delete Logs';

    private function __construct()
    {
    }

    /**
     * Saves events related with User
     * @param UserEvent $userEvent constant from UserEvent class
     * @param string $message description of event
     * @param boolean $result if event was sucessful or fail
     * @return boolean
     * Returns true if event was successfuly saved, otherwise false (bad arguments
     * or error during file saving)
     */
    public static function log($userEvent, $message, $result)
    {
        $event = new Event('\STM\User\UserEvent', 'users');
        $event->setMessage($userEvent, $message, $result);
        return $event->logEvent();
    }
}
