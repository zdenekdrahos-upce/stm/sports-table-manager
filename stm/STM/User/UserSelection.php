<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\User;

use STM\Utils\Classes;
use STM\DB\Query\QueryFactory;
use STM\Entities\AbstractEntitySelection;

final class UserSelection extends AbstractEntitySelection
{

    public function setId($idUser)
    {
        if (is_numeric($idUser)) {
            $this->selection->setAttributeWithValue('id_user', (string) $idUser);
        }
    }

    public function setName($username)
    {
        if (is_string($username)) {
            $this->selection->setAttributeWithValue('name', $username, 'like binary');
        }
    }

    public function setUserGroup($userGroup)
    {
        if (Classes::isClassConstant('\STM\User\UserGroup', $userGroup)) {
            $this->selection->setAttributeWithValue('id_user_group', UserGroup::getIdGroup($userGroup));
        }
    }

    public function setNeverLoggedUsers()
    {
        $this->selection->setNullAttribute('last_login', true);
    }

    protected function getQuery()
    {
        $query = QueryFactory::selectAll(DT_USERS);
        $query->setOrderBy('id_user_group asc, name asc');
        return $query;
    }
}
