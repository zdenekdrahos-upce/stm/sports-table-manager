<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\User;

use STM\Competition\Competition;
use STM\DB\SQL\SelectQuery;
use STM\DB\SQL\DeleteQuery;
use STM\DB\SQL\InsertQuery;
use STM\DB\SQL\Condition;

class UserSQL
{

    /** @return InsertQuery */
    public static function selectCanUserManageCompetition(User $user, Competition $competition)
    {
        $query = new SelectQuery(DT_COMPETITIONS_USERS);
        $query->setColumns('1');
        $query->setWhere(self::getUserCompetitionCondition($user, $competition));
        return $query;
    }

    /** @return InsertQuery */
    public static function addCompetitionManagedByUser(User $user, Competition $competition)
    {
        $query = new InsertQuery(DT_COMPETITIONS_USERS);
        $query->addAttributes(
            array(
                'id_user' => $user->getId(),
                'id_competition' => $competition->getId(),
            )
        );
        return $query;
    }

    /** @return DeleteQuery */
    public static function deleteCompetitionManagedByUser(User $user, Competition $competition)
    {
        $delete = new DeleteQuery(DT_COMPETITIONS_USERS);
        $delete->setWhere(self::getUserCompetitionCondition($user, $competition));
        return $delete;
    }

    private static function getUserCompetitionCondition($user, $competition)
    {
        return new Condition(
            'id_user = {U} AND id_competition = {C}',
            array('U' => $user->getId(), 'C' => $competition->getId())
        );
    }
}
