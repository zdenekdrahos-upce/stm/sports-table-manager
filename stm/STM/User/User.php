<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\User;

use STM\DB\Object\DatabaseObject;
use STM\StmDatetime;
use STM\Competition\Competition;
use STM\Utils\Arrays;
use STM\Utils\Dates;
use STM\Utils\Generators;

/**
 * User class
 * - instance: represents row from database table 'users'
 * - static methods: for finding information about all users in table, creating, ...
 */
final class User
{
    /** @var DatabaseObject */
    private static $db;

    /** @var int */
    private $id_user;
    /** @var string */
    private $name;
    /** @var string */
    private $password;
    /** @var string */
    private $email;
    /** @var string */
    private $ip_address;
    /** @var mysql datetime */
    private $date_registration;
    /** @var mysql datetime */
    private $last_login;
    /** @var char */
    private $id_user_group;

    private function __construct()
    {
    }

    public static function setORM(DatabaseObject $dbObject)
    {
        if (is_null(self::$db)) {
            self::$db = $dbObject;
        }
    }

    /**
     * Authentication: checks if user with $username exists and if password
     * matches password in database
     * @param string $username
     * @param string $password
     * @return User/false
     */
    public static function authenticateUser($username, $password)
    {
        $factory = new UserFactory();
        $user = $factory->findByName($username);
        if ($user instanceof User && $user->verifyPassword($password)) {
            $user->updateLastLogin();
            return $user;
        }
        return false;
    }

    /**
     * Creates new user in database table 'users'.
     * @param array $attributes - contains key 'name' and 'id_user_group'
     * @return string/false
     * Returns generated password if creating was successful, otherwise returns false
     */
    public static function create($attributes)
    {
        if (UserValidator::checkCreate($attributes)) {
            $password = Generators::generatePassword();
            if (UserValidator::checkPasssword($password)) {
                $attributes['password'] = Generators::generateHash($password, $attributes['name']);
                $attributes['ip_address'] = $_SERVER['REMOTE_ADDR'];
                if (STM_DB_TRIGGERS === false) {
                    $attributes['date_registration'] = Dates::stringToDatabaseDate('now');
                }
                if (self::$db->insertValues($attributes)) {
                    return $password;
                }
            }
        }
        return false;
    }

    /**
     * Checks if username exists as user in database. Method is case-INsensitive,
     * because it is used before creating and in 'users' table in database is name
     * unique. If you want case-sensitive search then use findByName()
     * @param string $username
     * @return boolean
     */
    public static function exists($username)
    {
        return self::$db->existsItem('name', $username);
    }


    // INSTANCE METHODS FOR ONE USER
    /**
     * Deletes user from database if user is not last admin
     * @return boolean
     */
    public function delete()
    {
        if (!$this->isLastAdmin()) {
            return self::$db->deleteById($this->id_user);
        }
        return false;
    }

    /**
     * Finds if user is last admin. It is used because at least one admin must be in application
     * @return boolean
     */
    public function isLastAdmin()
    {
        $userFactory = new UserFactory();
        $userFactory->setCount();
        $numberOfAdmins = $userFactory->findByUserGroup(UserGroup::ADMIN);
        return $this->id_user_group == 'A' && $numberOfAdmins == 1;
    }

    /**
     * Updates user in database
     * @param array $attributes
     * @return boolean
     */
    private function update($method, $new_value, $attributes)
    {
        UserValidator::setUser($this);
        if (UserValidator::$method($new_value)) {
            return self::$db->updateById($this->id_user, $attributes);
        }
        return false;
    }

    /**
     * Changes user group
     * @param string $new_id_group valid = A, H
     * @return boolean
     */
    public function updateUserGroup($new_id_group)
    {
        return $this->update('checkUserGroup', $new_id_group, array('id_user_group' => $new_id_group));
    }

    /**
     * Changes email if $new_email is valid email address
     * @param string $new_email
     * @return boolean
     */
    public function updateEmail($new_email)
    {
        return $this->update('checkEmail', $new_email, array('email' => $new_email));
    }

    /**
     * Updates date of user last login. If it's user first login to STM then it
     * also updates IP address
     * @return boolean
     */
    private function updateLastLogin()
    {
        $attributes = array('last_login' => Dates::stringToDatabaseDate('now'));
        if (is_null($this->last_login)) {
            $attributes['ip_address'] = $_SERVER['REMOTE_ADDR'];
        };
        return self::$db->updateById($this->id_user, $attributes);
    }

    /**
     * @return string/false
     * Returns new generated password if reset was successful, otherwise returns false
     */
    public function resetPassword()
    {
        $new_password = Generators::generatePassword();
        $update = $this->update(
            'checkPasssword',
            $new_password,
            array('password' => Generators::generateHash($new_password, $this->name))
        );
        return $update ? $new_password : false;
    }

    /**
     * Changes password if 'new' is valid and 'old' matches current password
     * @param array $old_password - keys: new, old
     * @return boolean
     * Returns true if change was successful
     */
    public function updatePassword($passwords)
    {
        if (Arrays::isArrayValid($passwords, array('new', 'old'))) {
            return $this->update(
                'checkNewPassword',
                $passwords,
                array('password' => Generators::generateHash($passwords['new'], $this->name))
            );
        }
        return false;
    }

    /**
     * @param string $password its not hash
     * @return boolean
     * Returns true if $password matches current user password, otherwise false.
     */
    public function verifyPassword($password)
    {
        if (is_string($password)) {
            return $this->password == Generators::generateHash($password, $this->name);
        }
        return false;
    }


    // COMPETITION FOR COMPETITION MANAGER
    /**
     * Add competition for CompetitionManager user
     * @param Competition $competition
     * @return boolean
     * Returns true if user is CompetitionManager and he is not already managing
     * competition and competition was succesfully added in DB.
     */
    public function addManagedCompetition($competition)
    {
        UserValidator::setUser($this);
        if (UserValidator::checkGrantCompetition($competition)) {
            return self::$db->query(UserSQL::addCompetitionManagedByUser($this, $competition));
        }
        return false;
    }

    /**
     * @param Competition $competition
     * @return true
     * Returns true if user is CompetitionManager and deleting was successful
     */
    public function deleteManagedCompetition($competition)
    {
        if ($this->canUpdateCompetition($competition)) {
            return self::$db->query(
                UserSQL::deleteCompetitionManagedByUser($this, $competition)
            );
        }
        return false;
    }

    /**
     * Checks if user is CompetitionManager and can update selected competition
     * @param Competition $competition
     * @return boolean
     */
    public function canUpdateCompetition($competition)
    {
        if ($this->id_user_group == 'C' && $competition instanceof Competition) {
            $query = UserSQL::selectCanUserManageCompetition($this, $competition);
            return is_array(self::$db->select($query, 'array', true));
        }
        return false;
    }


    // GETTERS
    /**
     * @return int
     */
    public function getId()
    {
        return (int) $this->id_user;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return UserGroup constant - e.g. Admin, not A
     */
    public function getUserGroup()
    {
        return UserGroup::getGroupname($this->id_user_group);
    }

    /** @return StmDatetime */
    public function getDateRegistration()
    {
        return new StmDatetime($this->date_registration);
    }

    /** @return StmDatetime */
    public function getDateLastLogin()
    {
        return new StmDatetime($this->last_login);
    }

    /**
     * Method for templating. Returns user as an array
     * @return array
     */
    public function toArray()
    {
        return array(
            'name' => $this->name,
            'email' => $this->email,
            'user_group' => $this->getUserGroup()
        );
    }

    /**
     * @return string
     * Returns name of the user and his user group
     */
    public function __toString()
    {
        return $this->name . ' (' . $this->getUserGroup() . ')';
    }
}
