<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\User;

use STM\Helpers\ObjectValidator;
use STM\Web\Message\FormError;
use STM\Web\Message\SessionMessage;
use STM\Competition\Competition;

/**
 * Class for validating attributes for database table 'USERS'
 * - attributes = name, id_user_group, password
 */
final class UserValidator
{
    /** @var ObjectValidator */
    private static $validator;

    /**
     * @param \STM\Libs\FormProcessor $formProcessor
     */
    public static function init($formProcessor = false)
    {
        if (is_null(self::$validator)) {
            self::$validator = new ObjectValidator('\STM\User\User');
        }
        self::$validator->setFormProcessor($formProcessor);
    }

    /**
     * Sets instance of the User class. Instance is used for checking if
     * new value of attribute is different from current value in user.
     * @param User $user
     */
    public static function setUser($user)
    {
        self::$validator->setComparedObject($user);
    }

    /**
     * Removes user, so attributes are not compared with competition attributes
     */
    public static function resetUser()
    {
        self::$validator->resetComparedObject();
    }

    /**
     * Checks if array $attributes can be used as values for new user in database.
     * @param array $attributes keys: name, id_user_group
     * @return boolean
     */
    public static function checkCreate($attributes)
    {
        if (self::$validator->checkCreate($attributes, array('name', 'id_user_group'))) {
            self::checkName($attributes['name']);
            self::checkUserGroup($attributes['id_user_group']);
        }
        return self::$validator->isValid();
    }

    /**
     * Checks if $id_score_type is A or H and different from current user_group
     * @param string $id_user_group
     * @return boolean
     * Returns true if $id_user_group can be written into the database
     */
    public static function checkUserGroup($id_user_group)
    {
        self::$validator->getFormProcessor()->checkCondition(
            in_array($id_user_group, array('A', 'M', 'C', 'R')),
            'Invalid User group.'
        );
        self::$validator->getFormProcessor()->checkCondition(
            $id_user_group != UserGroup::getIdGroup(self::$validator->getValueFromCompared('getUserGroup')),
            FormError::get('no-change', array(stmLang('user', 'group')))
        );
        self::$validator->getFormProcessor()->checkCondition(
            !self::$validator->getValueFromCompared('isLastAdmin'),
            FormError::get('last-admin')
        );
        return self::$validator->isValid();
    }

    /**
     * Checks if $new_name is filled and string and length is from interval <2, 30>
     * @param string $new_name
     * @return boolean
     */
    public static function checkName($new_name)
    {
        return self::$validator->checkUniqueName($new_name, array('min' => 2, 'max' => 30), 'getUsername');
    }

    /**
     * Checks if password is string and if it is in range
     * Used in creating user and for reseting user password
     * @param string $password
     * @param string $name name of the field for error message
     * @return type
     */
    public static function checkPasssword($password, $name = 'Password')
    {
        return self::$validator->checkString($password, array('min' => 5, 'max' => 50), $name);
    }

    /**
     * Checks if $new_password can be used as new password and if $old_password matches current user password.
     * Used for changing user password
     * @param array $passwords - keys: new, old
     * @return boolean
     */
    public static function checkNewPassword($passwords)
    {
        if (self::$validator->checkUpdate($passwords, array('new', 'old'))) {
            self::checkPasssword($passwords['new'], stmLang('user', 'password', 'new'));
            self::checkPasssword($passwords['old'], stmLang('user', 'password', 'old'));
            if (false || self::$validator->isValid()) {
                self::$validator->getFormProcessor()->checkCondition(
                    $passwords['new'] != $passwords['old'],
                    FormError::get('password-same')
                );
                self::$validator->getFormProcessor()->checkCondition(
                    self::$validator->getComparedObject()->verifyPassword($passwords['old']),
                    FormError::get('password-old')
                );
            }
        }
        return self::$validator->isValid();
    }

    /**
     * Checks if $email is valid email address and string <4,60>
     * @param string $email
     * @return boolean
     */
    public static function checkEmail($email)
    {
        self::$validator->checkString(
            $email,
            array('min' => 4, 'max' => 60),
            stmLang('user', 'email'),
            self::$validator->getValueFromCompared('getEmail')
        );
        self::$validator->getFormProcessor()->checkEmail(
            $email,
            FormError::get('invalid-email', array(stmLang('user', 'email')))
        );
        return self::$validator->isValid();
    }

    public static function checkGrantCompetition($competition)
    {
        self::$validator->getFormProcessor()->checkCondition(
            self::$validator->isSetComparedObject(),
            'Updated user must be set'
        );
        if (self::$validator->isValid()) {
            $user = self::$validator->getComparedObject();
            self::$validator->getFormProcessor()->checkCondition(
                $competition instanceof Competition,
                'Invalid competition'
            );
            self::$validator->getFormProcessor()->checkCondition(
                $user->getUserGroup() == UserGroup::COMPETITION_MANAGER,
                SessionMessage::get('user-grant')
            );
            if (self::$validator->isValid()) {
                self::$validator->getFormProcessor()->checkCondition(
                    $user->canUpdateCompetition($competition) == false,
                    'User already manages this competition.'
                );
            }
        }
        return self::$validator->isValid();
    }
}
