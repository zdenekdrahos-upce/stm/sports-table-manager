<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\User;

use STM\Entities\AbstractEntityFactory;

class UserFactory extends AbstractEntityFactory
{
    public function findById($id)
    {
        return $this->entityHelper->findById($id);
    }

    public function findByName($username)
    {
        $methods = array('setName' => $username);
        return $this->entityHelper->setAndFindObject($methods);
    }

    public function findByUserGroup($userGroup)
    {
        $methods = array('setUserGroup' => $userGroup);
        return $this->entityHelper->setAndFindArray($methods);
    }

    public function findNeverLoggedUsers()
    {
        $methods = array('setNeverLoggedUsers' => null);
        return $this->entityHelper->setAndFindArray($methods);
    }

    public function findAll()
    {
        return $this->entityHelper->findAll();
    }

    protected function getEntitySelection()
    {
         return new UserSelection();
    }

    protected function getEntityClass()
    {
        return '\STM\User\User';
    }
}
