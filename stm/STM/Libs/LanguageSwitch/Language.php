<?php
/*
 * This file is part of the Sports Table Manager (https://bitbucket.org/zdenekdrahos/sports-table-manager)
 * Copyright (c) 2012, 2013 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * Sports Table Manager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace STM\Libs\LanguageSwitch;

class Language
{
    /** @var \STM\Lib\LanguageSwitch\Language */
    private static $instance;

    /** @return \STM\Lib\LanguageSwitch\LanguageSwitch */
    public static function getLanguageSwitch()
    {
        return self::$instance->languageSwitch;
    }

    /** @return string */
    public static function getSelectedLanguage()
    {
        return self::$instance->languageSwitch->getNameOfSelectedLanguage();
    }

    public static function init($langRoot = STM_LANG_ROOT)
    {
        if (is_null(self::$instance)) {
            self::$instance = new self($langRoot);
        }
    }

    /** @var \STM\Lib\LanguageSwitch\LanguageSwitch */
    private $languageSwitch;

    private function __construct($langRoot)
    {
        $this->languageSwitch = new LanguageSwitch($langRoot, STM_COOKIE_NAME);
        LanguageHelper::redirectToCleanURL();
        Translator::init($this->languageSwitch);
    }
}
