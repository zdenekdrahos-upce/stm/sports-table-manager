<?php
/**
 * PHP Language-Switch class - simple PHP class to switch languages on website
 * Copyright (C) 2011,2012  Zdenek Drahos (https://bitbucket.org/zdenekdrahos/php-language-switch-class)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace STM\Libs\LanguageSwitch;

final class LanguageSwitch
{
    private $langRoot = STM_LANG_ROOT;
    private $cookieName = STM_COOKIE_NAME;
    private $selectedLang = STM_DEFAULT_LANGUAGE;

    /** Initialize selected language. Sets cookie only if user use GET parameter */
    public function __construct($langRoot = STM_LANG_ROOT, $cookieName = STM_COOKIE_NAME)
    {
        $this->setLanguageRoot($langRoot);
        $this->setCookieName($cookieName);
        if (isset($_GET['language']) && $this->isLanguageSupported($_GET['language'])) {
            $this->selectedLang = $_GET['language'];
            $this->setLanguageCookie($this->selectedLang);
        } elseif (isset($_COOKIE[$this->cookieName]) && $this->isLanguageSupported($_COOKIE[$this->cookieName])) {
            $this->selectedLang = $_COOKIE[$this->cookieName];
        } else {
            $this->checkDefaultLanguage();
            $this->selectedLang = STM_DEFAULT_LANGUAGE;
        }
    }

    public function getNameOfSelectedLanguage()
    {
        return $this->selectedLang;
    }

    public function getTranslationsArray()
    {
        include($this->langRoot . $this->selectedLang . STM_LANG_EXT);
        if (isset($lang) && is_array($lang)) {
            return $lang;
        } else {
            throw new Exception("{$this->selectedLang} is invalid language file");
        }
    }

    /** @return array returns array of available languages in alphabetic order */
    public function getAvailableLanguages()
    {
        $languages = array();
        foreach (scandir($this->langRoot) as $file) {
            $name = substr($file, 0, -strlen(STM_LANG_EXT));
            if ($this->isLanguageSupported($name)) {
                $languages[] = $name;
            }
        }
        return $languages;
    }

    /**
     * Sets language (cookie) for page $_SERVER['PHP_SELF'].
     * Redirects to the current page after setting cookie.
     * @param string $language
     */
    public function setLanguageForCurrentPage($language)
    {
        if ($this->isLanguageSupported($language) && $this->selectedLang != $language) {
            $this->setLanguageCookie($language, $_SERVER['PHP_SELF']);
            header('Location: ' . $_SERVER['REQUEST_URI']);
            exit;
        }
    }

    private function setLanguageRoot($langRoot)
    {
        $this->langRoot = is_string($langRoot) && is_dir($langRoot) ? $langRoot : STM_LANG_ROOT;
    }

    private function setCookieName($cookieName)
    {
        $this->cookieName = is_string($cookieName) && $cookieName != '' ? $cookieName : STM_COOKIE_NAME;
    }

    private function setLanguageCookie($language, $path = '/')
    {
        setcookie($this->cookieName, $language, STM_COOKIE_EXPIRE, $path);
    }

    private function checkDefaultLanguage()
    {
        if (!$this->isLanguageSupported(STM_DEFAULT_LANGUAGE)) {
            throw new Exception('Language file was not found!');
        }
    }

    private function isLanguageSupported($language)
    {
        return file_exists($this->langRoot . $language . STM_LANG_EXT);
    }
}
