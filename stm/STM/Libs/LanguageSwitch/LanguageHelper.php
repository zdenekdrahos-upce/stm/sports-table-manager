<?php
/**
 * PHP Language-Switch class - simple PHP class to switch languages on website
 * Copyright (C) 2011,2012  Zdenek Drahos (https://bitbucket.org/zdenekdrahos/php-language-switch-class)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace STM\Libs\LanguageSwitch;

final class LanguageHelper
{
    public static function redirectToCleanURL()
    {
        if (isset($_GET['language'])) {
            unset($_GET['language']);
            header('Location: ' . self::getHrefForLink(null));
            exit;
        }
    }

    public static function printLinksOfAvailableLanguages()
    {
        $languageSwitch = Language::getLanguageSwitch();
        if ($languageSwitch instanceof LanguageSwitch) {
            $langs = $languageSwitch->getAvailableLanguages();
            echo '<ul class="language-chooser">';
            foreach ($langs as $language) {
                $image = '<img src="web/images/flags/' . $language . '.png" title="';
                $image .= stmLang('lang-titles', $language) . '" />';
                if ($language == $languageSwitch->getNameOfSelectedLanguage()) {
                    echo '<li>' . $image . '</li>';
                } else {
                    echo '<li><a href="' . self::getHrefForLink($language) . '">' . $image . '</a></li>';
                }
            }
            echo '</ul>';
        }
    }

    private static function getHrefForLink($language)
    {
        return $_SERVER['PHP_SELF'] . '?' . http_build_query($_GET + array('language' => $language));
    }
}
