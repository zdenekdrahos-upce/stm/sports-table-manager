<?php
/**
 * PHP Language-Switch class - simple PHP class to switch languages on website
 * Copyright (C) 2011,2012  Zdenek Drahos (https://bitbucket.org/zdenekdrahos/php-language-switch-class)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace STM\Libs\LanguageSwitch;

final class Translator
{
    private static $translations;

    public static function init(LanguageSwitch $languageSwitch)
    {
        self::$translations = $languageSwitch->getTranslationsArray();
    }

    public static function get()
    {
        if (self::checkArguments(func_num_args())) {
            return self::getTranslation(func_get_args());
        }
        return '';
    }

    public static function getFormattedString()
    {
        if (self::checkArguments(func_num_args())) {
            return call_user_func_array('sprintf', self::getArrayForSprintf(func_get_args()));
        }
        return '';
    }

    private static function checkArguments($functionArgumentsCount)
    {
        if ($functionArgumentsCount == 0) {
            return false;
        } elseif (is_null(self::$translations)) {
            throw new Exception('Translator was not initialized');
        }
        return true;
    }

    private static function getArrayForSprintf($functionArguments)
    {
        $args = array_pop($functionArguments);
        $sprintf_array = array();
        $sprintf_array[] = self::getTranslation($functionArguments);
        if (is_array($args)) {
            foreach ($args as $arg) {
                $sprintf_array[] = $arg;
            }
        }
        return $sprintf_array;
    }

    private static function getTranslation($functionArguments)
    {
        $currentArguments = $functionArguments;
        $translation = self::$translations;
        while (count($currentArguments)) {
            $key = array_shift($currentArguments);
            if (is_string($key) && array_key_exists($key, $translation)) {
                $translation = $translation[$key];
            } else {
                return implode(' ', $functionArguments);
            }
        }
        return $translation;
    }
}
