<?php
/*
 * This file is part of the PHP File Cache (https://bitbucket.org/zdenekdrahos/php-file-cache)
 * Copyright (c) 2012, 2013 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * PHP File Cache is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace STM\Libs\FileCache;

class Cache
{
    /** @var \STM\Libs\FileCache\ISource */
    private $source;
    /** @var \STM\Libs\FileCache\IFile */
    private $file;

    public function __construct(ISource $source, IFile $file)
    {
        $this->source = $source;
        $this->file = $file;
    }

    public function display()
    {
        $this->checkCache();
        return $this->file->printContent();
    }

    private function checkCache()
    {
        if ($this->isCacheExpired()) {
            $this->reloadCache();
        }
    }

    private function isCacheExpired()
    {
        return $this->source->getEffectiveDate() > $this->file->getLastModificationDate();
    }

    private function reloadCache()
    {
        $this->source->reloadContent();
        $cache = $this->source->getContent();
        $this->file->writeContent($cache);
    }
}
