<?php
/*
 * This file is part of the PHP File Cache (https://bitbucket.org/zdenekdrahos/php-file-cache)
 * Copyright (c) 2012, 2013 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * PHP File Cache is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace STM\Libs\FileCache;

interface ISource
{
    public function getEffectiveDate();

    public function getContent();

    public function reloadContent();
}
