<?php

/**
 * PHP Logger class - simple PHP class to log events
 * Copyright (C) 2011  Zdenek Drahos (https://bitbucket.org/zdenekdrahos/php-logger-class)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

namespace STM\Libs;

class Logger
{
    /**
     * Method to save event to the log file. Location of logfile is determined
     * from constants which are defined in the file logger.php.<br />
     * @param mixed $event - any instance of class which has implemented __toString() method
     * @param string $logname
     * @return boolean
     * Returns true if message was successfully saved. False if: log's folder
     * doesn't exist or missing file permissions to log file.
     */
    public static function logEvent($event, $logname = STM_DEFAULT_LOG_NAME)
    {
        if (self::isEvent($event) && self::isLogWritable($logname)) {
            $file = fopen(STM_LOG_ROOT . $logname . STM_EXT, 'a');
            if ($file) {
                fwrite($file, $event);
                fclose($file);
                return true;
            }
        }
        return false;
    }

    /**
     * Converts csv file into 2D array.
     * @param string $logname
     * @return array
     * Returns 2-dimensional array, if file from argument is readable (exists)
     * Example:
     *  $array[0] = array(21, 11, 1989, 'Login', 'Zdenek Drahos');
     *  $array[0][1] = 11;
     */
    public static function getLogContent($logname = STM_DEFAULT_LOG_NAME)
    {
        if (is_string($logname) && is_readable(STM_LOG_ROOT . $logname . STM_EXT)) {
            $array = file(STM_LOG_ROOT . $logname . STM_EXT);
            for ($index = 0; $index < count($array); $index++) {
                $array[$index] = explode(STM_SEP, $array[$index]);
            }
            return $array;
        }
        return false;
    }

    /**
     * Returns only last X events from logfile.
     * Events are sorted from the latest to the oldest X events.
     * @param int $count
     * @param string $logname
     * @return array
     * Returns array if $count is integer and $logname is existing file in STM_LOG_ROOT.
     * If $count is smaller than one or bigger than number of records in file then
     * returns full content of logfile (in reverse order)
     */
    public static function getLastEvents($count, $logname = STM_DEFAULT_LOG_NAME)
    {
        $log = self::getLogContent($logname);
        if (is_array($log) && is_int($count)) {
            $log_count = count($log);
            if (self::isCountCorrect($count, $log_count)) {
                $lastEvents = array();
                for ($i = ($log_count - $count); $i < $log_count; $i++) {
                    $lastEvents[] = $log[$i];
                }
                $log = $lastEvents;
            }
            return array_reverse($log);
        }
        return false;
    }

    private static function isCountCorrect($count, $log_count)
    {
        return $count > 0 && $count < $log_count;
    }

    /**
     * It clears all content of the log file.
     * @return boolean
     * Returns true if the file was successfully cleared.
     */
    public static function clearLog($logname = STM_DEFAULT_LOG_NAME)
    {
        if (self::isLogWritable($logname)) {
            file_put_contents(STM_LOG_ROOT . $logname . STM_EXT, "");
            return true;
        }
        return false;
    }

    /**
     * Finds whether a variable can be writed into the log file.
     * @param object $event
     * @return boolean
     * True if $event is object and implements __toString()), otherwise false
     */
    private static function isEvent($event)
    {
        return is_object($event) ? method_exists($event, '__toString') : false;
    }

    /**
     * Finds whether an event can be saved into the log file and if $logname
     * can be used as name of the file
     * @param string $logname
     * @return boolean
     * Returns false, if log folder doesn't exist or user don't have file permission to write
     * or $logname is not string
     */
    private static function isLogWritable($logname = STM_DEFAULT_LOG_NAME)
    {
        if (is_string($logname) && file_exists(STM_LOG_ROOT)) {
            $file_exists = file_exists(STM_LOG_ROOT . $logname . STM_EXT);
            return ($file_exists && is_writable(STM_LOG_ROOT . $logname . STM_EXT)) ||
                    (!$file_exists && is_writable(STM_LOG_ROOT));
        }
        return false;
    }
}
