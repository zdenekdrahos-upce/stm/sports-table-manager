<?php
/*
 * This file is part of the PHP Sports Generators (https://bitbucket.org/zdenekdrahos/php-sports-generators)
 * Copyright (c) 2012, 2013 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * PHP Sports Generators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace STM\Libs\Generators\Playoff;

/**
 * BinaryTree class
 * - inserted items musto implement IBTreeItem interface, otherwise exception is thrown
 */
class BinaryTree
{
    private $items;
    private $itemCount;
    private $actualPosition;

    public function __construct()
    {
        $this->initTree();
    }

    public function destroy()
    {
        $this->initTree();
    }

    public function getItemCount()
    {
        return $this->itemCount;
    }

    public function insertRoot($item)
    {
        if ($this->isEmpty()) {
            $this->insert(1, $item);
        }
    }

    public function insertLeftSon($item)
    {
        if (!$this->isEmpty() && !$this->existsLeftSon()) {
            $this->insert(2 * $this->actualPosition, $item);
        }
    }

    public function insertRightSon($item)
    {
        if (!$this->isEmpty() && !$this->existsRightSon()) {
            $this->insert(2 * $this->actualPosition + 1, $item);
        }
    }

    public function deleteLeftSon()
    {
        if ($this->isLeftSonLeaf()) {
            return $this->delete(2 * $this->actualPosition);
        }
        return null;
    }

    public function deleteRightSon()
    {
        if ($this->isRightSonLeaf()) {
            return $this->delete(2 * $this->actualPosition + 1);
        }
        return null;
    }

    public function accessRoot()
    {
        if (!$this->isEmpty()) {
            $this->actualPosition = 1;
            return $this->accessActual();
        }
        return null;
    }

    public function accessLeftSon()
    {
        if ($this->existsLeftSon()) {
            $this->actualPosition = 2 * $this->actualPosition;
            return $this->accessActual();
        }
        return null;
    }

    public function accessRightSon()
    {
        if ($this->existsRightSon()) {
            $this->actualPosition = 2 * $this->actualPosition + 1;
            return $this->accessActual();
        }
        return null;
    }

    // non-standard method - the first found matching item is made accessible
    public function access($item)
    {
        $this->checkItem($item);
        foreach ($this->items as $key => $value) {
            if (call_user_func(array($item, 'isIdentical'), $item, $value)) {
                $this->actualPosition = $key;
                return $this->accessActual();
            }
        }
        return null;
    }

    public function accessLast($item)
    {
        $this->checkItem($item);
        $items = array_reverse($this->items, true);
        foreach ($items as $key => $value) {
            if (call_user_func(array($item, 'isIdentical'), $item, $value)) {
                $this->actualPosition = $key;
                return $this->accessActual();
            }
        }
        return null;
    }

    // returns complete array, iterators (in/pre/posrt order) should be there  instead of this method
    public function getItems()
    {
        return $this->items;
    }

    private function isEmpty()
    {
        return $this->getItemCount() == 0;
    }

    private function insert($index, $item)
    {
        $this->checkItem($item);
        if ($index >= 0) {
            $this->items[$index] = $item;
            $this->itemCount++;
            ksort($this->items);
        }
    }

    private function delete($index)
    {
        $removed = $this->items[$index];
        unset($this->items[$index]);
        $this->itemCount--;
        return $removed;
    }

    private function accessActual()
    {
        return $this->items[$this->actualPosition];
    }

    private function isLeftSonLeaf()
    {
        if ($this->existsLeftSon()) {
            return $this->isLeaf(2 * $this->actualPosition);
        }
        return false;
    }

    private function isRightSonLeaf()
    {
        if ($this->existsRightSon()) {
            return $this->isLeaf(2 * $this->actualPosition + 1);
        }
        return false;
    }

    private function existsLeftSon()
    {
        return $this->existsItem(2 * $this->actualPosition);
    }

    private function existsRightSon()
    {
        return $this->existsItem(2 * $this->actualPosition + 1);
    }

    private function isLeaf($index)
    {
        return !$this->existsItem(2 * $index) && !$this->existsItem(2 * $index + 1);
    }

    private function existsItem($index)
    {
        return isset($this->items[$index]);
    }

    private function checkItem($item)
    {
        if (!($item instanceof IBTreeItem)) {
            throw new PlayoffException(PlayoffException::EXC6);
        }
    }

    private function initTree()
    {
        $this->items = array();
        $this->itemCount = 0;
        $this->actualPosition = 1;
    }
}
