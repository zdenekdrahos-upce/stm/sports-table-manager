<?php
/*
 * This file is part of the PHP Sports Generators (https://bitbucket.org/zdenekdrahos/php-sports-generators)
 * Copyright (c) 2012, 2013 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * PHP Sports Generators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace STM\Libs\Generators\Playoff;

final class PlayoffTree
{
    /** @var array */
    private $teams;
    /** @var BinaryTree */
    private $btree;

    /** @param PlayoffGenerator $playoff */
    public function __construct($playoff)
    {
        if (!($playoff instanceof PlayoffGenerator)) {
            throw new PlayoffException(PlayoffException::EXC4);
        }
        $this->teams = $playoff->getTeams();
        $this->buildTree();
    }

    /** @return BinaryTree */
    public function getBinaryTree()
    {
        return $this->btree;
    }

    /** @return array */
    public function getArray()
    {
        $items = $this->btree->getItems();
        $result = array();
        $result['team_count'] = count($this->teams);
        $result['round_count'] = log(count($this->teams), 2);
        $result['rounds'] = array();
        for ($i = $result['round_count']; $i >= 1; $i--) {
            $round = array();
            $start = pow(2, $i);
            for ($j = 0; $j < pow(2, $i); $j+=2) {
                $round[] = array(
                    'home' => $items[$start + $j],
                    'away' => $items[$start + $j + 1]
                );
            }
            $result['rounds'][$i] = $round;
        }
        return $result;
    }

    private function buildTree()
    {
        $this->btree = new BinaryTree();
        $this->btree->insertRoot($this->teams[1]);
        for ($j = 1; $j <= count($this->teams) / 2; $j++) {
            $this->btree->accessRoot();
            $this->btree->access($this->teams[$j]);
            foreach ($this->findOpponents($j) as $opponent) {
                if ($j % 2 == 1) {
                    $this->btree->insertLeftSon($this->teams[$j]);
                    $this->btree->insertRightSon($opponent);
                    $this->btree->accessLeftSon();
                } else {
                    $this->btree->insertLeftSon($opponent);
                    $this->btree->insertRightSon($this->teams[$j]);
                    $this->btree->accessRightSon();
                }
            }
        }
    }

    private function findOpponents($team_index)
    {
        $opponents = array();
        for ($i = 2; $i <= count($this->teams); $i *= 2) {
            $opponent = $i - $team_index + 1;
            if ($opponent > $team_index) {
                $opponents[] = $this->teams[$opponent];
            }
        }
        return $opponents;
    }
}
