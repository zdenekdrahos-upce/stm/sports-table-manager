<?php


namespace STM\Libs\Generators\Playoff;

class Functions
{
    public static function isPowerOfTwo($number)
    {
        if (is_int($number) && $number >= 2) {
            return ($number & ($number - 1)) == 0;
        }
        return false;
    }
}
