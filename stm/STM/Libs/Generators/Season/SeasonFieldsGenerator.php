<?php
/*
 * This file is part of the PHP Sports Generators (https://bitbucket.org/zdenekdrahos/php-sports-generators)
 * Copyright (c) 2012, 2013 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * PHP Sports Generators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace STM\Libs\Generators\Season;

/**
 * SeasonFieldsGenerator class
 * - for generating season schedules with number of fields
 */
final class SeasonFieldsGenerator extends SeasonGenerator
{
    /** @var int */
    private $number_of_fields;

    /**
     * @param array $teams
     * @param int $period_count
     * @param int $number_of_fields
     * @return SeasonGenerator
     * @throws SeasonException
     */
    public static function create($teams, $period_count, $number_of_fields = 2)
    {
        parent::verifyArguments($teams, $period_count);
        self::verifyNumberOfFields($number_of_fields, count($teams));
        return new self($teams, $period_count, $number_of_fields);
    }

    protected function __construct($teams, $period_count, $number_of_fields = 2)
    {
        parent::__construct($teams, $period_count);
        $this->number_of_fields = $number_of_fields;
    }

    public function generate()
    {
        parent::generate();
        $this->updateSchedule();
        $this->updateRoundsInPeriod();
    }

    private function updateSchedule()
    {
        $old_schedule = $this->schedule;
        $this->schedule = array(0 => 1); // only because I want 1st round == 1st index in schedule
        $new_round_matches = array();
        foreach ($old_schedule as $round) {
            $matches = $round->getIterator();
            foreach ($matches as $z) {
                if (count($new_round_matches) >= $this->number_of_fields) {
                    $this->schedule[] = SeasonRound::constructFromMatches($new_round_matches);
                    $new_round_matches = array();
                }
                $new_round_matches[] = new SeasonMatch($z->home, $z->away);
            }
        }
        if (count($new_round_matches) > 0) {
            $this->schedule[] = SeasonRound::constructFromMatches($new_round_matches);
        }
        unset($this->schedule[0]);
    }

    private function updateRoundsInPeriod()
    {
        $matches_in_period = $this->team_count * ($this->team_count - 1) / 2;
        $this->rounds_in_period = ceil($matches_in_period / $this->number_of_fields);
    }

    private static function verifyNumberOfFields($number, $team_count)
    {
        $actual = floor($team_count / 2);
        if (!is_int($number) || $number <= 1 || $number >= $actual) {
            throw new SeasonException(SeasonException::EXC5);
        }
    }
}
