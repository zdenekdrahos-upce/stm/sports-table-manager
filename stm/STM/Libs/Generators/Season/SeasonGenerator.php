<?php
/*
 * This file is part of the PHP Sports Generators (https://bitbucket.org/zdenekdrahos/php-sports-generators)
 * Copyright (c) 2012, 2013 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * PHP Sports Generators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

namespace STM\Libs\Generators\Season;

/**
 * SeasonGenerator class
 * - for generating season schedules
 */
class SeasonGenerator implements \IteratorAggregate
{
    /** @var array */
    private $teams;
    /** @var int */
    private $period_count;
    /** @var int */
    protected $team_count;
    /** @var int */
    protected $rounds_in_period;
    /** @var array contains round for one period */
    protected $schedule;

    /**
     * @param array $teams
     * @param int $period_count
     * @return SeasonGenerator
     * @throws SeasonException
     */
    public static function create($teams, $period_count)
    {
        self::verifyArguments($teams, $period_count);
        return new self($teams, $period_count);
    }

    protected function __construct($teams, $period_count)
    {
        $this->schedule = array();
        $this->teams = array();
        $this->loadTeams($teams);
        $this->team_count = count($teams);
        $this->period_count = $period_count;
        $this->rounds_in_period = $this->calculateNumberOfRounds();
    }

    public function shuffleTeams()
    {
        if ($this->team_count % 2 == 1) {
            array_shift($this->teams);
            shuffle($this->teams);
            array_unshift($this->teams, null);
        } else {
            shuffle($this->teams);
        }
    }

    public function generate()
    {
        for ($i = 1; $i <= $this->rounds_in_period; $i++) {
            $this->schedule[$i] = SeasonRound::constructFromTeams($this->teams);
            $this->turnTeams();
        }
        $this->balanceMatchesOfFirstTeam();
    }

    public function getNumberOfPeriods()
    {
        return $this->period_count;
    }

    public function getNumberOfRoundsInPeriod()
    {
        return $this->rounds_in_period;
    }

    public function getIterator()
    {
        if (empty($this->schedule)) {
            throw new SeasonException(SeasonException::EXC2);
        }
        return new \ArrayIterator($this->schedule);
    }

    /**
     * Example:[1 2 3 4 5 6] -> matches 1-6, 2-5, 3-4
     * @return [1 6 2 3 4 5] -> matches 1-5, 6-4, 2-3
     */
    private function turnTeams()
    {
        $temp = $this->teams[count($this->teams) - 1];
        for ($i = count($this->teams) - 1; $i > 1; $i--) {
            $this->teams[$i] = $this->teams[$i - 1];
        }
        $this->teams[1] = $temp;
    }

    private function balanceMatchesOfFirstTeam()
    {
        for ($i = 1; $i <= $this->rounds_in_period; $i++) {
            if ($i % 2 == 0) {
                $this->schedule[$i]->swapMatchOfFirstTeam();
            }
        }
    }

    private function calculateNumberOfRounds()
    {
        return ($this->team_count % 2 == 0) ? $this->team_count - 1 : $this->team_count;
    }

    private function loadTeams($teams)
    {
        foreach ($teams as $team) {
            $this->teams[] = $team;
        }
        if (count($this->teams) % 2 == 1) {
            array_unshift($this->teams, null);
        }
    }

    protected static function verifyArguments($teams, $period_count)
    {
        $conditions = array(
            !is_array($teams),
            is_array($teams) ? in_array(null, $teams) : false,
            count($teams) < 2 || count($teams) > SEASON_MAX_TEAMS,
            !is_int($period_count) || $period_count < SEASON_MIN_PERIODS || $period_count > SEASON_MAX_PERIODS,
        );
        if (in_array(true, $conditions)) {
            throw new SeasonException(SeasonException::EXC1);
        }
    }
}
