<?php

/**
 * PHP Form Proccessing class - for basic form processing operations - escaping values,
 * checking required fields, other basic check operations.
 * Copyright (C) 2011  Zdenek Drahos (https://bitbucket.org/zdenekdrahos/php-form-proccessing-class)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

namespace STM\Libs;

class FormProcessor
{
    private $existsFilterVar;
    private $striptags;
    private $errors = array();

    /**
     * Creates new instance of class and escapes all $_POST variables
     */
    public function __construct($striptags = false)
    {
        $this->striptags = $striptags ? true : false;
        //$this->escapeValues($striptags);// problem in template
        $this->existsFilterVar = function_exists('filter_var');
    }

    /**
     * Checks if there were any errors during form processing
     * @return boolean
     */
    public function isValid()
    {
        return count($this->errors) ? false : true;
    }

    /**
     * Returns array with errors messages
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Clears all errors
     */
    public function resetErrors()
    {
        $this->errors = array();
    }

    /**
     * Method for inserting own error not connected with methods from this class
     * @param string $error_message
     */
    public function addError($error_message)
    {
        //$error_message = $this->escapeValue($error_message);
        if (!empty($error_message)) {
            $this->errors[] = $error_message;
        } else {
            $this->errors[] = 'ERROR';
        }
    }

    /**
     * @param string $required name of the $_POST variable
     * @param string $message error message, format: $required_filed . $message
     */
    public function checkRequiredFields($required, $message = ' is required.')
    {
        foreach ($required as $fieldname) {
            if (!isset($_POST[$fieldname]) || (empty($_POST[$fieldname]) && !is_numeric($_POST[$fieldname]))) {
                $this->addError($fieldname . $message);
            }
        }
    }

    /**
     * If condition is false then adds error. Basic method, don't have to use e.g.
     * checkRange($a, 0, 10) but you can use checkCondition($a >= 0 && $a <= 10)
     * @param boolean $condition
     * @param string $error_message
     */
    public function checkCondition($condition, $error_message = 'Error: function returns false.')
    {
        if ($condition == false) {
            $this->addError($error_message);
        }
    }

    /**
     * Checks if $_POST[$email] contains valid email address. For PHP >= 5.2 uses
     * filter_var function, otherwise regular expression. Checks only if $email is not empty
     * @param string $email
     * @param string $error_message
     */
    public function checkEmail($email, $error_message = 'Please insert valid email adddress.')
    {
        if (!empty($email)) {
            if ($this->existsFilterVar) {
                if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $this->addError($error_message);
                }
            } else {
                if (!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/", $email)) {
                    $this->addError($error_message);
                }
            }
        }
    }

    /**
     * Checks if $_POST[$url] contains valid internet address. For PHP >= 5.2 uses
     * filter_var function, otherwise regular expression. It works for
     * http://www.xxx.com as well as for www.xxx.com. Checks only if $url is not empty
     * @param string $url
     * @param string $error_message
     */
    public function checkUrlAddress($url, $error_message = 'Please insert valid url address.')
    {
        if (!empty($url)) {
            $url = (stristr($url, 'http://')) ? $url : 'http://' . $url;
            /* FILTER_VALIDATE_URL - valid e.g. xxxxxx
              if ($this->exists_filter_var) {
              if (!filter_var($url, FILTER_VALIDATE_URL)) {
              $this->addError($error_message);
              }
              } else { */
            $pattern = "#((http|https|ftp)://(\S*?\.\S*?))(\s|\;|\)|\]|\[|\{|\}|,|\"|'|:|\<|$|\.\s)#ie";
            if (!preg_match($pattern, $url)) {
                $this->addError($error_message);
            }
            //}
        }
    }

    /**
     * If $a is not equal to $b -> error. Method directly not work with POST variables
     * you have to send POST variable in argument e.g. checkEquality($_POST['pass'], ...)
     * @param mixed $a
     * @param mixed $b
     * @param string $error_message
     */
    public function checkEquality($a, $b, $error_message = 'Values not match')
    {
        if (!empty($a) && !empty($b)) {
            if ($a != $b) {
                $this->addError($error_message);
            }
        }
    }

    /**
     * Checks if $filename is valid name for file.
     * Error if name contains: space / ? * : ; { } \
     * @param type $filename
     * @param type $error_message
     */
    public function checkFilename($filename, $error_message = 'Name is not valid filename.')
    {
        if (!preg_match('/^[a-z][-\w]*$/i', $filename)) {
            $this->addError($error_message);
        }
    }

    /**
     * Checks if $value is from allowed range. Method is not limited by data type.
     * You can check numbers, dates, chars, ...
     * E.g. checkRange($value, 0, 10)
     *      - valid $value: 0, 1, 5, 10, ...
     *      - invalid $value: -20, -1, 11, 55
     * @param mixed $value
     * @param mixed $min
     * @param mixed $max
     * @param string $error_message
     */
    public function checkRange($value, $min, $max, $error_message = 'Value is out of bounds.')
    {
        if (!($value >= $min && $value <= $max)) {
            $this->addError($error_message);
        }
    }

    /**
     * Escapes all $_POST variables + 1D array
     */
    public function escapeValues()
    {
        foreach ($_POST as $post_name => $post_value) {
            if (!is_array($post_value)) {
                $_POST[$post_name] = $this->escapeValue($post_value);
            } else {
                foreach ($post_value as $key => $value) {
                    if (!is_array($value)) {
                        $_POST[$post_name][$key] = $this->escapeValue($value);
                    }
                }
            }
        }
    }

    /**
     * Provides basic value escape. Not escaping for database, it's work for
     * database class. Against PHP Injection, not against SQL injection.
     * @param mixed $value
     * @return mixed
     */
    private function escapeValue($value)
    {
        if ($this->striptags) {
            $value = strip_tags($value);
        }
        return htmlspecialchars(stripslashes(trim($value)));
    }

    /**
     * Initializes post variables, useful when you display filled values in form
     * e.g. after unsuccessfull submit
     * @param array $fields of names post variables
     */
    public function initVars($fields)
    {
        if (is_array($fields)) {
            foreach ($fields as $item) {
                if (!is_array($item)) {
                    $_POST[$item] = '';
                } else {
                    if (is_string($item[0]) && is_int($item[1])) {
                        for ($i = 0; $i < $item[1]; $i++) {
                            $_POST[$item[0]][$i] = '';
                        }
                    }
                }
            }
        }
    }
}
