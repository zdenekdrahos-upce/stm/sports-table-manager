<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Country;

use STM\Helpers\ObjectValidator;

/**
 * Class for validating attributes for database table 'COUNTRIES'
 * - attributes = country (name)
 */
final class CountryValidator
{
    /** @var ObjectValidator */
    private static $validator;

    /** @param \STM\Libs\FormProcessor $formProcessor */
    public static function init($formProcessor = false)
    {
        if (is_null(self::$validator)) {
            self::$validator = new ObjectValidator('\STM\Country\Country');
        }
        self::$validator->setFormProcessor($formProcessor);
    }

    public static function setCountry($country)
    {
        self::$validator->setComparedObject($country);
    }

    public static function resetCountry()
    {
        self::$validator->resetComparedObject();
    }

    public static function checkCreate($attributes)
    {
        if (self::$validator->checkCreate($attributes, array('country'))) {
            self::checkCountryName($attributes['country']);
        }
        return self::$validator->isValid();
    }

    public static function checkCountryName($new_name)
    {
        return self::$validator->checkUniqueName($new_name, array('min' => 2, 'max' => 40), '__toString');
    }
}
