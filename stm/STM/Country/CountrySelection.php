<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Country;

use STM\DB\Query\QueryFactory;
use STM\Entities\AbstractEntitySelection;

class CountrySelection extends AbstractEntitySelection
{

    public function setId($idCountry)
    {
        if (is_numeric($idCountry)) {
            $this->selection->setAttributeWithValue('id_country', (string) $idCountry);
        }
    }

    protected function getQuery()
    {
        return QueryFactory::selectAll(DT_COUNTRIES);
    }
}
