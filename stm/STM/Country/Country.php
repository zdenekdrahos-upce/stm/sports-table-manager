<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Country;

use STM\DB\Object\DatabaseObject;

final class Country
{
    /** @var DatabaseObject */
    private static $db;
    /** @var int */
    private $id_country;
    /** @var string */
    private $country;
    /** @var int */
    private $id_flag;

    private function __construct()
    {
    }

    public static function setORM(DatabaseObject $dbObject)
    {
        if (is_null(self::$db)) {
            self::$db = $dbObject;
        }
    }

    public static function create($attributes)
    {
        if (CountryValidator::checkCreate($attributes)) {
            return self::$db->insertValues($attributes);
        }
        return false;
    }

    public static function exists($country_name)
    {
        return self::$db->existsItem('country', $country_name);
    }

    public function delete()
    {
        return self::$db->deleteById($this->id_country);
    }

    public function updateCountryName($name)
    {
        CountryValidator::setCountry($this);
        if (CountryValidator::checkCountryName($name)) {
            return self::$db->updateById($this->id_country, array('country' => $name));
        }
        return false;
    }

    public function getId()
    {
        return (int) $this->id_country;
    }

    public function toArray()
    {
        return array('country' => $this->country);
    }

    public function __toString()
    {
        return $this->country;
    }
}
