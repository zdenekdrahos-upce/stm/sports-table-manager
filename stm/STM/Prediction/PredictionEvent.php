<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Prediction;

use STM\Match\Match;
use STM\Helpers\Event;

class PredictionEvent
{
    /** Creating new match */
    const CREATE = 'Create Prediction';
    /** Updating match - update date, home team, away team */
    const UPDATE = 'Update Prediction';

    private function __construct()
    {
    }

    /**
     * @param PredictionEvent $predictionEvent
     * @param Match $match
     * @return boolean
     * Returns true if event was successfuly saved, otherwise false (bad arguments
     * or error during file saving)
     */
    public static function log($predictionEvent, $match)
    {
        $event = new Event('\STM\Prediction\PredictionEvent', 'predictions');
        $event->setMessage($predictionEvent, self::getMessage($match), true);
        return $event->logEvent();
    }

    private static function getMessage($match)
    {
        if ($match instanceof Match) {
            $id_match = $match->getId();
            $id_competition = $match->getIdCompetition();
            $id_home = $match->getIdTeamHome();
            $id_away = $match->getIdTeamAway();
            return implode('"' . STM_SEP . '"', array($id_match, $id_competition, $id_home, $id_away));
        }
        return 'Invalid Match';
    }
}
