<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Prediction;

final class PredictionResult
{
    const EXACT = 'EXACT';
    const SUCCESS = 'SUCCESS';
    const FAIL = 'FAIL';
    const UNKNOWN = 'UNKNOWN';

    private function __construct()
    {
    }

    public static function getResult(Prediction $prediction)
    {
        $arr = $prediction->toArray();
        if ($arr['match_score'] != '-:-') {
            $diff_prediction = $prediction->getScoreHome() - $prediction->getScoreAway();
            $diff_match = $prediction->getMatchScoreHome() - $prediction->getMatchScoreAway();
            if ($arr['match_score'] == "{$prediction->getScoreHome()}:{$prediction->getScoreAway()}") {
                return self::EXACT;
            } elseif ($diff_prediction == $diff_match || ($diff_prediction > 0 && $diff_match > 0)
                || ($diff_prediction < 0 && $diff_match < 0)) {
                return self::SUCCESS;
            }
            return self::FAIL;
        }
        return self::UNKNOWN;
    }
}
