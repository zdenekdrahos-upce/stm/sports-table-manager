<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Prediction\Stats;

use STM\DB\SQL\SelectQuery;
use STM\DB\SQL\JoinTable;
use STM\DB\SQL\JoinType;

class PredictionStatsSQL
{

    /**
     * Selects statistics for all predicted matches.
     * @return SelectQuery
     */
    public static function selectPredictionStatistics()
    {
        $query = new SelectQuery(DT_MATCHES_PREDICTIONS . ' matches_predictions');
        $query->setColumns(
            'count(distinct matches.id_match) as matches,
            count(distinct matches.id_match) -
            sum(case when match_results.score_home is null then 1 else 0 end) as played_matches,
            count(matches_predictions.id_match) as predictions,
            sum(case when matches_predictions.score_home > matches_predictions.score_away
                     then 1 else 0 end) as win_predictions,
            sum(case when matches_predictions.score_home = matches_predictions.score_away
                     then 1 else 0 end) as draw_predictions,
            sum(case when matches_predictions.score_home < matches_predictions.score_away
                     then 1 else 0 end) as loss_predictions,
            sum(case
            when matches_predictions.score_home = match_results.score_home and
                 matches_predictions.score_away = match_results.score_away
            then 1
            else 0 end) as exact_predictions,
            sum(case
                when (match_results.score_home > match_results.score_away AND
                      matches_predictions.score_home > matches_predictions.score_away)
                  OR (match_results.score_home < match_results.score_away AND
                      matches_predictions.score_home < matches_predictions.score_away)
                  OR
                     (match_results.score_home = match_results.score_away AND
                      matches_predictions.score_home = matches_predictions.score_away)
                then 1
                else 0
            end) as success_predictions'
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                DT_MATCHES . ' matches',
                'matches.id_match = matches_predictions.id_match'
            )
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::LEFT,
                DT_MATCH_RESULTS . ' match_results',
                'matches.id_match = match_results.id_match'
            )
        );
        return $query;
    }
}
