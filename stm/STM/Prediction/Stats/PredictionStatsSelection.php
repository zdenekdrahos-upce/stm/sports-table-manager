<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Prediction\Stats;

use STM\User\User;
use STM\Match\Match;
use STM\Match\MatchSelection;
use STM\Match\MatchSelectionBuilder;
use STM\Competition\Competition;
use STM\Entities\AbstractEntitySelection;

class PredictionStatsSelection extends AbstractEntitySelection
{
    /** @var STM\Match\MatchSelectionBuilder */
    private $matchSelectionBuilder;

    public function __construct()
    {
        parent::__construct();
        $this->matchSelectionBuilder = new MatchSelectionBuilder();
    }

    public function setUser(User $user)
    {
        $this->selection->setAttributeWithValue('id_user', (string) $user->getId());
    }

    public function setMatch(Match $match)
    {
        $ms = array(
            'matchType' => MatchSelection::ALL_MATCHES,
            'loadScores' => false,
            'loadPeriods' => false,
            'matchId' => $match->getId(),
        );
        $this->setMatchSelection($ms);
    }

    public function setCompetition(Competition $competition)
    {
        $ms = array(
            'matchType' => MatchSelection::ALL_MATCHES,
            'loadScores' => false,
            'loadPeriods' => false,
            'competition' => $competition,
        );
        $this->setMatchSelection($ms);
    }

    public function setMatchSelection(array $matchSelection)
    {
        $selection = $this->matchSelectionBuilder->build($matchSelection);
        parent::addMatchSelectionFilter($selection, 'matches_predictions');
    }

    protected function getQuery()
    {
        return PredictionStatsSQL::selectPredictionStatistics();
    }
}
