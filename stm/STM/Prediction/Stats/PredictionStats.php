<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Prediction\Stats;

use STM\DB\Object\DatabaseObject;

/**
 * PredictionStats class
 * - consists of numbers (one db row) which analyze success in prediction of match results
 * - read only
 */
final class PredictionStats
{
    /** @var DatabaseObject */
    private static $db;

    /** @var int count of matches that are counted to statistic */
    private $matches;
    /** @var int count of played matches (matches with result) */
    private $played_matches;
    /** @var int count of all predictions */
    private $predictions;
    /** @var int number of predicted wins */
    private $win_predictions;
    /** @var int number of predicted draws */
    private $draw_predictions;
    /** @var int number of predicted loss */
    private $loss_predictions;
    /** @var int number of matches where result of predicted score (W,D,L) = match result */
    private $success_predictions;
    /** @var int number of matches where predicted score = match result */
    private $exact_predictions;

    private function __construct()
    {
        $this->typeAttributes();
    }

    public static function setORM(DatabaseObject $dbObject)
    {
        if (is_null(self::$db)) {
            self::$db = $dbObject;
        }
    }

    /**
     * Method for templating. Returns prediction statistics as an array
     * @return array
     */
    public function toArray()
    {
        return get_object_vars($this);
    }

    /**
     * Types attributes to int. Its needed because database_fetch returns everything as string.
     * Used if attributes value is null then value is converted to 0
     */
    private function typeAttributes()
    {
        foreach (get_object_vars($this) as $key => $value) {
            $this->$key = empty($value) ? 0 : (int) $value;
        }
    }
}
