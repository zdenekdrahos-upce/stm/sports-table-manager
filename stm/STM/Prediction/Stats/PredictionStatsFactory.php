<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Prediction\Stats;

use STM\User\User;
use STM\Match\Match;
use STM\Competition\Competition;
use STM\Entities\AbstractEntityFactory;

class PredictionStatsFactory extends AbstractEntityFactory
{
    public function findByUser(User $user)
    {
        $methods = array('setUser' => $user);
        return $this->entityHelper->setAndFindObject($methods);
    }

    public function findByMatch(Match $match)
    {
        $methods = array('setMatch' => $match);
        return $this->entityHelper->setAndFindObject($methods);
    }

    public function findByCompetition(Competition $competition)
    {
        $methods = array('setCompetition' => $competition);
        return $this->entityHelper->setAndFindObject($methods);
    }

    public function find(array $matchSelection)
    {
        $methods = array('setMatchSelection' => $matchSelection);
        return $this->entityHelper->setAndFindObject($methods);
    }

    protected function getEntitySelection()
    {
         return new PredictionStatsSelection();
    }

    protected function getEntityClass()
    {
        return 'STM\Prediction\Stats\PredictionStats';
    }
}
