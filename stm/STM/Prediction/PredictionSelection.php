<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Prediction;

use STM\User\User;
use STM\Match\Match;
use STM\Match\MatchSelection;
use STM\Match\MatchSelectionBuilder;
use STM\Utils\Classes;
use STM\Entities\AbstractEntitySelection;

final class PredictionSelection extends AbstractEntitySelection
{
    /** @var STM\Match\MatchSelectionBuilder */
    private $matchSelectionBuilder;

    public function __construct()
    {
        parent::__construct();
        $this->matchSelectionBuilder = new MatchSelectionBuilder();
    }

    public function setId(User $user, Match $match)
    {
        $this->setUser($user);
        $this->setMatch($match);
    }

    public function setUser(User $user)
    {
        $this->selection->setAttributeWithValue('predictions.id_user', (string) $user->getId());
    }

    public function setMatch(Match $match)
    {
        $ms = array(
            'matchType' => MatchSelection::ALL_MATCHES,
            'loadScores' => true,
            'loadPeriods' => true,
            'matchId' => $match->getId(),
        );
        $this->setMatchSelection($ms);
    }

    public function setMatchSelection(array $matchSelection)
    {
        $selection = $this->matchSelectionBuilder->build($matchSelection);
        parent::addMatchSelectionFilter($selection, 'predictions');
    }

    public function setMatchType($matchType)
    {
        if (Classes::isClassConstant('\STM\Match\MatchSelection', $matchType)) {
            if ($matchType == MatchSelection::UPCOMING_MATCHES) {
                $this->selection->setNullAttribute('results.id_match', true);
            } elseif ($matchType == MatchSelection::PLAYED_MATCHES) {
                $this->selection->setNullAttribute('results.id_match', false);
            }
        }
    }

    protected function getQuery()
    {
        return PredictionSQL::selectPredictions();
    }
}
