<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Prediction;

use STM\User\User;
use STM\Match\Match;
use STM\Match\MatchSelection;
use STM\Match\Filter\MatchFilter;
use STM\Entities\AbstractEntityFactory;

final class PredictionFactory extends AbstractEntityFactory
{
    public function findById(User $user, Match $match)
    {
        return $this->entityHelper->findById($user, $match);
    }

    public function findByUser(User $user)
    {
        $methods = array('setUser' => $user);
        return $this->entityHelper->setAndFindArray($methods);
    }

    public function findByFilter(User $user, array $filterArray)
    {
        $methods = array('setUser' => $user);
        if (isset($filterArray['match_type'])) {
            $methods['setMatchType'] = MatchFilter::getMatchType($filterArray['match_type']);
        }
        if (isset($filterArray['team']) || isset($filterArray['competition'])) {
            $ms = array(
                'matchType' => MatchSelection::ALL_MATCHES,
                'loadScores' => false,
                'loadPeriods' => false
            );
            if (isset($filterArray['competition'])) {
                $ms['competition'] = $filterArray['competition'];
            }
            if (isset($filterArray['team'])) {
                $ms['teams'] = array($filterArray['team']);
            }
            $methods['setMatchSelection'] = $ms;
        }

        return $this->entityHelper->setAndFindArray($methods);
    }

    public function find(array $matchSelection)
    {
        $methods = array('setMatchSelection' => $matchSelection);
        return $this->entityHelper->setAndFindArray($methods);
    }

    protected function getEntitySelection()
    {
         return new PredictionSelection();
    }

    protected function getEntityClass()
    {
        return 'STM\Prediction\Prediction';
    }
}
