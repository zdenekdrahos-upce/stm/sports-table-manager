<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Prediction;

use STM\Helpers\ObjectValidator;
use STM\Web\Message\FormError;
use STM\User\User;
use STM\Match\Match;
use STM\Utils\Dates;
use STM\StmFactory;

/**
 * Class for validating attributes for database table 'MATCHES_PREDICTIONS'
 * - validated attributes = match, user, score_home, score_away
 */
final class PredictionValidator
{
    /** @var ObjectValidator */
    private static $validator;

    /**
     * @param \STM\Libs\FormProcessor $formProcessor
     */
    public static function init($formProcessor = false)
    {
        if (is_null(self::$validator)) {
            self::$validator = new ObjectValidator('\STM\Prediction\Prediction');
        }
        self::$validator->setFormProcessor($formProcessor);
    }

    /**
     * Sets instance of the Prediction class. Instance is used for checking if
     * new value of attribute is different from current value in prediction.
     * @param Match $prediction
     */
    public static function setPrediction($prediction)
    {
        self::$validator->setComparedObject($prediction);
    }

    /**
     * Removes prediction, so attributes are not compared with prediction attributes
     */
    public static function resetPrediction()
    {
        self::$validator->resetComparedObject();
    }

    /**
     * Checks if array $attributes can be used as values for new prediction in database.
     * @param array $attributes keys: user, match, score_home, score_away
     * @return boolean
     * Return true if array contains keys user, match, score_home, score_away and
     * user and match are instances of classes User and Match and scores are valid numbers.
     */
    public static function checkCreate($attributes)
    {
        if (self::$validator->checkCreate($attributes, array('user', 'match', 'score_home', 'score_away'))) {
            self::$validator->getFormProcessor()->checkCondition($attributes['user'] instanceof User, 'Invalid user');
            self::$validator->getFormProcessor()->checkCondition(
                $attributes['match'] instanceof Match,
                'Invalid match'
            );
            if (self::$validator->isValid()) {
                self::checkFields($attributes, $attributes['match']);
                self::checkExistingPrediction($attributes);
            }
        }
        return self::$validator->isValid();
    }

    /**
     * Checks if scores can be used in update method. Scores must be valid numbers
     * and at least change in one score (compared Prediction is required)
     * @param array $attributes - keys: score_home, score_away
     * @return boolean
     */
    public static function checkUpdateScore($attributes)
    {
        if (self::$validator->checkUpdate($attributes, array('score_home', 'score_away'))) {
            $match = self::$validator->getValueFromCompared('getPredictedMatch');
            self::checkFields($attributes, $match);
            if (self::$validator->isValid()) {
                self::checkScoreChange($attributes);
            }
        }
        return self::$validator->isValid();
    }

    private static function checkFields($attributes, $match)
    {
        self::checkHomeScore($attributes['score_home']);
        self::checkAwayScore($attributes['score_away']);
        if (self::$validator->isValid()) {
            self::checkPredictionDateAndMatchScore($match);
            self::checkCompetitionStatus($match);
        }
    }

    /**
     * Checks if match can be predicted (date of the match must be not null and
     * smaller then current date)
     * @param Match $match
     */
    private static function checkPredictionDateAndMatchScore($match)
    {
        $match_date = $match->getDate();
        self::$validator->getFormProcessor()->checkCondition(
            $match_date->isValid(),
            FormError::get('empty-value', array(stmLang('match', 'date')))
        );
        self::$validator->getFormProcessor()->checkCondition(
            $match->hasScore() == false,
            FormError::get('prediction-score')
        );
        if (self::$validator->isValid()) {
            $are_dates_ok = Dates::areDatesInOrder('now', Dates::datetimeToDatabaseDate($match_date));
            self::$validator->getFormProcessor()->checkCondition($are_dates_ok, FormError::get('prediction-date'));
        }
    }

    /**
     * Checks if $score is number and from interval <1,25500>
     * @param int $score
     * @return boolean
     */
    private static function checkHomeScore($score)
    {
        return self::checkScore($score, 'score-home');
    }

    /**
     * Checks if $score is number and from interval <1,25500>
     * @param int $score
     * @return boolean
     */
    private static function checkAwayScore($score)
    {
        return self::checkScore($score, 'score-away');
    }

    /**
     * Checks number from parent with selected error name
     * @param int $score
     * @param string $name
     * @return boolean
     */
    private static function checkScore($score, $name)
    {
        return self::$validator->checkNumber($score, array('min' => 0, 'max' => 25500), stmLang('match', $name));
    }

    private static function checkCompetitionStatus($match)
    {
        $competition = StmFactory::find()->Competition->findById($match->getIdCompetition());
        self::$validator->getFormProcessor()->checkCondition(
            $competition->getStatusId() == 'P',
            FormError::get('prediction-competition')
        );
    }

    private static function checkExistingPrediction($attributes)
    {
        self::$validator->getFormProcessor()->checkCondition(
            StmFactory::find()->Prediction->findById($attributes['user'], $attributes['match']) == false,
            FormError::get('unique-value', array(stmLang('prediction', 'prediction')))
        );
    }

    private static function checkScoreChange($attributes)
    {
        self::$validator->getFormProcessor()->checkCondition(
            $attributes['score_home'] != self::$validator->getValueFromCompared('getScoreHome')
            || $attributes['score_away'] != self::$validator->getValueFromCompared('getScoreAway'),
            FormError::get('no-change', array(stmLang('match', 'score')))
        );
    }
}
