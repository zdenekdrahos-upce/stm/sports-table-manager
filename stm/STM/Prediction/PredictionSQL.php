<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Prediction;

use STM\DB\Database;
use STM\DB\SQL\SelectQuery;
use STM\DB\SQL\JoinTable;
use STM\DB\SQL\JoinType;
use STM\Match\MatchSQL;

class PredictionSQL
{

    /** @return SelectQuery */
    public static function selectPredictions()
    {
        $query = new SelectQuery(DT_MATCHES_PREDICTIONS . ' predictions');
        $query->setColumns(
            "predictions.id_match, predictions.id_user, predictions.score_home, predictions.score_away,
            (CASE WHEN matches.datetime IS NULL OR predictions.last_modified > matches.datetime
                  THEN 0 ELSE 1 END) as is_valid,
            results.score_home as match_score_home, results.score_away as match_score_away,
            concat(concat(matches.team_home, ':'), matches.team_away) as match_name"
        );
        $matches_query = MatchSQL::selectMatches();
        Database::escapeQuery($matches_query);
        $query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                "({$matches_query->generate()}) matches",
                'predictions.id_match = matches.id_match'
            )
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::LEFT,
                DT_MATCH_RESULTS . " results",
                'predictions.id_match = results.id_match'
            )
        );
        $query->setOrderBy('predictions.id_match');
        return $query;
    }
}
