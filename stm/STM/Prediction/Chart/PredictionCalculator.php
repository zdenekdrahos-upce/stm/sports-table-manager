<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Prediction\Chart;

class PredictionCalculator implements IPredictionRowCalculator
{
    public static function calculatePoints(&$row)
    {
        if ($row instanceof ChartRow) {
            if ($row->predictions == 0) {
                $row->points = 0;
            } else {
                $row->points = round(
                    ($row->successPredictions + 2 * $row->exactPredictions) *
                    ($row->successPredictions / $row->predictions),
                    2
                );
            }
        }
    }
}
