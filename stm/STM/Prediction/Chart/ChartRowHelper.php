<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Prediction\Chart;

class ChartRowHelper
{
    /**
     * Compares two instances of ChartRow by points.
     * @param ChartRow $a
     * @param ChartRow $b
     * @return int
     * Returns -1 if $a has more points than $b, if $b has more points then returns 1.
     * If points are equal, then compares number exact predictions and number of all
     * predicitions. If these number are equal too then returns 0
     */
    public static function compareByPoints($a, $b)
    {
        if ($a instanceof ChartRow && $b instanceof ChartRow) {
            if ($a->points != $b->points) {
                return ($a->points > $b->points) ? -1 : 1;
            } elseif ($a->exactPredictions != $b->exactPredictions) {
                return ($a->exactPredictions > $b->exactPredictions) ? -1 : 1;
            } elseif ($a->predictions != $b->predictions) {
                return ($a->predictions > $b->predictions) ? 1 : -1;
            }
        }
        return 0;
    }
}
