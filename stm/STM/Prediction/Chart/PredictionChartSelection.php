<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Prediction\Chart;

use STM\Match\MatchSelection;
use STM\Competition\Competition;
use STM\Match\MatchSelectionBuilder;
use STM\Entities\AbstractEntitySelection;

class PredictionChartSelection extends AbstractEntitySelection
{
    /** @var IPredictionRowCalculator */
    private $calculator;
    /** @var STM\Match\MatchSelectionBuilder */
    private $matchSelectionBuilder;

    public function __construct()
    {
        parent::__construct();
        $this->matchSelectionBuilder = new MatchSelectionBuilder();
    }

    public function getCalculator()
    {
        return $this->calculator;
    }

    public function setCalculator(IPredictionRowCalculator $calculator)
    {
        $this->calculator = $calculator;
    }

    public function setAllMatches()
    {
        $ms = array(
            'matchType' => MatchSelection::ALL_MATCHES,
            'loadScores' => false,
            'loadPeriods' => false
        );
        $this->setMatchSelection($ms);
    }

    public function setCompetition(Competition $competition)
    {
        $ms = array(
            'matchType' => MatchSelection::ALL_MATCHES,
            'loadScores' => false,
            'loadPeriods' => false,
            'competition' => $competition,
        );
        $this->setMatchSelection($ms);
    }

    public function setMatchSelection(array $matchSelection)
    {
        $selection = $this->matchSelectionBuilder->build($matchSelection);
        parent::addMatchSelectionFilter($selection, 'matches_predictions');
    }

    public function ignoreCheatPredictions()
    {
        $condition = PredictionChartSQL::getConditionIgnoringCheaters();
        $this->selection->addCondition($condition);
    }

    protected function getQuery()
    {
        return PredictionChartSQL::selectPredictionChart();
    }
}
