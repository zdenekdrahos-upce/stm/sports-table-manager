<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Prediction\Chart;

use STM\DB\SQL\SelectQuery;
use STM\DB\SQL\JoinTable;
use STM\DB\SQL\JoinType;
use STM\DB\SQL\Condition;
use STM\Prediction\Stats\PredictionStatsSQL;

class PredictionChartSQL
{

    /**
     * Selects prediction chart. Optionally select can be limited by where condition
     * @param Condition $where (attributes from tables matches)
     * @return SelectQuery
     */
    public static function selectPredictionChart()
    {
        $query = PredictionStatsSQL::selectPredictionStatistics();
        $query->resetColumns();
        $query->setColumns(
            'users.id_user as idUser, users.name as username,
            count(distinct matches.id_match) -
            sum(case when match_results.score_home is null then 1 else 0 end) as predictions,
            sum(case when matches_predictions.score_home = match_results.score_home and
                          matches_predictions.score_away = match_results.score_away
                     then 1
                     else 0
            end) as exactPredictions,
            sum(case
                when (match_results.score_home > match_results.score_away AND
                      matches_predictions.score_home > matches_predictions.score_away)
                  OR (match_results.score_home < match_results.score_away AND
                      matches_predictions.score_home < matches_predictions.score_away)
                  OR (match_results.score_home = match_results.score_away AND
                      matches_predictions.score_home = matches_predictions.score_away)
                then 1
                else 0
            end) as successPredictions,
            sum(case
                when matches.datetime IS NULL OR matches_predictions.last_modified > matches.datetime then 1
                else 0
            end) as cheatPredictions'
        );
        $query->setGroupBy('users.id_user, users.name');
        $query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                DT_USERS . ' users',
                'matches_predictions.id_user = users.id_user'
            )
        );
        return $query;
    }

    /** @return \STM\DB\SQL\Condition */
    public static function getConditionIgnoringCheaters()
    {
        return new Condition('matches.datetime IS NOT NULL AND matches_predictions.last_modified <= matches.datetime');
    }
}
