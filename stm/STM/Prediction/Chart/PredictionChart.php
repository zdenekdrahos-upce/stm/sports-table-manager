<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Prediction\Chart;

use STM\DB\Object\DatabaseObject;

/**
 * PredictionChart class
 * - read only
 */
final class PredictionChart
{
    /** @var DatabaseObject */
    private static $db;
    /** @var IPredictionRowCalculator */
    private $points_calculator;
    /** @var array */
    private $chart;

    private function __construct($query, $calculator)
    {
        $this->points_calculator = new $calculator();
        $this->chart = self::$db->select($query);
        $this->calculatePoints();
        $this->sortTable();
    }

    public static function setORM(DatabaseObject $dbObject)
    {
        if (is_null(self::$db)) {
            self::$db = $dbObject;
        }
    }

    public static function find(PredictionChartSelection $selection)
    {
        return new self($selection->getSelectQuery(), $selection->getCalculator());
    }

    /** @return array */
    public function getChart()
    {
        return $this->chart;
    }

    protected function calculatePoints()
    {
        foreach ($this->chart as &$chart_row) {
            $this->points_calculator->calculatePoints($chart_row);
        }
    }

    private function sortTable()
    {
        if (is_array($this->chart)) {
            usort($this->chart, array('\STM\Prediction\Chart\ChartRowHelper', 'compareByPoints'));
        }
    }
}
