<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Prediction\Chart;

/**
 * ChartRow class
 * - represents one line from prediction chart
 * - read only
 * - instances only from PredictionChart class
 */
final class ChartRow
{
    /** @var int */
    public $idUser;
    /** @var string username */
    public $username;
    /** @var int count of all predictions */
    public $predictions;
    /** @var int number of matches where result of predicted score (W,D,L) = match result */
    public $successPredictions;
    /** @var int number of matches where predicted score = match result */
    public $exactPredictions;
    /** @var double  */
    public $points;
    /** @var int number of matches where match.date is null or prediction.date > match.date */
    public $cheatPredictions;

    private function __construct()
    {
        $this->typeAttributes();
    }

    public function toArray()
    {
        return get_object_vars($this);
    }

    /** @return string */
    public function __toString()
    {
        return $this->username . ' | ' . $this->predictions . ' ' .
                $this->successPredictions . ' ' . $this->exactPredictions
                . ' | ' . $this->points;
    }

    /**
     * Types attributes to int. Its needed because database fetch returns everything as string.
     * Used if attributes value is null then value is converted to 0
     */
    private function typeAttributes()
    {
        foreach (get_object_vars($this) as $key => $value) {
            if ($key == 'username') {
                $this->$key = empty($value) ? '' : (string) $value;
            } else {
                $this->$key = empty($value) ? 0 : (int) $value;
            }
        }
    }
}
