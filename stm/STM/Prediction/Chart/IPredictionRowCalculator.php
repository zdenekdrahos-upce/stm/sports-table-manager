<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Prediction\Chart;

interface IPredictionRowCalculator
{
    public static function calculatePoints(&$row);
}
