<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Prediction\Chart;

use STM\Competition\Competition;
use STM\Entities\AbstractEntityFactory;

class PredictionChartFactory extends AbstractEntityFactory
{
    public function findAll(IPredictionRowCalculator $calculator)
    {
        $methods = array('setCalculator' => $calculator);
        return $this->entityHelper->setAndFindArray($methods);
    }

    public function findByCompetition(Competition $competition, IPredictionRowCalculator $calculator)
    {
        $methods = array(
            'setCalculator' => $calculator,
            'setCompetition' => $competition
        );
        return $this->entityHelper->setAndFindArray($methods);
    }

    public function find(array $matchSelection, IPredictionRowCalculator $calculator)
    {
        $methods = array(
            'setCalculator' => $calculator,
            'setMatchSelection' => $matchSelection
        );
        return $this->entityHelper->setAndFindArray($methods);
    }

    protected function useEntityClassAsDb()
    {
        return true;
    }

    protected function getEntitySelection()
    {
         return new PredictionChartSelection();
    }

    protected function getEntityClass()
    {
        return 'STM\Prediction\Chart\PredictionChart';
    }
}
