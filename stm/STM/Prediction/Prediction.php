<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Prediction;

use STM\DB\Object\DatabaseObject;
use STM\Match\Match;
use STM\Utils\Dates;
use STM\Match\MatchFactory;

/**
 * Prediction class
 * - instance: represents row from database table 'matches_predictions'
 * - static methods: for finding predictions, creating new prediction
 */
final class Prediction
{
    /** @var DatabaseObject */
    private static $db;

    /** @var int */
    private $id_match;
    /** @var int */
    private $id_user;
    /** @var int */
    private $score_home;
    /** @var int */
    private $score_away;
    /** @var boolean */
    private $isValid;

    /** @var Match */
    private $predicted_match;
    /** @var string home:away */
    private $match_name;
    /** @var int */
    private $match_score_home;
    /** @var int */
    private $match_score_away;

    private function __construct()
    {
    }

    public static function setORM(DatabaseObject $dbObject)
    {
        if (is_null(self::$db)) {
            self::$db = $dbObject;
        }
    }

    /**
     * Creates new row in database table 'matches_predictions' for specified $match and $user
     * @param array $attributes e.g. array('user' => 'instance of class User',
     * 'match' => 'instance of class Match', 'score_home' => 5, 'score_away' => '1')
     * @return boolean
     * Returns boolean to inform if creating operation was successful
     */
    public static function create($attributes)
    {
        if (PredictionValidator::checkCreate($attributes)) {
            return self::$db->insertValues(self::processAttributes($attributes));
        }
        return false;
    }

    /**
     * Processes array before creating - load id_match and id_user
     * @param array $attributes
     * @return array
     */
    private static function processAttributes($attributes)
    {
        $attributes['id_match'] = $attributes['match']->getId();
        unset($attributes['match']);
        $attributes['id_user'] = $attributes['user']->getId();
        unset($attributes['user']);
        if (STM_DB_TRIGGERS === false) {
            $attributes['last_modified'] = Dates::stringToDatabaseDate('now');
        }
        return $attributes;
    }


    // INSTANCE METHODS FOR ONE PREDICTION
    /**
     * Updates period scores in database
     * @param array $scores e.g. array('score_home' => 5, 'score_away' => '1')
     * @return boolean
     */
    public function updateScore($scores)
    {
        PredictionValidator::setPrediction($this);
        if (PredictionValidator::checkUpdateScore($scores)) {
            if (STM_DB_TRIGGERS === false) {
                $scores['last_modified'] = Dates::stringToDatabaseDate('now');
            }
            return self::$db->updateById(
                array('match' => $this->id_match, 'user' => $this->id_user),
                $scores
            );
        }
        return false;
    }

    /** @return int */
    public function getIdUser()
    {
        return (int) $this->id_user;
    }

    /** @return int */
    public function getIdMatch()
    {
        return (int) $this->id_match;
    }

    /** @return int */
    public function getScoreHome()
    {
        return (int) $this->score_home;
    }

    /** @return int */
    public function getScoreAway()
    {
        return (int) $this->score_away;
    }

    /** @return int|false */
    public function getMatchScoreHome()
    {
        return is_numeric($this->match_score_home) ? (int) $this->match_score_home : false;
    }

    /** @return int|false */
    public function getMatchScoreAway()
    {
        return is_numeric($this->match_score_away) ? (int) $this->match_score_away : false;
    }

    /** @return Match */
    public function getPredictedMatch()
    {
        if (is_null($this->predicted_match)) {
            $factory = new MatchFactory();
            $this->predicted_match = $factory->findById($this->id_match);
        }
        return $this->predicted_match;
    }

    /** @return boolean */
    public function isValid()
    {
        return (boolean) $this->is_valid;
    }

    /**
     * Method for templating. Returns prediction as an array
     * @return array
     */
    public function toArray()
    {
        return array(
            'id_match' => $this->getIdMatch(),
            'id_user' => $this->getIdUser(),
            'score_home' => $this->getScoreHome(),
            'score_away' => $this->getScoreAway(),
            'match_name' => $this->match_name,
            'match_score' => $this->getMatchScore(),
            'isValid' => $this->isValid()
        );
    }

    /** @return string */
    public function __toString()
    {
        return $this->score_home . ':' . $this->score_away;
    }

    /** @return string */
    private function getMatchScore()
    {
        if (is_numeric($this->match_score_home) && is_numeric($this->match_score_away)) {
            return "{$this->match_score_home}:{$this->match_score_away}";
        }
        return '-:-';
    }
}
