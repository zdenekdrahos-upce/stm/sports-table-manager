<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM;

use STM\Utils\Strings;
use \Datetime;
use \Exception;

class StmDatetime
{
    /** @var Datetime|false */
    private $datetime;
    /** @var boolean */
    private $isValid;

    /** @param string $datetime */
    public function __construct($datetime)
    {
        $this->datetime = false;
        if (Strings::isStringNonEmpty($datetime)) {
            try {
                $this->datetime = new Datetime($datetime);
            } catch (Exception $e) {
            }
        }
        $this->isValid = $this->datetime !== false;
    }

    public function getDatetime()
    {
        return $this->datetime;
    }

    public function isValid()
    {
        return $this->isValid;
    }

    public function __toString()
    {
        return $this->isValid ? $this->datetime->format(STM_DEFAULT_DATETIME_FORMAT) : '';
    }
}
