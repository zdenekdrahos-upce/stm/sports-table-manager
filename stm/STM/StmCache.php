<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM;

use STM\Competition\Competition;
use STM\Competition\Season\Season;
use STM\Match\MatchSelection;
use STM\Utils\Strings;
use STM\Libs\FileCache\Cache;
use STM\Cache\CacheHelper;
use STM\Cache\File\SerializationFileMatchPreview;
use STM\Cache\Loader\CompetitionPlayerStatsLoader;
use STM\Cache\Loader\MatchLoader;
use STM\Cache\Loader\MatchPreviewLoader;
use STM\Cache\Loader\SeasonTableLoader;
use STM\Cache\Loader\SeasonTeamsPositionsLoader;
use STM\Cache\Loader\TeamMatchesStatsLoader;

final class StmCache
{
    /**
     * @param int|Competition $competition instanceof Competition or competition ID
     * @return array Returns array of Match with loaded score and periods
     */
    public static function getAllCompetitionMatches($competition)
    {
        $match_selection = array(
            'matchType' => \STM\Match\MatchSelection::ALL_MATCHES,
            'loadScores' => true,
            'loadPeriods' => true,
            'competition' => $competition
        );
        $source = new MatchLoader($match_selection);
        $filename = $competition instanceof Competition ? $competition->getId() : $competition;
        $file = STM_CACHE_COMPETITION_MATCHES . $filename . '.txt';
        return CacheHelper::getContentFromCache($source, $file);
    }

    /**
     * @param int|Competition $competition  instanceof Competition or competition ID
     * @return \STM\Match\Stats\Action\MatchPlayerStats
     */
    public static function getCompetitionPlayerStatistics($competition)
    {
        $source = new CompetitionPlayerStatsLoader($competition);
        $filename = $competition instanceof Competition ? $competition->getId() : $competition;
        $file = STM_CACHE_COMPETITION_PLAYERS_STATS . $filename . '.txt';
        return CacheHelper::getContentFromCache($source, $file);
    }

    /**
     * @param Season|null $season
     * @return \STM\Competition\Season\Table\SeasonResultTable
     */
    public static function getSeasonTable($season)
    {
        $source = new SeasonTableLoader($season);
        $filename = $season instanceof Competition ? $season->getId() : $season;
        return CacheHelper::getContentFromCache($source, STM_CACHE_SEASON_TABLES . $filename . '.txt');
    }

    /**
     * @param Season|null $season
     * @return \STM\Competition\Season\Table\SeasonTeamsPositions
     */
    public static function getTeamsPositionsInTable($season)
    {
        $source = new SeasonTeamsPositionsLoader($season);
        $filename = $season instanceof Competition ? $season->getId() : $season;
        return CacheHelper::getContentFromCache(
            $source,
            STM_CACHE_SEASON_TEAM_POSITIONS . $filename . '.txt'
        );
    }

    /**
     * @param numeric $id_team
     * @return \STM\Match\Stats\TeamMatchesStats
     */
    public static function getStatisticsForTeam($id_team)
    {
        $ms = array(
            'matchType' => \STM\Match\MatchSelection::PLAYED_MATCHES,
            'loadScores' => true,
            'loadPeriods' => false,
            'teams' => array($id_team)
        );
        $file_id = "t-{$id_team}";
        return self::getTeamMatchesStatistics($ms, $id_team, $file_id);
    }

    /**
     * @param numeric $id_competition
     * @param numeric $id_team
     * @return \STM\Match\Stats\TeamMatchesStats
     */
    public static function getStatisticsForTeamInCompetition($id_competition, $id_team)
    {
        $ms = array(
            'matchType' => \STM\Match\MatchSelection::PLAYED_MATCHES,
            'loadScores' => true,
            'loadPeriods' => false,
            'competition' => $id_competition,
            'teams' => array($id_team)
        );
        $file_id = "t-{$id_team}-c-{$id_competition}";
        return self::getTeamMatchesStatistics($ms, $id_team, $file_id);
    }

    /**
     *
     * @param MatchSelection $match_selection
     * @param numeric $id_team
     * @param string $file_id
     * @return \STM\Match\Stats\TeamMatchesStats
     */
    public static function getTeamMatchesStatistics(array $match_selection, $id_team, $file_id)
    {
        if (is_numeric($id_team) && Strings::isStringNonEmpty($file_id)) {
            $source = new TeamMatchesStatsLoader($match_selection, $id_team);
            return CacheHelper::getContentFromCache($source, STM_CACHE . 'team-match-stats/' . $file_id . '.txt');
        }
        return false;
    }

    /**
     *
     * @param int $id_team
     * @return \STM\Match\Preview\MatchPreview|false
     */
    public static function getUpcomingMatchPreview($id_team)
    {
        if (is_int($id_team)) {
            $source = new MatchPreviewLoader($id_team);
            $path = STM_CACHE . 'upcoming-matches/' . $id_team . '.txt';
            $file = new SerializationFileMatchPreview($path);
            $cache = new Cache($source, $file);
            return $cache->display();
        }
        return false;
    }
}
