<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Entities;

use STM\Helpers\Selection;
use STM\DB\Query\QueryFactory;
use STM\Match\MatchSQL;
use STM\Match\MatchSelection;

abstract class AbstractEntitySelection
{
    /** @var \STM\DB\SQL\SelectQuery */
    protected $query;
    /** @var Selection */
    protected $selection;

    public function __construct()
    {
        $this->selection = new Selection();
        $this->query = $this->getQuery();
    }

    /** @return SelectQuery */
    public function getSelectQuery()
    {
        return $this->selection->getSelectQuery($this->query);
    }

    /** @return SelectQuery */
    public function getCountQuery()
    {
        return QueryFactory::selectCountInQuery($this->getSelectQuery());
    }

    /** @return Condition */
    public function getWhereCondition()
    {
        return $this->selection->getEscapedCondition();
    }

    public function reset()
    {
        $this->selection->reset();
        $this->query = $this->getQuery();
    }

    protected function addMatchSelectionFilter(MatchSelection $matchSelection, $tableName)
    {
        if ($matchSelection->isSetLimit()) {
            $this->query = MatchSQL::filterQueryByMatchSelection($this->query, $matchSelection, $tableName);
        } else {
            $this->selection->addCondition($matchSelection->getWhereCondition());
        }
    }

    abstract protected function getQuery();
}
