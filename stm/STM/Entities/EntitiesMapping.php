<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Entities;

class EntitiesMapping
{
    public static $MAPPING = array(
        'Attribute' => 'STM\Attribute\AttributeFactory',
        'Country' => 'STM\Country\CountryFactory',
        'Competition' => 'STM\Competition\CompetitionFactory',
        'CompetitionCategory' => 'STM\Competition\Category\CompetitionCategoryFactory',
        'Club' => 'STM\Club\ClubFactory',
        'ClubMember' => 'STM\Club\Member\ClubMemberFactory',
        'Match' => 'STM\Match\MatchFactory',
        'MatchAction' => 'STM\Match\Action\MatchActionFactory',
        'MatchDetail' => 'STM\Match\Detail\MatchDetailFactory',
        'MatchPlayerAction' => 'STM\Match\Action\Player\MatchPlayerActionFactory',
        'MatchPlayerStats' => 'STM\Match\Stats\Action\MatchPlayerStatsFactory',
        'MatchPlayer' => 'STM\Match\Lineup\MatchPlayerFactory',
        'MatchReferee' => 'STM\Match\Referee\MatchRefereeFactory',
        'MatchTeamAction' => 'STM\Match\Action\Team\MatchTeamActionFactory',
        'Person' => 'STM\Person\PersonFactory',
        'PersonAttribute' => 'STM\Person\Attribute\PersonAttributeFactory',
        'PersonPosition' => 'STM\Person\Position\PersonPositionFactory',
        'PlayoffSerie' => 'STM\Competition\Playoff\Serie\PlayoffSerieFactory',
        'Prediction' => 'STM\Prediction\PredictionFactory',
        'PredictionChart' => 'STM\Prediction\Chart\PredictionChartFactory',
        'PredictionStats' => 'STM\Prediction\Stats\PredictionStatsFactory',
        'Sport' => 'STM\Sport\SportFactory',
        'Stadium' => 'STM\Stadium\StadiumFactory',
        'Team' => 'STM\Team\TeamFactory',
        'TeamMember' => 'STM\Team\Member\TeamMemberFactory',
        'User' => 'STM\User\UserFactory',
    );
}
