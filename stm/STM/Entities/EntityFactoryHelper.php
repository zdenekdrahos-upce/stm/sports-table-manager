<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Entities;

use STM\Utils\Arrays;
use STM\DB\Database;
use STM\DB\Object\DatabaseObject;

class EntityFactoryHelper
{
    /** @var DatabaseObject */
    private $db;
    /** @var callable */
    private $dbCallbackMethod;
    /** @var \STM\Entities\AbstractEntitySelection */
    private $selection;

    public function __construct($entityClass, $dbClass)
    {
        if ($dbClass !== __CLASS__) {
            // if class loads or sets something else after basic object is
            // load, e.g. used in Match, where periods are loaded afterwards
            $this->dbCallbackMethod = array($dbClass, 'find');
        } else {
            // table and pk is not important, because object is used only
            // for reading without using methods for database table
            $this->db = new DatabaseObject(DT_ATTRIBUTES, array('id'));
            $this->db->setClass($entityClass);
            $this->db->setDatabase(Database::getDB());
            $this->dbCallbackMethod = array($this, 'find');
        }
    }

    public function setSelection(AbstractEntitySelection $selection)
    {
        $this->selection = $selection;
    }

    public function setFind()
    {
        $this->dbCallbackMethod[1] = 'find';
    }

    public function setCount()
    {
        $this->dbCallbackMethod[1] = 'count';
    }

    public function findById()
    {
        $this->selection->reset();
        call_user_func_array(array($this->selection, 'setId'), func_get_args());
        return $this->findObject();
    }

    public function findAll()
    {
        return $this->setAndFindArray(array());
    }

    public function setAndFindObject($methods)
    {
        $this->setupSelection($methods);
        return $this->findObject();
    }

    public function setAndFindArray($methods)
    {
        $this->setupSelection($methods);
        return $this->findArray();
    }

    public function findArray()
    {
        return call_user_func($this->dbCallbackMethod, $this->selection);
    }

    private function setupSelection($methods)
    {
        $this->selection->reset();
        foreach ($methods as $methodName => $argument) {
            $this->selection->{$methodName}($argument);
        }
    }

    private function findObject()
    {
        $result = $this->findArray();
        if ($this->dbCallbackMethod[1] == 'count') {
            return count($result) == 1 ? 1 : 0;
        } else {
            return Arrays::getObjectFromArray($result);
        }
    }

    private function find(AbstractEntitySelection $selection)
    {
        return $this->db->select($selection->getSelectQuery(), 'object', false);
    }

    private function count(AbstractEntitySelection $selection)
    {
        return $this->db->selectCountInQuery($selection->getCountQuery());
    }
}
