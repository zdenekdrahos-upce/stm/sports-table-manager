<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Entities;

class StmEntityFactory
{
    /** @var array */
    private $factories;
    /** @var string */
    private $factoryMethod;
    /** @var \STM\Entities\AbstractEntityFactory */
    private $currentFactory;

    public function __construct()
    {
        $this->factories = EntitiesMapping::$MAPPING;
        $this->factoryMethod = 'setFind';
    }

    public function useFindMethoud()
    {
        $this->factoryMethod = 'setFind';
    }

    public function useCountMethod()
    {
        $this->factoryMethod = 'setCount';
    }

    public function __get($name)
    {
        if ($this->isEntity($name)) {
            $this->loadFactory($name);
            $this->setFactoryMethod();
            return $this->currentFactory;
        } else {
            throw new InvalidArgumentException('Undefined entity');
        }
    }

    private function isEntity($name)
    {
        return array_key_exists($name, $this->factories);
    }

    private function loadFactory($name)
    {
        if (is_string($this->factories[$name])) {
            $className = $this->factories[$name];
            $this->factories[$name] = new $className();
        }
        $this->currentFactory = $this->factories[$name];
    }

    private function setFactoryMethod()
    {
        $this->currentFactory->{$this->factoryMethod}();
    }
}
