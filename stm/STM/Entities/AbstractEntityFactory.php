<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Entities;

abstract class AbstractEntityFactory
{
    /** @var EntityFactoryHelper */
    protected $entityHelper;

    public function __construct()
    {
        $entity = $this->getEntityClass();
        $database = $this->useEntityClassAsDb() ? $entity : 'STM\Entities\EntityFactoryHelper';
        $this->entityHelper = new EntityFactoryHelper($entity, $database);
        $this->entityHelper->setSelection($this->getEntitySelection());
    }

    public function setFind()
    {
        $this->entityHelper->setFind();
    }

    public function setCount()
    {
        $this->entityHelper->setCount();
    }

    protected function useEntityClassAsDb()
    {
        return false;
    }

    abstract protected function getEntityClass();

    abstract protected function getEntitySelection();
}
