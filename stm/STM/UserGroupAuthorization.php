<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM;

use STM\Web\Page\Page;
use STM\Web\Page\PageException;
use STM\DB\Object\DatabaseObject;
use STM\Session;
use STM\User\UserGroup;

/**
 * UserGroupAuthorization class
 * - used for finding if logged user or visitor has access to selected part of STM
 */
class UserGroupAuthorization
{
    /** @var DatabaseObject */
    private static $db;

    public static function setORM(DatabaseObject $dbObject)
    {
        if (is_null(self::$db)) {
            self::$db = $dbObject;
        }
    }

    /** @var Page */
    private $page;
    /** @var string */
    private $user_group;
    /** @var boolean */
    private $isAuthorized;

    public function __construct(Page $page)
    {
        $this->page = $page;
        $this->user_group = $this->getUserGroup();
        $this->isAuthorized = null;
    }

    /**
     * Checks if logged user (or visitor) has access to loaded page (index.php?arguments...)
     * @return boolean
     */
    public function isAuthorized()
    {
        if (is_null($this->isAuthorized)) {
            $this->checkAuthorization();
        }
        return $this->isAuthorized;
    }

    private function checkAuthorization()
    {
        if ($this->isUserProfile()) {
            $this->isAuthorized = true;
        } else {
            $this->isAuthorized = $this->getDbSettings() === 'Y';
        }
    }

    private function getDbSettings()
    {
        $primaryKey = array(
            'user_group' => $this->user_group,
            'module' => $this->page->getModuleForAuthorization()
        );
        $stdObject = self::$db->selectById($primaryKey);
        return $stdObject->{$this->page->getDatabaseColumn()};
    }

    /**
     * Checks if loaded page is user profile. Only owner of profile has access to
     * profile and he can change email/password or delete his account. Even admin
     * don't have access to all user profiles, but only to their profile.
     * (+ admin can delete account, reset password or change user group of all users)
     * Profile isn't checked in database
     * @return boolean
     */
    private function isUserProfile()
    {
        return $this->page->getModule() == 'profile';
    }

    /**
     * @return char
     * Returns id of user group of visitor (logged or unregistred user)
     */
    private function getUserGroup()
    {
        $session = Session::getInstance();
        $user_group = $session->isLoggedIn() ? $session->getUserGroup() : UserGroup::VISITOR;
        if (!$session->isLoggedIn()) {
            if (STM_ALLOW_VISITOR_ACCESS !== true) {
                $allowed_pages = array('type=login', 'type=about');
                if (STM_ALLOW_USER_REGISTRATION === true) {
                    $allowed_pages[] = 'type=registration';
                }
                if (!in_array($_SERVER['QUERY_STRING'], $allowed_pages)) {
                    throw new PageException('?type=login', stmLang('session', 'visitor-access'));
                }
            }
        }
        return UserGroup::getIdGroup($user_group);
    }
}
