<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Person;

use STM\DB\SQL\JoinType;
use STM\Entities\AbstractEntitySelection;

class PersonSelection extends AbstractEntitySelection
{

    public function setId($id_person)
    {
        if (is_numeric($id_person)) {
            $this->selection->setAttributeWithValue('persons.id_person', (string) $id_person);
        }
    }

    public function setForename($forename)
    {
        $this->selection->setAttributeLikeValue('persons.forename', $forename);
    }

    public function setSurname($surname)
    {
        $this->selection->setAttributeLikeValue('persons.surname', $surname);
    }

    public function setTeam($id_team)
    {
        if (is_numeric($id_team)) {
            $this->query->setJoinTables(PersonSQL::getJoinTeamsTable(JoinType::JOIN));
            $this->selection->setAttributeWithValue('team_persons.id_team', (string) $id_team);
        }
    }

    public function setPersonsWithoutTeam()
    {
        $this->query->setJoinTables(PersonSQL::getJoinTeamsTable(JoinType::LEFT));
        $this->selection->setNullAttribute('team_persons.id_person', true);
    }

    protected function getQuery()
    {
        return PersonSQL::selectPersons();
    }
}
