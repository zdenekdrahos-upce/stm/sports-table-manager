<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Person;

use STM\DB\Object\DatabaseObject;
use STM\StmDatetime;
use STM\Utils\Dates;

final class Person
{
    /** @var DatabaseObject */
    private static $db;

    /** @var int */
    private $id_person;
    /** @var string */
    private $forename;
    /** @var string */
    private $surname;
    /** @var mysql datetime */
    private $birth_date;
    /** @var string */
    private $characteristic;
    /** @var int */
    private $id_photo;
    /** @var int */
    private $id_country;

    /** @var string */
    private $name_country;

    private function __construct()
    {
    }

    public static function setORM(DatabaseObject $dbObject)
    {
        if (is_null(self::$db)) {
            self::$db = $dbObject;
        }
    }

    public static function create($attributes)
    {
        if (PersonValidator::checkCreate($attributes)) {
            if (self::$db->insertValues(self::processAttributes($attributes), true)) {
                return self::$db->getLastInsertId();
            }
        }
        return false;
    }

    public function delete()
    {
        if ($this->canBeDeleted()) {
            return self::$db->deleteById($this->id_person);
        }
        return false;
    }

    public function canBeDeleted()
    {
        $query = PersonSQL::selectIncidenceOfPerson($this);
        return self::$db->selectCountInQuery($query) == 0;
    }

    public function update($details)
    {
        PersonValidator::setPerson($this);
        if (PersonValidator::checkPersonUpdate($details)) {
            return self::$db->updateById($this->id_person, self::processAttributes($details));
        }
        return false;
    }

    public function getId()
    {
        return (int) $this->id_person;
    }

    public function getIdCountry()
    {
        return is_numeric($this->id_country) ? (int) $this->id_country : false;
    }

    public function getBirthDate()
    {
        return new StmDatetime($this->birth_date);
    }

    public function toArray()
    {
        return array(
            'forename' => $this->forename,
            'surname' => $this->surname,
            'birth_date' => $this->getBirthDate(),
            'characteristic' => $this->characteristic,
            'country' => $this->name_country
        );
    }

    public function __toString()
    {
        return $this->surname . ' ' . $this->forename;
    }

    private static function processAttributes($attributes)
    {
        $attributes['birth_date'] = Dates::stringToDatabaseDate($attributes['birth_date']);
        return $attributes;
    }
}
