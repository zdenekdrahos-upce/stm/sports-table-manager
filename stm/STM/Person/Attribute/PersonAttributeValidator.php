<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Person\Attribute;

use STM\Helpers\ObjectValidator;
use STM\Web\Message\FormError;
use STM\Person\Person;
use STM\Attribute\Attribute;
use STM\StmFactory;

/**
 * Class for validating attributes for database table 'PERSONS_ATTRIBUTES'
 * - attributes = value
 */
final class PersonAttributeValidator
{
    /** @var ObjectValidator */
    private static $validator;

    /** @param \STM\Libs\FormProcessor $formProcessor */
    public static function init($formProcessor = false)
    {
        if (is_null(self::$validator)) {
            self::$validator = new ObjectValidator('\STM\Person\Attribute\PersonAttribute');
        }
        self::$validator->setFormProcessor($formProcessor);
    }

    public static function setPersonAttribute($attribute)
    {
        self::$validator->setComparedObject($attribute);
    }

    public static function resetAttribute()
    {
        self::$validator->resetComparedObject();
    }

    public static function checkCreate($attributes)
    {
        if (self::$validator->checkCreate($attributes, array('person', 'attribute', 'value'))) {
            self::$validator->getFormProcessor()->checkCondition(
                $attributes['person'] instanceof Person,
                'Invalid person'
            );
            self::$validator->getFormProcessor()->checkCondition(
                $attributes['attribute'] instanceof Attribute,
                'Invalid attribute'
            );
            self::checkAttributeValue($attributes['value']);
            if (self::$validator->isValid()) {
                self::checkPersonAttribute($attributes['person'], $attributes['attribute']);
            }
        }
        return self::$validator->isValid();
    }

    public static function checkAttributeValue($new_value)
    {
        $old = self::$validator->getValueFromCompared('getValue');
        return self::$validator->checkString(
            $new_value,
            array('min' => 1, 'max' => 100),
            stmLang('person', 'attribute-value'),
            $old
        );
    }

    private static function checkPersonAttribute($person, $attribute)
    {
        $pa = StmFactory::find()->PersonAttribute->findById($person, $attribute);
        self::$validator->getFormProcessor()->checkCondition(
            !($pa instanceof PersonAttribute),
            FormError::get('unique-value', array(stmLang('person', 'attribute')))
        );
    }
}
