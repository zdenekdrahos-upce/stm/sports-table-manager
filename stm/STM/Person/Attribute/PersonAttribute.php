<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Person\Attribute;

use STM\DB\Object\DatabaseObject;
use STM\Person\Person;
use STM\Attribute\Attribute;

final class PersonAttribute
{
    /** @var DatabaseObject */
    private static $db;

    /** @var int */
    private $id_person;
    /** @var int */
    private $id_attribute;
    /** @var string */
    private $attribute;
    /** @var string */
    private $value;

    private function __construct()
    {
    }

    public static function setORM(DatabaseObject $dbObject)
    {
        if (is_null(self::$db)) {
            self::$db = $dbObject;
        }
    }

    public static function create($attributes)
    {
        if (PersonAttributeValidator::checkCreate($attributes)) {
            return self::$db->insertValues(self::processAttributes($attributes));
        }
        return false;
    }

    public static function deletePersonAttributes(Person $person)
    {
        $where = PersonAttributeSQL::getPersonDeleteCondition($person);
        return self::$db->deleteByCondition($where);
    }

    public static function deleteAttributeFromPersons(Attribute $attribute)
    {
        $where = PersonAttributeSQL::getAttributeDeleteCondition($attribute);
        return self::$db->deleteByCondition($where);
    }

    public function delete()
    {
        return self::$db->deleteById(array('person' => $this->id_person, 'id_attribute' => $this->id_attribute));
    }

    public function updateValue($newValue)
    {
        PersonAttributeValidator::setPersonAttribute($this);
        if (PersonAttributeValidator::checkAttributeValue($newValue)) {
            return self::$db->updateById(
                array('person' => $this->id_person, 'id_attribute' => $this->id_attribute),
                array('value' => $newValue)
            );
        }
        return false;
    }

    public function getIdPerson()
    {
        return (int) $this->id_person;
    }

    public function getIdAttribute()
    {
        return (int) $this->id_attribute;
    }

    public function getAttribute()
    {
        return $this->attribute;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function toArray()
    {
        return array(
            'attribute' => $this->attribute,
            'value' => $this->value
        );
    }

    public function __toString()
    {
        return $this->attribute . ' = ' . $this->value;
    }

    private static function processAttributes($attributes)
    {
        $result_array = array();
        $result_array['id_person'] = $attributes['person']->getId();
        $result_array['id_attribute'] = $attributes['attribute']->getId();
        $result_array['value'] = $attributes['value'];
        return $result_array;
    }
}
