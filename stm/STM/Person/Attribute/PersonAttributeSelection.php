<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Person\Attribute;

use STM\Entities\AbstractEntitySelection;
use STM\Person\Person;
use STM\Attribute\Attribute;

class PersonAttributeSelection extends AbstractEntitySelection
{

    public function setId(Person $person, Attribute $attribute)
    {
        $this->setPerson($person);
        $this->setAttribute($attribute);
    }

    public function setPerson(Person $person)
    {
        $this->selection->setAttributeLikeValue('persons_attributes.id_person', (string)$person->getId());
    }

    public function setAttribute(Attribute $attribute)
    {
        $this->selection->setAttributeLikeValue('persons_attributes.id_attribute', (string)$attribute->getId());
    }

    protected function getQuery()
    {
        return PersonAttributeSQL::selectPersonAttributes();
    }
}
