<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Person\Attribute;

use STM\Entities\AbstractEntityFactory;
use STM\Person\Person;
use STM\Attribute\Attribute;

class PersonAttributeFactory extends AbstractEntityFactory
{
    public function findById(Person $person, Attribute $attribute)
    {
        return $this->entityHelper->findById($person, $attribute);
    }

    public function findByPerson(Person $person)
    {
        $methods = array('setPerson' => $person);
        return $this->entityHelper->setAndFindArray($methods);
    }

    protected function getEntitySelection()
    {
         return new PersonAttributeSelection();
    }

    protected function getEntityClass()
    {
        return 'STM\Person\Attribute\PersonAttribute';
    }
}
