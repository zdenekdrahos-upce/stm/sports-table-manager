<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Person\Attribute;

use STM\DB\SQL\SelectQuery;
use STM\DB\SQL\JoinType;
use STM\DB\SQL\JoinTable;
use STM\DB\SQL\Condition;
use STM\Person\Person;
use STM\Attribute\Attribute;

class PersonAttributeSQL
{

    /** @return SelectQuery */
    public static function selectPersonAttributes()
    {
        $query = new SelectQuery(DT_PERSONS_ATTRIBUTES . ' persons_attributes');
        $query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                DT_ATTRIBUTES . ' attributes',
                'attributes.id_attribute = persons_attributes.id_attribute'
            )
        );
        return $query;
    }

    /** @return STM\DB\SQL\Condition */
    public static function getPersonDeleteCondition(Person $person)
    {
        return new Condition('id_person = {P}', array('P' => $person->getId()));
    }

    /** @return STM\DB\SQL\Condition */
    public static function getAttributeDeleteCondition(Attribute $attribute)
    {
        return new Condition('id_attribute = {A}', array('A' => $attribute->getId()));
    }
}
