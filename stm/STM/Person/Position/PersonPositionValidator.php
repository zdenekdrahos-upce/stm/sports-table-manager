<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Person\Position;

use STM\Helpers\ObjectValidator;
use STM\Web\Message\FormError;
use STM\Sport\Sport;

/**
 * Class for validating attributes for database table 'PERSON_POSITIONS'
 * - attributes = position (name), abbrevation, id_sport
 */
final class PersonPositionValidator
{
    /** @var ObjectValidator */
    private static $validator;

    /** @param \STM\Libs\FormProcessor $formProcessor */
    public static function init($formProcessor = false)
    {
        if (is_null(self::$validator)) {
            self::$validator = new ObjectValidator('\STM\Person\Position\PersonPosition');
        }
        self::$validator->setFormProcessor($formProcessor);
    }

    public static function setPosition($position)
    {
        self::$validator->setComparedObject($position);
    }

    public static function resetPosition()
    {
        self::$validator->resetComparedObject();
    }

    public static function checkCreate($attributes)
    {
        if (self::$validator->checkCreate($attributes, self::getAttributes())) {
            self::checkAttributes($attributes);
        }
        return self::$validator->isValid();
    }

    public static function checkUpdate($attributes)
    {
        if (self::$validator->checkUpdate($attributes, self::getAttributes())) {
            self::checkAttributes($attributes);
            if (self::$validator->isValid()) {
                $new_name = $attributes['position'] != self::$validator->getValueFromCompared('__toString');
                $new_sport = $attributes['sport']->getId() != self::$validator->getValueFromCompared('getIdSport');
                $new_abbrevation = $attributes['abbrevation'] !=
                    self::$validator->getValueFromCompared('getAbbrevation');
                self::$validator->getFormProcessor()->checkCondition(
                    $new_sport || $new_abbrevation || $new_name,
                    FormError::get('no-change', array(stmLang('person', 'position')))
                );
            }
        }
        return self::$validator->isValid();
    }

    private static function checkAttributes($attributes)
    {
        self::checkName($attributes['position']);
        self::checkAbbrevation($attributes['abbrevation']);
        self::checkSport($attributes['sport']);
    }

    private static function checkName($name)
    {
        if ($name != self::$validator->getValueFromCompared('__toString')) {
            self::$validator->checkUniqueName($name, array('min' => 2, 'max' => 30), '__toString');
        }
    }

    private static function checkAbbrevation($abbrevation)
    {
        self::$validator->checkString($abbrevation, array('min' => 1, 'max' => 5), stmLang('position', 'abbrevation'));
    }

    private static function checkSport($sport)
    {
        self::$validator->getFormProcessor()->checkCondition($sport instanceof Sport, 'Invalid sport');
    }

    private static function getAttributes()
    {
        return array('position', 'abbrevation', 'sport');
    }
}
