<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Person\Position;

use STM\DB\Object\DatabaseObject;

/**
 * PersonPosititon class
 * - instance: represents row from database table 'PERSON_POSITIONS'
 */
final class PersonPosition
{
    /** @var DatabaseObject */
    private static $db;

    /** @var int */
    private $id_position;
    /** @var string */
    private $position;
    /** @var string */
    private $abbrevation;
    /** @var int */
    private $id_sport;
    /** @var string */
    private $sport;

    private function __construct()
    {
    }

    public static function setORM(DatabaseObject $dbObject)
    {
        if (is_null(self::$db)) {
            self::$db = $dbObject;
        }
    }

    public static function create($attributes)
    {
        if (PersonPositionValidator::checkCreate($attributes)) {
            return self::$db->insertValues(self::processAttributes($attributes), true);
        }
        return false;
    }

    public static function exists($position_name)
    {
        return self::$db->existsItem('position', $position_name);
    }

    public function delete()
    {
        if ($this->canBeDeleted()) {
            return self::$db->deleteById($this->id_position);
        }
        return false;
    }

    public function canBeDeleted()
    {
        $query = PersonPositionSQL::selectIncidenceOfPersonPosition($this);
        return self::$db->selectCountInQuery($query) == 0;
    }

    public function update($attributes)
    {
        PersonPositionValidator::setPosition($this);
        if (PersonPositionValidator::checkUpdate($attributes)) {
            return self::$db->updateById($this->id_position, self::processAttributes($attributes));
        }
        return false;
    }

    public function getId()
    {
        return (int) $this->id_position;
    }

    public function getIdSport()
    {
        return (int) $this->id_sport;
    }

    public function getAbbrevation()
    {
        return $this->abbrevation;
    }

    public function toArray()
    {
        return array(
            'position' => $this->position,
            'abbrevation' => $this->abbrevation,
            'sport' => $this->sport
        );
    }

    public function __toString()
    {
        return $this->position;
    }

    private static function processAttributes($attributes)
    {
        $attributes['id_sport'] = $attributes['sport']->getId();
        unset($attributes['sport']);
        return $attributes;
    }
}
