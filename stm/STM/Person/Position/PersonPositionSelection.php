<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Person\Position;

use STM\Entities\AbstractEntitySelection;
use STM\Sport\Sport;

class PersonPositionSelection extends AbstractEntitySelection
{

    public function setId($isPosition)
    {
        if (is_numeric($isPosition)) {
            $this->selection->setAttributeWithValue('person_positions.id_position', (string) $isPosition);
        }
    }

    public function setSport(Sport $sport)
    {
        $this->selection->setAttributeLikeValue('person_positions.id_sport', (string) $sport->getId());
    }

    protected function getQuery()
    {
        return PersonPositionSQL::selectPersonPositions();
    }
}
