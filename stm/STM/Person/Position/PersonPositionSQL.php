<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Person\Position;

use STM\DB\SQL\SelectQuery;
use STM\DB\SQL\JoinTable;
use STM\DB\SQL\JoinType;
use STM\DB\SQL\Condition;
use STM\DB\Query\QueryFactory;

class PersonPositionSQL
{

    /** @return SelectQuery */
    public static function selectPersonPositions()
    {
        $query = new SelectQuery(DT_PERSON_POSITIONS . ' person_positions');
        $query->setColumns(
            'id_position, position, abbrevation, sports.id_sport, sports.sport'
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                DT_SPORTS . ' sports',
                'person_positions.id_sport = sports.id_sport'
            )
        );
        return $query;
    }

    /** @return SelectQuery */
    public static function selectIncidenceOfPersonPosition(PersonPosition $position)
    {
        $condition = new Condition('id_position = {I}', array('I' => $position->getId()));
        $tables = array(
            DT_TEAMS_PERSONS => $condition, DT_CLUBS_MEMBERS => $condition,
            DT_MATCH_REFEREES => $condition, DT_MATCH_PLAYERS => $condition
        );
        return QueryFactory::selectIncidence($tables);
    }
}
