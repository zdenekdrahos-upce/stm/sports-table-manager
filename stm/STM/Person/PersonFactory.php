<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Person;

use STM\Entities\AbstractEntityFactory;

final class PersonFactory extends AbstractEntityFactory
{
    public function findById($id)
    {
        return $this->entityHelper->findById($id);
    }

    public function findAll()
    {
        return $this->entityHelper->findAll();
    }

    /**
     * Filter array:
     * - if it's set 'no_team' key then filter for persons without team is activated
     * - if key 'forename' then filter for person forename is activated
     * - if key 'surname' then filter for person surname is activated
     * - if key 'id_team' then filter for membership in team is activated
     * @param array $filterArray
     * @return array
     */
    public function findByFilter($filterArray)
    {
        $methods = array();
        if (isset($filterArray['no_team'])) {
            $methods['setPersonsWithoutTeam'] = null;
        }
        if (isset($filterArray['forename'])) {
            $methods['setForename'] = $filterArray['forename'];
        }
        if (isset($filterArray['surname'])) {
            $methods['setSurname'] = $filterArray['surname'];
        }
        if (isset($filterArray['id_team'])) {
            $methods['setTeam'] = $filterArray['id_team'];
        }
        return $this->entityHelper->setAndFindArray($methods);
    }

    protected function getEntitySelection()
    {
         return new PersonSelection();
    }

    protected function getEntityClass()
    {
        return '\STM\Person\Person';
    }
}
