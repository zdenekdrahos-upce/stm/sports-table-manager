<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Person;

use STM\DB\SQL\SelectQuery;
use STM\DB\SQL\JoinType;
use STM\DB\SQL\JoinTable;
use STM\DB\SQL\Condition;
use STM\DB\Query\QueryFactory;

class PersonSQL
{

    /** @return SelectQuery */
    public static function selectPersons()
    {
        $query = new SelectQuery(DT_PERSONS . ' persons');
        $query->setColumns(
            'distinct persons.id_person, persons.forename, persons.surname,
            persons.birth_date, persons.characteristic, persons.id_photo,
            persons.id_country, countries.country as name_country'
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::LEFT,
                DT_COUNTRIES . ' countries',
                'persons.id_country = countries.id_country'
            )
        );
        $query->setOrderBy('surname, forename, id_person');
        return $query;
    }

    /** @return SelectQuery */
    public static function selectIncidenceOfPerson(Person $person)
    {
        $tables = array(
            DT_TEAMS_PERSONS => new Condition('id_person = {I}', array('I' => $person->getId())),
            DT_CLUBS_MEMBERS => new Condition('id_person = {I}', array('I' => $person->getId())),
            DT_MATCH_REFEREES => new Condition('id_referee = {I}', array('I' => $person->getId()))
        );
        return QueryFactory::selectIncidence($tables);
    }

    public static function getJoinTeamsTable($joinType)
    {
        return new JoinTable(
            $joinType,
            DT_TEAMS_PERSONS . ' team_persons',
            'persons.id_person = team_persons.id_person'
        );
    }
}
