<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Person;

use STM\Helpers\ObjectValidator;
use STM\Web\Message\FormError;
use STM\Country\Country;
use STM\StmFactory;

/**
 * Class for validating attributes for database table 'PERSONS'
 * - attributes = forename, surname, birth_date, characteristic, id_country
 */
final class PersonValidator
{
    /** @var ObjectValidator */
    private static $validator;

    /** @param \STM\Libs\FormProcessor $formProcessor */
    public static function init($formProcessor = false)
    {
        if (is_null(self::$validator)) {
            self::$validator = new ObjectValidator('\STM\Person\Person');
        }
        self::$validator->setFormProcessor($formProcessor);
    }

    public static function setPerson($person)
    {
        self::$validator->setComparedObject($person);
    }

    public static function resetPerson()
    {
        self::$validator->resetComparedObject();
    }

    public static function checkCreate($attributes)
    {
        if (self::$validator->checkCreate($attributes, self::getAttributes())) {
            self::checkFields($attributes);
        }
        return self::$validator->isValid();
    }

    public static function checkPersonUpdate($details)
    {
        if (self::$validator->checkUpdate($details, self::getAttributes())) {
            self::checkFields($details);
            self::checkChangeInPersonDetails($details);
        }
        return self::$validator->isValid();
    }

    private static function checkFields($attributes)
    {
        self::checkForename($attributes['forename']);
        self::checkSurname($attributes['surname']);
        self::checkBirthDate($attributes['birth_date']);
        self::checkCharacteristic($attributes['characteristic']);
        self::checkCountry($attributes['id_country']);
        return self::$validator->isValid();
    }

    private static function checkForename($forename)
    {
        self::$validator->checkString($forename, array('min' => 2, 'max' => 40), stmLang('person', 'forename'));
    }

    private static function checkSurname($surname)
    {
        self::$validator->checkString($surname, array('min' => 2, 'max' => 40), stmLang('person', 'surname'));
    }

    private static function checkBirthDate($birth_date)
    {
        self::$validator->checkOptionalDate($birth_date, stmLang('person', 'birth-date'));
    }

    private static function checkCharacteristic($characteristic)
    {
        self::checkOptionalString(
            $characteristic,
            array('min' => 5, 'max' => 250),
            stmLang('person', 'characteristic')
        );
    }

    private static function checkCountry($id_country)
    {
        if (!empty($id_country)) {
            self::$validator->getFormProcessor()->checkCondition(
                StmFactory::find()->Country->findById($id_country) instanceof Country,
                'Country must be existing country'
            );
        }
    }

    private static function checkOptionalString($string, $range, $error_name)
    {
        if ($string != '') {
            self::$validator->checkString($string, $range, $error_name);
        }
    }

    private static function checkChangeInPersonDetails($details)
    {
        $change = false;
        $current = self::getCurrentNonObjectAttributes();
        foreach ($current as $key => $value) {
            if ($value != $details[$key]) {
                $change = true;
                break;
            }
        }
        $change = self::checkChangeInObject($change, $details['id_country'], 'getIdCountry');
        self::$validator->getFormProcessor()->checkCondition(
            $change,
            FormError::get('no-change', array(stmLang('team', 'person')))
        );
    }

    private static function getCurrentNonObjectAttributes()
    {
        $current = self::$validator->getValueFromCompared('toArray');
        unset($current['country']);
        return $current;
    }

    private static function checkChangeInObject($change, $new_value, $getter)
    {
        if ($change == false) {
            if (!empty($new_value)) {
                $change = $new_value != self::$validator->getValueFromCompared($getter);
            } else {
                $change = is_int(self::$validator->getValueFromCompared($getter));
            }
        }
        return $change;
    }

    private static function getAttributes()
    {
        return array('forename', 'surname', 'birth_date', 'characteristic', 'id_country');
    }
}
