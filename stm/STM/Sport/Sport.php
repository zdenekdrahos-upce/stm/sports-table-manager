<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Sport;

use STM\DB\Object\DatabaseObject;

/**
 * Sport class
 * - instance: represents row from database table 'SPORTS'
 */
final class Sport
{
    /** @var DatabaseObject */
    private static $db;

    /** @var int */
    private $id_sport;
    /** @var string */
    private $sport;
    /** @var int */
    private $id_parent;

    /** @var string */
    private $parent_sport;
    /** @var int */
    private $count_childs;
    /** @var int */
    private $count_positions;
    /** @var int */
    private $count_match_actions;

    private function __construct()
    {
    }

    public static function setORM(DatabaseObject $dbObject)
    {
        if (is_null(self::$db)) {
            self::$db = $dbObject;
        }
    }

    public static function create($attributes)
    {
        if (SportValidator::checkCreate($attributes)) {
            return self::$db->insertValues(self::processAttributes($attributes), true);
        }
        return false;
    }

    public static function exists($sportName, $parentSport = null)
    {
        if (is_string($sportName)) {
            $selection = new SportSelection();
            $selection->setIncaseSensitiveSportName($sportName);
            if ($parentSport instanceof Sport) {
                $selection->setParentSport($parentSport);
            } else {
                $selection->setParentSportNull();
            }
            $factory = new SportFactory();
            $factory->setCount();
            return $factory->find($selection) > 0;
        }
        return false;
    }

    public function isSportFromCategory($sportName)
    {
        if ($this->count_childs > 0) {
            return self::exists($sportName, $this);
        }
        return false;
    }

    public function delete()
    {
        if ($this->canBeDeleted()) {
            return self::$db->deleteById($this->id_sport);
        }
        return false;
    }

    public function canBeDeleted()
    {
        return $this->count_match_actions == 0 && $this->count_positions == 0;
    }

    public function update($attributes)
    {
        SportValidator::setSport($this);
        if (SportValidator::checkUpdate($attributes)) {
            return self::$db->updateById($this->id_sport, self::processAttributes($attributes));
        }
        return false;
    }

    public function getId()
    {
        return (int) $this->id_sport;
    }

    public function getIdParent()
    {
        return !is_null($this->id_parent) ? (int) $this->id_parent : null;
    }

    public function toArray()
    {
        return array(
            'name' => $this->sport,
            'parent_sport' => $this->parent_sport,
            'count_childs' => $this->count_childs,
            'count_positions' => $this->count_positions,
            'count_match_actions' => $this->count_match_actions,
        );
    }

    public function __toString()
    {
        return $this->sport;
    }

    private static function processAttributes($attributes)
    {
        return array(
            'sport' => $attributes['sport'],
            'id_parent' => $attributes['parent_sport'] ? $attributes['parent_sport']->getId() : '',
        );
    }
}
