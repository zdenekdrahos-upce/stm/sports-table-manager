<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Sport;

use STM\Entities\AbstractEntitySelection;

class SportSelection extends AbstractEntitySelection
{

    public function setId($idSport)
    {
        if (is_numeric($idSport)) {
            $this->selection->setAttributeWithValue('sports.id_sport', (string) $idSport);
        }
    }

    public function setParentSport(Sport $sport)
    {
        $this->selection->setAttributeWithValue('sports.id_parent', (string) $sport->getId());
    }

    public function setParentSportNull()
    {
        $this->selection->setNullAttribute('sports.id_parent', true);
    }

    public function setIncaseSensitiveSportName($sportName)
    {
        $name = mb_strtolower($sportName, 'UTF-8');
        $this->selection->setAttributeWithValue('LOWER(sports.sport)', $name);
    }

    protected function getQuery()
    {
        return SportSQL::selectSports();
    }
}
