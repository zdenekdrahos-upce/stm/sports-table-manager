<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Sport;

use STM\Helpers\ObjectValidator;
use STM\Web\Message\FormError;

/**
 * Class for validating attributes for database table 'SPORTS'
 * - attributes = sport (name)
 */
final class SportValidator
{
    /** @var ObjectValidator */
    private static $validator;

    /** @param \STM\Libs\FormProcessor $formProcessor */
    public static function init($formProcessor = false)
    {
        if (is_null(self::$validator)) {
            self::$validator = new ObjectValidator('\STM\Sport\Sport');
        }
        self::$validator->setFormProcessor($formProcessor);
    }

    public static function setSport($sport)
    {
        self::$validator->setComparedObject($sport);
    }

    public static function resetSport()
    {
        self::$validator->resetComparedObject();
    }

    public static function checkCreate($attributes)
    {
        if (self::$validator->checkCreate($attributes, array('sport', 'parent_sport'))) {
            self::checkFields($attributes);
        }
        return self::$validator->isValid();
    }

    public static function checkUpdate($attributes)
    {
        if (self::$validator->checkUpdate($attributes, array('sport', 'parent_sport'))) {
            self::checkFields($attributes);
            self::checkChange($attributes);
        }
        return self::$validator->isValid();
    }

    private static function checkFields($attributes)
    {
        self::checkSportName($attributes['sport']);
        self::checkParentSport($attributes['parent_sport']);
        if (self::$validator->isValid()) {
            self::checkUniqueName($attributes['sport'], $attributes['parent_sport']);
        }
    }

    public static function checkSportName($new_name)
    {
        $error = stmLang('sport', 'sport');
        return self::$validator->checkString($new_name, array('min' => 2, 'max' => 50), $error);
    }

    public static function checkParentSport($sport)
    {
        self::$validator->getFormProcessor()->checkCondition(
            empty($sport) || $sport instanceof Sport,
            'Invalid sport'
        );
        return self::$validator->isValid();
    }

    private static function checkUniqueName($sport, $parentSport)
    {
        self::$validator->getFormProcessor()->checkCondition(
            !Sport::exists($sport, $parentSport),
            FormError::get('sport-unique')
        );
        if ($parentSport instanceof Sport && self::$validator->isSetComparedObject()) {
            self::$validator->getFormProcessor()->checkCondition(
                $parentSport->getId() != self::$validator->getValueFromCompared('getId'),
                FormError::get('category-parent')
            );
        }
    }

    private static function checkChange($attributes)
    {
        $new_name = $attributes['sport'] != self::$validator->getValueFromCompared('__toString');
        $current_parent = $attributes['parent_sport'] ? $attributes['parent_sport']->getId() : null;
        $new_parent = $current_parent != self::$validator->getValueFromCompared('getIdParent');
        self::$validator->getFormProcessor()->checkCondition(
            $new_name || $new_parent,
            FormError::get('no-change', array(stmLang('sport', 'sport')))
        );
    }
}
