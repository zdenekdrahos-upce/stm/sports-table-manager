<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Sport;

use STM\Entities\AbstractEntityFactory;

class SportFactory extends AbstractEntityFactory
{
    public function findById($id)
    {
        return $this->entityHelper->findById($id);
    }

    public function findSubcategories(Sport $sport)
    {
        $methods = array('setParentSport' => $sport);
        return $this->entityHelper->setAndFindArray($methods);
    }

    public function findAll()
    {
        return $this->entityHelper->findAll();
    }

    public function find(SportSelection $selection)
    {
        $this->entityHelper->setSelection($selection);
        return $this->entityHelper->findArray();
    }

    protected function getEntitySelection()
    {
         return new SportSelection();
    }

    protected function getEntityClass()
    {
        return 'STM\Sport\Sport';
    }
}
