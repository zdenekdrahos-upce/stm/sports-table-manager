<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Sport;

use STM\DB\Database;
use STM\DB\SQL\SelectQuery;
use STM\DB\SQL\JoinTable;
use STM\DB\SQL\JoinType;

class SportSQL
{

    /** @return SelectQuery */
    public static function selectSports()
    {
        $query = new SelectQuery(DT_SPORTS . ' sports');
        $null_function = Database::getNullFunction();
        $query->setColumns('sports.id_sport, sports.sport, sports.id_parent, parent.sport as parent_sport');
        $query->setColumns("{$null_function}(childs.count, 0) as count_childs");
        $query->setColumns("{$null_function}(positions.count, 0) as count_positions");
        $query->setColumns("{$null_function}(match_actions.count, 0) as count_match_actions");
        $query->setJoinTables(
            new JoinTable(
                JoinType::LEFT,
                DT_SPORTS . ' parent',
                'sports.id_parent = parent.id_sport'
            )
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::LEFT,
                '(' . self::selectCountSportSubcategories(DT_SPORTS, DT_SPORTS, 'id_parent')->generate() .
                ')childs',
                'sports.id_sport = childs.id_sport'
            )
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::LEFT,
                '(' . self::selectCountSportSubcategories(DT_SPORTS, DT_PERSON_POSITIONS)->generate() .
                ')positions',
                'sports.id_sport = positions.id_sport'
            )
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::LEFT,
                '(' . self::selectCountSportSubcategories(DT_SPORTS, DT_MATCH_ACTIONS)->generate() .
                ')match_actions',
                'sports.id_sport = match_actions.id_sport'
            )
        );
        $query->setOrderBy(
            'childs.count desc, positions.count desc, match_actions.count desc, sports.sport'
        );
        return $query;
    }

    /** @return SelectQuery */
    private static function selectCountSportSubcategories($table, $child_table, $child_id = 'id_sport')
    {
        $query = new SelectQuery($table . ' parent');
        $query->setColumns('parent.id_sport, count(*) as count');
        $query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                $child_table . ' childs',
                "parent.id_sport = childs.{$child_id}"
            )
        );
        $query->setGroupBy('parent.id_sport');
        Database::escapeQuery($query);
        return $query;
    }
}
