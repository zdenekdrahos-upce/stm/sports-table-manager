<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Utils;

final class Arrays
{
    /**
     * @param array $array
     * @return array/object/false
     * If $array is empty returns false. If array contains only 1 item then returns
     * this item, otherwise returns $array from argument
     */
    public static function checkArray($array)
    {
        if (empty($array)) {
            return false;
        } elseif (count($array) == 1) {
            return array_shift($array);
        } else {
            return $array;
        }
    }

    /**
     * Used in findById methods
     * @param array $array
     * @return mixed
     * If argument is array with one item then this item is returnes. If there are
     * more than one objects or array is empty then false is returned.
     */
    public static function getObjectFromArray($array)
    {
        if (is_array($array) && count($array) == 1) {
            return array_shift($array);
        }
        return false;
    }

    /**
     * Removes empty fields from array
     * @param array $array
     * @return array/boolean
     * Returns array. If argument is not array then returns false.
     */
    public static function cleanArray($array)
    {
        if (is_array($array)) {
            foreach ($array as $key => $value) {
                if (empty($value) && !is_numeric($value)) {
                    unset($array[$key]);
                }
            }
            return $array;
        }
        return false;
    }

    /**
     * Function for checking if associative array contains all needed keys
     * @param array $array e.g. array('min' => 5, 'max' = 10)
     * @param array $structure e.g. array('min', 'max')
     * @return boolean
     * Returns true $array contains all keys defined if $structure array and number of
     * items in $array must be same as in $structure
     */
    public static function isArrayValid($array, $structure)
    {
        if (is_array($array) && is_array($structure)) {
            if (count($array) === count($structure)) {
                foreach ($structure as $key) {
                    if (!array_key_exists($key, $array)) {
                        return false;
                    }
                }
                return true;
            }
        }
        return false;
    }

    public static function getAssocArray($objects, $method_for_key, $method_for_value = false)
    {
        $array = array();
        if (is_array($objects)) {
            foreach ($objects as $object) {
                if (is_object($object)) {
                    $value = $method_for_value ? $object->$method_for_value() : $object;
                    $key = $object->$method_for_key();
                } elseif (is_array($object)) {
                    $value = $method_for_value ? $object[$method_for_value] : $object;
                    $key = $object[$method_for_key];
                } else {
                    continue;
                }
                $array[$key] = $value;
            }
        }
        return $array;
    }

    public static function getNumberArray($min, $max)
    {
        $array = array();
        for ($i = $min; $i <= $max; $i++) {
            $array[$i] = $i;
        }
        return $array;
    }
}
