<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Utils;

final class Strings
{
    /**
     * Checks if argument is non empty string. String is
     * @param string $string
     * @return boolean
     * Returns true if trimmed string is non empty
     */
    public static function isStringNonEmpty($string)
    {
        return is_string($string) ? strlen(trim($string)) > 0 : false;
    }

    /**
     * Replaces {XYX} values with XYX values from array
     * @param string $string e.g. "STM is {TEXT}!"
     * @param string $array  e.g. array('TEXT' => 'Sports Table Manager');
     * @return string/false  e.g. "STM is Sports Table Manager!"
     */
    public static function replaceValues($string, $array)
    {
        if (Strings::isStringNonEmpty($string) && is_array($array)) {
            foreach ($array as $key => $value) {
                $string = str_replace('{' . strtoupper($key) . '}', $value, $string);
            }
            return $string;
        }
        return false;
    }


    public static function toCamelCase($string)
    {
        if (Strings::isStringNonEmpty($string)) {
            $stringWithSpaces = str_replace('_', ' ', $string);
            $sentence = ucwords($stringWithSpaces);
            $camelCase = str_replace(' ', '', $sentence);
            $camelCase[0] = strtolower($camelCase[0]);
            return $camelCase;
        }
        return '';
    }
}
