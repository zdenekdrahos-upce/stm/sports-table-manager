<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Utils;

use STM\StmDatetime;
use \Datetime;
use \Exception;

final class Dates
{
    /**
     * @param StmDatetime $datetime
     * @param string $error_message default: 'not-set' == stmLang('not-set')
     * @param string $format format for Datetime::format
     * @return string
     * Returns StmDatetime as string, formatted according $format. If $datetime is
     * invalid argument then $error_message is returned
     */
    public static function convertDatetimeToString(
        $datetime,
        $error_message = 'not-set',
        $format = STM_DEFAULT_DATETIME_FORMAT
    ) {
        if ($datetime instanceof StmDatetime && $datetime->isValid()) {
            if (Strings::isStringNonEmpty($format)) {
                return $datetime->getDatetime()->format($format);
            } else {
                return $datetime->__toString();
            }
        } else {
            return $error_message == 'not-set' ? stmLang('not-set') : $error_message;
        }
    }

    /**
     * Converts string (if contains date) into database date & time
     * @param string $string
     * @return string
     * Returns string as datetime for database (datetime for MySQL)
     * Format is YEAR-MONTH-DAY HOURS:MINUTES:SECONDS (e.g. 2011-09-02 09:30:47)
     * If $time is not string or $time is not valid $datetime then returns empty string
     */
    public static function stringToDatabaseDate($string)
    {
        $datetime = new StmDatetime($string);
        return self::datetimeToDatabaseDate($datetime);
    }

    public static function datetimeToDatabaseDate(StmDatetime $datetime)
    {
        if ($datetime->isValid()) {
            return $datetime->getDatetime()->format('Y-m-d H:i:s');
        }
        return '';
    }

    public static function timestampToDatabaseDate($timestamp)
    {
        if (is_int($timestamp)) {
            return date("Y-m-d H:i:s", $timestamp);
        }
        return '';
    }

    /**
     * Checks if $date is between start/end dates. If start and end date is not set
     * then it returns true.
     * @param string $date
     * @param StmDatetime $date_start
     * @param StmDatetime $date_end
     * @return boolean
     */
    public static function isDateInRange($date, StmDatetime $date_start, StmDatetime $date_end)
    {
        $date = new StmDatetime($date);
        if ($date->isValid()) {
            if ($date_start->isValid() && $date_end->isValid()) {
                return self::inOrder($date_start, $date) && self::inOrder($date, $date_end);
            } elseif ($date_start->isValid()) {
                return self::inOrder($date_start, $date);
            } elseif ($date_end->isValid()) {
                return self::inOrder($date, $date_end);
            } else {
                return true;
            }
        }
        return false;
    }

    /**
     * @param string $date_start
     * @param string $date_end
     * @return boolean
     */
    public static function areDatesInOrder($date_start, $date_end)
    {
        $date_start = new StmDatetime($date_start);
        $date_end = new StmDatetime($date_end);
        if ($date_start->isValid() && $date_end->isValid()) {
            return self::inOrder($date_start, $date_end);
        }
        return false;
    }

    public static function strtotime($string)
    {
        try {
            $datetime = new Datetime($string);
            return self::getTimestamp($datetime);
        } catch (Exception $e) {
            return false;
        }
    }

    private static function inOrder(StmDatetime $date_start, StmDatetime $date_end)
    {
        $diff = $date_start->getDatetime()->diff($date_end->getDatetime());
        return $diff->invert == 0;
    }

    private static function getTimestamp(Datetime $datetime)
    {
        return $datetime->getTimestamp();
    }
}
