<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Utils;

final class Classes
{
    /**
     * Finds if $constant is constant from class $class
     * @param string $class name of the class
     * @param string $constant constant
     * @return boolean
     */
    public static function isClassConstant($class, $constant)
    {
        if (is_string($class) && is_string($constant)) {
            $helper = new \ReflectionClass($class);
            return in_array($constant, $helper->getConstants());
        }
        return false;
    }
}
