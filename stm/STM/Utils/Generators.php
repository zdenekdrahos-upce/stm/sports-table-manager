<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Utils;

final class Generators
{
    /**
     * Generates password
     * @return string
     * Return a randomly generated string that is 10 characters long and contains
     * numbers (1-9) and small letters
     */
    public static function generatePassword()
    {
        $password = array();
        for ($i = 1; $i <= 10; $i++) {
            $is_even = rand(1, 9) % 2;
            $min = $is_even ? 49 : 97;
            $max = $is_even ? 57 : 122;
            $password[$i] = chr(rand($min, $max));
        }
        return implode('', $password);
    }

    /**
     * Calculate the sha1 salted hash of a string
     * @param string $string
     * @param string $salt
     * @return string
     * Returns the sha1 hash as a string.
     */
    public static function generateHash($string, $salt)
    {
        if (is_string($string) && is_string($salt)) {
            return hash_hmac('sha1', $string, $salt);
        }
        return null;
    }

    /**
     * Generates random token for CSRF protection
     * @return string
     */
    public static function generateRandomToken()
    {
        return hash_hmac('sha256', mt_rand() . self::generatePassword(), time());
    }
}
