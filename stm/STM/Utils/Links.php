<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Utils;

use STM\Libs\LanguageSwitch\Language;
use STM\Web\Page\Page;

final class Links
{
    public static function getPreviousPage($default_link)
    {
        if (self::isSetPreviousPage()) {
            return $_SERVER['HTTP_REFERER'];
        }
        return $default_link;
    }

    public static function isSetPreviousPage()
    {
        if (isset($_SERVER['HTTP_REFERER'])) {
            return self::isStmLink() && self::isDifferentStmPage();
        }
        return false;
    }

    private static function isStmLink()
    {
        return is_int(strpos($_SERVER['HTTP_REFERER'], self::getUrlForStmIndex()));
    }

    private static function getUrlForStmIndex()
    {
        $protocol = isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on" ? 'https' : 'http';
        $url = "{$protocol}://{$_SERVER['SERVER_NAME']}{$_SERVER['PHP_SELF']}";
        return str_replace('index.php', '', $url);
    }

    private static function isDifferentStmPage()
    {
        return !is_int(strpos($_SERVER['HTTP_REFERER'], $_SERVER['REQUEST_URI']));
    }

    public static function getHelpLink()
    {
        $page = Page::loadFromUrl();
        $lang = self::getLanguageShortcut();
        return STM_WEBSITE . "{$lang}/help/module/{$page->getHelpPath()}";
    }

    private static function getLanguageShortcut()
    {
        switch (Language::getSelectedLanguage()) {
            case 'czech':
                return 'cs';
            case 'english':
                return 'en';
            default:
                return 'en';
        }
    }
}
