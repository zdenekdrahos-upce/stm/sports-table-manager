<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Stadium;

use STM\DB\Query\QueryFactory;
use STM\Entities\AbstractEntitySelection;

class StadiumSelection extends AbstractEntitySelection
{

    public function setId($idStadium)
    {
        if (is_numeric($idStadium)) {
            $this->selection->setAttributeWithValue('id_stadium', (string) $idStadium);
        }
    }

    protected function getQuery()
    {
        return QueryFactory::selectAll(DT_STADIUMS);
    }
}
