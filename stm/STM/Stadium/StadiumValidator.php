<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Stadium;

use STM\Helpers\ObjectValidator;
use STM\Web\Message\FormError;
use STM\Utils\Arrays;

/**
 * Class for validating attributes for database table 'STADIUMS'
 * - attributes = name, capacity, map_link, field width/height
 */
final class StadiumValidator
{
    /** @var ObjectValidator */
    private static $validator;

    /** @param \STM\Libs\FormProcessor $formProcessor */
    public static function init($formProcessor = false)
    {
        if (is_null(self::$validator)) {
            self::$validator = new ObjectValidator('\STM\Stadium\Stadium');
        }
        self::$validator->setFormProcessor($formProcessor);
    }

    public static function setStadium($stadium)
    {
        self::$validator->setComparedObject($stadium);
    }

    public static function resetStadium()
    {
        self::$validator->resetComparedObject();
    }

    public static function checkCreate($attributes)
    {
        if (self::$validator->checkCreate($attributes, self::getAttributes())) {
            self::checkFields($attributes);
        }
        return self::$validator->isValid();
    }

    public static function checkUpdate($attributes)
    {
        if (self::$validator->checkUpdate($attributes, self::getAttributes())) {
            self::checkFields($attributes);
            if (self::$validator->isValid()) {
                self::checkChange($attributes);
            }
        }
        return self::$validator->isValid();
    }

    private static function checkFields($attributes)
    {
        self::checkName($attributes['name']);
        self::checkCapacity($attributes['capacity']);
        self::checkMapLink($attributes['map_link']);
        self::checkFieldSize(
            array(
                'field_width' => $attributes['field_width'],
                'field_height' => $attributes['field_height']
            )
        );
    }

    public static function checkName($new_name)
    {
        return self::$validator->checkString(
            $new_name,
            array('min' => 2, 'max' => 60),
            stmLang('name'),
            false
        );
    }

    public static function checkCapacity($new_capacity)
    {
        return self::$validator->checkNumberAndCompare(
            $new_capacity,
            array('min' => 1, 'max' => 999999),
            false,
            stmLang('stadium', 'capacity')
        );
    }

    public static function checkMapLink($new_map_link)
    {
        self::$validator->checkString(
            $new_map_link,
            array('min' => 4, 'max' => 150),
            stmLang('stadium', 'map-link'),
            false
        );
        if (self::$validator->isValid()) {
            self::$validator->getFormProcessor()->checkUrlAddress(
                $new_map_link,
                FormError::get('invalid-website', array(stmLang('stadium', 'map-link')))
            );
        }
        return self::$validator->isValid();
    }

    public static function checkFieldSize($dimensions)
    {
        self::$validator->getFormProcessor()->checkCondition(
            Arrays::isArrayValid($dimensions, array('field_width', 'field_height')),
            'Invalid format of field size array'
        );
        if (self::$validator->isValid()) {
            if (!empty($dimensions['field_width'])) {
                self::$validator->checkString(
                    $dimensions['field_width'],
                    array('min' => 1, 'max' => 20),
                    stmLang('stadium', 'field-width')
                );
            }
            if (!empty($dimensions['field_height'])) {
                self::$validator->checkString(
                    $dimensions['field_height'],
                    array('min' => 1, 'max' => 20),
                    stmLang('stadium', 'field-height')
                );
            }
        }
        return self::$validator->isValid();
    }

    private static function checkChange($attributes)
    {
        $is_different = false;
        $current_values = self::$validator->getValueFromCompared('toArray');
        foreach ($current_values as $key => $value) {
            $attribute_key = $key == 'stadium' ? 'name' : $key;
            if ($value != $attributes[$attribute_key]) {
                $is_different = true;
                break;
            }
        }
        self::$validator->getFormProcessor()->checkCondition(
            $is_different,
            FormError::get('no-change', array(stmLang('club', 'stadium')))
        );
    }

    private static function getAttributes()
    {
        return array('name', 'capacity', 'map_link', 'field_width', 'field_height');
    }
}
