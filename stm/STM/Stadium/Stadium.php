<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Stadium;

use STM\DB\Object\DatabaseObject;

final class Stadium
{
    /** @var DatabaseObject */
    private static $db;
    /** @var int */
    private $id_stadium;
    /** @var string */
    private $name;
    /** @var int */
    private $capacity;
    /** @var string */
    private $map_link;
    /** @var string */
    private $field_width;
    /** @var string */
    private $field_height;
    /** @var int */
    private $id_photo;

    private function __construct()
    {
    }

    public static function setORM(DatabaseObject $dbObject)
    {
        if (is_null(self::$db)) {
            self::$db = $dbObject;
        }
    }

    public static function create($attributes)
    {
        if (StadiumValidator::checkCreate($attributes)) {
            return self::$db->insertValues($attributes, true);
        }
        return false;
    }

    public function delete()
    {
        return self::$db->deleteById($this->id_stadium);
    }

    public function update($attributes)
    {
        StadiumValidator::setStadium($this);
        if (StadiumValidator::checkUpdate($attributes)) {
            return self::$db->updateById($this->id_stadium, $attributes);
        }
        return false;
    }

    public function getId()
    {
        return (int) $this->id_stadium;
    }

    public function getCapacity()
    {
        return (int) $this->capacity;
    }

    public function toArray()
    {
        return array(
            'stadium' => $this->name,
            'capacity' => $this->getCapacity(),
            'map_link' => $this->map_link,
            'field_width' => $this->field_width,
            'field_height' => $this->field_height,
        );
    }

    public function __toString()
    {
        return $this->name;
    }
}
