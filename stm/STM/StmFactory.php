<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM;

use STM\Entities\StmEntityFactory;

final class StmFactory
{
    /** @var StmEntityFactory */
    private static $findFactory;
    /** @var StmEntityFactory */
    private static $countFactory;

    /** @return StmEntityFactory */
    public static function find()
    {
        if (is_null(self::$findFactory)) {
            self::$findFactory = new StmEntityFactory();
            self::$findFactory->useFindMethoud();
        }
        return self::$findFactory;
    }

    /** @return StmEntityFactory */
    public static function count()
    {
        if (is_null(self::$countFactory)) {
            self::$countFactory = new StmEntityFactory();
            self::$countFactory->useCountMethod();
        }
        return self::$countFactory;
    }
}
