<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match;

final class ExtendedMatch
{
    /** @var Match */
    public $match;
    /** @var char - W,L,D,? */
    public $result;
    /** @var string - e.g. 2:2, -:- */
    public $score;

    public function __construct(Match $match, $id_selected_team)
    {
        $this->match = $match;
        $this->score = $match->hasScore() ? "{$match->getScoreHome()}:{$match->getScoreAway()}" : '-:-';
        $this->result = $this->getResult($id_selected_team);
    }

    private function getResult($id_selected_team)
    {
        if ($this->match->hasScore() && in_array(
            $id_selected_team,
            array($this->match->getIdTeamHome(), $this->match->getIdTeamAway())
        )) {
            if ($this->match->getScoreHome() == $this->match->getScoreAway()) {
                return 'D';
            } elseif ($this->match->getScoreHome() > $this->match->getScoreAway()) {
                return $id_selected_team == $this->match->getIdTeamHome() ? 'W' : 'L';
            } elseif ($this->match->getScoreHome() < $this->match->getScoreAway()) {
                return $id_selected_team == $this->match->getIdTeamAway() ? 'W' : 'L';
            }
        }
        return '?';
    }
}
