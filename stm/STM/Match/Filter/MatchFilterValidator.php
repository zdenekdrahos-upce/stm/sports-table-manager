<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Filter;

use STM\Helpers\ObjectValidator;
use STM\Web\Message\FormError;

final class MatchFilterValidator
{
    /** @var ObjectValidator */
    private static $validator;
    /** @var array */
    private static $attributes;

    /**
     * @param \STM\Libs\FormProcessor $formProcessor
     */
    public static function init($formProcessor = false)
    {
        if (is_null(self::$validator)) {
            self::$validator = new ObjectValidator('\STM\Match\Filter\MatchFilter');
        }
        self::$validator->setFormProcessor($formProcessor);
    }

    public static function setMatchFilter($match_filter)
    {
        self::$validator->setComparedObject($match_filter);
        if (self::$validator->isSetComparedObject()) {
            self::$attributes = self::$validator->getValueFromCompared('getAttributes');
        }
    }

    public static function resetMatchFilter()
    {
        self::$validator->resetComparedObject();
    }

    public static function check()
    {
        self::$validator->getFormProcessor()->checkCondition(
            self::$validator->isSetComparedObject(),
            'Match filter is not set'
        );
        if (self::$validator->isValid()) {
            self::checkMatchType();
            self::checkDates();
            self::checkRound();
            self::checkCompetition();
            self::checkTeams();
            self::checkMaxMatches();
        }
        return self::$validator->isValid();
    }

    public static function changed()
    {
        if (self::$validator->isSetComparedObject()) {
            $default = new MatchFilter();
            $attributes = $default->getAttributes();
            foreach ($attributes as $key => $value) {
                if ($value != self::$attributes[$key]) {
                    return true;
                }
            }
        }
        return false;
    }

    private static function checkMatchType()
    {
        $isValid = in_array(self::$attributes['match_type'], array('A', 'P', 'U'), true);
        self::$validator->getFormProcessor()->checkCondition(
            $isValid,
            FormError::get('invalid-value', array(stmLang('match', 'filter', 'match-selection'), 'A, P, U'))
        );
    }

    private static function checkDates()
    {
        self::$validator->checkOptionalDate(
            self::$attributes['date_start'],
            stmLang('competition', 'date-start')
        );
        self::$validator->checkOptionalDate(
            self::$attributes['date_end'],
            stmLang('competition', 'date-end')
        );
    }

    private static function checkRound()
    {
        if (self::$attributes['round']) {
            self::$validator->checkNumber(
                self::$attributes['round'],
                array('min' => 1, 'max' => 30),
                stmLang('match', 'season-round')
            );
        }
    }

    private static function checkCompetition()
    {
        if (self::$attributes['id_competition']) {
            self::$validator->getFormProcessor()->checkCondition(
                is_numeric(self::$attributes['id_competition']),
                'Invalid ID competition'
            );
        }
    }

    private static function checkTeams()
    {
        $isValid = true;
        foreach (array_keys(self::$attributes['teams']) as $team) {
            if (!is_numeric($team)) {
                $isValid = false;
                break;
            }
        }
        self::$validator->getFormProcessor()->checkCondition($isValid, 'Invalid ID team');
    }

    private static function checkMaxMatches()
    {
        $max = self::$attributes['result_table'] ? STM_MAX_MATCHES_FOR_TABLE : STM_MAX_MATCHES_FOR_LISTING;
        $count = self::$validator->getValueFromCompared('getCountMatches');
        self::$validator->getFormProcessor()->checkCondition(
            $count <= $max,
            FormError::get('match-filter-maxteams', array($max))
        );
    }
}
