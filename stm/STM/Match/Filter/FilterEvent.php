<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Filter;

use STM\Helpers\Event;

class FilterEvent
{
    /** Filtering in Matches */
    const TABLE = 'Result Table';
    /** Filtering in Matches */
    const MATCHES = 'Matches Listing';

    private function __construct()
    {
    }

    /**
     * Saves events related with Match and Period
     * @param MatchEvent $matchEvent constant from MatchEvent class
     * @param string $message description of event
     * @param boolean $result if event was sucessful or fail
     * @return boolean
     * Returns true if event was successfuly saved, otherwise false (bad arguments
     * or error during file saving)
     */
    public static function log($matchEvent, $message, $result)
    {
        $event = new Event('\STM\Match\Filter\FilterEvent', 'filter');
        $event->setMessage($matchEvent, $message, $result);
        return $event->logEvent();
    }
}
