<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Filter;

use STM\Match\MatchSelection;
use STM\Match\Table\ResultTable;
use STM\Match\Table\TablePoints;
use STM\Helpers\Pagination;
use STM\StmFactory;

final class MatchFilter
{
    private $match_type;
    private $load_score;
    private $load_periods;
    private $result_table;
    private $date_start;
    private $date_end;
    private $round;
    private $id_competition;
    private $head_to_head;
    private $teams;
    private $number_of_matches;
    private $rows_limit;

    public function __construct()
    {
        $this->match_type = substr(MatchSelection::PLAYED_MATCHES, 0, 1);
        $this->load_score = true;
        $this->load_periods = false;
        $this->result_table = false;
        $this->date_start = '';
        $this->date_end = '';
        $this->id_competition = '';
        $this->head_to_head = false;
        $this->teams = array();
        $this->rows_limit = array('max' => 15, 'offset' => 0);
    }

    public function getResult()
    {
        $ms = $this->getMatchSelection();
        if ($this->result_table) {
            return ResultTable::getTable($ms, new TablePoints(), STM_MATCH_PERIODS);
        } else {
            $ms['limit'] = array(
                'max' => $this->rows_limit['max'],
                'offset' => $this->rows_limit['offset']
            );
            return StmFactory::find()->Match->find($ms);
        }
    }

    public function getCountMatches()
    {
        if (is_null($this->number_of_matches)) {
            $selection = $this->getMatchSelection();
            $this->number_of_matches = StmFactory::count()->Match->find($selection);
        }
        return $this->number_of_matches;
    }

    private function getMatchSelection()
    {
        $ms = array(
            'matchType' => self::getMatchType($this->match_type),
            'loadScores' => $this->load_score,
            'loadPeriods' => $this->load_periods,
        );
        if (is_numeric($this->round)) {
            $ms['seasonRound'] = $this->round;
        }
        if (is_numeric($this->id_competition)) {
            $ms['competition'] = $this->id_competition;
        }
        if ($this->date_start || $this->date_end) {
            $ms['dates'] = array('min' => $this->date_start, 'max' => $this->date_end);
        }
        if ($this->teams) {
            $ms['teams'] = array_keys($this->teams);
            $ms['headToHead'] = $this->head_to_head ? true : false;
        }
        return $ms;
    }

    public function getAttributes()
    {
        return get_object_vars($this);
    }

    public function setPagination(Pagination $pagination)
    {
        $this->rows_limit = array(
            'max' => $pagination->getPerPage(),
            'offset' => $pagination->getOffset()
        );
    }

    public function initPostAndFilter()
    {
        $this->initTextValues(
            array('match_type', 'round', 'id_competition', 'date_start', 'date_end', 'teams')
        );
        $this->initBooleanValues(
            array('load_score', 'load_periods', 'result_table', 'head_to_head')
        );
    }

    private function initTextValues($names)
    {
        foreach ($names as $name) {
            $_POST[$name] = isset($_POST[$name]) ? $_POST[$name] : $this->$name;
            $this->$name = $_POST[$name];
        }
    }

    private function initBooleanValues($names)
    {
        foreach ($names as $name) {
            $this->$name = isset($_POST[$name]) || $this->$name == true;
            if ($this->$name) {
                $_POST[$name] = 'on';
            } else {
                unset($_POST[$name]);
            }
        }
    }

    public static function getMatchType($matchType)
    {
        if ($matchType == 'U') {
            return MatchSelection::UPCOMING_MATCHES;
        } elseif ($matchType == 'P') {
            return MatchSelection::PLAYED_MATCHES;
        } else {
            return MatchSelection::ALL_MATCHES;
        }
    }
}
