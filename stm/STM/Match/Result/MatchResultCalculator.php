<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Result;

use STM\Match\Period\Period;
use STM\Match\Match;
use STM\Match\Action\MatchAction;
use STM\Match\Stats\Action\StatisticCalculationType;
use STM\StmFactory;

/**
 * MatchResultCalculator class
 * - for calculating match result according to match actions
 * - and saving match score as new period in DB
 */
final class MatchResultCalculator
{
    /** @var Match */
    private $match;
    /** @var array */
    private $match_actions;
    /** @var array */
    private $score;

    public function __construct()
    {
        $this->match = null;
        $this->match_actions = array();
        $this->resetScore();
    }

    public function setMatch(Match $match)
    {
        $this->match = $match;
        $this->loadMatchActions();
    }

    /**
     * Calculates match score according to $match_action. Then score is saved to DB.
     * If match has existing score then at first current score is deleted.
     * PeriodValidator can contains errors from creating period (e.g. score number > 100)
     * @param MatchAction $match_action
     * @param StatisticCalculationTypeat $calculation_method
     */
    public function createNewMatchScore($match_action, $calculation_method)
    {
        MatchResultCalculatorValidator::setMatch($this->match);
        if (MatchResultCalculatorValidator::check($match_action, $calculation_method)) {
            $this->calculateScore($match_action, $calculation_method);
            $this->deleteExistingScore();
            return $this->saveScore();
        }
        return false;
    }

    public function getLastCalculatedScore()
    {
        return $this->score;
    }

    public function calculateScore(MatchAction $match_action, $calculation_method)
    {
        $this->resetScore();
        foreach ($this->match_actions as $action) {
            if ($action->getIdMatchAction() == $match_action->getId()) {
                $team = $this->match->getIdTeamHome() == $action->getIdTeam() ? 'home' : 'away';
                $this->score[$team] += $this->getAddedScore($action, $calculation_method);
            }
        }
    }

    private function getAddedScore($match_action, $calculation_method)
    {
        if ($calculation_method == StatisticCalculationType::SUM) {
            $temp = $match_action->toArray();
            if (is_numeric($temp['minute_action'])) {
                return (int) $temp['minute_action'];
            }
        }
        return 1;
    }

    private function deleteExistingScore()
    {
        if ($this->match->hasScore()) {
            Period::deleteMatchPeriods($this->match);
        }
    }

    private function saveScore()
    {
        $period = array(
            'match' => $this->match,
            'score_home' => $this->score['home'],
            'score_away' => $this->score['away'],
            'note' => null
        );
        return Period::create($period);
    }

    private function resetScore()
    {
        $this->score = array('home' => 0, 'away' => 0);
    }

    private function loadMatchActions()
    {
        $this->match_actions = StmFactory::find()->MatchPlayerAction->findByMatch($this->match);
    }
}
