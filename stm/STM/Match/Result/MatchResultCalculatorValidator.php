<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Result;

use STM\Helpers\ObjectValidator;
use STM\Web\Message\FormError;
use STM\Match\Action\MatchAction;
use STM\Utils\Classes;

/**
 * MatchResultCalculatorValidator class
 * - validating input for match result calculation
 */
final class MatchResultCalculatorValidator
{
    /** @var ObjectValidator */
    private static $validator;

    /** @param \STM\Libs\FormProcessor $formProcessor */
    public static function init($formProcessor = false)
    {
        if (is_null(self::$validator)) {
            self::$validator = new ObjectValidator('\STM\Match\Match');
        }
        self::$validator->setFormProcessor($formProcessor);
    }

    public static function setMatch($match)
    {
        self::$validator->setComparedObject($match);
    }

    public static function resetMatch()
    {
        self::$validator->resetComparedObject();
    }

    public static function check($match_action, $calculation_type)
    {
        self::$validator->getFormProcessor()->checkCondition(
            self::$validator->isSetComparedObject(),
            'Match is not set'
        );
        self::checkMatchAction($match_action);
        self::checkCalculationType($calculation_type);
        return self::$validator->isValid();
    }

    private static function checkMatchAction($match_action)
    {
        self::$validator->getFormProcessor()->checkCondition(
            $match_action !== false,
            FormError::get('empty-value', array(stmLang('match', 'calculate-score', 'calculation')))
        );
        if (self::$validator->isValid()) {
            self::$validator->getFormProcessor()->checkCondition(
                $match_action instanceof MatchAction,
                'Match action must be instance of class MatchAction.'
            );
        }
        return self::$validator->isValid();
    }

    private static function checkCalculationType($calculation_method)
    {
        self::$validator->getFormProcessor()->checkCondition(
            Classes::isClassConstant('\STM\Match\Stats\Action\StatisticCalculationType', $calculation_method),
            FormError::get('invalid-value', array(stmLang('settings', 'statistic-calculation'), 'string'))
        );
        return self::$validator->isValid();
    }
}
