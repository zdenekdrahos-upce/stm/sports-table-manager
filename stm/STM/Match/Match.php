<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match;

use STM\DB\Object\DatabaseObject;
use \Serializable;
use STM\StmDatetime;
use STM\Utils\Dates;
use STM\Match\Period\PeriodFactory;

/**
 * Match class
 * - instance: represents row from database table 'matches'
 * - static methods: for finding matches, creating new match
 */
final class Match implements Serializable
{
    /** @var DatabaseObject */
    private static $db;
    /** @var PeriodFactory */
    private static $periodFactory;

    // attributes from table matches (match in basic competition)
    /** @var int */
    private $id_match;
    /** @var int */
    private $id_competition;
    /** @var int */
    private $id_home;
    /** @var int */
    private $id_away;
    /** @var mysql datetime */
    private $datetime;
    // season attributes
    /** @var int */
    private $round;
    // playoff attributes
    /** @var int */
    private $id_serie;

    // score from table periods
    /** @var int */
    private $score_home;
    /** @var int */
    private $score_away;

    // team names (from table teams)
    /** @var string */
    private $team_home;
    /** @var string */
    private $team_away;

    /** @var array(Periods)*/
    private $periods;

    private function __construct()
    {
    }

    public static function setORM(DatabaseObject $dbObject)
    {
        if (is_null(self::$db)) {
            self::$db = $dbObject;
            self::$periodFactory = new PeriodFactory();
        }
    }

    /**
     * Finds and returns matches from table 'matches'. Loading periods is specified
     * in argument $match_selection
     * @param MatchSelection $matchSelection
     * @return array/false
     * Returns array of matches (even if only 1 or no match exists) or false
     */
    public static function find(MatchSelection $matchSelection)
    {
        $matches = self::$db->select($matchSelection->getSelectQuery());
        if (!empty($matches) && $matchSelection->loadPeriods()) {
            $periods = self::$periodFactory->find($matchSelection);
            $periods = self::transformPeriods($periods);
            return self::loadPeriods($matches, $periods);
        } else {
            return $matches;
        }
    }

    public static function count(MatchSelection $selection)
    {
        return self::$db->selectCountInQuery($selection->getCountQuery());
    }

    private static function transformPeriods(array $periods)
    {
        $match_periods = array();
        if (!empty($periods)) {
            foreach ($periods as $period) {
                $match_periods[$period->getIdMatch()][] = $period;
            }
        }
        return $match_periods;
    }

    /**
     * Add periods (if exist) to matches which where found by method find().
     * @param array $matches
     * @param array $period
     * @return array
     */
    private static function loadPeriods($matches, $periods)
    {
        foreach ($matches as $match) {
            if (array_key_exists($match->getId(), $periods)) {
                $match->periods = $periods[$match->getId()];
            } else {
                $match->periods = array();
            }
        }
        return $matches;
    }

    /**
     * Deletes all matches specified by the instance of MatchSelection.
     * LIMIT is NOT applied!!!
     * @param MatchSelection $matchSelection
     * @return boolean
     * Returns true exists matches from MatchSelection. It doesn't check if match
     * deleting operation was successful
     */
    public static function deleteMatches($matchSelection)
    {
        if ($matchSelection instanceof MatchSelection) {
            $condition = MatchSQL::getMatchesDeleteCondition($matchSelection->getWhereCondition());
            return self::$db->deleteByCondition($condition);
        }
        return false;
    }

    /**
     * Creates new row in database table 'matches'
     * @param array $attributes contains competition, home_team, away_team and datetime
     * @return int/boolean
     * Returns id of new match if creating was successful, otherwise false
     */
    public static function create($attributes)
    {
        if (MatchValidator::checkCreate($attributes)) {
            $match = self::$db->insertValues(self::processAttributes($attributes), true);
            return $match ? self::$db->getLastInsertId() : false;
        }
        return false;
    }

    /**
     * Processes array which contains instance of Team/Competition into array
     * which contains values which can be inserted into database (numbers = id)
     * @param array $attributes
     * @return array
     */
    private static function processAttributes($attributes)
    {
        $result_array = array();
        $result_array['id_home'] = $attributes['home_team']->getId();
        $result_array['id_away'] = $attributes['away_team']->getId();
        $result_array['datetime'] = Dates::stringToDatabaseDate($attributes['datetime']);
        if (isset($attributes['competition'])) {// in update competition cannot be changed
            $result_array['id_competition'] = $attributes['competition']->getId();
        }
        if ($attributes['round'] !== false) {
            $result_array['round'] = $attributes['round'];
        }
        if ($attributes['serie'] !== false) {
            $result_array['id_serie'] = $attributes['serie']->getId();
        }
        return $result_array;
    }


    // INSTANCE METHODS FOR ONE MATCH
    /**
     * Deletes match from database. If match has periods then periods are deleted too.
     * @return boolean
     */
    public function delete()
    {
        return self::$db->deleteById($this->id_match);
    }

    /**
     * Updates match attributes in database ( home_team, away_team, datetime, round, date)
     * @param array $attributes
     * @return boolean
     */
    public function update($attributes)
    {
        MatchValidator::setMatch($this);
        if (MatchValidator::checkUpdate($attributes)) {
            return self::$db->updateById($this->id_match, self::processAttributes($attributes));
        }
        return false;
    }

    /**
     * Checks if match was played (has filled rows in table periods)
     * @return boolean
     */
    public function hasPeriods()
    {
        if ($this->hasLoadedPeriods()) {
            return count($this->periods) >= 1;
        } else {
            self::$periodFactory->setCount();
            $count = self::$periodFactory->findByMatch($this);
            self::$periodFactory->setFind();
            return $count > 0;
        }
    }

    /**
     * Checks if match has loaded periods. E.g. match can have periods, but
     * loading periods was not selected in MatchSelection instance, so this method
     * returns false (hasPeriods returns true)
     * @return boolean
     */
    public function hasLoadedPeriods()
    {
        return is_array($this->periods);
    }

    /**
     * Checks if match has loaded scores
     * @return boolean
     */
    public function hasScore()
    {
        return is_int($this->getScoreHome()) && is_int($this->getScoreAway());
    }


    // GETTERS
    /**
     * Gets periods (instance of the class Period) for match
     * @return array/false
     * Returns false if periods was not loaded during finding match.
     */
    public function getPeriods()
    {
        if (is_array($this->periods)) {
            return $this->periods;
        }
        return false;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return (int) $this->id_match;
    }

    /**
     * @return int
     */
    public function getIdCompetition()
    {
        return (int) $this->id_competition;
    }

    /**
     * @return int
     */
    public function getIdTeamHome()
    {
        return (int) $this->id_home;
    }

    /**
     * @return int
     */
    public function getIdTeamAway()
    {
        return (int) $this->id_away;
    }

    /** @return StmDatetime */
    public function getDate()
    {
        return new StmDatetime($this->datetime);
    }

    /**
     * @return int/false
     */
    public function getScoreHome()
    {
        return is_numeric($this->score_home) ? (int) $this->score_home : false;
    }

    /**
     * @return int/false
     */
    public function getScoreAway()
    {
        return is_numeric($this->score_away) ? (int) $this->score_away : false;
    }

    /**
     * @return int/false
     */
    public function getRound()
    {
        return is_numeric($this->round) ? (int) $this->round : false;
    }

    /**
     * @return int/false
     */
    public function getIdSerie()
    {
        return is_numeric($this->id_serie) ? (int) $this->id_serie : false;
    }

    /**
     * Method for templating. Returns match as an array
     * @return array
     */
    public function toArray()
    {
        $base_array = array(
            'date' => $this->getDate(),
            'team_home' => $this->team_home,
            'team_away' => $this->team_away,
            'score_home' => $this->getScoreHome(),
            'score_away' => $this->getScoreAway(),
        );
        if ($this->getRound()) {
            $base_array['round'] = $this->getRound();
        }
        if ($this->getIdSerie()) {
            $base_array['id_serie'] = $this->getIdSerie();
        }
        return $base_array;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->team_home . ':' . $this->team_away;
    }

    public function serialize()
    {
        $vars = get_object_vars($this);
        return serialize($vars);
    }

    public function unserialize($serialized)
    {
        $vars = unserialize($serialized);
        foreach ($vars as $key => $value) {
            $this->$key = $value;
        }
    }
}
