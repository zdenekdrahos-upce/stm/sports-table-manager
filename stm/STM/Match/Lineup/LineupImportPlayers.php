<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Lineup;

use STM\Match\Filter\MatchFilter;
use STM\Person\Position\PersonPosition;
use STM\Competition\Competition;
use STM\Match\MatchSelection;
use STM\StmFactory;
use STM\DB\Object\DatabaseObject;
use STM\DB\SQL\MultipleInsertQuery;

/**
 * LineupImportPlayers class
 * - import all competition players to all matches in competition
 */
final class LineupImportPlayers
{
    /** @var DatabaseObject */
    private static $db;

    public static function setORM(DatabaseObject $dbObject)
    {
        if (is_null(self::$db)) {
            self::$db = $dbObject;
        }
    }

    /** @var Competition */
    private $competition;
    /** @var PersonPosition */
    private $personPosition;
    /** @var MatchSelection */
    private $matchType;
    /** @var array */
    private $players;
    /** @var array */
    private $importSummary;
    /** @var \STM\DB\SQL\MultipleInsertQuery */
    private $insertQuery;

    /**
     * @param Competition $competition
     * @param \STM\Person\Position\PersonPosition $position
     * @param MatchSelection $matchType constant from class
     * @return array
     * Returns summary with keys total_matches, created_players,
     * failed_players and existing_players
     */
    public function importPlayersToLineup(Competition $competition, $position, $matchType)
    {
        $this->resetSummary();
        if ($position instanceof PersonPosition) {
            $this->competition = $competition;
            $this->personPosition = $position;
            $this->insertQuery = new MultipleInsertQuery(DT_MATCH_PLAYERS);
            $this->loadMatchType($matchType);
            $this->loadTeamMembers();
            $this->importPlayers();
        }
        return $this->importSummary;
    }

    private function resetSummary()
    {
        $this->importSummary = array(
            'successful_import' => false,
            'total_matches' => 0,
            'created_players' => 0,
            'existing_players' => 0
        );
    }

    private function loadMatchType($matchType)
    {
        $this->matchType = MatchFilter::getMatchType($matchType);
        $this->matches = $this->getCompetitionMatches();
        $this->lineup = new LineupCompetition($this->competition);
    }

    private function loadTeamMembers()
    {
        $members = StmFactory::find()->TeamMember->findCompetitionPlayers($this->competition);
        $this->players = $this->categorizePlayersToTeams($members);
    }

    private function categorizePlayersToTeams($players)
    {
        $teamsWithPlayers = array();
        foreach ($players as $player) {
            if (!array_key_exists($player->getIdTeam(), $teamsWithPlayers)) {
                $teamsWithPlayers[$player->getIdTeam()] = array();
            }
            $teamsWithPlayers[$player->getIdTeam()][] = $player;
        }
        return $teamsWithPlayers;
    }

    private function importPlayers()
    {
        foreach ($this->matches as $match) {
            $matchMembers = $this->getAllMembersForMatch($match);
            foreach ($matchMembers as $team_member) {
                if ($this->lineup->existsPlayer($team_member->getId(), $match)) {
                    $this->importSummary['existing_players']++;
                } else {
                    $this->insertQuery->addAnotherAttributes($this->getMatchPlayerValues($match, $team_member));
                    $this->importSummary['created_players']++;
                }
            }
        }
        if ($this->importSummary['created_players'] > 0) {
            $this->importSummary['successful_import'] = self::$db->query($this->insertQuery);
        }
    }

    private function getCompetitionMatches()
    {
        $ms = array(
            'matchType' => $this->matchType,
            'loadScores' => false,
            'loadPeriods' => false,
            'competition' => $this->competition,
        );
        $matches = StmFactory::find()->Match->find($ms);
        $this->importSummary['total_matches'] = count($matches);
        return $matches;
    }

    private function getAllMembersForMatch($match)
    {
        $players = array();
        if (array_key_exists($match->getIdTeamHome(), $this->players)) {
            foreach ($this->players[$match->getIdTeamHome()] as $player) {
                $players[] = $player;
            }
        }
        if (array_key_exists($match->getIdTeamAway(), $this->players)) {
            foreach ($this->players[$match->getIdTeamAway()] as $player) {
                $players[] = $player;
            }
        }
        return $players;
    }

    private function getMatchPlayerValues($match, $teamMember)
    {
        return array(
            'id_team_player' => $teamMember->getId(),
            'id_match' => $match->getId(),
            'id_position' => $this->personPosition->getId(),
            'minute_in' => STM_MINUTE_IN,
            'minute_out' => STM_MINUTE_OUT
        );
    }
}
