<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Lineup;

use STM\Match\Match;
use STM\Competition\Competition;
use STM\Entities\AbstractEntitySelection;

class MatchPlayerSelection extends AbstractEntitySelection
{

    public function setId($idPlayer)
    {
        if (is_numeric($idPlayer)) {
            $this->selection->setAttributeWithValue('match_players.id_match_player', (string) $idPlayer);
        }
    }

    public function setMatch(Match $match)
    {
        $this->selection->setAttributeWithValue('match_players.id_match', (string)$match->getId());
    }

    public function setCompetition(Competition $competition)
    {
        $this->selection->setAttributeWithValue('matches.id_competition', (string)$competition->getId());
    }

    protected function getQuery()
    {
        return MatchPlayerSQL::selectMatchPlayers();
    }
}
