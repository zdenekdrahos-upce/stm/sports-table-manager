<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Lineup;

use STM\DB\Object\DatabaseObject;

/**
 * MatchPlayer class
 * - instance: represents row from database table 'MATCH_PLAYERS'
 * - loaded also in Lineups
 */
final class MatchPlayer
{
    /** @var DatabaseObject */
    private static $db;
    /** @var int */
    private $id_match_player;
    /** @var int */
    private $id_team_player;
    /** @var int */
    private $id_match;
    /** @var int */
    private $id_position;
    /** @var string */
    private $minute_in;
    /** @var string */
    private $minute_out;
    /** @var int */
    private $id_substitution_player;

    /** @var string */
    private $name_player;
    /** @var string */
    private $name_position;
    /** @var string */
    private $name_match;
    /** @var string */
    private $name_team;
    /** @var int */
    private $id_person;
    /** @var int */
    private $id_team;

    private function __construct()
    {
    }

    public static function setORM(DatabaseObject $dbObject)
    {
        if (is_null(self::$db)) {
            self::$db = $dbObject;
        }
    }

    public static function create($attributes)
    {
        if (MatchPlayerValidator::checkCreate($attributes)) {
            return self::$db->insertValues(self::processAttributes($attributes), true);
        }
        return false;
    }

    public function delete()
    {
        return self::$db->deleteById($this->id_match_player);
    }

    public function update($attributes)
    {
        MatchPlayerValidator::setMatchPlayer($this);
        if (MatchPlayerValidator::checkUpdate($attributes)) {
            $attributes['id_position'] = $attributes['position']->getId();
            unset($attributes['position']);
            return self::$db->updateById($this->id_match_player, $attributes);
        }
        return false;
    }

    public function getIdMatchPlayer()
    {
        return (int) $this->id_match_player;
    }

    public function getIdTeamPlayer()
    {
        return (int) $this->id_team_player;
    }

    public function getIdMatch()
    {
        return (int) $this->id_match;
    }

    public function getIdPosition()
    {
        return is_numeric($this->id_position) ? (int) $this->id_position : false;
    }

    public function getIdSubstitutionPlayer()
    {
        return is_numeric($this->id_substitution_player) ? (int) $this->id_substitution_player : false;
    }

    public function getIdPerson()
    {
        return (int) $this->id_person;
    }

    public function getIdTeam()
    {
        return (int) $this->id_team;
    }

    public function toArray()
    {
        return array(
            'player' => $this->name_player,
            'position' => $this->name_position,
            'match' => $this->name_match,
            'team' => $this->name_team,
            'minute_in' => $this->minute_in,
            'minute_out' => $this->minute_out,
        );
    }

    /** @return string */
    public function __toString()
    {
        return "{$this->name_team} - {$this->name_player} ({$this->name_position})";
    }

    private static function processAttributes($attributes)
    {
        $attributes['id_match'] = $attributes['match']->getId();
        unset($attributes['match']);
        $attributes['id_position'] = $attributes['position']->getId();
        unset($attributes['position']);
        $attributes['id_team_player'] = $attributes['team_player']->getId();
        unset($attributes['team_player']);
        return $attributes;
    }
}
