<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Lineup;

use STM\Helpers\ObjectValidator;
use STM\Web\Message\FormError;
use STM\Match\Match;
use STM\Person\Position\PersonPosition;
use STM\Team\Member\TeamMember;
use STM\StmFactory;

/**
 * Class for validating attributes for database table 'MATCH_PLAYERS'
 * - attributes = club, person, position, date_since, date_to
 */
final class MatchPlayerValidator
{
    /** @var ObjectValidator */
    private static $validator;

    /** @param \STM\Libs\FormProcessor $formProcessor */
    public static function init($formProcessor = false)
    {
        if (is_null(self::$validator)) {
            self::$validator = new ObjectValidator('\STM\Match\Lineup\MatchPlayer');
        }
        self::$validator->setFormProcessor($formProcessor);
    }

    public static function setMatchPlayer($match_player)
    {
        self::$validator->setComparedObject($match_player);
    }

    public static function resetMatchPlayer()
    {
        self::$validator->resetComparedObject();
    }

    public static function checkCreate($attributes)
    {
        if (self::$validator->checkCreate(
            $attributes,
            array('match', 'team_player', 'position', 'minute_in', 'minute_out', 'id_substitution_player')
        )) {
            self::checkFields($attributes, $attributes['match']);
        }
        return self::$validator->isValid();
    }

    public static function checkUpdate($attributes)
    {
        if (self::$validator->checkUpdate(
            $attributes,
            array('position', 'minute_in', 'minute_out', 'id_substitution_player')
        )) {
            $attributes['team_player'] = StmFactory::find()->TeamMember->findById(
                self::$validator->getValueFromCompared('getIdTeamPlayer')
            );
            $attributes['match'] = StmFactory::find()->Match->findById(
                self::$validator->getValueFromCompared('getIdMatch')
            );
            self::checkFields($attributes);
            if (self::$validator->isValid()) {
                self::checkChange($attributes);
            }
        }
        return self::$validator->isValid();
    }

    private static function checkFields($attributes)
    {
        $substitution = StmFactory::find()->MatchPlayer->findById($attributes['id_substitution_player']);
        self::checkMatch($attributes['match']);
        self::checkTeamPlayer($attributes['team_player']);
        self::checkPosition($attributes['position']);
        self::checkMinuteIn($attributes['minute_in']);
        self::checkMinuteOut($attributes['minute_out']);
        self::checkSubstitution($attributes['id_substitution_player'], $substitution);
        if (self::$validator->isValid()) {
            self::checkTeamPlayerAndMatch($attributes['team_player'], $attributes['match']);
            self::checkTeamPlayerAndSubstitution($attributes['team_player'], $substitution);
            self::checkSubstitutionPlayerAndMatch($substitution, $attributes['match']);
        }
    }

    private static function checkMatch($match)
    {
        self::$validator->getFormProcessor()->checkCondition($match instanceof Match, 'Invalid match');
    }

    private static function checkTeamPlayer($team_player)
    {
        self::$validator->getFormProcessor()->checkCondition(
            $team_player instanceof TeamMember,
            'Invalid player'
        );
        if (self::$validator->isValid()) {
            self::$validator->getFormProcessor()->checkCondition(
                $team_player->isTeamPlayer(),
                FormError::get('match-player')
            );
        }
    }

    private static function checkPosition($position)
    {
        self::$validator->getFormProcessor()->checkCondition(
            $position instanceof PersonPosition,
            'Invalid position'
        );
    }

    public static function checkMinuteIn($minute_in)
    {
        if (!empty($minute_in) || is_numeric($minute_in)) {
            self::$validator->checkString(
                $minute_in,
                array('min' => 1, 'max' => 20),
                stmLang('person', 'since')
            );
        }
    }

    public static function checkMinuteOut($minute_out)
    {
        if (!empty($minute_out) || is_numeric($minute_out)) {
            self::$validator->checkString(
                $minute_out,
                array('min' => 1, 'max' => 20),
                stmLang('person', 'to')
            );
        }
    }

    private static function checkSubstitution($id_substitution, $substitution)
    {
        if ($id_substitution) {
            self::$validator->getFormProcessor()->checkCondition(
                $substitution instanceof MatchPlayer,
                'Invalid substitution'
            );
            if (self::$validator->isValid() && self::$validator->isSetComparedObject()) {
                $is_different = $substitution->getIdMatchPlayer() !=
                    self::$validator->getValueFromCompared('getIdMatchPlayer');
                self::$validator->getFormProcessor()->checkCondition(
                    $is_different,
                    FormError::get('match-substitution-player')
                );
            }
        }
    }

    /**
     * @param TeamMember $team_player
     * @param Match $match
     */
    private static function checkTeamPlayerAndMatch($team_player, $match)
    {
        $id_team = $team_player->getIdTeam();
        $match_teams = array($match->getIdTeamHome(), $match->getIdTeamAway());
        self::$validator->getFormProcessor()->checkCondition(
            in_array($id_team, $match_teams, true),
            'Team must be in match'
        );
    }

    /**
     * @param TeamMember $team_player
     * @param MatchPlayer|false $substitution_player
     */
    private static function checkTeamPlayerAndSubstitution($team_player, $substitution_player)
    {
        if ($substitution_player) {
            $id_team_from_member = $team_player->getIdTeam();
            $id_team_from_substitution = $substitution_player->getIdTeam();
            self::$validator->getFormProcessor()->checkCondition(
                $id_team_from_member == $id_team_from_substitution,
                FormError::get('match-substitution-team')
            );
        }
    }

    /**
     * @param MatchPlayer $substitution_player
     * @param Match $match
     */
    private static function checkSubstitutionPlayerAndMatch($substitution_player, $match)
    {
        if ($substitution_player) {
            $is_same = $substitution_player->getIdMatch() == $match->getId();
            self::$validator->getFormProcessor()->checkCondition(
                $is_same,
                FormError::get('match-substitution-team')
            );
        }
    }

    private static function checkChange($attributes)
    {
        $current_aray = self::$validator->getValueFromCompared('toArray');
        $new_position = $attributes['position']->getId() !=
            self::$validator->getValueFromCompared('getIdPosition');
        $new_minute_in = $attributes['minute_in'] != $current_aray['minute_in'];
        $new_minute_out = $attributes['minute_out'] != $current_aray['minute_out'];
        $new_substitution = $attributes['id_substitution_player'] !=
            self::$validator->getValueFromCompared('getIdSubstitutionPlayer');
        $is_different = $new_position || $new_minute_in || $new_minute_out || $new_substitution;
        self::$validator->getFormProcessor()->checkCondition(
            $is_different,
            FormError::get('no-change', array(stmLang('team', 'player')))
        );
    }
}
