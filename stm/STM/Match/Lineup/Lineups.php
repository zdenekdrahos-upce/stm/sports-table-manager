<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Lineup;

use STM\Match\Match;
use STM\Utils\Arrays;

final class Lineups
{
    /** @var array key = id_team_member, value = MatchPlayer */
    private $all_match_players;
    /** @var array value = MatchPlayer */
    private $lineup_home;
    /** @var array value = MatchPlayer */
    private $lineup_away;

    public function __construct(Match $match)
    {
        $this->lineup_home = array();
        $this->lineup_away = array();
        $this->loadLineups($match);
    }

    public function getLineupHome()
    {
        return $this->lineup_home;
    }

    public function getLineupAway()
    {
        return $this->lineup_away;
    }

    public function getPlayer($id_team_member)
    {
        if (is_numeric($id_team_member) && array_key_exists($id_team_member, $this->all_match_players)) {
            return $this->all_match_players[$id_team_member];
        }
        return false;
    }

    public function getSubstitutionPlayer($id_match_player)
    {
        if (is_numeric($id_match_player)) {
            foreach ($this->all_match_players as $match_player) {
                if ($match_player->getIdMatchPlayer() == $id_match_player) {
                    return $match_player;
                }
            }
        }
        return false;
    }

    private function loadLineups($match)
    {
        $playerFactory = new MatchPlayerFactory();
        $matchPlayers = $playerFactory->findByMatch($match);
        foreach ($matchPlayers as $match_player) {
            if ($match_player->getIdTeam() == $match->getIdTeamHome()) {
                $this->lineup_home[] = $match_player;
            } else {
                $this->lineup_away[] = $match_player;
            }
        }
        $this->all_match_players = Arrays::getAssocArray($matchPlayers, 'getIdTeamPlayer');
    }
}
