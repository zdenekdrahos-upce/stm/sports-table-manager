<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Lineup;

use STM\Match\Match;
use STM\Competition\Competition;
use STM\Entities\AbstractEntityFactory;

class MatchPlayerFactory extends AbstractEntityFactory
{
    public function findById($id)
    {
        return $this->entityHelper->findById($id);
    }

    public function findByMatch(Match $match)
    {
        $methods = array('setMatch' => $match);
        return $this->entityHelper->setAndFindArray($methods);
    }

    public function findByCompetition(Competition $competition)
    {
        $methods = array('setCompetition' => $competition);
        return $this->entityHelper->setAndFindArray($methods);
    }

    protected function getEntitySelection()
    {
         return new MatchPlayerSelection();
    }

    protected function getEntityClass()
    {
        return 'STM\Match\Lineup\MatchPlayer';
    }
}
