<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Lineup;

use STM\DB\SQL\SelectQuery;
use STM\DB\SQL\JoinTable;
use STM\DB\SQL\JoinType;
use STM\DB\Query\QueryFactory;

class MatchPlayerSQL
{
    /** @return SelectQuery */
    public static function selectMatchPlayers()
    {
        $query = new SelectQuery(DT_MATCH_PLAYERS . ' match_players');
        $query->setColumns(
            "match_players.id_match_player, match_players.id_team_player,
            match_players.id_match, match_players.id_position, match_players.minute_in,
            match_players.minute_out, match_players.id_substitution_player,
            player.name_person as name_player, player.name_team, player.id_team,
            player.id_person, positions.position as name_position, matches.name_match"
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                DT_PERSON_POSITIONS . ' positions',
                'match_players.id_position = positions.id_position'
            )
        );
        // match name
        $match_query = QueryFactory::getMatchNameQuery();
        $query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                "({$match_query->generate()}) matches",
                'match_players.id_match = matches.id_match'
            )
        );
        // persons names
        $persons_query = QueryFactory::getTeamMemberNameQuery();
        $query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                "({$persons_query->generate()}) player",
                'match_players.id_team_player = player.id_team_player'
            )
        );
        $query->setOrderBy('player.id_team, positions.position, player.name_person');
        return $query;
    }
}
