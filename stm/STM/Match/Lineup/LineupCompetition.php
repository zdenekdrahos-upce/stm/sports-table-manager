<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Lineup;

use STM\Competition\Competition;
use STM\Match\Match;

/**
 * LineupCompetition class
 * Loads all match players from competition matches. Players are not classify
 * to home/away players, so only methods existsPlayer() can be used.
 * Used in importing team players to competition for check if player
 * doesn't already exist.
 */
final class LineupCompetition
{
    /** @var array key = id_team_member, value = MatchPlayer */
    private $allMatchPlayers;

    public function __construct(Competition $competition)
    {
        $this->allMatchPlayers = array();
        $this->loadLineups($competition);
    }

    public function existsPlayer($idTeamMember, Match $match)
    {
        if (is_numeric($idTeamMember)) {
            $key = $this->getKey($match->getId(), $idTeamMember);
            return array_key_exists($key, $this->allMatchPlayers);
        }
        return false;
    }

    private function loadLineups($competition)
    {
        $factory = new MatchPlayerFactory();
        $matchPlayers = $factory->findByCompetition($competition);
        foreach ($matchPlayers as $player) {
            $key = $this->getKey($player->getIdMatch(), $player->getIdTeamPlayer());
            $this->allMatchPlayers[$key] = $player;
        }
    }

    private function getKey($id_match, $id_team_player)
    {
        return $id_match . '-' . $id_team_player;
    }
}
