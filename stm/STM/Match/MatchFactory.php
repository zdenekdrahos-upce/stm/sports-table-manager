<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match;

use STM\Competition\Competition;
use STM\Competition\Playoff\Serie\PlayoffSerie;
use STM\Entities\AbstractEntityFactory;
use STM\Utils\Dates;
use STM\DB\Object\DatabaseObject;

final class MatchFactory extends AbstractEntityFactory
{
    /** @var DatabaseObject */
    private static $db;

    public static function setORM(DatabaseObject $dbObject)
    {
        if (is_null(self::$db)) {
            self::$db = $dbObject;
        }
    }

    /** @var \STM\Match\MatchSelectionBuilder */
    private $selectionBuilder;

    public function __construct()
    {
        parent::__construct();
        $this->selectionBuilder = new MatchSelectionBuilder();
    }

    public function findById($id)
    {
        return $this->entityHelper->findById($id);
    }

    public function findByCompetition($competition)
    {
        $method = $competition instanceof Competition ? 'setCompetition' : 'setCompetitionId';
        $methods = array($method => $competition);
        return $this->entityHelper->setAndFindArray($methods);
    }

    public function findByPlayoffSerie(PlayoffSerie $playoffSerie)
    {
        $methods = array(
            'setLoadingPeriods' => false,
            'setPlayoffSerie' => $playoffSerie
        );
        return $this->entityHelper->setAndFindArray($methods);
    }

    public function find(array $matchSelection)
    {
        $selection = $this->selectionBuilder->build($matchSelection);
        $this->entityHelper->setSelection($selection);
        return $this->entityHelper->findArray();
    }

    public function getLastPlayedMatch($idCompetition, $idTeam)
    {
        $query = MatchSQL::selectLastPlayedMatch(MatchSQL::getMatchCondition($idCompetition, $idTeam));
        return $this->getMatch($query);
    }

    public function getFirstUpcomingMatch($idCompetition, $idTeam)
    {
        $query = MatchSQL::selectFirstUpcomingMatch(MatchSQL::getMatchCondition($idCompetition, $idTeam));
        return $this->getMatch($query);
    }

    public function getDateOfLastModifiedMatch(array $selectionArray)
    {
        $matchSelection = $this->selectionBuilder->build($selectionArray);
        $query = MatchSQL::selectDateOfLastModifiedMatch($matchSelection->getWhereCondition());
        $result = self::$db->select($query, 'array', true);
        return $result['SEARCHED_DATE'] ? Dates::strtotime($result['SEARCHED_DATE']) : 0;
    }

    private function getMatch($query)
    {
        $result = self::$db->select($query);
        return array_shift($result);
    }

    protected function useEntityClassAsDb()
    {
        return true;
    }

    protected function getEntitySelection()
    {
         return new MatchSelection();
    }

    protected function getEntityClass()
    {
        return 'STM\Match\Match';
    }
}
