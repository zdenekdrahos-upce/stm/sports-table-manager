<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Action;

use STM\Entities\AbstractEntitySelection;
use STM\Sport\Sport;

class MatchActionSelection extends AbstractEntitySelection
{

    public function setId($id_person)
    {
        if (is_numeric($id_person)) {
            $this->selection->setAttributeWithValue('match_actions.id_match_action', (string) $id_person);
        }
    }

    public function setSport(Sport $sport)
    {
        $this->selection->setAttributeWithValue('match_actions.id_sport', (string) $sport->getId());
    }

    protected function getQuery()
    {
        return MatchActionSQL::selectMatchActions();
    }
}
