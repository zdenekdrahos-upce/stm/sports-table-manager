<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Action\Team;

use STM\Helpers\ObjectValidator;
use STM\Match\Match;
use STM\Web\Message\FormError;
use STM\Match\Action\MatchAction;
use STM\StmFactory;

/**
 * Class for validating attributes for database table 'MATCH_TEAM_STATISTICS'
 * - attributes = match, match_action, value_home, value_away
 */
final class MatchTeamActionValidator
{
    /** @var ObjectValidator */
    private static $validator;

    /** @param \STM\Libs\FormProcessor $formProcessor */
    public static function init($formProcessor = false)
    {
        if (is_null(self::$validator)) {
            self::$validator = new ObjectValidator('\STM\Match\Action\Team\MatchTeamAction');
        }
        self::$validator->setFormProcessor($formProcessor);
    }

    public static function setMatchTeamAction($match_team_action)
    {
        self::$validator->setComparedObject($match_team_action);
    }

    public static function resetMatchTeamAction()
    {
        self::$validator->resetComparedObject();
    }

    public static function checkCreate($attributes)
    {
        if (self::$validator->checkCreate(
            $attributes,
            array('match', 'match_action', 'value_home', 'value_away')
        )) {
            self::$validator->getFormProcessor()->checkCondition(
                $attributes['match'] instanceof Match,
                'Invalid match'
            );
            self::$validator->getFormProcessor()->checkCondition(
                $attributes['match_action'] instanceof MatchAction,
                'Invalid match action'
            );
            self::checkValueHome($attributes['value_home']);
            self::checkValueAway($attributes['value_away']);
            if (self::$validator->isValid()) {
                self::checkExistingTeamAction($attributes['match'], $attributes['match_action']);
            }
        }
        return self::$validator->isValid();
    }

    public static function checkUpdate($attributes)
    {
        if (self::$validator->checkUpdate($attributes, array('value_home', 'value_away'))) {
            self::checkValueHome($attributes['value_home']);
            self::checkValueAway($attributes['value_away']);
            self::checkChange($attributes);
        }
        return self::$validator->isValid();
    }

    private static function checkExistingTeamAction($match, $match_action)
    {
        $team_action = StmFactory::find()->MatchTeamAction->findById($match, $match_action);
        self::$validator->getFormProcessor()->checkCondition(
            $team_action == false,
            FormError::get('unique-value', array(stmLang('match', 'team-action')))
        );
    }

    private static function checkValueHome($value_home)
    {
        self::$validator->checkString($value_home, array('min' => 1, 'max' => 20), stmLang('match', 'home'));
    }

    private static function checkValueAway($value_away)
    {
        self::$validator->checkString($value_away, array('min' => 1, 'max' => 20), stmLang('match', 'away'));
    }

    private static function checkChange($values)
    {
        $arr = self::$validator->getValueFromCompared('toArray');
        $new_home = $values['value_home'] != $arr['value_home'];
        $new_away = $values['value_away'] != $arr['value_away'];
        self::$validator->getFormProcessor()->checkCondition(
            $new_away || $new_home,
            FormError::get('no-change', array(stmLang('match', 'team-action')))
        );
    }
}
