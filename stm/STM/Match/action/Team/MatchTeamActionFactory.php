<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Action\Team;

use STM\Entities\AbstractEntityFactory;
use STM\Match\Match;
use STM\Match\Action\MatchAction;

class MatchTeamActionFactory extends AbstractEntityFactory
{
    public function findById(Match $match, MatchAction $matchAction)
    {
        return $this->entityHelper->findById($match, $matchAction);
    }

    public function findByMatch(Match $match)
    {
        $methods = array('setMatch' => $match);
        return $this->entityHelper->setAndFindArray($methods);
    }

    public function findByAction(MatchAction $matchAction)
    {
        $methods = array('setMatchAction' => $matchAction);
        return $this->entityHelper->setAndFindArray($methods);
    }

    protected function getEntitySelection()
    {
         return new MatchTeamActionSelection();
    }

    protected function getEntityClass()
    {
        return 'STM\Match\Action\Team\MatchTeamAction';
    }
}
