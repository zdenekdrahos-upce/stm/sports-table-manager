<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Action\Team;

use STM\DB\Object\DatabaseObject;
use STM\Match\Action\MatchAction;

/**
 * MatchTeamAction class
 * - instance: represents row from database table 'MATCH_TEAM_STATISTICS'
 */
final class MatchTeamAction
{
    /** @var DatabaseObject */
    private static $db;

    /** @var int */
    private $id_match;
    /** @var int */
    private $id_match_statistic;
    /** @var string */
    private $value_home;
    /** @var string */
    private $value_away;

    /** @var string */
    private $name_statistic;

    private function __construct()
    {
    }

    public static function setORM(DatabaseObject $dbObject)
    {
        if (is_null(self::$db)) {
            self::$db = $dbObject;
        }
    }

    public static function create($attributes)
    {
        if (MatchTeamActionValidator::checkCreate($attributes)) {
            return self::$db->insertValues(self::processAttributes($attributes));
        }
        return false;
    }

    public static function deleteByAction(MatchAction $action)
    {
        $condition = MatchTeamActionSQL::getActionDeleteCondition($action);
        return self::$db->deleteByCondition($condition);
    }

    public function delete()
    {
        return self::$db->deleteById(array('match' => $this->id_match, 'action' => $this->id_match_statistic));
    }

    public function update($attributes)
    {
        MatchTeamActionValidator::setMatchTeamAction($this);
        if (MatchTeamActionValidator::checkUpdate($attributes)) {
            return self::$db->updateById(
                array('match' => $this->id_match, 'action' => $this->id_match_statistic),
                $attributes
            );
        }
        return false;
    }

    public function getIdMatch()
    {
        return (int) $this->id_match;
    }

    public function getIdMatchAction()
    {
        return (int) $this->id_match_statistic;
    }

    public function toArray()
    {
        return array(
            'value_home' => $this->value_home,
            'value_away' => $this->value_away,
            'name_action' => $this->name_statistic,
        );
    }

    public function __toString()
    {
        return "{$this->name_statistic} = {$this->value_home}:{$this->value_away}";
    }

    private static function processAttributes($attributes)
    {
        $attributes['id_match'] = $attributes['match']->getId();
        unset($attributes['match']);
        $attributes['id_match_statistic'] = $attributes['match_action']->getId();
        unset($attributes['match_action']);
        return $attributes;
    }
}
