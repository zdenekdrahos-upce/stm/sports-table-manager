<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Action\Team;

use STM\DB\SQL\SelectQuery;
use STM\DB\SQL\JoinTable;
use STM\DB\SQL\JoinType;
use STM\DB\SQL\Condition;
use STM\Match\Action\MatchAction;

class MatchTeamActionSQL
{

    /** @return SelectQuery */
    public static function selectMatchTeamActions()
    {
        $query = new SelectQuery(DT_MATCH_TEAM_ACTIONS . ' match_team_actions');
        $query->setColumns(
            'id_match, id_match_statistic, value_home, value_away,
             actions.match_action as name_statistic'
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                DT_MATCH_ACTIONS . ' actions',
                'match_team_actions.id_match_statistic = actions.id_match_action'
            )
        );
        return $query;
    }

    /** @return STM\DB\SQL\Condition */
    public static function getActionDeleteCondition(MatchAction $action)
    {
        return new Condition('id_match_statistic = {M}', array('M' => $action->getId()));
    }
}
