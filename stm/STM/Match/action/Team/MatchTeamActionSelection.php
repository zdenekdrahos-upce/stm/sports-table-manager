<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Action\Team;

use STM\Entities\AbstractEntitySelection;
use STM\Match\Match;
use STM\Match\Action\MatchAction;

class MatchTeamActionSelection extends AbstractEntitySelection
{

    public function setId(Match $match, MatchAction $matchAction)
    {
        $this->setMatch($match);
        $this->setMatchAction($matchAction);
    }

    public function setMatch(Match $match)
    {
        $this->selection->setAttributeLikeValue('match_team_actions.id_match', (string)$match->getId());
    }

    public function setMatchAction(MatchAction $matchAction)
    {
        $this->selection->setAttributeLikeValue(
            'match_team_actions.id_match_statistic',
            (string)$matchAction->getId()
        );
    }

    protected function getQuery()
    {
        return MatchTeamActionSQL::selectMatchTeamActions();
    }
}
