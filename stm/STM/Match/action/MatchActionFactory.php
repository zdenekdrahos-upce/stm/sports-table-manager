<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Action;

use STM\Entities\AbstractEntityFactory;
use STM\Sport\Sport;

class MatchActionFactory extends AbstractEntityFactory
{
    public function findById($id)
    {
        return $this->entityHelper->findById($id);
    }

    public function findBySport(Sport $sport)
    {
        $methods = array('setSport' => $sport);
        return $this->entityHelper->setAndFindArray($methods);
    }

    public function findAll()
    {
        return $this->entityHelper->findAll();
    }

    protected function getEntitySelection()
    {
         return new MatchActionSelection();
    }

    protected function getEntityClass()
    {
        return 'STM\Match\Action\MatchAction';
    }
}
