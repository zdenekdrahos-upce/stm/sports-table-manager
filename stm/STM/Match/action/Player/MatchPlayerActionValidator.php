<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Action\Player;

use STM\Helpers\ObjectValidator;
use STM\Web\Message\FormError;
use STM\Match\Lineup\MatchPlayer;
use STM\Match\Action\MatchAction;
use STM\StmFactory;

/**
 * Class for validating attributes for database table 'MATCH_PLAYER_ACTIONS'
 * - attributes = match_player, match_action, minute_action, action_comment
 */
final class MatchPlayerActionValidator
{
    /** @var ObjectValidator */
    private static $validator;

    /** @param \STM\Libs\FormProcessor $formProcessor */
    public static function init($formProcessor = false)
    {
        if (is_null(self::$validator)) {
            self::$validator = new ObjectValidator('\STM\Match\Action\Player\MatchPlayerAction');
        }
        self::$validator->setFormProcessor($formProcessor);
    }

    public static function setMatchPlayerAction($match_player_action)
    {
        self::$validator->setComparedObject($match_player_action);
    }

    public static function resetMatchPlayerAction()
    {
        self::$validator->resetComparedObject();
    }

    public static function checkCreate($attributes)
    {
        if (self::$validator->checkCreate(
            $attributes,
            array('match_player', 'match_action', 'minute_action', 'action_comment')
        )) {
            self::$validator->getFormProcessor()->checkCondition(
                $attributes['match_player'] instanceof MatchPlayer,
                'Invalid match player'
            );
            self::$validator->getFormProcessor()->checkCondition(
                $attributes['match_action'] instanceof MatchAction,
                'Invalid match action'
            );
            self::checkMinuteAction($attributes['minute_action']);
            self::checkActionComment($attributes['action_comment']);
            if (self::$validator->isValid()) {
                self::checkExistingAction(
                    $attributes['match_player'],
                    $attributes['match_action'],
                    $attributes['minute_action']
                );
            }
        }
        return self::$validator->isValid();
    }

    public static function checkUpdate($attributes)
    {
        if (self::$validator->checkUpdate($attributes, array('minute_action', 'action_comment'))) {
            self::checkMinuteAction($attributes['minute_action']);
            self::checkActionComment($attributes['action_comment']);
            if (self::$validator->isValid()) {
                self::checkChange($attributes);
                $arr = self::$validator->getValueFromCompared('toArray');
                if ($attributes['minute_action'] != $arr['minute_action']) {
                    $match_player = self::$validator->getValueFromCompared('getIdMatchPlayer');
                    $match_action = self::$validator->getValueFromCompared('getIdMatchAction');
                    $match_player = StmFactory::find()->MatchPlayer->findById($match_player);
                    $match_action = StmFactory::find()->MatchAction->findById($match_action);
                    self::checkExistingAction($match_player, $match_action, $attributes['minute_action']);
                }
            }
        }
        return self::$validator->isValid();
    }

    private static function checkMinuteAction($minute_in)
    {
        self::$validator->checkString(
            $minute_in,
            array('min' => 1, 'max' => 20),
            stmLang('match', 'player-action-minute')
        );
    }

    private static function checkActionComment($action_comment)
    {
        if (!empty($action_comment) || is_numeric($action_comment)) {
            self::$validator->checkString(
                $action_comment,
                array('min' => 1, 'max' => 200),
                stmLang('match', 'player-action-comment')
            );
        }
    }

    private static function checkChange($attributes)
    {
        $arr = self::$validator->getValueFromCompared('toArray');
        $new_minute = $attributes['minute_action'] != $arr['minute_action'];
        $new_comment = $attributes['action_comment'] != $arr['action_comment'];
        self::$validator->getFormProcessor()->checkCondition(
            $new_comment || $new_minute,
            FormError::get('no-change', array(stmLang('match', 'player-action')))
        );
    }

    private static function checkExistingAction($match_player, $match_action, $minute_action)
    {
        $action = StmFactory::find()->MatchPlayerAction->findById($match_player, $match_action, $minute_action);
        self::$validator->getFormProcessor()->checkCondition(
            $action == false,
            FormError::get('unique-value', array(stmLang('match', 'player-action')))
        );
    }
}
