<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Action\Player;

use STM\DB\SQL\SelectQuery;
use STM\DB\SQL\JoinTable;
use STM\DB\SQL\JoinType;
use STM\DB\SQL\Condition;
use STM\DB\Query\QueryFactory;
use STM\Match\Action\MatchAction;

class MatchPlayerActionSQL
{

    /** @return SelectQuery */
    public static function selectMatchPlayerActions()
    {
        $query = new SelectQuery(DT_MATCH_PLAYER_ACTIONS . ' match_player_actions');
        $query->setColumns(
            "match_player_actions.id_match_player, match_player_actions.id_match_action,
            match_player_actions.minute_action, match_player_actions.action_comment,
            actions.match_action as name_action, matches.name_match, matches.id_match,
            player.id_person, player.name_person, player.id_team, player.name_team, player.id_team_player"
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                DT_MATCH_PLAYERS . ' match_players',
                'match_player_actions.id_match_player = match_players.id_match_player'
            )
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                DT_MATCH_ACTIONS . ' actions',
                'match_player_actions.id_match_action = actions.id_match_action'
            )
        );
        // match name
        $match_query = QueryFactory::getMatchNameQuery();
        $query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                "({$match_query->generate()}) matches",
                'match_players.id_match = matches.id_match'
            )
        );
        // persons names
        $persons_query = QueryFactory::getTeamMemberNameQuery();
        $query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                "({$persons_query->generate()}) player",
                'match_players.id_team_player = player.id_team_player'
            )
        );
        $query->setOrderBy(
            'player.id_team, match_player_actions.id_match_player, actions.match_action,
             match_player_actions.minute_action'
        );
        return $query;
    }

    /** @return STM\DB\SQL\Condition */
    public static function getActionDeleteCondition(MatchAction $action)
    {
        return new Condition('id_match_action = {M}', array('M' => $action->getId()));
    }
}
