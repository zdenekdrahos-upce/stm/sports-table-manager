<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Action\Player;

use STM\DB\Object\DatabaseObject;
use STM\Match\Action\MatchAction;

/**
 * MatchPlayerAction class
 * - instance: represents row from database table 'MATCH_PLAYER_ACTIONS'
 */
final class MatchPlayerAction
{
    /** @var DatabaseObject */
    private static $db;
    /** @var int */
    private $id_match_player;
    /** @var int */
    private $id_match_action;
    /** @var string */
    private $minute_action;
    /** @var string */
    private $action_comment;

    /** @var string */
    private $name_action;
    /** @var int */
    private $id_match;
    /** @var string */
    private $name_match;
    /** @var int */
    private $id_person;
    /** @var string */
    private $name_person;
    /** @var int */
    private $id_team;
    /** @var string */
    private $name_team;
    /** @var int */
    private $id_team_player;

    private function __construct()
    {
    }

    public static function setORM(DatabaseObject $dbObject)
    {
        if (is_null(self::$db)) {
            self::$db = $dbObject;
        }
    }

    public static function create($attributes)
    {
        if (MatchPlayerActionValidator::checkCreate($attributes)) {
            return self::$db->insertValues(self::processAttributes($attributes));
        }
        return false;
    }

    public static function deleteByAction(MatchAction $action)
    {
        $condition = MatchPlayerActionSQL::getActionDeleteCondition($action);
        return self::$db->deleteByCondition($condition);
    }

    public function delete()
    {
        return self::$db->deleteById($this->getPrimaryKey());
    }

    public function update($attributes)
    {
        MatchPlayerActionValidator::setMatchPlayerAction($this);
        if (MatchPlayerActionValidator::checkUpdate($attributes)) {
            return self::$db->updateById($this->getPrimaryKey(), $attributes);
        }
        return false;
    }

    private function getPrimaryKey()
    {
        return array(
            'player' => $this->id_match_player,
            'action' => $this->id_match_action,
            'minute' => $this->minute_action
        );
    }

    public function getIdMatchPlayer()
    {
        return (int) $this->id_match_player;
    }

    public function getIdMatchAction()
    {
        return (int) $this->id_match_action;
    }

    public function getIdMatch()
    {
        return (int) $this->id_match;
    }

    public function getIdPerson()
    {
        return (int) $this->id_person;
    }

    public function getIdTeam()
    {
        return (int) $this->id_team;
    }

    public function getIdTeamPlayer()
    {
        return (int) $this->id_team_player;
    }

    public function toArray()
    {
        return array(
            'name_action' => $this->name_action,
            'minute_action' => $this->minute_action,
            'action_comment' => $this->action_comment,
            'name_player' => $this->name_person,
            'name_match' => $this->name_match,
            'name_team' => $this->name_team
        );
    }

    /** @return string */
    public function __toString()
    {
        return "{$this->name_person} ({$this->name_action}, {$this->minute_action})";
    }

    private static function processAttributes($attributes)
    {
        $new = array();
        $new['id_match_player'] = $attributes['match_player']->getIdMatchPlayer();
        $new['id_match_action'] = $attributes['match_action']->getId();
        $new['minute_action'] = $attributes['minute_action'];
        $new['action_comment'] = $attributes['action_comment'];
        return $new;
    }
}
