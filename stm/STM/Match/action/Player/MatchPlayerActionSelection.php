<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Action\Player;

use STM\Team\Team;
use STM\Person\Person;
use STM\Team\Member\TeamMember;
use STM\Match\Lineup\MatchPlayer;
use STM\Match\Action\MatchAction;
use STM\Match\Match;
use STM\Entities\AbstractEntitySelection;

class MatchPlayerActionSelection extends AbstractEntitySelection
{

    /** @param Match $match  */
    public function setId(MatchPlayer $match_player, MatchAction $match_action, $minute_action)
    {
        if (is_string($minute_action)) {
            $this->selection->setAttributeWithValue(
                'match_player_actions.id_match_player',
                (string) $match_player->getIdMatchPlayer()
            );
            $this->selection->setAttributeWithValue(
                'match_player_actions.id_match_action',
                (string) $match_action->getId()
            );
            $this->selection->setAttributeWithValue(
                'match_player_actions.minute_action',
                $minute_action
            );
        }
    }

    /** @param Match $match  */
    public function setMatch(Match $match)
    {
        $this->selection->setAttributeWithValue('match_players.id_match', (string) $match->getId());
    }

    /** @param Team $team  */
    public function setTeam(Team $team)
    {
        $this->selection->setAttributeWithValue('player.id_team', (string) $team->getId());
    }

    /** @param \STM\Team\Member\TeamMember $teamPlayer  */
    public function setTeamPlayer(TeamMember $teamPlayer)
    {
        $this->selection->setAttributeWithValue('player.id_team_player', (string) $teamPlayer->getId());
    }

    /** @param \STM\Person\Person $person  */
    public function setPerson(Person $person)
    {
        $this->selection->setAttributeWithValue('player.id_person', (string) $person->getId());
    }

    protected function getQuery()
    {
        return MatchPlayerActionSQL::selectMatchPlayerActions();
    }
}
