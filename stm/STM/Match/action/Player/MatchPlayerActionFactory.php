<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Action\Player;

use STM\Team\Team;
use STM\Person\Person;
use STM\Team\Member\TeamMember;
use STM\Match\Lineup\MatchPlayer;
use STM\Match\Action\MatchAction;
use STM\Match\Match;
use STM\Entities\AbstractEntityFactory;

class MatchPlayerActionFactory extends AbstractEntityFactory
{
    public function findById(MatchPlayer $match_player, MatchAction $match_action, $minute_action)
    {
        return $this->entityHelper->findById($match_player, $match_action, $minute_action);
    }

    public function findByMatch(Match $match)
    {
        $methods = array('setMatch' => $match);
        return $this->entityHelper->setAndFindArray($methods);
    }

    public function findByPerson(Person $person, $team = null)
    {
        $methods = array('setPerson' => $person);
        if ($team instanceof Team) {
            $methods['setTeam'] = $team;
        }
        return $this->entityHelper->setAndFindArray($methods);
    }

    public function findByTeamPlayer(TeamMember $teamMember)
    {
        $methods = array('setTeamPlayer' => $teamMember);
        return $this->entityHelper->setAndFindArray($methods);
    }

    public function findAll()
    {
        return $this->entityHelper->findAll();
    }

    protected function getEntitySelection()
    {
         return new MatchPlayerActionSelection();
    }

    protected function getEntityClass()
    {
        return 'STM\Match\Action\Player\MatchPlayerAction';
    }
}
