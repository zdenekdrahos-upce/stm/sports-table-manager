<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Action;

use STM\DB\Object\DatabaseObject;

/**
 * MatchAction class
 * - instance: represents row from database table 'MATCH_ACTIONS'
 */
final class MatchAction
{
    /** @var DatabaseObject */
    private static $db;

    /** @var int */
    private $id_match_action;
    /** @var string */
    private $match_action;
    /** @var int */
    private $count_in_teams;
    /** @var int */
    private $count_in_players;
    /** @var int */
    private $id_sport;
    /** @var string */
    private $sport;

    private function __construct()
    {
    }

    public static function setORM(DatabaseObject $dbObject)
    {
        if (is_null(self::$db)) {
            self::$db = $dbObject;
        }
    }

    public static function create($attributes)
    {
        if (MatchActionValidator::checkCreate($attributes)) {
            return self::$db->insertValues(self::processAttributes($attributes), true);
        }
        return false;
    }

    public static function exists($action_name)
    {
        return self::$db->existsItem('match_action', $action_name);
    }

    public function delete()
    {
        if ($this->canBeDeleted()) {
            return self::$db->deleteById($this->id_match_action);
        }
        return false;
    }

    public function canBeDeleted()
    {
        return $this->count_in_players == 0 && $this->count_in_teams == 0;
    }

    public function update($attributes)
    {
        MatchActionValidator::setMatchAction($this);
        if (MatchActionValidator::checkUpdate($attributes)) {
            return self::$db->updateById($this->id_match_action, self::processAttributes($attributes));
        }
        return false;
    }

    public function getId()
    {
        return (int) $this->id_match_action;
    }

    public function getIdSport()
    {
        return (int) $this->id_sport;
    }

    public function toArray()
    {
        return array(
            'match_action' => $this->match_action,
            'sport' => $this->sport,
            'count_in_teams' => $this->count_in_teams,
            'count_in_players' => $this->count_in_players,
        );
    }

    public function __toString()
    {
        return $this->match_action;
    }

    private static function processAttributes($attributes)
    {
        $attributes['id_sport'] = $attributes['sport']->getId();
        unset($attributes['sport']);
        return $attributes;
    }
}
