<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Action;

use STM\Helpers\ObjectValidator;
use STM\Web\Message\FormError;
use STM\Sport\Sport;

/**
 * Class for validating attributes for database table 'MATCH_ACTIONS'
 * - attributes = match_action (name), id_sport
 */
final class MatchActionValidator
{
    /** @var ObjectValidator */
    private static $validator;

    /** @param \STM\Libs\FormProcessor $formProcessor */
    public static function init($formProcessor = false)
    {
        if (is_null(self::$validator)) {
            self::$validator = new ObjectValidator('\STM\Match\Action\MatchAction');
        }
        self::$validator->setFormProcessor($formProcessor);
    }

    public static function setMatchAction($match_action)
    {
        self::$validator->setComparedObject($match_action);
    }

    public static function resetMatchAction()
    {
        self::$validator->resetComparedObject();
    }

    public static function checkCreate($attributes)
    {
        if (self::$validator->checkCreate($attributes, self::getAttributes())) {
            self::checkAttributes($attributes);
        }
        return self::$validator->isValid();
    }

    public static function checkUpdate($attributes)
    {
        if (self::$validator->checkUpdate($attributes, self::getAttributes())) {
            self::checkAttributes($attributes);
            if (self::$validator->isValid()) {
                self::checkChange($attributes);
            }
        }
        return self::$validator->isValid();
    }

    private static function checkAttributes($attributes)
    {
        self::checkName($attributes['match_action']);
        self::checkSport($attributes['sport']);
    }

    private static function checkName($name)
    {
        if ($name != self::$validator->getValueFromCompared('__toString')) {
            self::$validator->checkUniqueName($name, array('min' => 2, 'max' => 50), '__toString');
        }
    }

    private static function checkSport($sport)
    {
        self::$validator->getFormProcessor()->checkCondition($sport instanceof Sport, 'Invalid sport');
    }

    private static function checkChange($attributes)
    {
        $new_name = $attributes['match_action'] != self::$validator->getValueFromCompared('__toString');
        $new_sport = $attributes['sport']->getId() != self::$validator->getValueFromCompared('getIdSport');
        self::$validator->getFormProcessor()->checkCondition(
            $new_sport || $new_name,
            FormError::get('no-change', array(stmLang('action', 'name')))
        );
    }

    private static function getAttributes()
    {
        return array('match_action', 'sport');
    }
}
