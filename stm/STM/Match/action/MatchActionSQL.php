<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Action;

use STM\DB\SQL\SelectQuery;
use STM\DB\SQL\JoinType;
use STM\DB\SQL\JoinTable;

class MatchActionSQL
{

    /** @return SelectQuery */
    public static function selectMatchActions()
    {
        $query = new SelectQuery(DT_MATCH_ACTIONS . ' match_actions');
        $query->setColumns(
            'match_actions.id_match_action, match_actions.match_action, sports.id_sport,
            sports.sport, count(team_actions.id_match_statistic) as count_in_teams,
            count(player_actions.id_match_action) as count_in_players'
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                DT_SPORTS . ' sports',
                'match_actions.id_sport = sports.id_sport'
            )
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::LEFT,
                DT_MATCH_TEAM_ACTIONS . ' team_actions',
                'match_actions.id_match_action = team_actions.id_match_statistic'
            )
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::LEFT,
                DT_MATCH_PLAYER_ACTIONS . ' player_actions',
                'match_actions.id_match_action = player_actions.id_match_action'
            )
        );
        $query->setGroupBy(
            'match_actions.id_match_action, match_actions.match_action, sports.id_sport, sports.sport'
        );
        $query->setOrderBy('sports.sport, match_actions.match_action');
        return $query;
    }
}
