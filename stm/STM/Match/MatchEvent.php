<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match;

use STM\Helpers\Event;

/**
 * MatchEvent class
 * - for saving events related with Match and Period
 */
class MatchEvent
{
    /** Creating new match */
    const CREATE = 'Create Match';
    /** Updating match - update date, home team, away team */
    const UPDATE = 'Update Match';
    /** Deleting match - match + periods if some periods belongs to match */
    const DELETE = 'Delete Match';
    /** Deleting result - periods from table match_periods */
    const DELETE_RESULT = 'Delete Result';
    /** Creating period of the match */
    const CREATE_PERIOD = 'Create Period';
    /** Creating result - all periods */
    const CREATE_RESULT = 'Create Result';
    /** Updating score of the match */
    const UPDATE_RESULT = 'Update Result';
    /** Deleting one period of the match */
    const DELETE_PERIOD = 'Delete Period';
    /** Updating or creating match detail */
    const UPDATE_DETAIL = 'Update Detail';
    /** Deleting match detail */
    const DELETE_DETAIL = 'Delete Detail';
    /** Actions with referees */
    const REFEREES = 'Referees';
    /** Actions with referees */
    const PLAYERS = 'Players';
    /** Actions with Match actions */
    const MATCH_ACTIONS = 'Match Actions';
    /** Actions with Match TEAM actions */
    const MATCH_TEAM_ACTIONS = 'Match Team Actions';
    /** Actions with Match PLAYER actions */
    const MATCH_PLAYER_ACTIONS = 'Match Player Actions';


    private function __construct()
    {
    }

    /**
     * Saves events related with Match and Period
     * @param MatchEvent $matchEvent constant from MatchEvent class
     * @param string $message description of event
     * @param boolean $result if event was sucessful or fail
     * @return boolean
     * Returns true if event was successfuly saved, otherwise false (bad arguments
     * or error during file saving)
     */
    public static function log($matchEvent, $message, $result)
    {
        $event = new Event('\STM\Match\MatchEvent', 'matches');
        $event->setMessage($matchEvent, $message, $result);
        return $event->logEvent();
    }
}
