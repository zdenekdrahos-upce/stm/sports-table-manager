<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Table;

use STM\DB\Object\DatabaseObject;
use \Serializable;
use STM\StmCache;
use STM\Match\MatchSelectionBuilder;
use STM\Competition\Season\Season;

class ResultTable implements Serializable
{
    /** @var DatabaseObject */
    private static $db;
    /** @var \STM\DB\SQL\Condition */
    private $where_condition;
    /** @var TablePoints */
    protected $points;
    /** @var int */
    private $match_periods;
    /** @var array */
    private $table_home;
    /** @var array */
    private $table_away;
    /** @var array */
    private $table_full;

    protected function __construct($match_selection, $points, $match_periods)
    {
        $selectionBuilder = new MatchSelectionBuilder();
        $ms = $selectionBuilder->build($match_selection);
        $this->where_condition = $ms->getWhereCondition();
        $this->points = $points;
        $this->match_periods = $match_periods;
        $this->table_home = $this->loadTable(true);
        $this->table_away = $this->loadTable(false);
        $this->calculateTruthPoints();
        $this->calculateFullTable();
    }

    public static function setORM(DatabaseObject $dbObject)
    {
        if (is_null(self::$db)) {
            self::$db = $dbObject;
        }
    }

    /**
     * @param array $match_selection
     * @param TablePoints $points
     * @return ResultTable|false
     */
    public static function getTable(array $match_selection, TablePoints $points, $match_periods)
    {
        if (self::canBeInstanceCreated($match_selection, $points, $match_periods)) {
            return new self($match_selection, $points, $match_periods);
        }
        return false;
    }

    /**
     * @param array $tables
     * @return ResultTable|false
     */
    public static function import($season, $new_tables)
    {
        if (is_array($new_tables) && $season instanceof Season) {
            $table = StmCache::getSeasonTable($season);
            $table->table_full = $new_tables['full'];
            $table->table_home = $new_tables['home'];
            $table->table_away = $new_tables['away'];
            return $table;
        }
        return false;
    }

    public function serialize()
    {
        $tables = array(
            'home' => $this->table_home,
            'away' => $this->table_away,
            'full' => $this->table_full
        );
        return serialize($tables);
    }

    public function unserialize($serialized)
    {
        $tables = unserialize($serialized);
        $this->table_home = $tables['home'];
        $this->table_away = $tables['away'];
        $this->table_full = $tables['full'];
    }

    /** @return array Returns empty array if no matches in selection */
    public function getTableFull()
    {
        return $this->table_full;
    }

    /** @return array Returns empty array if no matches in selection */
    public function getTableHome()
    {
        return $this->table_home;
    }

    /** @return array Returns empty array if no matches in selection */
    public function getTableAway()
    {
        return $this->table_away;
    }

    /**
     * Loads home or away table
     * @param bool $home_away determines if load home or away table
     * @return array
     * Returns sorted array, items are instances of TableRow
     */
    private function loadTable($home_away)
    {
        $query = ResultTableSQL::selectSeasonTable(
            $home_away,
            $this->match_periods,
            $this->where_condition
        );
        $temp_table = self::$db->select($query);
        $this->calculatePoints($temp_table);
        $this->sortTable($temp_table);
        return $temp_table;
    }

    private function calculateFullTable()
    {
        $this->table_full = array();
        foreach ($this->table_home as $home_row) {
            foreach ($this->table_away as $away_row) {
                if ($home_row->idTeam == $away_row->idTeam) {
                    $this->table_full[] = TableRowHelper::mergeRows($home_row, $away_row);
                    break;
                }
            }
        }
        $this->checkOneGroundTeams();
        $this->sortTable($this->table_full);
    }

    private function checkOneGroundTeams()
    {
        $tables = array('table_home', 'table_away');
        foreach ($tables as $attribute_name) {
            foreach ($this->$attribute_name as $table_row) {
                if ($this->isNotInFullTable($table_row)) {
                    $this->table_full[] = $table_row;
                }
            }
        }
    }

    private function isNotInFullTable($table_row)
    {
        foreach ($this->table_full as $row) {
            if ($row->idTeam == $table_row->idTeam) {
                return false;
            }
        }
        return true;
    }

    protected function calculatePoints(&$array)
    {
        foreach ($array as $key => $table_row) {
            $array[$key] = TableRowHelper::calculatePointsInRow($table_row, $this->points);
        }
    }

    private function calculateTruthPoints()
    {
        foreach ($this->table_home as &$home_row) {
            $home_row->truthPoints =
                TableRowHelper::getPointsHomeForTruthTable($home_row, $this->points);
        }
        foreach ($this->table_away as &$away_row) {
            $away_row->truthPoints =
                TableRowHelper::getPointsAwayForTruthTable($away_row, $this->points);
        }
    }

    private function sortTable(&$array)
    {
        usort($array, array('\STM\Match\Table\TableRowHelper', 'compareByPoints'));
    }

    protected static function canBeInstanceCreated($match_selection, $points, $match_periods)
    {
        return is_numeric($match_periods);
    }
}
