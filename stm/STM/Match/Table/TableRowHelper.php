<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Table;

use STM\Stats\HomeAwayNumericStats;

/**
 * TableSort class
 * - for sorting season tables (array which consists of instance of TableRow)
 * - sorting by other columns is used TableSorter (http://tablesorter.com)
 */
class TableRowHelper
{
    /**
     * Compares two instances of TableRow by points.
     * @param TableRow $a
     * @param TableRow $b
     * @return int
     */
    public static function compareByPoints(TableRow $a, TableRow $b)
    {
        if ($a->points != $b->points) {
            $is_a_bigger = $a->points > $b->points;
        } elseif ($a->matches != $b->matches) {
            $is_a_bigger = $a->matches < $b->matches;
        } elseif ($a->goalFor != $b->goalFor) {
            $is_a_bigger = $a->goalFor > $b->goalFor;
        } elseif ($a->goalAgainst != $b->goalAgainst) {
            $is_a_bigger = $a->goalAgainst < $b->goalAgainst;
        } elseif ($a->truthPoints != $b->truthPoints) {
            $is_a_bigger = $a->truthPoints > $b->truthPoints;
        } else {
            return 0;
        }
        return $is_a_bigger ? -1 : 1;
    }

    /**
     * Merges two rows. Used for calculating table when you have home and away table.
     * @param TableRow $home
     * @param TableRow $away
     * @return TableRow/false
     * Returns false if at least one parameter isn't instance of TableRow or
     * parameters are results for different teams
     */
    public static function mergeRows(TableRow $home, TableRow $away)
    {
        if ($home->idTeam == $away->idTeam) {
            $row = new TableRow();
            $row->idTeam = $home->idTeam;
            $row->name = $home->name;
            foreach (array_keys(get_class_vars('\STM\Match\Table\TableRow')) as $key) {
                if (!in_array($key, array('name', 'idTeam'))) {
                    $row->$key += $home->$key;
                    $row->$key += $away->$key;
                }
            }
            return $row;
        }
        return false;
    }

    public static function calculatePointsInRow(TableRow $row, TablePoints $points)
    {
        $row->points = 0;
        $row->points += $row->win * $points->points_win;
        $row->points += $row->winInOt * $points->points_win_ot;
        $row->points += $row->draw * $points->points_draw;
        $row->points += $row->lossInOt * $points->points_loss_ot;
        $row->points += $row->loss * $points->points_loss;
        $row->points += $row->extraPoints;
        return $row;
    }

    /** @return int */
    public static function getPointsForTruthTable(TableRow $home, TableRow $away, TablePoints $points)
    {
        return self::getPointsHomeForTruthTable($home, $points) +
            self::getPointsAwayForTruthTable($away, $points);
    }

    /** @return int */
    public static function getPointsHomeForTruthTable(TableRow $home, TablePoints $points)
    {
        $win = 0;
        $draw = $home->draw * ($points->points_draw - $points->points_win);
        $loss = ($home->loss + $home->lossInOt) * (-$points->points_win);
        return $win + $draw + $loss;
    }

    /** @return int */
    public static function getPointsAwayForTruthTable(TableRow $away, TablePoints $points)
    {
        $win = ($away->win + $away->winInOt) * $points->points_win;
        $draw = $away->draw * $points->points_draw;
        $loss = 0;
        return $win + $draw + $loss;
    }

    /** @return array */
    public static function getTeamRowsFromTable(ResultTable $result_table, $id_team)
    {
        return array(
            'full' => self::getTeamRowFromTable($result_table->getTableFull(), $id_team),
            'home' => self::getTeamRowFromTable($result_table->getTableHome(), $id_team),
            'away' => self::getTeamRowFromTable($result_table->getTableAway(), $id_team),
        );
    }

    /** @return TableRow */
    private static function getTeamRowFromTable(array $table, $id_team)
    {
        $row = false;
        if (is_numeric($id_team)) {
            foreach ($table as $table_row) {
                if ($table_row->idTeam == $id_team) {
                    $row = $table_row;
                }
            }
        }
        return $row ? $row : new TableRow();
    }

    /** @return \STM\Stats\HomeAwayNumericStats */
    public static function getTeamPositions(ResultTable $result_table, $id_team)
    {
        $stats = new HomeAwayNumericStats();
        $stats->home = self::getTeamPosition($result_table->getTableHome(), $id_team);
        $stats->away = self::getTeamPosition($result_table->getTableAway(), $id_team);
        $stats->total = self::getTeamPosition($result_table->getTableFull(), $id_team);
        return $stats;
    }

    /** @return int */
    private static function getTeamPosition(array $table, $id_team)
    {
        foreach ($table as $position => $row) {
            if ($row->idTeam == $id_team) {
                return $position + 1;
            }
        }
        return 0;
    }
}
