<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Table\Cross;

use STM\Match\Table\ResultTable;

/**
 * Only works if names of teams are unique
 */
final class CrossTable
{
    /** @var array from ResultTable::getTableFull */
    private $full_table;
    /** @var array item = Match */
    private $matches;
    /** @var array item = CrossTableRow */
    private $cross_table;

    public function __construct(ResultTable $table, array $matches)
    {
        $this->full_table = $table->getTableFull();
        $this->matches = $matches;
    }

    public function getTable()
    {
        $this->cross_table = array();
        $this->initRows();
        $this->loadResults();
        return $this->cross_table;
    }

    private function initRows()
    {
        foreach ($this->full_table as $row) {
            $this->cross_table[$row->name] = new CrossTableRow($row);
            foreach ($this->full_table as $opponent_row) {
                $this->cross_table[$row->name]->opponents[$opponent_row->name] = array();
            }
        }
    }

    private function loadResults()
    {
        foreach (array_keys($this->cross_table) as $team_name) {
            $this->loadTeamResults($team_name);
        }
    }

    private function loadTeamResults($team)
    {
        foreach ($this->matches as $match) {
            $temp = $match->toArray();
            if (in_array($team, array($temp['team_home'], $temp['team_away']))) {
                if ($team == $temp['team_home']) {
                    $this->addResultToTeam(
                        $team,
                        $temp['team_away'],
                        "{$temp['score_home']}:{$temp['score_away']}"
                    );
                } else {
                    $this->addResultToTeam(
                        $team,
                        $temp['team_home'],
                        "{$temp['score_away']}:{$temp['score_home']}"
                    );
                }
            }
        }
    }

    private function addResultToTeam($team, $opponent, $score)
    {
        if ($score != ':') {
            $this->cross_table[$team]->opponents[$opponent][] = $score;
        }
    }
}
