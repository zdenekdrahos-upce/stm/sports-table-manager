<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Table\Cross;

use STM\Match\Table\TableRow;

final class CrossTableRow
{
    /** @var TableRow */
    public $tableRow;
    /** @var array */
    public $opponents;

    public function __construct(TableRow $row)
    {
        $this->tableRow = $row;
        $this->opponents = array();
    }
}
