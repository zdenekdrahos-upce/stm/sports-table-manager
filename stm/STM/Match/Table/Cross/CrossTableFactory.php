<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Table\Cross;

use STM\Competition\Season\Season;
use STM\StmCache;
use STM\Match\Table\ResultTable;
use STM\Match\Table\TablePoints;
use STM\StmFactory;

class CrossTableFactory
{
    public static function getSeasonCrossTable(Season $season)
    {
        $result_table = StmCache::getSeasonTable($season);
        $matches = StmCache::getAllCompetitionMatches($season);
        return new CrossTable($result_table, $matches);
    }

    public static function getMatchSelectionCrossTable(array $selection)
    {
        $result_table = ResultTable::getTable($selection, new TablePoints(), STM_MATCH_PERIODS);
        $matches = StmFactory::find()->Match->find($selection);
        return new CrossTable($result_table, $matches);
    }
}
