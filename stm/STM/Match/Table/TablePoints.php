<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Table;

use STM\Competition\Season\Season;

final class TablePoints
{
    public $points_win;
    public $points_win_ot;
    public $points_draw;
    public $points_loss_ot;
    public $points_loss;

    public function __construct()
    {
        $this->points_win = STM_POINT_WIN;
        $this->points_win_ot = STM_POINT_WIN_OT;
        $this->points_draw = STM_POINT_DRAW;
        $this->points_loss_ot = STM_POINT_LOSS_OT;
        $this->points_loss = STM_POINT_LOSS;
    }

    public static function getSeasonPoints(Season $season)
    {
        $points = new self();
        $points->points_win = $season->getPointWin();
        $points->points_win_ot = $season->getPointWinOvertime();
        $points->points_draw = $season->getPointDraw();
        $points->points_loss_ot = $season->getPointLossOvertime();
        $points->points_loss = $season->getPointLoss();
        return $points;
    }
}
