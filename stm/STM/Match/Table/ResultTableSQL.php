<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Table;

use STM\DB\Database;
use STM\DB\SQL\SelectQuery;
use STM\DB\SQL\JoinTable;
use STM\DB\SQL\JoinType;

class ResultTableSQL
{
    /**
     * @param bool $home_away determines if load home or away table
     * @return SelectQuery
     * Returns query for creating season tables
     */
    public static function selectSeasonTable($home_away, $match_periods, $condition)
    {
        $match_periods = (int) $match_periods;
        $table = self::getTable($home_away, $match_periods, $condition);
        $id = $home_away ? 'id_home' : 'id_away';

        $query = new SelectQuery("({$table->generate()}) result_table");
        $query->setColumns(
            'teams.name, teams.id_team as idTeam, win, winInOt, draw, lossInOt, loss, goalFor, goalAgainst'
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                DT_TEAMS . ' teams',
                "teams.id_team = result_table.{$id}"
            )
        );
        return $query;
    }

    private static function getTable($home_away, $match_periods, $condition)
    {
        $id = $home_away ? 'matches.id_home' : 'matches.id_away';
        $query = new SelectQuery(DT_MATCH_RESULTS . ' match_results');
        $query->setColumns($id);
        $query->setColumns('sum(case when score_home = score_away THEN 1 ELSE 0 END) as draw');
        if ($home_away) {
            $query->setColumns(self::getTableHomeAttributes($match_periods));
        } else {
            $query->setColumns(self::getTableAwayAttributes($match_periods));
        }
        $query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                DT_MATCHES . ' matches',
                "match_results.id_match = matches.id_match"
            )
        );
        $query->setWhere($condition);
        $query->setGroupBy($id);
        Database::escapeQuery($query);
        return $query;
    }

    private static function getTableHomeAttributes($match_periods)
    {
        return "
            sum(case when period_count > $match_periods and score_home > score_away
                THEN 1 ELSE 0 END) as winInOt,
            sum(case when period_count > $match_periods and score_home < score_away
                THEN 1 ELSE 0 END) as lossInOt,
            sum(case when period_count <= $match_periods and score_home > score_away
                THEN 1 ELSE 0 END) as win,
            sum(case when period_count <= $match_periods and score_home < score_away
                THEN 1 ELSE 0 END) as loss,
            sum(score_home) as goalFor,
            sum(score_away) as goalAgainst
        ";
    }

    private static function getTableAwayAttributes($match_periods)
    {
        return "
            sum(case when period_count > $match_periods and score_home < score_away
                THEN 1 ELSE 0 END) as winInOt,
            sum(case when period_count > $match_periods and score_home > score_away
                THEN 1 ELSE 0 END) as lossInOt,
            sum(case when period_count <= $match_periods and score_home < score_away
                THEN 1 ELSE 0 END) as win,
            sum(case when period_count <= $match_periods and score_home > score_away
                THEN 1 ELSE 0 END) as loss,
            sum(score_away) as goalFor,
            sum(score_home) as goalAgainst
        ";
    }
}
