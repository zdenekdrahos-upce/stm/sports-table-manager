<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Table;

final class TableRow
{
    /** @var string */
    public $name;
    /** @var int */
    public $idTeam;
    /** @var int */
    public $matches;
    /** @var int */
    public $win;
    /** @var int */
    public $winInOt;
    /** @var int */
    public $draw;
    /** @var int */
    public $lossInOt;
    /** @var int */
    public $loss;
    /** @var int */
    public $goalFor;
    /** @var int */
    public $goalAgainst;
    /** @var int */
    public $extraPoints;
    /** @var int */
    public $points;
    /** @var int */
    public $truthPoints;

    public function __construct()
    {
        $this->typeAttributes();
        $this->updateMatchCount();
    }

    public function toArray()
    {
        return get_object_vars($this);
    }

    public function __toString()
    {
        return $this->name . ' - ' . $this->matches . ' | ' .
            $this->win . ' ' . $this->winInOt . ' ' . $this->draw . ' ' .
            $this->lossInOt . ' ' . $this->loss . ' ' .
            $this->goalFor . ':' . $this->goalAgainst . ' | ' .
            $this->points . " ({$this->extraPoints})";
    }

    /**
     * Types attributes to int, except the name which is type to string. Its
     * needed because database_fetch returns everything as string
     */
    private function typeAttributes()
    {
        foreach (get_object_vars($this) as $key => $value) {
            if ($key == 'name') {
                $this->$key = empty($value) ? '' : (string) $value;
            } else {
                $this->$key = empty($value) ? 0 : (int) $value;
            }
        }
    }

    public function updateMatchCount()
    {
        $this->matches = $this->win + $this->winInOt + $this->draw + $this->lossInOt + $this->loss;
    }
}
