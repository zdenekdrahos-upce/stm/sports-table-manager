<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match;

use STM\DB\Database;
use STM\DB\SQL\SelectQuery;
use STM\DB\SQL\JoinTable;
use STM\DB\SQL\JoinType;
use STM\DB\SQL\Condition;
use STM\Match\MatchSelection;

class MatchSQL
{

    /**
     * Selects all information about match. Query selects id_match, id_competition,
     * datetime, id_home, id_away, round from table 'MATCHES' and name of home and away
     * team from table 'TEAMS'
     * @return SelectQuery
     */
    public static function selectMatches()
    {
        $query = new SelectQuery(DT_MATCHES . ' matches');
        $query->setColumns(
            'matches.id_match, matches.id_competition, matches.datetime, matches.round,
             matches.id_serie, home.id_team as id_home, home.name as team_home,
             away.id_team as id_away, away.name as team_away '
        );
        $query->setJoinTables(new JoinTable(JoinType::JOIN, DT_TEAMS . ' home', 'matches.id_home = home.id_team'));
        $query->setJoinTables(new JoinTable(JoinType::JOIN, DT_TEAMS . ' away', 'matches.id_away = away.id_team'));
        $query->setOrderBy('matches.datetime asc, matches.round asc, matches.id_match asc');
        return $query;
    }

    /** @return SelectQuery */
    public static function selectLastPlayedMatch(Condition $where)
    {
        $date_where = clone $where;
        $query = self::selectMatches();
        $query->setColumns('match_scores.score_home, match_scores.score_away');
        $query->setJoinTables(
            new JoinTable(
                JoinType::LEFT,
                DT_MATCH_RESULTS . " match_scores",
                'matches.id_match = match_scores.id_match'
            )
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::LEFT,
                DT_MATCH_PERIODS . " periods",
                'matches.id_match = periods.id_match'
            )
        );
        $where->addCondition('matches.datetime = dates.SEARCHED_DATE');
        $query->setWhere($where);
        $date_query = self::getMaxDateOfLastPlayedMatch($date_where);
        $query->setJoinTables(new JoinTable(JoinType::CROSS, "({$date_query->generate()}) dates"));
        return $query;
    }

    private static function getMaxDateOfLastPlayedMatch($where)
    {
        $date_query = new SelectQuery(DT_MATCHES . ' matches');
        $date_query->setColumns('MAX(datetime) as SEARCHED_DATE');
        $date_query->setJoinTables(
            new JoinTable(
                JoinType::LEFT,
                DT_MATCH_PERIODS . " periods",
                'matches.id_match = periods.id_match'
            )
        );
        $where->addCondition('datetime <= ' . Database::getCurrentDateFunction());
        $date_query->setWhere($where);
        Database::escapeQuery($date_query);
        return $date_query;
    }

    /** @return SelectQuery */
    public static function selectFirstUpcomingMatch(Condition $where)
    {
        $date_where = clone $where;
        $query = self::selectMatches();
        $query->setJoinTables(
            new JoinTable(
                JoinType::LEFT,
                DT_MATCH_PERIODS . " periods",
                'matches.id_match = periods.id_match'
            )
        );
        $where->addCondition('periods.id_match IS NULL AND matches.datetime = dates.SEARCHED_DATE');
        $query->setWhere($where);
        $date_query = self::getMinDateOfUpcomingMatch($date_where);
        $query->setJoinTables(new JoinTable(JoinType::CROSS, "({$date_query->generate()}) dates"));
        return $query;
    }

    private static function getMinDateOfUpcomingMatch($where)
    {
        $date_query = new SelectQuery(DT_MATCHES . ' matches');
        $date_query->setColumns('MIN(datetime) as SEARCHED_DATE');
        $date_query->setJoinTables(
            new JoinTable(
                JoinType::LEFT,
                DT_MATCH_PERIODS . " periods",
                'matches.id_match = periods.id_match'
            )
        );
        $where->addCondition('periods.id_match IS NULL');
        $where->addCondition('datetime >= ' . Database::getCurrentDateFunction());
        $date_query->setWhere($where);
        Database::escapeQuery($date_query);
        return $date_query;
    }

    /** @return SelectQuery */
    public static function selectDateOfLastModifiedMatch(Condition $where)
    {
        $query = new SelectQuery(DT_MATCH_PERIODS . ' periods');
        $query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                DT_MATCHES . " matches",
                'matches.id_match = periods.id_match'
            )
        );
        $query->setColumns('MAX(last_modified) as SEARCHED_DATE');
        $query->setWhere($where);
        return $query;
    }

    /** @return \STM\DB\SQL\Condition */
    public static function getMatchCondition($id_competition, $id_team)
    {
        $condition = new Condition('');
        if (is_numeric($id_competition)) {
            $condition->addCondition('matches.id_competition = {C}', array('C' => $id_competition));
        }
        if (is_numeric($id_team)) {
            $condition->addCondition('(matches.id_home = {T} OR matches.id_away = {T})', array('T' => $id_team));
        }
        return $condition;
    }

    /** @return \STM\DB\SQL\Condition */
    public static function getMatchesDeleteCondition(Condition $escapedCondition)
    {
        // there is no table alias in DELETE FROM
        $text_condition = str_replace(
            'matches.',
            '',
            $escapedCondition->__toString()
        );
        return new Condition($text_condition);
    }

    public static function joinHomeYourTeams()
    {
        return new JoinTable(
            JoinType::JOIN,
            DT_COMPETITIONS_TEAMS . ' yourTeamHome',
            'matches.id_home = yourTeamHome.id_team AND matches.id_competition = yourTeamHome.id_competition'
        );
    }

    public static function joinAwayYourTeams()
    {
        return new JoinTable(
            JoinType::JOIN,
            DT_COMPETITIONS_TEAMS . ' yourTeamAway',
            'matches.id_away = yourTeamAway.id_team AND matches.id_competition = yourTeamAway.id_competition'
        );
    }

    public static function getJoinYourTeamsCondition()
    {
        return new Condition(
            '(yourTeamHome.your_team = {T} OR yourTeamAway.your_team = {T})',
            array('T' => 'Y')
        );
    }

    public static function filterQueryByMatchSelection(SelectQuery $query, MatchSelection $matchSelection, $tableName)
    {
        $joinTable = self::joinMatchSelection($matchSelection, $tableName);
        $query->setJoinTables($joinTable);
        return $query;
    }

    private static function joinMatchSelection(MatchSelection $matchSelection, $tableName)
    {
        $matchQuery = $matchSelection->getSelectQuery();
        Database::escapeQuery($matchQuery);
        return new JoinTable(
            JoinType::JOIN,
            "({$matchQuery->generate()}) match_selection",
            "{$tableName}.id_match = match_selection.id_match"
        );
    }
}
