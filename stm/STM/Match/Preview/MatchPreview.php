<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Preview;

/**
 * MatchPreview class
 * - preview of the next match of selected team
 */
class MatchPreview
{
    /** @var Match */
    public $match = null;
    /** @var int */
    public $idTeam = null;
    /** @var string */
    public $competitionName = '';
    /** @var \STM\Stats\HomeAwayComparison - home/away value is array with ExtendedMatch(s) */
    public $lastMatches = null;
    /** @var array - array with ExtendedMatch(s)*/
    public $headToHeadMatches = array();
    /** @var ResultTable */
    public $headToHeadTable = null;
    /** @var \STM\Stats\HomeAwayComparison - home/away value is instanceof TableRowsStats */
    public $seasonStatistics = null;
}
