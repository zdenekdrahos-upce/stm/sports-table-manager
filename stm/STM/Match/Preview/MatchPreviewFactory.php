<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Preview;

use STM\Competition\Season\Season;
use STM\Match\Match;
use STM\Match\ExtendedMatch;
use STM\Stats\HomeAwayComparison;
use STM\Match\Table\ResultTable;
use STM\Match\Table\TablePoints;
use STM\StmCache;
use STM\Match\Stats\Table\TableRowsStatsFactory;
use STM\StmFactory;

class MatchPreviewFactory
{
    /** @var int */
    private $idTeam;
    /** @var Match */
    private $match;
    /** @var MatchPreview */
    private $preview;

    public function __construct($id_team, $id_competition = null)
    {
        $this->idTeam = $id_team;
        $this->match = StmFactory::find()->Match->getFirstUpcomingMatch($id_competition, $id_team);
    }

    /**
     *
     * @param int $matches_count number of loaded matches (last played, head to head)
     * @return MatchPreview
     */
    public function loadPreviewData($matches_count)
    {
        $this->preview = new MatchPreview();
        if ($this->match instanceof Match) {
            $this->loadLastMatches($matches_count);
            $this->loadHeadToHead($matches_count);
            if ($this->match->getRound() !== false) {
                $this->loadSeasonStatistics();
            }
            $this->convertMatches();
            $this->loadBasicAttributes();
        }
        return $this->preview;
    }

    private function loadLastMatches($matches_count)
    {
        $last_matches = new HomeAwayComparison();
        $last_matches->home = $this->getLastMatches($this->match->getIdTeamHome(), $matches_count);
        $last_matches->away = $this->getLastMatches($this->match->getIdTeamAway(), $matches_count);
        $this->preview->lastMatches = $last_matches;
    }

    private function loadHeadToHead($matches_count)
    {
        $ms = $this->getHeadToHeadSelection($matches_count);
        $this->preview->headToHeadMatches = StmFactory::find()->Match->find($ms);
        $this->preview->headToHeadTable = ResultTable::getTable($ms, new TablePoints(), STM_MATCH_PERIODS);
    }

    private function loadSeasonStatistics()
    {
        $season_table = StmCache::getSeasonTable(Season::findById($this->match->getIdCompetition()));
        $statistics = new HomeAwayComparison();
        $statistics->home = TableRowsStatsFactory::getForTeamFromTable(
            $season_table,
            $this->match->getIdTeamHome()
        );
        $statistics->away = TableRowsStatsFactory::getForTeamFromTable(
            $season_table,
            $this->match->getIdTeamAway()
        );
        $this->preview->seasonStatistics = $statistics;
    }

    private function getLastMatches($id_team, $matches_count)
    {
        $ms = $this->getMatchSelection($matches_count);
        $ms['teams'] = array($id_team);
        return StmFactory::find()->Match->find($ms);
    }

    private function getHeadToHeadSelection($matches_count)
    {
        $ms = $this->getMatchSelection($matches_count);
        $ms['teams'] = array($this->match->getIdTeamHome(), $this->match->getIdTeamAway());
        $ms['headToHead'] = true;
        return $ms;
    }

    private function getMatchSelection($matches_count)
    {
        return array(
            'matchType' => \STM\Match\MatchSelection::PLAYED_MATCHES,
            'loadScores' => true,
            'loadPeriods' => false,
            'limit' => array('max' => $matches_count),
            'order' => array(
                'datetime' => false,
            )
        );
    }

    private function convertMatches()
    {
        $this->convertMatchesToExtended($this->preview->headToHeadMatches, $this->idTeam);
        $this->convertMatchesToExtended($this->preview->lastMatches->home, $this->match->getIdTeamHome());
        $this->convertMatchesToExtended($this->preview->lastMatches->away, $this->match->getIdTeamAway());
    }

    private function convertMatchesToExtended(&$matches, $id_team)
    {
        foreach ($matches as $key => $match) {
            $matches[$key] = new ExtendedMatch($match, $id_team);
        }
    }

    private function loadBasicAttributes()
    {
        $this->loadCompetitionName();
        $this->preview->match = $this->match;
        $this->preview->idTeam = $this->idTeam;
    }

    private function loadCompetitionName()
    {
        $competition = StmFactory::find()->Competition->findById($this->match->getIdCompetition());
        $this->preview->competitionName = $competition->__toString();
    }
}
