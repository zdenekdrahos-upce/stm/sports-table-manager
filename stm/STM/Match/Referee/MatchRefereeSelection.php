<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Referee;

use STM\Entities\AbstractEntitySelection;
use STM\Person\Person;
use STM\Match\Match;

class MatchRefereeSelection extends AbstractEntitySelection
{

    public function setId(Match $match, Person $person)
    {
        $this->setMatch($match);
        $this->setPerson($person);
    }

    public function setMatch(Match $match)
    {
        $this->selection->setAttributeLikeValue('match_referees.id_match', (string)$match->getId());
    }

    public function setPerson(Person $person)
    {
        $this->selection->setAttributeLikeValue('match_referees.id_referee', (string)$person->getId());
    }

    protected function getQuery()
    {
        return MatchRefereeSQL::selectMatchReferees();
    }
}
