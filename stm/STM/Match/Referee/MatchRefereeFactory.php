<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Referee;

use STM\Entities\AbstractEntityFactory;
use STM\Person\Person;
use STM\Match\Match;

class MatchRefereeFactory extends AbstractEntityFactory
{
    public function findById(Match $match, Person $person)
    {
        return $this->entityHelper->findById($match, $person);
    }

    public function findByMatch(Match $match)
    {
        $methods = array('setMatch' => $match);
        return $this->entityHelper->setAndFindArray($methods);
    }

    public function findByPerson(Person $person)
    {
        $methods = array('setPerson' => $person);
        return $this->entityHelper->setAndFindArray($methods);
    }

    protected function getEntitySelection()
    {
         return new MatchRefereeSelection();
    }

    protected function getEntityClass()
    {
        return 'STM\Match\Referee\MatchReferee';
    }
}
