<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Referee;

use STM\Helpers\ObjectValidator;
use STM\Web\Message\FormError;
use STM\Match\Match;
use STM\Person\Person;
use STM\Person\Position\PersonPosition;
use STM\StmFactory;

/**
 * Class for validating attributes for database table 'MATCH_REFEREES'
 * - attributes = match, person, position
 */
final class MatchRefereeValidator
{
    /** @var ObjectValidator */
    private static $validator;

    /** @param \STM\Libs\FormProcessor $formProcessor */
    public static function init($formProcessor = false)
    {
        if (is_null(self::$validator)) {
            self::$validator = new ObjectValidator('\STM\Match\Referee\MatchReferee');
        }
        self::$validator->setFormProcessor($formProcessor);
    }

    public static function setMatchReferee($match_referee)
    {
        self::$validator->setComparedObject($match_referee);
    }

    public static function resetMatchReferee()
    {
        self::$validator->resetComparedObject();
    }

    public static function checkCreate($attributes)
    {
        if (self::$validator->checkCreate($attributes, array('match', 'person', 'position'))) {
            self::$validator->getFormProcessor()->checkCondition(
                $attributes['match'] instanceof Match,
                'Invalid club'
            );
            self::$validator->getFormProcessor()->checkCondition(
                $attributes['person'] instanceof Person,
                'Invalid person'
            );
            self::checkPosition($attributes['position']);
            self::checkExistingMatchReferee($attributes['match'], $attributes['person']);
        }
        return self::$validator->isValid();
    }

    public static function checkPosition($position)
    {
        self::$validator->getFormProcessor()->checkCondition(
            $position instanceof PersonPosition,
            'Invalid position'
        );
        if (self::$validator->isValid() && self::$validator->isSetComparedObject()) {
            $current_id = self::$validator->getValueFromCompared('getIdPosition');
            self::$validator->getFormProcessor()->checkCondition(
                $position->getId() != $current_id,
                FormError::get('no-change', array(stmLang('club', 'position')))
            );
        }
        return self::$validator->isValid();
    }

    private static function checkExistingMatchReferee($club, $person)
    {
        if (self::$validator->isValid()) {
            $referee = StmFactory::find()->MatchReferee->findById($club, $person);
            self::$validator->getFormProcessor()->checkCondition(
                $referee == false,
                FormError::get('unique-value', array(stmLang('match', 'match-referee')))
            );
        }
    }
}
