<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Referee;

use STM\DB\SQL\SelectQuery;
use STM\DB\SQL\JoinTable;
use STM\DB\SQL\JoinType;
use STM\DB\Query\QueryFactory;

class MatchRefereeSQL
{

    /** @return SelectQuery */
    public static function selectMatchReferees()
    {
        $query = new SelectQuery(DT_MATCH_REFEREES . ' match_referees');
        $query->setColumns(
            "match_referees.id_match, match_referees.id_referee, match_referees.id_position,
            positions.position as name_position, matches.name_match,
            concat(concat(persons.forename, ' '), persons.surname) as name_referee"
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                DT_PERSONS . ' persons',
                'match_referees.id_referee = persons.id_person'
            )
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                DT_PERSON_POSITIONS . ' positions',
                'match_referees.id_position = positions.id_position'
            )
        );
        // match name
        $match_query = QueryFactory::getMatchNameQuery();
        $query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                "({$match_query->generate()}) matches",
                'match_referees.id_match = matches.id_match'
            )
        );
        return $query;
    }
}
