<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Referee;

use STM\DB\Object\DatabaseObject;

/**
 * MatchReferee class
 * - instance: represents row from database table 'MATCH_REFEREES'
 */
final class MatchReferee
{
    /** @var DatabaseObject */
    private static $db;

    private $id_match;
    private $id_referee;
    private $id_position;
    private $name_match;
    private $name_referee;
    private $name_position;

    private function __construct()
    {
    }

    public static function setORM(DatabaseObject $dbObject)
    {
        if (is_null(self::$db)) {
            self::$db = $dbObject;
        }
    }

    public static function create($attributes)
    {
        if (MatchRefereeValidator::checkCreate($attributes)) {
            return self::$db->insertValues(self::processAttributes($attributes));
        }
        return false;
    }

    public function updatePersonPosition($position)
    {
        MatchRefereeValidator::setMatchReferee($this);
        if (MatchRefereeValidator::checkPosition($position)) {
            return self::$db->updateById(
                array('match' => $this->id_match, 'referee' => $this->id_referee),
                array('id_position' => $position->getId())
            );
        }
        return false;
    }

    public function delete()
    {
        return self::$db->deleteById(array('match' => $this->id_match, 'referee' => $this->id_referee));
    }

    public function getIdMatch()
    {
        return (int) $this->id_match;
    }

    public function getIdPerson()
    {
        return (int) $this->id_referee;
    }

    public function getIdPosition()
    {
        return (int) $this->id_position;
    }

    public function toArray()
    {
        return array(
            'person' => $this->name_referee,
            'position' => $this->name_position,
            'match' => $this->name_match
        );
    }

    public function __toString()
    {
        return $this->name_referee;
    }

    private static function processAttributes($attributes)
    {
        $result = array();
        $result['id_match'] = $attributes['match']->getId();
        $result['id_referee'] = $attributes['person']->getId();
        $result['id_position'] = $attributes['position']->getId();
        return $result;
    }
}
