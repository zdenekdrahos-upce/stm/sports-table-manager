<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match;

use STM\Utils\Classes;

/**
 * ScoreType class
 * - represents database table 'SCORE_TYPES'
 * - used for getting part of SQL query which is used in match score calculation
 * - used for simpler work with primary keys (F, T) from table
 */
class ScoreType
{
    /**
     * For Football ScoreType score is counted as sum of all periods
     */
    const FOOTBALL = 'Football scores';
    /**
     * For Football ScoreType score is counted as sum number of periods which team won
     */
    const TENNIS = 'Tennis scores';


    private function __construct()
    {
    }

    /**
     * Finds description of score type specified by primary key from DB table ScoreType
     * @param string $id_score_type valid = F, T
     * @return string
     */
    public static function getName($id_score_type)
    {
        if (is_string($id_score_type)) {
            switch ($id_score_type) {
                case 'F':
                    return self::FOOTBALL;
                case 'T':
                    return self::TENNIS;
            }
        }
        return 'Invalid Type';
    }

    /**
     * Finds match score calculation for home and away team
     * @param ScoreType $score_type
     * @param boolean $team determine if score is counted for home or away team
     * @return string
     * If $score_type is not constant from this class then returns query for
     * football scores
     */
    public static function getScoreQuery($score_type, $team = true)
    {
        $teams = self::loadTeams($team);
        if (Classes::isClassConstant('\STM\Match\ScoreType', $score_type)) {
            switch ($score_type) {
                case self::FOOTBALL:
                    return self::getFootballQuery($teams);
                case self::TENNIS:
                    return self::getTennisQuery($teams);
            }
        }
        return self::getFootballQuery($teams);
    }

    /**
     * @param array $teams
     * @return string
     * Returns score counted as sum of all periods
     */
    private static function getFootballQuery($teams)
    {
        return "SUM({$teams['home']}) as score_home,
                SUM({$teams['away']}) as score_away ";
    }

    /**
     * @param array $teams
     * @return string
     * Returns score counted as sum number of periods which team won
     */
    private static function getTennisQuery($teams)
    {
        return "SUM(CASE
                     WHEN score_home is null OR score_away is null THEN null
                     WHEN {$teams['home']} > {$teams['away']} THEN 1
                     ELSE 0 END) as score_home,
               SUM(CASE
                     WHEN score_home is null OR score_away is null THEN null
                     WHEN {$teams['home']} < {$teams['away']} THEN 1
                     ELSE 0 END) as score_away ";
    }

    /**
     * @param boolean $home
     * @return array
     * Returns array with keys 'home', 'away' and values score_home/score_away (depends
     * on $home). Values are used as attributes in Match for score. In Match there is
     * always used score_home/score_away, but for calculation of away season table
     * is used score_away/score_home
     */
    private static function loadTeams($home)
    {
        $home = is_bool($home) ? $home : true;
        return array(
            'home' => $home ? 'score_home' : 'score_away',
            'away' => $home ? 'score_away' : 'score_home'
        );
    }
}
