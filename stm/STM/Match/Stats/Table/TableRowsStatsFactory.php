<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Stats\Table;

use STM\Match\Table\ResultTable;
use STM\Match\Table\TableRow;
use STM\Match\Table\TableRowHelper;
use STM\Stats\HomeAwayNumericStats;

final class TableRowsStatsFactory
{
    /** @var array - name of attributes in TableRow */
    private static $search_stats = array(
        'matches', 'win', 'winInOt', 'draw', 'lossInOt', 'loss',
        'goalFor', 'goalAgainst', 'points', 'truthPoints'
    );
    /** @var TableRowsStats */
    private $stats;

    private function __construct(TableRow $home, TableRow $away)
    {
        $this->stats = new TableRowsStats();
        $this->initStatistics();
        $this->loadHomeStatistics($home);
        $this->loadAwayStatistics($away);
    }

    /** @return TableRowsStats */
    public static function getForTeamFromTable(ResultTable $result_table, $id_team)
    {
        $rows = TableRowHelper::getTeamRowsFromTable($result_table, $id_team);
        $instance = new self($rows['home'], $rows['away']);
        $instance->loadPositions($result_table, $id_team);
        return $instance->stats;
    }

    /** @return TableRowsStats */
    public static function getForTeam(TableRow $home, TableRow $away)
    {
        $instance = new self($home, $away);
        return $instance->stats;
    }

    private function initStatistics()
    {
        $this->stats->position = new HomeAwayNumericStats();
        foreach (self::$search_stats as $key) {
            $this->stats->$key = new HomeAwayNumericStats();
        }
    }

    private function loadHomeStatistics($table_row)
    {
        foreach (self::$search_stats as $key) {
            $this->stats->$key->home = $table_row->$key;
        }
    }

    private function loadAwayStatistics($table_row)
    {
        foreach (self::$search_stats as $key) {
            $this->stats->$key->away = $table_row->$key;
        }
    }

    private function loadPositions(ResultTable $table, $id_team)
    {
        $this->stats->position = TableRowHelper::getTeamPositions($table, $id_team);
    }
}
