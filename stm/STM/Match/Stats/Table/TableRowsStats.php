<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Stats\Table;

final class TableRowsStats
{
    /** @var \STM\Stats\HomeAwayNumericStats */
    public $position;
    /** @var \STM\Stats\HomeAwayNumericStats */
    public $matches;
    /** @var \STM\Stats\HomeAwayNumericStats */
    public $win;
    /** @var \STM\Stats\HomeAwayNumericStats */
    public $winInOt;
    /** @var \STM\Stats\HomeAwayNumericStats */
    public $draw;
    /** @var \STM\Stats\HomeAwayNumericStats */
    public $lossInOt;
    /** @var \STM\Stats\HomeAwayNumericStats */
    public $loss;
    /** @var \STM\Stats\HomeAwayNumericStats */
    public $goalFor;
    /** @var \STM\Stats\HomeAwayNumericStats */
    public $goalAgainst;
    /** @var \STM\Stats\HomeAwayNumericStats */
    public $points;
    /** @var \STM\Stats\HomeAwayNumericStats */
    public $truthPoints;
}
