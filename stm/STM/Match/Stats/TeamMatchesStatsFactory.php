<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Stats;

use STM\Match\Match;
use STM\Match\ExtendedMatch;
use STM\Match\Table\ResultTable;
use STM\Match\Table\TablePoints;
use STM\Match\Stats\Table\TableRowsStatsFactory;
use \stdClass;
use STM\StmFactory;

final class TeamMatchesStatsFactory
{
    /** @var array */
    private $match_selection;
    /** @var TeamMatchesStats */
    private $stats;

    /** @return TeamMatchesStats */
    public static function loadFromDatabase(array $match_selection, $id_team, TablePoints $points = null)
    {
        if (is_numeric($id_team)) {
            $instance = new self($match_selection, $id_team, $points);
            return $instance->stats;
        }
        return false;
    }

    private function __construct(array $match_selection, $id_team, TablePoints $points = null)
    {
        $tablePoints = is_null($points) ? new TablePoints() : $points;
        $this->initStatistics($id_team);
        $this->setMatchSelection($match_selection);
        $this->loadTables($tablePoints);
        $this->loadMatches();
        $this->loadTopMatches();
    }

    private function initStatistics($id_team)
    {
        $this->stats = new TeamMatchesStats();
        $this->stats->idSelectedTeam = is_numeric($id_team) ? (int) $id_team : -1;
    }

    private function setMatchSelection($match_selection)
    {
        $this->match_selection = $match_selection;
        $this->match_selection['teams'] = array($this->stats->idSelectedTeam);
    }

    private function loadTables(TablePoints $points)
    {
        $this->stats->resultTable = ResultTable::getTable($this->match_selection, $points, STM_MATCH_PERIODS);
        $this->stats->tableRowStatistics = TableRowsStatsFactory::getForTeamFromTable(
            $this->stats->resultTable,
            $this->stats->idSelectedTeam
        );
    }

    private function loadMatches()
    {
        $this->stats->matches = array();
        $matches = StmFactory::find()->Match->find($this->match_selection);
        foreach ($matches as $match) {
            $this->stats->matches[$match->getId()] =
                new ExtendedMatch($match, $this->stats->idSelectedTeam);
        }
    }

    private function loadTopMatches()
    {
        foreach ($this->stats->matches as $match) {
            if ($match->match->hasScore()) {
                $stats = $this->getTeamStatisticsInMatch($match->match);
                $this->stats->highestAggregateScore->checkMatch(
                    $match,
                    $stats->aggregateScore,
                    $stats->isHome
                );
                $this->stats->highestGoalFor->checkMatch($match, $stats->goalFor, $stats->isHome);
                $this->stats->highestGoalAgainst->checkMatch($match, $stats->goalAgainst, $stats->isHome);
                if ($match->result != 'D') {
                    $statistic = $stats->isWin ? 'highestWin' : 'highestLoss';
                    $this->stats->$statistic->checkMatch(
                        $match,
                        abs($stats->scoreDifference),
                        $stats->isHome
                    );
                }
            }
        }
    }

    private function getTeamStatisticsInMatch(Match $match)
    {
        $object = new stdClass();
        $object->isHome = $match->getIdTeamHome() == $this->stats->idSelectedTeam;
        $object->aggregateScore = $match->getScoreHome() + $match->getScoreAway();
        $object->goalFor = $object->isHome ? $match->getScoreHome() : $match->getScoreAway();
        $object->goalAgainst = $object->isHome ? $match->getScoreAway() : $match->getScoreHome();
        $object->scoreDifference = $object->isHome ?
            ($match->getScoreHome() - $match->getScoreAway()) :
            ($match->getScoreAway() - $match->getScoreHome());
        $object->isWin = $object->scoreDifference > 0;
        return $object;
    }
}
