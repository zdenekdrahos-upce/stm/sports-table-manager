<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Stats\Action;

use STM\DB\Database;
use STM\DB\SQL\SelectQuery;
use STM\DB\SQL\JoinTable;
use STM\DB\SQL\JoinType;

class MatchPlayerStatsSQL
{

    /** @return SelectQuery */
    public static function getPlayerGeneralStats($condition)
    {
        $query = new SelectQuery(DT_MATCH_PLAYERS . ' match_players');
        $query->setColumns(
            'match_players.id_team_player as idTeamPlayer,
            count(distinct match_players.id_match) as playedMatches,
            sum(match_players.minute_out) - sum(match_players.minute_in) as playedTime'
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                DT_TEAMS_PERSONS . ' team_persons',
                'match_players.id_team_player = team_persons.id_team_player'
            )
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                DT_MATCHES . ' matches',
                'match_players.id_match = matches.id_match'
            )
        );
        $condition->addCondition(Database::getIsNumeric('match_players.minute_in'));
        $condition->addCondition(Database::getIsNumeric('match_players.minute_out'));
        $query->setWhere($condition);
        $query->setGroupBy('match_players.id_team_player');
        return $query;
    }

    /** @return SelectQuery */
    public static function getPlayerActionStats($condition)
    {
        $query = new SelectQuery(DT_MATCH_PLAYERS . ' match_players');
        $query->setColumns(
            'match_players.id_team_player as idTeamPlayer,
            match_player_actions.id_match_action as idMatchAction,
            match_actions.match_action as matchAction,
            count(match_player_actions.minute_action) as count,
            sum(match_player_actions.minute_action) as sum'
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                DT_TEAMS_PERSONS . ' team_persons',
                'match_players.id_team_player = team_persons.id_team_player'
            )
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                DT_MATCH_PLAYER_ACTIONS . ' match_player_actions',
                'match_players.id_match_player = match_player_actions.id_match_player'
            )
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                DT_MATCH_ACTIONS . ' match_actions',
                'match_actions.id_match_action = match_player_actions.id_match_action'
            )
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                DT_MATCHES . ' matches',
                'match_players.id_match = matches.id_match'
            )
        );
        $condition->addCondition(Database::getIsNumeric('minute_action'));
        $query->setWhere($condition);
        $query->setGroupBy(
            'match_players.id_team_player, match_player_actions.id_match_action, match_actions.match_action'
        );
        $query->setOrderBy(
            'match_players.id_team_player, match_player_actions.id_match_action'
        );
        return $query;
    }
}
