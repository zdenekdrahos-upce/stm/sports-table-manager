<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Stats\Action;

use STM\DB\SQL\Condition;
use STM\Match\Match;
use STM\Match\MatchSelectionBuilder;
use STM\Team\Team;
use STM\Person\Person;
use STM\Team\Member\TeamMember;
use STM\Competition\Competition;
use STM\StmFactory;
use STM\Entities\AbstractEntitySelection;

class MatchPlayerStatsSelection extends AbstractEntitySelection
{
    /** @var array */
    private $members;
    /** @var \STM\Team\Member\TeamMemberFactory */
    private $teamMemberFactory;

    public function __construct()
    {
        parent::__construct();
        $this->members = array();
        $this->teamMemberFactory = StmFactory::find()->TeamMember;
    }

    /** @return Condition */
    public function getMembers()
    {
        return $this->members;
    }

    /** @param \STM\Competition\Competition|int $competition  */
    public function setCompetition($competition)
    {
        $ms = array(
            'matchType' => \STM\Match\MatchSelection::PLAYED_MATCHES,
            'loadScores' => false,
            'loadPeriods' => false,
            'competition' => $competition,
        );
        $this->filterByMatchSelection($ms);
        if ($competition instanceof Competition) {
            $this->addMembers($this->teamMemberFactory->findCompetitionPlayers($competition));
        }
    }

    /** @param Team $team  */
    public function setTeam(Team $team)
    {
        $this->selection->setAttributeWithValue('team_persons.id_team', (string) $team->getId());
        $this->addMembers($this->teamMemberFactory->findByTeam($team));
    }

    /** @param TeamMember $team_player  */
    public function setTeamPlayer(TeamMember $team_player)
    {
        if ($team_player->isTeamPlayer()) {
            $this->selection->setAttributeWithValue(
                'team_persons.id_team_player',
                (string) $team_player->getId()
            );
            $this->addMembers(array($team_player));
        }
    }

    /** @param Match $match  */
    public function setMatch(Match $match)
    {
        $this->selection->setAttributeWithValue('match_players.id_match', (string) $match->getId());
        $this->addMembers($this->teamMemberFactory->findByMatch($match));
    }

    /** @param \STM\Person\Person $person  */
    public function setPerson(Person $person)
    {
        $this->selection->setAttributeWithValue('team_persons.id_person', (string) $person->getId());
        $this->addMembers($this->teamMemberFactory->findByPerson($person));
    }

    /** @param array $matchSelection  */
    public function filterByMatchSelection(array $matchSelection)
    {
        if (!empty($matchSelection)) {
            $builder = new MatchSelectionBuilder();
            $ms = $builder->build($matchSelection);
            $this->selection->addCondition($ms->getWhereCondition());
        }
    }

    private function addMembers($new_members)
    {
        $this->members = $this->members + $new_members;
    }

    protected function getQuery()
    {

    }
}
