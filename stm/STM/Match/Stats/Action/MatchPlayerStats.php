<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Stats\Action;

use STM\DB\Object\DatabaseObject;
use \Serializable;
use STM\Utils\Arrays;
use STM\StmFactory;

final class MatchPlayerStats implements Serializable
{
    /** @var DatabaseObject */
    private static $dbActionStats;
    /** @var DatabaseObject */
    private static $dbGeneralStats;

    /** @var array */
    private $statistics;
    /** @var array */
    private $all_actions;
    /** @var array */
    private $members;
    /** @var \STM\DB\SQL\Condition */
    private $condition;

    private function __construct(MatchPlayerStatsSelection $selection)
    {
        $this->statistics = array();
        $this->all_actions = array();
        $this->members = $selection->getMembers();
        $this->condition = $selection->getWhereCondition();
        $this->loadStatistics();
    }

    public static function setORM(DatabaseObject $dbObject)
    {
        if (is_null(self::$dbActionStats)) {
            self::$dbActionStats = $dbObject;
        } elseif (is_null(self::$dbGeneralStats)) {
            self::$dbGeneralStats = $dbObject;
        }
    }

    public static function find(MatchPlayerStatsSelection $selection)
    {
        return array(new self($selection));
    }

    public static function count(MatchPlayerStatsSelection $selection)
    {
        throw new \BadFunctionCallException();
    }

    public function getStatistics()
    {
        return $this->statistics;
    }

    public function getAllMatchActions()
    {
        return $this->all_actions;
    }

    public function serialize()
    {
        $tables = array(
            'statistics' => $this->statistics,
            'all_actions' => $this->all_actions,
        );
        return serialize($tables);
    }

    public function unserialize($serialized)
    {
        $tables = unserialize($serialized);
        $this->statistics = $tables['statistics'];
        $this->all_actions = $tables['all_actions'];
    }

    private function loadStatistics()
    {
        $this->addTeamMembers();
        $this->addGeneralStatistics();
        $this->addActionsStatistics();
        if (empty($this->members)) {
            $this->loadMembers();// if only MatchSelection filter or no condition
        }
    }

    private function addTeamMembers()
    {
        foreach ($this->members as $teamMember) {
            if ($teamMember->isTeamPlayer()) {
                $this->statistics[$teamMember->getId()] = array(
                    'member' => $teamMember,
                    'general' => PlayerGeneralStatistic::getEmpty($teamMember),
                    'actions' => array(),
                );
            }
        }
    }

    private function addGeneralStatistics()
    {
        $query = MatchPlayerStatsSQL::getPlayerGeneralStats($this->condition);
        $stats = self::$dbGeneralStats->select($query, 'object', false);
        foreach ($stats as $stat) {
            $this->statistics[$stat->idTeamPlayer]['general'] = $stat;
        }
    }

    private function addActionsStatistics()
    {
        $query = MatchPlayerStatsSQL::getPlayerActionStats($this->condition);
        $stats = self::$dbActionStats->select($query, 'object', false);
        foreach ($stats as $stat) {
            $this->statistics[$stat->idTeamPlayer]['actions'][$stat->idMatchAction] = $stat;
            $this->all_actions[$stat->idMatchAction] = $stat->matchAction;
        }
    }

    private function loadMembers()
    {
        $members = StmFactory::find()->TeamMember->findAll();
        $allTeamMembers = Arrays::getAssocArray($members, 'getId');
        foreach (array_keys($this->statistics) as $id_team_member) {
            $this->statistics[$id_team_member]['member'] = $allTeamMembers[$id_team_member];
        }
    }
}
