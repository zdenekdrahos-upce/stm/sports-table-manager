<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Stats\Action;

use STM\Team\Member\TeamMember;

final class PlayerGeneralStatistic
{
    public $idTeamPlayer;
    public $playedMatches;
    public $playedTime;

    public static function getEmpty(TeamMember $teamMember)
    {
        $instance = new self();
        $instance->idTeamPlayer = $teamMember->getId();
        $instance->playedMatches = 0;
        $instance->playedTime = 0;
        return $instance;
    }
}
