<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Stats\Action;

use STM\Entities\AbstractEntityFactory;
use STM\Team\Team;
use STM\Person\Person;

class MatchPlayerStatsFactory extends AbstractEntityFactory
{
    public function findByCompetition($competition)
    {
        $methods = array('setCompetition' => $competition);
        return $this->entityHelper->setAndFindObject($methods);
    }

    public function findByTeam(Team $team, array $matchSelectionFilter)
    {
        $methods = array(
            'setTeam' => $team,
            'filterByMatchSelection' => $matchSelectionFilter
        );
        return $this->entityHelper->setAndFindObject($methods);
    }

    public function findByPerson(Person $person, array $matchSelectionFilter)
    {
        $methods = array(
            'setPerson' => $person,
            'filterByMatchSelection' => $matchSelectionFilter
        );
        return $this->entityHelper->setAndFindObject($methods);
    }

    protected function useEntityClassAsDb()
    {
        return true;
    }

    protected function getEntityClass()
    {
        return '\STM\Match\Stats\Action\MatchPlayerStats';
    }

    protected function getEntitySelection()
    {
        return new MatchPlayerStatsSelection();
    }
}
