<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Stats\Action;

final class PlayerActionStatistic
{
    public $idTeamPlayer;
    public $idMatchAction;
    public $matchAction;
    public $sum;
    public $count;
}
