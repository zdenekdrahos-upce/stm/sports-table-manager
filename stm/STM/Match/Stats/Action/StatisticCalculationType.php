<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Stats\Action;

use STM\Utils\Classes;

class StatisticCalculationType
{
    const COUNT = 'COUNT';
    const SUM = 'SUM';

    private function __construct()
    {
    }

    public static function getName($type)
    {
        if (Classes::isClassConstant('\STM\Match\Stats\Action\StatisticCalculationType', $type)) {
            return stmLang('constants', 'statisticcalculationtype', $type);
        }
        return 'Invalid Type';
    }

    public static function getValue($action_statistic)
    {
        if ($action_statistic instanceof PlayerActionStatistic) {
            if (STM_STATISTIC_CALCULATION == self::COUNT) {
                return $action_statistic->count;
            } else {
                return $action_statistic->sum;
            }
        }
        return 0;
    }
}
