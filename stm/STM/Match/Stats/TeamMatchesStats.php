<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Stats;

final class TeamMatchesStats
{
    /** @var array */
    public $matches;
    /** @var ResultTable */
    public $resultTable;
    /** @var int */
    public $idSelectedTeam;
    /** @var TableRowsStats */
    public $tableRowStatistics;
    /** @var TopMatches */
    public $highestAggregateScore;
    /** @var TopMatches */
    public $highestGoalFor;
    /** @var TopMatches */
    public $highestGoalAgainst;
    /** @var TopMatches */
    public $highestWin;
    /** @var TopMatches */
    public $highestLoss;

    public function __construct()
    {
        $this->matches = array();
        $this->resultTable = false;
        $this->idSelectedTeam = false;
        $this->tableRowStatistics = false;
        $this->highestAggregateScore = new TopMatches();
        $this->highestGoalFor = new TopMatches();
        $this->highestGoalAgainst = new TopMatches();
        $this->highestWin = new TopMatches();
        $this->highestLoss = new TopMatches();
    }
}
