<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Stats;

use STM\Stats\HomeAwayNumericStats;
use STM\Match\ExtendedMatch;

final class TopMatches
{
    /** @var \STM\Stats\HomeAwayNumericStats */
    public $topValues;
    /** @var array item = id of match with top valu */
    public $matchesHome;
    /** @var array item = id of match with top valu */
    public $matchesAway;

    public function __construct()
    {
        $this->topValues = new HomeAwayNumericStats();
        $this->matchesHome = array();
        $this->matchesAway = array();
    }

    public function checkMatch(ExtendedMatch $match, $value, $is_home)
    {
        $attribute = $is_home ? 'home' : 'away';
        if ($value >= $this->topValues->$attribute) {
            $matches_key = $is_home ? 'matchesHome' : 'matchesAway';
            if ($value > $this->topValues->$attribute) {
                $this->topValues->$attribute = $value;
                $this->$matches_key = array();
            }
            array_push($this->$matches_key, $match->match->getId());
        }
    }
}
