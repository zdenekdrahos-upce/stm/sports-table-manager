<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match;

use STM\Competition\Competition;
use STM\Competition\Playoff\Serie\PlayoffSerie;

class MatchSelectionBuilder
{

    /**
     * @example $matchSelection = array(
     *      'matchType' => \STM\Match\MatchSelection::ALL_MATCHES,
     *      'loadScores' => true,
     *      'loadPeriods' => true,
     *      'matchId' => 1,
     *      'competition' => 1 or $competition instanceof Competition,
     *      'teams' => array(1, 2, $team instanceof Team, ...)
     *      'headToHead' => false,
     *      'seasonRound' => 5,
     *      'seasonMaxRound' => 6,
     *      'playoffSerie' => $serie instanceof \STM\Competition\Playoff\PlayoffSerie,
     *      'dates' => array('min' => 'now', 'max' => 'now + 1 year'),
     *      'limit' => array('max' => 50, 'offset' => 0),
     *      'order' => array(
     *          //example: 'orderTypes' => bool $isAscending // order of types will influence final order
     *          'datetime' => true,
     *          'round' => true,
     *          'idMatch' => false,
     *      ),
     * );
     * @param array $matchSelection
     * @return array
     */
    public function build(array $matchSelection)
    {
        $selection = new MatchSelection();
        if (isset($matchSelection['matchType'])) {
            $selection->setMatchType($matchSelection['matchType']);
        }
        if (isset($matchSelection['loadScores'])) {
            $selection->setLoadingScores($matchSelection['loadScores']);
        }
        if (isset($matchSelection['loadPeriods'])) {
            $selection->setLoadingPeriods($matchSelection['loadPeriods']);
        }
        if (isset($matchSelection['matchId'])) {
            $selection->setId($matchSelection['matchId']);
        }
        if (isset($matchSelection['competition'])) {
            if ($matchSelection['competition'] instanceof Competition) {
                $selection->setCompetition($matchSelection['competition']);
            } else {
                $selection->setCompetitionId($matchSelection['competition']);
            }
        }
        if (isset($matchSelection['teams'])) {
            $isHeadToHead = isset($matchSelection['headToHead']) && $matchSelection['headToHead'] === true;
            $teamsMethod = $isHeadToHead ? 'setHeadToHead' : 'setSelectedTeams';
            $selection->$teamsMethod($matchSelection['teams']);
        }
        if (isset($matchSelection['yourTeamsOnly'])) {
            $selection->setYourTeamsOnly();
        }
        if (isset($matchSelection['seasonRound'])) {
            $selection->setSeasonRound($matchSelection['seasonRound']);
        }
        if (isset($matchSelection['seasonMaxRound'])) {
            $selection->setSeasonMaxRound($matchSelection['seasonMaxRound']);
        }
        if (isset($matchSelection['playoffSerie']) && $matchSelection['playoffSerie'] instanceof PlayoffSerie) {
            $selection->setPlayoffSerie($matchSelection['playoffSerie']);
        }
        if (isset($matchSelection['dates']) && is_array($matchSelection['dates'])) {
            $dates = $matchSelection['dates'];
            $min = isset($dates['min']) && is_string($dates['min']) ? $dates['min'] : null;
            $max = isset($dates['max']) && is_string($dates['max']) ? $dates['max'] : null;
            $selection->setDates($min, $max);
        }
        if (isset($matchSelection['limit']) && is_array($matchSelection['limit'])) {
            $limit = $matchSelection['limit'];
            $max = isset($limit['max']) && is_int($limit['max']) ? $limit['max'] : 5;
            $offset = isset($limit['offset']) && is_int($limit['offset']) ? $limit['offset'] : 0;
            $selection->setLimit($max, $offset);
        }
        if (isset($matchSelection['order']) && is_array($matchSelection['order'])) {
            static $orderMethods = array(
            'datetime' => 'orderByDatetime',
            'round' => 'orderByRound',
            'idMatch' => 'orderByIdMatch',
            );
            $order = array();
            foreach ($matchSelection['order'] as $type => $isAscending) {
                if (isset($orderMethods[$type])) {
                    $method = $orderMethods[$type];
                    $order[$method] = $isAscending;
                }
            }
            foreach ($order as $method => $isAscending) {
                $selection->$method($isAscending);
            }
        }
        return $selection;
    }
}
