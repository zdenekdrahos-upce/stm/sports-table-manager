<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match;

use STM\Team\Team;
use STM\Competition\Competition;
use STM\Competition\Playoff\Serie\PlayoffSerie;
use STM\DB\SQL\SelectQuery;
use STM\DB\SQL\JoinTable;
use STM\DB\SQL\JoinType;
use STM\Utils\Classes;
use STM\Entities\AbstractEntitySelection;

class MatchSelection extends AbstractEntitySelection
{
    /** Selection consists of all matches in database */
    const ALL_MATCHES = 'ALL_MATCHES';
    /** Selection consists of matches that were played (they have score in match_periods) */
    const PLAYED_MATCHES = 'PLAYED_MATCHES';
    /** Selection consists of matches that don't have score in match_periods */
    const UPCOMING_MATCHES = 'UPCOMING_MATCHES';

    /** @var MatchSelection constant from MatchSelection class */
    private $matchType;
    /** @var boolean - if load match score (e.g. to highlight winner, show score) */
    private $loadScores;
    /** @var boolean if load periods, periods are loaded in Match */
    private $loadPeriods;

    public function __construct()
    {
        parent::__construct();
        $this->matchType = MatchSelection::ALL_MATCHES;
        $this->loadPeriods = true;
        $this->loadScores = true;
    }

    public function getSelectQuery()
    {
        $query = parent::getSelectQuery();
        return $this->updateQueryWithMatchTypeAndScores($query);
    }

    public function isSetLimit()
    {
        return $this->selection->getLimit() != false;
    }

    /** @return boolean */
    public function loadPeriods()
    {
        return $this->loadPeriods;
    }

    /** @param MatchSelection $matchType constant from MatchSelection class */
    public function setMatchType($matchType)
    {
        if (Classes::isClassConstant('\STM\Match\MatchSelection', $matchType)) {
            $this->matchType = $matchType;
            if ($this->matchType == self::UPCOMING_MATCHES) {
                $this->loadScores = false;
                $this->loadPeriods = false;
                $this->selection->setNullAttribute('match_periods.id_match', true);
            }
        }
    }

    /** @param boolean $loadPeriods */
    public function setLoadingPeriods($loadPeriods)
    {
        $this->loadPeriods = ($this->matchType == self::UPCOMING_MATCHES) ? false : $loadPeriods;
    }

    /** @param boolean $loadScores */
    public function setLoadingScores($loadScores)
    {
        $this->loadScores = ($this->matchType == self::UPCOMING_MATCHES) ? false : $loadScores;
    }

    /** @param Competition $competition  */
    public function setCompetition(Competition $competition)
    {
        $this->selection->setAttributeWithValue('matches.id_competition', (string) $competition->getId());
    }

    /** @param int $id_competition  */
    public function setCompetitionId($id_competition)
    {
        if (is_numeric($id_competition)) {
            $this->selection->setAttributeWithValue('matches.id_competition', (string) $id_competition);
        }
    }

    /** @param array $teams */
    public function setSelectedTeams(array $teams)
    {
        $this->setTeamsInCondition($teams, false);
    }

    /** @param array $teams */
    public function setHeadToHead(array $teams)
    {
        $this->setTeamsInCondition($teams, true);
    }

    public function setYourTeamsOnly()
    {
        $this->query->setJoinTables(MatchSQL::joinHomeYourTeams());
        $this->query->setJoinTables(MatchSQL::joinAwayYourTeams());
        $this->selection->addCondition(MatchSQL::getJoinYourTeamsCondition());
    }

    /** @param numeric $id_match */
    public function setId($id_match)
    {
        if (is_numeric($id_match)) {
            $this->loadScores = true;
            $this->loadPeriods = true;
            $this->selection->setAttributeWithValue('matches.id_match', (string) $id_match);
        }
    }

    /** @param int $round */
    public function setSeasonRound($round)
    {
        if (is_numeric($round)) {
            $this->selection->setAttributeWithValue('matches.round', (string) $round);
        }
    }

    /** @param int $round */
    public function setSeasonMaxRound($round)
    {
        if (is_numeric($round)) {
            $this->selection->setAttributeWithValue('matches.round', (string) $round, '<=');
        }
    }

    /** @param \STM\Competition\Playoff\PlayoffSerie $playoff_serie */
    public function setPlayoffSerie(PlayoffSerie $playoff_serie)
    {
        $this->selection->setAttributeWithValue('matches.id_serie', (string) $playoff_serie->getId());
    }

    /**
     * @param string|null $date_min
     * @param string|null $date_max
     */
    public function setDates($date_min, $date_max)
    {
        $this->selection->setAttributeWithDates('matches.datetime', $date_min, $date_max);
    }

    /**
     * @param int $max_rows
     * @param int $offset
     */
    public function setLimit($max_rows, $offset = 0)
    {
        $this->selection->setRowsLimit($max_rows, $offset);
    }

    public function orderByDatetime($isAscending)
    {
        $this->selection->addColumnOrder('matches.datetime', $isAscending);
    }

    public function orderByRound($isAscending)
    {
        $this->selection->addColumnOrder('matches.round', $isAscending);
    }

    public function orderByIdMatch($isAscending)
    {
        $this->selection->addColumnOrder('matches.id_match', $isAscending);
    }

    private function setTeamsInCondition($teams, $and)
    {
        $ids = array();
        if (is_array($teams)) {
            foreach ($teams as $team) {
                if ($team instanceof Team) {
                    $ids[] = $team->getId();
                } elseif (is_numeric($team)) {// used in MatchFilter
                    $ids[] = $team;
                }
            }
        }
        $this->selection->setAttributesInMultipleColumns(
            array('matches.id_home', 'matches.id_away'),
            $ids,
            $and
        );
    }

    private function updateQueryWithMatchTypeAndScores(SelectQuery $query)
    {
        if ($this->loadScores) {
            $query->setColumns('match_scores.score_home, match_scores.score_away');
            $query->setJoinTables(
                new JoinTable(
                    $this->getJoinType(),
                    DT_MATCH_RESULTS . " match_scores",
                    'matches.id_match = match_scores.id_match'
                )
            );
        } else {
            $query->setJoinTables(
                new JoinTable(
                    $this->getJoinType(),
                    '(select distinct id_match from ' . DT_MATCH_PERIODS . ')match_periods',
                    'matches.id_match = match_periods.id_match'
                )
            );
        }
        return $query;
    }

    private function getJoinType()
    {
        return $this->matchType == MatchSelection::PLAYED_MATCHES ? JoinType::JOIN : JoinType::LEFT;
    }

    protected function getQuery()
    {
        return MatchSQL::selectMatches();
    }
}
