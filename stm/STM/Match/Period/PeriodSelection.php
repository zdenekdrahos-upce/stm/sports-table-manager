<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Period;

use STM\Match\Match;
use STM\Match\MatchSelection;
use STM\Entities\AbstractEntitySelection;

class PeriodSelection extends AbstractEntitySelection
{
    public function setMatch(Match $match)
    {
        $this->selection->setAttributeWithValue('match_periods.id_match', (string)$match->getId());
    }

    public function setMatchSelection(MatchSelection $matchSelection)
    {
        parent::addMatchSelectionFilter($matchSelection, 'match_periods');
    }

    protected function getQuery()
    {
        return PeriodSQL::selectMatchPeriods();
    }
}
