<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Period;

use STM\Match\Match;
use STM\DB\SQL\SelectQuery;
use STM\DB\SQL\JoinTable;
use STM\DB\SQL\JoinType;
use STM\DB\SQL\Condition;

class PeriodSQL
{
    /**
     * Selects all informations (id_match, id_period, score_home, score_away, note)
     * about each periods in database table 'MATCH_PERIODS', but table is also joined to
     * 'MATCHES' so you can use them in WHERE condition (e.g. matches.id_home = 5)
     * @return SelectQuery
     */
    public static function selectMatchPeriods()
    {
        $query = new SelectQuery(DT_MATCH_PERIODS . ' match_periods');
        $query->setColumns(
            'match_periods.id_match, match_periods.id_period, match_periods.score_home,
             match_periods.score_away, match_periods.note'
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                DT_MATCHES . ' matches',
                'matches.id_match = match_periods.id_match'
            )
        );
        $query->setGroupBy(
            'match_periods.id_match, match_periods.id_period, match_periods.score_home,
             match_periods.score_away, match_periods.note'
        );
        $query->setOrderBy('match_periods.id_match, id_period asc');
        return $query;
    }

    /** @return \STM\DB\SQL\Condition */
    public static function getMatchCondition(Match $match)
    {
        return new Condition('id_match = {M}', array('M' => $match->getId()));
    }
}
