<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Period;

use STM\DB\Object\DatabaseObject;
use \Serializable;
use STM\Match\Match;
use STM\Utils\Dates;
use STM\Match\MatchFactory;

/**
 * Period class
 * - instance: represents row from database table 'match_periods'
 * - static methods: for finding match periods, creating new match period, ...
 */
final class Period implements Serializable
{
    /** @var DatabaseObject */
    private static $db;

    /** @var int */
    private $id_match;
    /** @var int */
    private $id_period;
    /** @var int */
    private $score_home;
    /** @var int */
    private $score_away;
    /** @var string */
    private $note;

    private function __construct()
    {
    }

    public static function setORM(DatabaseObject $dbObject)
    {
        if (is_null(self::$db)) {
            self::$db = $dbObject;
        }
    }

    /**
     * Creates new row in database table 'periods' for specified $match
     * @param array $attributes e.g. array('match' => 'instance of class Match',
     * 'score_home' => 5, 'score_away' => '1')
     * @return boolean
     * Returns boolean to inform if creating operation was successful
     */
    public static function create($attributes)
    {
        if (PeriodValidator::checkCreate($attributes)) {
            return self::$db->insertValues(self::processAttributes($attributes));
        }
        return false;
    }

    /**
     * Processes array before creating - load id_match
     * @param array $attributes
     * @return array
     */
    private static function processAttributes($attributes)
    {
        if (STM_DB_TRIGGERS === false) {
            $attributes['id_period'] = self::countMatchPeriods($attributes['match']) + 1;
            $attributes['last_modified'] = Dates::stringToDatabaseDate('now');
        }
        $attributes['id_match'] = $attributes['match']->getId();
        unset($attributes['match']);
        return $attributes;
    }

    /**
     * Delete all match periods
     * @param Match $match
     * @return boolean
     * Return true if delete was successful. False if $match is not instance of
     * class Match or the match hasn't at least one period in database table 'periods' or fail in DB.
     */
    public static function deleteMatchPeriods(Match $match)
    {
        if (self::countMatchPeriods($match)) {
            $condition = PeriodSQL::getMatchCondition($match);
            return self::$db->deleteByCondition($condition);
        }
        return false;
    }

    /**
     * @param Match $match
     * @return int/false
     * Returns number of periods in database table 'periods' for $match from argument.
     * Returns false if $match is not instance of 'Match'.
     */
    private static function countMatchPeriods(Match $match)
    {
        return self::$db->selectCount(PeriodSQL::getMatchCondition($match));
    }


    // INSTANCE METHODS FOR ONE PERIOD
    /**
     * Deletes period from database
     * @return boolean
     */
    public function delete()
    {
        if (STM_DB_PROCEDURES === true) {
            return self::$db->executeProcedure(
                'delete_match_period',
                array($this->id_match, $this->id_period)
            );
        } else {
            return $this->deletePeriodWithoutProcedure();
        }
    }

    private function deletePeriodWithoutProcedure()
    {
        // 1.delete in database, but $this is still 'old period'
        $delete = self::$db->deleteById(array('match' => $this->id_match, 'period' => $this->id_period));
        // 2.change indexes (id_period) of remaining periods
        $matchFactory = new MatchFactory();
        $match = $matchFactory->findById($this->id_match);
        $periodFactory = new PeriodFactory();
        $periods = $periodFactory->findByMatch($match);
        foreach ($periods as $position => $period) {
            self::$db->updateById(
                array('match' => $period->id_match, 'period' => $period->id_period),
                array('id_period' => ($position + 1))
            );
        }
        return $delete;
    }

    /**
     * Updates period in database
     * @param array $attributes
     * @return boolean
     */
    public function update($attributes)
    {
        PeriodValidator::setPeriod($this);
        if (PeriodValidator::checkUpdate($attributes) && PeriodValidator::checkChange($attributes)) {
            if (STM_DB_TRIGGERS === false) {
                $attributes['last_modified'] = Dates::stringToDatabaseDate('now');
            }
            return self::$db->updateById(
                array('match' => $this->id_match, 'period' => $this->id_period),
                $attributes
            );
        }
        return false;
    }

    // GETTERS
    /** @return int */
    public function getIdPeriod()
    {
        return (int) $this->id_period;
    }

    /** @return int */
    public function getIdMatch()
    {
        return (int) $this->id_match;
    }

    /** @return int */
    public function getScoreHome()
    {
        return (int) $this->score_home;
    }

    /** @return int */
    public function getScoreAway()
    {
        return (int) $this->score_away;
    }

    /**
     * @return string/false
     * Returns false if note is not set
     */
    public function getNote()
    {
        return $this->isNoteSet() ? $this->note : false;
    }

    /**
     * Method for templating. Returns period as an array
     * @return array
     */
    public function toArray()
    {
        return array(
            'id_period' => $this->id_period,
            'score_home' => $this->score_home,
            'score_away' => $this->score_away,
            'note' => $this->getNote()
        );
    }

    /**
     * @return string
     * Returns 'score_home:score_away' and if note is set + '[note]'
     */
    public function __toString()
    {
        $note = $this->isNoteSet() ? " [{$this->note}]" : '';
        return $this->score_home . ':' . $this->score_away . $note;
    }

    public function serialize()
    {
        $vars = get_object_vars($this);
        return serialize($vars);
    }

    public function unserialize($serialized)
    {
        $vars = unserialize($serialized);
        foreach ($vars as $key => $value) {
            $this->$key = $value;
        }
    }

    private function isNoteSet()
    {
        return !(is_null($this->note) || $this->note === '');
    }
}
