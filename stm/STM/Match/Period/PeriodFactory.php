<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Period;

use STM\Match\Match;
use STM\Match\MatchSelection;
use STM\Entities\AbstractEntityFactory;

class PeriodFactory extends AbstractEntityFactory
{
    /**
     * Finds ALL periods for match sorted by id_period ascending
     * @param Match $match
     * @return array
     * Return array of Periods (even if no period exists)
     */
    public function findByMatch(Match $match)
    {
        $methods = array('setMatch' => $match);
        return $this->entityHelper->setAndFindArray($methods);
    }

    public function find(MatchSelection $matchSelection)
    {
        $methods = array('setMatchSelection' => $matchSelection);
        return $this->entityHelper->setAndFindArray($methods);
    }

    protected function getEntitySelection()
    {
         return new PeriodSelection();
    }

    protected function getEntityClass()
    {
        return 'STM\Match\Period\Period';
    }
}
