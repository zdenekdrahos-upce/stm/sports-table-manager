<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Period;

use STM\Helpers\ObjectValidator;
use STM\Web\Message\FormError;
use STM\Match\Match;
use STM\Utils\Arrays;

/**
 * Class for validating attributes for database table 'MATCH_PERIODS'
 * - validated attributes = score_home, score_away, note
 */
final class PeriodValidator
{
    /** @var ObjectValidator */
    private static $validator;

    /** @param \STM\Libs\FormProcessor $formProcessor */
    public static function init($formProcessor = false)
    {
        if (is_null(self::$validator)) {
            self::$validator = new ObjectValidator('\STM\Match\Period\Period');
        }
        self::$validator->setFormProcessor($formProcessor);
    }

    public static function setPeriod($period)
    {
        self::$validator->setComparedObject($period);
    }

    public static function resetPeriod()
    {
        self::$validator->resetComparedObject();
    }

    public static function checkCreate($attributes)
    {
        if (self::$validator->checkCreate(
            $attributes,
            array('match', 'score_home', 'score_away', 'note')
        )) {
            self::$validator->getFormProcessor()->checkCondition(
                $attributes['match'] instanceof Match,
                'Match must be instance of the class Match.'
            );
            self::checkFields($attributes);
        }
        return self::$validator->isValid();
    }

    public static function checkUpdate($attributes)
    {
        if (self::$validator->checkUpdate(
            $attributes,
            array('score_home', 'score_away', 'note')
        )) {
            self::checkFields($attributes);
            if (self::$validator->isValid()) {
                 self::checkChange($attributes);
            }
        }
        return self::$validator->isValid();
    }

    private static function checkFields($attributes)
    {
        $scores = array(
            'score_home' => $attributes['score_home'],
            'score_away' => $attributes['score_away']
        );
        self::checkScores($scores);
        self::checkNote($attributes['note']);
    }

    public static function checkScores($period)
    {
        self::$validator->getFormProcessor()->checkCondition(
            Arrays::isArrayValid($period, array('score_home', 'score_away')),
            'Invalid input array'
        );
        if (self::$validator->isValid()) {
            self::checkHomeScore($period['score_home']);
            self::checkAwayScore($period['score_away']);
        }
        return self::$validator->isValid();
    }

    private static function checkNote($note)
    {
        if (!empty($note) || is_numeric($note)) {
            self::$validator->checkString(
                $note,
                array('min' => 1, 'max' => 10),
                stmLang('match', 'period', 'note')
            );
        }
    }

    private static function checkHomeScore($score)
    {
        return self::checkScore($score, 'score-home');
    }

    private static function checkAwayScore($score)
    {
        return self::checkScore($score, 'score-away');
    }

    private static function checkScore($score, $name)
    {
        return self::$validator->checkNumber(
            $score,
            array('min' => 0, 'max' => 100),
            stmLang('match', $name)
        );
    }

    public static function checkChangeInPeriods(Match $match, array $periods)
    {
        $change = false;
        if ($match->hasLoadedPeriods()) {
            foreach ($match->getPeriods() as $period) {
                if (array_key_exists($period->getIdPeriod(), $periods)) {
                    self::setPeriod($period);
                    $new_period_attributes = $periods[$period->getIdPeriod()];
                    if (self::checkChange($new_period_attributes)) {
                        $change = true;
                        break;
                    }
                }
            }
        }
        self::$validator->getFormProcessor()->checkCondition(
            $change,
            FormError::get('no-change', array(stmLang('prediction', 'result')))
        );
        return self::$validator->isValid();
    }

    public static function checkChange($attributes)
    {
        $new_home = $attributes['score_home'] != self::$validator->getValueFromCompared('getScoreHome');
        $new_away = $attributes['score_away'] != self::$validator->getValueFromCompared('getScoreAway');
        $current_note = self::$validator->getValueFromCompared('getNote');
        if (!empty($current_note) || is_numeric($current_note)) {
            $new_note = $attributes['note'] !== $current_note;
        } else {
            $new_note = strlen($attributes['note']) > 0;
        }
        return $new_home || $new_away || $new_note;
    }
}
