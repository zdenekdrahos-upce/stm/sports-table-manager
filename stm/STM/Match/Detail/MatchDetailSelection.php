<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Detail;

use STM\Entities\AbstractEntitySelection;

class MatchDetailSelection extends AbstractEntitySelection
{

    public function setId($idMatch)
    {
        if (is_numeric($idMatch)) {
            $this->selection->setAttributeWithValue('match_details.id_match', (string) $idMatch);
        }
    }

    protected function getQuery()
    {
        return MatchDetailSQL::selectMatchDetails();
    }
}
