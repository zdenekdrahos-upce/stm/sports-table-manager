<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Detail;

use STM\Entities\AbstractEntityFactory;

class MatchDetailFactory extends AbstractEntityFactory
{
    public function findById($idMatch)
    {
        return $this->entityHelper->findById($idMatch);
    }

    protected function getEntitySelection()
    {
         return new MatchDetailSelection();
    }

    protected function getEntityClass()
    {
        return 'STM\Match\Detail\MatchDetail';
    }
}
