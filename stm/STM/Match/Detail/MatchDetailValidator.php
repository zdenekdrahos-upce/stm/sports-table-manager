<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Detail;

use STM\Helpers\ObjectValidator;
use STM\Web\Message\FormError;
use STM\Web\Message\SessionMessage;
use STM\Match\Match;
use STM\Stadium\Stadium;
use STM\StmFactory;

/**
 * Class for validating attributes for database table 'MATCH_DETAILS'
 * - attributes = match, spectators, match_comment, id_stadium
 */
final class MatchDetailValidator
{
    /** @var ObjectValidator */
    private static $validator;

    /** @param \STM\Libs\FormProcessor $formProcessor */
    public static function init($formProcessor = false)
    {
        if (is_null(self::$validator)) {
            self::$validator = new ObjectValidator('\STM\Match\Detail\MatchDetail');
        }
        self::$validator->setFormProcessor($formProcessor);
    }

    public static function setMatchDetail($match_detail)
    {
        self::$validator->setComparedObject($match_detail);
    }

    public static function resetMatchDetail()
    {
        self::$validator->resetComparedObject();
    }

    public static function checkCreate($attributes)
    {
        if (self::$validator->checkCreate($attributes, self::getAttributes())) {
            self::checkFields($attributes);
        }
        return self::$validator->isValid();
    }

    public static function checkUpdate($attributes)
    {
        if (self::$validator->checkUpdate($attributes, self::getAttributes())) {
            self::checkFields($attributes);
            self::checkChangeInMatchDetail($attributes);
        }
        return self::$validator->isValid();
    }

    public static function checkChangeInMatchDetail($details)
    {
        self::$validator->getFormProcessor()->checkCondition(
            self::$validator->isSetComparedObject(),
            'Match detail must be set'
        );
        if (self::$validator->isValid()) {
            $change = false;
            $current = self::getCurrentNonObjectAttributes();
            foreach ($current as $key => $value) {
                if ($value != $details[$key]) {
                    $change = true;
                    break;
                }
            }
            $change = self::checkChangeInObject($change, $details['id_stadium'], 'getIdStadium');
            self::$validator->getFormProcessor()->checkCondition(
                $change,
                FormError::get('no-change', array(stmLang('header', 'match', 'read', 'detail')))
            );
        }
        return self::$validator->isValid();
    }

    private static function checkFields($attributes)
    {
        self::checkMatch($attributes['match']);
        self::checkSpectators($attributes['spectators']);
        self::checkMatchComment($attributes['match_comment']);
        self::checkStadium($attributes['id_stadium']);
        if (self::$validator->isValid()) {
            self::checkSetAttributes($attributes);
        }
    }

    private static function checkMatch($match)
    {
        self::$validator->getFormProcessor()->checkCondition(
            $match instanceof Match,
            'Match must be existing match'
        );
    }

    private static function checkSpectators($spectators)
    {
        if (!empty($spectators) || is_numeric($spectators)) {
            self::$validator->checkNumber(
                $spectators,
                array('min' => 0, 'max' => 500000),
                stmLang('match', 'detail-spectators')
            );
        }
    }

    private static function checkMatchComment($match_comment)
    {
        if ($match_comment) {
            self::$validator->checkString(
                $match_comment,
                array('min' => 3, 'max' => 500),
                stmLang('match', 'detail-comment')
            );
        }
    }

    private static function checkStadium($id_stadium)
    {
        if (!empty($id_stadium)) {
            self::$validator->getFormProcessor()->checkCondition(
                StmFactory::find()->Stadium->findById($id_stadium) instanceof Stadium,
                'Stadium must be existing stadium'
            );
        }
    }

    private static function checkSetAttributes($attributes)
    {
        $isSet = is_numeric($attributes['spectators']) ||
            !empty($attributes['match_comment']) ||
            !empty($attributes['id_stadium']);
        self::$validator->getFormProcessor()->checkCondition(
            $isSet,
            SessionMessage::get('match-detail-notset')
        );
    }

    private static function getCurrentNonObjectAttributes()
    {
        $current = self::$validator->getValueFromCompared('toArray');
        unset($current['stadium']);
        return $current;
    }

    private static function checkChangeInObject($change, $new_value, $getter)
    {
        if ($change == false) {
            if (!empty($new_value)) {
                $change = $new_value != self::$validator->getValueFromCompared($getter);
            } else {
                $change = is_int(self::$validator->getValueFromCompared($getter));
            }
        }
        return $change;
    }

    private static function getAttributes()
    {
        return array('match', 'spectators', 'match_comment', 'id_stadium');
    }
}
