<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Detail;

use STM\DB\Object\DatabaseObject;

final class MatchDetail
{
    /** @var DatabaseObject */
    private static $db;

    /** @var int */
    private $id_match;
    /** @var int */
    private $spectators;
    /** @var string */
    private $match_comment;
    /** @var int */
    private $id_stadium;

    /** @var string */
    private $name_stadium;

    private function __construct()
    {
    }

    public static function setORM(DatabaseObject $dbObject)
    {
        if (is_null(self::$db)) {
            self::$db = $dbObject;
        }
    }

    public static function create($attributes)
    {
        if (MatchDetailValidator::checkCreate($attributes)) {
            return self::$db->insertValues(self::processAttributes($attributes), true);
        }
        return false;
    }

    public function update($attributes)
    {
        MatchDetailValidator::setMatchDetail($this);
        if (MatchDetailValidator::checkUpdate($attributes)) {
            return self::$db->updateById($this->id_match, self::processAttributes($attributes));
        }
        return false;
    }

    public function delete()
    {
        return self::$db->deleteById($this->id_match);
    }

    public function getId()
    {
        return (int) $this->id_match;
    }

    public function getIdStadium()
    {
        return is_numeric($this->id_stadium) ? (int) $this->id_stadium : false;
    }

    public function toArray()
    {
        return array(
            'spectators' => $this->spectators,
            'match_comment' => $this->match_comment,
            'stadium' => $this->name_stadium
        );
    }

    private static function processAttributes($attributes)
    {
        $attributes['id_match'] = $attributes['match']->getId();
        unset($attributes['match']);
        if (empty($attributes['id_stadium'])) {
            unset($attributes['id_stadium']);
        }
        return $attributes;
    }
}
