<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match\Detail;

use STM\DB\SQL\SelectQuery;
use STM\DB\SQL\JoinTable;
use STM\DB\SQL\JoinType;

class MatchDetailSQL
{

    /** @return SelectQuery */
    public static function selectMatchDetails()
    {
        $query = new SelectQuery(DT_MATCH_DETAILS . ' match_details');
        $query->setColumns(
            'id_match, spectators, match_comment, stadiums.id_stadium,
            stadiums.name as name_stadium'
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::LEFT,
                DT_STADIUMS . ' stadiums',
                'match_details.id_stadium = stadiums.id_stadium'
            )
        );
        return $query;
    }
}
