<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Match;

use STM\Helpers\ObjectValidator;
use STM\Web\Message\FormError;
use STM\Competition\Competition;
use STM\Competition\Season\Season;
use STM\Competition\Playoff\Playoff;
use STM\Competition\Playoff\Serie\PlayoffSerie;
use STM\StmFactory;

/**
 * Class for validating attributes for database table 'MATCHES'
 * - validated attributes = competition, home_team, away_team, datetime, round, serie
 */
final class MatchValidator
{
    /** @var ObjectValidator */
    private static $validator;

    /** @param \STM\Libs\FormProcessor $formProcessor */
    public static function init($formProcessor = false)
    {
        if (is_null(self::$validator)) {
            self::$validator = new ObjectValidator('\STM\Match\Match');
        }
        self::$validator->setFormProcessor($formProcessor);
    }

    public static function setMatch($match)
    {
        self::$validator->setComparedObject($match);
    }

    public static function resetMatch()
    {
        self::$validator->resetComparedObject();
    }

    public static function checkCreate($attributes)
    {
        if (self::$validator->checkCreate(
            $attributes,
            array('competition', 'home_team', 'away_team', 'datetime', 'round', 'serie')
        )) {
            self::$validator->getFormProcessor()->checkCondition(
                $attributes['competition'] instanceof Competition,
                'Competition must be instance of class Competition.'
            );
            if (self::$validator->isValid()) {
                self::checkFields($attributes['competition'], $attributes);
            }
        }
        return self::$validator->isValid();
    }

    public static function checkUpdate($attributes)
    {
        if (self::$validator->checkUpdate(
            $attributes,
            array('home_team', 'away_team', 'datetime', 'round', 'serie')
        )) {
            $competition = StmFactory::find()->Competition->findById(
                self::$validator->getValueFromCompared('getIdCompetition')
            );
            self::checkFields($competition, $attributes);
            if (self::$validator->isValid()) {
                self::checkChange($attributes);
            }
        }
        return self::$validator->isValid();
    }

    private static function checkFields($competition, $attributes)
    {
        self::checkDate($attributes['datetime'], $competition);
        self::checkTeams($attributes['home_team'], $attributes['away_team'], $competition);
        if (self::$validator->isValid()) {
            if ($competition instanceof Season) {
                self::$validator->getFormProcessor()->checkCondition(
                    $attributes['serie'] === false,
                    FormError::get('match-playoff-serie-seas')
                );
                self::checkSeasonMatch($competition, $attributes);
            } elseif ($competition instanceof Playoff) {
                self::$validator->getFormProcessor()->checkCondition(
                    $attributes['round'] === false,
                    FormError::get('match-seasonround-play')
                );
                self::checkPlayoffSerie($attributes);
            } else {
                self::$validator->getFormProcessor()->checkCondition(
                    $attributes['round'] === false,
                    FormError::get('match-seasonround-comp')
                );
                self::$validator->getFormProcessor()->checkCondition(
                    $attributes['serie'] === false,
                    FormError::get('match-playoffserie-comp')
                );
            }
        }
    }

    public static function checkDate($date, $competition)
    {
        self::$validator->checkOptionalDate($date, stmLang('match', 'date'));
        if (self::$validator->isValid() && !empty($date)) {
            self::$validator->getFormProcessor()->checkCondition(
                $competition->isValidDate($date),
                FormError::get('date-integrity')
            );
        }
        return self::$validator->isValid();
    }

    private static function checkTeams($home_team, $away_team, $competition)
    {
        self::$validator->getFormProcessor()->checkCondition(
            $competition->isTeamFromCompetition($home_team),
            FormError::get('import-invalid-team', array($home_team))
        );
        self::$validator->getFormProcessor()->checkCondition(
            $competition->isTeamFromCompetition($away_team),
            FormError::get('import-invalid-team', array($away_team))
        );
        if (self::$validator->isValid()) {
            self::$validator->getFormProcessor()->checkCondition(
                $home_team->getId() != $away_team->getId(),
                FormError::get('match-teams')
            );
        }
    }

    private static function checkChange($attributes)
    {
        $new_home_team = $attributes['home_team']->getId() !=
            self::$validator->getValueFromCompared('getIdTeamHome');
        $new_away_team = $attributes['away_team']->getId() !=
            self::$validator->getValueFromCompared('getIdTeamAway');
        $current_date = self::$validator->getValueFromCompared('getDate');
        $new_date = $attributes['datetime'] != $current_date->__toString();
        $new_round = $attributes['round'] != self::$validator->getValueFromCompared('getRound');
        // serie cannot be changed
        $is_different = $new_home_team || $new_away_team || $new_date || $new_round;
        self::$validator->getFormProcessor()->checkCondition(
            $is_different,
            FormError::get('no-change', array(stmLang('competition', 'match')))
        );
    }

    // PLAYOFF VALIDATOR
    private static function checkPlayoffSerie($attributes)
    {
        if ($attributes['serie'] !== false) {
            self::$validator->getFormProcessor()->checkCondition(
                $attributes['serie'] instanceof PlayoffSerie,
                'Serie must be instance of class PlayoffSerie.'
            );
            if (self::$validator->isValid()) {
                $serie_teams = array($attributes['serie']->getIdTeam1(), $attributes['serie']->getIdTeam2());
                self::$validator->getFormProcessor()->checkCondition(
                    in_array($attributes['home_team']->getId(), $serie_teams),
                    'Home team is not from serie'
                );
                self::$validator->getFormProcessor()->checkCondition(
                    in_array($attributes['away_team']->getId(), $serie_teams),
                    'Away team is not from serie'
                );
                if (!self::$validator->isSetComparedObject()) {
                    // new match cannot be created in closed serie, but match can be updated
                    self::$validator->getFormProcessor()->checkCondition(
                        is_null($attributes['serie']->getIdNextSerie()),
                        FormError::get('match-serie')
                    );
                }
            }
        } else {
            self::$validator->getFormProcessor()->addError('For playoff serie must be set');
        }
    }

    // SEASON VALIDATOR
    private static function checkSeasonMatch($competition, $attributes)
    {
        if ($attributes['round'] !== false) {
            self::$validator->checkNumber(
                $attributes['round'],
                array('min' => 1, 'max' => $competition->getMaxRound()),
                stmLang('match', 'season-round')
            );
            self::checkSeasonMaxMatches($competition, $attributes['home_team'], $attributes['away_team']);
            self::checkSeasonRoundTeams(
                $competition,
                $attributes['round'],
                $attributes['home_team'],
                $attributes['away_team']
            );
        } else {
            self::$validator->getFormProcessor()->addError(FormError::get('match-seasonround-set'));
        }
    }

    private static function checkSeasonRoundTeams($competition, $round, $home, $away)
    {
        // team count in one round == 1
        if (!self::$validator->isSetComparedObject() || self::isRoundDifferent($round)) {
            $match_selection = new MatchSelection();
            $match_selection->setMatchType(MatchSelection::ALL_MATCHES);
            $match_selection->setLoadingPeriods(false);
            $match_selection->setLoadingScores(false);
            $match_selection->setCompetition($competition);
            $match_selection->setSelectedTeams(array($home, $away));
            $match_selection->setSeasonRound($round);
            $count = Match::count($match_selection);
            self::$validator->getFormProcessor()->checkCondition(
                $count == 0,
                FormError::get('match-seasonround')
            );
        }
    }

    private static function checkSeasonMaxMatches($competition, $home, $away)
    {
        // count of team matches in season == Season::getSeasonPeriods
        if (!self::$validator->isSetComparedObject() || self::areTeamsDifferent($home, $away)) {
            $match_selection = new MatchSelection();
            $match_selection->setMatchType(MatchSelection::ALL_MATCHES);
            $match_selection->setLoadingPeriods(false);
            $match_selection->setLoadingScores(false);
            $match_selection->setCompetition($competition);
            $match_selection->setHeadToHead(array($home, $away));
            $count = Match::count($match_selection);
            self::$validator->getFormProcessor()->checkCondition(
                $count < $competition->getSeasonPeriods(),
                FormError::get('match-seasonperiods', array($competition->getSeasonPeriods()))
            );
        }
    }

    private static function isRoundDifferent($round)
    {
        if (self::$validator->isSetComparedObject()) {
            return $round != self::$validator->getValueFromCompared('getRound');
        }
        return true;
    }

    private static function areTeamsDifferent($home_team, $away_team)
    {
        $current_team_ids = array(
            self::$validator->getValueFromCompared('getIdTeamHome'),
            self::$validator->getValueFromCompared('getIdTeamAway')
        );
        $new_home_team = !in_array($home_team->getId(), $current_team_ids);
        $new_away_team = !in_array($away_team->getId(), $current_team_ids);
        return $new_home_team || $new_away_team;
    }
}
