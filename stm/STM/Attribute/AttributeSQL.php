<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Attribute;

use STM\DB\Database;
use STM\DB\SQL\SelectQuery;
use STM\DB\SQL\JoinTable;
use STM\DB\SQL\JoinType;

class AttributeSQL
{

    /** @return SelectQuery */
    public static function selectAttributes()
    {
        $query = new SelectQuery(DT_ATTRIBUTES . ' attributes');
        $query->setColumns(
            'attributes.id_attribute, attributes.attribute, ' .
            Database::getNullFunction() . '(attribute_usage, 0) as attribute_usage'
        );
        $subquery = self::getGroupedAttributesCount();
        $query->setJoinTables(
            new JoinTable(
                JoinType::LEFT,
                "({$subquery->generate()}) attusage",
                'attributes.id_attribute = attusage.id_attribute'
            )
        );
        $query->setOrderBy('attributes.attribute');
        return $query;
    }

    /** @return SelectQuery */
    private static function getGroupedAttributesCount()
    {
        $subquery = new SelectQuery(DT_PERSONS_ATTRIBUTES);
        $subquery->setColumns('id_attribute, count(*) as attribute_usage');
        $subquery->setGroupBy('id_attribute');
        Database::escapeQuery($subquery);
        return $subquery;
    }
}
