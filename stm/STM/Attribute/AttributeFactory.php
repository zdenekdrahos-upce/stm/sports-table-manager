<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Attribute;

use STM\Entities\AbstractEntityFactory;

class AttributeFactory extends AbstractEntityFactory
{
    public function findById($id)
    {
        return $this->entityHelper->findById($id);
    }

    public function findAll()
    {
        return $this->entityHelper->findAll();
    }

    protected function getEntitySelection()
    {
         return new AttributeSelection();
    }

    protected function getEntityClass()
    {
        return '\STM\Attribute\Attribute';
    }
}
