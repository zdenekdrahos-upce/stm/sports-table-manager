<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Attribute;

use STM\Helpers\ObjectValidator;

/**
 * Class for validating attributes for database table 'ATTRIBUTES'
 * - attributes = attribute (name)
 */
final class AttributeValidator
{
    /** @var ObjectValidator */
    private static $validator;

    /** @param \STM\Libs\FormProcessor $formProcessor */
    public static function init($formProcessor = false)
    {
        if (is_null(self::$validator)) {
            self::$validator = new ObjectValidator('\STM\Attribute\Attribute');
        }
        self::$validator->setFormProcessor($formProcessor);
    }

    public static function setAttribute($attribute)
    {
        self::$validator->setComparedObject($attribute);
    }

    public static function resetAttribute()
    {
        self::$validator->resetComparedObject();
    }

    public static function checkCreate($attributes)
    {
        if (self::$validator->checkCreate($attributes, array('attribute'))) {
            self::checkAttributeName($attributes['attribute']);
        }
        return self::$validator->isValid();
    }

    public static function checkAttributeName($new_name)
    {
        return self::$validator->checkUniqueName($new_name, array('min' => 2, 'max' => 30), '__toString');
    }
}
