<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Attribute;

use STM\DB\Object\DatabaseObject;

final class Attribute
{
    /** @var DatabaseObject */
    private static $db;
    /** @var int */
    private $id_attribute;
    /** @var string */
    private $attribute;
    /** @var int */
    private $attribute_usage;

    private function __construct()
    {
    }

    public static function setORM(DatabaseObject $dbObject)
    {
        if (is_null(self::$db)) {
            self::$db = $dbObject;
        }
    }

    public static function create($attributes)
    {
        if (AttributeValidator::checkCreate($attributes)) {
            return self::$db->insertValues($attributes);
        }
        return false;
    }

    public static function exists($attributeName)
    {
        return self::$db->existsItem('attribute', $attributeName);
    }

    public function delete()
    {
        if ($this->canBeDeleted()) {
            return self::$db->deleteById($this->id_attribute);
        }
        return false;
    }

    public function canBeDeleted()
    {
        return $this->attribute_usage == 0;
    }

    public function updateAttributeName($name)
    {
        AttributeValidator::setAttribute($this);
        if (AttributeValidator::checkAttributeName($name)) {
            return self::$db->updateById($this->id_attribute, array('attribute' => $name));
        }
        return false;
    }

    public function getId()
    {
        return (int) $this->id_attribute;
    }

    public function toArray()
    {
        return array(
            'attribute' => $this->attribute,
            'usage' => (int) $this->attribute_usage
        );
    }

    public function __toString()
    {
        return $this->attribute;
    }
}
