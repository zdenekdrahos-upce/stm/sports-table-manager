<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Attribute;

use STM\Entities\AbstractEntitySelection;

class AttributeSelection extends AbstractEntitySelection
{

    public function setId($idAttribute)
    {
        if (is_numeric($idAttribute)) {
            $this->selection->setAttributeWithValue('attributes.id_attribute', (string) $idAttribute);
        }
    }

    protected function getQuery()
    {
        return AttributeSQL::selectAttributes();
    }
}
