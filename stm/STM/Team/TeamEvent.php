<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Team;

use STM\Helpers\Event;

/**
 * TeamEvent class
 * - for saving events related with Team
 */
class TeamEvent
{
    /** Creating new team */
    const CREATE = 'Create Team';
    /** Updating team - update name */
    const UPDATE = 'Update Team';
    /** Deleting team - add flag deleted, but appearances of the team in competitions will stay */
    const DELETE = 'Delete Team';
    /** Team Member - create/update/delete */
    const TEAM_MEMBER = 'Team Member';
    /** Countries - create/update/delete */
    const COUNTRY = 'Country';
    /** Stadiums - create/update/delete */
    const STADIUM = 'Stadium';
    /** Clubs - create/update/delete */
    const CLUB = 'Club';
    /** Persons - create/update/delete */
    const PERSON = 'Person';
    /** Person's attribute - create/update/delete */
    const PERSON_ATTRIBUTE = 'Person Attribute';
    /** Person's position - create/update/delete */
    const PERSON_POSITION = 'Person Position';
    /** Sports - create/update/delete */
    const SPORT = 'Sport';
    /** Import team and clubs */
    const IMPORT = 'Import Teams & Clubs';
    /** Import persons */
    const IMPORT_PERSONS = 'Import Persons';

    private function __construct()
    {
    }

    /**
     * Saves events related with Team
     * @param TeamEvent $teamEvent constant from TeamEvent class
     * @param string $message description of event
     * @param boolean $result if event was sucessful or fail
     * @return boolean
     * Returns true if event was successfuly saved, otherwise false (bad arguments
     * or error during file saving)
     */
    public static function log($teamEvent, $message, $result)
    {
        $event = new Event('\STM\Team\TeamEvent', 'teams');
        $event->setMessage($teamEvent, $message, $result);
        return $event->logEvent();
    }
}
