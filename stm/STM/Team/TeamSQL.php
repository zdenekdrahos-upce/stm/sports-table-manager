<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Team;

use STM\DB\SQL\SelectQuery;
use STM\DB\SQL\JoinType;
use STM\DB\SQL\JoinTable;
use STM\DB\SQL\Condition;
use STM\DB\Query\QueryFactory;

class TeamSQL
{

    /** @return SelectQuery */
    public static function selectTeams()
    {
        $query = new SelectQuery(DT_TEAMS . ' teams');
        $query->setColumns(
            'teams.id_team, teams.name, teams.id_club, clubs.name as club_name'
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                DT_CLUBS . ' clubs',
                'clubs.id_club = teams.id_club'
            )
        );
        $query->setOrderBy('clubs.name, teams.name');
        return $query;
    }

    /** @return JoinTable */
    public static function getJoinCompetitionsTable()
    {
        return new JoinTable(
            JoinType::JOIN,
            DT_COMPETITIONS_TEAMS . ' competitions_teams',
            'teams.id_team = competitions_teams.id_team'
        );
    }

    /** @return SelectQuery */
    public static function selectIncidenceOfTeam(Team $team)
    {
        $teamCondition = new Condition(
            'id_team = {I}',
            array('I' => $team->getId())
        );
        $matchCondition = new Condition(
            'id_home = {I} OR id_away = {I}',
            array('I' => $team->getId())
        );
        $tables = array(
            DT_COMPETITIONS_TEAMS => $teamCondition,
            DT_TEAMS_PERSONS => $teamCondition,
            DT_MATCHES => $matchCondition
        );
        return QueryFactory::selectIncidence($tables);
    }
}
