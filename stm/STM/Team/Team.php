<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Team;

use STM\DB\Object\DatabaseObject;
use \Serializable;

final class Team implements Serializable
{
    /** @var DatabaseObject */
    private static $db;

    /** @var int */
    private $id_team;
    /** @var string */
    private $name;
    /** @var int */
    private $id_club;
    /** @var string */
    private $club_name;

    private function __construct()
    {
    }

    public static function setORM(DatabaseObject $dbObject)
    {
        if (is_null(self::$db)) {
            self::$db = $dbObject;
        }
    }

    public static function create($attributes)
    {
        if (TeamValidator::checkCreate($attributes)) {
            return self::$db->insertValues(self::processAttributes($attributes));
        }
        return false;
    }

    public static function exists($team_name)
    {
        return self::$db->existsItem('name', $team_name);
    }

    public function update($attributes)
    {
        TeamValidator::setTeam($this);
        if (TeamValidator::checkUpdate($attributes)) {
            return self::$db->updateById($this->id_team, self::processAttributes($attributes));
        }
        return false;
    }

    public function delete()
    {
        if ($this->canBeDeleted()) {
            return self::$db->deleteById($this->id_team);
        }
        return false;
    }

    public function canBeDeleted()
    {
        $query = TeamSQL::selectIncidenceOfTeam($this);
        return self::$db->selectCountInQuery($query) == 0;
    }

    /** @return int */
    public function getId()
    {
        return (int) $this->id_team;
    }

    /** @return int */
    public function getIdClub()
    {
        return (int) $this->id_club;
    }

    /** @return string */
    public function getNameClub()
    {
        return $this->club_name;
    }

    /** @return array */
    public function toArray()
    {
        return array(
            'name' => $this->name,
            'club_name' => $this->club_name
        );
    }

    /** @return string returns name of the team */
    public function __toString()
    {
        return $this->name;
    }

    public function serialize()
    {
        $vars = get_object_vars($this);
        return serialize($vars);
    }

    public function unserialize($serialized)
    {
        $vars = unserialize($serialized);
        foreach ($vars as $key => $value) {
            $this->$key = $value;
        }
    }

    private static function processAttributes($attributes)
    {
        $attributes['id_club'] = $attributes['club']->getId();
        unset($attributes['club']);
        return $attributes;
    }
}
