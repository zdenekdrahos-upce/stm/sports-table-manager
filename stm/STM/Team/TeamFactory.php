<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Team;

use STM\Entities\AbstractEntityFactory;
use STM\Club\Club;
use STM\Competition\Competition;

class TeamFactory extends AbstractEntityFactory
{
    public function findById($idTeam)
    {
        return $this->entityHelper->findById($idTeam);
    }

    public function findByClub(Club $club)
    {
        $methods = array('setClub' => $club);
        return $this->entityHelper->setAndFindArray($methods);
    }

    public function findByCompetition(Competition $competition)
    {
        $methods = array('setCompetition' => $competition);
        return $this->entityHelper->setAndFindArray($methods);
    }

    public function findYourTeams()
    {
        $methods = array('setYourTeamsOnly' => null);
        return $this->entityHelper->setAndFindArray($methods);
    }

    public function findAll()
    {
        return $this->entityHelper->findAll();
    }

    protected function getEntitySelection()
    {
         return new TeamSelection();
    }

    protected function getEntityClass()
    {
        return 'STM\Team\Team';
    }
}
