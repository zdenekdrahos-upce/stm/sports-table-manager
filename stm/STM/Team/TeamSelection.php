<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Team;

use STM\Club\Club;
use STM\Competition\Competition;
use STM\Entities\AbstractEntitySelection;
use STM\DB\SQL\Condition;

class TeamSelection extends AbstractEntitySelection
{

    public function setId($idTeam)
    {
        if (is_numeric($idTeam)) {
            $this->selection->setAttributeWithValue('teams.id_team', (string) $idTeam);
        }
    }

    public function setClub(Club $club)
    {
        $this->selection->setAttributeWithValue('teams.id_club', (string) $club->getId());
    }

    public function setCompetition(Competition $competition)
    {
        $this->query->setJoinTables(TeamSQL::getJoinCompetitionsTable());
        $condition = new Condition(
            "competitions_teams.id_competition = {C}",
            array('C' => $competition->getId())
        );
        $this->selection->addCondition($condition);
    }

    public function setYourTeamsOnly()
    {
        $this->query->setJoinTables(TeamSQL::getJoinCompetitionsTable());
        $condition = new Condition("competitions_teams.your_team = {T}", array('T' => 'Y'));
        $this->query->setDistinct();
        $this->selection->addCondition($condition);
    }

    protected function getQuery()
    {
        return TeamSQL::selectTeams();
    }
}
