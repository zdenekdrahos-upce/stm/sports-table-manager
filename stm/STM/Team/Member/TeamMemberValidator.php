<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Team\Member;

use STM\Helpers\ObjectValidator;
use STM\Web\Message\FormError;
use STM\Team\Team;
use STM\Person\Person;
use STM\StmFactory;
use STM\Person\Position\PersonPosition;
use STM\Utils\Dates;

/**
 * Class for validating attributes for database table 'TEAMS_PERSONS'
 * - attributes = club, person, position, date_since, date_to
 */
final class TeamMemberValidator
{
    /** @var ObjectValidator */
    private static $validator;

    /** @param \STM\Libs\FormProcessor $formProcessor */
    public static function init($formProcessor = false)
    {
        if (is_null(self::$validator)) {
            self::$validator = new ObjectValidator('\STM\Team\Member\TeamMember');
        }
        self::$validator->setFormProcessor($formProcessor);
    }

    public static function setTeamMember($team_member)
    {
        self::$validator->setComparedObject($team_member);
    }

    public static function resetTeamMember()
    {
        self::$validator->resetComparedObject();
    }

    public static function checkCreate($attributes)
    {
        if (self::$validator->checkCreate(
            $attributes,
            array('team', 'person', 'position', 'date_since', 'date_to', 'is_player', 'dress_number')
        )) {
            self::$validator->getFormProcessor()->checkCondition(
                $attributes['team'] instanceof Team,
                'Invalid team'
            );
            self::$validator->getFormProcessor()->checkCondition(
                $attributes['person'] instanceof Person,
                'Invalid person'
            );
            self::checkPosition($attributes['position']);
            self::checkDates($attributes['date_since'], $attributes['date_to'], $attributes['person']);
            if ($attributes['is_player']) {
                self::checkDressNumber($attributes['dress_number']);
            }
        }
        return self::$validator->isValid();
    }

    public static function checkDressNumber($dress_number)
    {
        if ($dress_number !== '') {
            self::$validator->checkNumber(
                $dress_number,
                array('min' => 0, 'max' => 255),
                stmLang('team', 'dress-number')
            );
        }
        return self::$validator->isValid();
    }

    public static function checkUpdate($attributes)
    {
        self::$validator->getFormProcessor()->checkCondition(
            self::$validator->isSetComparedObject(),
            'Updated team member is not set'
        );
        if (self::$validator->checkUpdate(
            $attributes,
            array('position', 'date_since', 'date_to', 'is_player', 'dress_number')
        )) {
            $person = StmFactory::find()->Person->findById(self::$validator->getValueFromCompared('getIdPerson'));
            self::checkPosition($attributes['position']);
            self::checkDates($attributes['date_since'], $attributes['date_to'], $person);
            if ($attributes['is_player']) {
                self::checkDressNumber($attributes['dress_number']);
            }
            if (self::$validator->isValid()) {
                self::checkChange($attributes);
            }
        }
        return self::$validator->isValid();
    }

    private static function checkPosition($position)
    {
        self::$validator->getFormProcessor()->checkCondition(
            $position instanceof PersonPosition,
            'Invalid position'
        );
    }

    private static function checkDates($since, $to, $person)
    {
        self::$validator->checkOptionalDate($since, stmLang('person', 'since'));
        self::$validator->checkOptionalDate($to, stmLang('person', 'to'));
        self::checkDateIntegrity($since, $to);
        if (self::$validator->isValid()) {
            self::compareDatesToBirthDate($since, $to, $person);
        }
    }

    private static function checkDateIntegrity($since, $to)
    {
        if (!empty($since) && !empty($to)) {
            self::$validator->getFormProcessor()->checkCondition(
                Dates::areDatesInOrder($since, $to),
                FormError::get('date-integrity')
            );
        }
    }

    private static function compareDatesToBirthDate($since, $to, $person)
    {
        $person_birth = $person->getBirthDate();
        if ($person_birth->isValid()) {
            $person_birth = $person_birth->__toString();
            if (!empty($since)) {
                self::$validator->getFormProcessor()->checkCondition(
                    Dates::areDatesInOrder($person_birth, $since),
                    FormError::get('compare-birth-date')
                );
            }
            if (!empty($to)) {
                self::$validator->getFormProcessor()->checkCondition(
                    Dates::areDatesInOrder($person_birth, $to),
                    FormError::get('compare-birth-date')
                );
            }
        }
    }

    private static function checkChange($attributes)
    {
        $new_position = $attributes['position']->getId() !=
            self::$validator->getValueFromCompared('getIdPosition');
        $new_since = $attributes['date_since'] !=
            self::$validator->getValueFromCompared('getDateSince');
        $new_to = $attributes['date_to'] !=
            self::$validator->getValueFromCompared('getDateTo');
        $new_player_status = $attributes['is_player'] !=
            self::$validator->getValueFromCompared('is_team_player');
        $new_dress_number = $attributes['dress_number'] !=
            self::$validator->getValueFromCompared('getDressNumber');
        $is_different = $new_position || $new_since || $new_to || $new_player_status || $new_dress_number;
        self::$validator->getFormProcessor()->checkCondition(
            $is_different,
            FormError::get('no-change', array(stmLang('team', 'person')))
        );
    }
}
