<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Team\Member;

use STM\DB\Object\DatabaseObject;
use STM\Team\Member\TeamMember;
use STM\Team\Member\TeamMemberValidator;

class TeamPlayerHelper
{
    /** @var DatabaseObject */
    private static $db;

    /** @var \STM\Team\Member\TeamMember */
    private $teamMember;

    public static function setORM(DatabaseObject $dbObject)
    {
        if (is_null(self::$db)) {
            self::$db = $dbObject;
        }
    }

    public function __construct(TeamMember $teamMember)
    {
        $this->teamMember = $teamMember;
    }

    public static function create($idTeamMember, $dressNumber)
    {
        $data = array(
            'id_team_player' => $idTeamMember,
            'dress_number' => $dressNumber ? $dressNumber : ''
        );
        return self::$db->insertValues($data, true);
    }

    public function createTeamPlayer()
    {
        return self::$db->insertValues(
            array('id_team_player' => $this->teamMember->getId())
        );
    }

    // no check of change, only check if empty or valid number
    public function updateDressNumber($dress_number)
    {
        TeamMemberValidator::setTeamMember($this->teamMember);
        if (TeamMemberValidator::checkDressNumber($dress_number)) {
            return self::$db->updateById(
                $this->teamMember->getId(),
                array('dress_number' => $dress_number ? $dress_number : '')
            );
        }
        return false;
    }

    public function deleteTeamPlayer()
    {
        return self::$db->deleteById($this->teamMember->getId());
    }
}
