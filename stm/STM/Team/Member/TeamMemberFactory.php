<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Team\Member;

use STM\Team\Team;
use STM\Match\Match;
use STM\Competition\Competition;
use STM\Person\Person;
use STM\Utils\Dates;
use STM\Person\Position\PersonPosition;
use STM\Entities\AbstractEntityFactory;

final class TeamMemberFactory extends AbstractEntityFactory
{
    public function findById($id)
    {
        return $this->entityHelper->findById($id);
    }

    public function findAll()
    {
        return $this->entityHelper->findAll();
    }

    public function findByIdTeam($idTeam)
    {
        $methods = array('setIdTeam' => $idTeam);
        return $this->entityHelper->setAndFindArray($methods);
    }

    public function findByTeam(Team $team)
    {
        $methods = array('setTeam' => $team);
        return $this->entityHelper->setAndFindArray($methods);
    }

    public function findByPerson(Person $person)
    {
        $methods = array('setPerson' => $person);
        return $this->entityHelper->setAndFindArray($methods);
    }

    public function findByMatch(Match $match)
    {
        $methods = array('setMatch' => $match);
        return $this->entityHelper->setAndFindArray($methods);
    }

    public function findByPosition(PersonPosition $position)
    {
        $methods = array('setPosition' => $position);
        return $this->entityHelper->setAndFindArray($methods);
    }

    public function findCompetitionPlayers(Competition $competition)
    {
        $methods = array(
            'setOnlyPlayers' => null,
            'setCompetition' => $competition
        );
        return $this->entityHelper->setAndFindArray($methods);
    }

    public function findMatchPlayers(Team $team, Competition $competition, Match $match)
    {
        $methods = array(
            'setOnlyPlayers' => null,
            'setTeam' => $team,
            'setCompetition' => $competition
        );
        if ($match->getDate()->isValid()) {
            $methods['setDate'] = array($match->getDate());
        }
        return $this->entityHelper->setAndFindArray($methods);
    }

    public function filterTeamMembers(Team $team, array $filterArray)
    {
        $methods = array('setTeam' => $team);
        if (isset($filterArray['date']) && Dates::stringToDatabaseDate($filterArray['date'])) {
            $methods['setDate'] = $filterArray['date'];
        }
        if (isset($filterArray['persons'])) {
            if ($filterArray['persons'] == 'p') {
                $methods['setOnlyPlayers'] = null;
            } elseif ($filterArray['persons'] == 't') {
                $methods['setOnlyNonPlayers'] = null;
            }
        }
        return $this->entityHelper->setAndFindArray($methods);
    }

    protected function getEntitySelection()
    {
         return new TeamMemberSelection();
    }

    protected function getEntityClass()
    {
        return 'STM\Team\Member\TeamMember';
    }
}
