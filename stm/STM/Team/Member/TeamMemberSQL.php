<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Team\Member;

use STM\DB\SQL\SelectQuery;
use STM\DB\SQL\JoinType;
use STM\DB\SQL\JoinTable;
use STM\DB\SQL\Condition;

class TeamMemberSQL
{

    /** @return SelectQuery */
    public static function selectTeamMembers()
    {
        $query = new SelectQuery(DT_TEAMS_PERSONS . ' teams_members');
        $query->setColumns(
            "teams_members.id_team_player, teams_members.id_team,
            teams_members.id_person, teams_members.id_position,
            teams_members.date_since, teams_members.date_to,
            players.dress_number, players.id_team_player as is_team_player,
            teams.name as name_team, positions.position as name_position,
            concat(concat(persons.forename, ' '), persons.surname) as name_person"
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                DT_TEAMS . ' teams',
                'teams_members.id_team = teams.id_team'
            )
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                DT_PERSONS . ' persons',
                'teams_members.id_person = persons.id_person'
            )
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                DT_PERSON_POSITIONS . ' positions',
                'teams_members.id_position = positions.id_position'
            )
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::LEFT,
                DT_TEAMS_PLAYERS . ' players',
                'teams_members.id_team_player = players.id_team_player'
            )
        );
        $query->setOrderBy('positions.position, date_since desc, date_to asc');
        return $query;
    }
}
