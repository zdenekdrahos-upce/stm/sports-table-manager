<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Team\Member;

use STM\Team\Team;
use STM\Match\Match;
use STM\Competition\Competition;
use STM\DB\SQL\Condition;
use STM\DB\SQL\JoinTable;
use STM\DB\SQL\JoinType;
use STM\Helpers\Selection;
use STM\Person\Person;
use STM\Person\Position\PersonPosition;
use STM\Entities\AbstractEntitySelection;

class TeamMemberSelection extends AbstractEntitySelection
{

    public function setId($id_team_member)
    {
        if (is_numeric($id_team_member)) {
            $this->selection->setAttributeWithValue('teams_members.id_team_player', (string) $id_team_member);
        }
    }

    public function setTeam(Team $team)
    {
        $this->setIdTeam($team->getId());
    }

    public function setIdTeam($idTeam)
    {
        if (is_numeric($idTeam)) {
            $this->selection->setAttributeWithValue('teams_members.id_team', (string) $idTeam);
        }
    }

    public function setPerson(Person $person)
    {
        $this->selection->setAttributeWithValue('teams_members.id_person', (string) $person->getId());
    }

    public function setMatch(Match $match)
    {
        $this->query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                DT_MATCH_PLAYERS . ' match_players',
                'teams_members.id_team_player = match_players.id_team_player'
            )
        );
        $this->selection->setAttributeWithValue('match_players.id_match', (string) $match->getId());
    }

    public function setCompetition(Competition $competition)
    {
        $this->query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                DT_COMPETITIONS_TEAMS . ' competitions_teams',
                'teams_members.id_team = competitions_teams.id_team'
            )
        );
        $this->selection->setAttributeWithValue(
            'competitions_teams.id_competition',
            (string) $competition->getId()
        );
        $date_conditions = array_filter(
            array(
                // if member's dates are between competition dates
                $this->getConditionForMemberInCompetitionDates('teams_members.date_since', $competition),
                $this->getConditionForMemberInCompetitionDates('teams_members.date_to', $competition),
                // if competition's dates are between member dates
                $this->getConditionForCompetitionInMemberDates($competition, 'getDateStart'),
                $this->getConditionForCompetitionInMemberDates($competition, 'getEndStart'),
            ),
            'strlen'
        );
        if ($date_conditions) {
            $condition = new Condition('(' . implode(' OR ', $date_conditions) . ')');
            $this->selection->addCondition($condition);
        }
    }

    private function getConditionForMemberInCompetitionDates($date_column, $competition)
    {
        $selection_start = new Selection();
        $selection_start->setAttributeWithDates(
            $date_column,
            $competition->getDateStart(),
            $competition->getEndStart()
        );
        return $selection_start->getEscapedCondition();
    }

    private function getConditionForCompetitionInMemberDates($competition, $getter)
    {
        if ($competition->$getter()->isValid()) {
            $selection_start = new Selection();
            $selection_start->setDateBetweenAttributes(
                $competition->$getter(),
                'teams_members.date_since',
                'teams_members.date_to'
            );
            return $selection_start->getEscapedCondition();
        }
        return '';
    }

    public function setPosition(PersonPosition $position)
    {
        $this->selection->setAttributeWithValue('teams_members.id_position', (string) $position->getId());
    }

    public function setOnlyPlayers()
    {
        $this->selection->setNullAttribute('players.id_team_player', false);
    }

    public function setOnlyNonPlayers()
    {
        $this->selection->setNullAttribute('players.id_team_player', true);
    }

    /** @param string $date */
    public function setDate($date)
    {
        $this->selection->setDateBetweenAttributes($date, 'teams_members.date_since', 'teams_members.date_to');
    }

    protected function getQuery()
    {
        return TeamMemberSQL::selectTeamMembers();
    }
}
