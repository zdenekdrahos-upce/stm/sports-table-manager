<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Team\Member;

use STM\DB\Object\DatabaseObject;
use STM\StmDatetime;
use STM\Utils\Dates;
use \Serializable;

/**
 * TeamMember class
 * - instance: represents row from database table 'TEAMS_PERSONS'
 */
final class TeamMember implements Serializable
{
    /** @var DatabaseObject */
    private static $db;

    private $id_team_player;
    private $id_team;
    private $id_person;
    private $id_position;
    private $date_since;
    private $date_to;
    private $name_team;
    private $name_person;
    private $name_position;
    private $is_team_player;
    private $dress_number;

    /** @var TeamPlayerHelper */
    private $teamPlayer;

    private function __construct()
    {
        $this->teamPlayer = new TeamPlayerHelper($this);
    }

    public static function setORM(DatabaseObject $dbObject)
    {
        if (is_null(self::$db)) {
            self::$db = $dbObject;
        }
    }

    /**
     *
     * @param array $attributes
     * @return int|boolean
     * Returns id of created member if creating was successful, otherwise false
     */
    public static function create($attributes)
    {
        if (TeamMemberValidator::checkCreate($attributes)) {
            if (self::$db->insertValues(self::processAttributes($attributes), true)) {
                $idMember = self::$db->getLastInsertId();
                if ($idMember && $attributes['is_player']) {
                    TeamPlayerHelper::create($idMember, $attributes['dress_number']);
                }
                return true;
            }
        }
        return false;
    }

    public function update($attributes)
    {
        TeamMemberValidator::setTeamMember($this);
        if (TeamMemberValidator::checkUpdate($attributes)) {
            return self::$db->updateById($this->id_team_player, self::processAttributes($attributes));
        }
        return false;
    }

    public function delete()
    {
        return self::$db->deleteById($this->id_team_player);
    }

    public function createTeamPlayer()
    {
        if (!$this->isTeamPlayer()) {
            $created = $this->teamPlayer->createTeamPlayer();
            if ($created) {
                $this->is_team_player = true;
                return true;
            }
        }
        return false;
    }

    // no check of change, only check if empty or valid number
    public function updateDressNumber($dressNumber)
    {
        return $this->teamPlayer->updateDressNumber($dressNumber);
    }

    public function deleteTeamPlayer()
    {
        if ($this->isTeamPlayer()) {
            return $this->teamPlayer->deleteTeamPlayer();
        }
        return false;
    }

    public function getDressNumber()
    {
        return $this->dress_number && $this->isTeamPlayer() ? (int) $this->dress_number : false;
    }

    public function isTeamPlayer()
    {
        return $this->is_team_player ? true : false;
    }

    public function getId()
    {
        return (int) $this->id_team_player;
    }

    public function getIdTeam()
    {
        return (int) $this->id_team;
    }

    public function getIdPerson()
    {
        return (int) $this->id_person;
    }

    public function getIdPosition()
    {
        return (int) $this->id_position;
    }

    public function getDateSince()
    {
        return new StmDatetime($this->date_since);
    }

    public function getDateTo()
    {
        return new StmDatetime($this->date_to);
    }

    public function getNameTeam()
    {
        return $this->name_team;
    }

    public function toArray()
    {
        return array(
            'team' => $this->name_team,
            'person' => $this->name_person,
            'position' => $this->name_position,
            'date_since' => $this->getDateSince(),
            'date_to' => $this->getDateTo(),
            'is_team_player' => $this->isTeamPlayer(),
            'dress_number' => $this->dress_number
        );
    }

    public function __toString()
    {
        return $this->name_person;
    }

    public function serialize()
    {
        $vars = get_object_vars($this);
        unset($vars['teamPlayer']);
        return serialize($vars);
    }

    public function unserialize($serialized)
    {
        $vars = unserialize($serialized);
        foreach ($vars as $key => $value) {
            $this->$key = $value;
        }
        $this->teamPlayer = new TeamPlayerHelper($this);
    }

    private static function processAttributes($attributes)
    {
        $result = array();
        if (array_key_exists('team', $attributes)) {
            $result['id_team'] = $attributes['team']->getId();
        }
        if (array_key_exists('person', $attributes)) {
            $result['id_person'] = $attributes['person']->getId();
        }
        $result['id_position'] = $attributes['position']->getId();
        $result['date_since'] = Dates::stringToDatabaseDate($attributes['date_since']);
        $result['date_to'] = Dates::stringToDatabaseDate($attributes['date_to']);
        return $result;
    }
}
