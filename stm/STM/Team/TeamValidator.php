<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Team;

use STM\Helpers\ObjectValidator;
use STM\Web\Message\FormError;
use STM\Club\Club;

/**
 * Class for validating attributes for database table 'TEAMS'
 * - attributes = name, club
 */
final class TeamValidator
{
    /** @var ObjectValidator */
    private static $validator;

    /** @param \STM\Libs\FormProcessor $formProcessor */
    public static function init($formProcessor = false)
    {
        if (is_null(self::$validator)) {
            self::$validator = new ObjectValidator('\STM\Team\Team');
        }
        self::$validator->setFormProcessor($formProcessor);
    }

    public static function setTeam($team)
    {
        self::$validator->setComparedObject($team);
    }

    public static function resetTeam()
    {
        self::$validator->resetComparedObject();
    }

    public static function checkCreate($attributes)
    {
        if (self::$validator->checkCreate($attributes, array('name', 'club'))) {
            self::checkFields($attributes);
        }
        return self::$validator->isValid();
    }

    public static function checkUpdate($attributes)
    {
        if (self::$validator->checkUpdate($attributes, array('name', 'club'))) {
            self::checkFields($attributes);
        }
        return self::$validator->isValid();
    }

    private static function checkFields($attributes)
    {
        self::checkName($attributes['name']);
        self::checkClub($attributes['club']);
        if (self::$validator->isValid() && self::$validator->isSetComparedObject()) {
            self::checkChange($attributes);
        }
        if (self::$validator->isValid()) {
            self::checkUniqueName($attributes['name'], $attributes['club']);
        }
    }

    public static function checkName($new_name)
    {
        self::$validator->checkString($new_name, array('min' => 2, 'max' => 50), stmLang('name'));
        return self::$validator->isValid();
    }

    public static function checkClub($club)
    {
        self::$validator->getFormProcessor()->checkCondition($club instanceof Club, 'Invalid club');
        return self::$validator->isValid();
    }

    private static function checkUniqueName($name, $club)
    {
        self::$validator->getFormProcessor()->checkCondition(
            !$club->isTeamFromClub($name),
            FormError::get('team-club')
        );
    }

    private static function checkChange($attributes)
    {
        $new_name = $attributes['name'] != self::$validator->getValueFromCompared('__toString');
        $new_club = $attributes['club']->getId() != self::$validator->getValueFromCompared('getIdClub');
        self::$validator->getFormProcessor()->checkCondition(
            $new_name || $new_club,
            FormError::get('no-change', array(stmLang('competition', 'team')))
        );
    }
}
