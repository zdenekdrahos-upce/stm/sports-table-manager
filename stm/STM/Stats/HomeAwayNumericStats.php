<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Stats;

class HomeAwayNumericStats extends HomeAwayComparison
{
    public $total = null;

    public function getTotal()
    {
        if (is_null($this->total)) {
            return $this->home + $this->away;
        }
        return $this->total;
    }

    public function getMaxValue()
    {
        return $this->home >= $this->away ? $this->home : $this->away;
    }

    public function getDifference()
    {
        return $this->home - $this->away;
    }

    public function getPercentageHome()
    {
        return Stats::getPercentage($this->home, $this->getTotal());
    }

    public function getPercentageAway()
    {
        return Stats::getPercentage($this->away, $this->getTotal());
    }

    public function __toString()
    {
        return "{$this->home} x {$this->away}";
    }
}
