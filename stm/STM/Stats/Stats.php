<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Stats;

final class Stats
{
    /**
     * Divides numbers and returns percentage
     * @param number $amount  e.g. 7
     * @param number $total  e.g. 11
     * @return double    e.g. 63.64
     * If paramateres are not numbers or $total = 0 returns 0
     */
    public static function getPercentage($amount, $total)
    {
        if (is_numeric($amount) && is_numeric($total)) {
            if ($total != 0) {
                return round(100 * $amount / $total, 2);
            }
        }
        return 0;
    }
}
