<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Stats;

class HomeAwayComparison
{
    /** @var mixed */
    public $home;
    /** @var mixed */
    public $away;
}
