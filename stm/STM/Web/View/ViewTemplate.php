<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Web\View;

use \Exception;

/**
 * ViewTemplate class
 * - templating: sets variable in Controller and include template file
 * - template file: contains html elements with simple php code (if, for, ...)
 * - data: should contain variables used in template file
 */
class ViewTemplate implements IViewContent
{
    /** @var string */
    private $template_path;
    /** @var array */
    private $data;

    /**
     * Creates new ViewTemplate if $template_path is existing file and $data is array
     * @param string $template_path absolute path to template file
     * @param array $data array with keys and values used in template
     */
    public function __construct($template_path, $data = array())
    {
        if ($this->existsTemplate($template_path) && $this->checkData($data)) {
            $this->template_path = $template_path;
            $this->data = $data;
        }
    }

    public function display()
    {
        if ($this->template_path) {
            extract($this->data);
            include($this->template_path);
        }
    }

    private function existsTemplate($template)
    {
        if (file_exists($template)) {
            return true;
        }
        $template = str_replace(STM_SITE_ROOT, 'STM_SITE_ROOT/', $template);
        throw new Exception('Template [' . $template . '] was not found.');
        return false;
    }

    private function checkData($data)
    {
        return is_array($data);
    }
}
