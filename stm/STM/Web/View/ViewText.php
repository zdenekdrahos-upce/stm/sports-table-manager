<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Web\View;

use STM\Utils\Strings;

/**
 * ViewText class
 * - used for adding simple text elements to View (e.g. link related to profile page for all update actions)
 */
class ViewText implements IViewContent
{
    /** @var string */
    private $text;

    public function __construct($text)
    {
        if (Strings::isStringNonEmpty($text)) {
            $this->text = $text;
        }
    }

    public function display()
    {
        if ($this->text) {
            echo $this->text;
        }
    }
}
