<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Web\View;

/**
 * IViewContent interface
 * - used in View class (only instances that implements this interface can be added to View)
 */
interface IViewContent
{
    /**
     * Displays - prints output to user screen
     */
    public function display();
}
