<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Web\View;

/**
 * View class
 * - used for presentation to user
 * - right now it can only prints output to screen
 */
class View
{
    /** @var array(IViewSource) */
    private $view_parts;

    public function __construct()
    {
        $this->view_parts = false;
    }

    /**
     * Displays contetn - prints output to user screen
     */
    public function display()
    {
        if ($this->view_parts) {
            foreach ($this->view_parts as $view) {
                $view->display();
            }
        }
    }

    /**
     * @param IViewContent $content
     */
    public function setContent($content)
    {
        if ($content instanceof IViewContent) {
            $this->view_parts[] = $content;
        }
    }
}
