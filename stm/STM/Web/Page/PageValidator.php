<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Web\Page;

use STM\Web\Message\SessionMessage;

final class PageValidator
{
    /** @var Page */
    private static $page;

    public static function check(Page $page)
    {
        self::$page = $page;
        self::isValidModule();
        self::isValidAction();
        self::isValidMethod();
        self::checkUrlLogic();
    }

    private static function isValidModule()
    {
        $modules = array(
            'index', 'competition', 'season', 'playoff', 'category', 'match',
            'team', 'user', 'settings', 'prediction', 'country',
            'stadium', 'club', 'person', 'attribute', 'sport', 'position',
            'action', 'import', 'statistics', 'export', 'profile'
        );
        if (!in_array(self::$page->getModule(), $modules)) {
            throw new PageException("index.php", SessionMessage::get('page-module'));
        }
    }

    private static function isValidAction()
    {
        if (!in_array(self::$page->getAction(), array('create', 'read', 'update', 'delete'))) {
            $module = self::$page->getModule();
            throw new PageException("index.php?module={$module}", SessionMessage::get('page-action'));
        }
    }

    private static function isValidMethod()
    {
        if (!method_exists(self::$page->getControllerName(), self::$page->getMethodName())) {
            $module = self::$page->getModule();
            $link = $module == 'index' ? 'index.php' : "index.php?module={$module}";
            throw new PageException($link, SessionMessage::get('page-type'));
        }
    }

    private static function checkUrlLogic()
    {
        $source = self::$page->getSourceArray();
        // Index module only with type argument, otherwise redirect to index.php
        if (self::$page->getModule() == 'index') {
            $invalid_conditions = array(
                isset($source['module']),
                isset($source['action']),
                isset($source['type']) && self::$page->getType() == 'index'
            );
            if (in_array(true, $invalid_conditions)) {
                throw new PageException('index.php');
            }
        } elseif (self::$page->getType() != 'index') {// check if action (read) is set
            if (!array_key_exists('action', $source)) {
                throw new PageException('index.php?module=' . self::$page->getModule());
            }
        } elseif (count($source) > 1 && (array_key_exists('action', $source) ||
            array_key_exists('type', $source))
        ) {// check if only module is set in index page
            throw new PageException('index.php?module=' . self::$page->getModule());
        }
    }
}
