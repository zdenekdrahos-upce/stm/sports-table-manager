<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Web\Page;

use \Exception;

final class PageException extends Exception
{
    public $failMessage;

    public function __construct($redirectedPage, $failMessage = false)
    {
        $this->failMessage = $failMessage;
        parent::__construct($redirectedPage);
    }
}
