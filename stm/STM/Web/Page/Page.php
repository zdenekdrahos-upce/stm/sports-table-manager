<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Web\Page;

use STM\Utils\Strings;

final class Page
{
    private $module;
    private $action;
    private $type;
    private $source_array;

    public static function loadFromUrl()
    {
        return new self($_GET);
    }

    public function getModule()
    {
        return $this->module;
    }

    public function getAction()
    {
        return $this->action;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getControllerName()
    {
        $namespace = ucfirst($this->getIncludedModule());
        $name = ucfirst($this->module);
        return '\STM\Module\\' . $namespace . '\\' . $name . 'Controller';
    }

    public function getMethodName()
    {
        $underScoreMethod = $this->type == 'index' ? $this->type : ($this->action . '_' . $this->type);
        return Strings::toCamelCase($underScoreMethod);
    }

    public function getDatabaseColumn()
    {
        return 'action_' . $this->action;
    }

    public function getHeader()
    {
        $header = stmLang('header', $this->module, 'index');
        if ($this->type != 'index') {
            $header = stmLang('header', $this->module, $this->action, $this->type);
        }
        return $header;
    }

    public function getIncludedModule()
    {
        return in_array($this->module, array('season', 'playoff')) ? 'Competition' : ucfirst($this->module);
    }

    public function getModuleForAuthorization()
    {
        if (in_array($this->module, array('season', 'category', 'playoff'))) {
            return 'competition';
        } elseif (in_array(
            $this->module,
            array(
                'club', 'country', 'stadium', 'person', 'attribute', 'sport',
                'position', 'action', 'statistics'
            )
        )) {
            return 'team';
        } elseif (in_array($this->module, array('settings', 'import', 'export'))) {
            return 'user';
        } elseif (in_array($this->module, array('match'))) {
            return 'match';
        } else {
            return $this->module;// index, prediction, profile
        }
    }

    public function getSelectedModuleForMenu()
    {
        if (in_array($this->module, array('competition', 'season', 'category', 'playoff'))) {
            return 'competition';
        } elseif (in_array($this->module, array('match'))
            || ($this->module == 'import' && $this->type == 'matches')) {
            return 'match';
        } elseif (in_array($this->module, array('prediction'))) {
            return 'prediction';
        } elseif (in_array($this->module, array('team', 'club'))
            || ($this->module == 'import' && $this->type == 'teams')) {
            return 'team';
        } elseif (in_array($this->module, array('sport', 'stadium', 'country', 'action'))) {
            return 'other';
        } elseif (in_array($this->module, array('person', 'position', 'attribute'))) {
            return 'person';
        } elseif (in_array($this->module, array('user', 'settings', 'export'))) {
            return 'admin';
        } else {
            return '';
        }
    }

    public function getHelpPath()
    {
        if ($this->module != 'index') {
            $path = "{$this->module}/";
            $path .= $this->action == 'read' && $this->type == 'index' ? '' : "{$this->action}/{$this->type}";
            return $path;
        } else {
            return $this->type == 'index' ? '' : $this->type;
        }
    }

    public function getSourceArray()
    {
        return $this->source_array;
    }

    private function __construct(array $array)
    {
        $this->source_array = $array;
        $this->loadModule();
        $this->loadAction();
        $this->loadType();
    }

    private function loadModule()
    {
        $this->module = isset($this->source_array['module']) ? $this->source_array['module'] : 'index';
    }

    private function loadAction()
    {
        $this->action = isset($this->source_array['action']) ? $this->source_array['action'] : 'read';
    }

    private function loadType()
    {
        $this->type = isset($this->source_array['type']) ? $this->source_array['type'] : 'index';
    }
}
