<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Web\HTML;

use STM\Session;

final class Forms
{
    public static function select($name, $options, $empty_value = false, $class = false)
    {
        $selected_key = isset($_POST[$name]) ? $_POST[$name] : (isset($_GET[$name]) ? $_GET[$name] :'');
        $class = $class ? (' class="' . $class . '"') : '';
        echo '<select name="' . $name . '" id="' . $name . '" size="1"' . $class . '>';
        if ($empty_value) {
            echo '<option' . ('' == $selected_key ? ' selected' : '') . ' value="">';
            echo '- ' . stmLang('form', 'empty') . ' -</option>';
        }
        foreach ($options as $key => $option) {
            echo '<option' . ($key == $selected_key ? ' selected' : '') . ' value="' . $key . '">';
            echo $option . '</option>';
        }
        echo '</select>';
    }

    public static function printCurrentGetFieldsAsHiddenInputs($exceptions = array())
    {
        foreach ($_GET as $key => $value) {
            if (!in_array($key, $exceptions)) {
                echo '<input type="hidden" name="' . $key . '" value="' . $value . '" />';
            }
        }
        echo "\n";
    }

    public static function selectBoolean($name, $class = false)
    {
        $options = array(
            'true' => stmLang('settings', 'allowed'),
            'false' => stmLang('settings', 'denied')
        );
        self::select($name, $options, false, $class);
    }

    public static function inputDate($name)
    {
        $_POST[$name] = isset($_POST[$name]) ? $_POST[$name] : '';
        echo '<input class="inline" type="text" size="40" maxlength="50" ';
        echo 'name="' . $name . '" id="' . $name . '" value="' . $_POST[$name] . '"><br />';
        echo '<script src="web/scripts/jquery-ui.min.js" type="text/javascript"></script>';
        echo '<script src="web/scripts/jquery-ui-timepicker-addon.js" type="text/javascript"></script>';
        echo '<script>$(function() {$( "#' . $name . '" ).datetimepicker({showOn: "button", showButton: true, ';
        echo 'buttonImage: "web/images/calendar.png", buttonImageOnly: true, altFormat: "dd M yy", ';
        echo 'dateFormat: "dd M yy", timeFormat: "hh:mm", hour: 16, });});</script>';
    }

    public static function inputDeleteToken()
    {
        $session = Session::getInstance();
        if ($session->isLoggedIn()) {
            echo '<input type="hidden" name="token" value="' . $_SESSION['stmUser']['token'] . '" />' . "\n";
        }
    }

    public static function optionsFromSelect()
    {
        ob_start();
        call_user_func_array(array('\STM\Web\HTML\Forms', 'select'), func_get_args());
        $select = ob_get_clean();
        $start = mb_strpos($select, '<option', 0, 'UTF-8');
        $end = mb_strpos($select, '</select>', 0, 'UTF-8');
        echo mb_substr($select, $start, $end - $start, 'UTF-8');
    }
}
