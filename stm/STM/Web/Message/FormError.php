<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Web\Message;

final class FormError
{
    public static function get($message, $args = array())
    {
        if (is_array($args) && !empty($args)) {
            return stmLangf('form-error', $message, $args);
        } else {
            return stmLang('form-error', $message);
        }
    }
}
