<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Web\Message;

final class SessionMessage
{
    public static function get($message, $args = array())
    {
        if (is_array($args) && !empty($args)) {
            return stmLangf('session', $message, $args);
        } else {
            return stmLang('session', $message);
        }
    }
}
