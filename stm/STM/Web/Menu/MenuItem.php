<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Web\Menu;

final class MenuItem
{
    public $name;
    public $link;
    public $submenu;

    public function __construct($name, $link)
    {
        $this->name = $name;
        $this->link = $link;
        $this->submenu = array();
    }
}
