<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Web\Menu;

use STM\Utils\Strings;

final class MenuPrintout
{
    private $menu;
    private $selected_item;

    public function __construct()
    {
        $this->menu = array();
        $this->selected_item = null;
    }

    public function setMenu($menu)
    {
        if (is_array($menu)) {
            $this->menu = $menu;
        }
    }

    public function setSelectedItem($selected_item)
    {
        if (Strings::isStringNonEmpty($selected_item)) {
            $this->selected_item = $selected_item;
        }
    }

    public function display()
    {
        $level = 1;
        $this->printList($this->menu, $level);
    }

    private function printList($menu, $level)
    {
        echo '<ul>';
        foreach ($menu as $item) {
            if ($level == 1) {
                echo '<li' . ($this->isModuleSelected($item->name) ? ' class="selected"' : '') . '>';
            } else {
                echo '<li>';
            }
            $this->printLink($item->link, $item->name);
            if ($item->submenu) {
                $this->printList($item->submenu, ++$level);
                --$level;
            }
            echo '</li>';
        }
        echo '</ul>';
    }

    private function isModuleSelected($module)
    {
        return $this->selected_item && is_int(strpos(strtolower($module), $this->selected_item));
    }

    private function printLink($link, $text)
    {
        $target = is_int(strpos($link, 'http')) ? '_blank' : '_self';
        echo '<a href="' . $link . '" target="' . $target . '">' . stmLang('menu', $text) . '</a>';
    }
}
