<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Web\Menu;

use STM\Session;
use STM\Utils\Links;

final class Menu
{
    private static $menu;

    public static function load()
    {
        self::$menu = array();
        self::$menu[] = self::getCompetitionMenu();
        self::$menu[] = self::getMatchMenu();
        if (STM_ALLOW_PREDICTIONS) {
            self::$menu[] = self::getPredictionMenu();
        }
        self::$menu[] = self::getTeamMenu();
        self::$menu[] = self::getPersonMenu();
        self::$menu[] = self::getOtherMenu();
        $session = Session::getInstance();
        if ($session->getUserGroup() == 'Admin') {
            self::$menu[] = self::getAdminMenu();
        }
        self::$menu[] = self::getHelpMenu();
        return self::$menu;
    }

    private static function getCompetitionMenu()
    {
        $competition = new MenuItem('competitions', '?module=competition');
        $competition->submenu[] = new MenuItem('competitions', '?module=competition');
        $competition->submenu[] = new MenuItem('categories', '?module=category');
        return $competition;
    }

    private static function getMatchMenu()
    {
         $match = new MenuItem('matches', '?module=match');
         $match->submenu[] = new MenuItem('matches', '?module=match');
         $match->submenu[] = new MenuItem('import-matches', '?module=import&action=create&type=matches');
         return $match;
    }

    private static function getPredictionMenu()
    {
        $prediction = new MenuItem('predictions', '?module=prediction');
        $prediction->submenu[] = new MenuItem('chart', '?module=prediction&action=read&type=chart');
        $prediction->submenu[] = new MenuItem('my-predictions', '?module=prediction');
        $prediction->submenu[] = new MenuItem('my-stats', '?module=prediction&action=read&type=user_stats');
        return $prediction;
    }

    private static function getTeamMenu()
    {
        $team = new MenuItem('teams', '?module=team');
        $team->submenu[] = new MenuItem('teams', '?module=team');
        $team->submenu[] = new MenuItem('clubs', '?module=club');
        $team->submenu[] = new MenuItem('head-to-head', '?module=statistics&action=read&type=head_to_head');
        $team->submenu[] = new MenuItem('import-teams', '?module=import&action=create&type=teams');
        return $team;
    }

    private static function getPersonMenu()
    {
        $person = new MenuItem('persons', '?module=person');
        $person->submenu[] = new MenuItem('persons', '?module=person');
        $person->submenu[] = new MenuItem('positions', '?module=position');
        $person->submenu[] = new MenuItem('attributes', '?module=attribute');
        $person->submenu[] = new MenuItem('import-persons', '?module=import&action=create&type=persons');
        return $person;
    }

    private static function getOtherMenu()
    {
        $other = new MenuItem('other', '?module=sport');
        $other->submenu[] = new MenuItem('sports', '?module=sport');
        $other->submenu[] = new MenuItem('actions', '?module=action');
        $other->submenu[] = new MenuItem('stadiums', '?module=stadium');
        $other->submenu[] = new MenuItem('countries', '?module=country');
        return $other;
    }

    private static function getAdminMenu()
    {
        $admin = new MenuItem('admin', '?module=user');
        $admin->submenu[] = new MenuItem('users', '?module=user');
        $admin->submenu[] = new MenuItem('settings', '?module=settings&action=read&type=stm');
        $admin->submenu[] = new MenuItem('export', '?module=export');
        $admin->submenu[] = new MenuItem('import', '?module=import');
        return $admin;
    }

    private static function getHelpMenu()
    {
        return new MenuItem('help', Links::getHelpLink());
    }
}
