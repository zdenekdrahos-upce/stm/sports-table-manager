<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Export;

use STM\Helpers\Event;

class ExportEvent
{
    private function __construct()
    {
    }

    /**
     * Saves events related with Export
     * @param ExportEvent $exportEvent constant from ExportEvent class
     * @param string $message description of event
     * @param boolean $result if event was sucessful or fail
     * @return boolean
     * Returns true if event was successfuly saved, otherwise false (bad arguments
     * or error during file saving)
     */
    public static function log($exportEvent, $message, $result)
    {
        $event = new Event('\STM\Export\ExportEvent', 'matches');
        $event->setMessage($exportEvent, $message, $result);
        return $event->logEvent();
    }
}
