<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Export\File;

use STM\Competition\Competition;
use STM\Utils\Dates;
use STM\StmFactory;

class ExportCompetitionMatches implements IFileExport
{
    private $format;
    private $data;
    private $competition;
    private $max_periods;

    public function __construct($competition)
    {
        $this->max_periods = 0;
        $this->format = '{H};{A};{D};{R}';
        if ($competition instanceof Competition) {
            $this->competition = $competition;
            $this->loadData();
        } else {
            $this->data = '';
        }
    }

    public function getFormat()
    {
        return $this->format;
    }

    public function getData()
    {
        return $this->data;
    }

    private function loadData()
    {
        ob_start();
        foreach ($this->getMatches() as $match) {
            $match_array = $match->toArray();
            $data = array();
            $data[] = $match_array['team_home'];
            $data[] = $match_array['team_away'];
            $data[] = Dates::datetimeToDatabaseDate($match->getDate());
            $data[] = $match->getRound();
            if ($match->hasLoadedPeriods()) {
                foreach ($match->getPeriods() as $period) {
                    $data[] = "{$period->getScoreHome()}:{$period->getScoreAway()}";
                }
                $this->max_periods = max(array($this->max_periods, count($match->getPeriods())));
            }
            echo implode(';', $data) . "\n";
        }
        $this->data = ob_get_clean();
        $this->addPeriodsToFormat();
    }

    private function getMatches()
    {
        $ms = array(
            'matchType' => \STM\Match\MatchSelection::ALL_MATCHES,
            'loadScores' => false,
            'loadPeriods' => true,
            'competition' => $this->competition,
        );
        return StmFactory::find()->Match->find($ms);
    }

    private function addPeriodsToFormat()
    {
        $format = array($this->format);
        for ($i = 1; $i <= $this->max_periods; $i++) {
            $format[] = '{V}:{O}';
        }
        $this->format = implode(';', $format);
    }
}
