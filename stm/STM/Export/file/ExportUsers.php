<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Export\File;

use STM\User\UserGroup;
use STM\Utils\Dates;
use STM\StmFactory;

class ExportUsers implements IFileExport
{
    private $format;
    private $data;

    public function __construct()
    {
        $this->format = '{U};{G};{E};{R}';
        $this->loadData();
    }

    public function getFormat()
    {
        return $this->format;
    }

    public function getData()
    {
        return $this->data;
    }

    private function loadData()
    {
        ob_start();
        $allUsers = StmFactory::find()->User->findAll();
        foreach ($allUsers as $user) {
            $data = array();
            $data[] = $user->getUsername();
            $data[] = UserGroup::getIdGroup($user->getUserGroup());
            $data[] = $user->getEmail();
            $data[] = Dates::datetimeToDatabaseDate($user->getDateRegistration());
            echo implode(';', $data) . "\n";
        }
        $this->data = ob_get_clean();
    }
}
