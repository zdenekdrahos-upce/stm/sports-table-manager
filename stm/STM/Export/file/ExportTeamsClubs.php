<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Export\File;

use STM\StmFactory;

class ExportTeamsClubs implements IFileExport
{
    private $format;
    private $data;

    public function __construct()
    {
        $this->format = '{T};{C}';
        $this->loadData();
    }

    public function getFormat()
    {
        return $this->format;
    }

    public function getData()
    {
        return $this->data;
    }

    private function loadData()
    {
        ob_start();
        foreach (StmFactory::find()->Club->findAll() as $club) {
            echo ";{$club->__toString()}\n";
        }
        foreach (StmFactory::find()->Team->findAll() as $team) {
            echo "{$team->__toString()};{$team->getNameClub()}\n";
        }
        $this->data = ob_get_clean();
    }
}
