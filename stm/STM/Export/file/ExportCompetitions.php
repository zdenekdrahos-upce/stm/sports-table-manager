<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Export\File;

use STM\Utils\Dates;
use STM\Competition\Season\Season;
use STM\Competition\Playoff\Playoff;
use STM\StmFactory;

class ExportCompetitions implements IFileExport
{
    private $format;
    private $data;

    public function __construct()
    {
        $this->format = '{N};{T};{S};{V};{K};{O};{D};{P};{A};{B};{C};{D};{E};{F};{G}';
        $this->loadData();
    }

    public function getFormat()
    {
        return $this->format;
    }

    public function getData()
    {
        return $this->data;
    }

    private function loadData()
    {
        ob_start();
        foreach (StmFactory::find()->Competition->findAll() as $competition) {
            $data = array();
            $data[] = $competition->__toString();
            $data[] = $competition->getIdCompetitionType();
            $data[] = $competition->getScoreType();
            $data[] = $competition->getStatusId();
            $data[] = $competition->getNameCategory();
            $data[] = Dates::datetimeToDatabaseDate($competition->getDateStart());
            $data[] = Dates::datetimeToDatabaseDate($competition->getEndStart());
            $data[] = $competition->getNumberOfMatchPeriods();
            if ($competition->getIdCompetitionType() == 'S') {
                $competition = Season::findById($competition->getId());
                $data[] = $competition->getPointWin();
                $data[] = $competition->getPointWinOvertime();
                $data[] = $competition->getPointDraw();
                $data[] = $competition->getPointLossOvertime();
                $data[] = $competition->getPointLoss();
            } elseif ($competition->getIdCompetitionType() == 'P') {
                $competition = Playoff::findById($competition->getId());
                $data[] = ';;;;;;' . $competition->getSerieWinMatches();
            }
            echo implode(';', $data) . "\n";
        }
        $this->data = ob_get_clean();
    }
}
