<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM;

use STM\User\User;
use STM\Utils\Generators;
use STM\Utils\Strings;

/**
 * Session class
 * - helper class to work with sessions (log in/out users + sending messages)
 */
class Session
{
    private static $instance;

    /** @var boolean */
    private $isLogged;
    /** @var string */
    private $messageSuccess;
    /** @var string */
    private $messageFail;

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function __construct()
    {
        if (session_id() === '' && !headers_sent()) {
            session_start();
        }
        $this->checkLogin();
        $this->checkMessages();
    }

    /**
     * Returns if the user is logged in
     * @return boolean
     */
    public function isLoggedIn()
    {
        return $this->isLogged;
    }

    /**
     * Logs user if $username is filled. Checking if user exists etc. is performed
     * before calling this method
     * @param string $username
     */
    public function login($user)
    {
        if ($user instanceof User) {
            $_SESSION['stmUser'] = array(
                'id_user' => $user->getId(),
                'name' => $user->getUsername(),
                'user_group' => $user->getUserGroup(),
                'token' => Generators::generateRandomToken(),
            );
            $this->isLogged = true;
        }
    }

    /**
     * Logout the user and destroys the session
     */
    public function logout()
    {
        if ($this->isLogged) {
            unset($_SESSION);
            $this->isLogged = false;
            session_destroy();
        }
    }

    /**
     * Checks if the user is logged in
     */
    private function checkLogin()
    {
        $this->isLogged = isset($_SESSION['stmUser']);
    }

    // MESSAGES
    private function checkMessages()
    {
        $this->checkMessage('messageSuccess');
        $this->checkMessage('messageFail');
    }
    /**
     * Method called in constructor which checks message in session. If there was
     * a message then it's picks up and destroys session.
     */
    private function checkMessage($name)
    {
        if (isset($_SESSION[$name])) {
            $this->$name = $_SESSION[$name];
            unset($_SESSION[$name]);
        } else {
            $this->$name = "";
        }
    }

    public function setMessageSuccess($successMessage)
    {
        $this->setMessage('messageSuccess', $successMessage);
    }

    public function setMessageFail($failMessage)
    {
        $this->setMessage('messageFail', $failMessage);
    }

    public function hasMessageSuccess()
    {
        return $this->hasMessage($this->messageSuccess);
    }

    public function hasMessageFail()
    {
        return $this->hasMessage($this->messageFail);
    }

    public function getMessageSuccess()
    {
        return $this->messageSuccess;
    }

    public function getMessageFail()
    {
        return $this->messageFail;
    }

    private function setMessage($name, $message)
    {
        if (Strings::isStringNonEmpty($message)) {
            $_SESSION[$name] = ucfirst($message);
        }
    }

    private function hasMessage($message)
    {
        return !empty($message);
    }

    // GETTERS
    /**
     * Finds name of the logged user
     * @return string/false
     * Returns false is user is not logged in
     */
    public function getUsername()
    {
        return $this->getUserAttribute('name');
    }

    public function getUserId()
    {
        return $this->getUserAttribute('id_user');
    }

    public function getUserGroup()
    {
        return $this->getUserAttribute('user_group');
    }

    private function getUserAttribute($attribute)
    {
        if ($this->isLogged) {
            return $_SESSION['stmUser'][$attribute];
        }
        return false;
    }
}
