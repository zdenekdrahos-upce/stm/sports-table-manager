<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Cache\Loader;

use STM\Competition\Season\Table\SeasonTeamsPositions;

final class SeasonTeamsPositionsLoader extends AbstractSeasonLoader
{
    /** @var \STM\Competition\Season\Table\SeasonTeamsPositions */
    private $season_team_positions;

    public function getContent()
    {
        return $this->season_team_positions;
    }

    protected function loadDataFromDb()
    {
        $this->season_team_positions = new SeasonTeamsPositions($this->season);
    }
}
