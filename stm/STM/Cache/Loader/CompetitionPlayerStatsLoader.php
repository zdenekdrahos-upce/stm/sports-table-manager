<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Cache\Loader;

use STM\Libs\FileCache\ISource;
use STM\Competition\Competition;
use STM\StmFactory;

final class CompetitionPlayerStatsLoader implements ISource
{
    /** @var int */
    private $idCompetition;
    /** @var \STM\Match\Stats\Action\MatchPlayerStats */
    private $stats;

    public function __construct($competition)
    {
        if ($competition instanceof Competition) {
            $this->idCompetition = $competition->getId();
        } else {
            $this->idCompetition = $competition;
        }
    }

    public function getContent()
    {
        return $this->stats;
    }

    public function getEffectiveDate()
    {
        // content is reloaded only if file NOT exists
        return file_exists(STM_CACHE_COMPETITION_PLAYERS_STATS . $this->idCompetition . '.txt') ? 0 : 1;
    }

    public function reloadContent()
    {
        $this->stats = StmFactory::find()->MatchPlayerStats->findByCompetition($this->idCompetition);
    }
}
