<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Cache\Loader;

use STM\Competition\Season\Table\SeasonResultTable;

final class SeasonTableLoader extends AbstractSeasonLoader
{
    /** @var SeasonResultTable */
    private $seasonTable;

    public function getContent()
    {
        return $this->seasonTable;
    }

    protected function loadDataFromDb()
    {
        $this->seasonTable = SeasonResultTable::loadTableFromDb($this->season);
    }
}
