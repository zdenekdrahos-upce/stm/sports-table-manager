<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Cache\Loader;

use STM\Libs\FileCache\ISource;
use STM\Competition\Season\Season;
use STM\Cache\CacheHelper;

abstract class AbstractSeasonLoader implements ISource
{
    /** @var int */
    private $idSeason;
    /** @var Season */
    protected $season;

    public function __construct($season)
    {
        if ($season instanceof Season) {
            $this->season = $season;
            $this->idSeason = $season->getId();
        } else {
            $this->idSeason = $season;
        }
    }

    public function getEffectiveDate()
    {
        if (CacheHelper::canBeSeasonTableDeleted($this->idSeason)) {
            return CacheHelper::getLastmodDateForCompetition($this->idSeason);
        }
        return 0;
    }

    public function reloadContent()
    {
        if (is_null($this->season)) {
            $this->season = Season::findById($this->idSeason);
        }
        if ($this->season) {
            $this->loadDataFromDb();
        }
    }

    abstract protected function loadDataFromDb();
}
