<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Cache\Loader;

use STM\Libs\FileCache\ISource;
use STM\StmFactory;
use STM\Match\Preview\MatchPreviewFactory;

final class MatchPreviewLoader implements ISource
{
    /** @var Match */
    private $idTeam;
    /** @var \STM\Match\Preview\MatchPreview */
    private $preview;

    public function __construct($idTeam)
    {
        $this->idTeam = $idTeam;
    }

    public function getContent()
    {
        return $this->preview;
    }

    public function getEffectiveDate()
    {
        $ms = array(
            'matchType' => \STM\Match\MatchSelection::PLAYED_MATCHES,
            'loadScores' => true,
            'loadPeriods' => true,
            'teams' => array($this->idTeam)
        );
        $date = StmFactory::find()->Match->getDateOfLastModifiedMatch($ms);
        return $date ? $date : 1;
    }

    public function reloadContent()
    {
        $preview = new MatchPreviewFactory($this->idTeam);
        $this->preview = $preview->loadPreviewData(4);
    }
}
