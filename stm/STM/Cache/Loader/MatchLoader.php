<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Cache\Loader;

use STM\Libs\FileCache\ISource;
use STM\StmFactory;
use STM\Cache\CacheHelper;

final class MatchLoader implements ISource
{
    /** @var array */
    private $match_selection;
    /** @var array - item = Match */
    private $matches;

    public function __construct(array $match_selection)
    {
        $this->match_selection = $match_selection;
    }

    public function getContent()
    {
        return $this->matches;
    }

    public function getEffectiveDate()
    {
        return CacheHelper::getLastmodDateForMatchSelection($this->match_selection);
    }

    public function reloadContent()
    {
        $this->matches = StmFactory::find()->Match->find($this->match_selection);
    }
}
