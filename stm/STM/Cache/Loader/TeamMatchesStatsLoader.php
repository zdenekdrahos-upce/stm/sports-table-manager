<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Cache\Loader;

use STM\Libs\FileCache\ISource;
use STM\Match\Stats\TeamMatchesStatsFactory;
use STM\Cache\CacheHelper;

final class TeamMatchesStatsLoader implements ISource
{
    /** @var array */
    private $match_selection;
    /** @var int */
    private $idTeam;
    /** @var \STM\Match\Stats\TeamMatchesStats */
    private $statistics;

    public function __construct(array $matchSelection, $idTeam)
    {
        $this->match_selection = $matchSelection;
        $this->idTeam = $idTeam;
    }

    public function getContent()
    {
        return $this->statistics;
    }

    public function getEffectiveDate()
    {
        return CacheHelper::getLastmodDateForMatchSelection($this->match_selection);
    }

    public function reloadContent()
    {
        $this->statistics = TeamMatchesStatsFactory::loadFromDatabase($this->match_selection, $this->idTeam);
    }
}
