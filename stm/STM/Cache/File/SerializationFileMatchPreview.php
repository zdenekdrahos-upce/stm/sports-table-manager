<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Cache\File;

use STM\Libs\FileCache\IFile;
use STM\Utils\Dates;
use STM\Match\Match;
use STM\Match\Preview\MatchPreview;

final class SerializationFileMatchPreview implements IFile
{
    /** @var SerializationFile */
    private $file;

    public function __construct($filePath)
    {
        $this->file = new SerializationFile($filePath);
    }

    public function getLastModificationDate()
    {
        if (file_exists($this->file->getFilePath())) {
            $currentContent = $this->printContent();
            if ($currentContent instanceof MatchPreview && $currentContent->match instanceof Match) {
                $matchDate = Dates::datetimeToDatabaseDate($currentContent->match->getDate());
                $nowDate = Dates::stringToDatabaseDate('now');
                if (Dates::areDatesInOrder($nowDate, $matchDate) &&
                        $this->competitionCacheWasNotDeleted($currentContent->match)) {
                    return $this->file->getLastModificationDate();
                }
            }
        }
        return 0;
    }

    public function printContent()
    {
        return $this->file->printContent();
    }

    public function writeContent($content)
    {
        return $this->file->writeContent($content);
    }

    /**
     * Name of cache file is ID_TEAM, but cache is deleted according to ID_COMPETITION.
     * Competition cache can be deleted if user modify date of match, delete all matches
     * from round etc. So if competition matches cache is deleted or has younger date
     * than date of the date modification of preview cache then that's a signal that date
     * of previewed match is probably expired.
     * Little problem is that user can change date of the match, so competition cache is deleted.
     * Then preview match will be marked as experied until the competition cache file is
     * renewed. Solution would be saving extra map of team and competition of his previewed
     * match. But that is little complicated solution and it would slower the deleting of
     * competition cache. So this problem won't be solved because I assume that matches are
     * main part of STM and the most visited (so cache will be quickly renewed)
     * @param Match $match
     * @return bool
     */
    private function competitionCacheWasNotDeleted(Match $match)
    {
        $competitionCache = STM_CACHE_COMPETITION_MATCHES . $match->getIdCompetition() . '.txt';
        if (file_exists($competitionCache)) {
            $matchDate = Dates::timestampToDatabaseDate($this->file->getLastModificationDate());
            $cacheDate = Dates::timestampToDatabaseDate(filemtime($competitionCache));
            return Dates::areDatesInOrder($cacheDate, $matchDate);
        }
        return false;
    }
}
