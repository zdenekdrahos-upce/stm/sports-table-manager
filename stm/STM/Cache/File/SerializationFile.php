<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Cache\File;

use STM\Libs\FileCache\IFile;
use \Exception;

final class SerializationFile implements IFile
{
    /** @var string */
    private $filePath;

    public function __construct($filePath)
    {
        if (!is_string($filePath)) {
            throw new Exception('Invalid parameter in constructor of CacheSerializationFile');
        }
        $this->filePath = $filePath;
    }

    public function getLastModificationDate()
    {
        if (file_exists($this->filePath)) {
            return filemtime($this->filePath);
        }
        return 0;
    }

    public function printContent()
    {
        $tables = file_get_contents($this->filePath);
        return unserialize($tables);
    }

    public function writeContent($content)
    {
        return file_put_contents($this->filePath, serialize($content));
    }

    public function getFilePath()
    {
        return $this->filePath;
    }
}
