<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Cache;

use STM\Libs\FileCache\ISource;
use STM\Libs\FileCache\Cache;
use STM\Utils\Strings;
use STM\Competition\Competition;
use STM\Cache\File\SerializationFile;
use STM\StmFactory;

final class CacheHelper
{

    /**
     * @param \STM\Libs\FileCache\ISource $source
     * @param string $filepath path for SerializationFile
     * @return mixed
     */
    public static function getContentFromCache(ISource $source, $filepath)
    {
        $file = new SerializationFile($filepath);
        $cache = new Cache($source, $file);
        return $cache->display();
    }

    public static function getLastmodDateForCompetition($competition)
    {
        $ms = array(
            'matchType' => \STM\Match\MatchSelection::PLAYED_MATCHES,
            'loadScores' => false,
            'loadPeriods' => false,
            'competition' => $competition
        );
        return self::getLastmodDateForMatchSelection($ms);
    }

    public static function getLastmodDateForMatchSelection(array $match_selection)
    {
        $date = StmFactory::find()->Match->getDateOfLastModifiedMatch($match_selection);
        return $date ? $date : 1;
    }

    public static function canBeSeasonTableDeleted($id_season)
    {
        return self::canBeSeasonCacheDeleted($id_season, STM_CACHE_SEASON_TABLES);
    }

    public static function canBeSeasonTeamPositionsDeleted($id_season)
    {
        return self::canBeSeasonCacheDeleted($id_season, STM_CACHE_SEASON_TEAM_POSITIONS);
    }

    private static function canBeSeasonCacheDeleted($id_season, $root_path)
    {
        if (is_int($id_season) && Strings::isStringNonEmpty(STM_NOT_DELETED_SEASONS_TABLE)) {
            $seasons = explode(',', STM_NOT_DELETED_SEASONS_TABLE);
            return !(in_array($id_season, $seasons) && file_exists($root_path . $id_season . '.txt'));
        }
        return true;
    }

    /** @param ing $id_competition */
    public static function deleteCache($id_competition)
    {
        if (is_int($id_competition)) {
            self::deleteCacheFile(STM_CACHE_COMPETITION_MATCHES, $id_competition);
            self::deleteTeamMatchStats($id_competition);
            if (CacheHelper::canBeSeasonTableDeleted($id_competition)) {
                self::deleteCacheFile(STM_CACHE_SEASON_TABLES, $id_competition);
            }
            if (CacheHelper::canBeSeasonTeamPositionsDeleted($id_competition)) {
                self::deleteCacheFile(STM_CACHE_SEASON_TEAM_POSITIONS, $id_competition);
            }
        }
    }

    /** @param ing $id_competition */
    public static function deleteActionStatsCache($id_competition)
    {
        if (is_int($id_competition)) {
            self::deleteCacheFile(STM_CACHE_COMPETITION_PLAYERS_STATS, $id_competition);
        }
    }

    private static function deleteCacheFile($path, $file_name)
    {
        if (file_exists("{$path}/{$file_name}.txt")) {
            unlink("{$path}/{$file_name}.txt");
        }
    }

    private static function deleteTeamMatchStats($id_competition)
    {
        foreach (scandir(STM_CACHE_TEAM_MATCHES_STATS) as $file) {
            if (is_int(strpos($file, "-c-{$id_competition}.txt"))) {
                unlink(STM_CACHE_TEAM_MATCHES_STATS . $file);
            }
        }
    }
}
