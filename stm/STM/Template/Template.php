<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Template;

/**
 * Template class
 * - holds only path to file where template is located
 */
final class Template
{
    /** @var string Name of the file (without extension) */
    private $name;
    /** @var string Absolute path */
    private $path;

    private function __construct($name, $path)
    {
        $this->name = $name;
        $this->path = $path . $this->name . '.php';
    }

    /**
     * @param string $name
     * @param string $path
     * @return Template/false
     * Returns instance of Template class if file with $name exists in $path folder
     */
    public static function findByName($name, $path)
    {
        if (self::exists($name, $path)) {
            return new self($name, $path);
        }
        return false;
    }

    /**
     * Checks if file with $name exists in $path folder
     * @param string $name
     * @param string $path
     * @return boolean
     */
    public static function exists($name, $path)
    {
        return is_string($name) && is_string($path) &&
                is_bool(strpos($name, '../')) &&
                is_dir($path) && file_exists($path . $name . '.php');
    }

    /**
     * @return boolean
     * Returns true if file was deleted (Careful: if file is deleted and then save
     * method is called -> file will be again created)
     */
    public function delete()
    {
        return unlink($this->path);
    }

    /**
     * @param string $content
     * @return boolean
     * Returns true if content was successfully saved into the template file.
     */
    public function save($content = '')
    {
        if (is_string($content)) {
            $result = @file_put_contents($this->path, $content);
            return is_int($result);
        }
        return false;
    }

    /**
     * @return string
     * Returns template absolute path (e.g. c:/templates/matches.php used in include/require function)
     */
    public function getFullPath()
    {
        return $this->path;
    }

    /**
     * Reads file into a string
     * @return string/false
     * Returns the read data or FALSE on failure.
     */
    public function getContent()
    {
        return file_get_contents($this->path);
    }

    /**
     * @return string
     * Returns name of the template file without extension
     */
    public function __toString()
    {
        return $this->name;
    }
}
