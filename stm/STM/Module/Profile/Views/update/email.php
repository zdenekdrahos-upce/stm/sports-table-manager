<?php
/**
 * Update user email (UserController::updateEmail)
 */
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <label for="email"><?php echo stmLang('user', 'email'); ?>: </label>
    <input type="email" id="email" name="email" size="40" maxlength="60" value="<?php echo $_POST['email']; ?>" />

    <input type="submit" value="<?php echo stmLang('form', 'change'); ?>" name="change_email" />
</form>
