<?php
/**
 * Change user password (UserController::updateEmail)
 */
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <label for="old"><?php echo stmLang('user', 'password', 'old'); ?>:</label>
    <input type="password" id="old" name="old_password" size="20" maxlength="50" />
    <label for="new"><?php echo stmLang('user', 'password', 'new'); ?>: </label>
    <input type="password" id="new" name="new_password" size="20" maxlength="50" />
    <label for="cnew"><?php echo stmLang('user', 'password', 'confirm'); ?>: </label>
    <input type="password" id="cnew" name="confirm_password" size="20" maxlength="50" />

    <input type="submit" value="<?php echo stmLang('form', 'change'); ?>" name="change_pass" />
</form>
