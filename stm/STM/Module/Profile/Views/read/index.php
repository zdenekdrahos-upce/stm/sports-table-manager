<?php
/**
 * Basic info page about user. Only
 * $user        selected user
 *
 * DETAIL PAGE IS VISIBLE ONLY FOR LOGGED USER, ADMIN DONT HAVE ACCESS TO USER PROFILES
 * User can change email/password and delete his account
 * Admin extra: change usergroup, reset passwords and delete accounts of all users
 */
?>

<ul>
    <li><?php echo stmLang('name'); ?>: <strong><?php echo $user->getUsername(); ?></strong></li>
    <li><?php echo stmLang('user', 'group'); ?>: <strong><?php echo stmLang('constants', 'usergroup', $user->getUserGroup()); ?></strong></li>
    <li><?php echo stmLang('user', 'email'); ?>: <strong><?php echo $user->getEmail(); ?></strong></li>
    <li><?php echo stmLang('user', 'date-reg'); ?>: <strong><?php echo $user->getDateRegistration(); ?></strong></li>
</ul>
