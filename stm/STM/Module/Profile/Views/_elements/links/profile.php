    <h2><a href="?module=profile"><?php echo $username; ?></a></h2>
    <ul>
        <li><a href="?module=prediction"><?php echo stmLang('header', 'prediction', 'read', 'predictions'); ?></a></li>
        <li><a href="<?php echo buildUrl(array('action' => 'update', 'type' => 'email')) ?>"><?php echo stmLang('header', 'profile', 'update', 'email'); ?></a></li>
        <li><a href="<?php echo buildUrl(array('action' => 'update', 'type' => 'password')) ?>"><?php echo stmLang('header', 'profile', 'update', 'password'); ?></a></li>
        <li><?php include(STM_MODULES_ROOT . 'Profile/Views/_elements/delete/account.php'); ?></li>
    </ul>
