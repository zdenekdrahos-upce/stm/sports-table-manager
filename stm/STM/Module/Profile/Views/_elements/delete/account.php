<?php
$url = '?module=profile&action=delete&type=account';
$submit_message = stmLang('user', 'delete-account');
$question = stmLang('user', 'delete-account-msg');
include(STM_ADMIN_TEMPLATE_ROOT . 'form/delete-form.php');
