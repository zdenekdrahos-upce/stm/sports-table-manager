<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\Profile;

use STM\Module\ModuleProcessing;
use STM\User\User;
use STM\User\UserValidator;
use STM\Web\Message\FormError;
use STM\Web\Message\SessionMessage;

class ProfileProcessing extends ModuleProcessing
{
    /** @var User */
    private $user;

    public function __construct($user = false)
    {
        $this->user = $user instanceof User ? $user : false;
        parent::__construct($this->user, '\STM\User\UserEvent');
        UserValidator::init($this->formProcessor);
    }

    public function updateEmail()
    {
        if (isset($_POST['change_email']) && parent::isObjectSet()) {
            $this->formProcessor->escapeValues();
            $update = $this->user->updateEmail($_POST['email']);
            parent::checkAction($update, 'UPDATE', 'Change email');
            parent::setSessionMessage($update, $this->getMessage('change'));
            return $update;
        } else {
            $_POST['email'] = $this->user->getEmail();
        }
        return false;
    }

    public function updatePassword()
    {
        if (isset($_POST['change_pass']) && parent::isObjectSet()) {
            $this->formProcessor->escapeValues();
            $this->formProcessor->checkRequiredFields(array('old_password', 'new_password', 'confirm_password'));
            $this->formProcessor->checkEquality(
                $_POST['new_password'],
                $_POST['confirm_password'],
                FormError::get('password-confirmation')
            );
            if ($this->formProcessor->isValid()) {
                $update = $this->user->updatePassword(
                    array('new' => $_POST['new_password'], 'old' => $_POST['old_password'])
                );
                parent::checkAction($update, 'UPDATE', 'Change password');
                // no OK session message, because user is logout after change
                return $update;
            }
        }
        return false;
    }

    private function getMessage($name)
    {
        return SessionMessage::get($name, array(stmLang('user', 'user'), $this->user->getUsername()));
    }
}
