<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\Profile;

use STM\Module\ModuleController;
use STM\Module\User\UserProcessing;

class ProfileController extends ModuleController
{
    /** @var \STM\Module\Profile\ProfileProcessing */
    private $processing;

    protected function __construct()
    {
        parent::__construct(true);
        parent::checkLoggedUser();
        parent::setMainPage('index.php?module=profile');
        $this->processing = new ProfileProcessing($this->loggedUser);
    }

    /** index.php?module=profile */
    public function index()
    {
        parent::setProfileInLeftMenu();
        parent::setContent(array('user' => $this->loggedUser));
    }

    /** index.php?module=profile&action=update&type=email */
    public function updateEmail()
    {
        if ($this->processing->updateEmail()) {
            parent::redirectToIndexPage();
        } else {
            parent::setProfileInLeftMenu();
            parent::setFormErrors($this->processing);
            parent::setContent(array('current_email' => $this->loggedUser->getEmail()));
        }
    }

    /** index.php?module=profile&action=update&type=password */
    public function updatePassword()
    {
        if ($this->processing->updatePassword()) {
            redirect('?type=logout&u=password');
        } else {
            parent::setProfileInLeftMenu();
            parent::setFormErrors($this->processing);
            parent::setContent();
        }
    }

    /** index.php?module=profile&action=delete&type=account */
    public function deleteAccount()
    {
        $userProcessing = new UserProcessing($this->loggedUser);
        if ($userProcessing->deleteUser()) {
            redirect('?type=logout&u=delete');
        } else {
            parent::redirectToIndexPage();
        }
    }

    protected function getProfileLinks()
    {
        return array('username' => $this->loggedUser->getUsername());
    }
}
