<?php
/**
 * Create person attribute  (PersonController::createAttribute)
 * $attributes        all attributes from application
 */
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <label for="attribute"><?php echo stmLang('person', 'attribute'); ?>: </label>
    <?php include(STM_MODULES_ROOT . 'Person/Views/_elements/form-parts/attributes.php'); ?>
    <label for="value"><?php echo stmLang('person', 'attribute-value'); ?>: </label>
    <input type="text" id="value" name="value" size="40" maxlength="100" value="<?php echo $_POST['value']; ?>" />

    <input type="submit" name="create" value="<?php echo stmLang('form', 'create'); ?>" />
</form>
