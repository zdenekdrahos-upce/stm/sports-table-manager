<?php
/*
 * List of all persons
 * $persons
 */
?>

<?php include(STM_MODULES_ROOT . 'Person/Views/_elements/filtering/form.php'); ?>

<?php if (empty($persons)): ?>
    <p><?php echo stmLang('no-content'); ?></p>
<?php else: ?>
    <table>
        <tr>
            <th><?php echo stmLang('name'); ?></th>
            <th><?php echo stmLang('team', 'links'); ?></th>
        </tr>
        <?php foreach($persons as $person): ?>
        <tr>
            <td><?php echo $person; ?></td>
            <td>
                <a href="?module=person&action=read&type=person&prs=<?php echo $person->getId(); ?>"><?php echo stmLang('header', 'person', 'read', 'person'); ?></a>,
                <a href="?module=person&action=read&type=team&prs=<?php echo $person->getId(); ?>"><?php echo stmLang('menu', 'teams'); ?></a>,
                <a href="?module=person&action=read&type=club&prs=<?php echo $person->getId(); ?>"><?php echo stmLang('menu', 'clubs'); ?></a>,
                <a href="?module=statistics&action=read&type=person_players&prs=<?php echo $person->getId(); ?>"><?php echo stmLang('header', 'statistics', 'index'); ?></a>
            </td>
        </tr>
        <?php endforeach; ?>
    </table>
<?php endif;
