<?php
/*
 * List of clubs where the selected person is employed
 * $club_members
 */
?>

<?php if (empty($club_members)): ?>
    <p><?php echo stmLang('no-content'); ?></p>
<?php else: ?>
    <ul>
        <?php foreach($club_members as $club_member): extract($club_member->toArray()); ?>
        <li>
            <a href="?module=club&action=read&type=club&clb=<?php echo $club_member->getIdClub();?>">
                <?php echo $club; ?>
            </a>
            (<?php echo $position; ?>)
        </li>
        <?php endforeach; ?>
    </ul>
<?php endif;
