<?php
/*
 * Detail of selected person
 * $person            selected person
 * $person_details    from Person::toArray
 * $attributes        from \STM\StmFactory::find()->PersonAttribute->findByPerson
 */
?>

<ul>
    <li><?php echo stmLang('name'); ?>: <strong><?php echo $person; ?></strong></li>
    <li><?php echo stmLang('person', 'birth-date'); ?>: <strong><?php echo \STM\Utils\Dates::convertDatetimeToString($person_details['birth_date']); ?></strong></li>
    <li><?php echo stmLang('person', 'characteristic'); ?>:
        <aside class="text"><strong><?php echo $person_details['characteristic']; ?></strong></aside>
    </li>
    <li><?php echo stmLang('person', 'country'); ?>: <strong><?php echo $person_details['country']; ?></strong></li>
</ul>

<hr />
<h2><?php echo stmLang('person', 'attributes'); ?></h2>
<?php if (empty($attributes)): ?>
    <p><?php echo stmLang('no-content'); ?></p>
<?php else: ?>
    <ul>
        <?php foreach($attributes as $attribute): ?>
        <li>
            <?php echo $attribute; ?>
            <aside>
                <a href="<?php echo buildUrl(array('action' => 'update', 'type' => 'attribute', 'atr' => $attribute->getIdAttribute())); ?>"><?php echo stmLang('person', 'attribute-update'); ?></a>
                <?php include(STM_MODULES_ROOT . 'Person/Views/_elements/delete/attribute.php'); ?>
            </aside>
        </li>
        <?php endforeach; ?>
    </ul>
<?php endif;
