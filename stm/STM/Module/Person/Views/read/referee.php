<?php
/*
 * List of matches where the selected person was referee
 * $referees
 */
?>

<?php if (empty($referees)): ?>
    <p><?php echo stmLang('no-content'); ?></p>
<?php else: ?>
    <ul>
        <?php foreach($referees as $referee): extract($referee->toArray()); ?>
        <li>
            <a href="?module=match&action=read&type=referees&m=<?php echo $referee->getIdMatch();?>"><?php echo $match; ?></a>
            (<?php echo $position; ?>)
        </li>
        <?php endforeach; ?>
    </ul>
<?php endif;
