<?php
/*
 * List of teams where the selected person is employed
 * $team_members
 */
?>

<?php if (empty($team_members)): ?>
    <p><?php echo stmLang('no-content'); ?></p>
<?php else: ?>
    <table>
        <thead>
            <tr>
                <th><?php echo stmLang('person', 'team'); ?></th>
                <th><?php echo stmLang('person', 'position'); ?></th>
                <th><?php echo stmLang('person', 'since'); ?></th>
                <th><?php echo stmLang('person', 'to'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($team_members as $team_member): extract($team_member->toArray()); ?>
            <tr>
                <td><a href="?module=team&action=read&type=team&t=<?php echo $team_member->getIdTeam();?>"><?php echo $team; ?></a></td>
                <td><?php echo $position; ?></td>
                <td><?php echo \STM\Utils\Dates::convertDatetimeToString($date_since, '-'); ?></td>
                <td><?php echo \STM\Utils\Dates::convertDatetimeToString($date_to, '-'); ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php endif;
