<?php
/**
 * Update attribute value  (PersonController::updateAttribute)
 */
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <label for="value"><?php echo stmLang('person', 'attribute'); ?>: </label>
    <input type="text" id="value" name="value" size="40" maxlength="100" value="<?php echo $_POST['value']; ?>" />

    <input type="submit" name="update" value="<?php echo stmLang('form', 'change'); ?>" />
</form>
