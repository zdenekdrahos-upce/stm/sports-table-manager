    <h2><a href="<?php echo buildUrl(array('module' => 'person', 'action' => 'read', 'type' => 'person')) ?>"><?php echo $person; ?></a></h2>
    <ul>
        <li><a href="<?php echo buildUrl(array('module' => 'person', 'action' => 'read', 'type' => 'club', 'c' => null, 't' => null)) ?>"><?php echo stmLang('header', 'person', 'read', 'club'); ?></a></li>
        <li><a href="<?php echo buildUrl(array('module' => 'person', 'action' => 'read', 'type' => 'team', 'c' => null, 't' => null)) ?>"><?php echo stmLang('header', 'person', 'read', 'team'); ?></a></li>
        <li><a href="<?php echo buildUrl(array('module' => 'statistics', 'action' => 'read', 'type' => 'person_players', 'c' => null, 't' => null)) ?>"><?php echo stmLang('header', 'statistics', 'read', 'person_players'); ?></a></li>
        <li><a href="<?php echo buildUrl(array('module' => 'statistics', 'action' => 'read', 'type' => 'person_matches', 'c' => null, 't' => null)) ?>"><?php echo stmLang('header', 'statistics', 'read', 'person_matches'); ?></a></li>
        <li><a href="<?php echo buildUrl(array('module' => 'person', 'action' => 'read', 'type' => 'referee', 'c' => null, 't' => null)) ?>"><?php echo stmLang('header', 'person', 'read', 'referee'); ?></a></li>
        <li><a href="<?php echo buildUrl(array('module' => 'person', 'action' => 'update', 'type' => 'person', 'c' => null, 't' => null)) ?>"><?php echo stmLang('header', 'person', 'update', 'person'); ?></a></li>
        <li><a href="<?php echo buildUrl(array('module' => 'person', 'action' => 'create', 'type' => 'attribute', 'c' => null, 't' => null)) ?>"><?php echo stmLang('header', 'person', 'create', 'attribute'); ?></a></li>
        <li><?php include(STM_MODULES_ROOT . 'Person/Views/_elements/delete/attributes.php'); ?></li>
        <li><?php include(STM_MODULES_ROOT . 'Person/Views/_elements/delete/person.php'); ?></li>
    </ul>
