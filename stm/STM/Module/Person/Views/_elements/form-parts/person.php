<?php
/**
 * Form for create or update person
 * $submit_button       text in submit button
 */
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <h2><?php echo stmLang('form', 'required'); ?></h2>
    <label for="forename"><?php echo stmLang('person', 'forename'); ?>: </label>
    <input type="text" id="forename" name="forename" size="40" maxlength="40" value="<?php echo $_POST['forename']; ?>" />
    <label for="surname"><?php echo stmLang('person', 'surname'); ?>: </label>
    <input type="text" id="surname" name="surname" size="40" maxlength="40" value="<?php echo $_POST['surname']; ?>" />

    <h2><?php echo stmLang('form', 'optional'); ?></h2>
    <label for="birth_date"><?php echo stmLang('person', 'birth-date'); ?>: </label>
    <?php echo \STM\Web\HTML\Forms::inputDate('birth_date'); ?>

    <label for="club_info"><?php echo stmLang('person', 'characteristic'); ?>: </label>
    <textarea id="characteristic" name="characteristic" cols="32" rows="8"><?php echo $_POST['characteristic']; ?></textarea>

    <label for="country"><?php echo stmLang('person', 'country'); ?>: </label>
    <?php \STM\Web\HTML\Forms::select('id_country', \STM\Utils\Arrays::getAssocArray($countries, 'getId', '__toString'), true); ?>

    <input type="submit" name="submit_person" value="<?php echo stmLang('form', $submit_button); ?>" />

</form>
