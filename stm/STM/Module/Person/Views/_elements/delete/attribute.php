<?php
$url = '?module=person&action=delete&type=attribute&prs=' . $_GET['prs'] . '&atr=' . $attribute->getIdAttribute();
$submit_message = stmLang('person', 'delete-attribute');
$question = stmLang('person', 'delete-attribute-msg') . $attribute . " ({$person})";
include(STM_ADMIN_TEMPLATE_ROOT . 'form/delete-form.php');
