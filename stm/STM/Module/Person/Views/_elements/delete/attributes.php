<?php
$url = '?module=person&action=delete&type=attributes&prs=' . $_GET['prs'];
$submit_message = stmLang('person', 'delete-attributes');
$question = stmLang('person', 'delete-attributes-msg') . $person;
include(STM_ADMIN_TEMPLATE_ROOT . 'form/delete-form.php');
