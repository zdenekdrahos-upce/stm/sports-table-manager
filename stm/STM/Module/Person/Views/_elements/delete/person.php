<?php
$url = '?module=person&action=delete&type=person&prs=' . $person->getId();
$submit_message = stmLang('person', 'delete');
$question = stmLang('person', 'delete-msg') . $person->__toString();
include(STM_ADMIN_TEMPLATE_ROOT . 'form/delete-form.php');
