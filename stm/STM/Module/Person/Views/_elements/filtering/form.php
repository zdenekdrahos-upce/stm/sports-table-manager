<?php
/*
 * Filter form of persons
 * $teams
 */
?>

<h2><a href="javascript:toggleDiv('toogleFilter');"><?php echo stmLang('person', 'filter', 'header'); ?></a></h2>
<div class="toogleFilter"<?php if (!empty($_POST)) echo ' id="displayed"' ?>>
<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <label for="no_team" class="inline"><?php echo stmLang('person', 'filter', 'no-team'); ?>:</label>
    <input type="checkbox" id="no_team" name="no_team" class="inline" <?php echo (isset($_POST['no_team'])) ? 'checked="checked"' : ''; ?> /><br />
    <label for="id_team" class="inline"><?php echo stmLang('competition', 'team'); ?>:</label>
    <?php \STM\Web\HTML\Forms::select('id_team', \STM\Utils\Arrays::getAssocArray($teams, 'getId'), true, 'inline'); ?><br />
    <label for="forename" class="inline"><?php echo stmLang('person', 'forename'); ?>: </label>
    <input type="text" id="forename" name="forename" class="inline" size="40" maxlength="80" value="<?php echo isset($_POST['forename']) ? $_POST['forename'] : ''; ?>" /><br />
    <label for="surname" class="inline"><?php echo stmLang('person', 'surname'); ?>: </label>
    <input type="text" id="surname" name="surname" class="inline" size="40" maxlength="80" value="<?php echo isset($_POST['surname']) ? $_POST['surname'] : ''; ?>" /><br />
    <input class="inline" type="submit" value="<?php echo stmLang('match', 'filter', 'submit'); ?>" />
</form>
</div>
<hr />
