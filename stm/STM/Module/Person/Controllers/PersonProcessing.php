<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\Person;

use STM\Module\ModuleProcessing;
use STM\Web\Message\SessionMessage;
use STM\Person\Person;
use STM\Person\Attribute\PersonAttribute;
use STM\Person\PersonValidator;
use STM\Person\Attribute\PersonAttributeValidator;

class PersonProcessing extends ModuleProcessing
{
    /** @var \Person */
    private $person;

    public function __construct($person)
    {
        $this->person = $person instanceof Person ? $person : false;
        parent::__construct($this->person, '\STM\Team\TeamEvent');
        PersonValidator::init($this->formProcessor);
        PersonAttributeValidator::init($this->formProcessor);
    }

    public function createPerson()
    {
        if (isset($_POST['submit_person'])) {
            unset($_POST['submit_person']);
            $this->formProcessor->escapeValues();
            $result = Person::create($_POST);
            parent::checkAction($result, 'PERSON', "New Person: {$_POST['forename']} {$_POST['surname']}");
            parent::setSessionMessage(
                $result,
                $this->getMessage('create', "{$_POST['forename']} {$_POST['surname']}")
            );
            return $result;
        } else {
            $this->initFormFields();
        }
        return false;
    }

    public function createAttribute()
    {
        if (isset($_POST['create'])) {
            $this->formProcessor->escapeValues();
            $attributes = array(
                'person' => $this->person,
                'attribute' => $this->entities->Attribute->findById($_POST['id_attribute']),
                'value' => $_POST['value']
            );
            $result = PersonAttribute::create($attributes);
            parent::checkAction(
                $result,
                'PERSON_ATTRIBUTE',
                "Add attribute {$attributes['attribute']} to Person {$this->person} ({$this->person->getId()})"
            );
            parent::setSessionMessage($result, $this->getMessage('create', stmLang('person', 'attribute')));
            return $result;
        } else {
            $this->formProcessor->initVars(array('id_attribute', 'value'));
        }
        return false;
    }

    public function deletePerson()
    {
        if (parent::canBeDeleted()) {
            if ($this->person->canBeDeleted()) {
                $delete = $this->person->delete();
                parent::setSessionMessage(
                    $delete,
                    $this->getMessage('delete-success'),
                    $this->getMessage('delete-fail')
                );
                parent::log(
                    'PERSON',
                    "Delete Person: {$this->person->__toString()} ({$this->person->getId()})",
                    $delete
                );
            } else {
                $this->session->setMessageFail(SessionMessage::get('delete-person'));
            }
        }
    }

    public function deletePersonAttributes()
    {
        if (parent::canBeDeleted()) {
            $delete = PersonAttribute::deletePersonAttributes($this->person);
            parent::setSessionMessage(
                $delete,
                "Person {$this->person->__toString()} attributes were deleted",
                "Person {$this->person->__toString()} attributes were not deleted, try it again"
            );
            parent::setSessionMessage(
                $delete,
                $this->getMessage('delete-success', stmLang('person', 'attributes')),
                $this->getMessage('delete-fail', stmLang('person', 'attributes'))
            );
            parent::log(
                'PERSON_ATTRIBUTE',
                "Delete Attributes from Person {$this->person->__toString()} ({$this->person->getId()})",
                $delete
            );
        }
    }

    public function deleteAttribute($attribute)
    {
        if (parent::canBeDeleted() && $attribute instanceof PersonAttribute) {
            $delete = $attribute->delete();
            parent::setSessionMessage(
                $delete,
                $this->getMessage('delete-success', stmLang('person', 'attribute')),
                $this->getMessage('delete-fail', stmLang('person', 'attribute'))
            );
            parent::log(
                'PERSON_ATTRIBUTE',
                "Delete {$this->person->__toString()}'s Attribute [{$attribute->getAttribute()}]",
                $delete
            );
        }
    }

    public function updatePerson()
    {
        if (parent::isObjectSet() && isset($_POST['submit_person'])) {
            unset($_POST['submit_person']);
            $this->formProcessor->escapeValues();
            $result = $this->person->update($_POST);
            parent::checkAction(
                $result,
                'PERSON',
                "Update Person Details of {$this->person->__toString()} ({$this->person->getId()})"
            );
            parent::setSessionMessage($result, $this->getMessage('change'));
            return $result;
        } else {
            $this->initFormFields();
            $this->checkFields();
        }
        return false;
    }

    public function updateAttribute($attribute)
    {
        if (parent::isObjectSet() && isset($_POST['update']) && $attribute instanceof PersonAttribute) {
            $this->formProcessor->escapeValues();
            $result = $attribute->updateValue($_POST['value']);
            parent::checkAction(
                $result,
                'PERSON_ATTRIBUTE',
                "Update {$this->person->__toString()}'s Attribute [{$attribute->getAttribute()}]"
            );
            parent::setSessionMessage($result, $this->getMessage('change'));
            return $result;
        } else {
            $_POST['value'] = $attribute->getValue();
        }
        return false;
    }

    private function initFormFields()
    {
        $this->formProcessor->initVars(array('forename', 'surname', 'birth_date', 'characteristic', 'id_country'));
    }

    private function checkFields()
    {
        if (parent::isObjectSet()) {
            $current_values = $this->person->toArray();
            foreach ($_POST as $key => $value) {
                if (empty($value) && array_key_exists($key, $current_values)) {
                    $_POST[$key] = $current_values[$key];
                }
            }
            $_POST['id_country'] = empty($_POST['id_country']) ? $this->person->getIdCountry() : $_POST['id_country'];
        }
    }

    private function getMessage($name, $attribute = false)
    {
        $attribute = $attribute || !parent::isObjectSet() ? $attribute : $this->person->__toString();
        return SessionMessage::get($name, array(stmLang('club', 'person'), $attribute));
    }
}
