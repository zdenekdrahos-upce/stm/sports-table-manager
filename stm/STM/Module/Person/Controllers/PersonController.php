<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\Person;

use STM\Module\Controller;
use STM\Web\Message\SessionMessage;
use STM\Person\Person;
use STM\Person\Attribute\PersonAttribute;

class PersonController extends Controller
{
    /** @var \Person */
    private $person;
    /** @var \STM\Person\Attribute\PersonAttribute */
    private $attribute;
    /** @var \STM\Module\Person\PersonProcessing */
    private $processing;

    protected function __construct()
    {
        parent::__construct();
        parent::setMainPage('index.php?module=person');
        $this->loadSelectedPerson();
        $this->processing = new PersonProcessing($this->person);
    }

    /** index.php?module=person */
    public function index()
    {
        parent::setIndexInLeftMenu();
        parent::setContent(
            array(
                'persons' => $this->entities->Person->findByFilter($_POST),
                'teams' => $this->entities->Team->findAll(),
            ),
            'index'
        );
    }

    /** index.php?module=person&action=create&type=person */
    public function createPerson()
    {
        if ($this->processing->createPerson()) {
            parent::redirectToIndexPage();
        } else {
            parent::setIndexInLeftMenu();
            parent::setFormErrors($this->processing);
            parent::setContent(array('countries' => $this->entities->Country->findAll()));
        }
    }

    /** index.php?module=person&action=create&type=person&PRS=ID_PERSON */
    public function createAttribute()
    {
        if ($this->processing->createAttribute()) {
            $this->redirectToPersonDetail();
        } else {
            parent::setProfileInLeftMenu();
            parent::setFormErrors($this->processing);
            parent::setContent(array('attributes' => $this->entities->Attribute->findAll()));
        }
    }

    /** index.php?module=person&action=read&type=person&PRS=ID_PERSON */
    public function readPerson()
    {
        if ($this->checkSelectedPerson()) {
            parent::setProfileInLeftMenu();
            parent::setContent(
                array(
                    'person' => $this->person,
                    'person_details' => $this->person->toArray(),
                    'attributes' => $this->entities->PersonAttribute->findByPerson($this->person),
                )
            );
        }
    }

    /** index.php?module=person&action=read&type=club&PRS=ID_PERSON */
    public function readClub()
    {
        if ($this->checkSelectedPerson()) {
            parent::setProfileInLeftMenu();
            parent::setContent(array('club_members' => $this->entities->ClubMember->findByPerson($this->person)));
        }
    }

    /** index.php?module=person&action=read&type=team&PRS=ID_PERSON */
    public function readTeam()
    {
        if ($this->checkSelectedPerson()) {
            parent::setProfileInLeftMenu();
            parent::setContent(array('team_members' => $this->entities->TeamMember->findByPerson($this->person)));
        }
    }

    /** index.php?module=person&action=read&type=referee&PRS=ID_PERSON */
    public function readReferee()
    {
        if ($this->checkSelectedPerson()) {
            parent::setProfileInLeftMenu();
            parent::setContent(array('referees' => $this->entities->MatchReferee->findByPerson($this->person)));
        }
    }

    /** index.php?module=person&action=update&type=person&PRS=ID_PERSON */
    public function updatePerson()
    {
        if ($this->checkSelectedPerson()) {
            if ($this->processing->updatePerson()) {
                redirect(buildUrl(array('action' => 'read')));
            } else {
                $data = array(
                    'person' => $this->person,
                    'countries' => $this->entities->Country->findAll(),
                );
                parent::setProfileInLeftMenu();
                parent::setFormErrors($this->processing);
                parent::setContent($data);
            }
        }
    }

    /** index.php?module=person&action=update&type=attribute&PRS=ID_PERSON&ATTR=ID_ATTRIBUTE */
    public function updateAttribute()
    {
        if ($this->checkPersonAttribute()) {
            if ($this->processing->updateAttribute($this->attribute)) {
                redirect(buildUrl(array('action' => 'read', 'type' => 'person', 'atr' => null)));
            } else {
                parent::setProfileInLeftMenu();
                parent::setFormErrors($this->processing);
                parent::setContent();
            }
        }
    }

    /** index.php?module=person&action=delete&type=person&PRS=ID_PERSON */
    public function deletePerson()
    {
        if ($this->checkSelectedPerson()) {
            $this->processing->deletePerson();
            parent::redirectToIndexPage();
        }
    }

    /** index.php?module=person&action=delete&type=attributes&PRS=ID_PERSON */
    public function deleteAttributes()
    {
        if ($this->checkSelectedPerson()) {
            $this->processing->deletePersonAttributes();
            $this->redirectToPersonDetail();
        }
    }

    /** index.php?module=person&action=delete&type=attribute&PRS=ID_PERSON&ATTR=ID_ATTRIBUTE */
    public function deleteAttribute()
    {
        if ($this->checkPersonAttribute()) {
            $this->processing->deleteAttribute($this->attribute);
            $this->redirectToPersonDetail();
        }
    }

    private function checkPersonAttribute()
    {
        if ($this->checkSelectedPerson()) {
            $this->loadSelectedAttribute();
            if ($this->checkSelectedAttribute()) {
                return true;
            }
        }
    }

    private function checkSelectedPerson()
    {
        if (!($this->person instanceof Person)) {
            $this->session->setMessageFail(SessionMessage::get('invalid', array(stmLang('club', 'person'))));
            parent::redirectToIndexPage();
        }
        return true;
    }

    private function checkSelectedAttribute()
    {
        if (!($this->attribute instanceof PersonAttribute)) {
            $this->session->setMessageFail(SessionMessage::get('invalid', array(stmLang('person', 'attribute'))));
            $this->redirectToPersonDetail();
        }
        return true;
    }

    private function loadSelectedPerson()
    {
        $this->person = isset($_GET['prs']) ? $this->entities->Person->findById($_GET['prs']) : false;
    }

    private function loadSelectedAttribute()
    {
        $attribute = isset($_GET['atr']) ? $this->entities->Attribute->findById($_GET['atr']) : false;
        $this->attribute = $this->entities->PersonAttribute->findById($this->person, $attribute);
    }

    private function redirectToPersonDetail()
    {
        redirect(buildUrl(array('action' => 'read', 'type' => 'person', 'atr' => null)));
    }

    protected function getProfileLinks()
    {
        return array('person' => $this->person);
    }
}
