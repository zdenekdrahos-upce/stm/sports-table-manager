<?php
/*
 * Update member
 * $team_member
 * $positions
 */
extract($team_member->toArray());
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <h2><?php echo stmLang('form', 'required'); ?></h2>
    <label for="current"><?php echo stmLang('team', 'person'); ?>: </label>
    <input readonly="readonly" type="text" id="current" size="40" value="<?php echo $person; ?>" />
    <?php include(STM_MODULES_ROOT . 'Team/Views/_elements/form-parts/member.php'); ?>

    <input type="submit" name="update" value="<?php echo stmLang('form', 'change'); ?>" />
</form>
