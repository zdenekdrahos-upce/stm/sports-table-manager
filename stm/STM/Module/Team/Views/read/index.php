<?php
/**
 * List of all teams
 * $teams                   current teams from application
 */
?>

<?php
$fields = array(
    'c' => array(
        'name' => stmLang('competition', 'competition'),
        'values' => \STM\Utils\Arrays::getAssocArray($competitions, 'getId', '__toString'),
    ),
);
include(STM_ADMIN_TEMPLATE_ROOT . 'form/filtering-form.php');
?>

<?php if (empty($teams)): ?>
    <p><?php echo stmLang('no-content'); ?></p>
<?php else: ?>
    <table>
        <tr>
            <th><?php echo stmLang('competition', 'team'); ?></th>
            <th><?php echo stmLang('team', 'club'); ?></th>
            <th><?php echo stmLang('team', 'links'); ?></th>
        </tr>
        <?php foreach ($teams as $team): ?>
        <tr>
            <td><a href="?module=team&action=read&type=team&t=<?php echo $team->getId(); ?>"><?php echo $team; ?></a></td>
            <td><?php echo $team->getNameClub(); ?></td>
            <td>
                <a href="?module=team&action=read&type=competitions&t=<?php echo $team->getId(); ?>"><?php echo stmLang('menu', 'competitions'); ?></a>,
                <a href="?module=team&action=read&type=members&t=<?php echo $team->getId(); ?>"><?php echo stmLang('header', 'team', 'read', 'members'); ?></a>
            </td>
        </tr>
        <?php endforeach; ?>
    </table>
<?php endif;
