<?php
/**
 * Info page about selected team (TeamController::teTeam)
 * $team        Team
 * $club        club
 * $id_club
 * $last_match
 * $next_match
 */
use STM\Match\ExtendedMatch;
?>

<ul>
    <li><?php echo stmLang('name'); ?>: <strong><?php echo $team; ?></strong></li>
    <li><?php echo stmLang('team', 'club'); ?>: <strong><a href="<?php echo buildUrl(array('module' => 'club', 'type' => 'club', 'clb' => $id_club)); ?>"><?php echo $club; ?></a></strong></li>
    <li><?php
        echo stmLang('team', 'last-match') . ': ';
        if ($last_match) {
            $match = new ExtendedMatch($last_match, $team->getId());
            echo " <a href=\"?module=match&action=read&type=match&m={$last_match->getId()}\">{$last_match} {$match->score}</a>";
            echo ' (' . \STM\Utils\Dates::convertDatetimeToString($last_match->getDate(), '-') . ')';
        } else {
            echo '<strong>-</strong>';
        }
    ?></li>
    <li><?php
        echo stmLang('team', 'next-match') . ': ';
        if ($next_match) {
            $match = new ExtendedMatch($next_match, $team->getId());
            echo " <a href=\"?module=match&action=read&type=match&m={$next_match->getId()}\">{$next_match}</a>";
            echo ' (' . \STM\Utils\Dates::convertDatetimeToString($next_match->getDate(), '-') . ')';
        } else {
            echo '<strong>-</strong>';
        }
    ?></li>
</ul>
