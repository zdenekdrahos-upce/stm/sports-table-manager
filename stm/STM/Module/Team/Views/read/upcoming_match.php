<?php
/**
 * Preview of next match of the team
 * $preview     instanceof \STM\Match\Preview\MatchPreview
 */
?>

<?php
if (empty($preview->match)) {
    echo '<p>' . stmLang('no-content') . '</p>';
} else {
    include(STM_ADMIN_TEMPLATE_ROOT . 'statistics/upcoming-match.php');
}
