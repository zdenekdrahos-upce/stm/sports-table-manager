<?php
/**
 * Info page about competitions for selected team (TeamController::read_competitions)
 * $competitions            competitions which the team parcitipated
 * $id_team
 */
use STM\Competition\CompetitionType;
?>

<?php if ($competitions): ?>
    <table>
        <tr>
            <th><?php echo stmLang('competition', 'competition'); ?></th>
            <th><?php echo stmLang('team', 'links'); ?></th>
        </tr>
    <?php foreach ($competitions as $competition): ?>
        <tr>
            <td>
                <a href="?module=competition&action=read&type=competition&c=<?php echo $competition->getId(); ?>"><?php echo $competition; ?></a>
                (<?php echo stmLang('constants', 'competitiontype', CompetitionType::getCompetitionType($competition->getIdCompetitionType())); ?>)
            </td>
            <td>
                <a href="?module=statistics&action=read&type=team&t=<?php echo $id_team . '&c=' . $competition->getId(); ?>"><?php echo stmLang('header', 'statistics', 'read', 'team'); ?></strong></a>,
                <?php if ($competition->getIdCompetitionType() != 'P'): ?>
                <a href="?module=competition&action=read&type=matches&t=<?php echo $id_team . '&c=' . $competition->getId(); ?>"><?php echo stmLang('header', 'competition', 'read', 'matches'); ?></strong></a>
                <?php else: ?>
                <a href="?module=playoff&action=read&type=tree&c=<?php echo $competition->getId(); ?>"><?php echo stmLang('header', 'playoff', 'read', 'tree'); ?></strong></a>
                <?php endif; ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </table>
<?php else: ?>
    <p><?php echo stmLang('no-content'); ?></p>
<?php endif;
