<?php
/*
 * List of members of the team
 * $team_members
 */
?>
<h2><a href="javascript:toggleDiv('toogleFilter');"><?php echo stmLang('match', 'filter', 'header'); ?></a></h2>
<div class="toogleFilter"<?php if (!empty($_POST)) echo ' id="displayed"' ?>>
<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <label for="persons" class="inline"><?php echo stmLang('menu', 'persons'); ?>:</label>
    <?php
    $options = array(
        'p' => stmLang('team', 'memberfilter-only-players'),
        't' => stmLang('team', 'memberfilter-no-players'),
    );
    \STM\Web\HTML\Forms::select('persons', $options, true, 'inline');
    ?><br />
    <label for="date" class="inline"><?php echo stmLang('team', 'memberfilter-date'); ?>: </label>
    <?php echo \STM\Web\HTML\Forms::inputDate('date'); ?>
    <input class="inline" type="submit" value="<?php echo stmLang('match', 'filter', 'submit'); ?>" />

</form>
</div>
<hr />

<?php if (empty($team_members)): ?>
    <p><?php echo stmLang('no-content'); ?></p>
<?php else: ?>
    <table>
        <thead>
            <tr>
                <th><?php echo stmLang('team', 'person'); ?></th>
                <th><?php echo stmLang('person', 'position'); ?></th>
                <th><?php echo stmLang('person', 'since'); ?></th>
                <th><?php echo stmLang('person', 'to'); ?></th>
                <th><?php echo stmLang('team', 'player'); ?></th>
                <th><?php echo stmLang('team', 'links'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($team_members as $team_member): extract($team_member->toArray()); ?>
            <tr>
                <td><a href="?module=person&action=read&type=person&prs=<?php echo $team_member->getIdPerson();?>"><?php echo $person; ?></a></td>
                <td><?php echo $position; ?></td>
                <td><?php echo \STM\Utils\Dates::convertDatetimeToString($date_since, '-'); ?></td>
                <td><?php echo \STM\Utils\Dates::convertDatetimeToString($date_to, '-'); ?></td>
                <td class="<?php echo $is_team_player ? 'yes' : 'no';?>"><?php echo $dress_number; ?></td>
                <td>
                    <?php if ($is_team_player): ?>
                    <?php include(STM_MODULES_ROOT . 'Team/Views/_elements/delete/player.php'); ?><br />
                    <?php endif; ?>
                    <a href="<?php echo buildUrl(array('action' => 'update', 'type' => 'member', 'tm' => $team_member->getId()));?>"><?php echo stmLang('form', 'change'); ?></a>,
                    <?php include(STM_MODULES_ROOT . 'Team/Views/_elements/delete/member.php'); ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php endif;
