<?php
/**
 * Create new team (TeamController::create_team)
 */
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <?php include(STM_MODULES_ROOT . 'Team/Views/_elements/form-parts/team.php'); ?>

    <input type="submit" name="create_team" value="<?php echo stmLang('form', 'create'); ?>" />
</form>

<hr />
<a href="?module=club&action=create&type=club"><?php echo  stmLang('header', 'club', 'create', 'club');?></a>
