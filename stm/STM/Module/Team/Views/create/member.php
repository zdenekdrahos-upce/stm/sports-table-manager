<?php
/**
 * Create new team member
 */
?>

<?php include(STM_MODULES_ROOT . 'Person/Views/_elements/filtering/form.php'); ?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">

    <table>
        <tr>
            <th colspan="3"><?php echo stmLang('form', 'required'); ?></th>
            <th colspan="4"><?php echo stmLang('form', 'optional'); ?></th>
        </tr>
        <tr>
            <th>&nbsp;</th>
            <th><?php echo stmLang('team', 'person'); ?></th>
            <th><?php echo stmLang('person', 'position'); ?></th>
            <th><?php echo stmLang('person', 'since'); ?></th>
            <th><?php echo stmLang('person', 'to'); ?></th>
            <th><?php echo stmLang('team', 'is-player'); ?></th>
            <th><?php echo stmLang('team', 'dress-number'); ?></th>
        </tr>
        <?php foreach($persons as $person): ?>
            <tr>
                <td><input id="<?php echo $person->getId(); ?>" type="checkbox" name="persons[<?php echo $person->getId(); ?>][id_person]" value="<?php echo $person->getId(); ?>" /></td>
                <td><label for="<?php echo $person->getId(); ?>"><?php echo $person; ?></label></td>
                <td><?php \STM\Web\HTML\Forms::select("persons[{$person->getId()}][id_position]", $positions); ?></td>
                <td><input type="text" size="10" maxlength="50" name="persons[<?php echo $person->getId(); ?>][date_since]" /></td>
                <td><input type="text" size="10" maxlength="50" name="persons[<?php echo $person->getId(); ?>][date_to]" /></td>
                <td class="checkbox"><input type="checkbox" class="inline" name="persons[<?php echo $person->getId(); ?>][is_player]" /></td>
                <td><input class="inline" type="text" name="persons[<?php echo $person->getId(); ?>][dress_number]" size="5" maxlength="3" value="" /></td>
            </tr>
        <?php endforeach; ?>
    </table>

    <input type="submit" name="create" value="<?php echo stmLang('form', 'create'); ?>" />
</form>
