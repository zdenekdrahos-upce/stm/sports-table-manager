<?php
$url = '?module=team&action=delete&type=player&t=' . $_GET['t'] . '&tm=' . $team_member->getId();
$submit_message = stmLang('team', 'delete-player');
$question = stmLang('team', 'delete-player-msg') . $team_member . " ({$position}, {$team})";
include(STM_ADMIN_TEMPLATE_ROOT . 'form/delete-form.php');
