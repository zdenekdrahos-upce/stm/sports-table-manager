<?php
$url = '?module=team&action=delete&type=member&t=' . $_GET['t'] . '&tm=' . $team_member->getId();
$submit_message = stmLang('team', 'delete-member');
$question = stmLang('team', 'delete-member-msg') . $team_member . " ({$position}, {$team})";
include(STM_ADMIN_TEMPLATE_ROOT . 'form/delete-form.php');
