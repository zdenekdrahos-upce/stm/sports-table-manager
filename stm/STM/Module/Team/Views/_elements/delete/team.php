<?php
$url = '?module=team&action=delete&type=team&t=' . $_GET['t'];
$submit_message = stmLang('team', 'delete');
$question = stmLang('team', 'delete-msg') . $team_name;
include(STM_ADMIN_TEMPLATE_ROOT . 'form/delete-form.php');
