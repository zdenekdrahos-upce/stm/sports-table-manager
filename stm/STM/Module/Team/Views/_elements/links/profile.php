    <h2><a href="<?php echo buildUrl(array('module' => 'team', 'action' => 'read', 'type' => 'team')) ?>"><?php echo $team_name; ?></a></h2>
    <ul>
        <li><a href="<?php echo buildUrl(array('module' => 'team', 'action' => 'read', 'type' => 'competitions', 'c' => null)); ?>"><?php echo stmLang('header', 'team', 'read', 'competitions'); ?></a></li>
        <li><a href="<?php echo buildUrl(array('module' => 'team', 'action' => 'read', 'type' => 'upcoming_match', 'c' => null)); ?>"><?php echo stmLang('header', 'team', 'read', 'upcoming_match'); ?></a></li>
        <li><a href="<?php echo buildUrl(array('module' => 'statistics', 'action' => 'read', 'type' => 'team_players', 'c' => null)); ?>"><?php echo stmLang('header', 'statistics', 'read', 'team_players'); ?></a></li>
        <li><a href="<?php echo buildUrl(array('module' => 'statistics', 'action' => 'read', 'type' => 'team', 'c' => null)); ?>"><?php echo stmLang('header', 'statistics', 'read', 'team'); ?></a></li>
        <li><a href="<?php echo buildUrl(array('module' => 'team', 'action' => 'read', 'type' => 'members', 'c' => null)); ?>"><?php echo stmLang('header', 'team', 'read', 'members'); ?></a></li>
        <li><a href="<?php echo buildUrl(array('module' => 'team', 'action' => 'create', 'type' => 'member', 'c' => null)); ?>"><?php echo stmLang('header', 'team', 'create', 'member'); ?></a></li>
        <li><a href="<?php echo buildUrl(array('module' => 'team', 'action' => 'update', 'type' => 'team', 'c' => null)); ?>"><?php echo stmLang('header', 'team', 'update', 'team'); ?></a></li>
        <li><?php include(STM_MODULES_ROOT . 'Team/Views/_elements/delete/team.php'); ?></li>
    </ul>
