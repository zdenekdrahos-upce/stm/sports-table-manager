    <label for="position"><?php echo stmLang('person', 'position'); ?>: </label>
    <?php \STM\Web\HTML\Forms::select('id_position', \STM\Utils\Arrays::getAssocArray($positions, 'getId', '__toString')); ?>

    <h2><?php echo stmLang('form', 'optional'); ?></h2>
    <label for="date_since"><?php echo stmLang('person', 'since'); ?>: </label>
    <?php echo \STM\Web\HTML\Forms::inputDate('date_since'); ?>
    <label for="date_to"><?php echo stmLang('person', 'to'); ?>: </label>
    <?php echo \STM\Web\HTML\Forms::inputDate('date_to'); ?>

    <?php if (!isset($team_member) || !$team_member->isTeamPlayer()): ?>
    <label for="is_player" class="inline"><?php echo stmLang('team', 'is-player'); ?>:</label>
    <input type="checkbox" id="is_player" name="is_player" class="inline" <?php echo (isset($_POST['is_player'])) ? 'checked="checked"' : ''; ?> /><br />
    <?php endif; ?>

    <label for="dress_number"><?php echo stmLang('team', 'dress-number'); ?>: </label>
    <input type="text" id="dress_number" name="dress_number" size="40" maxlength="3" value="<?php echo $_POST['dress_number']; ?>" />
