<label for="name"><?php echo stmLang('name'); ?>: </label>
<input type="text" id="name" name="name" size="40" maxlength="50" value="<?php echo $_POST['name']; ?>" />
<label for="club"><?php echo stmLang('team', 'club'); ?>: </label>
<?php \STM\Web\HTML\Forms::select('club', \STM\Utils\Arrays::getAssocArray($clubs, 'getId', '__toString')); ?>
