<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\Team;

use STM\Module\ModuleController;
use STM\Web\Message\SessionMessage;
use STM\Team\Member\TeamMember;
use STM\Utils\Arrays;
use STM\StmCache;

class TeamController extends ModuleController
{
    /** @var \TeamMember */
    private $teamMember;
    /** @var \STM\Module\Team\TeamProcessing */
    private $processing;

    protected function __construct()
    {
        parent::__construct();
        parent::setMainPage('index.php?module=team');
        $this->loadSelectedTeamMember();
        $this->processing = new TeamProcessing($this->team);
    }

    /** index.php?module=team */
    public function index()
    {
        parent::setIndexInLeftMenu();
        $data = array(
            'teams' => parent::checkSelectedCompetition(false) ?
                $this->entities->Team->findByCompetition($this->competition) : $this->entities->Team->findAll(),
            'competitions' => $this->entities->Competition->find(parent::getCompetitionsSelection()),
        );
        parent::setContent($data, 'index');
    }

    /** index.php?module=team&action=create&type=team */
    public function createTeam()
    {
        if ($this->processing->createTeam()) {
            parent::redirectToIndexPage();
        } else {
            parent::setIndexInLeftMenu();
            parent::setFormErrors($this->processing);
            parent::setContent(array('clubs' => $this->entities->Club->findAll()));
        }
    }

    /** index.php?module=club&action=create&type=member&CLB=ID_CLUB */
    public function createMember()
    {
        if ($this->checkSelectedTeam()) {
            $persons = $this->entities->Person->findByFilter($_POST);
            $positions = Arrays::getAssocArray($this->entities->PersonPosition->findAll(), 'getId');
            if ($this->processing->createTeamMembers($persons, $positions)) {
                redirect(buildUrl(array('action' => 'read', 'type' => 'members')));
            } else {
                $data = array(
                    'teams' => $this->entities->Team->findAll(),
                    'persons' => $persons,
                    'positions' => $positions,
                );
                parent::setProfileInLeftMenu();
                parent::setFormErrors($this->processing);
                parent::setContent($data);
            }
        }
    }

    /** index.php?module=team&action=read&type=team&t=ID_TEAM */
    public function readTeam()
    {
        if ($this->checkSelectedTeam()) {
            parent::setProfileInLeftMenu();
            parent::setContent(
                array(
                    'team' => $this->team,
                    'club' => $this->team->getNameClub(),
                    'id_club' => $this->team->getIdClub(),
                    'last_match' => $this->entities->Match->getLastPlayedMatch(null, $this->team->getId()),
                    'next_match' => $this->entities->Match->getFirstUpcomingMatch(null, $this->team->getId())
                )
            );
        }
    }

    /** index.php?module=team&action=read&type=team&t=ID_TEAM&c=ID_COMPETITION */
    public function readCompetitions()
    {
        if ($this->checkSelectedTeam()) {
            $data = array(
                'id_team' => $this->team->getId(),
                'competitions' => $this->entities->Competition->findByTeam($this->team)
            );
            parent::setProfileInLeftMenu();
            parent::setContent($data);
        }
    }

    /** index.php?module=team&action=read&type=upcoming_match&t=ID_TEAM */
    public function readUpcomingMatch()
    {
        if ($this->checkSelectedTeam()) {
            parent::setProfileInLeftMenu();
            parent::setContent(array('preview' => StmCache::getUpcomingMatchPreview($this->team->getId())));
        }
    }

    /** index.php?module=team&action=read&type=members&t=ID_TEAM */
    public function readMembers()
    {
        if ($this->checkSelectedTeam()) {
            parent::setProfileInLeftMenu();
            $members = $this->entities->TeamMember->filterTeamMembers($this->team, $_POST);
            parent::setContent(array('team_members' => $members));
        }
    }

    /** index.php?module=team&action=update&type=team&t=ID_TEAM */
    public function updateTeam()
    {
        if ($this->checkSelectedTeam()) {
            if ($this->processing->updateTeam()) {
                redirect(buildUrl(array('action' => 'read', 'type' => 'team')));
            } else {
                parent::setProfileInLeftMenu();
                parent::setFormErrors($this->processing);
                $data = array('clubs' => $this->entities->Club->findAll());
                parent::setContent($data);
            }
        }
    }

    /** index.php?module=team&action=update&type=club&t=ID_TEAM&tm=ID_TEAM_MEMBER */
    public function updateMember()
    {
        if ($this->checkSelectedTeamMember()) {
            if ($this->processing->updateMember($this->teamMember)) {
                redirect(buildUrl(array('action' => 'read', 'type' => 'members')));
            } else {
                $data = array(
                    'team_member' => $this->teamMember,
                    'positions' => $this->entities->PersonPosition->findAll(),
                );
                parent::setProfileInLeftMenu();
                parent::setFormErrors($this->processing);
                parent::setContent($data);
            }
        }
    }

    /** index.php?module=team&action=delete&type=member&t=ID_TEAM&tm=ID_TEAM_MEMBER */
    public function deleteMember()
    {
        if ($this->checkSelectedTeamMember()) {
            $this->processing->deleteMember($this->teamMember);
            redirect(buildUrl(array('action' => 'read', 'type' => 'members')));
        }
    }

    /** index.php?module=team&action=delete&type=player&t=ID_TEAM&tm=ID_TEAM_MEMBER */
    public function deletePlayer()
    {
        if ($this->checkSelectedTeamMember()) {
            $this->processing->deletePlayer($this->teamMember);
            redirect(buildUrl(array('action' => 'read', 'type' => 'members')));
        }
    }

    /** index.php?module=team&action=delete&type=team&t=ID_TEAM */
    public function deleteTeam()
    {
        if ($this->checkSelectedTeam()) {
            $this->processing->deleteTeam();
            parent::redirectToIndexPage();
        }
    }

    protected function getProfileLinks()
    {
        return array(
            'team_name' => $this->team->__toString(),
        );
    }

    private function checkSelectedTeamMember()
    {
        if (!($this->teamMember instanceof TeamMember)) {
            $this->session->setMessageFail(SessionMessage::get('invalid-member'));
            parent::redirectToIndexPage();
        }
        return true;
    }

    private function loadSelectedTeamMember()
    {
        $this->teamMember = isset($_GET['tm']) ? $this->entities->TeamMember->findById($_GET['tm']) : false;
    }
}
