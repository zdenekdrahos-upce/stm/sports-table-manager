<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\Team;

use STM\Module\ModuleProcessing;
use STM\Web\Message\FormError;
use STM\Web\Message\SessionMessage;
use STM\Team\Team;
use STM\Team\TeamValidator;
use STM\Team\Member\TeamMember;
use STM\Team\Member\TeamMemberValidator;
use STM\Utils\Arrays;

class TeamProcessing extends ModuleProcessing
{
    /** @var \Team */
    private $team;

    public function __construct($team = false)
    {
        $this->team = $team instanceof Team ? $team : false;
        parent::__construct($this->team, '\STM\Team\TeamEvent');
        TeamValidator::init($this->formProcessor);
        TeamMemberValidator::init($this->formProcessor);
    }

    public function createTeam()
    {
        if (isset($_POST['create_team'])) {
            $this->formProcessor->escapeValues();
            $club = isset($_POST['club']) ? $this->entities->Club->findById($_POST['club']) : false;
            $create = Team::create(array('name' => $_POST['name'], 'club' => $club));
            parent::checkAction($create, 'CREATE', $_POST['name'] . ' (' . $club . ')');
            parent::setSessionMessage($create, $this->getMessage('create', $_POST['name']));
            return $create;
        } else {
            $_POST['name'] = '';
            $_POST['club'] = isset($_GET['clb']) ? $_GET['clb'] : '';
        }
        return false;
    }

    public function createTeamMembers($persons, $positions)
    {
        if (parent::isObjectSet() && isset($_POST['create'])) {
            $this->formProcessor->escapeValues();
            $_POST['persons'] = isset($_POST['persons']) ? $_POST['persons'] : array();
            $count_all = 0;
            $count_created = 0;
            $persons = Arrays::getAssocArray($persons, 'getId');
            foreach ($_POST['persons'] as $person_array) {
                if (array_key_exists('id_person', $person_array)) {
                    $this->formProcessor->resetErrors();
                    $count_all++;
                    if ($this->createTeamMember($person_array, $persons, $positions)) {
                        $count_created++;
                    }
                }
            }
            if ($count_all == 0) {
                $this->formProcessor->addError(FormError::get('nothing-checked'));
                return false;
            } else {
                parent::setSessionMessage(
                    true,
                    SessionMessage::get('team-add-member', array($count_created, $count_all))
                );
                parent::log(
                    'TEAM_MEMBER',
                    "Add {$count_created}/{$count_all} member to {$this->team->__toString()}",
                    true
                );
                return true;
            }
        }
        return false;
    }

    private function createTeamMember($person_array, $persons, $positions)
    {
        $this->formProcessor->escapeValues();
        $attributes = array(
            'team' => $this->team,
            'person' => isset($person_array['id_person']) ? $persons[$person_array['id_person']] : false,
            'position' => isset($person_array['id_position']) ? $positions[$person_array['id_position']] : false,
            'date_since' => $person_array['date_since'],
            'date_to' => $person_array['date_to'],
            'is_player' => isset($person_array['is_player']),
            'dress_number' => $person_array['dress_number'],
        );
        return TeamMember::create($attributes);
    }

    public function updateMember($team_member)
    {
        if (isset($_POST['update']) && $team_member instanceof TeamMember) {
            $this->formProcessor->escapeValues();
            $attributes = array(
                'position' => $this->entities->PersonPosition->findById($_POST['id_position']),
                'date_since' => $_POST['date_since'],
                'date_to' => $_POST['date_to'],
                'is_player' => isset($_POST['is_player']) ? true : $team_member->isTeamPlayer(),
                'dress_number' => $_POST['dress_number'],
            );
            $result = $team_member->update($attributes);
            parent::checkAction(
                $result,
                'TEAM_MEMBER',
                "Update {$team_member} [{$attributes['position']}] in {$this->team->__toString()}"
            );
            if ($result && !$team_member->isTeamPlayer() && isset($_POST['is_player'])) {
                $this->createPlayer($team_member, $team_member->__toString(), $attributes['position']);
            }
            if ($result && $team_member->isTeamPlayer() &&
                $attributes['dress_number'] != $team_member->getDressNumber()
            ) {
                $this->updateDressNumber($team_member, $team_member->__toString(), $attributes['position']);
            }
            parent::setSessionMessage($result, $this->getMemberMessage('change', $team_member));
            return $result;
        } elseif ($team_member instanceof TeamMember) {
            $_POST['id_position'] = $team_member->getIdPosition();
            $_POST['date_since'] = $team_member->getDateSince();
            $_POST['date_to'] = $team_member->getDateTo();
            $_POST['dress_number'] = $team_member->getDressNumber();
        } else {
            $this->formProcessor->initVars(array('id_position', 'date_since', 'date_to', 'dress_number'));
        }
        return false;
    }

    private function createPlayer($team_member, $person, $position)
    {
        $create = false;
        if ($team_member instanceof TeamMember && !$team_member->isTeamPlayer()) {
            $create = $team_member->createTeamPlayer();
        }
        parent::log('TEAM_MEMBER', "Add player {$person} [{$position}] to {$this->team->__toString()}", $create);
    }

    private function updateDressNumber($team_member, $person, $position)
    {
        $update = false;
        if ($team_member instanceof TeamMember) {
            $update = $team_member->updateDressNumber($_POST['dress_number']);
        }
        parent::log(
            'TEAM_MEMBER',
            "Update dress number of {$person} [{$position}] in {$this->team->__toString()}",
            $update
        );
    }

    public function updateTeam()
    {
        if (parent::isObjectSet() && isset($_POST['update'])) {
            $this->formProcessor->escapeValues();
            $club = isset($_POST['club']) ? $this->entities->Club->findById($_POST['club']) : false;
            $result = $this->team->update(array('name' => $_POST['name'], 'club' => $club));
            $name = isset($_POST['name']) ? $_POST['name'] : '';
            parent::checkAction($result, 'UPDATE', "{$name} ({$this->team->__toString()})");
            parent::setSessionMessage($result, $this->getMessage('change'));
            if ($result && $_POST['name'] != $this->team->__toString()) {
                foreach ($this->entities->Competition->findByTeam($this->team) as $competition) {
                    parent::deleteCacheIfSuccessfulAction(true, $competition->getId());
                }
            }
            return $result;
        } else {
            $_POST['name'] = $this->team->__toString();
            $_POST['club'] = $this->team->getIdClub();
        }
        return false;
    }

    public function deleteMember($team_member)
    {
        if (parent::canBeDeleted() && $team_member instanceof TeamMember) {
            $arr = $team_member->toArray();
            $delete = $team_member->delete();
            parent::setSessionMessage(
                $delete,
                $this->getMemberMessage('delete-success', $team_member),
                $this->getMemberMessage('delete-fail', $team_member)
            );
            parent::log(
                'TEAM_MEMBER',
                "Delete member {$team_member} [{$arr['position']}] from {$this->team->__toString()}",
                $delete
            );
        }
    }

    public function deletePlayer($team_member)
    {
        if (parent::canBeDeleted() && $team_member instanceof TeamMember) {
            if ($team_member->isTeamPlayer()) {
                $arr = $team_member->toArray();
                $delete = $team_member->deleteTeamPlayer();
                parent::setSessionMessage(
                    $delete,
                    $this->getMemberMessage('delete-success', $team_member),
                    $this->getMemberMessage('delete-fail', $team_member)
                );
                parent::log(
                    'TEAM_MEMBER',
                    "Unmark player {$team_member} [{$arr['position']}] from {$this->team->__toString()}",
                    $delete
                );
            } else {
                $this->session->setMessageFail('Member is not player');
            }
        }
    }

    public function deleteTeam()
    {
        if (parent::canBeDeleted()) {
            if ($this->team->canBeDeleted()) {
                $delete = $this->team->delete();
                parent::setSessionMessage(
                    $delete,
                    $this->getMessage('delete-success'),
                    $this->getMessage('delete-fail')
                );
                parent::log('DELETE', $this->team->__toString(), $delete);
            } else {
                $this->session->setMessageFail(SessionMessage::get('delete-team'));
            }
        }
    }

    private function getMessage($name, $team = false)
    {
        $team = $team || !parent::isObjectSet() ? $team : $this->team->__toString();
        return SessionMessage::get($name, array(stmLang('competition', 'team'), $team));
    }

    private function getMemberMessage($name, $member)
    {
        return SessionMessage::get($name, array(stmLang('team', 'person'), $member->__toString()));
    }
}
