<?php
use STM\Competition\CompetitionStatus;
use STM\Match\ScoreType;
?>

    <h2><?php echo stmLang('competition', 'competition'); ?></h2>

    <h3><?php echo stmLang('form', 'required'); ?></h3>
    <label for="name"><?php echo stmLang('competition', 'name'); ?>: </label>
    <input type="text" id="name" name="name" size="40" maxlength="50" value="<?php echo $_POST['name']; ?>" />
    <label for="match_periods"><?php echo stmLang('competition', 'min-match-periods'); ?>: </label>
    <?php \STM\Web\HTML\Forms::select('match_periods', \STM\Utils\Arrays::getNumberArray(1, 10)); ?>
    <label for="competition_type"><?php echo stmLang('competition', 'type'); ?>: </label>
    <?php \STM\Web\HTML\Forms::select('id_competition_type', $competition_type_options); ?>
    <label for="score_type"><?php echo stmLang('competition', 'score-type'); ?>: </label>
    <?php
    $option_array = array(
        'F' => stmLang('constants', 'scoretype', ScoreType::FOOTBALL),
        'T' => stmLang('constants', 'scoretype', ScoreType::TENNIS)
    );
    \STM\Web\HTML\Forms::select('id_score_type', $option_array);
    ?>
    <label for="status"><?php echo stmLang('competition', 'status'); ?>: </label>
    <?php
    $option_array = array(
        'P' => stmLang('constants', 'competitionstatus', CompetitionStatus::PUBLICS),
        'V' => stmLang('constants', 'competitionstatus', CompetitionStatus::VISIBLE),
        'H' => stmLang('constants', 'competitionstatus', CompetitionStatus::HIDDEN)
    );
    \STM\Web\HTML\Forms::select('id_status', $option_array);
    ?>
    <label for="category"><?php echo stmLang('competition', 'category'); ?>: </label>
    <?php \STM\Web\HTML\Forms::select('id_category', \STM\Utils\Arrays::getAssocArray($categories, 'getId', '__toString'), true); ?>

    <h3><?php echo stmLang('form', 'optional'); ?></h3>
    <label for="date_start"><?php echo stmLang('competition', 'date-start'); ?>: </label>
    <?php echo \STM\Web\HTML\Forms::inputDate('date_start'); ?>
    <label for="date_end"><?php echo stmLang('competition', 'date-end'); ?>: </label>
    <?php echo \STM\Web\HTML\Forms::inputDate('date_end');
