    <h2><?php echo stmLang('competition', 'season'); ?></h2>
    <label for="season_periods"><?php echo stmLang('competition', 'periods'); ?>: </label>
    <input type="text" name="season[season_periods]" size="3" maxlength="2" value="<?php echo $_POST['season']['season_periods']; ?>" />
    <label for="win"><?php echo stmLang('competition', 'win'); ?>: </label>
    <input type="text" id="win" name="season[point_win]" size="3" maxlength="2" value="<?php echo $_POST['season']['point_win']; ?>" />
    <label for="win_ot"><?php echo stmLang('competition', 'win-ot'); ?>: </label>
    <input type="text" id="win_ot" name="season[point_win_ot]" size="3" maxlength="2" value="<?php echo $_POST['season']['point_win_ot']; ?>" />
    <label for="draw"><?php echo stmLang('competition', 'draw'); ?>: </label>
    <input type="text" id="draw" name="season[point_draw]" size="3" maxlength="2" value="<?php echo $_POST['season']['point_draw']; ?>" />
    <label for="loss_ot"><?php echo stmLang('competition', 'loss-ot'); ?>: </label>
    <input type="text" id="loss_ot" name="season[point_loss_ot]" size="3" maxlength="2" value="<?php echo $_POST['season']['point_loss_ot']; ?>" />
    <label for="loss"><?php echo stmLang('competition', 'loss'); ?>: </label>
    <input type="text" id="loss" name="season[point_loss]" size="3" maxlength="2" value="<?php echo $_POST['season']['point_loss']; ?>" />
