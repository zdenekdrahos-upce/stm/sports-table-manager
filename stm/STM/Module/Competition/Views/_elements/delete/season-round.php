<?php
$url = '?module=season&action=delete&type=round_matches&c=' . $_GET['c'] . '&r=' . $round->round;
$submit_message = stmLang('competition', 'delete-seasonround');
$question = stmLang('competition', 'delete-seasonround-msg') . $round->round . '?';
include(STM_ADMIN_TEMPLATE_ROOT . 'form/delete-form.php');
