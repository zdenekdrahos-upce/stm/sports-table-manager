<?php
$url = '?module=competition&action=delete&type=team&c=' . $_GET['c'] . '&t=' . $team->getId();
$submit_message = stmLang('competition', 'delete-team');
$question = stmLang('competition', 'delete-team-msg') . $team;
include(STM_ADMIN_TEMPLATE_ROOT . 'form/delete-form.php');
