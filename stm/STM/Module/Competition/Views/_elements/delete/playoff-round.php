<?php
$url = '?module=playoff&action=delete&type=round&c=' . $_GET['c'];
$submit_message = stmLang('competition', 'delete-playoffround');
$question = stmLang('competition', 'delete-playoffround-msg');
include(STM_ADMIN_TEMPLATE_ROOT . 'form/delete-form.php');
