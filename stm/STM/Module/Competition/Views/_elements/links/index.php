    <h2><?php echo stmLang('left-menu-header'); ?></h2>
    <ul>
        <li><a href="index.php?module=competition"><?php echo stmLang('header', 'competition', 'index'); ?></a></li>
        <li><a href="index.php?module=competition&action=create&type=competition"><?php echo stmLang('header', 'competition', 'create', 'competition'); ?></a></li>
        <li><a href="index.php?module=season&action=create&type=season"><?php echo stmLang('header', 'season', 'create', 'season'); ?></a></li>
        <li><a href="index.php?module=playoff&action=create&type=playoff"><?php echo stmLang('header', 'playoff', 'create', 'playoff'); ?></a></li>
    </ul>
