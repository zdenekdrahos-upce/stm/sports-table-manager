    <h2><a href="?module=competition&action=read&type=competition&c=<?php echo $_GET['c']; ?>"><?php echo $competition_name; ?></a></h2>
    <ul>
        <?php if ($id_category): ?>
        <li class="header"><?php echo stmLang('competition', 'category'); ?></li>
        <li><a href="<?php echo buildUrl(array('module' => 'category', 'action' => 'read', 'type' => 'category', 'c' => null, 'cat' => $id_category, 't' => null, 'r' => null)); ?>"><?php echo $category; ?></a></li>
        <?php endif; ?>
        <li class="header"><?php echo stmLang('competition', 'competition'); ?></li>
        <li><a href="<?php echo buildUrl(array('module' => 'competition', 'action' => 'read', 'type' => 'statistics', 's' => null, 't' => null, 'r' => null)); ?>"><?php echo stmLang('header', 'competition', 'read', 'statistics'); ?></a></li>
        <li><a href="<?php echo buildUrl(array('module' => 'statistics', 'action' => 'read', 'type' => 'competition_players', 's' => null, 't' => null, 'r' => null)); ?>"><?php echo stmLang('header', 'statistics', 'read', 'competition_players'); ?></a></li>
        <li><a href="<?php echo buildUrl(array('module' => 'competition', 'action' => 'read', 'type' => 'teams', 's' => null, 't' => null, 'r' => null)); ?>"><?php echo stmLang('header', 'competition', 'read', 'teams'); ?></a></li>
        <?php if (!$is_playoff): ?>
        <li><a href="<?php echo buildUrl(array('module' => 'competition', 'action' => 'read', 'type' => 'matches', 's' => null, 't' => null, 'r' => null)); ?>"><?php echo stmLang('header', 'competition', 'read', 'matches'); ?></a></li>
        <li><a href="<?php echo buildUrl(array('module' => 'match', 'action' => 'create', 'type' => 'match', 's' => null, 't' => null, 'r' => null)); ?>"><?php echo stmLang('header', 'match', 'create', 'match'); ?></a></li>
        <li><?php include(STM_MODULES_ROOT . 'Competition/Views/_elements/delete/matches.php'); ?></li>
        <?php endif; ?>
        <li><a href="<?php echo buildUrl(array('module' => 'competition', 'action' => 'update', 'type' => 'competition', 't' => null, 'r' => null)); ?>"><?php echo stmLang('header', 'competition', 'update', 'competition'); ?></a></li>
        <li><a href="<?php echo buildUrl(array('module' => 'competition', 'action' => 'read', 'type' => 'advanced_actions', 't' => null, 'r' => null)); ?>"><?php echo stmLang('header', 'competition', 'read', 'advanced_actions'); ?></a></li>
        <li><?php include(STM_MODULES_ROOT . 'Competition/Views/_elements/delete/competition.php'); ?></li>

        <?php if ($is_season): ?>
        <li class="header"><?php echo stmLang('competition', 'season'); ?></li>
        <li><a href="<?php echo buildUrl(array('module' => 'season', 'action' => 'read', 'type' => 'tables', 't' => null, 'r' => null)); ?>"><?php echo stmLang('header', 'season', 'read', 'tables'); ?></a></li>
        <li><a href="<?php echo buildUrl(array('module' => 'season', 'action' => 'read', 'type' => 'cross_table', 't' => null, 'r' => null)); ?>"><?php echo stmLang('header', 'season', 'read', 'cross_table'); ?></a></li>
        <li><a href="<?php echo buildUrl(array('module' => 'season', 'action' => 'read', 'type' => 'rounds', 't' => null, 'r' => null)); ?>"><?php echo stmLang('header', 'season', 'read', 'rounds'); ?></a></li>
        <li><a href="<?php echo buildUrl(array('module' => 'season', 'action' => 'create', 'type' => 'schedule', 't' => null, 'r' => null)); ?>"><?php echo stmLang('header', 'season', 'create', 'schedule'); ?></a></li>
        <li><a href="<?php echo buildUrl(array('module' => 'season', 'action' => 'update', 'type' => 'season', 't' => null, 'r' => null)); ?>"><?php echo stmLang('header', 'season', 'update', 'season'); ?></a></li>
        <?php endif; ?>

        <?php if ($is_playoff): ?>
        <li class="header"><?php echo stmLang('competition', 'playoff'); ?></li>
        <li><a href="<?php echo buildUrl(array('module' => 'playoff', 'action' => 'read', 'type' => 'tree', 's' => null, 't' => null, 'r' => null)); ?>"><?php echo stmLang('header', 'playoff', 'read', 'tree'); ?></a></li>
        <li><a href="<?php echo buildUrl(array('module' => 'playoff', 'action' => 'read', 'type' => 'series', 's' => null, 't' => null, 'r' => null)); ?>"><?php echo stmLang('header', 'playoff', 'read', 'series'); ?></a></li>
        <li><a href="<?php echo buildUrl(array('module' => 'playoff', 'action' => 'create', 'type' => 'round', 's' => null, 't' => null, 'r' => null)); ?>"><?php echo stmLang('header', 'playoff', 'create', 'round'); ?></a></li>
        <li><a href="<?php echo buildUrl(array('module' => 'playoff', 'action' => 'update', 'type' => 'seeded', 's' => null, 't' => null, 'r' => null)); ?>"><?php echo stmLang('header', 'playoff', 'update', 'seeded'); ?></a></li>
        <li><a href="<?php echo buildUrl(array('module' => 'playoff', 'action' => 'update', 'type' => 'playoff', 's' => null, 't' => null, 'r' => null)); ?>"><?php echo stmLang('header', 'playoff', 'update', 'playoff'); ?></a></li>
        <?php endif; ?>

        <?php if (STM_ALLOW_PREDICTIONS): ?>
        <li class="header"><?php echo stmLang('match', 'predictions'); ?></li>
        <li><a href="<?php echo buildUrl(array('module' => 'prediction', 'action' => 'read', 'type' => 'competition_stats', 't' => null, 'r' => null)); ?>"><?php echo stmLang('header', 'prediction', 'read', 'competition_stats'); ?></a></li>
        <li><a href="<?php echo buildUrl(array('module' => 'prediction', 'action' => 'read', 'type' => 'chart', 't' => null, 'r' => null)); ?>"><?php echo stmLang('header', 'prediction', 'read', 'chart'); ?></a></li>
        <?php endif; ?>
    </ul>
