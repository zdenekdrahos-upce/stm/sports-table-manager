<?php
/**
 * Add teams from other competition
 * $competitions        all competitions from application
 */
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <select name="competition" size="1">
        <?php foreach ($competitions as $competition): ?>
        <option value="<?php echo $competition->getId(); ?>"><?php echo $competition; ?></option>
        <?php endforeach; ?>
    </select>
    <input type="submit" name="add" value="<?php echo stmLang('header', 'competition', 'create', 'teams'); ?>" />
</form>
