<?php
/**
 * Create competition summary
 * $is_playoff
 * $max_field_count
 */
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">

    <table>
        <tr>
            <th colspan="2"><?php echo stmLang('competition', 'summary', 'type'); ?></th>
            <th class="left"><?php echo stmLang('competition', 'summary', 'optional-settings'); ?></th>
        </tr>
        <tr>
            <td class="checkbox"><input id="schedule" type="checkbox" name="schedule[checked]" /></td>
            <td><label for="schedule"><?php echo stmLang('competition', 'summary', 'schedule', 'header'); ?></label></td>
            <td class="left"><?php echo \STM\Web\HTML\Forms::select('schedule[fields_count]', \STM\Utils\Arrays::getNumberArray(2, $max_field_count), true, 'inline'); ?> <?php echo stmLang('competition', 'summary', 'schedule', 'fields'); ?></td>
        </tr>
        <tr>
            <td class="checkbox"><input id="teams" type="checkbox" name="teams[checked]" /></td>
            <td><label for="teams"><?php echo stmLang('header', 'competition', 'read', 'teams'); ?></label></td>
            <td class="left">
                <label for="teams_count"><input class="inline" id="teams_count" type="checkbox" name="teams[count]" checked="checked" /> <?php echo stmLang('competition', 'summary', 'teams', 'count'); ?></label>
                <label for="teams_list"><input class="inline" id="teams_list" type="checkbox" name="teams[list]" checked="checked" /> <?php echo stmLang('competition', 'summary', 'teams', 'list'); ?></label>
            </td>
        </tr>
        <?php if (!$is_playoff): ?>
        <tr>
            <td class="checkbox"><input id="winners" type="checkbox" name="winners[checked]" /></td>
            <td><label for="winners"><?php echo stmLang('competition', 'summary', 'winners', 'header'); ?></label></td>
            <td class="left">
                <label for="winners_top_3"><input class="inline" id="winners_top_3" type="checkbox" name="winners[top_3]" checked="checked" /> <?php echo stmLang('competition', 'summary', 'winners', 'top-3'); ?></label>
                <label for="winners_players"><input class="inline" id="winners_players" type="checkbox" name="winners[players]" /> <?php echo stmLang('competition', 'summary', 'winners', 'top-players'); ?></label>
            </td>
        </tr>
        <?php endif; ?>
        <tr>
            <td class="checkbox"><input id="statistics" type="checkbox" name="statistics[checked]" /></td>
            <td><label for="statistics"><?php echo stmLang('header', 'competition', 'read', 'statistics'); ?></label></td>
            <td class="left">
                <label for="statistics_matches_all"><input class="inline" id="statistics_matches_all" type="checkbox" name="statistics[matches_count_all]" checked="checked" /> <?php echo stmLang('competition', 'stats', 'all-matches'); ?></label>
                <label for="statistics_matches_played"><input class="inline" id="statistics_matches_played" type="checkbox" name="statistics[matches_count_played]" /> <?php echo stmLang('competition', 'stats', 'played-matches'); ?></label>
                <label for="statistics_matches_upcoming"><input class="inline" id="statistics_matches_upcoming" type="checkbox" name="statistics[matches_count_upcoming]" /> <?php echo stmLang('competition', 'stats', 'upcoming-matches'); ?></label>
                <label for="statistics_goals_count"><input class="inline" id="statistics_goals_count" type="checkbox" name="statistics[goals_count]" /> <?php echo stmLang('competition', 'summary', 'statistics', 'goals-count'); ?></label>
                <label for="statistics_goals_average"><input class="inline" id="statistics_goals_average" type="checkbox" name="statistics[goals_average]" /> <?php echo stmLang('competition', 'summary', 'statistics', 'goals-average'); ?></label>
            </td>
        </tr>
        <tr>
            <td class="checkbox"><input id="action_players" type="checkbox" name="players_actions[checked]" /></td>
            <td><label for="action_players"><?php echo stmLang('header', 'match', 'read', 'action_player'); ?></label></td>
            <td class="left">
                <label for="action_players_top_5"><input class="inline" id="action_players_top_5" type="checkbox" name="players_actions[top_5]" /> <?php echo stmLang('competition', 'summary', 'player-actions', 'top-5'); ?></label>
                <label for="action_players_teams"><input class="inline" id="action_players_teams" type="checkbox" name="players_actions[teams]" /> <?php echo stmLang('competition', 'summary', 'player-actions', 'teams'); ?></label>
                <label for="action_players_full_list"><input class="inline" id="action_players_full_list" type="checkbox" name="players_actions[full_list]" /> <?php echo stmLang('competition', 'summary', 'player-actions', 'full-list'); ?></label>
            </td>
        </tr>
        <?php if (!$is_playoff): ?>
        <tr>
            <td class="checkbox"><input id="tables" type="checkbox" name="tables[checked]" /></td>
            <td><label for="tables"><?php echo stmLang('competition', 'summary', 'tables'); ?></label></td>
            <td class="left">
                <label for="tables_full"><input class="inline" id="tables_full" type="checkbox" name="tables[full]" checked="checked" /> <?php echo stmLang('competition', 'table', 'full'); ?></label>
                <label for="tables_home"><input class="inline" id="tables_home" type="checkbox" name="tables[home]" /> <?php echo stmLang('competition', 'table', 'home');; ?></label>
                <label for="table_away"><input class="inline" id="table_away" type="checkbox" name="tables[away]" /> <?php echo stmLang('competition', 'table', 'away');; ?></label>
                <label for="tables_cross"><input class="inline" id="tables_cross" type="checkbox" name="tables[cross]" checked="checked" /> <?php echo stmLang('header', 'season', 'read', 'cross_table'); ?></label>
            </td>
        </tr>
        <?php endif; ?>
        <tr>
            <td class="checkbox"><input id="matches" type="checkbox" name="matches[checked]" /></td>
            <td><label for="matches"><?php echo stmLang('competition', 'summary', 'matches', 'header'); ?></label></td>
            <td class="left">
                <label for="matches_players_actions"><input class="inline" id="matches_players_actions" type="checkbox" name="matches[players_actions]" checked="checked" /> <?php echo stmLang('header', 'match', 'read', 'action_player'); ?></label>
                <label for="matches_periods"><input class="inline" id="matches_periods" type="checkbox" name="matches[periods]" /> <?php echo stmLang('match', 'periods'); ?></label>
            </td>
        </tr>
    </table>

    <input type="submit" name="create" value="<?php echo stmLang('form', 'create'); ?>" />

</form>
