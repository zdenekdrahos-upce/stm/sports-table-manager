<?php
/**
 * Create new competition
 */
use STM\Competition\CompetitionType;
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <?php
    $competition_type_options = array('X' => stmLang('constants', 'competitiontype', CompetitionType::COMPETITION));
    include(STM_MODULES_ROOT . 'Competition/Views/_elements/form-parts/competition.php');
    ?>

    <input type="submit" name="create" value="<?php echo stmLang('form', 'create'); ?>" />

</form>
