<?php
/**
 * Generate first playoff round
 * $round_number        number of generated round
 * $teams               teams from competition
 * $generator           \STM\Libs\Generators\Playoff\PlayoffGenerator
 */
?>

<h2><?php echo stmLang('competition', 'round', 'save'); ?></h2>
<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <input type="submit" name="generate" value="<?php echo stmLang('competition', 'round', 'save'); ?>" />
</form>
<hr />

<h2><?php echo stmLang('competition', 'round', 'generate-n') . $round_number; ?></h2>
<ul>
    <?php foreach ($generator as $match): ?>
    <li><?php echo $match['home'] . ' <strong>vs</strong> ' . $match['away']; ?></li>
    <?php endforeach; ?>
</ul>
<hr />

<h2><?php echo stmLang('competition', 'round', 'teams-seeded'); ?></h2>
<ul class="no-bullet">
    <?php foreach ($teams as $team): ?>
    <li><strong><?php echo $team->getSeeded() ? $team->getSeeded() : '?'; ?></strong>. <?php echo $team->getTeam(); ?></li>
    <?php endforeach; ?>
</ul>
