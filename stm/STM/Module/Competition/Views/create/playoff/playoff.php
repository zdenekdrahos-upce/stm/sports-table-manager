<?php
/**
 * Create new playoff
 */
use STM\Competition\CompetitionType;
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <?php
    $competition_type_options =  array('P' => stmLang('constants', 'competitiontype', CompetitionType::PLAYOFF));
    include(STM_MODULES_ROOT . 'Competition/Views/_elements/form-parts/competition.php');
    include(STM_MODULES_ROOT . 'Competition/Views/_elements/form-parts/playoff.php');
    ?>

    <input type="submit" name="create" value="<?php echo stmLang('form', 'create'); ?>" />

</form>
