<?php
/**
 * Generate playoff round
 * $playoff
 * $round_number        number of generated round
 * $previous_round      PlayoffSerie from previous round
 * $generator           \STM\Libs\Generators\Playoff\PlayoffGenerator generated round
 */
?>

<h2><?php echo stmLang('competition', 'round', 'save'); ?></h2>
<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <input type="submit" name="generate" value="<?php echo stmLang('competition', 'round', 'save'); ?>" />
</form>
<hr />

<h2><?php echo stmLang('competition', 'round', 'generate-n') . $round_number; ?></h2>
<ul>
    <?php foreach ($generator as $match): ?>
    <li>
        <?php echo $match['home']->team->isFinished($playoff) ? $match['home']->team->getNameWinner() : $match['home']; ?> (<?php echo $match['home']->team->getSeeded(); ?>)
        <strong>vs</strong>
        <?php echo $match['away']->team->isFinished($playoff) ? $match['away']->team->getNameWinner() : $match['away']; ?> (<?php echo $match['away']->team->getSeeded(); ?>)
    </li>
    <?php endforeach; ?>
</ul>
<hr />

<h2><?php echo stmLang('competition', 'round', 'previous'); ?></h2>
<table class="matches">
    <tr>
        <th><?php echo stmLang('competition', 'round', 'seeded'); ?></th>
        <th><?php echo stmLang('match', 'home'); ?></th>
        <th><?php echo stmLang('match', 'away'); ?></th>
        <th><?php echo stmLang('match', 'score'); ?></th>
    </tr>
    <?php foreach ($previous_round as $serie): extract($serie->toArray()) ?>
    <tr>
        <td><?php echo $seeded; ?></td>
        <td><?php echo $team_1 . ' (' . $playoff->getSeededOfTeam($id_team_1) . ')'; ?></td>
        <td><?php echo $team_2 . ' (' . $playoff->getSeededOfTeam($id_team_2) . ')'; ?></td>
        <td><?php echo $wins_team_1 . ':' . $wins_team_2; ?></td>
    </tr>
    <?php endforeach; ?>
</table>
<hr />

<h2><?php echo stmLang('competition', 'round', 'change-seeded'); ?></h2>
<form method="post" action="<?php echo buildUrl(array('action' => 'update', 'type' => 'series_seeded')); ?>">
    <input type="radio" name="type" value="fixed" checked="checked" /> <?php echo stmLang('competition', 'round', 'seeded-fixed'); ?><br />
    <input type="radio" name="type" value="variable" /> <?php echo stmLang('competition', 'round', 'seeded-variable'); ?><br />

    <input type="submit" name="update" value="<?php echo stmLang('competition', 'round', 'change-seeded'); ?>" />
</form>
