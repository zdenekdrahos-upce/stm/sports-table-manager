<?php
/**
 * Import team players to competition matches
 */
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">

    <label for="match_type" class="inline"><?php echo stmLang('match', 'filter', 'match-selection'); ?>:</label>
    <?php
    $options = array('A' => 'all-matches', 'P' => 'played-matches', 'U' => 'upcoming-matches');
    foreach ($options as $key => $value) {
        $options[$key] = stmLang('match', 'filter', $value);
    }
    \STM\Web\HTML\Forms::select('match_type', $options, false);
    ?>
    <label for="id_position"><?php echo stmLang('person', 'position'); ?>: </label>
    <td><?php include(STM_MODULES_ROOT . 'Match/Views/_elements/form-parts/player/position.php'); ?></td>

    <input type="submit" name="create" value="<?php echo stmLang('form', 'create'); ?>" />
</form>
