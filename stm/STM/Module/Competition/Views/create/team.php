<?php
/**
 * Add (create) team to competition
 */
$col_count = 3;
$actual_col_count = 0;
?>

<?php if ($teams_count < count($all_teams)): ?>
    <form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
        <table>
        <?php foreach ($all_teams as $team): ?>
        <?php if(!$competition->isTeamFromCompetition($team)): ?>
            <?php if($actual_col_count++ % $col_count == 0): ?><tr><?php endif; ?>
                <td class="checkbox"><input id="<?php echo $team->getId(); ?>" class="inline" type="checkbox" name="teams[]" value="<?php echo $team->getId(); ?>" /></td>
                <td class="left"><label class="inline" for="<?php echo $team->getId(); ?>"><?php echo $team; ?></label></td>
            <?php if($actual_col_count % $col_count == 0 || $team == end($all_teams)): ?></tr><?php endif; ?>
        <?php endif; ?>
        <?php endforeach; ?>
        </table>
        <input type="submit" name="add" value="<?php echo stmLang('header', 'competition', 'create', 'team'); ?>" />
    </form>
<?php else: ?>
    <p><?php echo stmLang('no-content'); ?></p>
<?php endif; ?>

<hr />
<a href="?module=team&action=create&type=team"><?php echo stmLang('header', 'team', 'create', 'team'); ?></a>
