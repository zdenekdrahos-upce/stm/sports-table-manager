<?php
/**
 * Create new season
 */
use STM\Competition\CompetitionType;
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <?php
    $competition_type_options =  array('S' => stmLang('constants', 'competitiontype', CompetitionType::SEASON));
    include(STM_MODULES_ROOT . 'Competition/Views/_elements/form-parts/competition.php');
    include(STM_MODULES_ROOT . 'Competition/Views/_elements/form-parts/season.php');
    ?>

    <input type="submit" name="create" value="<?php echo stmLang('form', 'create'); ?>" />

</form>
