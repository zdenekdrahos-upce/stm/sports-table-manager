<?php
/**
 * Generate season schedule
 * $team_count          number of teams in season
 * $season_periods      number of season periods
 * $generator           \STM\Libs\Generators\Season\SeasonGenerator
 * $generated_matches   generated matches, array[round][match] = array('h' => id_home, 'a' => id_away)
 * $existing_matches
 */
?>

<h2><?php echo stmLang('competition', 'season'); ?></h2>
<ul>
    <li><?php echo stmLang('competition', 'stats', 'team-count'); ?>: <strong><?php echo $team_count; ?></strong></li>
    <li><?php echo stmLang('competition', 'periods'); ?>: <strong><?php echo $season_periods; ?></strong></li>
    <li><a href=""><strong><?php echo stmLang('competition', 'schedule', 'reload'); ?></strong></a> (<?php echo stmLang('competition', 'schedule', 'reload-help'); ?>)</li>
    <?php if ($existing_matches): ?>
    <li><?php echo stmLang('competition', 'schedule', 'regenerate'); ?></li>
    <?php endif; ?>
</ul>
<hr />
<h2><?php echo stmLang('competition', 'schedule', 'save'); ?></h2>
<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <?php for($i = 1; $i <= $season_periods; $i++): ?>
    <label for="date<?php echo $i; ?>"><?php echo stmLang('competition', 'schedule', 'start-date'); ?><?php echo $i; ?></label>
    <input type="text" id="date<?php echo $i; ?>" name="dates[]" size="40" maxlength="40" value="<?php echo $_POST['dates'][$i - 1]; ?>" />
    <?php endfor; ?>
    <input type="hidden" name="matches" value='<?php echo json_encode($generated_matches); ?>' />

    <input type="submit" name="generate" value="<?php echo stmLang('competition', 'schedule', 'save'); ?>" />
</form>

<hr />
<h2><?php echo stmLang('competition', 'schedule', 'preview'); ?></h2>

<table class="matches" style="text-align: left;width:auto">
    <?php
    $round_name = stmLang('match', 'season-round');
    foreach ($generator as $number => $round) {
        echo "<tr><th>{$number}.{$round_name}</th></tr>";
        foreach ($round as $match) {
            echo '<tr><td>' . $match->home . ' : ' . $match->away . '</td></tr>';
        }
    }
    ?>
</table>
