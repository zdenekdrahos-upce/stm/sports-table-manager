<?php
/**
 * Advanced competition managment
 * $is_season
 */
?>

<ul>
    <li><a href="<?php echo buildUrl(array('action' => 'create', 'type' => 'players')); ?>"><?php echo stmLang('header', 'competition', 'create', 'players'); ?></a></li>
    <?php if ($is_season): ?>
    <li><a href="<?php echo buildUrl(array('module' => 'season', 'action' => 'update', 'type' => 'round_date')); ?>"><?php echo stmLang('header', 'season', 'update', 'round_date'); ?></a></li>
    <li><a href="<?php echo buildUrl(array('module' => 'season', 'action' => 'update', 'type' => 'table')); ?>"><?php echo stmLang('header', 'season', 'update', 'table'); ?></a></li>
    <li><a href="<?php echo buildUrl(array('module' => 'import', 'action' => 'update', 'type' => 'table')); ?>"><?php echo stmLang('header', 'import', 'update', 'table'); ?></a></li>
    <?php endif; ?>
    <li><a href="<?php echo buildUrl(array('action' => 'create', 'type' => 'summary')); ?>"><?php echo stmLang('header', 'competition', 'create', 'summary'); ?></a></li>
</ul>
