<?php
/**
 * Competition info page
 * $competition
 * $name
 * $type
 * $competition_data
 * $is_season
 * $is_playoff
 * $last_matches
 * $next_matches
 */
?>

<ul>
    <li><?php echo stmLang('name'); ?>: <strong><?php echo $name; ?></strong></li>
    <li><?php echo stmLang('competition', 'type'); ?>: <strong><?php echo $type; ?></strong></li>
</ul>



<ul class="vertical">
    <li><a href="javascript:toggleOneDiv('next-matches', 'toogle');"><?php echo stmLang('competition', 'sections', 'next-matches'); ?></a></li>
    <li><a href="javascript:toggleOneDiv('last-matches', 'toogle');"><?php echo stmLang('competition', 'sections', 'last-matches'); ?></a></li>
    <li><a href="javascript:toggleOneDiv('detail', 'toogle');"><?php echo stmLang('competition', 'sections', 'detail'); ?></a></li>
</ul>

<div class="toogle" name="next-matches" id="displayed">
<?php include(__DIR__ . '/detail/next_matches.php'); ?>
</div>

<div class="toogle" name="last-matches">
<?php include(__DIR__ . '/detail/last_matches.php'); ?>
</div>

<div class="toogle" name="detail">
<?php include(__DIR__ . '/detail/basic_details.php'); ?>
</div>
