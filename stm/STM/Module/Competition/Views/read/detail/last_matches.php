<?php
/**
 * Last X competition matches
 */
?>

<h2><?php echo stmLang('competition', 'sections', 'last-matches'); ?></h2>
<table>
    <?php if ($last_matches): ?>
        <tr>
            <th><?php echo stmLang('match', 'date'); ?></th>
            <?php if($is_season): ?>
            <th><?php echo stmLang('match', 'season-round'); ?></th>
            <?php endif; ?>
            <th><?php echo stmLang('competition', 'match'); ?></th>
            <th><?php echo stmLang('match', 'score'); ?></th>
            <th><?php echo stmLang('left-menu-header'); ?></th>
        </tr>
        <?php foreach ($last_matches as $match):
            $is_home_your_team = $competition->isYourTeam($match->getIdTeamHome());
            $is_away_your_team = $competition->isYourTeam($match->getIdTeamAway());
            extract($match->toArray());
        ?>
            <tr<?php echo $is_home_your_team || $is_away_your_team ? ' class="user"' : ''; ?>>
                <td><?php echo \STM\Utils\Dates::convertDatetimeToString($date, '-'); ?></td>
                <?php if($is_season): ?>
                <td><?php echo $round; ?></td>
                <?php endif; ?>
                <td><?php echo $team_home . ' : ' . $team_away; ?></td>
                <td><?php echo $match->hasScore() ? ($score_home . ' : ' . $score_away) : '-:-'; ?></td>
                <td>
                    <?php if ($is_home_your_team || $is_away_your_team): $lineup_type = $is_home_your_team ? 'lineup_home' : 'lineup_away' ?>
                    <a href="index.php?module=match&action=create&type=<?php echo "{$lineup_type}&m={$match->getId()}"; ?>"><?php echo stmLang('header', 'match', 'create', $lineup_type); ?></a>
                    <br />
                    <?php endif; ?>
                    <a href="index.php?module=match&action=read&type=action_player&m=<?php echo $match->getId(); ?>"><?php echo stmLang('header', 'match', 'read', 'action_player'); ?></a>
                </td>
            </tr>
        <?php endforeach; ?>
    <?php else: ?>
        <tr>
            <p><?php echo stmLang('no-content'); ?></p>
        </tr>
    <?php endif; ?>
</table>
