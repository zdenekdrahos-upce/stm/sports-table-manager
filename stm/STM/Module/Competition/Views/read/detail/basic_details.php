<?php
/**
 * Basic parameters of the competition
 */
use STM\StmDatetime;
?>

<h2><?php echo stmLang('competition', 'sections', 'detail'); ?></h2>
<ul>
    <?php
    foreach ($competition_data as $name => $value):
        $value = $value instanceof StmDatetime ? \STM\Utils\Dates::convertDatetimeToString($value) : $value
    ?>
        <li><?php echo stmLang('competition', $name) . ": <strong>{$value}</strong>"; ?></li>
    <?php endforeach; ?>
</ul>
