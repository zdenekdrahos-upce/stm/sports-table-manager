<?php
/**
 * Upcoming X competition matches
 */
$rounds = array()
?>

<h2><?php echo stmLang('competition', 'sections', 'next-matches'); ?></h2>
<table>
    <?php if ($next_matches): ?>
        <tr>
            <th><?php echo stmLang('match', 'date'); ?></th>
            <?php if($is_season): ?>
            <th><?php echo stmLang('match', 'season-round'); ?></th>
            <?php endif; ?>
            <th><?php echo stmLang('competition', 'match'); ?></th>
            <th><?php echo stmLang('left-menu-header'); ?></th>
        </tr>
        <?php foreach ($next_matches as $match):
            $is_home_your_team = $competition->isYourTeam($match->getIdTeamHome());
            $is_away_your_team = $competition->isYourTeam($match->getIdTeamAway());
            extract($match->toArray());
        ?>
            <tr<?php echo $is_home_your_team || $is_away_your_team ? ' class="user"' : ''; ?>>
                <td><?php echo \STM\Utils\Dates::convertDatetimeToString($date, '-'); ?></td>
                <?php if($is_season): ?>
                <td><?php echo $round; $rounds[$round] = 1; ?></td>
                <?php endif; ?>
                <td><?php echo $team_home . ' : ' . $team_away; ?></td>
                <td>
                    <a href="index.php?module=match&action=create&type=result&m=<?php echo $match->getId(); ?>"><?php echo stmLang('header', 'match', 'create', 'result'); ?></a>
                    <?php if (STM_ALLOW_PREDICTIONS): ?>
                    <br />
                    <a href="index.php?module=prediction&action=create&type=prediction&m=<?php echo $match->getId(); ?>"><?php echo stmLang('match', 'new-prediction'); ?></a>
                    <?php endif; ?>
                </td>
            </tr>
        <?php endforeach; ?>
    <?php else: ?>
        <tr>
            <p><?php echo stmLang('no-content'); ?></p>
        </tr>
    <?php endif; ?>
</table>

<?php
if ($rounds) {
    echo '<hr /><ul>';
    foreach(array_keys($rounds) as $round) {
        echo '<li>';
        echo '<a href="?module=match&action=create&type=round_results&c=' . $competition->getId() . '&r=' . $round . '">';
        echo stmLang('competition', 'tree', 'round-n') . $round . ': ';
        echo stmLang('header', 'match', 'create', 'round_results');
        echo '</a>';
        echo '</li>';
    }
    echo '</ul>';
}
