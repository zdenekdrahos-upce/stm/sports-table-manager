<?php
/**
 * Competition statistics
 * $teams_count
 * $all_matches_count
 * $played_matches_count
 * $upcoming_matches_count
 * $season_matches_count        only if competition is season
 * $match_percentage            only if competition is season
 * $played_rounds               only if competition is playoff
 * $total_rounds                only if competition is playoff
 * $players_counts
 */
?>

<ul>
    <li><?php echo stmLang('competition', 'stats', 'team-count'); ?> = <strong><?php echo $teams_count; ?></strong></li>
    <li><?php echo stmLang('competition', 'stats', 'all-matches'); ?> = <strong><?php echo $all_matches_count; ?></strong></li>
    <li><?php echo stmLang('competition', 'stats', 'played-matches'); ?> = <strong><?php echo $played_matches_count; ?></strong></li>
    <li><?php echo stmLang('competition', 'stats', 'upcoming-matches'); ?> = <strong><?php echo $upcoming_matches_count; ?></strong></li>
</ul>

<?php if (isset($season_matches_count)): ?>
<h2><?php echo stmLang('competition', 'season'); ?></h2>
<ul>
    <li><?php echo stmLang('competition', 'stats', 'total-matches'); ?> = <strong><?php echo $season_matches_count; ?></strong></li>
    <li><?php echo stmLang('competition', 'stats', 'percentage-matches'); ?> = <strong><?php echo $match_percentage; ?>%</strong></li>
</ul>
<?php endif; ?>

<?php if (isset($played_rounds)): ?>
<h2><?php echo stmLang('competition', 'playoff'); ?></h2>
<ul>
    <li><?php echo stmLang('competition', 'stats', 'played-rounds'); ?> = <strong><?php echo $played_rounds; ?></strong></li>
    <li><?php echo stmLang('competition', 'stats', 'total-rounds'); ?> = <strong><?php echo $total_rounds; ?></strong></li>
</ul>
<?php endif; ?>

<h2><?php echo stmLang('action', 'count_in_players'); ?></h2>
<ul>
    <li><?php echo stmLang('competition', 'stats', 'players-counts'); ?> = <strong><?php echo array_sum($players_counts); ?></strong></li>
</ul>
<?php if ($players_counts): ?>
<table>
    <tr>
        <th><?php echo stmLang('competition', 'team'); ?></th>
        <th><?php echo stmLang('competition', 'stats', 'players-count'); ?></th>
    </tr>
    <?php foreach($players_counts as $team => $count): ?>
    <tr>
        <td><?php echo $team; ?></td>
        <td><strong><?php echo $count; ?></strong></td>
    </tr>
    <?php endforeach; ?>
</table>
<?php endif;
