<?php
/**
 * Season cross table
 * $cross_table
 */
?>

<table class="crossTable" cellspacing="0">
    <tr>
        <td class="no">&nbsp;</td>
        <?php foreach (array_keys($cross_table) as $team): ?>
        <th><?php echo $team; ?></th>
        <?php endforeach; ?>
        <th class="results"><?php echo stmLang('competition', 'table', 'points'); ?></th>
        <th class="results"><?php echo stmLang('match', 'score'); ?></th>
        <th class="results"><?php echo stmLang('competition', 'table', 'position'); ?></th>
    </tr>
    <?php $position = 1; foreach ($cross_table as $team => $row): ?>
        <tr>
            <th><?php echo $team; ?></th>
            <?php foreach ($row->opponents as $opponent => $scores): ?>
                <td<?php echo $team == $opponent ? ' class="no"' : ''; ?>><?php echo empty($scores) ? '&nbsp;' : implode($scores, ', '); ?></td>
            <?php endforeach; ?>
            <td class="results"><?php echo $row->tableRow->points; ?></td>
            <td class="results"><?php echo $row->tableRow->goalFor . ':' . $row->tableRow->goalAgainst; ?></td>
            <td class="results"><strong><?php echo $position++; ?>.</strong></td>
        </tr>
<?php endforeach; ?>
</table>
