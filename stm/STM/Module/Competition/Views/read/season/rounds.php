<?php
/**
 * Season rounds
 */
?>

<?php if (empty($rounds)): ?>
    <p><?php echo stmLang('no-content'); ?></p>
<?php else: ?>

<table>
    <tr>
        <th><?php echo stmLang('match', 'season-round'); ?></th>
        <th><?php echo stmLang('competition', 'rounds', 'match-count'); ?></th>
        <th class="right"><?php echo stmLang('team', 'links'); ?></th>
    </tr>
    <?php foreach($rounds as $round): ?>
    <tr>
        <td><?php echo $round->round; ?></td>
        <td><?php echo "{$round->allMatches} ({$round->playedMatches})"; ?></td>
        <td class="right">
            <?php if ($round->allMatches > $round->playedMatches): ?>
            <a href="<?php echo buildUrl(array('module' => 'match', 'action' => 'create', 'type' => 'round_results', 'r' => $round->round)); ?>"><?php echo stmLang('header', 'match', 'create', 'round_results'); ?></a>,
            <?php endif; ?>
            <?php if ($round->allMatches > 0): ?>
            <?php include(STM_MODULES_ROOT . 'Competition/Views/_elements/delete/season-round.php'); ?>
            <?php endif; ?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>

<?php endif; ?>

<h2><?php echo stmLang('header', 'competition', 'read', 'advanced_actions'); ?></h2>
<ul>
    <li><a href="<?php echo buildUrl(array('module' => 'season', 'action' => 'update', 'type' => 'round_date')); ?>"><?php echo stmLang('header', 'season', 'update', 'round_date'); ?></a></li>
</ul>
