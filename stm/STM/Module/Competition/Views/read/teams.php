<?php
/**
 * List of all teams from competition (links to delete team, mark/unmark your_team, add team)
 * $competition     selected competition
 * $teams           teams from competition
 * $is_season
 */
?>

<?php if (empty($teams)): ?>
    <p><?php echo stmLang('no-content'); ?></p>
<?php else: ?>
    <table>
        <thead>
            <tr>
                <th><?php echo stmLang('competition', 'team'); ?></th>
                <th class="right"><?php echo stmLang('team', 'links'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($teams as $team): $class = $competition->isYourTeam($team->getId()) ? ' class="user"' : ''; ?>
            <tr<?php echo $class; ?>>
                <td><?php echo $team; ?></td>
                <td class="right">
                    <a href="?module=team&action=read&type=team&t=<?php echo $team->getId();?>"><?php echo stmLang('header', 'team', 'read', 'team'); ?></a>,
                    <?php if ($competition->isYourTeam($team->getId())): ?>
                    <a href="<?php echo buildUrl(array('action' => 'delete', 'type' => 'your_team', 't' => $team->getId())); ?>"><?php echo stmLang('competition', 'teams', 'unmark'); ?></a>,
                    <?php else: ?>
                    <a href="<?php echo buildUrl(array('action' => 'create', 'type' => 'your_team', 't' => $team->getId())); ?>"><?php echo stmLang('competition', 'teams', 'mark'); ?></a>,
                    <?php endif; ?>
                    <?php if ($is_season): ?>
                    <a href="<?php echo buildUrl(array('module' => 'season', 'action' => 'update', 'type' => 'extra_points', 't' => $team->getId())); ?>"><?php echo stmLang('header', 'season', 'update', 'extra_points'); ?></a>,
                    <?php endif; ?>
                    <?php include(STM_MODULES_ROOT . 'Competition/Views/_elements/delete/team.php'); ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>


<hr />
<h3><?php echo stmLang('competition', 'teams', 'how-add'); ?></h3>
<ul class="links">
    <li><a href="<?php echo buildUrl(array('action' => 'create', 'type' => 'team')) ?>"><?php echo stmLang('header', 'competition', 'create', 'team'); ?></a></li>
    <li><a href="<?php echo buildUrl(array('action' => 'create', 'type' => 'teams')) ?>"><?php echo stmLang('header', 'competition', 'create', 'teams'); ?></a></li>
</ul>
