<?php
/**
 * List of all competitions + links to delete competition/matches
 * $competitions        array with all competitions and seasons
 */
use STM\Competition\CompetitionType;
?>

<?php
$fields = array(
    't' => array(
        'name' => stmLang('competition', 'team'),
        'values' => \STM\Utils\Arrays::getAssocArray($teams, 'getId', '__toString'),
    ),
);
include(STM_ADMIN_TEMPLATE_ROOT . 'form/filtering-form.php');
?>

<?php if (empty($competitions)): ?>
    <p><?php echo stmLang('no-content'); ?></p>
<?php else: ?>
    <?php foreach ($competitions as $category_name => $competitions_from_category): ?>
    <?php if ($category_name): ?>
    <h2><strong><?php echo stmLang('competition', 'category'); ?>:</strong> <a href="index.php?module=category&action=read&type=category&cat=<?php echo $competitions_from_category[0]->getIdCategory(); ?>"><?php echo $category_name; ?></a></h2>
    <?php endif; ?>
    <table>
        <tr>
            <th><?php echo stmLang('competition', 'competition'); ?></th>
            <th class="width20"><?php echo stmLang('competition', 'type'); ?></th>
        </tr>
    <?php foreach ($competitions_from_category as $competition): ?>
        <tr>
            <td><a href="index.php?module=competition&action=read&type=competition&c=<?php echo $competition->getId(); ?>"><?php echo $competition; ?></a></td>
            <td><?php echo stmLang('constants', 'competitiontype', CompetitionType::getCompetitionType($competition->getIdCompetitionType())); ?></td>
        </tr>
    <?php endforeach; ?>
    </table>
    <?php endforeach; ?>
<?php endif;
