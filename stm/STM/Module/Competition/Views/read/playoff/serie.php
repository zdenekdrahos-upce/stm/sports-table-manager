<?php
/* Detail od playoff serie
 * $matches
 * $serie
 */
extract($serie->toArray());
?>

<ul>
    <li><?php echo stmLang('match', 'season-round'); ?>: <strong><?php echo $round; ?></strong></li>
    <li><?php echo stmLang('match', 'teams'); ?>: <strong><?php echo $team_1 . ' : ' . $team_2; ?></strong></li>
    <li><?php echo stmLang('match', 'score'); ?>: <strong><?php echo $wins_team_1 . ' : ' . $wins_team_2; ?></strong></li>
    <li><?php echo stmLang('competition', 'stats', 'played-matches'); ?>: <strong><?php echo count($matches); ?></strong></li>
</ul>

<h2><?php echo stmLang('competition', 'tree', 'matches'); ?></h2>
<?php if($matches): ?>
<table class="matches">
    <tr>
        <th><?php echo stmLang('competition', 'match'); ?></th>
        <th><?php echo stmLang('match', 'score'); ?></th>
    </tr>
    <?php foreach ($matches as $match): extract($match->toArray()); ?>
    <tr>
        <td><a href="index.php?module=match&action=read&type=match&m=<?php echo $match->getId(); ?>"><?php echo $match; ?></a></td>
        <td><?php echo $match->hasScore() ? ($score_home . ' : ' . $score_away) : '-:-'; ?></td>
    </tr>
    <?php endforeach; ?>
</table>
<?php else: ?>
    <p><?php echo stmLang('no-content'); ?></p>
<?php endif; ?>

<hr />
<a href="<?php echo buildUrl(array('module' => 'match', 'action' => 'create', 'type' => 'match')); ?>"><?php echo stmLang('header', 'match', 'create', 'match'); ?></a>
