<?php
/* Read series from playoff
 * $played_rounds
 * $max_rounds
 * $series          series from playoff round
 * $selected_round  round of playoff, series from this round are displayed
 */
?>

<h2><?php echo stmLang('competition', 'tree', 'rounds'); ?></h2>
<ul>
    <?php for($i = 1; $i <= $max_round; $i++): ?>
    <li>
        <?php echo stmLang('competition', 'tree', 'round-n') . $i; ?>:
        <strong>
        <?php if($i < $played_rounds): ?>
        <?php echo stmLang('competition', 'tree', 'closed'); ?>
        <?php elseif($i == $played_rounds): ?>
        <?php echo stmLang('competition', 'tree', 'actual'); ?> |
        <?php include(STM_MODULES_ROOT . 'Competition/Views/_elements/delete/playoff-round.php'); ?>
        <?php else: ?>
        <?php echo stmLang('competition', 'tree', 'not-generated'); ?>
        <?php endif; ?>
        </strong>
    </li>
    <?php endfor; ?>
</ul>

<?php if($played_rounds >= 1): ?>

    <h2><?php echo stmLang('competition', 'tree', 'round-select'); ?></h2>
    <form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
        <table>
            <tr>
                <td><?php \STM\Web\HTML\Forms::select('id_round', \STM\Utils\Arrays::getNumberArray(1, $played_rounds)); ?></td>
                <td><input type="submit" name="create" value="<?php echo stmLang('competition', 'tree', 'display-round'); ?>" /></td>
            </tr>
        </table>
    </form>

    <?php if (isset($selected_round)): ?>
    <h2><?php echo stmLang('competition', 'tree', 'serie-n') . $selected_round; ?></h2>
    <table class="matches">
        <tr>
            <th><?php echo stmLang('match', 'home'); ?></th>
            <th><?php echo stmLang('match', 'away'); ?></th>
            <th><?php echo stmLang('match', 'score'); ?></th>
            <th><?php echo stmLang('competition', 'tree', 'links'); ?></th>
        </tr>
        <?php foreach ($series as $serie): extract($serie->toArray()) ?>
        <tr>
            <td><?php echo $team_1; ?></td>
            <td><?php echo $team_2; ?></td>
            <td><?php echo $wins_team_1 . ':' . $wins_team_2; ?></td>
            <td>
                <a href="<?php echo buildUrl(array('type' => 'serie', 's' => $id_serie)); ?>"><?php echo stmLang('competition', 'tree', 'link-detail'); ?></a> |
                <a href="<?php echo buildUrl(array('module' => 'match', 'action' => 'create', 'type' => 'match', 's' => $id_serie)); ?>"><?php echo stmLang('competition', 'tree', 'link-addmatch'); ?></a>
            </td>
        </tr>
        <?php endforeach; ?>
    </table>
    <?php endif; ?>

<?php else: ?>
    <p><?php echo stmLang('no-content'); ?></p>
<?php endif;
