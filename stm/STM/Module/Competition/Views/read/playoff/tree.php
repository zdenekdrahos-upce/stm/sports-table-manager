<?php
/*
 * Display playoff tree
 * $playoff
 * $rounds      array from PlayoffTreeAdapter::getRounds
 */
use STM\Competition\Playoff\Serie\PlayoffSerie;
?>

<?php if ($playoff->getPlayedRounds() == 0): ?>
<h3><?php echo stmLang('competition', 'tree', 'no-round'); ?></h3>
<?php else: ?>

<?php foreach ($rounds as $round): ?>
<table class="playoff" style="height: <?php echo $playoff->getCountTeams() * 30; ?>px;" >
    <?php foreach ($round as $serie): ?>
    <tr>
        <td>
            <table>
                <?php if ($serie instanceof PlayoffSerie): extract($serie->toArray()); ?>
                <tr>
                    <td class="seeded"><?php echo $playoff->getSeededOfTeam($id_team_1); ?></td>
                    <td<?php echo $serie->isFinished($playoff) && $wins_team_1 > $wins_team_2 ? ' class="winner" ' : ''; ?>><?php echo $team_1; ?></td>
                    <td rowspan="2" class="score"><?php echo $wins_team_1 . ':' . $wins_team_2; ?></td>
                </tr>
                <tr>
                    <td class="seeded"><?php echo $playoff->getSeededOfTeam($id_team_2); ?></td>
                    <td<?php echo $serie->isFinished($playoff) && $wins_team_1 < $wins_team_2 ? ' class="winner" ' : ''; ?>><?php echo $team_2; ?></td>
                </tr>
                <?php else: ?>
                <tr>
                    <td><?php echo $serie; ?></td>
                </tr>
                <?php endif; ?>
            </table>
        </td>
    </tr>
    <?php endforeach; ?>
</table>
<?php endforeach; ?>

<?php endif;
