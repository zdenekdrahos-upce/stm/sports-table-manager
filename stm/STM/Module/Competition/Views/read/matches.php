<?php
/**
 * List of all matches from competition (link to match details)
 * $matches         competition matches
 * $teams
 * $competition
 */
use STM\Competition\Season\Season;
$is_season = $competition instanceof Season;
?>

<?php
$fields = array(
    't' => array(
        'name' => stmLang('competition', 'team'),
        'values' => \STM\Utils\Arrays::getAssocArray($teams, 'getId', '__toString'),
    ),
);
if ($is_season) {
    $fields['r'] = array(
        'name' => stmLang('match', 'season-round'),
        'values' => \STM\Utils\Arrays::getNumberArray(1, $competition->getMaxRound()),
    );
}
include(STM_ADMIN_TEMPLATE_ROOT . 'form/filtering-form.php');
?>

<table>
    <?php if ($matches): ?>
        <tr>
            <?php if($is_season): ?>
            <th><?php echo stmLang('match', 'season-round'); ?></th>
            <?php endif; ?>
            <th><?php echo stmLang('competition', 'match'); ?></th>
            <th><?php echo stmLang('match', 'date'); ?></th>
            <th><?php echo stmLang('match', 'score'); ?></th>
            <th><?php echo stmLang('match', 'periods'); ?></th>
        </tr>
        <?php foreach ($matches as $match):
            $is_home_your_team = $competition->isYourTeam($match->getIdTeamHome());
            $is_away_your_team = $competition->isYourTeam($match->getIdTeamAway());
            extract($match->toArray());
        ?>
            <tr<?php echo $is_home_your_team || $is_away_your_team ? ' class="user"' : ''; ?>>
                <?php if($is_season): ?>
                <td><?php echo $round; ?></td>
                <?php endif; ?>
                <td><a href="index.php?module=match&action=read&type=match&m=<?php echo $match->getId(); ?>"><?php echo $team_home . ' : ' . $team_away; ?></a></td>
                <td><?php echo \STM\Utils\Dates::convertDatetimeToString($date, '-'); ?></td>
                <td><?php echo $match->hasScore() ? ($score_home . ' : ' . $score_away) : '-:-'; ?></td>
                <td><?php echo $match->getPeriods() ? implode(', ', $match->getPeriods()) : ''; ?></td>
            </tr>
        <?php endforeach; ?>
    <?php else: ?>
        <tr>
            <p><?php echo stmLang('no-content'); ?></p>
        </tr>
    <?php endif; ?>
</table>
