<?php
/**
 * Teams from playoff with seeded
 * $teams           teams from playoff, with seeded
 */
?>

<?php if ($teams): ?>
    <form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
        <table class="seeded">
            <tr>
                <th><?php echo stmLang('competition', 'team'); ?></th>
                <th><?php echo stmLang('competition', 'seeded', 'current'); ?></th>
                <th><?php echo stmLang('competition', 'seeded', 'new'); ?></th>
            </tr>
    <?php foreach ($teams as $team): $id = $team->getIdTeam(); ?>
            <tr>
                <td><?php echo $team->getTeam(); ?></td>
                <td><strong><?php echo $team->getSeeded() ? $team->getSeeded() : stmLang('form', 'empty');; ?></strong></td>
                <td><input name="seeded[<?php echo $id; ?>]" type="text" size="3" maxlength="3" value="<?php echo isset($_POST['seeded'][$id]) ? $_POST['seeded'][$id] : $team->getSeeded(); ?>" /></td>
            </tr>
    <?php endforeach; ?>
            <tr>
                <td colspan="2">&nbsp;</td>
                <td><input type="submit" name="update" value="<?php echo stmLang('form', 'change'); ?>" /></td>
            </tr>
        </table>
    </form>
<?php else: ?>
    <p><?php echo stmLang('no-content'); ?></p>
<?php endif;
