<?php
/**
 * Update serie win matches in Playoff
 */
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <?php
    include(STM_MODULES_ROOT . 'Competition/Views/_elements/form-parts/playoff.php');
    ?>

    <input type="submit" name="update" value="<?php echo stmLang('form', 'change'); ?>" />
</form>
