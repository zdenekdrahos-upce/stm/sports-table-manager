<?php
/**
 * Update competition
 */
use STM\Competition\CompetitionType;
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <?php
    $competition_type_options = array($id_competition_type => stmLang('constants', 'competitiontype', CompetitionType::getCompetitionType($id_competition_type)));
    include(STM_MODULES_ROOT . 'Competition/Views/_elements/form-parts/competition.php');
    ?>

    <input type="submit" name="update" value="<?php echo stmLang('form', 'change'); ?>" />

</form>
