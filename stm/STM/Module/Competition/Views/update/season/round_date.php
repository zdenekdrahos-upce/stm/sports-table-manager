<?php
/**
 * Update date of matches in rounds
 * $rounds
 * $teams
 * $dates
 */
?>

<?php if (empty($rounds)): ?>
    <p><?php echo stmLang('no-content'); ?></p>
<?php else: ?>

<?php
$fields = array(
    't' => array(
        'name' => stmLang('competition', 'team'),
        'values' => \STM\Utils\Arrays::getAssocArray($teams, 'getId', '__toString'),
    ),
);
$filter_form_header = stmLang('competition', 'rounds', 'load-team-dates');
include(STM_ADMIN_TEMPLATE_ROOT . 'form/filtering-form.php');
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">

<table>
    <tr>
        <th>&nbsp;</th>
        <th><?php echo stmLang('match', 'season-round'); ?></th>
        <th><?php echo stmLang('competition', 'rounds', 'match-count'); ?></th>
        <th><?php echo stmLang('match', 'date'); ?></th>
    </tr>
    <?php foreach($rounds as $round): ?>
    <tr>
        <?php if ($round->allMatches > 0): ?>
        <td><input id="<?php echo $round->round; ?>" type="checkbox" name="rounds[<?php echo $round->round; ?>][round]" value="<?php echo $round->round; ?>" /></td>
        <?php else: ?>
        <td>–</td>
        <?php endif; ?>
        <td><label for="<?php echo $round->round; ?>"><?php echo $round->round; ?>.</label></td>
        <td><?php echo $round->allMatches; ?></td>
        <?php if ($round->allMatches > 0): ?>
        <td><?php
            $input_name = "rounds[{$round->round}][date]";
            $_POST[$input_name] = isset($dates[$round->round]) ? $dates[$round->round] : '';
            echo \STM\Web\HTML\Forms::inputDate($input_name);
        ?></td>
        <?php else: ?>
        <td>–</td>
        <?php endif; ?>
    </tr>
    <?php endforeach; ?>
</table>

    <input type="submit" name="update" value="<?php echo stmLang('form', 'change'); ?>" />
</form>

<?php endif;
