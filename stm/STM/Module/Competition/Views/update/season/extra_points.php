<?php
/**
 * Update season periods in the Season
 * $team                Team
 * $team_points         current number of team extra points
 */
?>

<ul>
    <li><?php echo stmLang('competition', 'team'); ?>: <strong><?php echo $team; ?></strong></li>
    <li><?php echo stmLang('competition', 'extra', 'current'); ?>: <strong><?php echo $team_points; ?></strong></li>
</ul>

<h2><?php echo stmLang('competition', 'extra', 'add'); ?></h2>
<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <label for="extra_points"><?php echo stmLang('competition', 'extra', 'added'); ?></label>
    <?php include(STM_MODULES_ROOT . 'Competition/Views/_elements/form-parts/extra-points.php'); ?>

    <input type="submit" name="update" value="<?php echo stmLang('competition', 'extra', 'add'); ?>" />
</form>
