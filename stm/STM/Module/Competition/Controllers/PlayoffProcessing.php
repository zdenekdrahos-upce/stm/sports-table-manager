<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\Competition;

use STM\Module\ModuleProcessing;
use STM\Web\Message\SessionMessage;
use STM\Competition\Playoff\Playoff;
use STM\Competition\Playoff\PlayoffValidator;
use STM\Competition\CompetitionValidator;
use STM\Competition\Playoff\Round\PlayoffRoundValidator;
use STM\Competition\Playoff\Round\PlayoffRoundSaver;
use STM\Competition\Playoff\Team\PlayoffTeamValidator;
use STM\Competition\Playoff\Team\PlayoffTeam;
use STM\Competition\Playoff\Serie\PlayoffSerie;
use STM\Libs\Generators\Playoff\PlayoffGenerator;

class PlayoffProcessing extends ModuleProcessing
{
    /** @var \Playoff */
    private $playoff;

    public function __construct($playoff = false)
    {
        $this->playoff = $playoff instanceof Playoff ? $playoff : false;
        parent::__construct($this->playoff, '\STM\Competition\CompetitionEvent');
        CompetitionValidator::init($this->formProcessor);
        PlayoffValidator::init($this->formProcessor);
    }

    public function createPlayoff()
    {
        if (isset($_POST['create'])) {
            $this->formProcessor->escapeValues();
            $attributes = array(
                'competition' => array(
                    'name' => $_POST['name'], 'match_periods' => $_POST['match_periods'],
                    'date_start' => $_POST['date_start'], 'date_end' => $_POST['date_end'],
                    'id_score_type' => $_POST['id_score_type'],
                    'id_competition_type' => $_POST['id_competition_type'],
                    'id_category' => $_POST['id_category'], 'id_status' => $_POST['id_status']
                ),
                'playoff' => $_POST['playoff']
            );
            $id_competition = Playoff::create($attributes);
            parent::checkAction($id_competition, 'CREATE', "New Playoff: {$_POST['name']} ({$id_competition})");
            parent::setSessionMessage($id_competition, $this->getMessage('create', $_POST['name']));
            return $id_competition;
        } else {
            $this->formProcessor->initVars(
                array(
                    'date_start', 'date_end', 'name', 'match_periods', 'id_competition_type',
                    'id_score_type', 'id_category', 'id_status'
                )
            );
            $_POST['playoff']['serie_win_matches'] = '';
            $_POST['match_periods'] = STM_MATCH_PERIODS;
        }
        return false;
    }

    public function generateRound()
    {
        if (parent::isObjectSet() && isset($_POST['generate'])) {
            $data = $this->getDataForGeneratingNewRound();
            PlayoffRoundValidator::init($this->formProcessor);
            $result = PlayoffRoundSaver::save($data);
            parent::checkAction(
                $result,
                'PLAYOFF_ROUND',
                "Generate round n.{$data['round']} ({$this->playoff->__toString()})"
            );
            parent::setSessionMessage($result, $this->getMessage('create', stmLang('match', 'season-round')));
            return $result;
        }
        return false;
    }

    public function updatePlayoff()
    {
        if (parent::isObjectSet() && isset($_POST['update'])) {
            $this->formProcessor->escapeValues();
            $update = $this->playoff->updatePlayoff($_POST['playoff']['serie_win_matches']);
            parent::checkAction($update, 'UPDATE', "Serie win matches ({$this->playoff->__toString()})");
            parent::setSessionMessage($update, $this->getMessage('change'));
            return $update;
        } else {
            $_POST['playoff']['serie_win_matches'] = $this->playoff->getSerieWinMatches();
        }
        return false;
    }

    public function updateTeamSeeded()
    {
        if (parent::isObjectSet() && isset($_POST['update'])) {
            $this->formProcessor->escapeValues();
            PlayoffTeamValidator::init($this->formProcessor);
            $update = PlayoffTeam::updatePlayoffSeeded($this->playoff, $_POST['seeded']);
            parent::checkAction($update, 'UPDATE', "Playoff seeded ({$this->playoff->__toString()})");
            parent::setSessionMessage($update, $this->getMessage('change'));
            return $update;
        }
        return false;
    }

    public function updateSeriesSeeded()
    {
        $result = false;
        if (parent::isObjectSet() && isset($_POST['update'])) {
            $this->formProcessor->checkRequiredFields(array('type'));
            $result = PlayoffSerie::updateSeededType($this->playoff, $_POST['type']);
        }
        parent::setSessionMessage(
            $result,
            $this->getMessage('change', stmLang('competition', 'round', 'seeded')),
            $this->getMessage('change-fail', stmLang('competition', 'round', 'seeded'))
        );
    }

    public function deleteRound()
    {
        if (parent::canBeDeleted()) {
            $delete = PlayoffSerie::deletePlayoffRound($this->playoff);
            parent::log(
                'PLAYOFF_ROUND',
                "Delete round n.{$this->playoff->getPlayedRounds()} ({$this->playoff->__toString()})",
                $delete
            );
            parent::setSessionMessage(
                $delete,
                $this->getMessage('delete-success', stmLang('match', 'season-round')),
                $this->getMessage('delete-fail', stmLang('match', 'season-round'))
            );
        }
    }

    private function getDataForGeneratingNewRound()
    {
        if ($this->playoff->getPlayedRounds() == 0) {
            $items_for_generator = $this->playoff->getTeamsWithSeeded();
        } else {
            $items_for_generator = $this->entities->PlayoffSerie->findByPlayoffRound(
                $this->playoff,
                $this->playoff->getPlayedRounds()
            );
        }
        $generator = PlayoffGenerator::create($items_for_generator);
        $data = array(
            'playoff' => $this->playoff,
            'round' => $this->playoff->getPlayedRounds() + 1,
            'matches' => $generator->generateRound(),
        );
        return $data;
    }

    private function getMessage($name, $competition = false)
    {
        $competition = $competition || !parent::isObjectSet() ? $competition : $this->playoff->__toString();
        return SessionMessage::get($name, array(stmLang('competition', 'playoff'), $competition));
    }
}
