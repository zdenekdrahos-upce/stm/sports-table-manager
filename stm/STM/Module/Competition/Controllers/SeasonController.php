<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\Competition;

use STM\Module\ModuleController;
use STM\Web\Message\SessionMessage;
use STM\Competition\Season\Season;
use STM\Competition\Season\Rounds\SeasonRounds;
use \STM\Match\Table\Cross\CrossTableFactory;
use STM\Competition\Season\Table\SeasonTableUpdater;
use STM\Utils\Arrays;
use STM\Libs\Generators\Season\SeasonGenerator;
use STM\Competition\Playoff\Playoff;
use STM\StmCache;

class SeasonController extends ModuleController
{
    /** @var \STM\Module\Competition\SeasonProcessing */
    private $processing;

    protected function __construct()
    {
        parent::__construct();
        parent::setMainPage('index.php?module=competition');
        $this->processing = new SeasonProcessing($this->competition);
    }

    /** index.php?module=season */
    public function index()
    {
        if ($this->session->hasMessageFail()) {
            $this->session->setMessageFail($this->session->getMessageFail());
        }
        parent::redirectToIndexPage();
    }

    /** index.php?module=season&action=create&type=season */
    public function createSeason()
    {
        if ($this->processing->createSeason()) {
            parent::redirectToIndexPage();
        } else {
            parent::setIndexInLeftMenu();
            parent::setFormErrors($this->processing);
            parent::setContent(array('categories' => $this->entities->CompetitionCategory->findAll()), 'season/season');
        }
    }

    /** index.php?module=season&action=create&type=schedule&c=ID_COMP */
    public function createSchedule()
    {
        if ($this->checkSelectedCompetition()) {
            if ($this->competition->getCountTeams() < 2 || $this->competition->getCountTeams() > SEASON_MAX_TEAMS) {
                $this->session->setMessageFail(SessionMessage::get('season-teamcount', array(SEASON_MAX_TEAMS)));
                redirect(buildUrl(array('module' => 'competition', 'action' => 'read', 'type' => 'teams')));
            } elseif ($this->processing->generateSchedule()) {
                redirect(buildUrl(array('module' => 'competition', 'action' => 'read', 'type' => 'matches')));
            } else {
                $generator = $this->getGenerator();
                $data = array(
                    'team_count' => $this->competition->getCountTeams(),
                    'season_periods' => $this->competition->getSeasonPeriods(),
                    'generator' => $generator,
                    'generated_matches' => $this->prepareGeneratedMatches($generator),
                    'existing_matches' => $this->competition->getCountMatches() > 0
                );
                parent::setProfileInLeftMenu();
                parent::setFormErrors($this->processing);
                parent::setContent($data, 'season/schedule');
            }
        }
    }

    /** index.php?module=season&action=read&type=tables&c=ID_COMP */
    public function readTables()
    {
        if ($this->checkSelectedCompetition()) {
            $data = array(
                'display_wot' => $this->competition->getPointWin() != $this->competition->getPointWinOvertime(),
                'display_lot' => $this->competition->getPointLoss() != $this->competition->getPointLossOvertime(),
                'result_table' => StmCache::getSeasonTable($this->competition),
                'your_teams' => $this->competition->getIdOfYourTeams(),
            );
            parent::setProfileInLeftMenu();
            parent::setContent($data, 'season/tables');
        }
    }

    /** index.php?module=season&action=read&type=rounds&c=ID_COMP */
    public function readRounds()
    {
        if ($this->checkSelectedCompetition()) {
            $rounds = new SeasonRounds($this->competition);
            $data = array(
                'rounds' => $rounds->getRounds(),
            );
            parent::setProfileInLeftMenu();
            parent::setContent($data, 'season/rounds');
        }
    }

    /** index.php?module=season&action=read&type=cross_table&c=ID_COMP */
    public function readCrossTable()
    {
        if ($this->checkSelectedCompetition()) {
            $cross_table = CrossTableFactory::getSeasonCrossTable($this->competition);
            $data = array(
                'cross_table' => $cross_table->getTable(),
            );
            parent::setProfileInLeftMenu();
            parent::setContent($data, 'season/cross_table');
        }
    }

    /** index.php?module=season&action=update&type=season&c=ID_COMP */
    public function updateSeason()
    {
        if ($this->checkSelectedCompetition()) {
            if ($this->processing->updateSeason()) {
                redirect(buildUrl(array('module' => 'competition', 'action' => 'read', 'type' => 'competition')));
            } else {
                parent::setProfileInLeftMenu();
                parent::setFormErrors($this->processing);
                parent::setContent(array(), 'season/season');
            }
        }
    }

    /** index.php?module=season&action=update&type=extra_points&c=ID_COMP&t=ID_TEAM */
    public function updateExtraPoints()
    {
        if ($this->checkSelectedCompetition()) {
            if (!$this->checkSelectedTeam(false)) {
                $this->session->setMessageFail(SessionMessage::get('competition-team'));
                redirect(buildUrl(array('action' => 'read', 'type' => 'teams', 't' => null)));
            } elseif (!$this->competition->isTeamFromCompetition($this->team)) {
                $this->session->setMessageFail(SessionMessage::get('competition-team'));
                redirect(buildUrl(array('action' => 'read', 'type' => 'teams', 't' => null)));
            } else {
                if ($this->processing->updateExtraPoints($this->team)) {
                    redirect($_SERVER['REQUEST_URI']);
                } else {
                    $data = array(
                        'team' => $this->team,
                        'team_points' => $this->competition->getTeamExtraPoints($this->team)
                    );
                    parent::setProfileInLeftMenu();
                    parent::setFormErrors($this->processing);
                    parent::setContent($data, 'season/extra_points');
                }
            }
        }
    }

    /** index.php?module=season&action=update&type=round_date&c=ID_COMP[&t=ID_TEAM] */
    public function updateRoundDate()
    {
        if ($this->checkSelectedCompetition()) {
            $rounds = new SeasonRounds($this->competition);
            if ($this->processing->updateRoundDate($rounds)) {
                redirect(buildUrl(array('action' => 'read', 'type' => 'rounds')));
            } else {
                parent::setProfileInLeftMenu();
                parent::setFormErrors($this->processing);
                parent::setContent(
                    array(
                        'teams' => $this->competition->getTeams(),
                        'rounds' => $rounds->getRounds(),
                        'dates' => $this->getDatesForRounds(),
                    ),
                    'season/round_date'
                );
            }
        }
    }

    /** index.php?module=season&action=update&type=table&c=ID_COMP */
    public function updateTable()
    {
        if ($this->checkSelectedCompetition()) {
            $updater = new SeasonTableUpdater($this->competition);
            $table = $updater->getUpdatedResultTable();
            $this->processing->updateTable($table);
            redirect(buildUrl(array('module' => 'season', 'action' => 'read', 'type' => 'tables')));
        }
    }

    private function getDatesForRounds()
    {
        if (parent::checkSelectedTeam(false)) {
            $ms = array(
                'matchType' => \STM\Match\MatchSelection::ALL_MATCHES,
                'loadScores' => false,
                'loadPeriods' => false,
                'competition' => $this->competition,
                'teams' => array($this->team),
            );
            $matches = $this->entities->Match->find($ms);
            return Arrays::getAssocArray($matches, 'getRound', 'getDate');
        }
        return array();
    }

    /** index.php?module=season&action=delete&type=round_matches&c=ID_COMP&r=ROUND */
    public function deleteRoundMatches()
    {
        if ($this->checkSelectedCompetition()) {
            $this->processing->deleteRoundMatches();
            redirect(buildUrl(array('action' => 'read', 'type' => 'rounds', 'r' => null)));
        }
    }

    private function prepareGeneratedMatches($generator)
    {
        $matches = array();
        foreach ($generator as $number => $round) {
            foreach ($round as $match) {
                $matches[$number][] = array(
                    'h' => $match->home->getId(),
                    'a' => $match->away->getId(),
                );
            }
        }
        return $matches;
    }

    private function getGenerator()
    {
        $generator = SeasonGenerator::create(
            $this->competition->getTeams(),
            $this->competition->getSeasonPeriods()
        );
        $generator->shuffleTeams();
        $generator->generate();
        return $generator;
    }

    /* OVERRIDE */
    // only season is loaded
    protected function loadSelectedCompetition()
    {
        parent::loadSelectedCompetition();
        if (parent::checkSelectedCompetition(false)) {
            if (!($this->competition instanceof Season)) {
                $this->session->setMessageFail(
                    SessionMessage::get('invalid', array(stmLang('competition', 'season')))
                );
                redirect('?module=competition&action=read&type=competition&c=' . $this->competition->getId());
            }
        }
    }

    // duplicate in CompetitionController
    /** @return array */
    protected function getProfileLinks()
    {
        return array(
            'competition_name' => $this->competition->__toString(),
            'is_season' => $this->competition instanceof Season,
            'is_playoff' => $this->competition instanceof Playoff,
            'id_category' => $this->competition->getIdCategory(),
            'category' => $this->competition->getNameCategory(),
        );
    }
}
