<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\Competition;

use STM\Module\ModuleProcessing;
use STM\Web\Message\FormError;
use STM\Web\Message\SessionMessage;
use STM\Competition\CompetitionValidator;
use STM\Competition\Season\Season;
use STM\Competition\Season\SeasonValidator;
use STM\Competition\Season\Schedule\SeasonScheduleValidator;
use STM\Competition\Season\Schedule\SeasonScheduleSaver;
use STM\Competition\Season\Schedule\SeasonRescheduler;
use STM\Competition\Season\Rounds\SeasonRounds;
use STM\Match\MatchValidator;
use STM\Competition\Season\Table\SeasonResultTable;
use STM\Cache\File\SerializationFile;

class SeasonProcessing extends ModuleProcessing
{
    /** @var \Season */
    private $season;

    public function __construct($season = false)
    {
        $this->season = $season instanceof Season ? $season : false;
        parent::__construct($this->season, '\STM\Competition\CompetitionEvent');
        CompetitionValidator::init($this->formProcessor);
        SeasonValidator::init($this->formProcessor);
    }

    public function createSeason()
    {
        if (isset($_POST['create'])) {
            $this->formProcessor->escapeValues();
            $attributes = array(
                'competition' => array(
                    'name' => $_POST['name'], 'match_periods' => $_POST['match_periods'],
                    'date_start' => $_POST['date_start'], 'date_end' => $_POST['date_end'],
                    'id_score_type' => $_POST['id_score_type'],
                    'id_competition_type' => $_POST['id_competition_type'],
                    'id_category' => $_POST['id_category'], 'id_status' => $_POST['id_status']
                ),
                'season' => $_POST['season']
            );
            $id_competition = Season::create($attributes);
            parent::checkAction($id_competition, 'CREATE', "New Season: {$_POST['name']} ({$id_competition})");
            parent::setSessionMessage($id_competition, $this->getMessage('create', $_POST['name']));
            return $id_competition;
        } else {
            $this->initPostForCreateSeason();
        }
        return false;
    }

    public function generateSchedule()
    {
        if (isset($_POST['generate']) && parent::isObjectSet()) {
            $data = array(
                'season' => $this->season,
                'dates' => $_POST['dates'],
                'matches' => json_decode($_POST['matches'], true)
            );
            SeasonScheduleValidator::init($this->formProcessor);
            if ($this->season->getCountMatches() == 0) {
                $message = 'Generate new schedule';
                $result = SeasonScheduleSaver::save($data);
            } else {
                $message = 'Regenerate existing schedule';
                $result = SeasonRescheduler::save($data);
            }
            parent::checkAction($result, 'SEASON_SCHEDULE', "{$message} ({$this->season->__toString()})");
            parent::setSessionMessage($result, SessionMessage::get('season-generated-schedule'));
            parent::deleteCacheIfSuccessfulAction($result, $this->season->getId());
            return $result;
        } else {
            $this->formProcessor->initVars(array(array('dates', $this->season->getSeasonPeriods())));
        }
        return false;
    }

    public function updateSeason()
    {
        if (parent::isObjectSet() && isset($_POST['update'])) {
            unset($_POST['update']);
            $this->formProcessor->escapeValues();
            $update = $this->season->updateSeason($_POST['season']);
            parent::checkAction($update, 'UPDATE', "Season Details ({$this->season->__toString()})");
            parent::setSessionMessage($update, $this->getMessage('change'));
            parent::deleteCacheIfSuccessfulAction($update, $this->season->getId());
            return $update;
        } else {
            $arr = $this->season->toArray();
            $_POST['season']['season_periods'] = $arr['season_periods'];
            $_POST['season']['point_win'] = $arr['points']['win'];
            $_POST['season']['point_win_ot'] = $arr['points']['win_overtime'];
            $_POST['season']['point_draw'] = $arr['points']['draw'];
            $_POST['season']['point_loss_ot'] = $arr['points']['loss_overtime'];
            $_POST['season']['point_loss'] = $arr['points']['loss'];
        }
        return false;
    }

    public function updateExtraPoints($team)
    {
        if (parent::isObjectSet() && isset($_POST['update'])) {
            $this->formProcessor->escapeValues();
            $update = $this->season->addExtraPoints($team, $_POST['extra_points']);
            parent::checkAction(
                $update,
                'UPDATE',
                "Team {$team} extra points were updated ({$this->season->__toString()})"
            );
            parent::setSessionMessage($update, $this->getMessage('change', stmLang('competition', 'team')));
            parent::deleteCacheIfSuccessfulAction($update, $this->season->getId());
            return $update;
        } else {
            $this->formProcessor->initVars(array('extra_points'));
        }
        return false;
    }

    public function updateRoundDate(SeasonRounds $rounds)
    {
        if (parent::isObjectSet() && isset($_POST['update'])) {
            $this->formProcessor->escapeValues();
            $_POST['rounds'] = isset($_POST['rounds']) ? $_POST['rounds'] : array();
            $count_all = 0;
            $count_created = 0;
            foreach ($_POST['rounds'] as $round_array) {
                if (array_key_exists('round', $round_array)) {
                    MatchValidator::init($this->formProcessor);
                    $count_all++;
                    $update = $rounds->updateMatchesDateInRound($round_array['round'], $round_array['date']);
                    if ($update) {
                        $count_created++;
                    }
                }
            }
            if ($count_all == 0) {
                $this->formProcessor->addError(FormError::get('nothing-checked'));
                return false;
            } else {
                parent::log(
                    'SEASON_ROUNDS',
                    "Update matches date in {$count_created} rounds ({$this->season->__toString()})",
                    true
                );
                parent::setSessionMessage(
                    true,
                    SessionMessage::get('competition-round-date-update', array($count_created, $count_all))
                );
                parent::deleteCacheIfSuccessfulAction(true, $this->season->getId());
                return true;
            }
        } else {
            $this->formProcessor->initVars(array('round_date'));
        }
        return false;
    }

    public function updateTable(SeasonResultTable $table)
    {
        if (parent::isObjectSet()) {
            $file = new SerializationFile(STM_CACHE_SEASON_TABLES . $this->season->getId() . '.txt');
            $update = $file->writeContent($table) !== false;
            parent::setSessionMessage(
                $update,
                SessionMessage::get('season-full-table-sort-success'),
                SessionMessage::get('season-full-table-sort-fail')
            );
            parent::log(
                'SEASON_TABLE',
                "Sort teams in full table according head to head matches ({$this->season->__toString()})",
                $update
            );
        }
    }

    public function deleteRoundMatches()
    {
        if (parent::canBeDeleted()) {
            $round = isset($_GET['r']) ? $_GET['r'] : false;
            $rounds = new SeasonRounds($this->season);
            $delete = $rounds->deleteMatchesFromRound($round);
            parent::setSessionMessage(
                $delete,
                $this->getMessage('delete-success', stmLang('match', 'season-round')),
                $this->getMessage('delete-fail', stmLang('match', 'season-round'))
            );
            parent::deleteCacheIfSuccessfulAction($delete, $this->season->getId());
            parent::log('SEASON_ROUNDS', "Delete round n. {$round} ({$this->season->__toString()})", $delete);
        }
    }

    private function initPostForCreateSeason()
    {
        $this->formProcessor->initVars(
            array(
                'date_start', 'date_end', 'name', 'match_periods', 'id_competition_type',
                'id_score_type', 'id_category', 'id_status'
            )
        );
        $season_variables = array(
            'season_periods' => '', 'point_win' => STM_POINT_WIN, 'point_win_ot' => STM_POINT_WIN_OT,
            'point_draw' => STM_POINT_DRAW, 'point_loss_ot' => STM_POINT_LOSS_OT, 'point_loss' => STM_POINT_LOSS
        );
        foreach ($season_variables as $key => $default_value) {
            $_POST['season'][$key] = $default_value;
        }
        $_POST['match_periods'] = STM_MATCH_PERIODS;
    }

    private function getMessage($name, $competition = false)
    {
        $competition = $competition || !parent::isObjectSet() ? $competition : $this->season->__toString();
        return SessionMessage::get($name, array(stmLang('competition', 'season'), $competition));
    }
}
