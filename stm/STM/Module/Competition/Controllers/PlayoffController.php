<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\Competition;

use STM\Module\ModuleController;
use STM\Web\Message\SessionMessage;
use STM\Competition\Playoff\Playoff;
use STM\Competition\Season\Season;
use STM\Competition\Playoff\Tree\PlayoffTreeBuilder;
use STM\Competition\Playoff\Tree\PlayoffTreeAdapter;
use STM\Libs\Generators\Playoff\PlayoffGenerator;
use STM\Libs\Generators\Playoff\PlayoffException;

class PlayoffController extends ModuleController
{
    /** @var \STM\Module\Competition\PlayoffProcessing */
    private $processing;
    /** @var \PlayoffSerie */
    private $serie;

    protected function __construct()
    {
        parent::__construct();
        parent::setMainPage('index.php?module=competition');
        $this->loadSelectedSerie();
        $this->processing = new PlayoffProcessing($this->competition);
    }

    /** index.php?module=playoff */
    public function index()
    {
        if ($this->session->hasMessageFail()) {
            $this->session->setMessageFail($this->session->getMessageFail());
        }
        parent::redirectToIndexPage();
    }

    /** index.php?module=playoff&action=create&type=playoff */
    public function createPlayoff()
    {
        if ($this->processing->createPlayoff()) {
            parent::redirectToIndexPage();
        } else {
            parent::setIndexInLeftMenu();
            parent::setFormErrors($this->processing);
            parent::setContent(
                array('categories' => $this->entities->CompetitionCategory->findAll()),
                'playoff/playoff'
            );
        }
    }

    /** index.php?module=playoff&action=create&type=playoff&c=ID_PLAYOFF */
    public function createRound()
    {
        if ($this->checkSelectedCompetition()) {
            $played_rounds = $this->competition->getPlayedRounds();
            if ($played_rounds == $this->competition->getMaxRound()) {
                $this->session->setMessageFail(SessionMessage::get('playoff-generated'));
                redirect(buildUrl(array('module' => 'playoff', 'action' => 'read', 'type' => 'series')));
            } elseif ($played_rounds == 0) {
                $this->generatFirstRound();
            } else {
                $this->generateNextRound();
            }
        }
    }

    /** index.php?module=playoff&action=read&type=series&c=ID_PLAYOFF */
    public function readSeries()
    {
        if ($this->checkSelectedCompetition()) {
            $data = array(
                'played_rounds' => $this->competition->getPlayedRounds(),
                'max_round' => $this->competition->getMaxRound()
            );
            if (!isset($_POST['id_round'])) {
                $_POST['id_round'] = $data['played_rounds'];
            }
            $round = $_POST['id_round'];
            if (is_numeric($round) && $round > 0) {
                $this->checkRoundNumber($round);
                $data['series'] = $this->entities->PlayoffSerie->findByPlayoffRound($this->competition, $round);
                $data['selected_round'] = $round;
            }
            parent::setProfileInLeftMenu();
            parent::setContent($data, 'playoff/series');
        }
    }

    /** index.php?module=playoff&action=read&type=series&c=ID_PLAYOFF&s=ID_SERIE */
    public function readSerie()
    {
        if ($this->checkSelectedCompetition() && $this->checkSelectedSerie()) {
            $data = array(
                'serie' => $this->serie,
                'matches' => $this->entities->Match->findByPlayoffSerie($this->serie),
            );
            parent::setProfileInLeftMenu();
            parent::setContent($data, 'playoff/serie');
        }
    }

    /** index.php?module=playoff&action=read&type=tree&c=ID_PLAYOFF */
    public function readTree()
    {
        if ($this->checkSelectedCompetition()) {
            $builder = new PlayoffTreeBuilder($this->competition);
            $adapter = new PlayoffTreeAdapter($builder);
            $data = array(
                'playoff' => $this->competition,
                'rounds' => $adapter->getRounds(),
            );
            parent::setProfileInLeftMenu();
            parent::setContent($data, 'playoff/tree');
        }
    }

    /** index.php?module=playoff&action=update&type=seeded&c=ID_PLAYOFF */
    public function updateSeeded()
    {
        if ($this->checkSelectedCompetition()) {
            if ($this->processing->updateTeamSeeded()) {
                redirect($_SERVER['REQUEST_URI']);
            } else {
                $data = array('teams' => $this->competition->getTeamsWithSeeded());
                parent::setProfileInLeftMenu();
                parent::setFormErrors($this->processing);
                parent::setContent($data, 'playoff/seeded');
            }
        }
    }

    /** index.php?module=playoff&action=update&type=series_seeded&c=ID_PLAYOFF */
    public function updateSeriesSeeded()
    {
        if ($this->checkSelectedCompetition()) {
            $this->processing->updateSeriesSeeded();
            redirect(buildUrl(array('action' => 'create', 'type' => 'round')));
        }
    }

    /** index.php?module=playoff&action=update&type=playoff&c=ID_PLAYOFF */
    public function updatePlayoff()
    {
        if ($this->checkSelectedCompetition()) {
            if ($this->processing->updatePlayoff()) {
                redirect(buildUrl(array('module' => 'competition', 'action' => 'read', 'type' => 'competition')));
            } else {
                parent::setProfileInLeftMenu();
                parent::setFormErrors($this->processing);
                parent::setContent(array(), 'playoff/playoff');
            }
        }
    }

    /** index.php?module=playoff&action=delete&type=round&c=ID_PLAYOFF */
    public function deleteRound()
    {
        if ($this->checkSelectedCompetition()) {
            $this->processing->deleteRound();
            redirect(buildUrl(array('action' => 'read', 'type' => 'series')));
        }
    }

    private function generatFirstRound()
    {
        if ($this->processing->generateRound()) {
            redirect(buildUrl(array('module' => 'playoff', 'action' => 'read', 'type' => 'series')));
        } else {
            try {
                $data = array(
                    'round_number' => $this->competition->getPlayedRounds() + 1,
                    'teams' => $this->competition->getTeamsWithSeeded(),
                    'generator' => $this->getGeneratedRound($this->competition->getTeamsWithSeeded())
                );
                parent::setProfileInLeftMenu();
                parent::setFormErrors($this->processing);
                parent::setContent($data, 'playoff/first_round');
            } catch (PlayoffException $e) {
                $this->session->setMessageFail(SessionMessage::get('playoff-teamcount'));
                redirect(buildUrl(array('module' => 'competition', 'action' => 'read', 'type' => 'teams')));
            }
        }
    }

    private function generateNextRound()
    {
        if ($this->processing->generateRound()) {
            redirect(buildUrl(array('module' => 'playoff', 'action' => 'read', 'type' => 'series')));
        } else {
            $previous_round = $this->entities->PlayoffSerie->findByPlayoffRound(
                $this->competition,
                $this->competition->getPlayedRounds()
            );
            $data = array(
                'playoff' => $this->competition,
                'round_number' => $this->competition->getPlayedRounds() + 1,
                'previous_round' => $previous_round,
                'generator' => $this->getGeneratedRound($previous_round)
            );
            parent::setProfileInLeftMenu();
            parent::setFormErrors($this->processing);
            parent::setContent($data, 'playoff/next_round');
        }
    }

    private function getGeneratedRound($teams)
    {
        $generator = PlayoffGenerator::create($teams);
        return $generator->generateRound();
    }

    private function checkRoundNumber($round_number)
    {
        if ($round_number < 1 || $round_number > $this->competition->getPlayedRounds()) {
            $this->session->setMessageFail(
                SessionMessage::get('invalid', array(stmLang('competition', 'tree', 'round-select')))
            );
            redirect($_SERVER['REQUEST_URI']);
        }
    }

    /* OVERRIDE */

    // only season is loaded
    protected function loadSelectedCompetition()
    {
        parent::loadSelectedCompetition();
        if (parent::checkSelectedCompetition(false)) {
            if (!($this->competition instanceof Playoff)) {
                $this->session->setMessageFail(
                    SessionMessage::get('invalid', array(stmLang('competition', 'playoff')))
                );
                redirect('?module=competition&action=read&type=competition&c=' . $this->competition->getId());
            }
        }
    }

    private function checkSelectedSerie()
    {
        if (!$this->serie) {
            $this->session->setMessageFail(SessionMessage::get('invalid', array(stmLang('competition', 'serie'))));
            redirect(buildUrl(array('module' => 'playoff', 'action' => 'read', 'type' => 'series', 's' => null)));
        }
        return true;
    }

    private function loadSelectedSerie()
    {
        $this->serie = isset($_GET['s']) ? $this->entities->PlayoffSerie->findById($_GET['s']) : false;
    }

    /** @return array */
    protected function getProfileLinks()
    {
        return array(
            'competition_name' => $this->competition->__toString(),
            'is_season' => $this->competition instanceof Season,
            'is_playoff' => $this->competition instanceof Playoff,
            'id_category' => $this->competition->getIdCategory(),
            'category' => $this->competition->getNameCategory(),
        );
    }
}
