<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\Competition;

use STM\Module\ModuleController;
use STM\Web\Message\SessionMessage;
use STM\Competition\Season\Season;
use STM\Competition\Playoff\Playoff;
use STM\Match\MatchSelection;
use STM\Competition\Summary\CompetitionSummaryFactory;
use STM\StmCache;

class CompetitionController extends ModuleController
{
    /** @var \STM\Module\Competition\CompetitionProcessing */
    private $processing;

    protected function __construct()
    {
        parent::__construct();
        parent::setMainPage('index.php?module=competition');
        $this->processing = new CompetitionProcessing($this->competition);
    }

    /** index.php?module=competition */
    public function index()
    {
        parent::setIndexInLeftMenu();
        parent::setContent(
            array(
                'competitions' => $this->getCompetitions(),
                'teams' => $this->entities->Team->findAll()
            )
        );
    }

    /** index.php?module=competition&action=create&type=competition */
    public function createCompetition()
    {
        if ($this->processing->createCompetition()) {
            parent::redirectToIndexPage();
        } else {
            parent::setIndexInLeftMenu();
            parent::setFormErrors($this->processing);
            parent::setContent(array('categories' => $this->entities->CompetitionCategory->findAll()));
        }
    }

    /** index.php?module=competition&action=create&type=team&c=ID_COMP */
    public function createTeam()
    {
        if ($this->checkSelectedCompetition()) {
            $this->checkPlayoffRound();
            if ($this->processing->createTeam()) {
                redirect(buildUrl(array('action' => 'read', 'type' => 'teams')));
            } else {
                $data = array(
                    'competition' => $this->competition,
                    'all_teams' => $this->entities->Team->findAll(),
                    'teams_count' => $this->competition->getCountTeams(),
                );
                parent::setProfileInLeftMenu();
                parent::setFormErrors($this->processing);
                parent::setContent($data);
            }
        }
    }

    /** index.php?module=competition&action=create&type=teams&c=ID_COMP */
    public function createTeams()
    {
        if ($this->checkSelectedCompetition()) {
            $this->checkPlayoffRound();
            if ($this->processing->addTeamsFromCompetition()) {
                redirect(buildUrl(array('action' => 'read', 'type' => 'teams')));
            } else {
                $data = array('competitions' => $this->entities->Competition->findAll());
                parent::setProfileInLeftMenu();
                parent::setFormErrors($this->processing);
                parent::setContent($data);
            }
        }
    }

    /** index.php?module=competition&action=create&type=your_team&c=ID_COMP */
    public function createYourTeam()
    {
        if ($this->checkSelectedCompetition()) {
            $this->processing->createYourTeam($this->team);
            redirect(buildUrl(array('action' => 'read', 'type' => 'teams', 't' => null)));
        }
    }

    /** index.php?module=competition&action=create&type=players&c=ID_COMP */
    public function createPlayers()
    {
        if ($this->checkSelectedCompetition()) {
            if ($this->processing->createPlayers()) {
                redirect(
                    buildUrl(
                        array('module' => 'statistics', 'action' => 'read', 'type' => 'competition_players')
                    )
                );
            } else {
                parent::setProfileInLeftMenu();
                parent::setFormErrors($this->processing);
                parent::setContent(array('positions' => $this->entities->PersonPosition->findAll()));
            }
        }
    }

    /** index.php?module=competition&action=create&type=summary&c=ID_COMP */
    public function createSummary()
    {
        if ($this->checkSelectedCompetition()) {
            if (isset($_POST['create'])) {
                $summary = new CompetitionSummaryFactory();
                $data = array(
                    'summary' => $summary->getCompetitionSummary($this->competition, $_POST)
                );
                parent::setContent($data, 'summary/template');
            } else {
                $data = array(
                    'is_playoff' => $this->competition instanceof Playoff,
                    'max_field_count' => ceil($this->competition->getCountTeams() / 2),
                );
                parent::setProfileInLeftMenu();
                parent::setContent($data, 'summary/form');
            }
        }
    }

    /** index.php?module=competition&action=read&type=competition&c=ID_COMP */
    public function readCompetition()
    {
        if ($this->checkSelectedCompetition()) {
            // basic competition
            $competition_array = $this->competition->toArray();
            $data = array(
                'competition' => $this->competition,
                'is_season' => $this->competition instanceof Season,
                'is_playoff' => $this->competition instanceof Playoff,
                'name' => $competition_array['name'],
                'type' => stmLang('constants', 'competitiontype', $competition_array['type']),
                'competition_data' => array(
                    'date-start' => $competition_array['date_start'],
                    'date-end' => $competition_array['date_end'],
                    'min-match-periods' => $competition_array['match_periods'],
                    'score-type' => stmLang('constants', 'scoretype', $competition_array['score_type']),
                    'status' => stmLang('constants', 'competitionstatus', $competition_array['status']),
                    'category' => $competition_array['category'] ?
                        $competition_array['category'] : stmLang('not-set'),
                ),
                'last_matches' => $this->getMatches(MatchSelection::PLAYED_MATCHES),
                'next_matches' => $this->getMatches(MatchSelection::UPCOMING_MATCHES),
            );
            if ($data['is_season']) {
                $season_fields = array(
                    'periods' => $competition_array['season_periods'],
                    'win' => $competition_array['points']['win'],
                    'win-ot' => $competition_array['points']['win_overtime'],
                    'draw' => $competition_array['points']['draw'],
                    'loss-ot' => $competition_array['points']['loss_overtime'],
                    'loss' => $competition_array['points']['loss'],
                );
                $data['competition_data'] = array_merge($data['competition_data'], $season_fields);
            }
            if ($data['is_playoff']) {
                $data['competition_data']['serie-win-matches'] = $this->competition->getSerieWinMatches();
            }
            parent::setProfileInLeftMenu();
            parent::setContent($data);
        }
    }

    private function getMatches($matchType)
    {
        $matchLimit = (int) ceil($this->competition->getCountTeams() / 2);
        $ascendingOrder = $matchType == MatchSelection::UPCOMING_MATCHES;
        $ms = array(
            'matchType' => $matchType,
            'loadScores' => true,
            'loadPeriods' => false,
            'competition' => $this->competition,
            'limit' => array('max' => $matchLimit),
            'order' => array(
                'datetime' => $ascendingOrder,
                'idMatch' => $ascendingOrder,
            )
        );
        return $this->entities->Match->find($ms);
    }

    /** index.php?module=competition&action=read&type=teams&c=ID_COMP */
    public function readTeams()
    {
        if ($this->checkSelectedCompetition()) {
            $data = array(
                'competition' => $this->competition,
                'teams' => $this->competition->getTeams(),
                'is_season' => $this->competition instanceof Season
            );
            parent::setProfileInLeftMenu();
            parent::setContent($data);
        }
    }

    /** index.php?module=competition&action=read&type=matches&c=ID_COMP */
    public function readMatches()
    {
        if ($this->checkSelectedCompetition()) {
            if ($this->competition instanceof Playoff) {
                redirect(buildUrl(array('module' => 'playoff', 'type' => 'series')));
            }
            $data = array(
                'teams' => $this->competition->getTeams(),
                'competition' => $this->competition,
            );
            if (parent::checkSelectedTeam(false) || isset($_GET['r'])) {
                $ms = array(
                    'matchType' => \STM\Match\MatchSelection::ALL_MATCHES,
                    'loadScores' => true,
                    'loadPeriods' => true,
                    'competition' => $this->competition,
                    'teams' => array($this->team),
                );
                if (isset($_GET['r'])) {
                    $ms['round'] = $_GET['r'];
                }
                $data['matches'] = $this->entities->Match->find($ms);
            } else {
                $data['matches'] = StmCache::getAllCompetitionMatches($this->competition->getId());
            }
            parent::setProfileInLeftMenu();
            parent::setContent($data);
        }
    }

    /** index.php?module=competition&action=read&type=statistics&c=ID_COMP */
    public function readStatistics()
    {
        if ($this->checkSelectedCompetition()) {
            $data = array(
                'teams_count' => $this->competition->getCountTeams(),
                'all_matches_count' => $this->competition->getCountMatches(MatchSelection::ALL_MATCHES),
                'played_matches_count' => $this->competition->getCountMatches(MatchSelection::PLAYED_MATCHES),
                'upcoming_matches_count' => $this->competition->getCountMatches(MatchSelection::UPCOMING_MATCHES),
                'players_counts' => $this->competition->getCountsPlayers()
            );
            if ($this->competition instanceof Season) {
                $data['season_matches_count'] = $this->competition->getTotalNumberOfMatches();
                $data['match_percentage'] = $data['all_matches_count'] ?
                    round(100 * $data['played_matches_count'] / $data['season_matches_count'], 2) : 0;
            }
            if ($this->competition instanceof Playoff) {
                $data['played_rounds'] = $this->competition->getPlayedRounds();
                $data['total_rounds'] = $this->competition->getMaxRound();
            }
            parent::setProfileInLeftMenu();
            parent::setContent($data);
        }
    }

    /** index.php?module=competition&action=read&type=advanced_actions&c=ID_COMP */
    public function readAdvancedActions()
    {
        if ($this->checkSelectedCompetition()) {
            parent::setProfileInLeftMenu();
            parent::setContent(
                array(
                    'is_season' => $this->competition instanceof Season
                )
            );
        }
    }

    public function updateCompetition()
    {
        if ($this->checkSelectedCompetition()) {
            if ($this->processing->updateCompetition()) {
                redirect(buildUrl(array('action' => 'read')));
            } else {
                $this->setProfileInLeftMenu();
                $this->setFormErrors($this->processing);
                $this->setContent(
                    array(
                        'categories' => $this->entities->CompetitionCategory->findAll(),
                        'id_competition_type' => $this->competition->getIdCompetitionType()
                    )
                );
            }
        }
    }

    /** index.php?module=competition&action=delete&type=competition&c=ID_COMP */
    public function deleteCompetition()
    {
        if ($this->checkSelectedCompetition()) {
            $delete = $this->processing->deleteCompetition();
            if ($delete) {
                parent::redirectToIndexPage();
            } else {
                redirect(buildUrl(array('action' => 'read')));
            }
        }
    }

    /** index.php?module=competition&action=delete&type=matches&c=ID_COMP */
    public function deleteMatches()
    {
        if ($this->checkSelectedCompetition()) {
            if ($this->competition instanceof Playoff) {
                $this->session->setMessageFail(SessionMessage::get('competition-playoffmatches'));
                redirect(buildUrl(array('module' => 'playoff', 'action' => 'read', 'type' => 'series')));
            }
            $this->processing->deleteMatches();
            redirect(buildUrl(array('action' => 'read', 'type' => 'competition')));
        }
    }

    /** index.php?module=competition&action=delete&type=team&c=ID_COMP&t=ID_TEAM */
    public function deleteTeam()
    {
        if ($this->checkSelectedCompetition()) {
            $this->checkPlayoffRound();
            $this->processing->deleteTeam($this->team);
            redirect(buildUrl(array('action' => 'read', 'type' => 'teams', 't' => null)));
        }
    }

    /** index.php?module=competition&action=delete&type=your_team&c=ID_COMP&t=ID_TEAM */
    public function deleteYourTeam()
    {
        if ($this->checkSelectedCompetition()) {
            $this->processing->deleteYourTeam($this->team);
            redirect(buildUrl(array('action' => 'read', 'type' => 'teams', 't' => null)));
        }
    }

    private function getCompetitions()
    {
        $cs = parent::getCompetitionsSelection();
        if (parent::checkSelectedTeam(false)) {
            $cs->setTeam($this->team);
        }
        $competitions = $this->entities->Competition->find($cs);
        return $this->categorizeCompetitions($competitions);
    }

    private function categorizeCompetitions($competitions)
    {
        $result = array();
        foreach ($competitions as $competition) {
            if (!array_key_exists($competition->getNameCategory(), $result)) {
                $result[$competition->getNameCategory()] = array();
            }
            $result[$competition->getNameCategory()][] = $competition;
        }
        return $result;
    }

    private function checkPlayoffRound()
    {
        if ($this->competition instanceof Playoff && $this->competition->getPlayedRounds() > 0) {
            $this->session->setMessageFail(SessionMessage::get('competition-playoffround'));
            redirect(buildUrl(array('module' => 'competition', 'action' => 'read', 'type' => 'teams')));
        }
    }

    // duplicate in SeasonController, PlayoffController and Match/views/create/match
    /** @return array */
    protected function getProfileLinks()
    {
        return array(
            'competition_name' => $this->competition->__toString(),
            'is_season' => $this->competition instanceof Season,
            'is_playoff' => $this->competition instanceof Playoff,
            'id_category' => $this->competition->getIdCategory(),
            'category' => $this->competition->getNameCategory(),
        );
    }
}
