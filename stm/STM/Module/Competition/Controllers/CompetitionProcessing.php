<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\Competition;

use STM\Module\ModuleProcessing;
use STM\Web\Message\FormError;
use STM\Web\Message\SessionMessage;
use STM\Competition\Competition;
use STM\Competition\CompetitionValidator;
use STM\Competition\Season\Season;
use STM\Match\Lineup\LineupImportPlayers;

class CompetitionProcessing extends ModuleProcessing
{
    /** @var \Competition */
    protected $competition;

    public function __construct($competition = false)
    {
        $this->competition = $competition instanceof Competition ? $competition : false;
        parent::__construct($this->competition, '\STM\Competition\CompetitionEvent');
        CompetitionValidator::init($this->formProcessor);
    }

    public function createCompetition()
    {
        if (isset($_POST['create'])) {
            unset($_POST['create']);
            $this->formProcessor->escapeValues();
            $id_competition = Competition::create($_POST);
            parent::checkAction($id_competition, 'CREATE', "New Competition: {$_POST['name']} ({$id_competition})");
            parent::setSessionMessage($id_competition, $this->getMessage('create', $_POST['name']));
            return $id_competition;
        } else {
            $this->formProcessor->initVars(
                array(
                    'date_start', 'date_end', 'name', 'match_periods', 'id_competition_type',
                    'id_score_type', 'id_category', 'id_status'
                )
            );
            $_POST['match_periods'] = STM_MATCH_PERIODS;
        }
        return false;
    }

    public function createTeam()
    {
        if (isset($_POST['add']) && parent::isObjectSet()) {
            $this->formProcessor->escapeValues();
            $_POST['teams'] = isset($_POST['teams']) ? array_unique($_POST['teams']) : array();
            $count_all = 0;
            $count_created = 0;
            foreach ($_POST['teams'] as $id_team) {
                $this->formProcessor->resetErrors();
                $count_all++;
                if ($this->competition->addTeam($this->entities->Team->findById($id_team))) {
                    $count_created++;
                }
            }
            if ($count_all == 0) {
                $this->formProcessor->addError(FormError::get('nothing-checked'));
            } elseif ($count_created > 0) {
                parent::log(
                    'ADD_TEAMS',
                    "Add Teams {$count_created}/{$count_all}  ({$this->competition->__toString()})",
                    true
                );
                parent::setSessionMessage(true, SessionMessage::get('competition-addteams'));
                if ($this->competition instanceof Season) {
                    parent::deleteCacheIfSuccessfulAction(true, $this->competition->getId());
                }
                return true;
            }
        }
        return false;
    }

    public function addTeamsFromCompetition()
    {
        if (isset($_POST['add']) && parent::isObjectSet()) {
            $this->formProcessor->escapeValues();
            $competition = $this->entities->Competition->findById($_POST['competition']);
            $add = $this->competition->addTeamsFromCompetition($competition);
            parent::checkAction(
                $add,
                'ADD_TEAMS',
                'Teams from ' . $competition . ' (' . $this->competition->__toString() . ')'
            );
            parent::setSessionMessage($add, SessionMessage::get('competition-addteams'));
            if ($this->competition instanceof Season) {
                parent::deleteCacheIfSuccessfulAction($add, $this->competition->getId());
            }
            return $add;
        }
        return false;
    }

    public function createYourTeam($team)
    {
        if (parent::isObjectSet() && !$this->competition->isYourTeam($team->getId())) {
            $add = $this->competition->addYourTeam($team);
            parent::setSessionMessage($add, $this->getMessage('create', stmLang('competition', 'team')));
            parent::log(
                'YOUR_TEAM',
                'Add ' . $team->__toString() . ' (' . $this->competition->__toString() . ')',
                $add
            );
        } else {
            $this->session->setMessageFail(SessionMessage::get('competition-team'));
        }
    }

    public function createPlayers()
    {
        if (isset($_POST['create']) && parent::isObjectSet()) {
            $import = new LineupImportPlayers();
            $position = isset($_POST['id_position']) ?
                $this->entities->PersonPosition->findById($_POST['id_position']) : null;
            $matchType = isset($_POST['match_type']) ? $_POST['match_type'] : null;
            $summary = $import->importPlayersToLineup($this->competition, $position, $matchType);
            $result = $summary['successful_import'];
            parent::deleteActionCacheIfSuccessfulAction($result, $this->competition->getId());
            $message = "Create {$summary['created_players']} players, ";
            $message .= "existing {$summary['existing_players']} players ({$this->competition->__toString()})";
            parent::checkAction($result, 'IMPORT_PLAYERS_TO_MATCHES', $message);
            parent::setSessionMessage(
                $result,
                SessionMessage::get(
                    'competition-import-players',
                    array($summary['total_matches'], $summary['created_players'], $summary['existing_players'])
                ),
                SessionMessage::get(
                    'competition-import-players-fail',
                    array($summary['total_matches'], $summary['created_players'], $summary['existing_players'])
                )
            );
            return true;
        }
        return false;
    }

    public function updateCompetition()
    {
        if (isset($_POST['update'])) {
            unset($_POST['update']);
            $this->formProcessor->escapeValues();
            $result = $this->competition->update($_POST);
            parent::checkAction(
                $result,
                'UPDATE',
                "Update Competition: {$_POST['name']} ({$this->competition->getId()})"
            );
            parent::setSessionMessage($result, $this->getMessage('change'));
            parent::deleteCacheIfSuccessfulAction($result, $this->competition->getId());
            return $result;
        } else {
            $this->formProcessor->initVars(array('id_score_type', 'id_status'));
            $arr = $this->competition->toArray();
            $unset_keys = array('status', 'score_type');
            foreach ($unset_keys as $key) {
                unset($arr[$key]);
            }
            foreach ($arr as $key => $value) {
                $_POST[$key] = $value;
            }
            $_POST['date_start'] = $_POST['date_start'];
            $_POST['date_end'] = $_POST['date_end'];
            $_POST['id_score_type'] = $this->competition->getScoreType();
            $_POST['id_status'] = $this->competition->getStatusId();
            $_POST['id_competition_type'] = 'X';
        }
        return false;
    }

    public function deleteCompetition()
    {
        if (parent::canBeDeleted()) {
            if ($this->competition->getCountMatches() == 0) {
                $delete = $this->competition->delete();
                parent::setSessionMessage(
                    $delete,
                    $this->getMessage('delete-success'),
                    $this->getMessage('delete-fail')
                );
                parent::log('DELETE', $this->competition->__toString(), $delete);
                parent::deleteCacheIfSuccessfulAction($delete, $this->competition->getId());
                return $delete;
            } else {
                $this->session->setMessageFail(SessionMessage::get('delete-competition'));
            }
        }
        return false;
    }

    public function deleteMatches()
    {
        if (parent::canBeDeleted()) {
            if ($this->competition->getCountMatches() > 0) {
                $delete = $this->competition->deleteMatches();
                parent::setSessionMessage(
                    $delete,
                    $this->getMessage('delete-success', stmLang('menu', 'matches')),
                    $this->getMessage('delete-fail', stmLang('menu', 'matches'))
                );
                parent::log('DELETE_MATCHES', 'In ' . $this->competition->__toString(), $delete);
                parent::deleteCacheIfSuccessfulAction($delete, $this->competition->getId());
            } else {
                $this->session->setMessageFail(SessionMessage::get('delete-competitionmatches'));
            }
        }
    }

    public function deleteTeam($team)
    {
        if (parent::canBeDeleted()) {
            if ($this->competition->isTeamFromCompetition($team)) {
                $delete = $this->competition->deleteTeam($team);
                parent::setSessionMessage(
                    $delete,
                    $this->getMessage('delete-success', stmLang('competition', 'team')),
                    $this->getMessage('delete-fail', stmLang('competition', 'team'))
                );
                parent::log(
                    'DELETE_TEAM',
                    $team->__toString() . ' (' . $this->competition->__toString() . ')',
                    $delete
                );
                parent::deleteCacheIfSuccessfulAction($delete, $this->competition->getId());
            } else {
                $this->session->setMessageFail(SessionMessage::get('competition-team'));
            }
        }
    }

    public function deleteYourTeam($team)
    {
        if (parent::isObjectSet() && $this->competition->isYourTeam($team->getId())) {
            $delete = $this->competition->deleteYourTeam($team);
            parent::setSessionMessage(
                $delete,
                $this->getMessage('delete-success', stmLang('competition', 'team')),
                $this->getMessage('delete-fail', stmLang('competition', 'team'))
            );
            parent::log(
                'YOUR_TEAM',
                'Delete ' . $team->__toString() . ' (' . $this->competition->__toString() . ')',
                $delete
            );
        } else {
            $this->session->setMessageFail(SessionMessage::get('competition-team'));
        }
    }

    private function getMessage($name, $competition = false)
    {
        $competition = $competition || !parent::isObjectSet() ? $competition : $this->competition->__toString();
        return SessionMessage::get($name, array(stmLang('competition', 'competition'), $competition));
    }
}
