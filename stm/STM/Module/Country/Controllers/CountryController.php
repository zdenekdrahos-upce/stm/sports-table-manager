<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\Country;

use STM\Module\Controller;
use STM\Web\Message\SessionMessage;
use STM\Country\Country;

class CountryController extends Controller
{
    /** @var \Country */
    private $country;
    /** @var \STM\Module\Country\CountryProcessing */
    private $processing;

    protected function __construct()
    {
        parent::__construct();
        parent::setMainPage('index.php?module=country');
        $this->loadSelectedCountry();
        $this->processing = new CountryProcessing($this->country);
    }

    /** index.php?module=country */
    public function index()
    {
        parent::setIndexInLeftMenu();
        parent::setContent(array('countries' => $this->entities->Country->findAll()), 'index');
    }

    /** index.php?module=country&action=create&type=country */
    public function createCountry()
    {
        if ($this->processing->createCountry()) {
            parent::redirectToIndexPage();
        } else {
            parent::setIndexInLeftMenu();
            parent::setFormErrors($this->processing);
            parent::setContent();
        }
    }

    /** index.php?module=country&action=update&type=name&CTR=ID_COUNTRY */
    public function updateName()
    {
        if ($this->checkSelectedCountry()) {
            if ($this->processing->updateName()) {
                parent::redirectToIndexPage();
            } else {
                parent::setIndexInLeftMenu();
                parent::setFormErrors($this->processing);
                parent::setContent(array('country_name' => $this->country->__toString()));
            }
        }
    }

    /** index.php?module=country&action=delete&type=country&CTR=ID_COUNTRY */
    public function deleteCountry()
    {
        if ($this->checkSelectedCountry()) {
            $this->processing->deleteCountry();
            parent::redirectToIndexPage();
        }
    }

    private function checkSelectedCountry()
    {
        if (!($this->country instanceof Country)) {
            $this->session->setMessageFail(SessionMessage::get('invalid', array(stmLang('club', 'country'))));
            parent::redirectToIndexPage();
        }
        return true;
    }

    private function loadSelectedCountry()
    {
        $this->country = isset($_GET['ctr']) ? $this->entities->Country->findById($_GET['ctr']) : false;
    }
}
