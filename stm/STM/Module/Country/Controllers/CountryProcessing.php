<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\Country;

use STM\Module\ModuleProcessing;
use STM\Web\Message\SessionMessage;
use STM\Country\Country;
use STM\Country\CountryValidator;

class CountryProcessing extends ModuleProcessing
{
    /** @var Country */
    private $country;

    public function __construct($country)
    {
        $this->country = $country instanceof Country ? $country : false;
        parent::__construct($this->country, '\STM\Team\TeamEvent');
        CountryValidator::init($this->formProcessor);
    }

    public function createCountry()
    {
        if (isset($_POST['create'])) {
            unset($_POST['create']);
            $this->formProcessor->escapeValues();
            $result = Country::create($_POST);
            parent::checkAction($result, 'COUNTRY', "New Country: {$_POST['country']}");
            parent::setSessionMessage($result, $this->getMessage('create', $_POST['country']));
            return $result;
        } else {
            $this->formProcessor->initVars(array('country'));
        }
        return false;
    }

    public function deleteCountry()
    {
        if (parent::canBeDeleted()) {
            $delete = $this->country->delete();
            parent::setSessionMessage($delete, $this->getMessage('delete-success'), $this->getMessage('delete-fail'));
            parent::log('COUNTRY', "Delete Country: {$this->country->__toString()}", $delete);
        }
    }

    public function updateName()
    {
        if (parent::isObjectSet() && isset($_POST['update'])) {
            $this->formProcessor->escapeValues();
            $result = $this->country->updateCountryName($_POST['country']);
            parent::checkAction(
                $result,
                'COUNTRY',
                "Update Country Name: {$this->country->__toString()} to {$_POST['country']}"
            );
            parent::setSessionMessage($result, $this->getMessage('change', $_POST['country']));
            return $result;
        } else {
            $_POST['country'] = $this->country->__toString();
        }
        return false;
    }

    private function getMessage($name, $country = false)
    {
        $country = $country || !parent::isObjectSet() ? $country : $this->country->__toString();
        return SessionMessage::get($name, array(stmLang('club', 'country'), $country));
    }
}
