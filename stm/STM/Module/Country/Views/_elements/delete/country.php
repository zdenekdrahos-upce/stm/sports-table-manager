<?php
$url = '?module=country&action=delete&type=country&ctr=' . $country->getId();
$submit_message = stmLang('form', 'delete');
$question = stmLang('country', 'delete-msg') . $country->__toString();
include(STM_ADMIN_TEMPLATE_ROOT . 'form/delete-form.php');
