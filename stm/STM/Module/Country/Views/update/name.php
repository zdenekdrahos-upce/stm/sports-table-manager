<?php
/**
 * Update name of the country  (CountryController::update_name)
 * $country_name        name of the country
 */
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <?php include(STM_MODULES_ROOT . 'Country/Views/_elements/form-parts/country.php'); ?>

    <input type="submit" name="update" value="<?php echo stmLang('form', 'change'); ?>" />
</form>
