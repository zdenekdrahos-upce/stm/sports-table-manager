<?php
/*
 * List of all countries
 * $countries
 */
?>

<?php if (empty($countries)): ?>
    <p><?php echo stmLang('no-content'); ?></p>
<?php else: ?>
    <table>
        <thead>
            <tr>
                <th><?php echo stmLang('name'); ?></th>
                <th><?php echo stmLang('team', 'links'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($countries as $country): ?>
            <tr id="<?php echo $country->getId();?>">
                <td><?php echo $country; ?></td>
                <td>
                    <a href="<?php echo buildUrl(array('action' => 'update', 'type' => 'name', 'ctr' => $country->getId())) ?>">
                        <?php echo stmLang('form', 'change'); ?>
                    </a>,
                    <?php include(STM_MODULES_ROOT . 'Country/Views/_elements/delete/country.php'); ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php endif;
