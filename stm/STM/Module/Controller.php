<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module;

use STM\Session;
use STM\Web\Page\Page;
use STM\Web\Page\PageValidator;
use STM\Web\Message\SessionMessage;
use STM\Web\View\View;
use STM\Web\View\ViewTemplate;
use STM\Web\View\ViewText;
use STM\UserGroupAuthorization;
use STM\Utils\Links;
use STM\Utils\Strings;
use STM\StmFactory;

/**
 * Controller class
 * - abstract class
 * - base class for every module controller
 */
abstract class Controller
{
    /** @var Page */
    protected $page;
    /** @var string */
    private $mainpage;
    /** @var View */
    private $view;
    /** @var View */
    private $leftMenu;
    /** @var Session */
    protected $session;
    /** @var \STM\Entities\StmEntityFactory findFactory */
    protected $entities;

    public static function create($page)
    {
        PageValidator::check($page);
        $className = $page->getControllerName();
        $controller = new $className;
        $controller->page = $page;
        $controller->checkUserCompetence();
        return $controller;
    }

    protected function __construct()
    {
        $this->mainpage = 'index.php';
        $this->view = new View();
        $this->session = Session::getInstance();
        $this->entities = StmFactory::find();
    }

    /** All controllers inherited from this class must contain index method */
    abstract public function index();

    /** Displays view = prints output to user screen */
    public function display()
    {
        $this->view->display();
    }

    /**
     * Sets main content of the page. File is specified by name, the full folder
     * path is determined from url link (modules/MODULE/views/ACTION/TYPE.php)
     * @param array $data
     * @param string $name if $name is empty, then TYPE from URL is used
     */
    protected function setContent($data = array(), $name = '')
    {
        $module = ucfirst($this->page->getIncludedModule());
        $action = $this->page->getAction();
        $filename = Strings::isStringNonEmpty($name) ? $name : $this->page->getType();
        $path = STM_MODULES_ROOT . "{$module}/Views/{$action}/{$filename}.php";
        $this->view->setContent(new ViewTemplate($path, $data));
    }

    protected function setTextContent($text)
    {
        $view_text = new ViewText($text);
        $this->view->setContent($view_text);
    }

    /**
     * Sets errors from FormProcessing instance to view
     * @param ModuleProcessing $processing
     */
    protected function setFormErrors(ModuleProcessing $processing)
    {
        $this->view->setContent($processing->getErrorsForView());
    }

    public function displayLeftMenu()
    {
        if ($this->isSetLeftMenu()) {
            $this->leftMenu->display();
        }
    }

    public function isSetLeftMenu()
    {
        return $this->leftMenu instanceof View;
    }

    protected function setIndexInLeftMenu()
    {
        $this->setLeftMenu('index', array());
    }

    protected function setProfileInLeftMenu($name = 'profile')
    {
        $this->setLeftMenu($name, $this->getProfileLinks());
    }

    /**
     * Set links in right panel
     * @param string $name name of the file folder modules/MODULE/views/_elements/links/$name.php
     */
    private function setLeftMenu($name, $data)
    {
        $this->leftMenu = new View();
        $this->leftMenu->setContent(new ViewTemplate(STM_ADMIN_TEMPLATE_ROOT . 'left-menu.php'));
        $path = STM_MODULES_ROOT . "{$this->page->getIncludedModule()}/Views/_elements/links/{$name}.php";
        $this->leftMenu->setContent(new ViewTemplate($path, $data));
    }

    protected function redirectToIndexPage()
    {
        redirect($this->mainpage);
    }

    public function setMainPage($main_page)
    {
        if (Strings::isStringNonEmpty($main_page)) {
            $this->mainpage = $main_page;
        }
    }

    /**
     * Checks if user has access to selected page
     */
    private function checkUserCompetence()
    {
        $authorization = new UserGroupAuthorization($this->page);
        if (!$authorization->isAuthorized()) {
            $this->session->setMessageFail(SessionMessage::get('authorization'));
            redirect(Links::getPreviousPage('index.php'));
        } elseif ($this->page->getModule() == 'prediction' && STM_ALLOW_PREDICTIONS == false) {
            $this->session->setMessageFail(SessionMessage::get('allow-prediction'));
            redirect(Links::getPreviousPage('index.php'));
        }
    }

    protected function getProfileLinks()
    {
        return array();
    }
}
