<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\Export;

use STM\Module\ModuleController;
use STM\Export\File\ExportTeamsClubs;
use STM\Export\File\ExportCompetitions;
use STM\Export\File\ExportCompetitionMatches;
use STM\Export\File\ExportUsers;
use STM\Export\File\IFileExport;
use STM\Match\MatchSelection;
use STM\Utils\Strings;
use STM\Utils\Dates;
use STM\Utils\Arrays;

class ExportController extends ModuleController
{
    /** @var \STM\Module\Export\ExportProcessing */
    private $processing;

    protected function __construct()
    {
        parent::__construct();
        parent::setMainPage('index.php?module=export');
        $this->processing = new ExportProcessing();
    }

    /** index.php?module=export */
    public function index()
    {
        parent::setIndexInLeftMenu();
        parent::setContent();
    }

    /** index.php?module=export&action=create&type=file_teams */
    public function createFileTeams()
    {
        $this->createFile(new ExportTeamsClubs());
    }

    /** index.php?module=export&action=create&type=file_competitions */
    public function createFileCompetitions()
    {
        $this->createFile(new ExportCompetitions());
    }

    /** index.php?module=export&action=create&type=file_matches */
    public function createFileMatches()
    {
        $competitions = $this->entities->Competition->findAll();
        $data =  array(
            'competitionsFilter' => Arrays::getAssocArray($competitions, 'getId', '__toString')
        );
        $this->createFile(new ExportCompetitionMatches($this->competition), $data);
    }

    /** index.php?module=export&action=create&type=file_users */
    public function createFileUsers()
    {
        $this->createFile(new ExportUsers());
    }

    private function createFile(IFileExport $export, $exportData = array())
    {
        $data = array('export' => $export);
        parent::setIndexInLeftMenu();
        parent::setContent(array_merge($data, $exportData), 'file_export');
    }

    /** index.php?module=export&action=create&type=web_club */
    public function createWebClub()
    {
        $club = $this->entities->Club->findById(isset($_POST['club']) ? $_POST['club'] : false);
        if ($this->processing->exportClub($club)) {
            $this->setWebExport(array('club' => $club), true);
        } else {
            $this->setWebExport(array('clubs' => $this->entities->Club->findAll()), false);
        }
    }

    /** index.php?module=export&action=create&type=web_team */
    public function createWebTeam()
    {
        $team = $this->entities->Team->findById(isset($_POST['team']) ? $_POST['team'] : false);
        if ($this->processing->exportTeam($team)) {
            $this->setWebExport(array('team' => $team), true);
        } else {
            $this->setWebExport(array('teams' => $this->entities->Team->findAll()), false);
        }
    }

    /** index.php?module=export&action=create&type=web_competition */
    public function createWebCompetition()
    {
        $competition = $this->entities->Competition->findById(
            isset($_POST['competition']) ? $_POST['competition'] : false
        );
        if ($this->processing->exportCompetition($competition)) {
            $this->setWebExport(array('competition' => $competition), true);
        } else {
            $this->setWebExport(array('competitions' => $this->entities->Competition->findAll()), false);
        }
    }

    /** index.php?module=export&action=create&type=web_match */
    public function createWebMatch()
    {
        $match = $this->entities->Match->findById(isset($_POST['match']) ? $_POST['match'] : false);
        if ($this->processing->exportMatch($match)) {
            $this->setWebExport(array('match' => $match), true);
        } else {
            $ms = array(
                'matchType' => MatchSelection::ALL_MATCHES,
                'loadScores' => false,
                'loadPeriods' => false,
                'limit' => array('max' => 50),
            );
            $matches = $this->entities->Match->find($ms);
            $this->setWebExport(array('matches' => $this->transformMatchesArray($matches)), false);
        }
    }

    /** index.php?module=export&action=create&type=web_person */
    public function createWebPerson()
    {
        $person = $this->entities->Person->findById(isset($_POST['person']) ? $_POST['person'] : false);
        if ($this->processing->exportPerson($person)) {
            $this->setWebExport(array('person' => $person), true);
        } else {
            $this->setWebExport(array('persons' => $this->entities->Person->findAll()), false);
        }
    }

    /** index.php?module=export&action=create&type=web_web_match_selection */
    public function createWebMatchSelection()
    {
        if ($this->processing->exportMatchSelection()) {
            $this->setWebExport(array('order_array' => $this->getOrderArray()), true);
        } else {
            $this->setWebExport(
                array(
                    'teams' => $this->entities->Team->findAll(),
                    'competitions' => $this->entities->Competition->findAll(),
                ),
                false
            );
        }
    }

    private function setWebExport($data, $display_result)
    {
        parent::setIndexInLeftMenu();
        $filename = $display_result ? 'result' : 'form';
        $path = "{$this->page->getType()}/{$filename}";
        parent::setContent($data, $path);
    }

    private function getOrderArray()
    {
        $order = array();
        foreach ($this->getOrder() as $order_method) {
            $order[$order_method['method']] = $order_method['is_ascending'];
        }
        return $order;
    }

    private function getOrder()
    {
        $methods = array(
            'order_date' => 'orderByDatetime',
            'order_round' => 'orderByRound',
            'order_match_id' => 'orderByIdMatch',
        );
        $order = array();
        foreach ($methods as $post_name => $method) {
            if (isset($_POST[$post_name]) && Strings::isStringNonEmpty($_POST[$post_name])) {
                $order[$_POST[$post_name]] = array(
                    'method' => $method,
                    'is_ascending' => isset($_POST[$post_name . '_ascending']) ? 'true' : 'false',
                );
            }
        }
        uksort($order, 'strcmp');
        return $order;
    }

    private function transformMatchesArray($matches)
    {
        $result = array();
        foreach ($matches as $match) {
            $date = Dates::convertDatetimeToString($match->getDate(), '-');
            $result[$match->getId()] = "{$match->__toString()} ({$date})";
        }
        return $result;
    }

    public static function getExportItems()
    {
        return array(
            'web' =>
                array('web_club', 'web_team', 'web_competition', 'web_match', 'web_person', 'web_match_selection'),
            'database' =>
                array('file_teams', 'file_competitions', 'file_matches', 'file_users'),
        );
    }
}
