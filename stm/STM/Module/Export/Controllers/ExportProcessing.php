<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\Export;

use STM\Module\ModuleProcessing;
use STM\Club\Club;
use STM\Team\Team;
use STM\Competition\Competition;
use STM\Match\Match;
use STM\Person\Person;

class ExportProcessing extends ModuleProcessing
{
    public function __construct()
    {
        parent::__construct(false, '\STM\Export\ExportEvent');
    }

    public function exportClub($club)
    {
        return isset($_POST['create']) ? $club instanceof Club : false;
    }

    public function exportTeam($team)
    {
        return isset($_POST['create']) ? $team instanceof Team : false;
    }

    public function exportCompetition($competition)
    {
        return isset($_POST['create']) ? $competition instanceof Competition : false;
    }

    public function exportMatch($match)
    {
        return isset($_POST['create']) ? $match instanceof Match : false;
    }

    public function exportPerson($person)
    {
        return isset($_POST['create']) ? $person instanceof Person : false;
    }

    public function exportMatchSelection()
    {
        return isset($_POST['create']);
    }
}
