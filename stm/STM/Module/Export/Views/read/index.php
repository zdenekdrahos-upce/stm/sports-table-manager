<?php  /* Export main page */  ?>

<?php foreach (STM\Module\Export\ExportController::getExportItems() as $name => $types): ?>
<h2><?php echo stmLang('export', $name); ?></h2>
<ul>
    <?php foreach ($types as $type): ?>
    <li><a href="<?php echo buildUrl(array('action' => 'create', 'type' => $type)); ?>"><?php echo stmLang('header', 'export', 'create', $type); ?></a></li>
    <?php endforeach; ?>
</ul>
<?php endforeach;
