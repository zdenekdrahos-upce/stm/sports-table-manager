    <h2><?php echo stmLang('menu', 'export'); ?></h2>
    <ul>
    <?php foreach (STM\Module\Export\ExportController::getExportItems() as $name => $types): ?>
        <li class="header"><?php echo stmLang('export', $name); ?></li>
        <?php foreach ($types as $type): ?>
        <li><a href="<?php echo buildUrl(array('action' => 'create', 'type' => $type, 'c' => null)); ?>"><?php echo stmLang('header', 'export', 'create', $type); ?></a></li>
        <?php endforeach; ?>
    <?php endforeach; ?>
    </ul>
