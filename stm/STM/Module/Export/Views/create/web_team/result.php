<?php
/**
 * Export team - result
 * $team
 */
?>

<code>&lt;?php
// team info
require_once('stm/bootstrap.php');
$team = \STM\StmFactory::find()->Team->findById(<?php echo $team->getId(); ?>);
include(STM_TEMPLATE_ROOT . 'team/team.php');

// last match
$match = \STM\StmFactory::find()->Match->getLastPlayedMatch(null, $team->getId());
include(STM_TEMPLATE_ROOT . 'match/match.php');

// next match
$match = \STM\StmFactory::find()->Match->getFirstUpcomingMatch(null, $team->getId());
include(STM_TEMPLATE_ROOT . 'match/match.php');

// team in competitions
$competitions = \STM\StmFactory::find()->Competition->findByTeam($team);
include(STM_TEMPLATE_ROOT . 'competitions.php');

// team members
$team_members = \STM\StmFactory::find()->TeamMember->findByTeam($team);
include(STM_TEMPLATE_ROOT . 'team/members.php');
?&gt;</code>
