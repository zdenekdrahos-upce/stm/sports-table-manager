<?php
/**
 * Export team - form
 * $teams
 */
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <label for="team"><?php echo stmLang('competition', 'team'); ?>: </label>
    <?php \STM\Web\HTML\Forms::select('team', \STM\Utils\Arrays::getAssocArray($teams, 'getId', '__toString'));  ?>

    <input type="submit" name="create" value="<?php echo stmLang('menu', 'export'); ?>" />
</form>
