<?php
/**
 * Export persons - form
 * $persons
 */
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <label for="person"><?php echo stmLang('team', 'person'); ?>: </label>
    <?php \STM\Web\HTML\Forms::select('person', \STM\Utils\Arrays::getAssocArray($persons, 'getId', '__toString'));  ?>

    <input type="submit" name="create" value="<?php echo stmLang('menu', 'export'); ?>" />
</form>
