<?php
/**
 * Export person - result
 * $person
 */
?>

<code>&lt;?php
// person info
require_once('stm/bootstrap.php');
$myPerson = \STM\StmFactory::find()->Person->findById(<?php echo $person->getId(); ?>);
include(STM_TEMPLATE_ROOT . 'person/person.php');

// person attribute
$myPerson_attributes = \STM\StmFactory::find()->PersonAttribute->findByPerson($myPerson);
include(STM_TEMPLATE_ROOT . 'person/attributes.php');

// member of team
$member_of_teams = \STM\StmFactory::find()->TeamMember->findByPerson($myPerson);
include(STM_TEMPLATE_ROOT . 'person/teams.php');

// member of club
$member_of_clubs = \STM\StmFactory::find()->ClubMember->findByPerson($myPerson);
include(STM_TEMPLATE_ROOT . 'person/clubs.php');

// referee in match
$referee_in_matches = \STM\StmFactory::find()->MatchReferee->findByPerson($myPerson);
include(STM_TEMPLATE_ROOT . 'person/referees.php');

// matches with statistic
$match_player_actions = \STM\StmFactory::find()->MatchPlayerAction->findByPerson($myPerson);
include(STM_TEMPLATE_ROOT . 'person/matches.php');
?&gt;</code>
