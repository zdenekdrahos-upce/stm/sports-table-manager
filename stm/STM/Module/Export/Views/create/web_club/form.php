<?php
/**
 * Export club - form
 * $clubs
 */
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <label for="club"><?php echo stmLang('team', 'club'); ?>: </label>
    <?php \STM\Web\HTML\Forms::select('club', \STM\Utils\Arrays::getAssocArray($clubs, 'getId', '__toString')); ?>

    <input type="submit" name="create" value="<?php echo stmLang('menu', 'export'); ?>" />
</form>
