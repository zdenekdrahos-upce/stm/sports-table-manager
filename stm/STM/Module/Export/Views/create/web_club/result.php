<?php
/**
 * Export club - result
 * $club
 */
?>

<code>&lt;?php
// club info
require_once('stm/bootstrap.php');
$club = \STM\StmFactory::find()->Club->findById(<?php echo $club->getId(); ?>);
include(STM_TEMPLATE_ROOT . 'club/club.php');

// club teams
$teams = \STM\StmFactory::find()->Team->findByClub($club);
include(STM_TEMPLATE_ROOT . 'teams.php');

// club members
$club_members = \STM\StmFactory::find()->ClubMember->findByClub($club);
include(STM_TEMPLATE_ROOT . 'club/members.php');
?&gt;</code>
