<?php
/**
 * Export match - form
 * $matches
 */
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <label for="match"><?php echo stmLang('competition', 'match'); ?>: </label>
    <?php \STM\Web\HTML\Forms::select('match', $matches);  ?>

    <input type="submit" name="create" value="<?php echo stmLang('menu', 'export'); ?>" />
</form>
