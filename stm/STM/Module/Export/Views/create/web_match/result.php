<?php
/**
 * Export match - result
 * $match
 */
?>

<code>&lt;?php
// match info
require_once('stm/bootstrap.php');
$match = \STM\StmFactory::find()->Match->findById(<?php echo $match->getId(); ?>);
include(STM_TEMPLATE_ROOT . 'match/match.php');

// match periods
$match_periods = $match->getPeriods();
include(STM_TEMPLATE_ROOT . 'match/periods.php');

// match detail
$match_detail = \STM\StmFactory::find()->MatchDetail->findById($match->getId());
include(STM_TEMPLATE_ROOT . 'match/detail.php');

// match referees
$match_referees = \STM\StmFactory::find()->MatchReferee->findByMatch($match);
include(STM_TEMPLATE_ROOT . 'match/referees.php');

// team actions
$match_team_actions = \STM\StmFactory::find()->MatchTeamAction->findByMatch($match);
include(STM_TEMPLATE_ROOT . 'match/actions_teams.php');

// player actions
$match_player_actions = \STM\StmFactory::find()->MatchPlayerAction->findByMatch($match);
include(STM_TEMPLATE_ROOT . 'match/actions_players.php');

// lineups
$lineups = new \STM\Match\Lineup\Lineups($match);
$match_lineup = $lineups->getLineupHome();
$is_home = true;
include(STM_TEMPLATE_ROOT . 'match/lineup.php');
$match_lineup = $lineups->getLineupAway();
$is_false = false;
include(STM_TEMPLATE_ROOT . 'match/lineup.php');

// match predictions statistics
$match_predictions_statistics = \STM\StmFactory::find()->PredictionStats->findByMatch($match);
include(STM_TEMPLATE_ROOT . 'match/predictions_statistics.php');
?&gt;</code>
