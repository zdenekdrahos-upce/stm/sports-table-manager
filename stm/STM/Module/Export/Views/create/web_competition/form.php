<?php
/**
 * Export competition - form
 * $competition
 */
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <label for="competition"><?php echo stmLang('competition', 'competition'); ?>: </label>
    <?php \STM\Web\HTML\Forms::select('competition', \STM\Utils\Arrays::getAssocArray($competitions, 'getId', '__toString'));  ?>

    <input type="submit" name="create" value="<?php echo stmLang('menu', 'export'); ?>" />
</form>
