<?php
/**
 * Export competition - result
 * $competition
 */
use STM\Competition\Season\Season;
use STM\Competition\Playoff\Playoff;
?>

<code>&lt;?php
// competition info
require_once('stm/bootstrap.php');
$competition = \STM\StmFactory::find()->Competition->findById(<?php echo $competition->getId(); ?>);
include(STM_TEMPLATE_ROOT . 'competition/competition.php');

// teams from competition
$teams = $competition->getTeams();
include(STM_TEMPLATE_ROOT . 'teams.php');

<?php if (!($competition instanceof Playoff)): ?>
// matches
$matchSelection = array(
    'matchType' => \STM\Match\MatchSelection::ALL_MATCHES,
    'loadScores' => true,
    'loadPeriods' => false,
    'competition' => $competition,
);  
$matches = \STM\StmFactory::find()->Match->find($matchSelection);
include(STM_TEMPLATE_ROOT . 'matches.php');
<?php if ($competition instanceof Season): ?>

// season tables
$seasonTable = \STM\StmCache::getSeasonTable($competition);
$your_teams = $competition->getIdOfYourTeams();
$table = $seasonTable->getTableFull();
include(STM_TEMPLATE_ROOT . 'competition/table_big.php');
$table = $seasonTable->getTableHome();
include(STM_TEMPLATE_ROOT . 'competition/table_small.php');
$table = $seasonTable->getTableAway();
include(STM_TEMPLATE_ROOT . 'competition/table_small.php');

// cross table
$cross_table = \STM\Match\Table\Cross\CrossTableFactory::getSeasonCrossTable($competition);
include(STM_TEMPLATE_ROOT . 'competition/season_cross_table.php');

// season positions of teams
$season_teams_positions = \STM\StmCache::getTeamsPositionsInTable($competition);
include(STM_TEMPLATE_ROOT . 'competition/season_positions.php');
<?php endif; ?>
<?php else: ?>
// playoff series
// $series = \STM\StmFactory::find()->PlayoffSerie->findByPlayoffRound($competition, 1);
$series = \STM\StmFactory::find()->PlayoffSerie->findByPlayoff($competition);
include(STM_TEMPLATE_ROOT . 'competition/playoff_series.php');

// matches from playoff serie
$serie = \STM\StmFactory::find()->PlayoffSerie->findById(1);
$matchSelection = array(
    'matchType' => \STM\Match\MatchSelection::ALL_MATCHES,
    'loadScores' => true,
    'loadPeriods' => false,
    'playoffSerie' => $serie,
);  
$matches = \STM\StmFactory::find()->Match->find($matchSelection);
include(STM_TEMPLATE_ROOT . 'matches.php');

// playoff tree
$builder = new \STM\Competition\Playoff\Tree\PlayoffTreeBuilder($competition);
$adapter = new \STM\Competition\Playoff\Tree\PlayoffTreeAdapter($builder);
$rounds = $adapter->getRounds();
include(STM_TEMPLATE_ROOT . 'competition/playoff_tree.php');
<?php endif; ?>

// other competitions from category
$category = $competition->getIdCategory();
$competitions = \STM\StmFactory::find()->Competition->findByCategory($category);
include(STM_TEMPLATE_ROOT . 'competitions.php');
?&gt;</code>
