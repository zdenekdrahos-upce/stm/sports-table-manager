<?php
/**
 * Export to file
 * $export \STM\Export\File\IFileExport
 */
use STM\Export\File\ExportCompetitionMatches;
?>

<?php 
if ($export instanceof ExportCompetitionMatches) {
    $fields = array(
        'c' => array(
            'name' => stmLang('competition', 'competition'),
            'values' => $competitionsFilter
        ),
    );
    include(STM_ADMIN_TEMPLATE_ROOT . 'form/filtering-form.php');
}
?>

<ul>
    <li><?php echo stmLang('import', 'format'); ?>: <strong><?php echo $export->getFormat(); ?></strong></li>
</ul>

<code readonly="true"><?php
echo $export->getData();
?></code>
