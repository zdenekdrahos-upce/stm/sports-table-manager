<?php
/**
 * Export match selection - form
 * $teams
 * $competitions
 */
$col_count = 5;
$actual_col_count = 0;
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">

    <label for="match_type" class="inline"><?php echo stmLang('match', 'filter', 'match-selection'); ?>:</label>
    <?php
    $options = array('A' => 'all-matches', 'P' => 'played-matches', 'U' => 'upcoming-matches');
    foreach ($options as $key => $value) {
        $options[$key] = stmLang('match', 'filter', $value);
    }
    \STM\Web\HTML\Forms::select('match_type', $options, false, 'inline');
    ?><br />

    <label for="load_score" class="inline"><?php echo stmLang('match', 'filter', 'load-scores'); ?>:</label>
    <input type="checkbox" id="load_score" name="load_score" class="inline" <?php echo (isset($_POST['load_score'])) ? 'checked="checked"' : ''; ?> /><br />
    <label for="load_periods" class="inline"><?php echo stmLang('match', 'filter', 'load-periods'); ?>:</label>
    <input type="checkbox" id="load_periods" name="load_periods" class="inline" <?php echo (isset($_POST['load_periods'])) ? 'checked="checked"' : ''; ?> /><br />

    <label for="date_start" class="inline"><?php echo stmLang('competition', 'date-start'); ?>: </label>
    <?php echo \STM\Web\HTML\Forms::inputDate('date_start'); ?>
    <label for="date_end" class="inline"><?php echo stmLang('competition', 'date-end'); ?>: </label>
    <?php echo \STM\Web\HTML\Forms::inputDate('date_end'); ?>

    <h3><?php echo stmLang('menu', 'competitions'); ?></h3>
    <label for="round" class="inline"><?php echo stmLang('match', 'season-round'); ?>:</label>
    <?php  \STM\Web\HTML\Forms::select('round', \STM\Utils\Arrays::getNumberArray(1, 50), true, 'inline'); ?><br />
    <label for="max_round" class="inline">Max <?php echo stmLang('match', 'season-round'); ?>:</label>
    <?php \STM\Web\HTML\Forms::select('max_round', \STM\Utils\Arrays::getNumberArray(1, 50), true, 'inline'); ?><br />
    <label for="id_competition" class="inline"><?php echo stmLang('competition', 'competition'); ?>:</label>
    <?php \STM\Web\HTML\Forms::select('id_competition', \STM\Utils\Arrays::getAssocArray($competitions, 'getId', '__toString'), true, 'inline'); ?><br />


    <h3><?php echo stmLang('menu', 'teams'); ?></h3>
    <input type="checkbox" id="head_to_head" name="head_to_head" class="inline" <?php echo (isset($_POST['head_to_head'])) ? 'checked="checked"' : ''; ?> />
    <label for="head_to_head" class="inline"><?php echo stmLang('match', 'filter', 'head-to-head'); ?>:</label><br />
    <table>
    <?php foreach ($teams as $team): ?>
        <?php if($actual_col_count++ % $col_count == 0): ?><tr><?php endif; ?>
            <td class="checkbox"><input type="checkbox" id="<?php echo $team->getId(); ?>" name="teams[<?php echo $team->getId(); ?>]" class="inline" <?php echo (isset($_POST['teams'][$team->getId()])) ? 'checked="checked"' : ''; ?> /></td>
            <td class="left"><label for="<?php echo $team->getId(); ?>" class="inline"><?php echo $team; ?></label></td>
        <?php if($actual_col_count % $col_count == 0 || $team == end($teams)): ?></tr><?php endif; ?>
    <?php endforeach; ?>
    </table>

    <h3><?php echo stmLang('export', 'order-priority'); ?></h3>
    <label for="order_date" class="inline"><?php echo stmLang('export', 'order-datetime'); ?>:</label>
    <?php  \STM\Web\HTML\Forms::select('order_date', \STM\Utils\Arrays::getNumberArray(1, 3), true, 'inline'); ?>
    <input type="checkbox" id="order_date_ascending" name="order_date_ascending" class="inline" <?php echo (isset($_POST['order_date_ascending'])) ? 'checked="checked"' : ''; ?> />
    <label for="order_date_ascending" class="inline"><?php echo stmLang('export', 'order-is-ascending'); ?></label><br />

    <label for="order_round" class="inline"><?php echo stmLang('export', 'order-round'); ?>:</label>
    <?php  \STM\Web\HTML\Forms::select('order_round', \STM\Utils\Arrays::getNumberArray(1, 3), true, 'inline'); ?>
    <input type="checkbox" id="order_round_ascending" name="order_round_ascending" class="inline" <?php echo (isset($_POST['order_round_ascending'])) ? 'checked="checked"' : ''; ?> />
    <label for="order_round_ascending" class="inline"><?php echo stmLang('export', 'order-is-ascending'); ?></label><br />

    <label for="order_match_id" class="inline"><?php echo stmLang('export', 'order-match-id'); ?>:</label>
    <?php  \STM\Web\HTML\Forms::select('order_match_id', \STM\Utils\Arrays::getNumberArray(1, 3), true, 'inline'); ?>
    <input type="checkbox" id="order_match_id_ascending" name="order_match_id_ascending" class="inline" <?php echo (isset($_POST['order_match_id_ascending'])) ? 'checked="checked"' : ''; ?> />
    <label for="order_match_id_ascending" class="inline"><?php echo stmLang('export', 'order-is-ascending'); ?></label><br />

    <h3><?php echo stmLang('menu', 'other'); ?></h3>
    <label for="limit_max" class="inline"><?php echo stmLang('export', 'limit-max'); ?>:</label>
    <?php  \STM\Web\HTML\Forms::select('limit_max', \STM\Utils\Arrays::getNumberArray(1, 50), true, 'inline'); ?><br />
    <label for="limit_offset" class="inline"><?php echo stmLang('export', 'limit-offset'); ?>:</label>
    <?php  \STM\Web\HTML\Forms::select('limit_offset', \STM\Utils\Arrays::getNumberArray(1, 50), true, 'inline'); ?><br />


    <input type="submit" name="create" value="<?php echo stmLang('menu', 'export'); ?>" />
</form>
