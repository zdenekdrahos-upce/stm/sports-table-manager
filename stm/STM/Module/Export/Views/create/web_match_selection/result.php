<?php
/**
 * Export match selection - result
 * $order_array
 */
use STM\Match\Filter\MatchFilter;

$matchType = MatchFilter::getMatchType($_POST['match_type']);
$load_score = isset($_POST['load_score']) ? 'true' : 'false';
$loadPeriods = isset($_POST['load_score']) ? 'true' : 'false';
?>

<code>&lt;?php
// match selection
require_once('stm/bootstrap.php');
$match_selection = array(
    'matchType' => \STM\Match\MatchSelection::<?php echo $matchType; ?>,
    'loadScores' => <?php echo $load_score; ?>,
    'loadPeriods' => <?php echo $loadPeriods; ?>,
<?php if ((isset($_POST['date_start']) && \STM\Utils\Strings::isStringNonEmpty($_POST['date_end'])) || (isset($_POST['date_start']) && \STM\Utils\Strings::isStringNonEmpty($_POST['date_start']))):
    $date_start = isset($_POST['date_start']) ? "'{$_POST['date_start']}'" : 'null';
    $date_end = isset($_POST['date_end']) ? "'{$_POST['date_end']}'" : 'null';
?>
    'dates' => array('min' => <?php echo $date_start; ?>, 'max' => <?php echo $date_end; ?>),
<?php endif; ?>
<?php if (isset($_POST['round']) && \STM\Utils\Strings::isStringNonEmpty($_POST['round'])): ?>
    'seasonRound' => <?php echo $_POST['round']; ?>,
<?php endif; ?>
<?php if (isset($_POST['max_round']) && \STM\Utils\Strings::isStringNonEmpty($_POST['max_round'])): ?>
    'seasonMaxRound' => <?php echo $_POST['max_round']; ?>,
<?php endif; ?>
<?php if (isset($_POST['id_competition']) && \STM\Utils\Strings::isStringNonEmpty($_POST['id_competition'])): ?>
    'competition' => <?php echo $_POST['id_competition']; ?>,
<?php endif; ?>
<?php if (isset($_POST['teams'])):
    $isHeadToHead = isset($_POST['head_to_head']) ? 'true' : 'false';
    $teams = implode(', ', array_keys($_POST['teams']));
?>
    'teams' => array(<?php echo $teams; ?>),
    'headToHead' => <?php echo $isHeadToHead; ?>,
<?php endif; ?>
<?php if (isset($_POST['limit_max']) && \STM\Utils\Strings::isStringNonEmpty($_POST['limit_max'])):
    $max = (int) $_POST['limit_max'];
    $offset = (int) isset($_POST['limit_offset']) && \STM\Utils\Strings::isStringNonEmpty($_POST['limit_offset']) ? $_POST['limit_offset'] : 0;
?>
    'limit' => array('max' => <?php echo $max; ?>, 'offset' => <?php echo $offset; ?>),
<?php endif; ?>
<?php if ($order_array): ?>
    'order' => array(
<?php foreach ($order_array as $method => $is_ascending): ?>
        <?php echo "'{$method}' => {$is_ascending}"; ?>,
<?php endforeach; ?>
    )
<?php endif; ?>
);
    
// matches
$matches = \STM\StmFactory::find()->Match->find($match_selection);
include(STM_TEMPLATE_ROOT . 'matches.php');

// result tables (like in season, but LIMIT is NOT applied)
$result_table = \STM\Match\Table\ResultTable::getTable($match_selection, new \STM\Match\Table\TablePoints(), STM_MATCH_PERIODS);
$table = $result_table->getTableFull();
include(STM_TEMPLATE_ROOT . 'competition/table_big.php');

// predictions statistics
$match_predictions_statistics = \STM\StmFactory::find()->PredictionStats->find($match_selection);
include(STM_TEMPLATE_ROOT . 'match/predictions_statistics.php');

// prediction chart
$calculator = new \STM\Prediction\Chart\PredictionCalculator();
$predictions_chart = \STM\StmFactory::find()->PredictionChart->find($match_selection, $calculator);
include(STM_TEMPLATE_ROOT . 'match/predictions_chart.php');

// statistics of team in selected matches - second parameter = ID TEAM
$statistics = \STM\Match\Stats\TeamMatchesStatsFactory::loadFromDatabase($match_selection, 1);
include(STM_TEMPLATE_ROOT . 'statistics/team.php');
?&gt;</code>