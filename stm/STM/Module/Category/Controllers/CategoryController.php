<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\Category;

use STM\Module\Controller;
use STM\Web\Message\SessionMessage;
use STM\Competition\Category\CompetitionCategory;

class CategoryController extends Controller
{
    /** @var \CompetitionCategory */
    private $category;
    /** @var \STM\Module\Category\CompetitionCategoryProcessing */
    private $processing;

    protected function __construct()
    {
        parent::__construct();
        parent::setMainPage('index.php?module=category');
        $this->loadSelectedCategory();
        $this->processing = new CategoryProcessing($this->category);
    }

    /** index.php?module=category */
    public function index()
    {
        parent::setIndexInLeftMenu();
        parent::setContent(array('categories' => $this->entities->CompetitionCategory->findAll()));
    }

    /** index.php?module=category&action=create&type=category */
    public function createCategory()
    {
        if ($this->processing->createCategory()) {
            parent::redirectToIndexPage();
        } else {
            parent::setIndexInLeftMenu();
            parent::setFormErrors($this->processing);
            parent::setContent(array('categories' => $this->entities->CompetitionCategory->findAll()));
        }
    }

    /** index.php?module=category&action=read&type=category&cat=ID_CATEGORY */
    public function readCategory()
    {
        if ($this->checkSelectedCategory()) {
            parent::setProfileInLeftMenu();
            parent::setContent(
                array(
                    'category' => $this->category,
                    'parent' => $this->entities->CompetitionCategory->findById($this->category->getIdParent()),
                    'descendants' => $this->entities->CompetitionCategory->findSubcategories($this->category),
                    'members' => $this->entities->Competition->findByCategory($this->category),
                )
            );
        }
    }

    /** index.php?module=category&action=update&type=category&cat=ID_CATEGORY */
    public function updateCategory()
    {
        if ($this->checkSelectedCategory()) {
            if ($this->processing->updateCategory()) {
                redirect(buildUrl(array('action' => 'read')));
            } else {
                parent::setProfileInLeftMenu();
                parent::setFormErrors($this->processing);
                parent::setContent(array('categories' => $this->entities->CompetitionCategory->findAll()));
            }
        }
    }

    /** index.php?module=category&action=delete&type=category&cat=ID_CATEGORY */
    public function deleteCategory()
    {
        if ($this->checkSelectedCategory()) {
            $this->processing->deleteCategory();
            parent::redirectToIndexPage();
        }
    }

    /**
     * @return boolean
     * Returns true if template is selected and exists. If template doesn't exist or
     * one template isn't selected, then user is redirected to index page
     */
    private function checkSelectedCategory()
    {
        if (!($this->category instanceof CompetitionCategory)) {
            $this->session->setMessageFail(SessionMessage::get('invalid', array(stmLang('category', 'category'))));
            parent::redirectToIndexPage();
        }
        return true;
    }

    private function loadSelectedCategory()
    {
        $this->category = isset($_GET['cat']) ? $this->entities->CompetitionCategory->findById($_GET['cat']) : false;
    }

    protected function getProfileLinks()
    {
        return array('category_name' => $this->category->getName());
    }
}
