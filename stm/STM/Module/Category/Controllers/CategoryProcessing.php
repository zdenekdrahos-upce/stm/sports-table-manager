<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\Category;

use STM\Module\ModuleProcessing;
use STM\Web\Message\SessionMessage;
use STM\Competition\Category\CompetitionCategory;
use STM\Competition\Category\CompetitionCategoryValidator;

class CategoryProcessing extends ModuleProcessing
{
    /** @var \CompetitionCategory */
    private $category;

    public function __construct($category)
    {
        $this->category = $category instanceof CompetitionCategory ? $category : false;
        parent::__construct($this->category, '\STM\Competition\CompetitionEvent');
        CompetitionCategoryValidator::init($this->formProcessor);
    }

    public function createCategory()
    {
        if (isset($_POST['create'])) {
            unset($_POST['create']);
            if (!is_numeric($_POST['id_parent_category'])) {
                $_POST['id_parent_category'] = null;
            }
            $this->formProcessor->escapeValues();
            $result = CompetitionCategory::create($_POST);
            parent::checkAction($result, 'CATEGORY', "New Category: {$_POST['name']}");
            parent::setSessionMessage($result, $this->getMessage('create', $_POST['name']));
            return $result;
        } else {
            $this->formProcessor->initVars(array('name', 'description', 'id_parent_category'));
        }
        return false;
    }

    public function deleteCategory()
    {
        if (parent::canBeDeleted()) {
            $delete = $this->category->delete();
            parent::setSessionMessage($delete, $this->getMessage('delete-success'), $this->getMessage('delete-fail'));
            parent::log('CATEGORY', "Delete Category: {$this->category->__toString()}", $delete);
        }
    }

    public function updateCategory()
    {
        if (parent::isObjectSet() && isset($_POST['update'])) {
            unset($_POST['update']);
            if (!is_numeric($_POST['id_parent_category'])) {
                $_POST['id_parent_category'] = null;
            }
            $this->formProcessor->escapeValues();
            $result = $this->category->update($_POST);
            parent::checkAction(
                $result,
                'CATEGORY',
                "Update category: {$_POST['name']} ({$this->category->__toString()})"
            );
            parent::setSessionMessage($result, $this->getMessage('change'));
            return $result;
        } else {
            $_POST['name'] = $this->category->getName();
            $_POST['description'] = $this->category->getDescription();
            $_POST['id_parent_category'] = $this->category->getIdParent();
        }
        return false;
    }

    private function getMessage($name, $category = false)
    {
        $category = $category || !parent::isObjectSet() ? $category : $this->category->__toString();
        return SessionMessage::get($name, array(stmLang('category', 'category'), $category));
    }
}
