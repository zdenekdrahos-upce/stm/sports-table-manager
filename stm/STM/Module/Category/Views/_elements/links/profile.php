    <h2><a href="<?php echo buildUrl(array('action' => 'read', 'type' => 'category')) ?>"><?php echo $category_name; ?></a></h2>
    <ul>
        <li><a href="<?php echo buildUrl(array('action' => 'update', 'type' => 'category')) ?>"><?php echo stmLang('header', 'category', 'update', 'category'); ?></a></li>
        <li><?php include(STM_MODULES_ROOT . 'Category/Views/_elements/delete/category.php'); ?></li>
    </ul>
