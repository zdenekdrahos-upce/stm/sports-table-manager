    <label for="name"><?php echo stmLang('name'); ?>: </label>
    <input type="text" id="name" name="name" size="40" maxlength="50" value="<?php echo $_POST['name']; ?>" />
    <label for="description"><?php echo stmLang('category', 'description'); ?>: </label>
    <textarea id="description" name="description" cols="32" rows="6"><?php echo $_POST['description']; ?></textarea>
    <label for="id_parent_category"><?php echo stmLang('category', 'parent'); ?>: </label>
    <?php \STM\Web\HTML\Forms::select('id_parent_category', \STM\Utils\Arrays::getAssocArray($categories, 'getId', '__toString'), true); ?>
