<?php
/*
 * List of all categories
 * $categories
 */
?>

<?php if (empty($categories)): ?>
    <p><?php echo stmLang('no-content'); ?></p>
<?php else: ?>
    <script type="text/javascript" src="web/scripts/jquery.tablesorter.min.js"></script>
    <script type="text/javascript">
    $(function() {
        $("#tablesorter").tablesorter();
    });
    </script>

    <table id="tablesorter">
        <thead>
            <tr>
                <th><a href=""><?php echo stmLang('category', 'category'); ?></a></th>
                <th><a href=""><?php echo stmLang('category', 'descendants-count'); ?></a></th>
                <th><a href=""><?php echo stmLang('category', 'competitions-count'); ?></a></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($categories as $category): ?>
            <tr>
                <td><a href="<?php echo buildUrl(array('type' => 'category', 'action' => 'read', 'cat' => $category->getId())); ?>" title="<?php echo $category->getDescription(); ?>"><?php echo $category->getName(); ?></a></td>
                <td><?php echo $category->getCountChilds(); ?></td>
                <td><?php echo $category->getCountCompetitions(); ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php endif;
