<?php
/*
 * List of all categories
 * $category        selected category
 * $parent          parent category
 * $descendants     all sub categories
 * $members         all competitions which are members of category
 */
?>

<ul>
    <li><?php echo stmLang('name'); ?>: <strong><?php echo $category->getName(); ?></strong></li>
    <li><?php echo stmLang('category', 'description'); ?>: <strong><?php echo $category->getDescription(); ?></strong></li>
    <li><?php echo stmLang('category', 'parent'); ?>: <strong><?php echo $parent == false ? stmLang('form', 'empty') : ("<a href=\"" . buildUrl(array('cat' => $category->getIdParent())) . "\">{$parent->getName()}</a>"); ?></strong></li>
    <li><?php echo stmLang('category', 'descendants-count'); ?>: <strong><?php echo $category->getCountChilds(); ?></strong></li>
    <li><?php echo stmLang('category', 'competitions-count'); ?>: <strong><?php echo $category->getCountCompetitions(); ?></strong></li>
</ul>

<h2><?php echo stmLang('category', 'descendants'); ?></h2>
<ul>
    <?php if (empty($descendants)): ?>
    <li><?php echo stmLang('no-content'); ?></li>
    <?php else: ?>
    <?php foreach ($descendants as $child): ?>
    <li><a href="<?php echo buildUrl(array('cat' => $child->getId())); ?>"><?php echo $child->getName(); ?></a></li>
    <?php endforeach; ?>
    <?php endif; ?>
</ul>

<h2><?php echo stmLang('category', 'competitions'); ?></h2>
<ul>
    <?php if (empty($members)): ?>
    <li><?php echo stmLang('no-content'); ?></li>
    <?php else: ?>
    <?php foreach ($members as $competition): ?>
    <li><a href="<?php echo buildUrl(array('module' => 'competition', 'type' => 'competition', 'cat' => null, 'c' => $competition->getId())); ?>"><?php echo $competition->__toString(); ?></a></li>
    <?php endforeach; ?>
    <?php endif; ?>
</ul>
