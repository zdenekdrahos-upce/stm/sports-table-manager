<?php
/**
 * Create new category
 * $categories      used for select parent category
 */
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <?php include(STM_MODULES_ROOT . 'Category/Views/_elements/form-parts/category.php'); ?>

    <input type="submit" name="create" value="<?php echo stmLang('form', 'create'); ?>" />

</form>
