<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module;

use STM\Competition\CompetitionStatus;
use STM\Competition\CompetitionSelection;
use STM\User\User;
use STM\User\UserGroup;
use STM\Web\Message\SessionMessage;

/**
 * ModuleController class
 * - abstract class
 * - contains loading base STM objects from url link
 *       - t    team
 *       - c    competition
 *       - m    match
 *       - loading of loggedUser (session) is optionally
 */
abstract class ModuleController extends Controller
{
    /** @var \STM\Team\Team */
    protected $team;
    /** @var Competition */
    protected $competition;
    /** @var Match */
    protected $match;
    /** @var User */
    protected $loggedUser;

    /**
     * @param boolean $load_user if data of logged user are loaded
     */
    protected function __construct($load_user = false)
    {
        parent::__construct();
        $this->loadSelectedTeam();
        $this->loadSelectedMatch();
        $this->loadSelectedCompetition();
        if ($load_user) {
            $this->loadLoggedUser();
        }
    }

    /**
     * Checks if url parameter 't' is valid id of the team
     * @param boolean $redirect
     * @return boolean
     * Returns true if required object is selected, otherwise if returns false or
     * redirects to index page with error message in session depends on parameter $redirect
     */
    protected function checkSelectedTeam($redirect = true)
    {
        return $this->check('team', $redirect);
    }

    /**
     * Checks if url parameter 'c' is valid id of the competition
     * @param boolean $redirect
     * @return boolean
     * Returns true if required object is selected, otherwise if returns false or
     * redirects to index page with error message in session depends on parameter $redirect
     */
    protected function checkSelectedCompetition($redirect = true)
    {
        $this->checkHiddenCompetitions();
        return $this->check('competition', $redirect);
    }

    /**
     * Checks if url parameter 'm' is valid id of the match
     * @param boolean $redirect
     * @return boolean
     * Returns true if required object is selected, otherwise if returns false or
     * redirects to index page with error message in session depends on parameter $redirect
     */
    protected function checkSelectedMatch($redirect = true)
    {
        return $this->check('match', $redirect);
    }

    protected function checkLoggedUser()
    {
        if (!$this->loggedUser) {
            $this->session->setMessageFail(SessionMessage::get('only-logged-users'));
            parent::redirectToIndexPage();
        }
    }

    /**
     * Loads team from link parameter $_GET['t']
     */
    protected function loadSelectedTeam()
    {
        $this->team = isset($_GET['t']) ? $this->entities->Team->findById($_GET['t']) : false;
    }

    /**
     * Loads competition from link parameter $_GET['c']
     */
    protected function loadSelectedCompetition()
    {
        $this->competition = isset($_GET['c']) ? $this->entities->Competition->findById($_GET['c']) : false;
    }

    /**
     * Loads match from link parameter $_GET['m']
     */
    protected function loadSelectedMatch()
    {
        $this->match = isset($_GET['m']) ? $this->entities->Match->findById($_GET['m']) : false;
    }

    /**
     * Loads logged user (id is saved in session)
     */
    protected function loadLoggedUser()
    {
        $user = $this->entities->User->findById($this->session->getUserId());
        $this->loggedUser = $user instanceof User ? $user : false;
    }

    protected function getCompetitionsSelection()
    {
        $competitionSelection = new CompetitionSelection();
        if (!CompetitionStatus::hasAccessToHiddenCompetition($this->session->getUserGroup())) {
            $competitionSelection->setCompetitionStatuses(
                array(CompetitionStatus::PUBLICS, CompetitionStatus::VISIBLE)
            );
        }
        return $competitionSelection;
    }

    /**
     * @param string $attribute
     * @param boolean $redirect
     * @return boolean
     * Returns true if required object is selected, otherwise if returns false or
     * redirects to index page with error message in session depends on parameter $redirect
     */
    private function check($attribute, $redirect)
    {
        if (!$this->$attribute) {
            if ($redirect) {
                $this->session->setMessageFail(
                    SessionMessage::get('invalid', array(stmLang('competition', $attribute)))
                );
                parent::redirectToIndexPage();
            } else {
                return false;
            }
        }
        return true;
    }

    protected function checkHiddenCompetitions()
    {
        if ($this->competition) {
            $user_group = $this->session->getUserGroup();
            $user_group = $user_group ? $user_group : UserGroup::VISITOR;
            if (!CompetitionStatus::hasAccessToCompetition($user_group, $this->competition)) {
                $this->session->setMessageFail(
                    SessionMessage::get('invalid', array(stmLang('competition', 'competition')))
                );
                parent::redirectToIndexPage();
            }
        }
    }
}
