<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\Match;

use STM\Module\ModuleController;
use STM\Web\Message\SessionMessage;
use STM\Match\Filter\MatchFilter;
use STM\Match\Referee\MatchReferee;
use STM\Match\Lineup\MatchPlayer;
use STM\Match\Action\Team\MatchTeamAction;
use STM\Match\Action\Player\MatchPlayerAction;
use STM\Utils\Arrays;
use STM\Match\Lineup\Lineups;
use STM\Competition\Season\Season;
use STM\Competition\Playoff\Playoff;
use STM\Utils\Dates;
use STM\Helpers\Pagination;
use STM\User\UserGroup;
use STM\Competition\Season\Rounds\SeasonRounds;

class MatchController extends ModuleController
{
    /** @var \STM\Module\Match\MatchProcessing */
    private $matchProcessing;
    /** @var \STM\Module\Match\PeriodProcessing */
    private $periodProcessing;
    /** @var \MatchReferee */
    private $referee;
    /** @var \STM\Match\Lineup\Lineups */
    private $lineups;
    /** @var \MatchPlayer */
    private $matchPlayer;
    /** @var \MatchTeamAction */
    private $teamAction;
    /** @var \MatchPlayerAction */
    private $playerAction;
    /** @var \PlayoffSerie */
    private $playoffSerie;

    protected function __construct()
    {
        parent::__construct(true);
        parent::setMainPage('index.php?module=match');
        $this->loadSelectedSerie();
        $this->loadSelectedReferee();
        $this->loadLineupsAndMatchPlayer();
        $this->loadTeamAction();
        $this->loadPlayerAction();
        $this->matchProcessing = new MatchProcessing($this->match);
        $this->periodProcessing = new PeriodProcessing($this->match);
    }

    /** index.php?module=match */
    public function index()
    {
        parent::setIndexInLeftMenu();
        $match_filter = new MatchFilter();
        $match_filter->initPostAndFilter();
        $isValid = $this->matchProcessing->checkFilter($match_filter);
        $data = array(
            'result' => array(),
            'competitions' => $this->entities->Competition->findAll(),
            'teams' => $this->entities->Team->findAll(),
        );
        if ($isValid) {
            $pagination = $this->getPagination($match_filter);
            $match_filter->setPagination($pagination);
            $data['result'] = $match_filter->getResult();
            $data['pagination'] = $pagination;
        }
        parent::setFormErrors($this->matchProcessing);
        parent::setContent($data);
    }

    /** index.php?module=match&action=create&type=match&c=ID_COMPETITION */
    public function createMatch()
    {
        if (parent::checkSelectedCompetition(false)) {
            if ($this->competition->getCountTeams() < 2) {
                $this->session->setMessageFail(SessionMessage::get('match-create'));
                redirect(buildUrl(array('module' => 'competition', 'action' => 'read', 'type' => 'teams')));
            } elseif ($this->matchProcessing->createMatch($this->competition)) {
                if ($this->competition instanceof Playoff) {
                    redirect(buildUrl(array('module' => 'playoff', 'action' => 'read', 'type' => 'serie')));
                }
                redirect(buildUrl(array('module' => 'competition', 'action' => 'read', 'type' => 'matches')));
            } else {
                $data = array(
                    'competition' => $this->competition,
                    'teams' => $this->competition->getTeams(),
                    'max_round' => ($this->competition instanceof Season ? $this->competition->getMaxRound() : false),
                );
                if ($this->competition instanceof Playoff) {
                    if ($this->checkSelectedSerie()) {
                        $data['teams'] = array(
                            $this->entities->Team->findById($this->playoffSerie->getIdTeam1()),
                            $this->entities->Team->findById($this->playoffSerie->getIdTeam2()),
                        );
                    }
                }
                parent::setProfileInLeftMenu('create-match');
                parent::setFormErrors($this->matchProcessing);
                parent::setContent($data);
            }
        } else {
            $this->session->setMessageFail(SessionMessage::get('match-competition'));
            redirect('?module=competition');
        }
    }

    /** index.php?module=match&action=create&type=round_results&c=ID_COMPETITION&r=ROUND */
    public function createRoundResults()
    {
        if (parent::checkSelectedCompetition(false)) {
            $round = isset($_GET['r']) ? $_GET['r'] : false;
            if (is_bool($round)) {
                $this->session->setMessageFail(
                    SessionMessage::get('invalid', array(stmLang('match', 'season-round')))
                );
                redirect(buildUrl(array('module' => 'season', 'action' => 'read', 'type' => 'rounds', 'r' => null)));
            } elseif (!($this->competition instanceof Season)) {
                $this->session->setMessageFail(
                    SessionMessage::get('invalid', array(stmLang('competition', 'season')))
                );
                $url = array('module' => 'competition', 'action' => 'read', 'type' => 'matches', 'r' => null);
                redirect(buildUrl($url));
            } elseif ($this->periodProcessing->createRoundResults()) {
                redirect(buildUrl(array('module' => 'competition', 'action' => 'read', 'type' => 'matches')));
            } else {
                $rounds = new SeasonRounds($this->competition);
                $data = array(
                    'matches' => $this->entities->Match->find($rounds->getMatchSelection($round)),
                    'match_periods' => $this->competition->getNumberOfMatchPeriods()
                );
                parent::setProfileInLeftMenu('create-match');
                parent::setFormErrors($this->periodProcessing);
                parent::setContent($data, 'result_round');
            }
        } else {
            $this->session->setMessageFail(SessionMessage::get('match-competition'));
            redirect('?module=competition');
        }
    }

    /** index.php?module=club&action=create&type=referee&m=ID_MATCH */
    public function createReferee()
    {
        if ($this->checkSelectedMatch()) {
            if ($this->matchProcessing->createReferee()) {
                redirect(buildUrl(array('action' => 'read', 'type' => 'referees')));
            } else {
                $data = array(
                    'persons' => $this->entities->Person->findAll(),
                    'positions' => $this->entities->PersonPosition->findAll(),
                );
                parent::setProfileInLeftMenu();
                parent::setFormErrors($this->matchProcessing);
                parent::setContent($data);
            }
        }
    }

    /** index.php?module=club&action=create&type=lineup_home&m=ID_MATCH */
    public function createLineupHome()
    {
        $this->createLineup('getIdTeamHome');
    }

    /** index.php?module=club&action=create&type=lineup_away&m=ID_MATCH */
    public function createLineupAway()
    {
        $this->createLineup('getIdTeamAway');
    }

    private function createLineup($getter)
    {
        if ($this->checkSelectedMatch()) {
            $id_team = $this->match->$getter();
            $members = $this->getTeamMembersForLineup($id_team);
            $positions = $this->entities->PersonPosition->findAll();
            if ($this->matchProcessing->createMatchPlayer($members, $positions)) {
                redirect(buildUrl(array('action' => 'read')));
            } else {
                $is_home = $getter == 'getIdTeamHome';
                $get_substitution_lineup = $is_home ? 'getLineupHome' : 'getLineupAway';
                $data = array(
                    'is_home' => $is_home,
                    'id_team' => $id_team,
                    'team_members' => $members,
                    'positions' => $positions,
                    'lineup' => $this->lineups,
                    'substitution_lineup' => $this->lineups->$get_substitution_lineup(),
                );
                parent::setProfileInLeftMenu();
                parent::setFormErrors($this->matchProcessing);
                parent::setContent($data, 'lineup');
            }
        }
    }

    private function getTeamMembersForLineup($id_team)
    {
        $team = $this->entities->Team->findById($id_team);
        return $this->entities->TeamMember->findMatchPlayers($team, $this->competition, $this->match);
    }

    /** index.php?module=match&action=create&type=period&m=ID_MATCH */
    public function createPeriod()
    {
        if ($this->checkSelectedMatch()) {
            if ($this->periodProcessing->createPeriod()) {
                redirect(buildUrl(array('action' => 'read', 'type' => 'match')));
            } else {
                parent::setProfileInLeftMenu();
                parent::setFormErrors($this->periodProcessing);
                parent::setContent();
            }
        }
    }

    /** index.php?module=match&action=create&type=result&m=ID_MATCH */
    public function createResult()
    {
        if ($this->checkSelectedMatch()) {
            if ($this->match->hasScore()) {
                redirect(buildUrl(array('action' => 'read', 'type' => 'match')));
            } elseif ($this->periodProcessing->createResult()) {
                redirect(buildUrl(array('action' => 'read', 'type' => 'match')));
            } else {
                parent::setProfileInLeftMenu();
                parent::setFormErrors($this->periodProcessing);
                parent::setContent(array('periods_count' => $this->competition->getNumberOfMatchPeriods()));
            }
        }
    }

    /** index.php?module=match&action=create&type=result_from_action&m=ID_MATCH */
    public function createResultFromAction()
    {
        if ($this->checkSelectedMatch()) {
            if ($this->periodProcessing->createResultFromAction()) {
                redirect(buildUrl(array('action' => 'read', 'type' => 'match')));
            } else {
                parent::setProfileInLeftMenu();
                parent::setFormErrors($this->periodProcessing);
                parent::setContent(
                    array(
                        'actions' => $this->entities->MatchAction->findAll(),
                        'current_score' => $this->match->hasScore() ?
                            ($this->match->getScoreHome() . ':' . $this->match->getScoreAway()) : '',
                    )
                );
            }
        }
    }

    /** index.php?module=match&action=create&type=action_team&m=ID_MATCH */
    public function createActionTeam()
    {
        if ($this->checkSelectedMatch()) {
            $actions = Arrays::getAssocArray($this->entities->MatchAction->findAll(), 'getId');
            if ($this->matchProcessing->createTeamAction($actions)) {
                redirect(buildUrl(array('action' => 'read')));
            } else {
                $data = array(
                    'actions' => $actions,
                    'default_actions_count' => 5,
                );
                parent::setProfileInLeftMenu();
                parent::setFormErrors($this->matchProcessing);
                parent::setContent($data);
            }
        }
    }

    /** index.php?module=match&action=create&type=action_player&m=ID_MATCH */
    public function createActionPlayer()
    {
        if ($this->checkSelectedMatch()) {
            $merged_players = array_merge($this->lineups->getLineupHome(), $this->lineups->getLineupAway());
            $players = Arrays::getAssocArray($merged_players, 'getIdMatchPlayer');
            $actions = Arrays::getAssocArray($this->entities->MatchAction->findAll(), 'getId');
            if ($this->matchProcessing->createPlayerAction($actions, $players)) {
                redirect(buildUrl(array('action' => 'read')));
            } else {
                $data = array(
                    'players' => $players,
                    'actions' => $actions,
                    'default_actions_count' => 5,
                );
                parent::setProfileInLeftMenu();
                parent::setFormErrors($this->matchProcessing);
                parent::setContent($data);
            }
        }
    }

    /** index.php?module=match&action=read&type=match&m=ID_MATCH */
    public function readMatch()
    {
        if ($this->checkSelectedMatch()) {
            $match_array = $this->match->toArray();
            $data = array(
                'competition' => $this->competition,
                'match' => $this->match,
                'team_home' => $match_array['team_home'],
                'team_away' => $match_array['team_away'],
                'score' => $this->match->hasScore() ?
                    ($match_array['score_home'] . ':' . $match_array['score_away']) : '-:-',
                'date' => Dates::convertDatetimeToString($match_array['date']),
                'periods' => $this->match->getPeriods(),
            );
            if ($this->competition instanceof Season) {
                $data['season_round'] = $match_array['round'];
            } elseif ($this->competition instanceof Playoff) {
                $data['id_serie'] = $match_array['id_serie'];
            }
            parent::setProfileInLeftMenu();
            parent::setContent($data);
        }
    }

    /** index.php?module=match&action=read&type=detail&m=ID_MATCH */
    public function readDetail()
    {
        if ($this->checkSelectedMatch()) {
            $data = array(
                'id_match' => $this->match->getId(),
                'match_name' => $this->match->__toString(),
                'detail' => $this->getMatchDetail(),
            );
            parent::setProfileInLeftMenu();
            parent::setContent($data);
        }
    }

    /** index.php?module=match&action=read&type=referees&m=ID_MATCH */
    public function readReferees()
    {
        if ($this->checkSelectedMatch()) {
            $data = array('referees' => $this->entities->MatchReferee->findByMatch($this->match));
            parent::setProfileInLeftMenu();
            parent::setContent($data);
        }
    }

    /** index.php?module=match&action=read&type=action_team&m=ID_MATCH */
    public function readActionTeam()
    {
        if ($this->checkSelectedMatch()) {
            $data = array('actions' => $this->entities->MatchTeamAction->findByMatch($this->match));
            parent::setProfileInLeftMenu();
            parent::setContent($data);
        }
    }

    /** index.php?module=match&action=read&type=action_player&m=ID_MATCH */
    public function readActionPlayer()
    {
        if ($this->checkSelectedMatch()) {
            $data = array('actions' => $this->entities->MatchPlayerAction->findByMatch($this->match));
            parent::setProfileInLeftMenu();
            parent::setContent($data);
        }
    }

    /** index.php?module=match&action=read&type=lineup_home&m=ID_MATCH */
    public function readLineupHome()
    {
        $this->readLineup('getLineupHome');
    }

    /** index.php?module=match&action=read&type=lineup_away&m=ID_MATCH */
    public function readLineupAway()
    {
        $this->readLineup('getLineupAway');
    }

    private function readLineup($getter)
    {
        if ($this->checkSelectedMatch()) {
            $data = array(
                'is_home' => $getter == 'getLineupHome',
                'lineup' => $this->lineups,
                'team_lineup' => $this->lineups->$getter()
            );
            parent::setProfileInLeftMenu();
            parent::setContent($data, 'lineup');
        }
    }

    /** index.php?module=match&action=update&type=match&m=ID_MATCH */
    public function updateMatch()
    {
        $data = array(
            'competition' => $this->competition,
            'teams' => $this->competition->getTeams(),
            'max_round' => ($this->competition instanceof Season ? $this->competition->getMaxRound() : false),
        );
        if ($this->competition instanceof Playoff) {
            $data['teams'] = array(
                $this->entities->Team->findById($this->playoffSerie->getIdTeam1()),
                $this->entities->Team->findById($this->playoffSerie->getIdTeam2()),
            );
        }
        $this->updateAttribute('updateMatch', $data);
    }

    /** index.php?module=match&action=update&type=detail&m=ID_MATCH */
    public function updateDetail()
    {
        if ($this->checkSelectedMatch()) {
            $detail = $this->getMatchDetail();
            if ($this->matchProcessing->updateDetail($detail)) {
                redirect(buildUrl(array('action' => 'read')));
            } else {
                $data = array(
                    'detail' => $detail,
                    'stadiums' => $this->entities->Stadium->findAll(),
                );
                parent::setProfileInLeftMenu();
                parent::setFormErrors($this->matchProcessing);
                parent::setContent($data);
            }
        }
    }

    /** index.php?module=match&action=update&type=referee&m=ID_MATCH */
    public function updateReferee()
    {
        if ($this->checkSelectedReferee()) {
            if ($this->matchProcessing->updateReferee($this->referee)) {
                redirect(buildUrl(array('action' => 'read', 'type' => 'referees')));
            } else {
                $data = array(
                    'referee' => $this->referee,
                    'positions' => $this->entities->PersonPosition->findAll(),
                );
                parent::setProfileInLeftMenu();
                parent::setFormErrors($this->matchProcessing);
                parent::setContent($data);
            }
        }
    }

    /** index.php?module=match&action=update&type=player&m=ID_MATCH&mp=ID_MATCH_PLAYER */
    public function updatePlayer()
    {
        if ($this->checkSelectedPlayer()) {
            $is_from_home_team = $this->matchPlayer->getIdTeam() == $this->match->getIdTeamHome();
            if ($this->matchProcessing->updateMatchPlayer($this->matchPlayer)) {
                $type = $is_from_home_team ? 'lineup_home' : 'lineup_away';
                redirect(buildUrl(array('action' => 'read', 'type' => $type, 'mp' => null)));
            } else {
                $data = array(
                    'player' => $this->matchPlayer,
                    'positions' => $this->entities->PersonPosition->findAll(),
                    'substitution_lineup' => $is_from_home_team ?
                        $this->lineups->getLineupHome() : $this->lineups->getLineupAway()
                );
                parent::setProfileInLeftMenu();
                parent::setFormErrors($this->matchProcessing);
                parent::setContent($data);
            }
        }
    }

    /** index.php?module=match&action=update&type=action_team&m=ID_MATCH&ma=ID_MATCH_ACTION */
    public function updateActionTeam()
    {
        if ($this->checkSelectedTeamAction()) {
            if ($this->matchProcessing->updateTeamAction($this->teamAction)) {
                redirect(buildUrl(array('action' => 'read', 'ma' => null)));
            } else {
                $data = array('team_action' => $this->teamAction);
                parent::setProfileInLeftMenu();
                parent::setFormErrors($this->matchProcessing);
                parent::setContent($data);
            }
        }
    }

    /** index.php?module=match&action=update&type=action_player&m=ID_MATCH */
    public function updateActionPlayer()
    {
        if ($this->checkSelectedPlayerAction()) {
            if ($this->matchProcessing->updatePlayerAction($this->playerAction)) {
                redirect(buildUrl(array('action' => 'read')));
            } else {
                $data = array('match_player_action' => $this->playerAction);
                parent::setProfileInLeftMenu();
                parent::setFormErrors($this->matchProcessing);
                parent::setContent($data);
            }
        }
    }

    /** index.php?module=match&action=update&type=result&m=ID_MATCH */
    public function updateResult()
    {
        if ($this->checkSelectedMatch()) {
            if ($this->periodProcessing->updateResult()) {
                redirect(buildUrl(array('action' => 'read', 'type' => 'match')));
            } else {
                parent::setProfileInLeftMenu();
                parent::setFormErrors($this->periodProcessing);
                parent::setContent(
                    array(
                        'periods_count' => count($this->match->getPeriods())
                    )
                );
            }
        }
    }

    private function updateAttribute($update_method, $template_data)
    {
        if ($this->checkSelectedMatch()) {
            if ($this->matchProcessing->$update_method()) {
                redirect(buildUrl(array('action' => 'read')));
            } else {
                parent::setProfileInLeftMenu();
                parent::setFormErrors($this->matchProcessing);
                parent::setContent($template_data);
            }
        }
    }

    /** index.php?module=match&action=delete&type=match&m=ID_MATCH */
    public function deleteMatch()
    {
        if ($this->checkSelectedMatch()) {
            $this->checkPlayoffBeforeDelete();
            $this->matchProcessing->deleteMatch();
            redirect('?module=competition&action=read&type=competition&c=' . $this->competition->getId());
        }
    }

    /** index.php?module=match&action=delete&type=result&m=ID_MATCH */
    public function deleteResult()
    {
        if ($this->checkSelectedMatch()) {
            $this->periodProcessing->deleteResult();
            redirect(buildUrl(array('action' => 'read', 'type' => 'match')));
        }
    }

    /** index.php?module=match&action=delete&type=detail&m=ID_MATCH */
    public function deleteDetail()
    {
        if ($this->checkSelectedMatch()) {
            $this->matchProcessing->deleteDetail($this->getMatchDetail());
            redirect(buildUrl(array('action' => 'read', 'type' => 'detail')));
        }
    }

    /** index.php?module=match&action=delete&type=referee&m=ID_MATCH */
    public function deleteReferee()
    {
        if ($this->checkSelectedReferee()) {
            $this->matchProcessing->deleteReferee($this->referee);
            redirect(buildUrl(array('action' => 'read', 'type' => 'referees')));
        }
    }

    /** index.php?module=match&action=delete&type=player&m=ID_MATCH&mp=ID_MATCH_PLAYER */
    public function deletePlayer()
    {
        if ($this->checkSelectedPlayer()) {
            $this->matchProcessing->deleteMatchPlayer($this->matchPlayer);
            $type = $this->matchPlayer->getIdTeam() == $this->match->getIdTeamHome() ? 'lineup_home' : 'lineup_away';
            redirect(buildUrl(array('action' => 'read', 'type' => $type, 'mp' => null)));
        }
    }

    /** index.php?module=match&action=delete&type=action_team&m=ID_MATCH&ma=ID_MATCH_ACTION */
    public function deleteActionTeam()
    {
        if ($this->checkSelectedTeamAction()) {
            $this->matchProcessing->deleteTeamAction($this->teamAction);
            redirect(buildUrl(array('action' => 'read', 'ma' => null)));
        }
    }

    /** index.php?module=match&action=delete&type=action_player&m=ID_MATCH */
    public function deleteActionPlayer()
    {
        if ($this->checkSelectedPlayerAction()) {
            $this->matchProcessing->deletePlayerAction($this->playerAction);
            redirect(buildUrl(array('action' => 'read')));
        }
    }

    /** index.php?module=match&action=update&type=period&m=ID_MATCH&p=ID_PERIOD */
    public function deletePeriod()
    {
        if ($this->checkSelectedMatch()) {
            $period = $this->checkPeriod();
            if ($period) {
                $this->periodProcessing->deletePeriod();
                redirect(buildUrl(array('action' => 'read', 'type' => 'match', 'p' => null)));
            }
        }
    }

    private function checkSelectedPlayerAction()
    {
        if ($this->checkSelectedMatch() && $this->playerAction instanceof MatchPlayerAction) {
            return true;
        } else {
            $this->session->setMessageFail(SessionMessage::get('invalid', array(stmLang('match', 'player-action'))));
            redirect(buildUrl(array('action' => 'read')));
        }
    }

    private function checkSelectedTeamAction()
    {
        if ($this->checkSelectedMatch() && $this->teamAction instanceof MatchTeamAction) {
            return true;
        } else {
            $this->session->setMessageFail(SessionMessage::get('invalid', array(stmLang('match', 'team-action'))));
            redirect(buildUrl(array('action' => 'read')));
        }
    }

    private function checkSelectedReferee()
    {
        if ($this->checkSelectedMatch() && $this->referee instanceof MatchReferee) {
            return true;
        } else {
            $this->session->setMessageFail(SessionMessage::get('invalid-member'));
            parent::redirectToIndexPage();
        }
    }

    private function checkSelectedPlayer()
    {
        if ($this->checkSelectedMatch() && $this->matchPlayer instanceof MatchPlayer &&
            $this->matchPlayer->getIdMatch() == $this->match->getId()
        ) {
            return true;
        } else {
            $this->session->setMessageFail(SessionMessage::get('invalid-member'));
            parent::redirectToIndexPage();
        }
    }

    private function checkPlayoffBeforeDelete()
    {
        if ($this->competition instanceof Playoff) {
            $playoff_serie = $this->entities->PlayoffSerie->findById($this->match->getIdSerie());
            if (!is_null($playoff_serie->getIdNextSerie())) {
                $this->session->setMessageFail(SessionMessage::get('match-serie'));
                redirect(buildUrl(array('action' => 'read', 'type' => 'match')));
            }
        }
    }

    private function checkSelectedSerie()
    {
        if (!$this->playoffSerie) {
            $this->session->setMessageFail(SessionMessage::get('invalid', array(stmLang('competition', 'serie'))));
            redirect(buildUrl(array('module' => 'playoff', 'action' => 'read', 'type' => 'series', 's' => null)));
        }
        return true;
    }

    private function loadSelectedSerie()
    {
        $this->playoffSerie = isset($_GET['s']) ? $this->entities->PlayoffSerie->findById($_GET['s']) : false;
        if ($this->match) {
            $this->playoffSerie = $this->entities->PlayoffSerie->findById($this->match->getIdSerie());
        }
    }

    /**
     * Used in match period actions where period has to be defined
     * @return Period
     * Returns instance of Period if $_GET['p'] is valid period number,
     * otherwise redirects to match detail page
     */
    private function checkPeriod()
    {
        $match_periods = $this->match->getPeriods();
        if (!isset($_GET['p']) || $_GET['p'] <= 0 || $_GET['p'] > count($match_periods)) {
            $this->session->setMessageFail(SessionMessage::get('invalid', array(stmLang('match', 'period-number'))));
            redirect('index.php?module=match&action=read&type=match&m=' . $this->match->getId());
        }
        return $match_periods[$_GET['p'] - 1];
    }

    // have to check, if match is not from Hidden competition
    protected function checkSelectedMatch($redirect = true)
    {
        if (parent::checkSelectedMatch($redirect)) {
            parent::checkHiddenCompetitions();
        }
        return true;
    }

    private function loadSelectedReferee()
    {
        if (isset($_POST['id_person'])) {
            $person = $this->entities->Person->findById($_POST['id_person']);
            $this->referee = $this->entities->MatchReferee->findById($this->match, $person);
        }
    }

    private function loadTeamAction()
    {
        if (parent::checkSelectedMatch(false) && isset($_GET['ma'])) {
            $action = $this->entities->MatchAction->findById($_GET['ma']);
            $this->teamAction = $this->entities->MatchTeamAction->findById($this->match, $action);
        }
    }

    private function loadPlayerAction()
    {
        if (isset($_POST['l_id_match_player']) && isset($_POST['l_id_match_action']) &&
            isset($_POST['l_minute_action'])
        ) {
            $player = $this->entities->MatchPlayer->findById($_POST['l_id_match_player']);
            $action = $this->entities->MatchAction->findById($_POST['l_id_match_action']);
            $this->playerAction =
                $this->entities->MatchPlayerAction->findById($player, $action, $_POST['l_minute_action']);
        }
    }

    private function loadLineupsAndMatchPlayer()
    {
        if (parent::checkSelectedMatch(false)) {
            $this->lineups = new Lineups($this->match);
            $this->matchPlayer = isset($_GET['mp']) ? $this->entities->MatchPlayer->findById($_GET['mp']) : false;
        }
    }

    /**
     * Notes:
     *   update/delete match or period  - competition from match (getIdCompetition), in link $_GET['m']
     *   create match                   - competition from $_GET['c']
     */
    protected function loadSelectedCompetition()
    {
        parent::loadSelectedCompetition(); // because createMatch need competition, from argument
        if (parent::checkSelectedMatch(false)) {
            $this->competition = $this->entities->Competition->findById($this->match->getIdCompetition());
        }
    }

    /**
     * Checks if logged User is CompetitionManager and if he has authorizations to match module
     * CM has access to read actions all the time, but to create/update/delete only if admin
     * assigned selected competition to him.
     * User is loaded only if his user group is CompetitionManager and action is c/u/d.
     */
    protected function loadLoggedUser()
    {
        if (isset($_GET['action']) && $_GET['action'] != 'read') {
            if ($this->session->getUserGroup() == UserGroup::COMPETITION_MANAGER) {
                parent::loadLoggedUser();
                if (!$this->loggedUser->canUpdateCompetition($this->competition)) {
                    $this->session->setMessageFail(SessionMessage::get('authorization'));
                    redirect('index.php');
                }
            }
        }
    }

    private function getPagination($filter)
    {
        $current_page = isset($_POST['page']) ? $_POST['page'] : 1;
        return new Pagination($filter->getCountMatches(), STM_MATCHES_ON_ONE_PAGE, $current_page);
    }

    private function getMatchDetail()
    {
        return $this->entities->MatchDetail->findById($this->match->getId());
    }

    /**
     * @return array
     */
    protected function getProfileLinks()
    {
        return array(
            'competition' => $this->competition,
            'competition_name' => $this->competition->__toString(),
            'id_competition' => $this->competition->getId(),
            'is_season' => $this->competition instanceof Season,
            'match_name' => $this->match ? $this->match->__toString() : 'New',
            'hasScore' => $this->match ? $this->match->hasScore() : false,
        );
    }
}
