<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\Match;

use STM\Module\ModuleProcessing;
use STM\Web\Message\SessionMessage;
use STM\Match\Match;
use STM\Match\Period\Period;
use STM\Match\Period\PeriodValidator;
use STM\Match\Result\MatchResultCalculator;
use STM\Match\Result\MatchResultCalculatorValidator;
use STM\Match\MatchValidator;

class PeriodProcessing extends ModuleProcessing
{
    /** @var \Match */
    private $match;

    public function __construct($match = false)
    {
        $this->match = $match instanceof Match ? $match : false;
        parent::__construct($this->match, '\STM\Match\MatchEvent');
        PeriodValidator::init($this->formProcessor);
    }

    public function createPeriod()
    {
        if (isset($_POST['create_period']) && parent::isObjectSet()) {
            unset($_POST['create_period']);
            $this->formProcessor->escapeValues();
            $attributes = $_POST;
            $attributes['match'] = $this->match;
            $create = Period::create($attributes);
            parent::checkAction(
                $create,
                'CREATE_PERIOD',
                "{$_POST['score_home']}:{$_POST['score_away']} in {$this->match} ({$this->match->getId()})"
            );
            parent::setSessionMessage($create, $this->getMessage('create', stmLang('match', 'period-number')));
            return $create;
        } else {
            $this->formProcessor->initVars(array('score_home', 'score_away', 'note'));
        }
        return false;
    }

    public function createResult()
    {
        if (isset($_POST['create']) && parent::isObjectSet()) {
            $this->formProcessor->escapeValues();
            $count_created_periods = 0;
            $periods = isset($_POST['periods']) ? $_POST['periods'] : array();
            foreach ($periods as $period) {
                $period['match'] = $this->match;
                if (Period::create($period)) {
                    $count_created_periods++;
                }
            }
            if ($count_created_periods > 0) {
                parent::setSessionMessage(
                    true,
                    SessionMessage::get('match-create-result', array($count_created_periods))
                );
                parent::log(
                    'CREATE_RESULT',
                    "Create '{$count_created_periods}' periods in {$this->match} ({$this->match->getId()})",
                    true
                );
                return true;
            }
        }
        return false;
    }

    public function createResultFromAction()
    {
        if (isset($_POST['create']) && parent::isObjectSet()) {
            MatchResultCalculatorValidator::init($this->formProcessor);
            $calc = new MatchResultCalculator();
            $calc->setMatch($this->match);
            $idAction = isset($_POST['match_action']) ? $_POST['match_action'] : false;
            $match_action = $this->entities->MatchAction->findById($idAction);
            $calculation_method = isset($_POST['calculation']) ? $_POST['calculation'] : STM_STATISTIC_CALCULATION;
            $create = $calc->createNewMatchScore($match_action, $calculation_method);
            $score = $calc->getLastCalculatedScore();
            parent::checkAction(
                $create,
                'CREATE_PERIOD',
                "{$score['home']}:{$score['away']} in {$this->match} ({$this->match->getId()})"
            );
            parent::setSessionMessage($create, $this->getMessage('create', stmLang('match', 'period-number')));
            return $create;
        } else {
            $_POST['calculation'] = STM_STATISTIC_CALCULATION;
        }
        return false;
    }

    public function createRoundResults()
    {
        if (isset($_POST['create'])) {
            MatchValidator::init($this->formProcessor);
            $this->formProcessor->escapeValues();
            $count_matches = 0;
            $id_of_created_matches = array();
            $count_created_periods = 0;
            $matches = isset($_POST['matches']) ? $_POST['matches'] : array();
            foreach ($matches as $id_match => $periods) {
                $match = $this->entities->Match->findById($id_match);
                if ($match) {
                    $id_of_created_matches[] = $id_match;
                    $count_matches++;
                    foreach ($periods as $period) {
                        if ($period['score_home'] !== '' && $period['score_home'] !== '') {
                            $period['match'] = $match;
                            $period['note'] = '';
                            if (Period::create($period)) {
                                $count_created_periods++;
                            }
                        }
                    }
                }
            }
            if ($count_created_periods > 0) {
                parent::setSessionMessage(
                    true,
                    SessionMessage::get('match-create-result', array($count_created_periods))
                );
                $matches = implode(', ', $id_of_created_matches);
                parent::log(
                    'CREATE_RESULT',
                    "Create '{$count_created_periods}' periods in matches: {$matches}",
                    true
                );
                return true;
            }
        }
        return false;
    }

    public function updateResult()
    {
        if (isset($_POST['update']) && parent::isObjectSet()) {
            $this->formProcessor->escapeValues();
            $count_updated_periods = 0;
            $periods = isset($_POST['periods']) ? $_POST['periods'] : array();
            PeriodValidator::checkChangeInPeriods($this->match, $periods);
            if ($this->formProcessor->isValid()) {
                foreach ($this->match->getPeriods() as $period) {
                    if (array_key_exists($period->getIdPeriod(), $periods)) {
                        $new_period_attributes = $periods[$period->getIdPeriod()];
                        if ($period->update($new_period_attributes)) {
                            $count_updated_periods++;
                        }
                    }
                }
            }
            if ($count_updated_periods > 0) {
                parent::setSessionMessage(
                    true,
                    SessionMessage::get('match-update-result', array($count_updated_periods))
                );
                parent::log(
                    'UPDATE_RESULT',
                    "Update '{$count_updated_periods}' periods in {$this->match} ({$this->match->getId()})",
                    true
                );
                return true;
            }
        } elseif ($this->match->hasLoadedPeriods()) {
            foreach ($this->match->getPeriods() as $period) {
                $_POST['periods'][$period->getIdPeriod()] = array(
                    'score_home' => $period->getScoreHome(),
                    'score_away' => $period->getScoreAway(),
                    'note' => $period->getNote(),
                );
            }
        }
        return false;
    }

    public function deleteResult()
    {
        if (parent::canBeDeleted()) {
            if ($this->match->hasPeriods()) {
                $delete = Period::deleteMatchPeriods($this->match);
                parent::setSessionMessage(
                    $delete,
                    $this->getMessage('delete-success', stmLang('match', 'score')),
                    $this->getMessage('delete-fail', stmLang('match', 'score'))
                );
                parent::log(
                    'DELETE_RESULT',
                    "{$this->match->getScoreHome()}:{$this->match->getScoreAway()} ({$this->match->getId()})",
                    $delete
                );
                parent::deleteCacheIfSuccessfulAction($delete, $this->match->getIdCompetition());
            } else {
                $this->session->setMessageFail(SessionMessage::get('match-result'));
            }
        }
    }

    public function deletePeriod()
    {
        if (parent::canBeDeleted()) {
            $period = $this->getEditedPeriod();
            $delete = $period->delete();
            parent::setSessionMessage(
                $delete,
                $this->getMessage('delete-success', stmLang('match', 'period-number')),
                $this->getMessage('delete-fail', stmLang('match', 'period-number'))
            );
            parent::deleteCacheIfSuccessfulAction($delete, $this->match->getIdCompetition());
            parent::log(
                'DELETE_PERIOD',
                "{$period->getScoreHome()}:{$period->getScoreAway()} in {$this->match} ({$this->match->getId()})",
                $delete
            );
        }
    }

    private function getEditedPeriod()
    {
        $periods = $this->match->getPeriods();
        return $periods[$_GET['p'] - 1];
    }

    private function getMessage($name, $match = false)
    {
        $match = $match || !parent::isObjectSet() ? $match : $this->match->__toString();
        return SessionMessage::get($name, array(stmLang('competition', 'match'), $match));
    }
}
