<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\Match;

use STM\Module\ModuleProcessing;
use STM\Web\Message\FormError;
use STM\Web\Message\SessionMessage;
use STM\Match\Match;
use STM\Match\MatchValidator;
use STM\Match\Action\Player\MatchPlayerAction;
use STM\Competition\Competition;
use STM\Match\Referee\MatchReferee;
use STM\Match\Referee\MatchRefereeValidator;
use STM\Match\Lineup\MatchPlayer;
use STM\Match\Lineup\MatchPlayerValidator;
use STM\Match\Action\Team\MatchTeamAction;
use STM\Match\Action\Team\MatchTeamActionValidator;
use STM\Match\Action\Player\MatchPlayerActionValidator;
use STM\Match\Filter\FilterEvent;
use STM\Match\Filter\MatchFilter;
use STM\Match\Filter\MatchFilterValidator;
use STM\Match\Detail\MatchDetail;
use STM\Match\Detail\MatchDetailValidator;
use STM\Utils\Arrays;

class MatchProcessing extends ModuleProcessing
{
    /** @var Match */
    private $match;

    public function __construct($match = false)
    {
        $this->match = $match instanceof Match ? $match : false;
        parent::__construct($this->match, '\STM\Match\MatchEvent');
        MatchValidator::init($this->formProcessor);
        MatchRefereeValidator::init($this->formProcessor);
        MatchPlayerValidator::init($this->formProcessor);
        MatchTeamActionValidator::init($this->formProcessor);
        MatchPlayerActionValidator::init($this->formProcessor);
    }

    public function checkFilter($filter)
    {
        $result = false;
        if ($filter instanceof MatchFilter) {
            $this->formProcessor->escapeValues();
            MatchFilterValidator::init($this->formProcessor);
            MatchFilterValidator::setMatchFilter($filter);
            $result = MatchFilterValidator::check();
            if (!isset($_POST['page']) && MatchFilterValidator::changed()) {
                $event = isset($_POST['result_table']) ? 'TABLE' : 'MATCHES';
                FilterEvent::log(
                    constant('\STM\Match\Filter\FilterEvent::' . $event),
                    "{$filter->getCountMatches()} matches",
                    $result
                );
            }
        }
        return $result;
    }

    public function createMatch($competition)
    {
        if (isset($_POST['create']) && $competition instanceof Competition) {
            $this->formProcessor->escapeValues();
            $attributes = $this->getDataForUpdateMatch();
            $attributes['competition'] = $competition;
            $id_match = Match::create($attributes);
            parent::checkAction(
                $id_match,
                'CREATE',
                "{$attributes['home_team']}:{$attributes['away_team']} ({$id_match})"
            );
            parent::setSessionMessage(
                $id_match,
                $this->getMessage('create', "{$attributes['home_team']}:{$attributes['away_team']}")
            );
            parent::deleteCacheIfSuccessfulAction($id_match, $competition->getId());
            return $id_match;
        } else {
            $this->formProcessor->initVars(array('datetime', 'home_team', 'away_team', 'competition', 'round'));
        }
        return false;
    }

    public function createReferee()
    {
        if (parent::isObjectSet() && isset($_POST['create'])) {
            $this->formProcessor->escapeValues();
            $attributes = array(
                'match' => $this->match,
                'person' => $this->entities->Person->findById($_POST['id_person']),
                'position' => $this->entities->PersonPosition->findById($_POST['id_position']),
            );
            $result = MatchReferee::create($attributes);
            $message = "Add referee {$attributes['person']} [{$attributes['position']}] to ";
            $message .= "{$this->match} ({$this->match->getId()})";
            parent::checkAction($result, 'REFEREES', $message);
            parent::setSessionMessage($result, SessionMessage::get('match-referee-add'));
            return $result;
        } else {
            $this->formProcessor->initVars(array('id_person', 'id_position'));
        }
        return false;
    }

    public function createMatchPlayer($members, $positions)
    {
        if (parent::isObjectSet() && isset($_POST['create'])) {
            $this->formProcessor->escapeValues();
            $_POST['players'] = isset($_POST['players']) ? $_POST['players'] : array();
            $count_all = 0;
            $count_created = 0;
            $members = Arrays::getAssocArray($members, 'getId');
            $positions = Arrays::getAssocArray($positions, 'getId');
            foreach ($_POST['players'] as $player_array) {
                if (array_key_exists('id_team_player', $player_array)) {
                    $this->formProcessor->resetErrors();
                    $count_all++;
                    $attributes = array(
                        'match' => $this->match,
                        'team_player' => isset($player_array['id_team_player']) ?
                            $members[$player_array['id_team_player']] : false,
                        'position' => isset($player_array['id_position']) ?
                            $positions[$player_array['id_position']] : false,
                        'minute_in' => $player_array['minute_in'],
                        'minute_out' => $player_array['minute_out'],
                        'id_substitution_player' => $player_array['id_substitution_player'],
                    );
                    if (MatchPlayer::create($attributes)) {
                        $count_created++;
                    }
                }
            }
            if ($count_all == 0) {
                $this->formProcessor->addError(FormError::get('nothing-checked'));
                return false;
            } else {
                parent::setSessionMessage(
                    true,
                    SessionMessage::get('match-player-create', array($count_created, $count_all))
                );
                parent::log(
                    'PLAYERS',
                    "Create players {$count_created}/{$count_all} in {$this->match} ({$this->match->getId()})",
                    true
                );
                parent::deleteActionCacheIfSuccessfulAction(true, $this->match->getIdCompetition());
                return true;
            }
        }
        return false;
    }

    public function createTeamAction($actions)
    {
        if (parent::isObjectSet() && isset($_POST['create'])) {
            $this->formProcessor->escapeValues();
            $_POST['actions'] = isset($_POST['actions']) ? $_POST['actions'] : array();
            $count_all = 0;
            $count_created = 0;
            foreach ($_POST['actions'] as $action_array) {
                $action_array = array_filter($action_array, 'strlen');
                if (count($action_array) > 0) {
                    $this->formProcessor->resetErrors();
                    $count_all++;
                    $attributes = array(
                        'match' => $this->match,
                        'match_action' => isset($action_array['match_action']) ?
                            $actions[$action_array['match_action']] : false,
                        'value_home' => $action_array['value_home'],
                        'value_away' => $action_array['value_away'],
                    );
                    if (MatchTeamAction::create($attributes)) {
                        $count_created++;
                    }
                }
            }
            if ($count_all == 0) {
                $this->formProcessor->addError(FormError::get('nothing-checked'));
                return false;
            } else {
                parent::log(
                    'MATCH_TEAM_ACTIONS',
                    "Create {$count_created} match actions in {$this->match->__toString()} ({$this->match->getId()})",
                    true
                );
                parent::setSessionMessage(
                    true,
                    SessionMessage::get('match-player-action-create', array($count_created, $count_all))
                );
                return true;
            }
        }
        return false;
    }

    public function createPlayerAction($actions, $players)
    {
        if (parent::isObjectSet() && isset($_POST['create'])) {
            $this->formProcessor->escapeValues();
            $_POST['actions'] = isset($_POST['actions']) ? $_POST['actions'] : array();
            $count_all = 0;
            $count_created = 0;
            foreach ($_POST['actions'] as $action_array) {
                $action_array = array_filter($action_array, 'strlen');
                if (count($action_array) > 0) {
                    $this->formProcessor->resetErrors();
                    $count_all++;
                    $attributes = array(
                        'match_action' => isset($action_array['match_action']) ?
                            $actions[$action_array['match_action']] : false,
                        'match_player' => isset($action_array['match_player']) ?
                            $players[$action_array['match_player']] : false,
                        'minute_action' => $action_array['minute_action'],
                        'action_comment' => '',
                    );
                    if (MatchPlayerAction::create($attributes)) {
                        $count_created++;
                    }
                }
            }
            if ($count_all == 0) {
                $this->formProcessor->addError(FormError::get('nothing-checked'));
                return false;
            } else {
                parent::log(
                    'MATCH_PLAYER_ACTIONS',
                    "Create {$count_created} match actions in {$this->match->__toString()} ({$this->match->getId()})",
                    true
                );
                parent::setSessionMessage(
                    true,
                    SessionMessage::get('match-player-action-create', array($count_created, $count_all))
                );
                parent::deleteActionCacheIfSuccessfulAction(true, $this->match->getIdCompetition());
                return true;
            }
        }
        return false;
    }

    public function updateMatch()
    {
        if (parent::isObjectSet() && isset($_POST['update'])) {
            $this->formProcessor->escapeValues();
            $update = $this->match->update($this->getDataForUpdateMatch());
            parent::checkAction($update, 'UPDATE', "Match {$this->match} ({$this->match->getId()})");
            parent::setSessionMessage($update, $this->getMessage('change'));
            parent::deleteCacheIfSuccessfulAction($update, $this->match->getIdCompetition());
            return $update;
        } else {
            $_POST['datetime'] = $this->match->getDate();
            $_POST['home_team'] = $this->match->getIdTeamHome();
            $_POST['away_team'] = $this->match->getIdTeamAway();
            if ($this->match->getRound()) {
                $_POST['round'] = $this->match->getRound();
            }
        }
        return false;
    }

    public function updateDetail($detail)
    {
        if (isset($_POST['update'])) {
            $this->formProcessor->escapeValues();
            unset($_POST['update']);
            $_POST['match'] = $this->match;
            MatchDetailValidator::init($this->formProcessor);
            $result = false;
            if ($detail instanceof MatchDetail) {
                $result = $detail->update($_POST);
            } else {
                $result = MatchDetail::create($_POST);
            }
            parent::checkAction($result, 'UPDATE_DETAIL', "{$this->match} ({$this->match->getId()})");
            parent::setSessionMessage(
                $result,
                $this->getMessage('change', stmLang('header', 'match', 'read', 'detail'))
            );
            return $result;
        } else {
            if ($detail) {
                $details = $detail->toArray();
                $_POST['spectators'] = $details['spectators'];
                $_POST['id_stadium'] = $detail->getIdStadium();
                $_POST['match_comment'] = $details['match_comment'];
            } else {
                $this->formProcessor->initVars(array('spectators', 'id_stadium', 'match_comment'));
            }
        }
        return false;
    }

    public function updateReferee($referee)
    {
        if (isset($_POST['update']) && $referee instanceof MatchReferee) {
            $this->formProcessor->escapeValues();
            $new_position = $this->entities->PersonPosition->findById($_POST['new_position']);
            $result = $referee->updatePersonPosition($new_position);
            parent::checkAction(
                $result,
                'REFEREES',
                "Update Position of {$referee->__toString()} in {$this->match} ({$this->match->getId()})"
            );
            parent::setSessionMessage($result, $this->getRefereeMessage('change', $referee));
            return $result;
        } else {
            $this->formProcessor->initVars(array('new_position'));
        }
        return false;
    }

    public function updateMatchPlayer($matchPlayer)
    {
        if (isset($_POST['update']) && $matchPlayer instanceof MatchPlayer) {
            unset($_POST['update']);
            $this->formProcessor->escapeValues();
            $_POST['position'] = $this->entities->PersonPosition->findById(
                isset($_POST['id_position']) ? $_POST['id_position'] : null
            );
            unset($_POST['id_position']);
            $result = $matchPlayer->update($_POST);
            parent::checkAction(
                $result,
                'PLAYERS',
                "Update player {$matchPlayer->__toString()} in {$this->match} ({$this->match->getId()})"
            );
            parent::setSessionMessage($result, $this->getPlayerMessage('change', $matchPlayer));
            parent::deleteActionCacheIfSuccessfulAction($result, $this->match->getIdCompetition());
            return $result;
        } else {
            $arr = $matchPlayer->toArray();
            $_POST['id_position'] = $matchPlayer->getIdPosition();
            $_POST['minute_in'] = $arr['minute_in'];
            $_POST['minute_out'] = $arr['minute_out'];
            $_POST['id_substitution_player'] = $matchPlayer->getIdSubstitutionPlayer();
        }
        return false;
    }

    public function updateTeamAction($teamAction)
    {
        if (isset($_POST['update']) && $teamAction instanceof MatchTeamAction) {
            unset($_POST['update']);
            $this->formProcessor->escapeValues();
            $result = $teamAction->update($_POST);
            parent::checkAction(
                $result,
                'MATCH_TEAM_ACTIONS',
                "Update action {$teamAction->__toString()} in {$this->match->__toString()} ({$this->match->getId()})"
            );
            parent::setSessionMessage($result, $this->getTeamActionMessage('change', $teamAction));
            return $result;
        } else {
            $arr = $teamAction->toArray();
            $_POST['value_home'] = $arr['value_home'];
            $_POST['value_away'] = $arr['value_away'];
        }
        return false;
    }

    public function updatePlayerAction($playerAction)
    {
        if (isset($_POST['update']) && $playerAction instanceof MatchPlayerAction) {
            unset($_POST['update']);
            $this->formProcessor->escapeValues();
            $attributes = array();
            $attributes['minute_action'] = $_POST['minute_action'];
            $attributes['action_comment'] = $_POST['action_comment'];
            $result = $playerAction->update($attributes);
            parent::checkAction(
                $result,
                'MATCH_PLAYER_ACTIONS',
                "Update action {$playerAction->__toString()} in {$this->match->__toString()} ({$this->match->getId()})"
            );
            parent::setSessionMessage($result, $this->getPlayerActionMessage('change', $playerAction));
            parent::deleteActionCacheIfSuccessfulAction($result, $this->match->getIdCompetition());
            return $result;
        } else {
            $arr = $playerAction->toArray();
            $_POST['minute_action'] = $arr['minute_action'];
            $_POST['action_comment'] = $arr['action_comment'];
        }
        return false;
    }

    public function deletePlayerAction($playerAction)
    {
        if (parent::canBeDeleted() && $playerAction instanceof MatchPlayerAction) {
            $delete = $playerAction->delete();
            parent::setSessionMessage(
                $delete,
                $this->getPlayerActionMessage('delete-success', $playerAction),
                $this->getPlayerActionMessage('delete-fail', $playerAction)
            );
            parent::log(
                'MATCH_PLAYER_ACTIONS',
                "Delete action {$playerAction} in {$this->match->__toString()} ({$this->match->getId()})",
                $delete
            );
            parent::deleteActionCacheIfSuccessfulAction($delete, $this->match->getIdCompetition());
        }
    }

    public function deleteTeamAction($teamAction)
    {
        if (parent::canBeDeleted() && $teamAction instanceof MatchTeamAction) {
            $delete = $teamAction->delete();
            parent::setSessionMessage(
                $delete,
                $this->getTeamActionMessage('delete-success', $teamAction),
                $this->getTeamActionMessage('delete-fail', $teamAction)
            );
            parent::log(
                'MATCH_TEAM_ACTIONS',
                "Delete action {$teamAction} in {$this->match->__toString()} ({$this->match->getId()})",
                $delete
            );
        }
    }

    public function deleteReferee($referee)
    {
        if (parent::canBeDeleted() && $referee instanceof MatchReferee) {
            $arr = $referee->toArray();
            $delete = $referee->delete();
            parent::setSessionMessage(
                $delete,
                $this->getRefereeMessage('delete-success', $referee),
                $this->getRefereeMessage('delete-fail', $referee)
            );
            $message = "Delete member {$referee->__toString()} [{$arr['position']}] in ";
            $message .= "{$this->match->__toString()} ({$this->match->getId()})";
            parent::log('REFEREES', $message, $delete);
        }
    }

    public function deleteMatchPlayer($matchPlayer)
    {
        if (parent::canBeDeleted() && $matchPlayer instanceof MatchPlayer) {
            $delete = $matchPlayer->delete();
            parent::setSessionMessage(
                $delete,
                $this->getPlayerMessage('delete-success', $matchPlayer),
                $this->getPlayerMessage('delete-fail', $matchPlayer)
            );
            parent::log(
                'PLAYERS',
                "Delete player {$matchPlayer->__toString()} in {$this->match} ({$this->match->getId()})",
                $delete
            );
            parent::deleteActionCacheIfSuccessfulAction($delete, $this->match->getIdCompetition());
        }
    }

    // delete
    public function deleteMatch()
    {
        if (parent::canBeDeleted()) {
            $delete = $this->match->delete();
            parent::setSessionMessage(
                $delete,
                $this->getMessage('delete-success'),
                $this->getMessage('delete-fail')
            );
            parent::log('DELETE', "{$this->match} ({$this->match->getId()})", $delete);
            parent::deleteCacheIfSuccessfulAction($delete, $this->match->getIdCompetition());
        }
    }

    public function deleteDetail($matchDetail)
    {
        if (parent::canBeDeleted()) {
            if ($matchDetail instanceof MatchDetail) {
                $delete = $matchDetail->delete();
                parent::setSessionMessage(
                    $delete,
                    $this->getMessage('delete-success', stmLang('header', 'match', 'read', 'detail')),
                    $this->getMessage('delete-fail', stmLang('header', 'match', 'read', 'detail'))
                );
                parent::log('DELETE_DETAIL', "{$this->match} ({$this->match->getId()})", $delete);
            } else {
                $this->session->setMessageFail(SessionMessage::get('match-detail-notset'));
            }
        }
    }

    private function getDataForUpdateMatch()
    {
        $id_serie = isset($_POST['id_serie']) ? $_POST['id_serie'] : false;
        if ($this->match) {
            $id_serie = $this->match->getIdSerie();
        }
        return array(
            'home_team' => $this->entities->Team->findById($_POST['home_team']),
            'away_team' => $this->entities->Team->findById($_POST['away_team']),
            'datetime' => $_POST['datetime'],
            'round' => isset($_POST['round']) ? $_POST['round'] : false,
            'serie' => $id_serie ? $this->entities->PlayoffSerie->findById($id_serie) : false
        );
    }

    private function getMessage($name, $match = false)
    {
        $match = $match || !parent::isObjectSet() ? $match : $this->match->__toString();
        return SessionMessage::get($name, array(stmLang('competition', 'match'), $match));
    }

    private function getRefereeMessage($name, $referee)
    {
        return SessionMessage::get($name, array(stmLang('match', 'match-referee'), $referee->__toString()));
    }

    private function getPlayerMessage($name, $match_player)
    {
        return SessionMessage::get($name, array(stmLang('team', 'player'), $match_player->__toString()));
    }

    private function getTeamActionMessage($name, $team_action)
    {
        $arr = $team_action->toArray();
        return SessionMessage::get($name, array(stmLang('match', 'team-action'), $arr['name_action']));
    }

    private function getPlayerActionMessage($name, $player_action)
    {
        return SessionMessage::get($name, array(stmLang('match', 'player-action'), $player_action->__toString()));
    }
}
