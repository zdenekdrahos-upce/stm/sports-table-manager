<?php
/*
 * Update team action
 * $team_action
 */
extract($team_action->toArray());
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">

    <label for="current"><?php echo stmLang('match', 'team-action'); ?>: </label>
    <input readonly="readonly" type="text" id="current" size="40" value="<?php echo $name_action; ?>" />

    <label for="value_home"><?php echo stmLang('match', 'home'); ?>: </label>
    <?php include(STM_MODULES_ROOT . 'Match/Views/_elements/form-parts/team_action/value_home.php'); ?>
    <label for="value_away"><?php echo stmLang('match', 'away'); ?>: </label>
    <?php include(STM_MODULES_ROOT . 'Match/Views/_elements/form-parts/team_action/value_away.php'); ?>

    <input type="submit" name="update" value="<?php echo stmLang('form', 'change'); ?>" />
</form>
