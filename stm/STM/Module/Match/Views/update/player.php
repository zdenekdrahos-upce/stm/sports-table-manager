<?php
/*
 * Update member
 * $team_member
 * $positions
 */
extract($player->toArray());
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">

    <label for="current"><?php echo stmLang('team', 'person'); ?>: </label>
    <input readonly="readonly" type="text" id="current" size="40" value="<?php echo $player; ?>" />
    <label for="position"><?php echo stmLang('person', 'position'); ?>: </label>
    <?php include(STM_MODULES_ROOT . 'Match/Views/_elements/form-parts/player/position.php'); ?>

    <label for="minute_in"><?php echo stmLang('person', 'since'); ?>: </label>
    <?php include(STM_MODULES_ROOT . 'Match/Views/_elements/form-parts/player/minute_in.php'); ?>
    <label for="minute_out"><?php echo stmLang('person', 'to'); ?>: </label>
    <?php include(STM_MODULES_ROOT . 'Match/Views/_elements/form-parts/player/minute_out.php'); ?>
    <label for="substitution"><?php echo stmLang('match', 'lineup-substitute'); ?>: </label>
    <?php include(STM_MODULES_ROOT . 'Match/Views/_elements/form-parts/player/substitution.php'); ?>

    <input type="submit" name="update" value="<?php echo stmLang('form', 'change'); ?>" />
</form>
