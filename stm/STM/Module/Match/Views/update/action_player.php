<?php
/*
 * Update player action
 * $match_player_action
 */
extract($match_player_action->toArray());
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">

    <label for="current"><?php echo stmLang('match', 'player-action'); ?>: </label>
    <input readonly="readonly" type="text" id="current" size="40" value="<?php echo $match_player_action; ?>" />

    <?php include(STM_MODULES_ROOT . 'Match/Views/_elements/form-parts/player_action/id_player_action.php'); ?>

    <h2><?php echo stmLang('form', 'required'); ?></h2>
    <label for="minute_action"><?php echo stmLang('match', 'player-action-minute'); ?>: </label>
    <?php include(STM_MODULES_ROOT . 'Match/Views/_elements/form-parts/player_action/minute_action.php'); ?>

    <h2><?php echo stmLang('form', 'optional'); ?></h2>
    <label for="comment_action"><?php echo stmLang('match', 'player-action-comment'); ?>: </label>
    <?php include(STM_MODULES_ROOT . 'Match/Views/_elements/form-parts/player_action/action_comment.php'); ?>

    <input type="submit" name="update" value="<?php echo stmLang('form', 'change'); ?>" />
</form>
