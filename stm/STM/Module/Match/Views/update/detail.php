<?php
/**
 * Update club
 */
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">

    <?php include(STM_MODULES_ROOT . 'Match/Views/_elements/form-parts/detail.php'); ?>

    <input type="submit" name="update" value="<?php echo stmLang('form', 'change'); ?>" />

</form>
