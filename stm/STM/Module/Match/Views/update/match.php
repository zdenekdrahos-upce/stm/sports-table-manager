<?php
/**
 * Creating new match
 * $competition         competition where the new match will be inserted
 * $teams               teams from competition
 */
use STM\Competition\Season\Season;
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <h2><?php echo stmLang('form', 'required'); ?></h2>

    <h3><?php echo stmLang('match', 'teams'); ?></h3>
    <label><?php echo stmLang('match', 'home'); ?>: </label>
    <?php
    $post_name = 'home_team';
    include(STM_MODULES_ROOT . 'Match/Views/_elements/form-parts/team.php');
    ?>
    <label><?php echo stmLang('match', 'away'); ?>: </label>
    <?php
    $post_name = 'away_team';
    include(STM_MODULES_ROOT . 'Match/Views/_elements/form-parts/team.php');
    ?>

    <?php if ($competition instanceof Season): ?>
    <h3><?php echo stmLang('match', 'season-fields'); ?></h3>
    <label><?php echo stmLang('match', 'season-round'); ?>:</label>
    <?php include(STM_MODULES_ROOT . 'Match/Views/_elements/form-parts/season_round.php'); ?>
    <?php endif; ?>

    <h2><?php echo stmLang('form', 'optional'); ?></h2>
    <label for="date"><?php echo stmLang('match', 'date'); ?>: </label>
    <?php include(STM_MODULES_ROOT . 'Match/Views/_elements/form-parts/date.php'); ?>


    <input type="submit" name="update" value="<?php echo stmLang('form', 'change'); ?>" />
</form>
