<?php
/*
 * Update position of the match referee
 * $referee
 * $positions
 */
extract($referee->toArray());
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <label for="current"><?php echo stmLang('club', 'person'); ?>:</label>
    <input readonly="readonly" type="text" id="current" size="40" value="<?php echo $person . " ({$position})"; ?>" />
    <label for="position"><?php echo stmLang('club', 'position'); ?>: </label>
    <?php $select_name = 'new_position'; include(STM_MODULES_ROOT . 'Match/Views/_elements/form-parts/referee/position.php'); ?>
    <?php include(STM_MODULES_ROOT . 'Match/Views/_elements/form-parts/referee/id_referee.php'); ?>

    <input type="submit" name="update" value="<?php echo stmLang('form', 'change'); ?>" />
</form>
