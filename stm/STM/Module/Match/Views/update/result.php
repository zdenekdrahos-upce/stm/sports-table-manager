<?php
/**
 * Update score of the match
 * $match_periods
 */
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">

    <?php include(STM_MODULES_ROOT . 'Match/Views/_elements/form-parts/result.php'); ?>

    <input type="submit" name="update" value="<?php echo stmLang('form', 'change'); ?>" />
</form>
