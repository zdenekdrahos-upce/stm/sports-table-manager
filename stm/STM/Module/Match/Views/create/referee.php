<?php
/*
 * Create new club member
 * $persons
 * $positions
 */
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <label for="position"><?php echo stmLang('club', 'person'); ?>: </label>
    <?php include(STM_MODULES_ROOT . 'Match/Views/_elements/form-parts/referee/person.php'); ?>
    <label for="position"><?php echo stmLang('club', 'position'); ?>: </label>
    <?php include(STM_MODULES_ROOT . 'Match/Views/_elements/form-parts/referee/position.php'); ?>

    <input type="submit" name="create" value="<?php echo stmLang('form', 'create'); ?>" />
</form>
