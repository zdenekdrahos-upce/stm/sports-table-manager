<?php
/**
 * Create match result according selected action
 * $actions
 * $current_score
 */
use STM\Match\Stats\Action\StatisticCalculationType;
?>

<?php if ($current_score): ?>
<ul>
    <li><?php echo stmLangf('match', 'calculate-score', 'existing-score', array($current_score)); ?></li>
</ul>
<?php endif; ?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">

    <label for="calculation"><?php echo stmLang('match', 'calculate-score', 'calculation'); ?>:</label>
    <?php
    $options = array(
        'COUNT' => stmLang('constants', 'statisticcalculationtype', StatisticCalculationType::COUNT),
        'SUM' => stmLang('constants', 'statisticcalculationtype', StatisticCalculationType::SUM),
    );
    \STM\Web\HTML\Forms::select('calculation', $options);
    ?>
    <label for="match_action"><?php echo stmLang('match', 'player-action'); ?>: </label>
    <?php include(STM_MODULES_ROOT . 'Match/Views/_elements/form-parts/team_action/match_actions.php'); ?>

    <input type="submit" name="create" value="<?php echo stmLang('form', 'create'); ?>" />
</form>
