<?php
/**
 * Create match results of matches from selected round
 * $match_periods
 * $matches
 */
?>

<ul>
    <li><?php echo stmLang('match', 'season-round') . ": <strong>{$_GET['r']}</strong>"; ?></li>
</ul>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <table style="min-width: 300px">
        <tr>
            <th><?php echo stmLang('competition', 'match'); ?></th>
            <th style="width: 170px"><?php echo stmLang('match', 'periods'); ?></th>
        </tr>
        <?php foreach($matches as $match): ?>
        <tr>
            <td><?php echo $match; ?></td>
            <td>
            <?php if ($match->hasScore()): ?>
                <strong><?php echo "{$match->getScoreHome()}:{$match->getScoreAway()}"; ?></strong>
                <?php echo $match->getPeriods() ? ('(' . implode(', ', $match->getPeriods()) . ')') : ''; ?>
            <?php else: ?>
            <?php for($i = 1; $i <= $match_periods; $i++):
                $path = "matches[{$match->getId()}][{$i}]";
            ?>
                <input type="text" class="inline" name="<?php echo $path; ?>[score_home]" size="5" maxlength="3" value="<?php echo isset($_POST['matches'][$match->getId()][$i]['score_home']) ? $_POST['matches'][$match->getId()][$i]['score_home'] : ''; ?>" /> :
                <input type="text" class="inline" name="<?php echo $path; ?>[score_away]" size="5" maxlength="3" value="<?php echo isset($_POST['matches'][$match->getId()][$i]['score_away']) ? $_POST['matches'][$match->getId()][$i]['score_away'] : ''; ?>" /><br />
            <?php endfor; ?>
            <?php endif; ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </table>

    <input type="submit" name="create" value="<?php echo stmLang('form', 'create'); ?>" />
</form>
