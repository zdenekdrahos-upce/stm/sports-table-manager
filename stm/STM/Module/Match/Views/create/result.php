<?php
/**
 * Create match result - all periods in one form
 * $periods_count
 */
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">

    <?php include(STM_MODULES_ROOT . 'Match/Views/_elements/form-parts/result.php'); ?>

    <input type="submit" name="create" value="<?php echo stmLang('form', 'create'); ?>" />
</form>
