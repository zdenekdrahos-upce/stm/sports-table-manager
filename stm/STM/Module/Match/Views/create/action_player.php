<?php
/*
 * Create player action
 * $actions
 * $players
 * $default_actions_count
 */
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <table id="players_actions">
        <tr>
            <th><?php echo stmLang('team', 'player'); ?></th>
            <th><?php echo stmLang('match', 'player-action'); ?></th>
            <th><?php echo stmLang('match', 'player-action-minute'); ?></th>
        </tr>
        <?php
        for ($i = 0; $i < $default_actions_count; $i++) {
            include(STM_MODULES_ROOT . 'Match/Views/_elements/form-parts/player_action/player_row.php');
        }
        ?>
    </table>

    <input type="submit" name="create" value="<?php echo stmLang('form', 'create'); ?>" />
</form>

<hr />
<?php
echo stmLang('form', 'add_new_rows') . ':';
echo \STM\Web\HTML\Forms::select('add_rows', \STM\Utils\Arrays::getNumberArray(1, 5), true, 'inline');
?>

<script type="text/javascript">
    $(document).ready(function() {
        var current_id = <?php echo $default_actions_count; ?>;
        var players = '<?php \STM\Web\HTML\Forms::optionsFromSelect("actions[5][match_player]", $players, true); ?>';
        var actions = '<?php \STM\Web\HTML\Forms::optionsFromSelect("actions[5][match_action]", $actions, true); ?>';
        $('#add_rows').change(function() {
            for (var i = 0; i < this.value; i++) {
                $('#players_actions tr:last').after('\
<tr>\n\
<td><select name="actions[' + current_id + '][match_player]" size="1">' + players + '</select></td>\n\
<td><select name="actions[' + current_id + '][match_action]" size="1">' + actions + '</select></td>\n\
<td><input type="text" name="actions[' + current_id + '][minute_action]" size="20" maxlength="20" value="" /></td>\n\
</tr>');
                current_id++;
            }
        });
    });
</script>
