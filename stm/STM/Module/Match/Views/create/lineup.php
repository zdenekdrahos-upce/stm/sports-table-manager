<?php
/**
 * Lineup
 * $is_home
 * $team_members
 * $positions
 * $lineup
 * $substitution_lineup
 */
?>


<?php if (empty($team_members)): ?>
<p><?php echo stmLang('no-content'); ?></p>
<?php else: ?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">

    <table>
        <thead>
            <tr>
                <th>&nbsp;</th>
                <th><?php echo stmLang('team', 'player'); ?></th>
                <th><?php echo stmLang('person', 'position'); ?></th>
                <th><?php echo stmLang('person', 'since'); ?></th>
                <th><?php echo stmLang('person', 'to'); ?></th>
                <th><?php echo stmLang('match', 'lineup-substitute'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php $printed_players = 0; foreach($team_members as $team_member): ?>
            <tr>
                <?php if ($lineup->getPlayer($team_member->getId()) == false): $printed_players++; ?>
                <td><input id="<?php echo $team_member->getId(); ?>" type="checkbox" name="players[<?php echo $team_member->getId(); ?>][id_team_player]" value="<?php echo $team_member->getId(); ?>" /></td>
                <td><label for="<?php echo $team_member->getId(); ?>"><?php echo $team_member; ?></label></td>
                <td><?php include(STM_MODULES_ROOT . 'Match/Views/_elements/form-parts/player/position.php'); ?></td>
                <td><?php include(STM_MODULES_ROOT . 'Match/Views/_elements/form-parts/player/minute_in.php'); ?></td>
                <td><?php include(STM_MODULES_ROOT . 'Match/Views/_elements/form-parts/player/minute_out.php'); ?></td>
                <td><?php include(STM_MODULES_ROOT . 'Match/Views/_elements/form-parts/player/substitution.php'); ?></td>
                <?php endif; ?>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php if ($printed_players == 0): ?>
    <br />
    <p><?php echo stmLang('no-content'); ?></p>
    <?php else: ?>
    <input type="submit" name="create" value="<?php echo stmLang('form', 'create'); ?>" />
    <?php endif; ?>

</form>

<?php endif; ?>

<hr />
<a href="?module=team&action=read&type=members&t=<?php echo $id_team; ?>"><?php echo stmLang('header', 'team', 'read', 'members'); ?></a>
