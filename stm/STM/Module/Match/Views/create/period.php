<?php
/**
 * Create new match period
 */
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <?php include(STM_MODULES_ROOT . 'Match/Views/_elements/form-parts/period.php'); ?>

    <input type="submit" name="create_period" value="<?php echo stmLang('form', 'create'); ?>" />
</form>
