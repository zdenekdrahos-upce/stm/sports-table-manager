<?php
$url = '?module=match&action=delete&type=action_player&m=' . $_GET['m'];
$submit_message = stmLang('form', 'delete');
$question = stmLang('match', 'delete-playeraction-msg') . $match_player_action . '?';
?>
<form method="post" action="<?php echo $url; ?>" class="delete">
    <?php \STM\Web\HTML\Forms::inputDeleteToken(); ?>
    <?php include(STM_MODULES_ROOT . 'Match/Views/_elements/form-parts/player_action/id_player_action.php'); ?>
    <input type="submit" name="delete" value="<?php echo $submit_message; ?>" onclick="return confirm('<?php echo $question; ?>')" />
</form>
