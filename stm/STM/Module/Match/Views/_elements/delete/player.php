<?php
$url = '?module=match&action=delete&type=player&m=' . $_GET['m'] . '&mp=' . $match_player->getIdMatchPlayer();
$submit_message = stmLang('form', 'delete');
$question = stmLang('match', 'delete-player-msg') . $player . " ({$position})";
include(STM_ADMIN_TEMPLATE_ROOT . 'form/delete-form.php');
