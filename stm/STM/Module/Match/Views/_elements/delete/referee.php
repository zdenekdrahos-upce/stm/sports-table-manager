<?php
$url = '?module=match&action=delete&type=referee&m=' . $_GET['m'];
$submit_message = stmLang('match', 'delete-referee');
$question = stmLang('match', 'delete-referee-msg') . $person . '?';
?>
<form method="post" action="<?php echo $url; ?>" class="delete">
    <?php \STM\Web\HTML\Forms::inputDeleteToken(); ?>
    <?php include(STM_MODULES_ROOT . 'Match/Views/_elements/form-parts/referee/id_referee.php'); ?>
    <input type="submit" name="delete" value="<?php echo $submit_message; ?>" onclick="return confirm('<?php echo $question; ?>')" />
</form>
