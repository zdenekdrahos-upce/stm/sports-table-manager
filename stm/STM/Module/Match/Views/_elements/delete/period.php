<?php
$url = '?module=match&action=delete&type=period&m=' . $_GET['m'] . '&p=' . $period->getIdPeriod();
$submit_message = stmLang('match', 'delete-period');
$question = stmLang('match', 'delete-period-msg') . $period->getIdPeriod() . '?';
include(STM_ADMIN_TEMPLATE_ROOT . 'form/delete-form.php');
