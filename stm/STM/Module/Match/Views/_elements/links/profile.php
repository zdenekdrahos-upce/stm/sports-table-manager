    <h2>
        <span><a href="<?php echo buildUrl(array('module' => 'competition', 'action' => 'read', 'type' => 'competition', 'm' => null, 'c' => $id_competition)); ?>"><?php echo $competition_name; ?></a></span>
        <a href="<?php echo buildUrl(array('module' => 'match', 'action' => 'read', 'type' => 'match')) ?>"><?php echo $match_name; ?></a>
    </h2>
    <ul>
        <li class="header"><?php echo stmLang('prediction', 'result'); ?></li>
        <?php if ($hasScore): ?>
        <li><a href="<?php echo buildUrl(array('module' => 'match', 'action' => 'update', 'type' => 'result')); ?>"><?php echo stmLang('header', 'match', 'update', 'result'); ?></a></li>
        <li><a href="<?php echo buildUrl(array('module' => 'match', 'action' => 'create', 'type' => 'period')); ?>"><strong><?php echo stmLang('match', 'period', 'add'); ?></strong></a></li>
        <li><?php include(STM_MODULES_ROOT . 'Match/Views/_elements/delete/result.php'); ?></li>
        <?php else: ?>
        <li><a href="<?php echo buildUrl(array('action' => 'create', 'type' => 'result')); ?>"><strong><?php echo stmLang('header', 'match', 'create', 'result'); ?></strong></a></li>
        <?php endif; ?>
        <li class="header"><?php echo stmLang('competition', 'match'); ?></li>
        <li><a href="<?php echo buildUrl(array('module' => 'match', 'action' => 'read', 'type' => 'detail')); ?>"><?php echo stmLang('header', 'match', 'read', 'detail'); ?></a></li>
        <li><a href="<?php echo buildUrl(array('module' => 'match', 'action' => 'read', 'type' => 'referees')); ?>"><?php echo stmLang('header', 'match', 'read', 'referees'); ?></a></li>
        <li><a href="<?php echo buildUrl(array('module' => 'match', 'action' => 'read', 'type' => 'lineup_home')); ?>"><?php echo stmLang('header', 'match', 'read', 'lineup_home'); ?></a></li>
        <li><a href="<?php echo buildUrl(array('module' => 'match', 'action' => 'read', 'type' => 'lineup_away')); ?>"><?php echo stmLang('header', 'match', 'read', 'lineup_away'); ?></a></li>
        <li><a href="<?php echo buildUrl(array('module' => 'match', 'action' => 'read', 'type' => 'action_team')); ?>"><?php echo stmLang('header', 'match', 'read', 'action_team'); ?></a></li>
        <li><a href="<?php echo buildUrl(array('module' => 'match', 'action' => 'read', 'type' => 'action_player')); ?>"><?php echo stmLang('header', 'match', 'read', 'action_player'); ?></a></li>
        <li><a href="<?php echo buildUrl(array('module' => 'match', 'action' => 'update', 'type' => 'match')); ?>"><?php echo stmLang('header', 'match', 'update', 'match'); ?></a></li>
        <li><?php include(STM_MODULES_ROOT . 'Match/Views/_elements/delete/match.php'); ?></li>
        <?php if (STM_ALLOW_PREDICTIONS): ?>
        <li class="header"><?php echo stmLang('match', 'predictions'); ?></li>
        <li><a href="<?php echo buildUrl(array('module' => 'prediction', 'action' => 'create', 'type' => 'prediction')); ?>"><?php echo stmLang('match', 'new-prediction'); ?></a></li>
        <li><a href="<?php echo buildUrl(array('module' => 'prediction', 'action' => 'read', 'type' => 'match_stats')); ?>"><?php echo stmLang('header', 'prediction', 'read', 'match_stats'); ?></a></li>
        <?php endif; ?>
    </ul>
