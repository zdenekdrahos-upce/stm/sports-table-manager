<?php
$select_name = isset($team_member) ? "players[{$team_member->getId()}][id_substitution_player]" : 'id_substitution_player';
\STM\Web\HTML\Forms::select($select_name, \STM\Utils\Arrays::getAssocArray($substitution_lineup, 'getIdMatchPlayer', '__toString'), true);
