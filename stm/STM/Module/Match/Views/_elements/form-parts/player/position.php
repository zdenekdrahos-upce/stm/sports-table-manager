<?php
$select_name = isset($team_member) ? "players[{$team_member->getId()}][id_position]" : 'id_position';
\STM\Web\HTML\Forms::select($select_name, \STM\Utils\Arrays::getAssocArray($positions, 'getId', '__toString'));
