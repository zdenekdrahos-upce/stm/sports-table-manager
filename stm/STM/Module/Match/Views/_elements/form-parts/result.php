
    <table>
        <tr>
            <th><?php echo stmLang('match', 'period-number'); ?></th>
            <th><?php echo stmLang('match', 'score-home'); ?></th>
            <th><?php echo stmLang('match', 'score-away'); ?></th>
            <th><?php echo stmLang('match', 'period', 'note'); ?></th>
        </tr>
    <?php for($i = 1; $i <= $periods_count; $i++): ?>
        <tr>
            <td><?php echo $i; ?></td>
            <td><input type="text" class="inline" name="periods[<?php echo $i; ?>][score_home]" size="5" maxlength="3" value="<?php echo isset($_POST['periods'][$i]['score_home']) ? $_POST['periods'][$i]['score_home'] : ''; ?>" /></td>
            <td><input type="text" class="inline" name="periods[<?php echo $i; ?>][score_away]" size="5" maxlength="3" value="<?php echo isset($_POST['periods'][$i]['score_away']) ? $_POST['periods'][$i]['score_away'] : ''; ?>" /></td>
            <td><input type="text" class="inline" name="periods[<?php echo $i; ?>][note]" size="10" maxlength="10" value="<?php echo isset($_POST['periods'][$i]['note']) ? $_POST['periods'][$i]['note'] : ''; ?>" /></td>
        </tr>
    <?php endfor; ?>
    </table>
