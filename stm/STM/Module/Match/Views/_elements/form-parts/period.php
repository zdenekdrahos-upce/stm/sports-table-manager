<label for="home"><?php echo stmLang('match', 'score-home'); ?>:</label>
<input type="text" id="home" name="score_home" size="5" maxlength="3" value="<?php echo $_POST['score_home']; ?>" />
<label for="away"><?php echo stmLang('match', 'score-away'); ?>:</label>
<input type="text" id="away" name="score_away" size="5" maxlength="3" value="<?php echo $_POST['score_away']; ?>" />
<label for="note"><?php echo stmLang('match', 'period', 'note'); ?>: </label>
<input type="text" id="note" name="note" size="10" maxlength="10" value="<?php echo $_POST['note']; ?>" />
