    <label for="spectators"><?php echo stmLang('match', 'detail-spectators'); ?>: </label>
    <input type="text" id="spectators" name="spectators" size="40" maxlength="6" value="<?php echo $_POST['spectators']; ?>" />

    <label for="stadium"><?php echo stmLang('match', 'detail-stadium'); ?>: </label>
    <?php \STM\Web\HTML\Forms::select('id_stadium', \STM\Utils\Arrays::getAssocArray($stadiums, 'getId', '__toString'), true); ?>

    <label for="match_comment"><?php echo stmLang('match', 'detail-comment'); ?>: </label>
    <textarea id="match_comment" name="match_comment" cols="32" rows="15"><?php echo $_POST['match_comment']; ?></textarea>
