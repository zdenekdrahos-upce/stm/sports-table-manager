<?php
extract($match_player->toArray());
?>
                <td><?php echo $position; ?></td>
                <td><?php echo $minute_in != '' ? $minute_in : '-'; ?></td>
                <td><?php echo $minute_out != '' ? $minute_out : '-'; ?></td>
                <td><?php
                $substitution = $lineup->getSubstitutionPlayer($match_player->getIdSubstitutionPlayer());
                if ($substitution) {
                    $temp = $substitution->toArray();
                    echo $temp['player'];
                } else {
                    echo '-';
                }
                ?></td>
