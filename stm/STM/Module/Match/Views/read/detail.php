<?php
/** Match detail
 * $detail
 * $id_match
 */
if ($detail) {
    $details = $detail->toArray();
} else {
    $details = array('spectators' => '', 'match_comment' => '', 'stadium' => '');
}
$spectators = is_numeric($details['spectators']) ? $details['spectators'] : stmLang('not-set');
$stadium = $details['stadium'] ? '<a href="' . buildUrl(array('module' => 'stadium', 'type' => 'stadium', 'clb' => null, 'std' => $detail->getIdStadium())) . '" target="_blank">' . $details['stadium'] . '</a>' : stmLang('not-set');
$comment = $details['match_comment'] ? $details['match_comment'] : stmLang('not-set');
?>

<ul>
    <li><?php echo stmLang('match', 'detail-spectators'); ?>: <strong><?php echo $spectators; ?></strong></li>
    <li><?php echo stmLang('match', 'detail-stadium'); ?>: <strong><?php echo $stadium; ?></strong></li>
    <li><?php echo stmLang('match', 'detail-comment'); ?>:
        <aside class="text"><strong><?php echo $comment; ?></strong></aside>
    </li>
</ul>

<h2><?php echo stmLang('left-menu-header'); ?></h2>
<ul>
    <li><a href="<?php echo buildUrl(array('action' => 'update')); ?>"><?php echo stmLang('header', 'match', 'update', 'detail'); ?></a></li>
    <li><?php include(STM_MODULES_ROOT . 'Match/Views/_elements/delete/detail.php'); ?></li>
</ul>
