<?php
/**
 * Match player actions (statistics)
 * $actions
 */
?>

<?php if (empty($actions)): ?>
    <p><?php echo stmLang('no-content'); ?></p>
<?php else: ?>
    <table>
        <thead>
            <tr>
                <th><?php echo stmLang('competition', 'team'); ?></th>
                <th><?php echo stmLang('team', 'player'); ?></th>
                <th><?php echo stmLang('match', 'player-action'); ?></th>
                <th><?php echo stmLang('match', 'player-action-minute'); ?></th>
                <th><?php echo stmLang('team', 'links'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($actions as $match_player_action): extract($match_player_action->toArray()); ?>
            <tr>
                <td><a href="?module=team&action=read&type=team&t=<?php echo $match_player_action->getIdTeam();?>"><?php echo $name_team; ?></a></td>
                <td><a href="?module=person&action=read&type=person&prs=<?php echo $match_player_action->getIdPerson();?>"><?php echo $name_player; ?></a></td>
                <td><a href="?module=action#<?php echo $match_player_action->getIdMatchAction();?>"><?php echo $name_action; ?></a></td>
                <td><?php echo $minute_action; ?></td>
                <td>
                    <form method="post" action="<?php echo buildUrl(array('action' => 'update'));?>" class="delete">
                        <?php include(STM_MODULES_ROOT . 'Match/Views/_elements/form-parts/player_action/id_player_action.php'); ?>
                        <input type="submit" name="go_to_update" value="<?php echo stmLang('form', 'change'); ?>," />
                    </form>
                    <?php include(STM_MODULES_ROOT . 'Match/Views/_elements/delete/action_player.php'); ?>
                </td>
            </tr>
            <?php if (\STM\Utils\Strings::isStringNonEmpty($action_comment)): ?>
            <tr class="action_comment">
                <td>&nbsp;</td>
                <th><?php echo stmLang('match', 'player-action-comment'); ?>:</th>
                <td colspan="3"><?php echo $action_comment; ?></td>
            </tr>
            <?php endif; ?>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>

<h2><?php echo stmLang('left-menu-header'); ?></h2>
<ul>
    <li><a href="<?php echo buildUrl(array('action' => 'create')); ?>"><?php echo stmLang('header', 'match', 'create', 'action_player'); ?></a></li>
    <li><a href="<?php echo buildUrl(array('action' => 'create', 'type' => 'result_from_action')); ?>"><?php echo stmLang('header', 'match', 'create', 'result_from_action'); ?></a></li>
</ul>
