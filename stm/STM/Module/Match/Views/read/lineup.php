<?php
/**
 * Lineup
 */
?>

<?php if (empty($team_lineup)): ?>
    <p><?php echo stmLang('no-content'); ?></p>
<?php else: ?>
    <table>
        <thead>
            <tr>
                <th><?php echo stmLang('team', 'player'); ?></th>
                <th><?php echo stmLang('person', 'position'); ?></th>
                <th><?php echo stmLang('person', 'since'); ?></th>
                <th><?php echo stmLang('person', 'to'); ?></th>
                <th><?php echo stmLang('match', 'lineup-substitute'); ?></th>
                <th><?php echo stmLang('team', 'links'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($team_lineup as $match_player): extract($match_player->toArray()); ?>
            <tr>
                <td><a href="?module=person&action=read&type=person&prs=<?php echo $match_player->getIdPerson();?>"><?php echo $player; ?></a></td>
                <?php include(STM_MODULES_ROOT . 'Match/Views/_elements/player/description.php'); ?>
                <td>
                    <a href="<?php echo buildUrl(array('action' => 'update', 'type' => 'player', 'mp' => $match_player->getIdMatchPlayer()));?>"><?php echo stmLang('form', 'change'); ?></a>,
                    <?php include(STM_MODULES_ROOT . 'Match/Views/_elements/delete/player.php'); ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>

<hr />
<a href="<?php echo buildUrl(array('action' => 'create')); ?>"><?php echo stmLang('header', 'match', 'create', $_GET['type']); ?></a>
