<?php
/**
 * Team actions (statistics)
 * $actions
 */
?>

<?php if (empty($actions)): ?>
    <p><?php echo stmLang('no-content'); ?></p>
<?php else: ?>
    <table>
        <thead>
            <tr>
                <th><?php echo stmLang('match', 'team-action'); ?></th>
                <th><?php echo stmLang('match', 'home'); ?></th>
                <th><?php echo stmLang('match', 'away'); ?></th>
                <th><?php echo stmLang('team', 'links'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($actions as $team_action): extract($team_action->toArray()); ?>
            <tr>
                <td><a href="?module=action#<?php echo $team_action->getIdMatchAction();?>"><?php echo $name_action; ?></a></td>
                <td><?php echo $value_home; ?></td>
                <td><?php echo $value_away; ?></td>
                <td>
                    <a href="<?php echo buildUrl(array('action' => 'update', 'type' => 'action_team', 'ma' => $team_action->getIdMatchAction()));?>"><?php echo stmLang('form', 'change'); ?></a>,
                    <?php include(STM_MODULES_ROOT . 'Match/Views/_elements/delete/action_team.php'); ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>

<h2><?php echo stmLang('left-menu-header'); ?></h2>
<ul>
    <li><a href="<?php echo buildUrl(array('action' => 'create', 'type' => 'action_team')); ?>"><?php echo stmLang('header', 'match', 'create', 'action_team'); ?></a></li>
</ul>
