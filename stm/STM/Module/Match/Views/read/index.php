<?php
/*
 * Homepage of MATCH module
 * $result  array of matches or instanceof \STM\Match\Table\ResultTable
 */
use STM\Match\Table\ResultTable;
?>

<h2><a href="javascript:toggleDiv('toogleFilter');"><?php echo stmLang('match', 'filter', 'header'); ?></a></h2>
<div class="toogleFilter"<?php if (isset($_POST['update'])) echo ' id="displayed"' ?>>
<?php include(__DIR__ . '/filter/form.php'); ?>
</div>
<hr />

<?php if ($result instanceof ResultTable): ?>
<?php
$result_table = $result;
$display_wot = false;
$display_lot = false;
$your_teams = array();
include(STM_ADMIN_TEMPLATE_ROOT . 'result-table/index.php');
?>
<?php else: ?>
<table>
    <?php if ($result): ?>
        <tr>
            <th><?php echo stmLang('competition', 'match'); ?></th>
            <th><?php echo stmLang('match', 'date'); ?></th>
            <th><?php echo stmLang('match', 'score'); ?></th>
            <th><?php echo stmLang('match', 'periods'); ?></th>
        </tr>
        <?php foreach ($result as $match): extract($match->toArray()); ?>
            <tr>
                <td><a href="index.php?module=match&action=read&type=match&m=<?php echo $match->getId(); ?>"><?php echo $team_home . ' : ' . $team_away; ?></a></td>
                <td><?php echo \STM\Utils\Dates::convertDatetimeToString($date, '-'); ?></td>
                <td><?php echo $match->hasScore() ? ($score_home . ' : ' . $score_away) : '-:-'; ?></td>
                <td><?php echo $match->getPeriods() ? implode(', ', $match->getPeriods()) : ''; ?></td>
            </tr>
        <?php endforeach; ?>
        <?php if ($pagination->getTotalPages() > 1): ?>
            <tr>
                <th colspan="3" class="pagination"><?php
                    for ($i = 1; $i <= $pagination->getTotalPages(); $i++) {
                        $class = $i == $pagination->getCurrentPage() ? ' class="selected"' : '';
                        echo '<input form="filterForm" type="submit" name="page"' . $class . ' value="' . $i . '" />';
                    }
                ?></th>
                <th><?php
                    echo stmLang('match', 'filter', 'page') . ': ';
                    echo "<strong>{$pagination->getCurrentPage()}/{$pagination->getTotalPages()}</strong>"; ?>
                </th>
            </tr>
        <?php endif; ?>

    <?php else: ?>
        <tr>
            <p><?php echo stmLang('no-content'); ?></p>
        </tr>
    <?php endif; ?>
</table>

<?php endif;
