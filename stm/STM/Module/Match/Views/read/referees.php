<?php
/**
 * Refereees
 * $referees
 */
?>

<?php if (empty($referees)): ?>
    <p><?php echo stmLang('no-content'); ?></p>
<?php else: ?>
    <table>
        <thead>
            <tr>
                <th><?php echo stmLang('match', 'match-referee'); ?></th>
                <th><?php echo stmLang('person', 'position'); ?></th>
                <th><?php echo stmLang('team', 'links'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($referees as $referee): extract($referee->toArray()); ?>
            <tr>
                <td><a href="?module=person&action=read&type=person&prs=<?php echo $referee->getIdPerson();?>"><?php echo $person; ?></a></td>
                <td><?php echo $position; ?></td>
                <td>
                    <form method="post" action="<?php echo buildUrl(array('action' => 'update', 'type' => 'referee'));?>" class="delete">
                        <?php include(STM_MODULES_ROOT . 'Match/Views/_elements/form-parts/referee/id_referee.php'); ?>
                        <input type="submit" name="go_to_update" value="<?php echo stmLang('club', 'position-update'); ?>," />
                    </form>
                    <?php include(STM_MODULES_ROOT . 'Match/Views/_elements/delete/referee.php'); ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>

<h2><?php echo stmLang('left-menu-header'); ?></h2>
<ul>
    <li><a href="<?php echo buildUrl(array('action' => 'create', 'type' => 'referee')); ?>"><?php echo stmLang('header', 'match', 'create', 'referee'); ?></a></li>
</ul>
