<?php
$col_count = 4;
$actual_col_count = 0;
?>

<form id="filterForm" method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">

    <label for="match_type" class="inline"><?php echo stmLang('match', 'filter', 'match-selection'); ?>:</label>
    <?php
    $options = array('A' => 'all-matches', 'P' => 'played-matches', 'U' => 'upcoming-matches');
    foreach ($options as $key => $value) {
        $options[$key] = stmLang('match', 'filter', $value);
    }
    \STM\Web\HTML\Forms::select('match_type', $options, false, 'inline');
    ?><br />
    <label for="load_score" class="inline"><?php echo stmLang('match', 'filter', 'load-scores'); ?>:</label>
    <input type="checkbox" id="load_score" name="load_score" class="inline" <?php echo (isset($_POST['load_score'])) ? 'checked="checked"' : ''; ?> /><br />
    <label for="load_periods" class="inline"><?php echo stmLang('match', 'filter', 'load-periods'); ?>:</label>
    <input type="checkbox" id="load_periods" name="load_periods" class="inline" <?php echo (isset($_POST['load_periods'])) ? 'checked="checked"' : ''; ?> /><br />
    <label for="result_table" class="inline"><?php echo stmLang('match', 'filter', 'result-table'); ?>:</label>
    <input type="checkbox" id="result_table" name="result_table" class="inline" <?php echo (isset($_POST['result_table'])) ? 'checked="checked"' : ''; ?> /><br />
    <label for="date_start" class="inline"><?php echo stmLang('competition', 'date-start'); ?>: </label>
    <?php echo \STM\Web\HTML\Forms::inputDate('date_start'); ?>
    <label for="date_end"><?php echo stmLang('competition', 'date-end'); ?>: </label>
    <?php echo \STM\Web\HTML\Forms::inputDate('date_end'); ?>

    <h3><a href="javascript:toggleDiv('selectCompetitions');"><?php echo stmLang('menu', 'competitions'); ?></a></h3>
    <div class="toogle selectCompetitions">
        <label for="round"><?php echo stmLang('match', 'season-round'); ?>:</label>
        <?php \STM\Web\HTML\Forms::select('round', \STM\Utils\Arrays::getNumberArray(1, 30), true, 'inline'); ?><br />
        <label for="id_competition"><?php echo stmLang('competition', 'competition'); ?>:</label>
        <?php \STM\Web\HTML\Forms::select('id_competition', \STM\Utils\Arrays::getAssocArray($competitions, 'getId', '__toString'), true, 'inline'); ?><br />
    </div>

    <h3><a href="javascript:toggleDiv('selectTeams');"><?php echo stmLang('menu', 'teams'); ?></a></h3>
    <div class="toogle selectTeams">
        <input type="checkbox" id="head_to_head" name="head_to_head" class="inline" <?php echo (isset($_POST['head_to_head'])) ? 'checked="checked"' : ''; ?> />
        <label for="head_to_head" class="inline"><?php echo stmLang('match', 'filter', 'head-to-head'); ?>:</label><br />
        <table>
        <?php foreach ($teams as $team): ?>
            <?php if($actual_col_count++ % $col_count == 0): ?><tr><?php endif; ?>
                <td class="checkbox"><input type="checkbox" id="<?php echo $team->getId(); ?>" name="teams[<?php echo $team->getId(); ?>]" class="inline" <?php echo (isset($_POST['teams'][$team->getId()])) ? 'checked="checked"' : ''; ?> /></td>
                <td class="left"><label for="<?php echo $team->getId(); ?>" class="inline"><?php echo $team; ?></label></td>
            <?php if($actual_col_count % $col_count == 0 || $team == end($teams)): ?></tr><?php endif; ?>
        <?php endforeach; ?>
        </table>
    </div>

    <input type="submit" name="update" value="<?php echo stmLang('match', 'filter', 'submit'); ?>" />
</form>
