<?php
/**
 * Match info page
 * $match               selected match
 * $competition         parent competition of the match
 * $team_home
 * $team_away
 * $score
 * $date
 * $periods
 * $season_round        if competition is season
 * $playoff_serie       parent serie, if competition is playoff
 */
?>

<ul>
    <li><?php echo stmLang('match', 'home'); ?>: <a href="?module=team&action=read&type=team&t=<?php echo $match->getIdTeamHome(); ?>"><?php echo $team_home; ?></a></li>
    <li><?php echo stmLang('match', 'away'); ?>: <a href="?module=team&action=read&type=team&t=<?php echo $match->getIdTeamAway(); ?>"><?php echo $team_away; ?></a></li>
    <li><?php echo stmLang('match', 'date'); ?>: <strong><?php echo $date; ?></strong></li>
    <?php if (isset($season_round)): ?>
    <li><?php echo stmLang('match', 'season-round'); ?>: <strong><?php echo $season_round; ?></strong></li>
    <?php endif; ?>
    <?php if (isset($id_serie)): ?>
    <li><strong><a href="<?php echo buildUrl(array('module' => 'playoff', 'type' => 'serie', 'c' => $competition->getId(), 's' => $id_serie, 'm' => null)); ?>"><?php echo stmLang('match', 'back-to-serie'); ?></a></strong></li>
    <?php endif; ?>
    <li><?php echo stmLang('match', 'score'); ?>: <strong><?php echo $score; ?></strong></li>
</ul>

<h2><?php echo stmLang('match', 'periods'); ?></h2>
<?php if ($periods): ?>
<ul>
    <?php foreach ($periods as $period): ?>
    <li><?php echo $period; ?> | <?php include(STM_MODULES_ROOT . 'Match/Views/_elements/delete/period.php'); ?></li>
    <?php endforeach; ?>
</ul>
<?php else: ?>
<p><?php echo stmLang('no-content'); ?></p>
<?php endif;
