<?php
/*
 * Person matches with match actions
 * $actions \STM\Match\Action\Player\MatchPlayerAction
 * $team_members
 */
?>

<?php
$fields = array(
    't' => array(
        'name' => stmLang('competition', 'team'),
        'values' => \STM\Utils\Arrays::getAssocArray($team_members, 'getIdTeam', 'getNameTeam'),
    ),
);
include(STM_ADMIN_TEMPLATE_ROOT . 'form/filtering-form.php');
?>

<?php include(STM_ADMIN_TEMPLATE_ROOT . 'statistics/matches-with-statistic.php');
