<?php
/*
 * Player statistics
 * $stats
 * $all_actions
 */
?>

<?php
$fields = array(
    't' => array(
        'name' => stmLang('competition', 'team'),
        'values' => \STM\Utils\Arrays::getAssocArray($team_members, 'getIdTeam', 'getNameTeam'),
    ),
    'c' => array(
        'name' => stmLang('competition', 'competition'),
        'values' => \STM\Utils\Arrays::getAssocArray($competitions, 'getId', '__toString'),
    ),
);
include(STM_ADMIN_TEMPLATE_ROOT . 'form/filtering-form.php');
?>

<table id="tablesorter">
    <thead>
    <tr>
        <th><a href=""><?php echo stmLang('person', 'team'); ?></a></th>
        <?php include(STM_MODULES_ROOT . 'Statistics/Views/_elements/actions-table/header.php'); ?>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach($stats as $team_stats):
        if (isset($_GET['t']) && !in_array($_GET['t'], array('', $team_stats['member']->getIdTeam()))) {
            continue;
        }
        extract($team_stats['member']->toArray());
    ?>
    <tr>
        <td><a href="?module=team&action=read&type=team&t=<?php echo $team_stats['member']->getIdTeam();?>"><?php echo $team; ?></a></td>
        <?php include(STM_MODULES_ROOT . 'Statistics/Views/_elements/actions-table/row.php'); ?>
    </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<script type="text/javascript" src="web/scripts/jquery.tablesorter.min.js"></script>
<script type="text/javascript">
    $(function() {
        $("#tablesorter").tablesorter();
    });
</script>
