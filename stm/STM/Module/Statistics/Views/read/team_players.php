<?php
/*
 * Player statistics
 * $stats
 * $all_actions
 */
?>

<?php
$fields = array(
    'c' => array(
        'name' => stmLang('competition', 'competition'),
        'values' => \STM\Utils\Arrays::getAssocArray($competitions, 'getId', '__toString'),
    ),
);
include(STM_ADMIN_TEMPLATE_ROOT . 'form/filtering-form.php');
?>

<table id="tablesorter">
    <thead>
    <tr>
        <th><a href=""><?php echo stmLang('team', 'player'); ?></a></th>
        <?php include(STM_MODULES_ROOT . 'Statistics/Views/_elements/actions-table/header.php'); ?>
    </tr>
    </thead>
    <tbody>
    <?php foreach($stats as $team_stats): extract($team_stats['member']->toArray()); ?>
    <tr>
        <td><a href="?module=statistics&action=read&type=player_matches&tm=<?php echo $team_stats['member']->getId();?>"><?php echo $person; ?></a> (<?php echo $position; ?>)</td>
        <?php include(STM_MODULES_ROOT . 'Statistics/Views/_elements/actions-table/row.php'); ?>
    </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<script type="text/javascript" src="web/scripts/jquery.tablesorter.min.js"></script>
<script type="text/javascript">
    $(function() {
        $("#tablesorter").tablesorter();
    });
</script>
