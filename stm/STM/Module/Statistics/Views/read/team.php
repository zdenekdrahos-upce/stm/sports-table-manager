<?php
/*
 * Team statistics
 * $stats \STM\Match\Stats\TeamMatchesStats
 * $competitions
 * $positions   only in seasons
 */
?>

<?php
$fields = array(
    'c' => array(
        'name' => stmLang('competition', 'competition'),
        'values' => \STM\Utils\Arrays::getAssocArray($competitions, 'getId', '__toString'),
    ),
);
include(STM_ADMIN_TEMPLATE_ROOT . 'form/filtering-form.php');
?>

<h2><?php echo stmLang('statistics', 'team-overall-results'); ?></h2>
<?php
$table_row_stats = $stats->tableRowStatistics;
include(STM_ADMIN_TEMPLATE_ROOT . 'statistics/table-row-stats.php');
?>

<h2><?php echo stmLang('statistics', 'records'); ?></h2>
<?php
$records = array('highestAggregateScore', 'highestGoalFor', 'highestGoalAgainst', 'highestWin', 'highestLoss');
foreach ($records as $record) {
    $name = stmLang('statistics', $record);
    $statistic = $stats->$record;
    include(STM_ADMIN_TEMPLATE_ROOT . 'statistics/top-matches.php');
}
?>

<?php if ($positions): ?>
<h2><?php echo stmLang('statistics', 'team-positions'); ?></h2>
<table>
    <tr>
        <th><?php echo stmLang('match', 'season-round'); ?></th>
        <th><?php echo stmLang('person', 'position'); ?></th>
    </tr>
    <?php foreach($positions['positions'] as $round => $position): ?>
    <tr>
        <td><?php echo $round; ?></td>
        <td><strong><?php echo $position ? "{$position}." : '-'; ?></strong></td>
    </tr>
    <?php endforeach; ?>
</table>
<?php endif;
