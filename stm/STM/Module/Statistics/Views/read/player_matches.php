<?php
/*
 * Player matches with match actions
 * $actions \STM\Match\Action\Player\MatchPlayerAction
 */
?>

<ul>
    <li>
        <?php echo stmLang('team', 'player'); ?>:
        <a href="<?php echo buildUrl(array(
            'type' => 'person_players', 
            'prs' => $teamMember->getIdPerson(), 
            't' => $teamMember->getIdTeam(), 
            'tm' => null
        ));?>">
            <?php echo $teamMember; ?>
        </a>
    </li>
</ul>

<?php include(STM_ADMIN_TEMPLATE_ROOT . 'statistics/matches-with-statistic.php');
