<?php
/*
 * Player statistics
 * $stats
 * $all_actions
 * $teams
 */
?>

<table id="tablesorter">
    <thead>
    <tr>
        <th><a href=""><?php echo stmLang('team', 'player'); ?></a></th>
        <th><a href=""><?php echo stmLang('competition', 'team'); ?></a></th>
        <th><a href=""><?php echo stmLang('person', 'position'); ?></a></th>
        <?php include(STM_MODULES_ROOT . 'Statistics/Views/_elements/actions-table/header.php'); ?>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach($stats as $team_stats):
        extract($team_stats['member']->toArray());
        if (isset($_GET['t']) && !in_array($_GET['t'], array('', $team_stats['member']->getIdTeam()))) {
            continue;;
        }
    ?>
    <tr>
        <td><a href="?module=statistics&action=read&type=player_matches&tm=<?php echo $team_stats['member']->getId();?>"><?php echo $person; ?></a></td>
        <td><a href="?module=statistics&action=read&type=team_players&t=<?php echo $team_stats['member']->getIdTeam() . '&c=' . $_GET['c'];?>"><?php echo $team; ?></a></td>
        <td><?php echo $position; ?></td>
        <?php include(STM_MODULES_ROOT . 'Statistics/Views/_elements/actions-table/row.php'); ?>
    </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<script type="text/javascript" src="web/scripts/jquery.tablesorter.min.js"></script>
<script type="text/javascript">
    $(function() {
        $("#tablesorter").tablesorter();
    });
</script>
