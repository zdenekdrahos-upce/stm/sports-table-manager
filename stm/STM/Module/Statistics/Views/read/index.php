<?php
/* Statistics main page */
$statistics = array(
    'head_to_head' => true,
    'competition_players' => false,
    'team_players' => false,
    'person_matches' => false,
    'player_matches' => false,
    'person_players' => false,
    'team' => false,
);
?>

<ul>
    <?php foreach ($statistics as $type => $print_link): ?>
    <?php if ($print_link): ?>
    <li><a href="<?php echo buildUrl(array('action' => 'read', 'type' => $type)); ?>"><?php echo stmLang('header', 'statistics', 'read', $type); ?></a></li>
    <?php else: ?>
    <li><?php echo stmLang('header', 'statistics', 'read', $type); ?></li>
    <?php endif; ?>
    <?php endforeach; ?>
</ul>
