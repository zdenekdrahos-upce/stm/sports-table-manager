<?php
/**
 * $teams
 * $result_table
 * $matches
 */
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <label for="home" class="inline"><?php echo stmLang('match', 'home'); ?>:</label>
    <?php \STM\Web\HTML\Forms::select('home', \STM\Utils\Arrays::getAssocArray($teams, 'getId', '__toString'), false, 'inline'); ?><br />
    <label for="away" class="inline"><?php echo stmLang('match', 'away'); ?>:</label>
    <?php \STM\Web\HTML\Forms::select('away', \STM\Utils\Arrays::getAssocArray($teams, 'getId', '__toString'), false, 'inline'); ?><br />
    <input type="submit" value="<?php echo stmLang('form', 'show'); ?>" />
</form>

<hr />

<?php if ($matches): ?>

<?php
$display_wot = STM_POINT_WIN > STM_POINT_WIN_OT;
$display_lot = STM_POINT_LOSS_OT > STM_POINT_LOSS;
$your_teams = array();
include(STM_ADMIN_TEMPLATE_ROOT . 'result-table/index.php');
?>
<hr />

<h2><?php echo stmLang('statistics', 'head-to-head', 'last-matches'); ?></h2>
<table>
        <tr>
            <th><?php echo stmLang('competition', 'match'); ?></th>
            <th><?php echo stmLang('match', 'date'); ?></th>
            <th><?php echo stmLang('match', 'score'); ?></th>
        </tr>
        <?php foreach ($matches as $match): extract($match->toArray()); ?>
            <tr>
                <td><a href="index.php?module=match&action=read&type=match&m=<?php echo $match->getId(); ?>">
                    <?php echo $team_home . ' : ' . $team_away; ?>
                </a></td>
                <td><?php echo \STM\Utils\Dates::convertDatetimeToString($date, '-'); ?></td>
                <td><?php echo $match->hasScore() ? ($score_home . ' : ' . $score_away) : '-:-'; ?></td>
            </tr>
        <?php endforeach; ?>
</table>

<?php else: ?>
<p><?php echo stmLang('no-content'); ?></p>
<?php endif;
