<?php
use STM\Match\Stats\Action\StatisticCalculationType;
?>

        <td><?php echo $team_stats['general']->playedMatches; ?></td>
        <td><?php echo $team_stats['general']->playedTime; ?></td>
        <?php foreach(array_keys($all_actions) as $id_action): ?>
        <td><?php
        if (isset($team_stats['actions'][$id_action])) {
            echo StatisticCalculationType::getValue($team_stats['actions'][$id_action]);
        } else {
            echo 0;
        } ?></td>
        <?php endforeach;
