<?php
use STM\Team\Member\TeamMember;

if ($team_name == '' && $team_member instanceof TeamMember) {
    $arr = $team_member->toArray();
    $team_name = $arr['team'];
}
include(STM_MODULES_ROOT . "Team/Views/_elements/links/profile.php");
