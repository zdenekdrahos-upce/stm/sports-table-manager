<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\Statistics;

use STM\Module\ModuleController;
use STM\Web\Message\SessionMessage;
use STM\StmCache;
use STM\Team\Member\TeamMember;
use STM\Match\Stats\Action\MatchPlayerStats;
use STM\Match\MatchSelection;
use STM\Match\Table\TablePoints;
use STM\Match\Table\ResultTable;
use STM\Person\Person;
use STM\Competition\Season\Season;

class StatisticsController extends ModuleController
{
    /** @var \Person */
    private $person;
    /** @var \TeamMember */
    private $teamMember;

    protected function __construct()
    {
        parent::__construct();
        parent::setMainPage('index.php?module=statistics');
        $this->loadSelectedPerson();
        $this->loadSelectedTeamMember();
    }

    /** index.php?module=statistics */
    public function index()
    {
        parent::setContent();
    }

    /** index.php?module=statistics&action=read&type=team&t=ID_TEAM[&c=ID_COMPETITION] */
    public function readTeam()
    {
        if ($this->checkSelectedTeam()) {
            $positions = false;
            if ($this->competition instanceof Season) {
                 $all_positions = StmCache::getTeamsPositionsInTable($this->competition);
                 $positions = $all_positions->getTeamPositions($this->team->getId());
            }
            parent::setProfileInLeftMenu('team');
            parent::setContent(
                array(
                    'stats' => $this->getTeamStatistics(),
                    'competitions' => $this->entities->Competition->findByTeam($this->team),
                    'positions' => $positions
                )
            );
        }
    }

    private function getTeamStatistics()
    {
        if ($this->competition) {
            return StmCache::getStatisticsForTeamInCompetition($this->competition->getId(), $this->team->getId());
        } else {
            return StmCache::getStatisticsForTeam($this->team->getId());
        }
    }

    /** index.php?module=statistics&action=read&type=person_matches&PRS=ID_PERSON[&t=ID_TEAM] */
    public function readPersonMatches()
    {
        if ($this->checkSelectedPerson()) {
            parent::setProfileInLeftMenu('person');
            parent::setContent(
                array(
                    'actions' => $this->entities->MatchPlayerAction->findByPerson($this->person, $this->team),
                    'team_members' => $this->entities->TeamMember->findByPerson($this->person),
                )
            );
        }
    }

    /** index.php?module=statistics&action=read&type=player_matches&tm=ID_TEAM_PLAYER */
    public function readPlayerMatches()
    {
        if ($this->checkSelectedTeamMember()) {
            $_GET['t'] = $this->teamMember->getIdTeam(); // for delete-team in Team' profile links
            parent::setProfileInLeftMenu('team');
            parent::setContent(
                array(
                    'teamMember' => $this->teamMember,
                    'actions' => $this->entities->MatchPlayerAction->findByTeamPlayer($this->teamMember)
                )
            );
        }
    }

    /** index.php?module=statistics&action=read&type=person_players&PRS=ID_PERSON */
    public function readPersonPlayers()
    {
        if ($this->checkSelectedPerson()) {
            $stats = $this->entities->MatchPlayerStats->findByPerson(
                $this->person,
                $this->getMatchSelectionFilter()
            );
            parent::setProfileInLeftMenu('person');
            $this->setStatsContent($stats);
        }
    }

    /** index.php?module=statistics&action=read&type=team_players&t=ID_TEAM */
    public function readTeamPlayers()
    {
        if ($this->checkSelectedTeam()) {
            $stats = $this->entities->MatchPlayerStats->findByTeam(
                $this->team,
                $this->getMatchSelectionFilter()
            );
            parent::setProfileInLeftMenu('team');
            $this->setStatsContent($stats);
        }
    }

    /** index.php?module=statistics&action=read&type=competition_players&c=ID_COMPETITION */
    public function readCompetitionPlayers()
    {
        if ($this->checkSelectedCompetition()) {
            $stats = StmCache::getCompetitionPlayerStatistics($this->competition);
            parent::setProfileInLeftMenu('competition');
            parent::setContent(
                array(
                    'stats' => $stats->getStatistics(),
                    'all_actions' => $stats->getAllMatchActions(),
                )
            );
        }
    }

    private function setStatsContent($stats)
    {
        parent::setContent(
            array(
                'stats' => $stats->getStatistics(),
                'all_actions' => $stats->getAllMatchActions(),
                'competitions' => $this->getCompetitionsForFilter(),
                'team_members' => $this->person ?
                    $this->entities->TeamMember->findByPerson($this->person) : array(),
            )
        );
    }

    private function getMatchSelectionFilter()
    {
         $ms = array(
            'matchType' => MatchSelection::ALL_MATCHES,
            'loadScores' => true,
            'loadPeriods' => true,
        );
        if (parent::checkSelectedCompetition(false)) {
            $ms['competition'] = $this->competition;
        }
        if (parent::checkSelectedTeam(false)) {
            $ms['teams'] = array($this->team);
        }
        return $ms;
    }

    private function getCompetitionsForFilter()
    {
        $cs = parent::getCompetitionsSelection();
        if ($this->team) {
            $cs->setTeam($this->team);
        }
        return $this->entities->Competition->find($cs);
    }

    /** index.php?module=statistics&action=read&type=head_to_head */
    public function readHeadToHead()
    {
        parent::setContent(
            array_merge(
                $this->getDataForHeadToHead(),
                array('teams' => $this->entities->Team->findAll())
            )
        );
    }

    private function getDataForHeadToHead()
    {
        $data = array('matches' => array(), 'result_table' => false);
        if (isset($_POST['home']) && isset($_POST['away']) &&
            is_numeric($_POST['home']) && is_numeric($_POST['away']) && $_POST['home'] != $_POST['away']
        ) {
            $ms = array(
                'matchType' => MatchSelection::PLAYED_MATCHES,
                'loadScores' => true,
                'loadPeriods' => false,
                'teams' => array($_POST['home'], $_POST['away']),
                'headToHead' => true,
                'limit' => array('max' => 5),
                'order' => array(
                    'datetime' => false,
                )
            );
            $data['matches'] = $this->entities->Match->find($ms);
            if ($data['matches']) {
                $data['result_table'] = ResultTable::getTable($ms, new TablePoints(), STM_MATCH_PERIODS);
            }
        }
        return $data;
    }

    private function loadSelectedTeamMember()
    {
        $this->teamMember = isset($_GET['tm']) ? $this->entities->TeamMember->findById($_GET['tm']) : false;
    }

    private function checkSelectedTeamMember()
    {
        if (!($this->teamMember instanceof TeamMember)) {
            $this->session->setMessageFail(SessionMessage::get('invalid', array(stmLang('team', 'player'))));
            parent::redirectToIndexPage();
        }
        return true;
    }

    private function checkSelectedPerson()
    {
        if (!($this->person instanceof Person)) {
            $this->session->setMessageFail(SessionMessage::get('invalid', array(stmLang('club', 'person'))));
            parent::redirectToIndexPage();
        }
        return true;
    }

    private function loadSelectedPerson()
    {
        $this->person = isset($_GET['prs']) ? $this->entities->Person->findById($_GET['prs']) : false;
    }

    protected function getProfileLinks()
    {
        return array(
            'person' => $this->person,
            'team_name' => $this->team ? $this->team->__toString() : '',
            'competition' => $this->competition,
            'team_member' => $this->teamMember,
        );
    }
}
