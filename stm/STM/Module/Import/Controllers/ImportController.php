<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\Import;

use STM\Module\ModuleController;
use STM\Import\IImport;
use STM\Import\Matches\ImportMatches;
use STM\Import\Teams\ImportTeams;
use STM\Import\Persons\ImportPersons;
use STM\Import\SeasonTable\ImportSeasonTable;
use STM\Competition\Season\Season;

class ImportController extends ModuleController
{
    protected function __construct()
    {
        parent::__construct();
        parent::setMainPage('index.php?module=import');
    }

    /** index.php?module=import */
    public function index()
    {
        parent::setContent();
    }

    /** index.php?module=import&action=create&type=matches */
    public function createMatches()
    {
        $this->importCreate(new ImportMatches());
    }

    /** index.php?module=import&action=create&type=teams */
    public function createTeams()
    {
        $this->importCreate(new ImportTeams());
    }

    /** index.php?module=import&action=create&type=persons */
    public function createPersons()
    {
        $this->importCreate(new ImportPersons());
    }

    /** index.php?module=import&action=update&type=table */
    public function updateTable()
    {
        if (empty($_POST) && $this->competition instanceof Season) {
            $_POST['competition'] = $this->competition->getId();
        }
        $this->importCreate(new ImportSeasonTable($this->competition));
    }

    /** index.php?module=import&action=create&type=... */
    private function importCreate(IImport $import)
    {
        $current_step = $this->getCurrentStep($import->getMaxStep());
        $method = "step{$current_step}";
        $result = $import->$method();
        if ($result === true) {
            redirect($_SERVER['REQUEST_URI']);
        } elseif (is_array($result)) {
            $this->includeProggresBar($current_step, $import->getMaxStep());
            parent::setContent($result, "{$_GET['type']}/step-{$current_step}");
        } else {
            $this->includeProggresBar($current_step, $import->getMaxStep());
            parent::setFormErrors($import);
            parent::setTextContent(
                '<hr /><a href="javascript:history.back()" >' . stmLang('import', 'go-to-previous') . '</a>'
            );
        }
    }

    private function includeProggresBar($current_step, $max_step)
    {
        parent::setTextContent(
            stmLang('import', 'step') . ": <strong>{$current_step} / {$max_step}</strong><br /><br />"
        );
    }

    private function getCurrentStep($max_step)
    {
        if (isset($_SESSION['import'])) {
            return $max_step;
        } else {
            for ($i = 1; $i <= $max_step; $i++) {
                if (isset($_POST["step-{$i}"])) {
                    return $i;
                }
            }
        }
        return 1;
    }
}
