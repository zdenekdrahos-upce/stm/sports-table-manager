<?php
/* Import main page */
$import = array(
    'create' => array('matches', 'teams', 'persons'),
    'update' => array('table')
);
?>

<ul>
    <?php foreach ($import as $action => $types): ?>
    <?php foreach ($types as $type): ?>
    <li><a href="<?php echo buildUrl(array('action' => $action, 'type' => $type)); ?>"><?php echo stmLang('header', 'import', $action, $type); ?></a></li>
    <?php endforeach; ?>
    <?php endforeach; ?>
</ul>
