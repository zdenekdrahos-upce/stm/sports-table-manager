<?php
/*
 * Import season table - 1st step
 */
?>


<form method="POST" action="<?php echo $_SERVER['REQUEST_URI']; ?>">

    <label for="competition" class="inline"><?php echo stmLang('competition', 'season'); ?></label>
    <?php \STM\Web\HTML\Forms::select('competition', \STM\Utils\Arrays::getAssocArray($competitions, 'getId', '__toString'), false, 'inline'); ?><br />

    <input type="checkbox" id="all_tables" name="all_tables" class="inline" />
    <label for="all_tables" class="inline"><?php echo stmLang('import', 'table-all'); ?></label><br /><br />

    <input type="submit" name="step-2" value="<?php echo stmLang('import', 'go-to-next'); ?>" />
</form>
