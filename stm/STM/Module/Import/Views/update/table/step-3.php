<?php
/*
 * Import season table - 3rd step
 */
?>

<h2><?php echo stmLang('import', 'summary'); ?></h2>
<ul>
    <li><?php echo stmLang('competition', 'season'); ?>: <strong><?php echo $import['season_name']; ?></strong></li>
    <li><a href="?module=season&action=read&type=tables&c=<?php echo $import['id_competition']; ?>"><?php echo stmLang('header', 'season', 'read', 'tables'); ?></a></li>
</ul>
