<?php
/*
 * Import season table - 2nd step
 */
$keys = array(
  'win', 'winInOt', 'draw', 'lossInOt', 'loss', 'goalFor', 'goalAgainst',
);
?>

<ul>
    <li><?php echo stmLang('competition', 'season') . ': <strong>' . $season . '</strong>'; ?></li>
</ul>

<form method="POST" action="<?php echo $_SERVER['REQUEST_URI']; ?>">

    <input type="hidden" name="competition" value="<?php echo $_POST['competition']; ?>" />

    <?php foreach($tables as $name => $rows): ?>
    <hr />
    <h2><?php echo stmLang('competition', 'table', $name); ?></h2>
    <table>
        <tr>
            <th><?php echo stmLang('competition', 'team'); ?></th>
            <th><?php echo stmLang('competition', 'table', 'win'); ?></th>
            <?php if ($displayWinInOT): ?>
            <th><?php echo stmLang('competition', 'table', 'winInOt'); ?></th>
            <?php endif; ?>
            <th><?php echo stmLang('competition', 'table', 'draw'); ?></th>
            <?php if ($displayLossInOT): ?>
            <th><?php echo stmLang('competition', 'table', 'lossInOt'); ?></th>
            <?php endif; ?>
            <th><?php echo stmLang('competition', 'table', 'loss'); ?></th>
            <th><?php echo stmLang('competition', 'table', 'goalFor'); ?></th>
            <th><?php echo stmLang('competition', 'table', 'goalAgainst'); ?></th>
        </tr>
    <?php foreach($rows as $row): $id = "{$name}[{$row->idTeam}]"; ?>
        <tr>
            <td><?php echo $row->name; ?></td>
            <?php foreach($keys as $key): ?>
            
            <?php 
            if ($key == 'winInOt' && $displayWinInOT == false) {
                echo '<input type="hidden" name="' . $id . '[winInOt]" value="0" />';
            } else if ($key == 'lossInOt' && $displayWinInOT == false) {
                echo '<input type="hidden" name="' . $id . '[lossInOt]" value="0" />';
            } else { ?>            
            <td><input class="inline" type="text" size="1" name="<?php echo "{$id}[{$key}]"; ?>" value="<?php echo $row->$key; ?>" /></td>
            <?php } ?>
            
            <?php endforeach; ?>
        </tr>
    <?php endforeach; ?>
    </table>
    <?php endforeach; ?>

    <input type="submit" name="step-3" value="<?php echo stmLang('import', 'save'); ?>" />
</form>
