<?php
/*
 * Import - 2nd step
 */
?>


<form method="POST" action="<?php echo $_SERVER['REQUEST_URI']; ?>">

    <table>
        <thead>
            <tr>
                <th style="width: 10px">&nbsp;</th>
                <th><?php echo stmLang('person', 'team'); ?></th>
                <th><?php echo stmLang('team', 'club'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($teams as $key => $line):
                $checkbox_class = !empty($line['errors']) ? ' class="no"' : '';
                $is_checked = empty($line['errors']) ? ' checked="on"' : '';
            ?>
            <input type="hidden" name="<?php echo "teams[{$key}][original]"; ?>" value="<?php echo $line['original']; ?>" />
            <tr>
                <td <?php echo $checkbox_class; ?>><input type="checkbox" name="<?php echo "teams[{$key}][checked]"; ?>" value="1" <?php echo $is_checked; ?> /></td>
                <td>
                    <input class="inline" type="text" name="<?php echo "teams[{$key}][team]"; ?>" value="<?php echo $line['teams']['team']; ?>" />
                </td>
                <td><?php
                    if (is_object($line['teams']['club'])) {
                        $_POST["teams[{$key}][club]"] = $line['teams']['club']->__toString();
                        \STM\Web\HTML\Forms::select("teams[{$key}][club]", \STM\Utils\Arrays::getAssocArray($clubs, '__toString', '__toString'), true, 'inline');
                    } else {
                        echo '<input class="inline" type="text" name="teams[' . $key . '][club]" value="' . $line['teams']['club'] . '" />';
                    }
                ?></td>
            </tr>
            <?php if (!empty($line['errors'])): ?>
            <tr>
                <td class="no">&nbsp;</td>
                <td class="no"><?php echo $line['original']; ?></td>
                <td class="no"><?php echo implode('<br />', $line['errors']); ?></td>
            </tr>
            <?php endif; ?>
            <?php endforeach; ?>
        </tbody>
    </table>

    <input type="submit" name="step-3" value="<?php echo stmLang('import', 'save'); ?>" />
</form>
