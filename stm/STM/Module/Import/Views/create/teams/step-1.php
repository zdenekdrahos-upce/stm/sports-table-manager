<?php
/*
 * Import - 1st step
 */
?>


<form method="POST" action="<?php echo $_SERVER['REQUEST_URI']; ?>">

    <h2><label for="format" class="inline"><?php echo stmLang('import', 'format'); ?></label></h2>
    <input type="text" id="format" name="format" size="100" maxlength="100" value="" />
    <aside class="help">
        <h3><?php echo stmLang('import', 'format-symbols'); ?></h3>
        {T} = <?php echo stmLang('person', 'team'); ?>,
        {C} = <?php echo stmLang('team', 'club'); ?>,
        {W} = <?php echo stmLang('import', 'format-surrounding'); ?>
    </aside>

    <h2><label for="input"><?php echo stmLang('import', 'input'); ?></label></h2>
    <textarea id="input" name="input" cols="30" rows="15" style="width: 100%;"></textarea>

    <input type="submit" name="step-2" value="<?php echo stmLang('import', 'go-to-next'); ?>" />
</form>
