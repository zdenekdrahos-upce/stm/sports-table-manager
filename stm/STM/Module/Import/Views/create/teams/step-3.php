<?php
/*
 * Import - 3rd step
 */
?>

<h2><?php echo stmLang('import', 'summary'); ?></h2>
<ul>
    <li><?php echo stmLang('import', 'team-count'); ?>: <strong><?php echo $import['created_teams'] . ' / ' . $import['all_teams']; ?></strong></li>
    <li><?php echo stmLang('import', 'club-count'); ?>: <strong><?php echo $import['created_clubs'] . ' / ' . $import['all_clubs']; ?></strong></li>
    <li><a href="?module=team"><?php echo stmLang('header', 'team', 'index'); ?></a></li>
    <li><a href="?module=club"><?php echo stmLang('header', 'club', 'index'); ?></a></li>
</ul>

<?php if ($import['errors']): ?>
<h2><?php echo stmLang('import', 'errors-team'); ?></h2>
<aside class="errors">
<?php
foreach ($import['errors'] as $error) {
    echo '<h2>' . $error['text'] . '</h2>';
    echo '<ul><li>';
    echo implode('</li><li>', $error['errors']);
    echo '</li></ul>';
}
?>
</aside>
<?php endif;
