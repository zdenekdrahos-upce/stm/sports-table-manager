<?php
/*
 * Import - 2nd step
 */
?>


<form method="POST" action="<?php echo $_SERVER['REQUEST_URI']; ?>">

    <input type="hidden" name="competition" value="<?php echo $_POST['competition']; ?>" />

    <table>
        <thead>
            <tr>
                <th style="width: 10px">&nbsp;</th>
                <th><?php echo stmLang('match', 'home'); ?></th>
                <th><?php echo stmLang('match', 'away'); ?></th>
                <th><?php echo stmLang('match', 'date'); ?></th>
                <th><?php echo stmLang('match', 'season-round'); ?></th>
                <th><?php echo stmLang('match', 'periods'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($matches as $key => $line):
                $checkbox_class = !empty($line['errors']) ? ' class="no"' : '';
                $is_checked = empty($line['errors']) ? ' checked="on"' : '';
            ?>
            <input type="hidden" name="<?php echo "match[{$key}][original]"; ?>" value="<?php echo $line['original']; ?>" />
            <tr>
                <td <?php echo $checkbox_class; ?>><input type="checkbox" name="<?php echo "match[{$key}][checked]"; ?>" value="1" <?php echo $is_checked; ?> /></td>
                <td><?php
                    if (is_object($line['match']['home_team'])) {
                        $_POST["match[{$key}][home_team]"] = $line['match']['home_team']->__toString();
                    }
                    \STM\Web\HTML\Forms::select("match[{$key}][home_team]", \STM\Utils\Arrays::getAssocArray($teams, '__toString', '__toString'), true, 'inline');
                ?></td>
                <td><?php
                    if (is_object($line['match']['away_team'])) {
                        $_POST["match[{$key}][away_team]"] = $line['match']['away_team']->__toString();
                    }
                    \STM\Web\HTML\Forms::select("match[{$key}][away_team]", \STM\Utils\Arrays::getAssocArray($teams, '__toString', '__toString'), true, 'inline');
                ?></td>
                <td><input class="inline" type="text" size="30" name="<?php echo "match[{$key}][datetime]"; ?>" value="<?php echo $line['match']['datetime']; ?>" /></td>

                <?php if ($is_season): ?>
                <td><input class="inline" type="text" size="3" name="<?php echo "match[{$key}][round]"; ?>" value="<?php echo $line['match']['round']; ?>" /></td>
                <?php else: ?>
                <td><?php echo $line['match']['round'] ? $line['match']['round'] : '-'; ?></td>
                <?php endif; ?>

                <?php if (isset($line['match']['periods'])): ?>
                <td>
                    <?php foreach($line['match']['periods'] as $id => $period): ?>
                    <input class="inline" type="text" size="1" name="<?php echo "match[{$key}][periods][{$id}][score_home]"; ?>" value="<?php echo $period['score_home']; ?>" /> :
                    <input class="inline" type="text" size="1" name="<?php echo "match[{$key}][periods][{$id}][score_away]"; ?>" value="<?php echo $period['score_away']; ?>" /><br />
                    <?php endforeach; ?>
                </td>
                <?php else: ?>
                <td>-</td>
                <?php endif; ?>
            </tr>
            <?php if (!empty($line['errors'])): ?>
            <tr>
                <td class="no">&nbsp;</td>
                <td class="no" colspan="2"><?php echo $line['original']; ?></td>
                <td class="no" colspan="3"><?php echo implode('<br />', $line['errors']); ?></td>
            </tr>
            <?php endif; ?>
            <?php endforeach; ?>
        </tbody>
    </table>

    <input type="submit" name="step-3" value="<?php echo stmLang('import', 'save'); ?>" />
</form>
