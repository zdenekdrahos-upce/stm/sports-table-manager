<?php
/*
 * Import - 1st step
 */
?>


<form method="POST" action="<?php echo $_SERVER['REQUEST_URI']; ?>">

    <h2><label for="competition"><?php echo stmLang('competition', 'competition'); ?></label></h2>
    <?php \STM\Web\HTML\Forms::select('competition', \STM\Utils\Arrays::getAssocArray($competitions, 'getId', '__toString')); ?>

    <h2><label for="format" class="inline"><?php echo stmLang('import', 'format'); ?></label></h2>
    <input type="text" id="format" name="format" size="100" maxlength="100" value="" />
    <input type="checkbox" id="remove_invalid" name="remove_invalid" class="inline" />
    <label for="remove_invalid" class="inline"><?php echo stmLang('import', 'remove-invalid'); ?></label><br />
    <aside class="help">
        <h3><?php echo stmLang('import', 'format-symbols'); ?></h3>
        {H} = <?php echo stmLang('match', 'home'); ?>,
        {A} = <?php echo stmLang('match', 'away'); ?>,
        {D} = <?php echo stmLang('match', 'date'); ?>,
        {R} = <?php echo stmLang('match', 'season-round'); ?>,
        {V} = <?php echo stmLang('match', 'score-home'); ?>,
        {O} = <?php echo stmLang('match', 'score-away'); ?>,
        {W} = <?php echo stmLang('import', 'format-surrounding'); ?>
    </aside>

    <h2><label for="input"><?php echo stmLang('import', 'input'); ?></label></h2>
    <textarea id="input" name="input" cols="30" rows="15" style="width: 100%;"></textarea>

    <input type="submit" name="step-2" value="<?php echo stmLang('import', 'go-to-next'); ?>" />
</form>
