<?php
/*
 * Import - 3rd step
 */
?>

<h2><?php echo stmLang('import', 'summary'); ?></h2>
<ul>
    <li><?php echo stmLang('import', 'match-count'); ?>: <strong><?php echo $import['created_matches'] . ' / ' . $import['all_matches']; ?></strong></li>
    <li><?php echo stmLang('import', 'period-count'); ?>: <strong><?php echo $import['created_periods'] . ' / ' . $import['all_periods']; ?></strong></li>
    <li><a href="?module=competition&action=read&type=matches&c=<?php echo $import['id_competition']; ?>"><?php echo stmLang('header', 'competition', 'read', 'matches'); ?></a></li>
</ul>

<?php if ($import['errors']): ?>
<h2><?php echo stmLang('import', 'errors-match'); ?></h2>
<aside class="errors">
<?php
foreach ($import['errors'] as $error) {
    echo '<h2>' . $error['text'] . '</h2>';
    echo '<ul><li>';
    echo implode('</li><li>', $error['errors']);
    echo '</li></ul>';
}
?>
</aside>
<?php endif;
