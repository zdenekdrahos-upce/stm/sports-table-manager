<?php
/*
 * Import - 3rd step
 */
?>

<h2><?php echo stmLang('import', 'summary'); ?></h2>
<ul>
    <li>
        <?php echo stmLang('import', 'person-count'); ?>: 
        <strong><?php echo $import['created_persons'] . ' / ' . $import['all_persons']; ?></strong>
    </li>
    <li>
        <?php echo stmLang('import', 'team-member-count'); ?>: 
        <strong><?php echo $import['created_team_members'] . ' / ' . $import['all_team_members']; ?></strong>
    </li>
    <li><a href="?module=person"><?php echo stmLang('header', 'person', 'index'); ?></a></li>
</ul>

<?php if ($import['errors']): ?>
<h2><?php echo stmLang('import', 'errors-match'); ?></h2>
<aside class="errors">
<?php
foreach ($import['errors'] as $error) {
    echo '<h2>' . $error['text'] . '</h2>';
    echo '<ul><li>';
    echo implode('</li><li>', $error['errors']);
    echo '</li></ul>';
}
?>
</aside>
<?php endif;
