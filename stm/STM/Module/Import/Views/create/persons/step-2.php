<?php
/*
 * Import - 2nd step
 */
?>


<form method="POST" action="<?php echo $_SERVER['REQUEST_URI']; ?>">

    <table>
        <thead>
            <tr>
                <th style="width: 10px">&nbsp;</th>
                <th><?php echo stmLang('person', 'forename'); ?></th>
                <th><?php echo stmLang('person', 'surname'); ?></th>
                <th><?php echo stmLang('person', 'birth-date'); ?></th>
                <th><?php echo stmLang('person', 'team'); ?></th>
                <th><?php echo stmLang('person', 'position'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($persons as $key => $line):
                $checkbox_class = !empty($line['errors']) ? ' class="no"' : '';
                $is_checked = empty($line['errors']) ? ' checked="on"' : '';
                $person = $line['person']['person'];
                $team = $line['person']['team'];
            ?>
            <input type="hidden" name="<?php echo "person[{$key}][original]"; ?>" value="<?php echo $line['original']; ?>" />
            <tr>
                <td<?php echo $checkbox_class; ?>><input type="checkbox" name="<?php echo "person[{$key}][checked]"; ?>" value="1" <?php echo $is_checked; ?> /></td>
                <td><input class="inline" type="text" size="30" name="<?php echo "person[{$key}][forename]"; ?>" value="<?php echo $person['forename']; ?>" /></td>
                <td><input class="inline" type="text" size="30" name="<?php echo "person[{$key}][surname]"; ?>" value="<?php echo $person['surname']; ?>" /></td>
                <td><input class="inline" type="text" size="30" name="<?php echo "person[{$key}][birth_date]"; ?>" value="<?php echo $person['birth_date']; ?>" /></td>
                <td><?php
                    $selectedTeam = $team['team'] ? $team['team']->__toString() : '';
                    $_POST["person[{$key}][team]"] = $selectedTeam;
                    \STM\Web\HTML\Forms::select("person[{$key}][team]", $teams, true, 'inline');
                ?></td>
                <td><?php
                    $selectedPosition = $team['position'] ? $team['position']->__toString() : '';
                    $_POST["person[{$key}][position]"] = $selectedPosition;
                    \STM\Web\HTML\Forms::select("person[{$key}][position]", $positions, true, 'inline');
                ?></td>
            </tr>
            <?php if (!empty($line['errors'])): ?>
            <tr>
                <td class="no">&nbsp;</td>
                <td class="no" colspan="2"><?php echo $line['original']; ?></td>
                <td class="no" colspan="3"><?php echo implode('<br />', $line['errors']); ?></td>
            </tr>
            <?php endif; ?>
            <?php endforeach; ?>
        </tbody>
    </table>

    <input type="submit" name="step-3" value="<?php echo stmLang('import', 'save'); ?>" />
</form>
