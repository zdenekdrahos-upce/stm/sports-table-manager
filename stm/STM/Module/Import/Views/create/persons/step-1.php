<?php
/*
 * Import - 1st step
 */
?>


<form method="POST" action="<?php echo $_SERVER['REQUEST_URI']; ?>">

    <h2><label for="format" class="inline"><?php echo stmLang('import', 'format'); ?></label></h2>
    <input type="text" id="format" name="format" size="100" maxlength="100" value="" />
    <input type="checkbox" id="remove_invalid" name="remove_invalid" class="inline" />
    <label for="remove_invalid" class="inline"><?php echo stmLang('import', 'remove-invalid'); ?></label><br />
    <aside class="help">
        <h3><?php echo stmLang('import', 'format-symbols'); ?></h3>
        {F} = <?php echo stmLang('person', 'forename'); ?>,
        {S} = <?php echo stmLang('person', 'surname'); ?>,
        <h3><?php echo stmLang('form', 'optional'); ?></h3>
        {B} = <?php echo stmLang('person', 'birth-date'); ?>
        {T} = <?php echo stmLang('person', 'team'); ?>
        {P} = <?php echo stmLang('person', 'position'); ?>
        {W} = <?php echo stmLang('import', 'format-surrounding'); ?>
    </aside>

    <h2><label for="input"><?php echo stmLang('import', 'input'); ?></label></h2>
    <textarea id="input" name="input" cols="30" rows="15" style="width: 100%;"></textarea>

    <input type="submit" name="step-2" value="<?php echo stmLang('import', 'go-to-next'); ?>" />
</form>
