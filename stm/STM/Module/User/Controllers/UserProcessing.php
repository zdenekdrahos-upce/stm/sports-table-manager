<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\User;

use STM\Module\ModuleProcessing;
use STM\User\User;
use STM\User\UserValidator;
use STM\User\UserGroup;
use STM\Web\Message\SessionMessage;
use \stdClass;

class UserProcessing extends ModuleProcessing
{
    /** @var User */
    private $user;

    public function __construct($user = false)
    {
        $this->user = $user instanceof User ? $user : false;
        parent::__construct($this->user, '\STM\User\UserEvent');
        UserValidator::init($this->formProcessor);
    }

    public function createUser()
    {
        if (isset($_POST['create'])) {
            $new_group = UserGroup::getGroupname($_POST['user_group']);
            $this->formProcessor->escapeValues();
            $password = User::create(array('name' => $_POST['name'], 'id_user_group' => $_POST['user_group']));
            parent::checkAction(is_string($password), 'CREATE', "New {$new_group}: {$_POST['name']}");
            parent::setSessionMessage(
                $password,
                SessionMessage::get('create-user', array($_POST['name'], $password))
            );
            return $password;
        } else {
            $this->formProcessor->initVars(array('name', 'user_group'));
        }
        return false;
    }

    public function createCompetition()
    {
        if (isset($_POST['add']) && parent::isObjectSet()) {
            $this->formProcessor->escapeValues();
            $id = isset($_POST['competition']) ? $_POST['competition'] : false;
            $competition = $this->entities->Competition->findById($id);
            $add = $this->user->addManagedCompetition($competition);
            parent::checkAction($add, 'COMPETITION_MANAGER', "{$competition} add to {$this->user->getUsername()}");
            parent::setSessionMessage($add, SessionMessage::get('user-add-competition'));
            return $add;
        }
        return false;
    }

    public function resetPassword()
    {
        if (parent::isObjectSet()) {
            $password = $this->user->resetPassword();
            parent::setSessionMessage(
                $password,
                SessionMessage::get('user-reset', array($this->user->getUsername(), $password)),
                $this->getMessage('create-fail', stmLang('user', 'reset-pass'))
            );
            parent::log('UPDATE', "Reset password of {$this->user->getUsername()}", $password);
            return $password;
        }
        return false;
    }

    public function updateUserGroup()
    {
        if (isset($_POST['change']) && parent::isObjectSet()) {
            $new_group = UserGroup::getGroupname($_POST['user_group']);
            $this->formProcessor->escapeValues();
            $update = $this->user->updateUserGroup($_POST['user_group']);
            $message = "Change the user group of {$this->user->getUsername()}: ";
            $message .= "{$this->user->getUserGroup()} -> {$new_group}";
            parent::checkAction($update, 'UPDATE', $message);
            parent::setSessionMessage($update, $this->getMessage('change'));
            return $update;
        } else {
            $_POST['user_group'] = UserGroup::getIdGroup($this->user->getUserGroup());
        }
        return false;
    }

    public function deleteLogs()
    {
        $this->updatedObject = new stdClass();
        if (isset($_POST['delete']) && parent::canBeDeleted()) {
            if (isset($_POST['logs']) && is_array($_POST['logs'])) {
                $deleted_logs = array();
                foreach ($_POST['logs'] as $logname) {
                    if (file_exists(STM_LOG_ROOT . $logname . STM_EXT)) {
                        $deleted_logs[] = $logname;
                        unlink(STM_LOG_ROOT . $logname . STM_EXT);
                    }
                }
                $message = implode(', ', $deleted_logs ? $deleted_logs : $_POST['logs']);
                $delete = count($deleted_logs) > 0;
                parent::setSessionMessage(
                    $delete,
                    SessionMessage::get('delete-success', array('', stmLang('header', 'user', 'delete', 'logs'))),
                    SessionMessage::get('delete-fail', array('', stmLang('header', 'user', 'delete', 'logs')))
                );
                parent::log('DELETE_LOGS', $message, $delete);
                return true;
            }
        }
        return false;
    }

    public function deleteUser()
    {
        if (parent::canBeDeleted()) {
            if (!$this->user->isLastAdmin()) {
                $delete = $this->user->delete();
                parent::setSessionMessage(
                    $delete,
                    $this->getMessage('delete-success'),
                    $this->getMessage('delete-fail')
                );
                parent::log('DELETE', "{$this->user->getUsername()} ({$this->user->getUserGroup()})", $delete);
                return $delete;
            } else {
                $this->session->setMessageFail('You cannot delete accout of last admin');
            }
        }
        return false;
    }

    public function deleteCompetition($competition)
    {
        if (parent::canBeDeleted()) {
            if ($this->user->canUpdateCompetition($competition)) {
                $delete = $this->user->deleteManagedCompetition($competition);
                parent::setSessionMessage(
                    $delete,
                    $this->getMessage('delete-success', stmLang('competition', 'competition')),
                    $this->getMessage('delete-fail', stmLang('competition', 'competition'))
                );
                parent::log(
                    'COMPETITION_MANAGER',
                    "{$competition} revoked from {$this->user->getUsername()}",
                    $delete
                );
            } else {
                $this->session->setMessageFail(
                    "Invalid competition {$competition}, competition is not managed by user"
                );
            }
        }
    }

    private function getMessage($name, $user = false)
    {
        $user = $user || !parent::isObjectSet() ? $user : $this->user->getUsername();
        return SessionMessage::get($name, array(stmLang('user', 'user'), $user));
    }
}
