<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\User;

use STM\Module\ModuleController;
use STM\User\UserGroup;
use STM\Web\Message\SessionMessage;
use STM\Utils\Classes;

class UserController extends ModuleController
{
    /** @var \User */
    private $user;
    /** @var \STM\Module\User\UserProcessing */
    private $processing;

    protected function __construct()
    {
        parent::__construct();
        parent::setMainPage('index.php?module=user');
        $this->loadUser();
        $this->processing = new UserProcessing($this->user);
    }

    /** index.php?module=user */
    public function index()
    {
        parent::setIndexInLeftMenu();
        parent::setContent($this->getUsersFromUsergroup());
    }

    /** index.php?module=user&action=create&type=user */
    public function createUser()
    {
        if ($this->processing->createUser()) {
            parent::redirectToIndexPage();
        } else {
            parent::setIndexInLeftMenu();
            parent::setFormErrors($this->processing);
            parent::setContent();
        }
    }

    /** index.php?module=user&action=create&type=competition&u=ID_USER */
    public function createCompetition()
    {
        if ($this->checkSelectedUser()) {
            if ($this->user->getUserGroup() == UserGroup::COMPETITION_MANAGER) {
                if ($this->processing->createCompetition()) {
                    redirect(buildUrl(array('action' => 'read', 'type' => 'user')));
                } else {
                    $data = array(
                        'user' => $this->user,
                        'competitions' => $this->entities->Competition->findAll()
                    );
                    parent::setIndexInLeftMenu();
                    parent::setFormErrors($this->processing);
                    parent::setContent($data);
                }
            } else {
                $this->session->setMessageFail(SessionMessage::get('user-grant'));
                redirect(buildUrl(array('action' => 'read', 'type' => 'user')));
            }
        }
    }

    /** index.php?module=user&action=read&type=users */
    public function readUsers()
    {
        parent::setIndexInLeftMenu();
        parent::setContent($this->getUsersFromUsergroup());
    }

    /** index.php?module=user&action=read&type=user_groups */
    public function readUserGroups()
    {
        parent::setIndexInLeftMenu();
        parent::setContent(
            array(
                'visitor_access' => STM_ALLOW_VISITOR_ACCESS ? 'read' : 'no',
                'prediction_class' => STM_ALLOW_PREDICTIONS ? 'crud' : 'no',
            )
        );
    }

    /** index.php?module=user&action=read&type=inactive_users */
    public function readInactiveUsers()
    {
        parent::setIndexInLeftMenu();
        parent::setContent(array('users' => $this->entities->User->findNeverLoggedUsers()));
    }

    /** index.php?module=user&action=read&type=user&u=ID_USER */
    public function readUser()
    {
        if ($this->checkSelectedUser()) {
            $data = array(
                'user' => $this->user,
                'is_cm' => $this->user->getUserGroup() == UserGroup::COMPETITION_MANAGER,
                'competitions' => $this->entities->Competition->findManagedCompetitions($this->user)
            );
            parent::setIndexInLeftMenu();
            parent::setContent($data);
        }
    }

    /** index.php?module=user&action=read&type=logs */
    public function readLogs()
    {
        parent::setIndexInLeftMenu();
        $logs = array(
            'competitions', 'matches', 'filter', 'predictions', 'teams', 'users', 'database'
        );
        parent::setContent(array('logs' => $logs));
    }

    /** index.php?module=user&action=delete&type=logs */
    public function deleteLogs()
    {
        if ($this->processing->deleteLogs()) {
            redirect($_SERVER['REQUEST_URI']);
        } else {
            parent::setIndexInLeftMenu();
            $logs = array(
                'competitions', 'matches', 'filter', 'predictions', 'teams', 'users', 'database'
            );
            parent::setContent(array('logs' => $logs));
        }
    }

    /** index.php?module=user&action=update&type=reset_password&u=ID_USER */
    public function updateResetPassword()
    {
        if ($this->checkSelectedUser()) {
            $this->processing->resetPassword();
            parent::redirectToIndexPage();
        }
    }

    /** index.php?module=user&action=update&type=reset_password&u=ID_USER */
    public function updateUserGroup()
    {
        if ($this->checkSelectedUser()) {
            if ($this->processing->updateUserGroup()) {
                if ($this->isUserOwner()) {
                    $_SESSION['stmUser']['user_group'] = UserGroup::getGroupname($_POST['user_group']);
                }
                parent::redirectToIndexPage();
            } else {
                $data = array(
                    'username' => $this->user->getUsername(),
                    'current_group' => $this->user->getUserGroup()
                );
                parent::setIndexInLeftMenu();
                parent::setFormErrors($this->processing);
                parent::setContent($data);
            }
        }
    }

    /** index.php?module=user&action=delete&type=competition&u=ID_USER */
    public function deleteCompetition()
    {
        if ($this->checkSelectedUser()) {
            if ($this->user->getUserGroup() == UserGroup::COMPETITION_MANAGER) {
                $this->processing->deleteCompetition($this->competition);
                redirect(buildUrl(array('action' => 'read', 'type' => 'user')));
            } else {
                $this->session->setMessageFail(SessionMessage::get('user-revoke'));
                redirect(buildUrl(array('action' => 'read', 'type' => 'user')));
            }
        }
    }

    /** index.php?module=user&action=delete&type=user&u=ID_USER */
    public function deleteUser()
    {
        if ($this->checkSelectedUser()) {
            if ($this->isUserOwner()) {
                redirect('?module=profile');
            } else {
                $this->processing->deleteUser();
                parent::redirectToIndexPage();
            }
        }
    }

    // HELPERS METHODS
    /**
     * Selects user according to url parameter $_GET['group']
     * @return array
     */
    private function getUsersFromUsergroup()
    {
        if (isset($_GET['group'])) {
            $group = UserGroup::getGroupname(strtoupper($_GET['group']));
            if (Classes::isClassConstant('\STM\User\UserGroup', $group)) {
                $constants = array('a' => 'admins', 'm' => 'managers', 'c' => 'comp-managers', 'r' => 'registered');
                $users = $this->entities->User->findByUserGroup($group);
                $group = stmLang('user', $constants[$_GET['group']]);
            } else {
                redirect(buildUrl(array('group' => null)));
            }
        } else {
            $users = $this->entities->User->findAll();
            $group = stmLang('user', 'all');
        }
        return array('users' => $users, 'group' => $group);
    }

    /**
     * Checks if logged user is same edited user
     * @return boolean
     */
    private function isUserOwner()
    {
        if ($this->user) {
            return $this->session->getUserId() == $this->user->getId();
        }
        return false;
    }

    private function checkSelectedUser()
    {
        if (!$this->user) {
            $this->session->setMessageFail(SessionMessage::get('invalid', array(stmLang('user', 'user'))));
            parent::redirectToIndexPage();
        }
        return true;
    }

    private function loadUser()
    {
        $this->user = isset($_GET['u']) ? $this->entities->User->findById($_GET['u']) : false;
    }
}
