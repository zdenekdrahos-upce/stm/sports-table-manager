<?php
/**
 * List of all users + links to delete, create new user, ...
 * $users       users from application
 */
?>

<ul class="vertical">
    <li><a href="<?php echo buildUrl(array('group' => null)); ?>"><?php echo stmLang('user', 'all'); ?></a></li>
    <li><a href="<?php echo buildUrl(array('group' => 'r')); ?>"><?php echo stmLang('user', 'registered'); ?></a></li>
    <li><a href="<?php echo buildUrl(array('group' => 'c')); ?>"><?php echo stmLang('user', 'comp-managers'); ?></a></li>
    <li><a href="<?php echo buildUrl(array('group' => 'm')); ?>"><?php echo stmLang('user', 'managers'); ?></a></li>
    <li><a href="<?php echo buildUrl(array('group' => 'a')); ?>"><?php echo stmLang('user', 'admins'); ?></a></li>
</ul>

<h2><?php echo $group; ?></h2>
<table>
    <tr>
        <th><?php echo stmLang('name'); ?></th>
        <th><?php echo stmLang('team', 'links'); ?></th>
    </tr>
<?php foreach ($users as $user): ?>
    <tr>
        <td>
            <a href="<?php echo buildUrl(array('action' => 'read', 'type' => 'user', 'u' => $user->getId(), 'group' => null)); ?>"><?php echo $user->getUsername(); ?></a>
            (<?php echo stmLang('constants', 'usergroup', $user->getUserGroup()); ?>)
        </td>
        <td>
            <a href="<?php echo buildUrl(array('action' => 'update', 'type' => 'user_group', 'u' => $user->getId())); ?>"><?php echo stmLang('user', 'change-group'); ?></a>,
            <a href="<?php echo buildUrl(array('action' => 'update', 'type' => 'reset_password', 'u' => $user->getId())); ?>"><?php echo stmLang('user', 'reset-pass'); ?></a>,
            <?php include(STM_MODULES_ROOT . 'User/Views/_elements/delete/user.php'); ?>
        </td>
    </tr>
<?php endforeach; ?>
</table>
