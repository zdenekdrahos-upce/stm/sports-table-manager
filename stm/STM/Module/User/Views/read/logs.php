<?php
/**
 * User Activity (logs)
 * $logs    name of logs
 */
?>

<ul>
    <?php foreach($logs as $name): ?>
    <li><a href="javascript:toggleOneDiv('<?php echo $name; ?>', 'toogle');"><?php echo stmLang('user', 'logs', $name); ?></a></li>
    <?php endforeach; ?>
</ul>

<?php foreach($logs as $name): ?>
<div class="toogle" name="<?php echo $name; ?>">
    <?php
    if (file_exists(STM_LOG_ROOT . $name . STM_EXT)) {
        echo '<code>' . file_get_contents(STM_LOG_ROOT . $name . STM_EXT) . '</code>';
    } else {
        echo '<p>' . stmLang('no-content') . '</p>';
    }
    ?>
</div>
<?php endforeach;
