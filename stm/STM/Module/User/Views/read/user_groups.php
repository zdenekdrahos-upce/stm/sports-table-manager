<?php
/**
 * Info page about user groups in STM
 * $visitor_access      if unregistred user has access to STM
 * $prediction_class
 */
?>

<table class="user_groups">
    <tr>
        <th class="corner">&nbsp;</th>
        <th><?php echo stmLang('user', 'admins'); ?></th>
        <th><?php echo stmLang('user', 'managers'); ?></th>
        <th><?php echo stmLang('user', 'comp-managers'); ?></th>
        <th><?php echo stmLang('user', 'registered'); ?></th>
        <th><?php echo stmLang('user', 'visitors'); ?></th>
    </tr>
    <tr>
        <th><?php echo stmLang('user', 'groupinfo', 'competitions'); ?></th>
        <td class="crud">&nbsp;</td>
        <td class="crud">&nbsp;</td>
        <td class="read">&nbsp;</td>
        <td class="read">&nbsp;</td>
        <td class="<?php echo $visitor_access; ?>">&nbsp;</td>
    </tr>
    <tr>
        <th><?php echo stmLang('user', 'groupinfo', 'matches'); ?></th>
        <td class="crud">&nbsp;</td>
        <td class="crud">&nbsp;</td>
        <td class="crud">&nbsp;</td>
        <td class="read">&nbsp;</td>
        <td class="<?php echo $visitor_access; ?>">&nbsp;</td>
    </tr>
    <tr>
        <th><?php echo stmLang('user', 'groupinfo', 'predictions'); ?></th>
        <td class="<?php echo $prediction_class; ?>">&nbsp;</td>
        <td class="<?php echo $prediction_class; ?>">&nbsp;</td>
        <td class="<?php echo $prediction_class; ?>">&nbsp;</td>
        <td class="<?php echo $prediction_class; ?>">&nbsp;</td>
        <td class="no">&nbsp;</td>
    </tr>
    <tr>
        <th><?php echo stmLang('user', 'groupinfo', 'teams'); ?></th>
        <td class="crud">&nbsp;</td>
        <td class="crud">&nbsp;</td>
        <td class="read">&nbsp;</td>
        <td class="read">&nbsp;</td>
        <td class="no">&nbsp;</td>
    </tr>
    <tr>
        <th><?php echo stmLang('user', 'groupinfo', 'users'); ?></th>
        <td class="crud">&nbsp;</td>
        <td class="no">&nbsp;</td>
        <td class="no">&nbsp;</td>
        <td class="no">&nbsp;</td>
        <td class="no">&nbsp;</td>
    </tr>
</table>

<ul>
    <li><?php echo stmLang('user', 'groupinfo', 'green'); ?></li>
    <li><?php echo stmLang('user', 'groupinfo', 'blue'); ?></li>
    <li><?php echo stmLang('user', 'groupinfo', 'red'); ?></li>
</ul>
