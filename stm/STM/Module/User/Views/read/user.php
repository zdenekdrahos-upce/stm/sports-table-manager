<?php
/**
 * Basic info page about user for ADMINISTRATORS
 * $user            selected user
 * $is_cm           determine if user group is CompetitionManager
 * $competitions    managed competitions
 */
?>

<ul>
    <li><?php echo stmLang('name'); ?>: <strong><?php echo $user->getUsername(); ?></strong></li>
    <li><?php echo stmLang('user', 'group'); ?>: <strong><?php echo stmLang('constants', 'usergroup', $user->getUserGroup()); ?></strong></li>
    <li><?php echo stmLang('user', 'date-reg'); ?>: <strong><?php echo $user->getDateRegistration(); ?></strong></li>
    <li><?php echo stmLang('user', 'last-login'); ?>: <strong><?php echo \STM\Utils\Dates::convertDatetimeToString($user->getDateLastLogin(), stmLang('user', 'never-login')); ?></strong></li>
</ul>

<?php if ($is_cm): ?>
<br />
<h3><?php echo stmLang('user', 'managed-competitions'); ?></h3>
<ul>
    <?php if ($competitions): ?>
        <?php foreach($competitions as $competition): ?>
        <li>
            <a href="<?php echo buildUrl(array('module' => 'competition', 'type' => 'competition', 'c' => $competition->getId(), 'u' => null)); ?>">
                <?php echo $competition; ?>
            </a>
            | <?php include(STM_MODULES_ROOT . 'User/Views/_elements/delete/competition.php'); ?>
        </li>
        <?php endforeach; ?>
    <?php else: ?>
        <p><?php echo stmLang('no-content'); ?></p>
    <?php endif; ?>
        <li><a href="<?php echo buildUrl(array('action' => 'create', 'type' => 'competition')); ?>"><strong><?php echo stmLang('user', 'grant-competition'); ?></strong></a></li>
</ul>
<?php endif;
