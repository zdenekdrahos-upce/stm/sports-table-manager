<?php
/**
 * List of inactive users
 * $users       never logged users
 */
?>

<table>
    <tr>
        <th><?php echo stmLang('user', 'user'); ?></th>
        <th><?php echo stmLang('user', 'date-reg'); ?></th>
        <th>&nbsp;</th>
    </tr>
    <?php foreach ($users as $user): ?>
    <tr>
        <td>
            <a href="<?php echo buildUrl(array('type' => 'user', 'u' => $user->getId(), 'group' => null)); ?>">
                <?php echo $user->getUsername(); ?>
            </a>
            (<?php echo stmLang('constants', 'usergroup', $user->getUserGroup()); ?>)
        </td>
        <td><?php echo $user->getDateRegistration(); ?></td>
        <td><?php include(STM_MODULES_ROOT . 'User/Views/_elements/delete/user.php'); ?></td>
    </tr>
    <?php endforeach; ?>
</table>
