<?php
use STM\User\UserGroup;
?>
<label for="user_group"><?php echo stmLang('user', 'group'); ?>: </label>
<?php
$option_array = array(
    'A' => stmLang('constants', 'usergroup', UserGroup::ADMIN),
    'M' => stmLang('constants', 'usergroup', UserGroup::MANAGER),
    'C' => stmLang('constants', 'usergroup', UserGroup::COMPETITION_MANAGER),
    'R' => stmLang('constants', 'usergroup', UserGroup::REGISTERED_USER)
);
\STM\Web\HTML\Forms::select('user_group', $option_array);
