    <h2><?php echo stmLang('left-menu-header'); ?></h2>
    <ul>
        <li><a href="index.php?module=user"><?php echo stmLang('header', 'user', 'index'); ?></a></li>
        <li><a href="index.php?module=user&action=read&type=inactive_users"><?php echo stmLang('header', 'user', 'read', 'inactive_users'); ?></a></li>
        <li><a href="index.php?module=user&action=create&type=user"><?php echo stmLang('header', 'user', 'create', 'user'); ?></a></li>
        <li><a href="index.php?module=user&action=read&type=user_groups"><?php echo stmLang('header', 'user', 'read', 'user_groups'); ?></a></li>
        <li><a href="index.php?module=user&action=read&type=logs"><?php echo stmLang('header', 'user', 'read', 'logs'); ?></a></li>
        <li><a href="index.php?module=user&action=delete&type=logs"><?php echo stmLang('header', 'user', 'delete', 'logs'); ?></a></li>
    </ul>
