<?php
$url = '?module=user&action=delete&type=user&u=' . $user->getId();
$submit_message = stmLang('form', 'delete');
$question = stmLang('user', 'delete-msg') . $user . '?';
include(STM_ADMIN_TEMPLATE_ROOT . 'form/delete-form.php');
