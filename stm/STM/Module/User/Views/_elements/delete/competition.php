<?php
$url = '?module=user&action=delete&type=competition&u=' . $_GET['u'] . '&c=' . $competition->getId();
$submit_message = stmLang('user', 'delete-competition');
$question = stmLang('user', 'delete-competition-msg') . $competition . '?';
include(STM_ADMIN_TEMPLATE_ROOT . 'form/delete-form.php');
