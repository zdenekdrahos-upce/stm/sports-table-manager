<?php
/**
 * Update user email (UserController::updateUserGroup)
 * $username            name of the selected user
 */
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <label for="current"><?php echo stmLang('user', 'user'); ?>:</label>
    <input readonly="readonly" type="text" id="current" size="40" value="<?php echo $username; ?>" />
    <?php include(STM_MODULES_ROOT . 'User/Views/_elements/form-parts/user-group.php'); ?>

    <input type="submit" name="change" value="<?php echo stmLang('form', 'change'); ?>" />
</form>
