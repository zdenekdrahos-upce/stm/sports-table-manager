<?php
/**
 * Delete logs
 * $logs    name of logs
 */
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">

    <?php \STM\Web\HTML\Forms::inputDeleteToken(); ?>

    <table>
        <tr>
            <th>&nbsp;</th>
            <th><?php echo stmLang('name'); ?></th>
            <th><?php echo stmLang('user', 'logs', 'filesize'); ?></th>
        </tr>
        <?php foreach($logs as $name): ?>
        <tr>
            <td><input id="<?php echo $name; ?>" type="checkbox" name="logs[]" value="<?php echo $name; ?>" /></td>
            <td><label for="<?php echo $name; ?>"><?php echo stmLang('user', 'logs', $name); ?></label></td>
            <td><?php echo file_exists(STM_LOG_ROOT . $name . STM_EXT) ? \STM\Utils\Files::humanFilesize(filesize(STM_LOG_ROOT . $name . STM_EXT)) : '0'; ?></td>
        </tr>
        <?php endforeach; ?>
    </table>

    <input type="submit" name="delete" value="<?php echo stmLang('form', 'delete'); ?>" />
</form>
