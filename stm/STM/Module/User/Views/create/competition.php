<?php
/**
 * Add (create) competition to user
 * $user                selected user
 * $competitions    list of all competitions
 */
use STM\Competition\CompetitionType;
use STM\Competition\CompetitionStatus;
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <select name="competition" size="1">
        <?php foreach($competitions as $competition): ?>
        <?php if(!$user->canUpdateCompetition($competition)): ?>
        <option value="<?php echo $competition->getId(); ?>">
            <?php echo $competition; ?>
            (<?php echo stmLang('constants', 'competitiontype', CompetitionType::getCompetitionType($competition->getIdCompetitionType())); ?>,
             <?php echo stmLang('constants', 'competitionstatus', CompetitionStatus::getStatus($competition->getStatusId())); ?>)
        </option>
        <?php endif; ?>
        <?php endforeach; ?>
    </select>
    <input type="submit" name="add" value="<?php echo stmLang('form', 'create'); ?>" />
</form>
