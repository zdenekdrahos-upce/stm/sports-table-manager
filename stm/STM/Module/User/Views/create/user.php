<?php
/**
 * Create new user (UserController::createUser)
 */
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <?php include(STM_MODULES_ROOT . 'User/Views/_elements/form-parts/name.php'); ?>
    <?php include(STM_MODULES_ROOT . 'User/Views/_elements/form-parts/user-group.php'); ?>

    <input type="submit" name="create" value="<?php echo stmLang('form', 'create'); ?>" />
</form>
