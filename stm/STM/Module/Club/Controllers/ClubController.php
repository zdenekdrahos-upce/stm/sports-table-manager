<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\Club;

use STM\Module\Controller;
use STM\Web\Message\SessionMessage;
use STM\Club\Club;
use STM\Club\Member\ClubMember;
use STM\Utils\Arrays;

class ClubController extends Controller
{
    /** @var \Club */
    private $club;
    /** @var \ClubMember */
    private $clubMember;
    /** @var \STM\Module\Club\ClubProcessing */
    private $processing;

    protected function __construct()
    {
        parent::__construct();
        parent::setMainPage('index.php?module=club');
        $this->loadSelectedClub();
        $this->loadSelectedClubMember();
        $this->processing = new ClubProcessing($this->club);
    }

    /** index.php?module=club */
    public function index()
    {
        parent::setIndexInLeftMenu();
        parent::setContent(array('clubs' => $this->entities->Club->findAll()), 'index');
    }

    /** index.php?module=club&action=create&type=club */
    public function createClub()
    {
        if ($this->processing->createClub()) {
            parent::redirectToIndexPage();
        } else {
            $data = array(
                'stadiums' => $this->entities->Stadium->findAll(),
                'countries' => $this->entities->Country->findAll(),
            );
            parent::setIndexInLeftMenu();
            parent::setFormErrors($this->processing);
            parent::setContent($data);
        }
    }

    /** index.php?module=club&action=create&type=member&CLB=ID_CLUB */
    public function createMember()
    {
        if ($this->checkSelectedClub()) {
            $persons = $this->entities->Person->findByFilter($_POST);
            $positions = Arrays::getAssocArray($this->entities->PersonPosition->findAll(), 'getId');
            if ($this->processing->createClubMember($persons, $positions)) {
                redirect(buildUrl(array('action' => 'read', 'type' => 'members')));
            } else {
                $data = array(
                    'persons' => $persons,
                    'positions' => $positions,
                    'teams' => $this->entities->Team->findAll()
                );
                parent::setProfileInLeftMenu();
                parent::setFormErrors($this->processing);
                parent::setContent($data);
            }
        }
    }

    /** index.php?module=club&action=read&type=club&CLB=ID_CLUB */
    public function readClub()
    {
        if ($this->checkSelectedClub()) {
            parent::setProfileInLeftMenu();
            parent::setContent(
                array(
                    'club' => $this->club,
                    'club_details' => $this->club->toArray()
                )
            );
        }
    }

    /** index.php?module=club&action=read&type=teams&CLB=ID_CLUB */
    public function readTeams()
    {
        if ($this->checkSelectedClub()) {
            parent::setProfileInLeftMenu();
            parent::setContent(
                array(
                    'teams' => $this->entities->Team->findByClub($this->club)
                )
            );
        }
    }

    /** index.php?module=club&action=read&type=teams&CLB=ID_CLUB */
    public function readMembers()
    {
        if ($this->checkSelectedClub()) {
            parent::setProfileInLeftMenu();
            parent::setContent(array('club_members' => $this->entities->ClubMember->findByClub($this->club)));
        }
    }

    /** index.php?module=club&action=update&type=club&CLB=ID_CLUB */
    public function updateClub()
    {
        if ($this->checkSelectedClub()) {
            if ($this->processing->updateClub()) {
                redirect(buildUrl(array('action' => 'read')));
            } else {
                $data = array(
                    'club' => $this->club,
                    'stadiums' => $this->entities->Stadium->findAll(),
                    'countries' => $this->entities->Country->findAll(),
                );
                parent::setProfileInLeftMenu();
                parent::setFormErrors($this->processing);
                parent::setContent($data);
            }
        }
    }

    /** index.php?module=club&action=update&type=member&CLB=ID_CLUB */
    public function updateMember()
    {
        if ($this->checkSelectedClubMember()) {
            if ($this->processing->updateMember($this->clubMember)) {
                redirect(buildUrl(array('action' => 'read', 'type' => 'members')));
            } else {
                $data = array(
                    'club_member' => $this->clubMember,
                    'positions' => $this->entities->PersonPosition->findAll(),
                );
                parent::setProfileInLeftMenu();
                parent::setFormErrors($this->processing);
                parent::setContent($data);
            }
        }
    }

    /** index.php?module=club&action=delete&type=member&CLB=ID_CLUB */
    public function deleteMember()
    {
        if ($this->checkSelectedClubMember()) {
            $this->processing->deleteMember($this->clubMember);
            redirect(buildUrl(array('action' => 'read', 'type' => 'members')));
        }
    }

    /** index.php?module=club&action=delete&type=member&CLB=ID_CLUB */
    public function deleteClub()
    {
        if ($this->checkSelectedClub()) {
            $this->processing->deleteClub();
            parent::redirectToIndexPage();
        }
    }

    private function checkSelectedClub()
    {
        if (!($this->club instanceof Club)) {
            $this->session->setMessageFail(SessionMessage::get('invalid', array(stmLang('team', 'club'))));
            parent::redirectToIndexPage();
        }
        return true;
    }

    private function checkSelectedClubMember()
    {
        if ($this->checkSelectedClub() && $this->clubMember instanceof ClubMember) {
            return true;
        } else {
            $this->session->setMessageFail(SessionMessage::get('invalid-member'));
            parent::redirectToIndexPage();
        }
    }

    private function loadSelectedClub()
    {
        $this->club = isset($_GET['clb']) ? $this->entities->Club->findById($_GET['clb']) : false;
    }

    private function loadSelectedClubMember()
    {
        if (isset($_POST['id_position']) && isset($_POST['id_person'])) {
            $person = $this->entities->Person->findById($_POST['id_person']);
            $position = $this->entities->PersonPosition->findById($_POST['id_position']);
            $this->clubMember = $this->entities->ClubMember->findById($this->club, $person, $position);
        }
    }

    protected function getProfileLinks()
    {
        return array('club_name' => $this->club->__toString());
    }
}
