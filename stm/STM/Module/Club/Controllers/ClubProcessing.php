<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\Club;

use STM\Module\ModuleProcessing;
use STM\Web\Message\FormError;
use STM\Web\Message\SessionMessage;
use STM\Club\Club;
use STM\Club\ClubValidator;
use STM\Club\Member\ClubMember;
use STM\Club\Member\ClubMemberValidator;
use STM\Utils\Arrays;

class ClubProcessing extends ModuleProcessing
{
    /** @var \Club */
    private $club;

    public function __construct($club)
    {
        $this->club = $club instanceof Club ? $club : false;
        parent::__construct($this->club, '\STM\Team\TeamEvent');
        ClubValidator::init($this->formProcessor);
        ClubMemberValidator::init($this->formProcessor);
    }

    public function createClub()
    {
        if (isset($_POST['create'])) {
            unset($_POST['create']);
            $this->formProcessor->escapeValues();
            $result = Club::create($_POST);
            parent::checkAction($result, 'CLUB', "New Club: {$_POST['name']}");
            parent::setSessionMessage($result, $this->getMessage('create', $_POST['name']));
            return $result;
        } else {
            $this->initFormFields();
        }
        return false;
    }

    public function createClubMember($persons, $positions)
    {
        if (parent::isObjectSet() && isset($_POST['create'])) {
            $this->formProcessor->escapeValues();
            $_POST['persons'] = isset($_POST['persons']) ? $_POST['persons'] : array();
            $count_all = 0;
            $count_created = 0;
            $persons = Arrays::getAssocArray($persons, 'getId');
            foreach ($_POST['persons'] as $person_array) {
                if (array_key_exists('id_person', $person_array)) {
                    $count_all++;
                    $this->formProcessor->resetErrors();
                    $attributes = array(
                        'club' => $this->club,
                        'person' => isset($person_array['id_person']) ?
                            $persons[$person_array['id_person']] : false,
                        'position' => isset($person_array['id_position']) ?
                            $positions[$person_array['id_position']] : false,
                    );
                    if (ClubMember::create($attributes)) {
                        $count_created++;
                    }
                }
            }
            if ($count_all == 0) {
                $this->formProcessor->addError(FormError::get('nothing-checked'));
                return false;
            } else {
                parent::setSessionMessage(
                    true,
                    SessionMessage::get('club-add-member', array($count_created, $count_all))
                );
                parent::log(
                    'CLUB',
                    "Add {$count_created}/{$count_all} members to {$this->club->__toString()} ({$this->club->getId()})",
                    true
                );
                return true;
            }
        }
        return false;
    }

    public function deleteClub()
    {
        if (parent::canBeDeleted()) {
            if ($this->club->canBeDeleted()) {
                $delete = $this->club->delete();
                parent::setSessionMessage(
                    $delete,
                    $this->getMessage('delete-success'),
                    $this->getMessage('delete-fail')
                );
                parent::log('CLUB', "Delete Club: {$this->club->__toString()} ({$this->club->getId()})", $delete);
            } else {
                $this->session->setMessageFail(SessionMessage::get('delete-club'));
            }
        }
    }

    public function updateClub()
    {
        if (parent::isObjectSet() && isset($_POST['update'])) {
            unset($_POST['update']);
            $this->formProcessor->escapeValues();
            $result = $this->club->update($_POST);
            parent::checkAction(
                $result,
                'CLUB',
                "Update Club Details of {$this->club->__toString()} ({$this->club->getId()})"
            );
            parent::setSessionMessage($result, $this->getMessage('change'));
            return $result;
        } else {
            $this->initFormFields();
            $this->checkFields();
        }
        return false;
    }

    public function updateMember($club_member)
    {
        if (isset($_POST['update']) && $club_member instanceof ClubMember) {
            $this->formProcessor->escapeValues();
            $new_position = $this->entities->PersonPosition->findById($_POST['new_position']);
            $result = $club_member->updatePersonPosition($new_position);
            parent::checkAction(
                $result,
                'CLUB',
                "Update Position of {$club_member} in {$this->club} ({$this->club->getId()})"
            );
            parent::setSessionMessage($result, $this->getMemberMessage('change', $club_member));
            return $result;
        } else {
            $this->formProcessor->initVars(array('id_club', 'id_person', 'id_position', 'new_position'));
        }
        return false;
    }

    public function deleteMember($club_member)
    {
        if (parent::canBeDeleted() && $club_member instanceof ClubMember) {
            $arr = $club_member->toArray();
            $delete = $club_member->delete();
            parent::setSessionMessage(
                $delete,
                $this->getMemberMessage('delete-success', $club_member),
                $this->getMemberMessage('delete-fail', $club_member)
            );
            parent::log(
                'CLUB',
                "Delete member {$club_member} [{$arr['position']}] from {$this->club} ({$this->club->getId()})",
                $delete
            );
        }
    }

    private function initFormFields()
    {
        $this->formProcessor->initVars(
            array(
                'name', 'city', 'foundation_date', 'club_info', 'club_colors', 'website',
                'address', 'email_contact', 'telephone_contact', 'id_stadium', 'id_country'
            )
        );
    }

    private function checkFields()
    {
        if (parent::isObjectSet()) {
            $current_values = $this->club->toArray();
            foreach ($_POST as $key => $value) {
                if (empty($value) && array_key_exists($key, $current_values)) {
                    $_POST[$key] = $current_values[$key];
                }
            }
            $_POST['id_stadium'] = empty($_POST['id_stadium']) ? $this->club->getIdStadium() : $_POST['id_stadium'];
            $_POST['id_country'] = empty($_POST['id_country']) ? $this->club->getIdCountry() : $_POST['id_country'];
        }
    }

    private function getMessage($name, $club = false)
    {
        $club = $club || !parent::isObjectSet() ? $club : $this->club->__toString();
        return SessionMessage::get($name, array(stmLang('team', 'club'), $club));
    }

    private function getMemberMessage($name, $member)
    {
        return SessionMessage::get($name, array(stmLang('club', 'member'), $member->__toString()));
    }
}
