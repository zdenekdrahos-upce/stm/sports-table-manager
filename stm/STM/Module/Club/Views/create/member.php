<?php
/*
 * Create new club member
 * $persons
 * $positions
 */
?>

<?php include(STM_MODULES_ROOT . 'Person/Views/_elements/filtering/form.php'); ?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">


    <table>
        <tr>
            <th>&nbsp;</th>
            <th><?php echo stmLang('team', 'person'); ?></th>
            <th><?php echo stmLang('person', 'position'); ?></th>
        </tr>
        <?php foreach($persons as $person): ?>
            <tr>
                <td><input id="<?php echo $person->getId(); ?>" type="checkbox" name="persons[<?php echo $person->getId(); ?>][id_person]" value="<?php echo $person->getId(); ?>" /></td>
                <td><label for="<?php echo $person->getId(); ?>"><?php echo $person; ?></label></td>
                <td><?php \STM\Web\HTML\Forms::select("persons[{$person->getId()}][id_position]", $positions, false, 'inline'); ?></td>
            </tr>
        <?php endforeach; ?>
    </table>

    <input type="submit" name="create" value="<?php echo stmLang('form', 'create'); ?>" />
</form>
