<?php
/**
 * Create new club
 */
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <?php include(STM_MODULES_ROOT . 'Club/Views/_elements/form-parts/club.php'); ?>

    <input type="submit" name="create" value="<?php echo stmLang('form', 'create'); ?>" />

</form>
