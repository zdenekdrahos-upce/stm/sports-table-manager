<?php
/*
 * Detail of selected club
 * $club            selected club
 * $club_details    from \STM\Club\Club::toArray
 */
?>

<ul>
    <li><?php echo stmLang('name'); ?>: <strong><?php echo $club; ?></strong></li>
    <li><?php echo stmLang('club', 'city'); ?>: <strong><?php echo $club_details['city']; ?></strong></li>
    <li><?php echo stmLang('club', 'foundation'); ?>: <strong><?php echo $club_details['foundation_date']; ?></strong></li>
    <li><?php echo stmLang('club', 'info'); ?>:
        <aside class="text"><strong><?php echo $club_details['club_info']; ?></strong></aside>
    </li>
    <li><?php echo stmLang('club', 'colors'); ?>: <strong><?php echo $club_details['club_colors']; ?></strong></li>
    <li><?php echo stmLang('club', 'website'); ?>: <strong><?php echo $club_details['website'] ? ('<a href="' . (!is_int(stristr($club_details['website'], 'http://')) ? $club_details['website'] : ('http://' . $club_details['website'])) . '" target="_blank">' . $club_details['website'] . '</a>') : ''; ?></strong></li>
    <li><?php echo stmLang('club', 'address'); ?>: <strong><?php echo $club_details['address']; ?></strong></li>
    <li><?php echo stmLang('club', 'email'); ?>: <strong><?php echo $club_details['email_contact']; ?></strong></li>
    <li><?php echo stmLang('club', 'telephone'); ?>: <strong><?php echo $club_details['telephone_contact']; ?></strong></li>
    <li><?php echo stmLang('club', 'stadium'); ?>: <strong><?php echo $club_details['stadium'] ? ('<a href="' . buildUrl(array('module' => 'stadium', 'type' => 'stadium', 'clb' => null, 'std' => $club->getIdStadium())) . '" target="_blank">' . $club_details['stadium'] . '</a>') : ''; ?></strong></li>
    <li><?php echo stmLang('club', 'country'); ?>: <strong><?php echo $club_details['country']; ?></strong></li>
</ul>
