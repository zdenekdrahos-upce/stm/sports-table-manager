<?php
/*
 * List of persons whic are employed in the selected club
 * $club_members
 */
?>

<?php if (empty($club_members)): ?>
    <p><?php echo stmLang('no-content'); ?></p>
<?php else: ?>
    <table>
        <thead>
            <tr>
                <th><?php echo stmLang('club', 'member'); ?></th>
                <th><?php echo stmLang('person', 'position'); ?></th>
                <th><?php echo stmLang('team', 'links'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($club_members as $club_member): extract($club_member->toArray()); ?>
            <tr>
                <td><a href="?module=person&action=read&type=person&prs=<?php echo $club_member->getIdPerson();?>"><?php echo $person; ?></a></td>
                <td><?php echo $position; ?></td>
                <td>
                    <form method="post" action="<?php echo buildUrl(array('action' => 'update', 'type' => 'member'));?>" class="delete">
                        <?php include(STM_MODULES_ROOT . 'Club/Views/_elements/form-parts/members/id_members.php'); ?>
                        <input type="submit" name="go_to_update" value="<?php echo stmLang('club', 'position-update'); ?>," />
                    </form>
                    <?php include(STM_MODULES_ROOT . 'Club/Views/_elements/delete/member.php'); ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php endif;
