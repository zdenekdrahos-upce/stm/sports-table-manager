<?php
/*
 * Teams from selected club
 * $teams
 */
?>

<?php if (empty($teams)): ?>
    <p><?php echo stmLang('no-content'); ?></p>
<?php else: ?>
    <ul>
        <?php foreach($teams as $team): ?>
        <li><a href="?module=team&action=read&type=team&t=<?php echo $team->getId(); ?>"><?php echo $team; ?></a></li>
        <?php endforeach; ?>
    </ul>
<?php endif;
