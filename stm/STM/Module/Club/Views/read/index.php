<?php
/*
 * List of all clubs
 * $clubs
 */
?>

<?php if (empty($clubs)): ?>
    <p><?php echo stmLang('no-content'); ?></p>
<?php else: ?>
    <table>
        <tr>
            <th><?php echo stmLang('team', 'club'); ?></th>
            <th><?php echo stmLang('team', 'links'); ?></th>
        </tr>
        <?php foreach($clubs as $club): ?>
        <tr>
            <td><?php echo $club; ?></td>
            <td>
                <a href="?module=club&action=read&type=club&clb=<?php echo $club->getId(); ?>"><?php echo stmLang('header', 'club', 'read', 'club'); ?></a>,
                <a href="?module=club&action=read&type=teams&clb=<?php echo $club->getId(); ?>"><?php echo stmLang('menu', 'teams'); ?></a>,
                <a href="?module=club&action=read&type=members&clb=<?php echo $club->getId(); ?>"><?php echo stmLang('header', 'club', 'read', 'members'); ?></a>
            </td>
        </tr>
        <?php endforeach; ?>
    </table>
<?php endif;
