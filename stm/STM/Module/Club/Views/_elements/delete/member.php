<?php
$url = '?module=club&action=delete&type=member&clb=' . $_GET['clb'];
$submit_message = stmLang('club', 'delete-member');
$question = stmLang('club', 'delete-member-msg') . $person . " ({$club})";
?>
<form method="post" action="<?php echo $url; ?>" class="delete">
    <?php \STM\Web\HTML\Forms::inputDeleteToken(); ?>
    <?php include(STM_MODULES_ROOT . 'Club/Views/_elements/form-parts/members/id_members.php'); ?>
    <input type="submit" name="delete" value="<?php echo $submit_message; ?>" onclick="return confirm('<?php echo $question; ?>')" />
</form>
