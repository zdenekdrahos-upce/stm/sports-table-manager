    <h2><a href="<?php echo buildUrl(array('action' => 'read', 'type' => 'club')) ?>"><?php echo $club_name; ?></a></h2>
    <ul>
        <li><a href="<?php echo buildUrl(array('action' => 'read', 'type' => 'teams')) ?>"><?php echo stmLang('header', 'club', 'read', 'teams'); ?></a></li>
        <li><a href="<?php echo buildUrl(array('module' => 'team', 'action' => 'create', 'type' => 'team')) ?>"><?php echo stmLang('header', 'team', 'create', 'team'); ?></a></li>
        <li><a href="<?php echo buildUrl(array('action' => 'read', 'type' => 'members')) ?>"><?php echo stmLang('header', 'club', 'read', 'members'); ?></a></li>
        <li><a href="<?php echo buildUrl(array('action' => 'create', 'type' => 'member')) ?>"><?php echo stmLang('header', 'club', 'create', 'member'); ?></a></li>
        <li><a href="<?php echo buildUrl(array('action' => 'update', 'type' => 'club')) ?>"><?php echo stmLang('header', 'club', 'update', 'club'); ?></a></li>
        <li><?php include(STM_MODULES_ROOT . 'Club/Views/_elements/delete/club.php'); ?></li>
    </ul>
