    <h2><?php echo stmLang('form', 'required'); ?></h2>
    <label for="name"><?php echo stmLang('name'); ?>: </label>
    <input type="text" id="name" name="name" size="40" maxlength="50" value="<?php echo $_POST['name']; ?>" />

    <h2><?php echo stmLang('form', 'optional'); ?></h2>
    <label for="city"><?php echo stmLang('club', 'city'); ?>: </label>
    <input type="text" id="city" name="city" size="40" maxlength="50" value="<?php echo $_POST['city']; ?>" />
    <label for="foundation_date"><?php echo stmLang('club', 'foundation'); ?>: </label>
    <?php echo \STM\Web\HTML\Forms::inputDate('foundation_date'); ?>
    <label for="club_info"><?php echo stmLang('club', 'info'); ?>: </label>
    <textarea id="club_info" name="club_info" cols="32" rows="15"><?php echo $_POST['club_info']; ?></textarea>
    <label for="club_colors"><?php echo stmLang('club', 'colors'); ?>: </label>
    <input type="text" id="club_colors" name="club_colors" size="40" maxlength="60" value="<?php echo $_POST['club_colors']; ?>" />
    <label for="website"><?php echo stmLang('club', 'website'); ?>: </label>
    <input type="text" id="website" name="website" size="40" maxlength="60" value="<?php echo $_POST['website']; ?>" />
    <label for="address"><?php echo stmLang('club', 'address'); ?>: </label>
    <input type="text" id="address" name="address" size="40" maxlength="100" value="<?php echo $_POST['address']; ?>" />
    <label for="email"><?php echo stmLang('club', 'email'); ?>: </label>
    <input type="email" id="email" name="email_contact" size="40" maxlength="60" value="<?php echo $_POST['email_contact']; ?>" />
    <label for="telephone"><?php echo stmLang('club', 'telephone'); ?>: </label>
    <input type="text" id="telephone" name="telephone_contact" size="40" maxlength="20" value="<?php echo $_POST['telephone_contact']; ?>" />
    <label for="stadium"><?php echo stmLang('club', 'stadium'); ?>: </label>
    <?php \STM\Web\HTML\Forms::select('id_stadium', \STM\Utils\Arrays::getAssocArray($stadiums, 'getId', '__toString'), true); ?>
    <label for="country"><?php echo stmLang('club', 'country'); ?>: </label>
    <?php \STM\Web\HTML\Forms::select('id_country', \STM\Utils\Arrays::getAssocArray($countries, 'getId', '__toString'), true); ?>
