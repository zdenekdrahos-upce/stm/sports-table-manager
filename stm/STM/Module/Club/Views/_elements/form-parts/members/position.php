<?php
$select_name = isset($select_name) && $select_name == 'new_position' ? $select_name : 'id_position';
\STM\Web\HTML\Forms::select($select_name, \STM\Utils\Arrays::getAssocArray($positions, 'getId', '__toString'));
