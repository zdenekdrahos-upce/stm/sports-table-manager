<?php /* About stm */ ?>

<img src="web/images/stm-dark-logo.png" title="Logo of STM" class="right" />

<ul>
    <li>
        <?php echo stmLang('index', 'app'); ?>:
        <strong>Sports Table Manager</strong>
    </li>
    <li>
        <?php echo stmLang('index', 'version'); ?>:
        <strong><?php echo STM_VERSION;?></strong>
    </li>
    <li>
        <?php echo stmLang('index', 'author'); ?>:
        <strong>Zdeněk Drahoš</strong>
    </li>
    <li>
        <?php echo stmLang('index', 'licence'); ?>:
        <a href="http://opensource.org/licenses/BSD-3-Clause" target="blank"><strong>New BSD License</strong></a>
    </li>
    <li>
        <?php echo stmLang('index', 'website'); ?>:
        <a href="<?php echo STM_WEBSITE; ?>" target="blank"><strong><?php echo STM_WEBSITE; ?></strong></a>
    </li>
    <li>
        <?php echo stmLang('index', 'src'); ?>:
        <a href="https://bitbucket.org/stm-sport/sports-table-manager/src" target="blank"><strong>https://bitbucket.org/stm-sport/sports-table-manager/src</strong></a>
    </li>
</ul>
<br />

<?php
if (\STM\Libs\LanguageSwitch\Language::getSelectedLanguage() == 'czech') {
    include(STM_LANG_ROOT . '/about-page/czech.php');
} else {
    include(STM_LANG_ROOT . '/about-page/english.php');
}

