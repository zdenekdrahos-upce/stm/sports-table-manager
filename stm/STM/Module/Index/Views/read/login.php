<?php
/*
 * Login form
 */
?>

<div class="login">

    <form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
        <label for="username"><?php echo stmLang('index', 'username'); ?>:</label>
        <input type="text" id="username" name="username" value="<?php echo $_POST['username']; ?>" /><br />
        <label for="password"><?php echo stmLang('index', 'password'); ?>:</label>
        <input type="password" id="password" name="password" value="" /><br />
        <label>&nbsp;</label>
        <input type="submit" name="submit" value="<?php echo stmLang('index', 'login'); ?>" />
    </form>

</div>
