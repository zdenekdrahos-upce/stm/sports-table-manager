
<?php if ($teams): ?>
<table class="alignLeft">
    <tr>
        <th><?php echo stmLang('competition', 'team'); ?></th>
        <th><?php echo stmLang('team', 'links'); ?></th>
    </tr>
    <?php foreach ($teams as $team): ?>
        <tr>
            <td>
                <?php echo $team; ?>
            </td>
            <td>
                <?php 
                // upcoming match
                echo "<a href=\"?module=team&action=read&type=team&t={$team->getId()}\">"
                        . stmLang('header', 'team', 'read', 'team') ."</a>, "; 
                // upcoming match
                echo "<a href=\"?module=team&action=read&type=upcoming_match&t={$team->getId()}\">"
                        . stmLang('header', 'team', 'read', 'upcoming_match') ."</a>, "; 
                // team members
                echo "<a href=\"?module=team&action=read&type=members&t={$team->getId()}\">"
                        . stmLang('header', 'team', 'read', 'members') ."</a>, "; 
                // add player
                echo "<a href=\"?module=team&action=create&type=member&t={$team->getId()}\">"
                        . stmLang('index', 'homepage', 'add-player') ."</a>, "; 
                // competitions
                echo "<a href=\"?module=team&action=read&type=competitions&t={$team->getId()}\">"
                        . stmLang('menu', 'competitions') ."</a>"; 
                ?>
            </td>
        </tr>
    <?php endforeach; ?>
</table>
<?php else: ?>
<p><?php echo stmLang('index', 'homepage', 'no-teams'); ?></p>
<?php endif; ?>