
<table class="alignLeft">
    <?php if ($nextMatches): ?>
        <tr>
            <th><?php echo stmLang('match', 'date'); ?></th>
            <th><?php echo stmLang('competition', 'match'); ?></th>
            <th><?php echo stmLang('team', 'links'); ?></th>
        </tr>
        <?php foreach ($nextMatches as $match):
            $isHomeTeam = in_array($match->getIdTeamHome(), $yourTeamsId, true);
            $isAwayTeam = in_array($match->getIdTeamAway(), $yourTeamsId, true);
            extract($match->toArray());
        ?>
            <tr>
                <td><?php echo \STM\Utils\Dates::convertDatetimeToString($date, '-'); ?></td>
                <td><?php echo $team_home . ' : ' . $team_away; ?></td>
                <td>
                    <a href="index.php?module=match&action=create&type=result&m=<?php echo $match->getId(); ?>">
                        <?php echo stmLang('header', 'match', 'create', 'result'); ?></a>, 
                    <a href="index.php?module=match&action=update&type=match&m=<?php echo $match->getId(); ?>">
                        <?php echo stmLang('index', 'homepage', 'change-date'); ?></a>, 
                    <?php if (STM_ALLOW_PREDICTIONS): ?>
                    <a href="index.php?module=prediction&action=create&type=prediction&m=<?php echo $match->getId(); ?>">
                        <?php echo stmLang('match', 'new-prediction'); ?>
                    </a>
                    <?php endif; ?>
                </td>
            </tr>
        <?php endforeach; ?>
    <?php else: ?>
        <tr>
            <p><?php echo stmLang('no-content'); ?></p>
        </tr>
    <?php endif; ?>
</table>
