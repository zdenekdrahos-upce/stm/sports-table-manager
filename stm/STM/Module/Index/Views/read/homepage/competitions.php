
<?php if ($competitions): ?>
<table class="alignLeft">
    <tr>
        <th><?php echo stmLang('competition', 'competition'); ?></th>
        <th><?php echo stmLang('competition', 'category'); ?></th>
        <th class="width20"><?php echo stmLang('competition', 'type'); ?></th>
        <th><?php echo stmLang('team', 'links'); ?></th>
    </tr>
    <?php foreach ($competitions as $competition): ?>
        <tr>
            <td><?php echo $competition; ?></td>
            <td><?php 
                if ($competition->getIdCategory()) {
                    echo "<a href=\"?module=category&action=read&type=category&cat={$competition->getIdCategory()}\">"
                        . $competition->getNameCategory() ."</a>"; 
                } else {
                    echo '–';
                }
            ?></td>
            <td><?php 
                echo stmLang('constants', 'competitiontype', 
                    STM\Competition\CompetitionType::getCompetitionType($competition->getIdCompetitionType())); 
            ?></td>
            <td>
                <?php 
                // competition detail
                echo "<a href=\"?module=competition&action=read&type=competition&c={$competition->getId()}\">"
                        . stmLang('header', 'competition', 'read', 'competition') ."</a>, "; 
                // players stats
                echo "<a href=\"?module=statistics&action=read&type=competition_players&c={$competition->getId()}\">"
                        . stmLang('header', 'statistics', 'read', 'competition_players') ."</a>"; 
                ?>
            </td>
        </tr>
    <?php endforeach; ?>
</table>
<?php else: ?>
<p><?php echo stmLang('index', 'homepage', 'no-competitions'); ?></p>
<?php endif; ?>