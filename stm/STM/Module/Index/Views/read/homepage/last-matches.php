
<table class="alignLeft">
    <?php if ($lastMatches): ?>
        <tr>
            <th><?php echo stmLang('match', 'date'); ?></th>
            <th><?php echo stmLang('competition', 'match'); ?></th>
            <th><?php echo stmLang('match', 'score'); ?></th>
            <th><?php echo stmLang('team', 'links'); ?></th>
        </tr>
        <?php foreach ($lastMatches as $match):
            $isHomeTeam = in_array($match->getIdTeamHome(), $yourTeamsId, true);
            $isAwayTeam = in_array($match->getIdTeamAway(), $yourTeamsId, true);
            $lineup_type = $isHomeTeam ? 'lineup_home' : 'lineup_away';
            extract($match->toArray());
        ?>
            <tr>
                <td><?php echo \STM\Utils\Dates::convertDatetimeToString($date, '-'); ?></td>                
                <td><?php echo $team_home . ' : ' . $team_away; ?></td>
                <td><?php echo $match->hasScore() ? ($score_home . ' : ' . $score_away) : '-:-'; ?></td>
                <td>
                    <a href="?module=match&action=create&type=<?php echo "{$lineup_type}&m={$match->getId()}"; ?>">
                        <?php echo stmLang('header', 'match', 'create', $lineup_type); ?></a>, 
                    <a href="?module=match&action=read&type=action_player&m=<?php echo $match->getId(); ?>">
                        <?php echo stmLang('header', 'match', 'read', 'action_player'); ?>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
    <?php else: ?>
        <tr>
            <p><?php echo stmLang('no-content'); ?></p>
        </tr>
    <?php endif; ?>
</table>
