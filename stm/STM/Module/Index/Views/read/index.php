<?php 
/* Main index page (welcome page) 
 * $competitions
 * $matchesCount        default number of previous/next matches
 * $teams               teams marked as your_team at least in one competition
 * $yourTeamsId
 * $lastMatches
 * $nextMatches
 */ 
?>

<h2><?php echo stmLang('index', 'homepage', 'competitions'); ?></h2>
<?php include(__DIR__ . '/homepage/competitions.php'); ?>
<br />

<h2><?php echo stmLang('index', 'homepage', 'teams'); ?></h2>
<h3><?php echo stmLangf('index', 'homepage', 'last-matches', array($matchesCount)); ?></h3>
<?php include(__DIR__ . '/homepage/last-matches.php'); ?>
<br />

<h3><?php echo stmLangf('index', 'homepage', 'next-matches', array($matchesCount)); ?></h3>
<?php include(__DIR__ . '/homepage/next-matches.php'); ?>
<br />

<h3><?php echo stmLang('match', 'teams'); ?></h3>
<?php include(__DIR__ . '/homepage/teams.php'); ?>
<br />