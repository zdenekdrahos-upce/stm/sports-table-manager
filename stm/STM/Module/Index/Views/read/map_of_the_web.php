<?php /* Map of the web */ ?>

<?php
include(STM_LANG_ROOT . \STM\Libs\LanguageSwitch\Language::getSelectedLanguage() . STM_LANG_EXT);

foreach ($lang['header'] as $module => $actions) {
    echo '<ul class="map">';
    echo '<li class="header"><a href="' . buildUrl(array('module' => $module, 'action' => null, 'type' => null)) . '">' . $actions['index'] . '</a>';
    unset($actions['index']);
    foreach ($actions as $action => $types) {
        echo '<ul><li>';
        $all_keys = array_keys($types);
        foreach ($types as $type => $name) {
            $link = $module == 'index' ? "?type={$type}" : buildUrl(array('module' => $module, 'action' => $action, 'type' => $type));
            echo "<a href=\"{$link}\">{$name}</a>";
            if ($type != end($all_keys)) {
                echo ', ';
            }
        }
        echo '</li></ul>';
    }
    echo '</li></ul>';
}
