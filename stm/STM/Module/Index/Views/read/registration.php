<?php /* Registration page */ ?>


<div class="login">

    <form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
        <?php include(STM_MODULES_ROOT . 'User/Views/_elements/form-parts/name.php'); ?><br />
        <label>&nbsp;</label>
        <input type="submit" name="register" value="<?php echo stmLang('index', 'register'); ?>" />
    </form>

</div>

<hr />

<h2><?php echo stmLang('index', 'instructions'); ?></h2>
<ol>
    <li><?php echo stmLang('index', 'step-1'); ?></li>
    <li><?php echo stmLang('index', 'step-2'); ?></li>
    <li><?php echo stmLang('index', 'step-3'); ?></li>
    <li><?php echo stmLang('index', 'step-4'); ?></li>
    <li><?php echo stmLang('index', 'step-5'); ?></li>
    <li><?php echo stmLang('index', 'step-6'); ?></li>
    <li><?php echo stmLang('index', 'step-7'); ?></li>
</ol>

<h2><?php echo stmLang('index', 'advantages'); ?></h2>
<ul>
    <li><?php echo stmLang('index', 'access-team'); ?></li>
    <li><?php echo stmLang('index', 'predictions'); ?></li>
</ul>
