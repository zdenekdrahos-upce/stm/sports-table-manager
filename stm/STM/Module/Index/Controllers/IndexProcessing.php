<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\Index;

use STM\Web\Message\SessionMessage;
use STM\Web\Message\FormError;
use STM\User\User;
use STM\User\UserValidator;
use STM\Module\ModuleProcessing;

class IndexProcessing extends ModuleProcessing
{
    public function __construct()
    {
        parent::__construct(false, '\STM\User\UserEvent');
        UserValidator::init($this->formProcessor);
    }

    public function login()
    {
        if (isset($_POST['submit'])) {
            $this->formProcessor->escapeValues();
            $this->formProcessor->checkCondition(
                $this->isNotEmpty($_POST['username']),
                FormError::get('empty-value', array(stmLang('index', 'username')))
            );
            $this->formProcessor->checkCondition(
                $this->isNotEmpty($_POST['password']),
                FormError::get('empty-value', array(stmLang('index', 'password')))
            );
            if ($this->formProcessor->isValid()) {
                $user = User::authenticateUser($_POST['username'], $_POST['password']);
                parent::setSessionMessage($user, SessionMessage::get('login', array($_POST['username'])));
                parent::log('LOGIN', $_POST['username'], $user);
                if ($user) {
                    $this->session->login($user);
                    return true;
                } else {
                    $this->formProcessor->addError(FormError::get('login'));
                }
            }
        } else {
            $this->formProcessor->initVars(array('username', 'password'));
        }
        return false;
    }

    public function registration()
    {
        if (isset($_POST['register']) && STM_ALLOW_USER_REGISTRATION === true) {
            $this->formProcessor->escapeValues();
            $attributes = array('name' => $_POST['name'], 'id_user_group' => 'R');
            $password = User::create($attributes);
            parent::checkAction(is_string($password), 'REGISTER', $_POST['name']);
            if (is_string($password)) {
                $this->session->setMessageSuccess(
                    SessionMessage::get('registration', array($_POST['name'], $password))
                );
                return true;
            }
        } else {
            $this->formProcessor->initVars(array('name'));
        }
        return false;
    }

    private function isNotEmpty($value)
    {
        return !empty($value) || is_numeric($value);
    }
}
