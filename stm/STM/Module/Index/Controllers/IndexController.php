<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\Index;

use STM\Utils\Links;
use STM\Web\Message\SessionMessage;
use STM\User\UserEvent;
use STM\Module\Controller;
use STM\Module\Index\Homepage\HomepageDataLoader;

class IndexController extends Controller
{
    /** @var \STM\Module\Index\IndexProcessing */
    private $processing;

    protected function __construct()
    {
        parent::__construct();
        parent::setMainPage('index.php');
        $this->processing = new IndexProcessing();
    }

    /** index.php */
    public function index()
    {
        $loader = new HomepageDataLoader(5);
        parent::setContent($loader->getData());
    }

    /** index.php?type=login */
    public function readLogin()
    {
        if ($this->session->isLoggedIn() || $this->processing->login()) {
            if (isset($_SESSION['previous_page'])) {
                $previous = $_SESSION['previous_page'];
                unset($_SESSION['previous_page']);
                if (is_bool(strpos($previous, '?type=registration'))) {
                    // success message would be lost, because logged user is
                    // redirected to index.php from registration page
                    redirect($previous);
                }
            }
            parent::redirectToIndexPage();
        } else {
            $_SESSION['previous_page'] = Links::getPreviousPage('index.php');
            parent::setFormErrors($this->processing);
            parent::setContent();
        }
    }

    /** index.php?type=logout[&u=password] */
    public function readLogout()
    {
        if ($this->session->isLoggedIn()) {
            UserEvent::log(UserEvent::LOGOUT, $this->session->getUsername(), true);
            $this->session->logout();
            $this->setLogoutMessage();
            redirect('index.php?type=login');
        } else {
            parent::redirectToIndexPage();
        }
    }

    private function setLogoutMessage()
    {
        session_start();
        if (isset($_GET['u']) && $_GET['u'] == 'password') {
            $this->session->setMessageSuccess(SessionMessage::get('user-change-password'));
        } elseif (isset($_GET['u']) && $_GET['u'] == 'delete') {
            $this->session->setMessageSuccess(SessionMessage::get('user-delete-account'));
        } else {
            $this->session->setMessageSuccess(SessionMessage::get('user-logout'));
        }
    }

    /** index.php?type=registration */
    public function readRegistration()
    {
        if (STM_ALLOW_USER_REGISTRATION === true && !$this->session->isLoggedIn()) {
            if ($this->processing->registration()) {
                redirect('index.php?type=login');
            } else {
                parent::setFormErrors($this->processing);
                parent::setContent(array(), 'registration');
            }
        } else {
            parent::redirectToIndexPage();
        }
    }

    /** index.php?type=about */
    public function readAbout()
    {
        parent::setContent(array(), 'about_stm');
    }

    /** index.php?type=map */
    public function readMap()
    {
        parent::setContent(array(), 'map_of_the_web');
    }
}
