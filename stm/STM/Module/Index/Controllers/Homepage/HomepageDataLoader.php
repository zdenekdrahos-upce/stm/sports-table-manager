<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\Index\Homepage;

use STM\StmFactory;
use STM\Match\MatchSelection;
use STM\Competition\CompetitionSelection;

class HomepageDataLoader
{
    /** @var array */
    private $data;
    /** @var int */
    private $matchesCount;

    public function __construct($matchesCount)
    {
        $this->data = array();
        $this->matchesCount = $matchesCount;
        $this->loadCompetitions();
        $this->loadMatches();
        $this->loadTeams();
    }

    public function getData()
    {
        return $this->data;
    }

    private function loadCompetitions()
    {
        $selection = new CompetitionSelection();
        $selection->setDate('now');
        $this->data['competitions'] = StmFactory::find()->Competition->find($selection);
    }

    private function loadMatches()
    {
        $this->data['matchesCount'] = $this->matchesCount;
        $this->data['lastMatches'] = $this->getMatches(MatchSelection::PLAYED_MATCHES);
        $this->data['nextMatches'] = $this->getMatches(MatchSelection::UPCOMING_MATCHES);
    }

    private function getMatches($matchType)
    {
        $ascendingOrder = $matchType == MatchSelection::UPCOMING_MATCHES;
        $matchSelection = array(
            'matchType' => $matchType,
            'loadScores' => true,
            'loadPeriods' => false,
            'yourTeamsOnly' => true,
            'limit' => array('max' => $this->matchesCount),
            'order' => array(
                'datetime' => $ascendingOrder,
                'idMatch' => $ascendingOrder,
            )
        );
        if ($matchType == MatchSelection::UPCOMING_MATCHES) {
            $matchSelection['dates'] = array('min' => 'now - 3 weeks');
        }
        return StmFactory::find()->Match->find($matchSelection);
    }

    private function loadTeams()
    {
        $teams = StmFactory::find()->Team->findYourTeams();
        $this->data['teams'] = $teams;
        $yourTeamsId = array();
        foreach ($teams as $team) {
            $yourTeamsId[] = $team->getId();
        }
        $this->data['yourTeamsId'] = $yourTeamsId;
    }
}
