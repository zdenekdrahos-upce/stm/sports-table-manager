<?php
/**
 * Update score of the prediction
 * $match   predicted match
 */
?>

<h2><?php echo $match; ?></h2>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <?php include(STM_MODULES_ROOT . 'Prediction/Views/_elements/form-parts/prediction.php'); ?>

    <input type="submit" name="update" value="<?php echo stmLang('form', 'change'); ?>" />
</form>
