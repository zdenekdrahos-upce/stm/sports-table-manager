<?php
/*
 * Create new prediction
 * $match       predicted match
 */
?>

<ul>
    <li>
        <?php echo stmLang('competition', 'match'); ?>:
        <a href="<?php echo buildUrl(array('module' => 'match', 'action' => 'read', 'type' => 'match')); ?>"><?php echo $match; ?></a>
    </li>
</ul>

<h2><?php echo stmLang('prediction', 'prediction'); ?></h2>
<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <?php include(STM_MODULES_ROOT . 'Prediction/Views/_elements/form-parts/prediction.php'); ?>

    <input type="submit" name="create" value="<?php echo stmLang('form', 'create'); ?>" />
</form>
