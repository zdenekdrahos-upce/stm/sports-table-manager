<?php
/*
 * Prediction statistics of the competition
 * $competition_name
 * $is_season
 * $statistics      array
 */
?>

<ul>
    <li><?php echo stmLang('prediction', 'chart', 'count'); ?>: <strong><?php echo $statistics['predictions']; ?></strong></li>
    <li><?php echo stmLang('prediction', 'chart', 'success'); ?>: <strong><?php echo $statistics['success_predictions']; ?> (<?php echo \STM\Stats\Stats::getPercentage($statistics['success_predictions'], $statistics['predictions']); ?>%)</strong></li>
    <li><?php echo stmLang('prediction', 'chart', 'exact'); ?>: <strong><?php echo $statistics['exact_predictions']; ?> (<?php echo \STM\Stats\Stats::getPercentage($statistics['exact_predictions'], $statistics['predictions']); ?>%)</strong></li>
</ul>

<h2><?php echo stmLang('prediction', 'stats', 'how-many'); ?></h2>
<ul>
    <li><?php echo stmLang('prediction', 'stats', 'win'); ?>: <strong><?php echo $statistics['win_predictions']; ?> (<?php echo \STM\Stats\Stats::getPercentage($statistics['win_predictions'], $statistics['predictions']); ?>%)</strong></li>
    <li><?php echo stmLang('prediction', 'stats', 'draw'); ?>: <strong><?php echo $statistics['draw_predictions']; ?> (<?php echo \STM\Stats\Stats::getPercentage($statistics['draw_predictions'], $statistics['predictions']); ?>%)</strong></li>
    <li><?php echo stmLang('prediction', 'stats', 'loss'); ?>: <strong><?php echo $statistics['loss_predictions']; ?> (<?php echo \STM\Stats\Stats::getPercentage($statistics['loss_predictions'], $statistics['predictions']); ?>%)</strong></li>
</ul>
