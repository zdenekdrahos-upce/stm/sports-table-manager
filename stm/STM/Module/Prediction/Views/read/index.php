<?php
/*
 * List of all predictions of the logged user
 * $predictions
 */
use STM\Prediction\PredictionResult;
?>

<h2><a href="javascript:toggleDiv('toogleFilter');"><?php echo stmLang('match', 'filter', 'header'); ?></a></h2>
<div class="toogleFilter"<?php if (!empty($_POST)) echo ' id="displayed"' ?>>
<form id="filterForm" method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <label for="match_type" class="inline"><?php echo stmLang('match', 'filter', 'match-selection'); ?>:</label>
    <?php
    $options = array('A' => 'all-matches', 'P' => 'played-matches', 'U' => 'upcoming-matches');
    foreach ($options as $key => $value) {
        $options[$key] = stmLang('match', 'filter', $value);
    }
    \STM\Web\HTML\Forms::select('match_type', $options, false, 'inline');
    ?><br />
    <label for="team" class="inline"><?php echo stmLang('competition', 'team'); ?>:</label>
    <?php \STM\Web\HTML\Forms::select('team', \STM\Utils\Arrays::getAssocArray($teams, 'getId', '__toString'), true, 'inline');  ?><br />
    <label for="competition" class="inline"><?php echo stmLang('competition', 'competition'); ?>:</label>
    <?php \STM\Web\HTML\Forms::select('competition', \STM\Utils\Arrays::getAssocArray($competitions, 'getId', '__toString'), true, 'inline');  ?><br />

    <input type="submit" name="update" value="<?php echo stmLang('match', 'filter', 'submit'); ?>" />
</form>
</div>
<br />

<?php if (empty($predictions)): ?>
    <p><?php echo stmLang('no-content'); ?></p>
<?php else: ?>
<table>
    <tr>
        <th><?php echo stmLang('prediction', 'match'); ?></th>
        <th><?php echo stmLang('prediction', 'result'); ?></th>
        <th><?php echo stmLang('prediction', 'prediction'); ?></th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
    </tr>
    <?php foreach($predictions as $prediction):
        extract($prediction->toArray());
        $class = $prediction->isValid() ? '' : ' class="no"';
        $result = PredictionResult::getResult($prediction);
    ?>
    <tr>
        <td <?php echo $class; ?>><a href="<?php echo buildUrl(array('module' => 'match', 'action' => 'read', 'type' => 'match', 'm' => $prediction->getIdMatch())); ?>"><?php echo $match_name; ?></a></td>
        <td><?php echo $match_score; ?></td>
        <td><?php echo $prediction; ?></td>
        <td class="<?php echo strtolower($result); ?>"></td>
        <?php if ($result == PredictionResult::UNKNOWN): ?>
        <td><a href="<?php echo buildUrl(array('action' => 'update', 'type' => 'prediction', 'm' => $prediction->getIdMatch())); ?>"><?php echo stmLang('header', 'prediction', 'update', 'prediction'); ?></a></td>
        <?php else: ?>
        <td>&nbsp;</td>
        <?php endif; ?>
    </tr>
    <?php endforeach; ?>
</table>
<?php endif;
