<?php
/*
 * Prediction chart
 * $chart                           array
 * $logged_user                     name of the logged user
 * $competition_name, $is_season    optional parameteres, used if competition chart is displayed
 */
?>

<script type="text/javascript" src="web/scripts/jquery.tablesorter.min.js"></script>
<script type="text/javascript">
    $(function() {
        $("#tablesorter").tablesorter();
    });
</script>

<table id="tablesorter">
    <thead>
        <tr>
            <th><a href=""><?php echo stmLang('prediction', 'chart', 'username'); ?></a></th>
            <th><a href=""><?php echo stmLang('prediction', 'chart', 'count'); ?> (<?php echo stmLang('prediction', 'chart', 'cheat'); ?>)</a></th>
            <th><a href=""><?php echo stmLang('prediction', 'chart', 'exact'); ?></a></th>
            <th><a href=""><?php echo stmLang('prediction', 'chart', 'success'); ?></a></th>
            <th><a href=""><?php echo stmLang('prediction', 'chart', 'points'); ?></a></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($chart as $row): ?>
            <tr<?php echo ($logged_user == $row->username) ? ' class="user"' : ''; ?>>
                <td><strong><?php echo $row->username; ?></strong></td>
                <td><?php echo "{$row->predictions} ({$row->cheatPredictions})"; ?></td>
                <td><?php echo $row->exactPredictions; ?></td>
                <td><?php echo \STM\Stats\Stats::getPercentage($row->successPredictions, $row->predictions); ?>%</td>
                <td><?php echo $row->points; ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
