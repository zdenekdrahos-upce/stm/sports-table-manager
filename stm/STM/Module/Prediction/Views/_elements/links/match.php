<?php
// $competition loaded in PredictionController::getProfileLinks
$match_name = $match->__toString();
$competition_name = $competition->__toString();
$id_competition = $match->getIdCompetition();
$is_season = $competition instanceof \STM\Competition\Season\Season;
$hasScore = $match->hasScore();
include(STM_MODULES_ROOT . "Match/Views/_elements/links/profile.php");
