    <h2><?php echo stmLang('left-menu-header'); ?></h2>
    <ul>
        <li><a href="<?php echo buildUrl(array('action' => 'read', 'type' => 'chart')); ?>"><?php echo stmLang('header', 'prediction', 'read', 'chart'); ?></a></li>
        <li><a href="?module=prediction"><?php echo stmLang('header', 'prediction', 'read', 'predictions'); ?></a></li>
        <li><a href="<?php echo buildUrl(array('action' => 'read', 'type' => 'user_stats')); ?>"><?php echo stmLang('header', 'prediction', 'read', 'user_stats'); ?></a></li>
    </ul>
