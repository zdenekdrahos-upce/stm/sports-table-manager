<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\Prediction;

use STM\Module\ModuleProcessing;
use STM\Web\Message\SessionMessage;
use STM\Match\Match;
use STM\Prediction\Prediction;
use STM\Prediction\PredictionValidator;

class PredictionProcessing extends ModuleProcessing
{
    /** @var Prediction */
    private $prediction;

    public function __construct($prediction = false)
    {
        $this->prediction = $prediction instanceof Prediction ? $prediction : false;
        parent::__construct($this->prediction, '\STM\Prediction\PredictionEvent');
        PredictionValidator::init($this->formProcessor);
    }

    public function createPrediction()
    {
        if (isset($_POST['create'])) {
            $this->formProcessor->escapeValues();
            $data = $this->getDataForCreatePrediction();
            $result = Prediction::create($data);
            parent::checkAction($result, 'CREATE', $data['match']);
            parent::setSessionMessage($result, $this->getMessage('create'));
            return $result;
        } else {
            $_POST['score']['score_home'] = '';
            $_POST['score']['score_away'] = '';
        }
        return false;
    }

    public function updateScore($match)
    {
        if (isset($_POST['update']) && $this->prediction instanceof Prediction && $match instanceof Match) {
            $this->formProcessor->escapeValues();
            $update = $this->prediction->updateScore($_POST['score']);
            parent::checkAction($update, 'UPDATE', $match);
            parent::setSessionMessage($update, $this->getMessage('change'));
            return $update;
        } else {
            $_POST['score']['score_home'] = $this->prediction->getScoreHome();
            $_POST['score']['score_away'] = $this->prediction->getScoreAway();
        }
        return false;
    }

    private function getDataForCreatePrediction()
    {
        $attributes = $_POST['score'];
        $attributes['user'] = $this->entities->User->findById($this->session->getUserId());
        $attributes['match'] = $this->entities->Match->findById($_GET['m']);
        return $attributes;
    }

    private function getMessage($name)
    {
        return SessionMessage::get($name, array('', stmLang('prediction', 'prediction')));
    }
}
