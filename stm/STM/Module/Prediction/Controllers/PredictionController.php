<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\Prediction;

use STM\Module\ModuleController;
use STM\Web\Message\SessionMessage;
use STM\Competition\Season\Season;
use STM\Prediction\Prediction;
use STM\Prediction\Chart\PredictionCalculator;

class PredictionController extends ModuleController
{
    /** @var \Prediction */
    private $prediction;
    /** @var \STM\Module\Prediction\PredictionProcessing */
    private $processing;

    protected function __construct()
    {
        parent::__construct(true);
        parent::checkLoggedUser();
        parent::setMainPage('index.php?module=prediction');
        $this->loadSelectedPrediction();
        $this->processing = new PredictionProcessing($this->prediction);
    }

    /** index.php?module=prediction */
    public function index()
    {
        parent::setIndexInLeftMenu();
        parent::setContent(
            array(
                'predictions' => $this->entities->Prediction->findByFilter($this->loggedUser, $_POST),
                'teams' => $this->entities->Team->findAll(),
                'competitions' => $this->entities->Competition->find(parent::getCompetitionsSelection()),
            )
        );
    }

    /** index.php?module=prediction&action=create&type=prediction&m=ID_MATCH */
    public function createPrediction()
    {
        if ($this->prediction instanceof Prediction) {
            $this->session->setMessageSuccess(SessionMessage::get('prediction-existing'));
            redirect('?module=prediction');
        } elseif ($this->checkSelectedMatch(false)) {
            if ($this->processing->createPrediction()) {
                parent::redirectToIndexPage();
            } else {
                parent::setIndexInLeftMenu();
                parent::setFormErrors($this->processing);
                parent::setContent(array('match' => $this->match));
            }
        } else {
            $this->session->setMessageFail(SessionMessage::get('prediction-match'));
            parent::redirectToIndexPage();
        }
    }

    /** index.php?module=prediction&action=read&type=userStats */
    public function readUserStats()
    {
        $statistics = $this->entities->PredictionStats->findByUser($this->loggedUser);
        parent::setIndexInLeftMenu();
        parent::setContent(array('statistics' => $statistics->toArray()), 'stats/user');
    }

    /** index.php?module=prediction&action=read&type=matchStats&m=ID_MATCH */
    public function readMatchStats()
    {
        if (parent::checkSelectedMatch(false)) {
            $stats = $this->entities->PredictionStats->findByMatch($this->match);
            $data = array(
                'statistics' => $stats->toArray(),
                'match_name' => $this->match->__toString(),
                'id_competition' => $this->match->getIdCompetition(),
                'competition_name' => $this->entities->Competition->findById($this->match->getIdCompetition())
            );
            parent::setProfileInLeftMenu('match');
            parent::setContent($data, 'stats/match');
        } else {
            $this->session->setMessageFail(SessionMessage::get('prediction-match'));
            redirect('?module=match');
        }
    }

    /** index.php?module=prediction&action=read&type=competitionStats&c=ID_COMPETITION */
    public function readCompetitionStats()
    {
        if (parent::checkSelectedCompetition(false)) {
            $stats = $this->entities->PredictionStats->findByCompetition($this->competition);
            $data = array(
                'statistics' => $stats->toArray(),
                'competition_name' => $this->competition->__toString(),
                'is_season' => $this->competition instanceof Season,
                'is_playoff' => $this->competition instanceof Playoff,
                'id_category' => $this->competition->getIdCategory(),
                'category' => $this->competition->getNameCategory(),
            );
            parent::setProfileInLeftMenu('competition');
            parent::setContent($data, 'stats/competition');
        } else {
            $this->session->setMessageFail(SessionMessage::get('prediction-competition'));
            redirect('?module=competition');
        }
    }

    /** index.php?module=prediction&action=read&type=chart[&c=ID_COMPETITION] */
    public function readChart()
    {
        $calculator = new PredictionCalculator();
        if (parent::checkSelectedCompetition(false)) {
            $data = array(
                'competition_name' => $this->competition->__toString(),
                'is_season' => $this->competition instanceof Season,
                'is_playoff' => $this->competition instanceof Playoff,
                'id_category' => $this->competition->getIdCategory(),
                'category' => $this->competition->getNameCategory(),
            );
            $pr = $this->entities->PredictionChart->findByCompetition($this->competition, $calculator);
            parent::setProfileInLeftMenu('competition');
        } elseif (isset($_GET['c'])) {
            redirect(buildUrl(array('c' => null)));
        } else {
            $pr = $this->entities->PredictionChart->findAll($calculator);
            parent::setIndexInLeftMenu();
        }
        $data['chart'] = $pr->getChart();
        $data['logged_user'] = $this->loggedUser->getUsername();
        parent::setContent($data);
    }

    /** index.php?module=prediction&action=update&type=predictions&m=ID_MATCH */
    public function updatePrediction()
    {
        if ($this->checkSelectedPrediction()) {
            if ($this->processing->updateScore($this->match)) {
                parent::redirectToIndexPage();
            } else {
                parent::setIndexInLeftMenu();
                parent::setFormErrors($this->processing);
                parent::setContent(array('match' => $this->match));
            }
        }
    }

    protected function checkSelectedMatch($redirect = true)
    {
        if (parent::checkSelectedMatch($redirect)) {
            $this->competition = $this->entities->Competition->findById($this->match->getIdCompetition());
            parent::checkHiddenCompetitions();
            return true;
        }
        return false;
    }

    /**
     * @return boolean
     * Returns true if prediction is selected and exists
     */
    private function checkSelectedPrediction()
    {
        if (!$this->prediction) {
            $this->session->setMessageFail(
                SessionMessage::get('invalid', array(stmLang('prediction', 'prediction')))
            );
            parent::redirectToIndexPage();
        }
        return true;
    }

    private function loadSelectedPrediction()
    {
        $this->prediction = false;
        if ($this->checkSelectedMatch(false)) {
            $this->prediction = $this->entities->Prediction->findById($this->loggedUser, $this->match);
        }
    }

    /**
     * @return array
     */
    protected function getProfileLinks()
    {
        return array(
            'competition' => $this->competition,
            'match' => $this->match
        );
    }
}
