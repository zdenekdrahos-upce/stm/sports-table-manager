<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\Settings;

use STM\Helpers\ObjectValidator;
use STM\Utils\Classes;
use STM\Web\Message\FormError;
use STM\Libs\FormProcessor;
use STM\Competition\Season\SeasonValidator;
use STM\Competition\CompetitionValidator;
use STM\Match\Lineup\MatchPlayerValidator;

final class SettingsValidator
{
    /** @var ObjectValidator */
    private static $validator;

    /** @param \STM\Libs\FormProcessor $formProcessor */
    public static function init($formProcessor = false)
    {
        if (is_null(self::$validator)) {
            self::$validator = new ObjectValidator('stdClass');
        }
        self::$validator->setFormProcessor($formProcessor);
    }

    public static function check($attributes)
    {
        if (self::$validator->checkCreate($attributes, array_keys(self::getAttributes()))) {
            self::checkBooleanAttribute($attributes['registration'], 'user registration');
            self::checkBooleanAttribute($attributes['visitors'], 'visitors access');
            self::checkBooleanAttribute($attributes['predictions'], 'predictions');
            self::checkLanguage($attributes['default_lang']);
            self::checkDatetimeFormat($attributes['datetime']);
            self::checkClubWebsite($attributes['website']);
            self::checkPoints($attributes);
            self::checkMatchPeriods($attributes['match_periods']);
            self::checkMinutes($attributes);
            self::checkFilteringParameters($attributes);
            self::checkOldSeasons($attributes['old_seasons']);
            self::checkStatisticCalculation($attributes['statistic_calculation']);
            self::checkChange($attributes);
        }
        return self::$validator->isValid();
    }

    public static function getAttributes()
    {
        return array(
            'registration' => STM_ALLOW_USER_REGISTRATION, 'visitors' => STM_ALLOW_VISITOR_ACCESS,
            'predictions' => STM_ALLOW_PREDICTIONS, 'default_lang' => STM_DEFAULT_LANGUAGE,
            'datetime' => STM_DEFAULT_DATETIME_FORMAT, 'website' => STM_CLUB_WEBSITE,
            'point_win' => STM_POINT_WIN, 'point_win_ot' => STM_POINT_WIN_OT, 'point_draw' => STM_POINT_DRAW,
            'point_loss_ot' => STM_POINT_LOSS_OT, 'point_loss' => STM_POINT_LOSS,
            'match_periods' => STM_MATCH_PERIODS, 'minute_in' => STM_MINUTE_IN, 'minute_out' => STM_MINUTE_OUT,
            'max_table' => STM_MAX_MATCHES_FOR_TABLE, 'max_listing' => STM_MAX_MATCHES_FOR_LISTING,
            'per_page' => STM_MATCHES_ON_ONE_PAGE, 'old_seasons' => STM_NOT_DELETED_SEASONS_TABLE,
            'statistic_calculation' => STM_STATISTIC_CALCULATION
        );
    }

    private static function checkBooleanAttribute($attribute, $error_name)
    {
        self::$validator->getFormProcessor()->checkCondition(
            in_array($attribute, array('true', 'false')),
            "Invalid value of {$error_name}"
        );
    }

    private static function checkLanguage($language)
    {
        self::$validator->getFormProcessor()->checkCondition(
            in_array($language, array('czech', 'english')),
            'Invalid value in default language'
        );
    }

    private static function checkDatetimeFormat($datetime)
    {
        self::$validator->checkString(
            $datetime,
            array('min' => 1, 'max' => 100),
            stmLang('settings', 'default-datetime-format')
        );
    }

    private static function checkClubWebsite($url)
    {
        self::$validator->getFormProcessor()->checkUrlAddress(
            $url,
            FormError::get('invalid-website', stmLang('settings', 'club-website'))
        );
    }

    private static function checkPoints($attributes)
    {
        $fp = new FormProcessor();
        SeasonValidator::init($fp);
        $points = array(
            'point_win' => 'checkPointWin', 'point_win_ot' => 'checkPointWinOt',
            'point_draw' => 'checkPointDraw', 'point_loss_ot' => 'checkPointLossOt',
            'point_loss' => 'checkPointLoss'
        );
        foreach ($points as $point => $method) {
            SeasonValidator::$method($attributes[$point]);
        }
        self::addErrors($fp);
    }

    private static function checkMatchPeriods($match_periods)
    {
        $fp = new FormProcessor();
        CompetitionValidator::init($fp);
        CompetitionValidator::checkMatchPeriods($match_periods);
        self::addErrors($fp);
    }

    private static function checkMinutes($attributes)
    {
        $fp = new FormProcessor();
        MatchPlayerValidator::init($fp);
        MatchPlayerValidator::checkMinuteIn($attributes['minute_in']);
        MatchPlayerValidator::checkMinuteOut($attributes['minute_out']);
        self::addErrors($fp);
    }

    private static function checkFilteringParameters($attributes)
    {
        $filter_args = array(
            'max_table' => 'filter-max-table', 'max_listing' => 'filter-max-listing',
            'per_page' => 'filter-per-page'
        );
        foreach ($filter_args as $arg => $translator_key) {
            self::$validator->checkNumber(
                $attributes[$arg],
                array('min' => 0, 'max' => 500),
                stmLang('settings', $translator_key)
            );
        }
    }

    private static function checkOldSeasons($old_seasons)
    {
        if ($old_seasons !== '') {
            self::$validator->checkString(
                $old_seasons,
                array('min' => 1, 'max' => 1000),
                stmLang('settings', 'old-seasons')
            );
        }
    }

    private static function checkStatisticCalculation($type)
    {
        self::$validator->getFormProcessor()->checkCondition(
            Classes::isClassConstant('\STM\Match\Stats\Action\StatisticCalculationType', $type),
            FormError::get('invalid-value', array(stmLang('settings', 'statistic-calculation'), 'string'))
        );
    }

    private static function checkChange($attributes)
    {
        $change = false;
        foreach (self::getAttributes() as $key => $current_value) {
            if (is_bool($current_value)) {
                $current_value = $current_value ? 'true' : 'false';
            }
            if ($current_value != $attributes[$key]) {
                $change = true;
                break;
            }
        }
        self::$validator->getFormProcessor()->checkCondition(
            $change,
            FormError::get('no-change', array(stmLang('header', 'settings', 'read', 'stm')))
        );
    }

    private static function addErrors(FormProcessor $fp)
    {
        if (!$fp->isValid()) {
            foreach ($fp->getErrors() as $error) {
                self::$validator->getFormProcessor()->addError($error);
            }
        }
    }
}
