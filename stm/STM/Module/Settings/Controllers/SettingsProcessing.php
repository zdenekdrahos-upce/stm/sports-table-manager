<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\Settings;

use STM\Module\ModuleProcessing;
use STM\Web\Message\FormError;
use STM\Web\Message\SessionMessage;
use STM\Template\Template;
use STM\Utils\Strings;

class SettingsProcessing extends ModuleProcessing
{
    public function __construct()
    {
        parent::__construct(false, false);
        SettingsValidator::init($this->formProcessor);
    }

    public function updateStmSettings()
    {
        if (isset($_POST['change'])) {
            $this->formProcessor->escapeValues();
            unset($_POST['change']);
            if (SettingsValidator::check($_POST)) {
                $template = Template::findByName('change_stm', STM_ADMIN_TEMPLATE_ROOT . 'settings/');
                if ($template instanceof Template) {
                    $text = Strings::replaceValues($template->getContent(), $this->getTemplateArray());
                    $config_file = Template::findByName('stm', STM_SITE_ROOT . '/stm/config/');
                    if (Strings::isStringNonEmpty($text) && $config_file->save($text)) {
                        $this->session->setMessageSuccess(
                            SessionMessage::get('change', array('', stmLang('menu', 'settings')))
                        );
                        return true;
                    } else {
                        $this->formProcessor->addError(FormError::get('file-not-saved'));
                    }
                } else {
                    $this->formProcessor->addError(
                        'Settings could not be saved, because template STM settings template doesn\'t exist.'
                    );
                }
            }
        } else {
            $this->initForm();
        }
        return false;
    }

    private function getTemplateArray()
    {
        unset($_POST['change']);
        $values = array();
        foreach ($_POST as $key => $value) {
            $values[strtoupper($key)] = $value;
        }
        return $values;
    }

    private function initForm()
    {
        foreach (SettingsValidator::getAttributes() as $key => $value) {
            if (is_bool($value)) {
                $value = $value ? 'true' : 'false';
            }
            $_POST[$key] = $value;
        }
    }
}
