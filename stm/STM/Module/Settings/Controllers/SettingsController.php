<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\Settings;

use STM\Module\Controller;

class SettingsController extends Controller
{
    /** @var \STM\Module\Settings\SettingsProcessing */
    private $processing;
    private $allowed;
    private $denied;

    protected function __construct()
    {
        parent::__construct();
        parent::setMainPage('index.php?module=settings&action=read&type=stm');
        $this->processing = new SettingsProcessing();
        $this->allowed = stmLang('settings', 'allowed');
        $this->denied = stmLang('settings', 'denied');
    }

    /** index.php?module=settings */
    public function index()
    {
        parent::redirectToIndexPage();
    }

    /** index.php?module=settings&action=read&type=stm */
    public function readStm()
    {
        $data = array(
            'registration' => STM_ALLOW_USER_REGISTRATION === true ? $this->allowed : $this->denied,
            'visitors' => STM_ALLOW_VISITOR_ACCESS === true ? $this->allowed : $this->denied,
            'predictions' => STM_ALLOW_PREDICTIONS === true ? $this->allowed : $this->denied,
        );
        parent::setIndexInLeftMenu();
        parent::setContent($data);
    }

    /** index.php?module=settings&action=read&type=database */
    public function readDatabase()
    {
        $password = '';
        for ($i = 0; $i < strlen(STM_DB_PASS); $i++) {
            $password .= '*';
        }
        $data = array(
            'password' => $password,
            'procedures' => STM_DB_PROCEDURES === true ? $this->allowed : $this->denied,
            'triggers' => STM_DB_TRIGGERS === true ? $this->allowed : $this->denied,
        );
        parent::setIndexInLeftMenu();
        parent::setContent($data);
    }

    /** index.php?module=settings&action=update&type=stm */
    public function updateStm()
    {
        if ($this->processing->updateStmSettings()) {
            redirect(buildUrl(array('action' => 'read')));
        } else {
            $data = array(
                'registration' => STM_ALLOW_USER_REGISTRATION === true ? $this->allowed : $this->denied,
                'visitors' => STM_ALLOW_VISITOR_ACCESS === true ? $this->allowed : $this->denied,
                'predictions' => STM_ALLOW_PREDICTIONS === true ? $this->allowed : $this->denied,
            );
            parent::setIndexInLeftMenu();
            parent::setFormErrors($this->processing);
            parent::setContent($data);
        }
    }
}
