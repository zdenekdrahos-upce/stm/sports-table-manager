<?php
/**
 * STM settings (file include/config/stm.php)
 * $registration    if user registration is allowed
 * $visitors        if unregistred visitor has access to application (only read competitions)
 * $predictions     if match score prediction is allowed
 */
use STM\Match\Stats\Action\StatisticCalculationType;
?>

<h2><?php echo stmLang('settings', 'settings-basic'); ?></h2>
<ul>
    <li><?php echo stmLang('settings', 'registration'); ?>: <strong><?php echo $registration; ?></strong></li>
    <li><?php echo stmLang('settings', 'visitors'); ?>: <strong><?php echo $visitors; ?></strong></li>
    <li><?php echo stmLang('settings', 'predictions'); ?>: <strong><?php echo $predictions; ?></strong></li>
    <li><?php echo stmLang('settings', 'default-language'); ?>: <strong><?php echo stmLang('lang-titles', STM_DEFAULT_LANGUAGE); ?></strong></li>
    <li><?php echo stmLang('settings', 'default-datetime-format'); ?>: <strong><?php echo STM_DEFAULT_DATETIME_FORMAT; ?></strong></li>
    <li><?php echo stmLang('settings', 'club-website'); ?>: <strong><?php echo STM_CLUB_WEBSITE; ?></strong></li>
</ul>
<h2><?php echo stmLang('settings', 'settings-defaultvalues'); ?></h2>
<ul>
    <li><?php echo stmLang('competition', 'win'); ?>: <strong><?php echo STM_POINT_WIN; ?></strong></li>
    <li><?php echo stmLang('competition', 'win-ot'); ?>: <strong><?php echo STM_POINT_WIN_OT; ?></strong></li>
    <li><?php echo stmLang('competition', 'draw'); ?>: <strong><?php echo STM_POINT_DRAW; ?></strong></li>
    <li><?php echo stmLang('competition', 'loss-ot'); ?>: <strong><?php echo STM_POINT_LOSS_OT; ?></strong></li>
    <li><?php echo stmLang('competition', 'loss'); ?>: <strong><?php echo STM_POINT_LOSS; ?></strong></li>
    <li><?php echo stmLang('competition', 'min-match-periods'); ?>: <strong><?php echo STM_MATCH_PERIODS; ?></strong></li>
</ul>
<h2><?php echo stmLang('settings', 'settings-lineups'); ?></h2>
<ul>
    <li><?php echo stmLang('settings', 'minute-in'); ?>: <strong><?php echo STM_MINUTE_IN; ?></strong></li>
    <li><?php echo stmLang('settings', 'minute-out'); ?>: <strong><?php echo STM_MINUTE_OUT; ?></strong></li>
</ul>
<h2><?php echo stmLang('settings', 'settings-filtering'); ?></h2>
<ul>
    <li><?php echo stmLang('settings', 'filter-max-table'); ?>: <strong><?php echo STM_MAX_MATCHES_FOR_TABLE; ?></strong></li>
    <li><?php echo stmLang('settings', 'filter-max-listing'); ?>: <strong><?php echo STM_MAX_MATCHES_FOR_LISTING; ?></strong></li>
    <li><?php echo stmLang('settings', 'filter-per-page'); ?>: <strong><?php echo STM_MATCHES_ON_ONE_PAGE; ?></strong></li>
</ul>
<h2><?php echo stmLang('menu', 'other'); ?></h2>
<ul>
    <li><?php echo stmLang('settings', 'old-seasons'); ?>: <strong><?php echo STM_NOT_DELETED_SEASONS_TABLE; ?></strong></li>
    <li><?php echo stmLang('settings', 'statistic-calculation'); ?>: <strong><?php echo StatisticCalculationType::getName(STM_STATISTIC_CALCULATION); ?></strong></li>
</ul>

<h3><a href="<?php echo buildUrl(array('action' => 'update')); ?>"><?php echo stmLang('header', 'settings', 'update', 'stm'); ?></a></h3>
