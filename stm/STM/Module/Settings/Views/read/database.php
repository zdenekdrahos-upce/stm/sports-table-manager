<?php
/**
 * Database settings (file include/config/database.php)
 * $password        password, e.g. **** if STM_DB_PASS consists of 4 letters
 * $procedures
 * $triggers
 */
?>

<ul>
    <li><?php echo stmLang('settings', 'db-server'); ?>: <strong><?php echo STM_DB_SERVER; ?></strong></li>
    <li><?php echo stmLang('settings', 'db-user'); ?>: <strong><?php echo STM_DB_USER; ?></strong></li>
    <li><?php echo stmLang('settings', 'db-password'); ?>: <strong><?php echo $password; ?></strong></li>
    <li><?php echo stmLang('settings', 'db-name'); ?>: <strong><?php echo STM_DB_NAME; ?></strong></li>
    <li><?php echo stmLang('settings', 'db-procedures'); ?>: <strong><?php echo $procedures; ?></strong></li>
    <li><?php echo stmLang('settings', 'db-triggers'); ?>: <strong><?php echo $triggers; ?></strong></li>
</ul>

<h2><?php echo stmLang('settings', 'db-editing'); ?></h2>
<ul>
    <li><?php echo stmLang('settings', 'db-web'); ?></li>
    <li><?php echo stmLang('settings', 'db-file'); ?></li>
</ul>
