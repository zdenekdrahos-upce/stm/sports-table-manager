    <h2><?php echo stmLang('left-menu-header'); ?></h2>
    <ul>
        <li><a href="index.php?module=settings&action=read&type=stm"><?php echo stmLang('header', 'settings', 'read', 'stm'); ?></a></li>
        <li><a href="index.php?module=settings&action=update&type=stm"><?php echo stmLang('header', 'settings', 'update', 'stm'); ?></a></li>
        <li><a href="index.php?module=settings&action=read&type=database"><?php echo stmLang('header', 'settings', 'read', 'database'); ?></a></li>
    </ul>
