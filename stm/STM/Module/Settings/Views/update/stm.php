<?php
/**
 * STM settings (file include/config/stm.php)
 * $registration    if user registration is allowed
 * $visitors        if unregistred visitor has access to application (only read competitions)
 */
use STM\Match\Stats\Action\StatisticCalculationType;
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <h2><?php echo stmLang('settings', 'settings-basic'); ?></h2>
    <label for="registration"><?php echo stmLang('settings', 'registration'); ?>:</label>
    <?php \STM\Web\HTML\Forms::selectBoolean('registration'); ?>
    <label for="visitors"><?php echo stmLang('settings', 'visitors'); ?>:</label>
    <?php \STM\Web\HTML\Forms::selectBoolean('visitors'); ?>
    <label for="predictions"><?php echo stmLang('settings', 'predictions'); ?>:</label>
    <?php \STM\Web\HTML\Forms::selectBoolean('predictions'); ?>
    <label for="default_lang"><?php echo stmLang('settings', 'default-language'); ?>:</label>
    <?php
    $options = array(
        'czech' => stmLang('lang-titles', 'czech'),
        'english' => stmLang('lang-titles', 'english'),
    );
    \STM\Web\HTML\Forms::select('default_lang', $options);
    ?>
    <label for="datetime"><?php echo stmLang('settings', 'default-datetime-format'); ?>: </label>
    <input type="text" id="datetime" name="datetime" size="40" value="<?php echo $_POST['datetime']; ?>" />
    <label for="website"><?php echo stmLang('settings', 'club-website'); ?>: </label>
    <input type="text" id="website" name="website" size="100" value="<?php echo $_POST['website']; ?>" />

    <h2><?php echo stmLang('settings', 'settings-defaultvalues'); ?></h2>
    <label for="win"><?php echo stmLang('competition', 'win'); ?>: </label>
    <input type="text" id="win" name="point_win" size="3" maxlength="2" value="<?php echo $_POST['point_win']; ?>" />
    <label for="win_ot"><?php echo stmLang('competition', 'win-ot'); ?>: </label>
    <input type="text" id="win_ot" name="point_win_ot" size="3" maxlength="2" value="<?php echo $_POST['point_win_ot']; ?>" />
    <label for="draw"><?php echo stmLang('competition', 'draw'); ?>: </label>
    <input type="text" id="draw" name="point_draw" size="3" maxlength="2" value="<?php echo $_POST['point_draw']; ?>" />
    <label for="loss_ot"><?php echo stmLang('competition', 'loss-ot'); ?>: </label>
    <input type="text" id="loss_ot" name="point_loss_ot" size="3" maxlength="2" value="<?php echo $_POST['point_loss_ot']; ?>" />
    <label for="loss"><?php echo stmLang('competition', 'loss'); ?>: </label>
    <input type="text" id="loss" name="point_loss" size="3" maxlength="2" value="<?php echo $_POST['point_loss']; ?>" />
    <label for="match_periods"><?php echo stmLang('competition', 'min-match-periods'); ?>:</label>
    <input type="text" id="match_periods" name="match_periods" size="3" maxlength="2" value="<?php echo $_POST['match_periods']; ?>" />

    <h2><?php echo stmLang('settings', 'settings-lineups'); ?></h2>
    <label for="minute_in"><?php echo stmLang('settings', 'minute-in'); ?>: </label>
    <input type="text" id="minute_in" name="minute_in" size="20" maxlength="20" value="<?php echo $_POST['minute_in']; ?>" />
    <label for="minute_out"><?php echo stmLang('settings', 'minute-out'); ?>:</label>
    <input type="text" id="minute_out" name="minute_out" size="20" maxlength="20" value="<?php echo $_POST['minute_out']; ?>" />

    <h2><?php echo stmLang('settings', 'settings-filtering'); ?></h2>
    <label for="max_table"><?php echo stmLang('settings', 'filter-max-table'); ?>:</label>
    <input type="text" id="max_table" name="max_table" size="3" maxlength="3" value="<?php echo $_POST['max_table']; ?>" />
    <label for="max_listing"><?php echo stmLang('settings', 'filter-max-listing'); ?>:</label>
    <input type="text" id="max_listing" name="max_listing" size="3" maxlength="3" value="<?php echo $_POST['max_listing']; ?>" />
    <label for="per_page"><?php echo stmLang('settings', 'filter-per-page'); ?>:</label>
    <input type="text" id="per_page" name="per_page" size="3" maxlength="3" value="<?php echo $_POST['per_page']; ?>" />

    <h2><?php echo stmLang('menu', 'other'); ?></h2>
    <label for="old_seasons"><?php echo stmLang('settings', 'old-seasons'); ?>:</label>
    <input type="text" id="old_seasons" name="old_seasons" size="40" value="<?php echo $_POST['old_seasons']; ?>" />
    <label for="statistic_calculation"><?php echo stmLang('settings', 'statistic-calculation'); ?>:</label>
    <?php
    $options = array(
        'COUNT' => stmLang('constants', 'statisticcalculationtype', StatisticCalculationType::COUNT),
        'SUM' => stmLang('constants', 'statisticcalculationtype', StatisticCalculationType::SUM),
    );
    \STM\Web\HTML\Forms::select('statistic_calculation', $options);
    ?>

    <input type="submit" value="<?php echo stmLang('form', 'change'); ?>" name="change" />
</form>
