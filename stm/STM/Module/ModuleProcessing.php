<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module;

use STM\Libs\FormProcessor;
use STM\Session;
use STM\Cache\CacheHelper;
use STM\Web\Message\SessionMessage;
use STM\Web\Message\FormError;
use STM\Web\View\ViewTemplate;
use STM\StmFactory;

/**
 * ModuleProcessing class
 * - helper class for work with forms in Controller/View
 * - usually for inherited classes contain single-page form submission (create, update) or
 *   deleting operations that sets message (true/false) and redirect user to index page
 */
class ModuleProcessing
{
    /** @var \STM\Libs\FormProcessor */
    protected $formProcessor;
    /** @var Session */
    protected $session;
    /** @var object/false */
    protected $updatedObject;
    /** @var string */
    protected $logger;
    /** @var StmFactory */
    protected $entities;

    public function __construct($object, $logger)
    {
        $this->updatedObject = $object;
        $this->logger = $logger;
        $this->formProcessor = new FormProcessor(true);
        $this->session = Session::getInstance();
        $this->entities = StmFactory::find();
    }

    /**
     * @return ViewTemplate/false
     * Returns instance of ViewTemplate if there were any errors in single-page form submission,
     * otherwise returns false (-> nothing is set in View::setContent)
     */
    public function getErrorsForView()
    {
        if (!$this->formProcessor->isValid()) {
            return new ViewTemplate(
                STM_ADMIN_TEMPLATE_ROOT . 'form/errors.php',
                array('errors' => $this->formProcessor->getErrors())
            );
        } else {
            return false;
        }
    }

    protected function setSessionMessage($result, $success_message, $failMessage = false)
    {
        if ($result) {
            $this->session->setMessageSuccess($success_message);
        } elseif (!is_bool($failMessage)) {
            $this->session->setMessageFail($failMessage);
        }
    }

    protected function log($event, $message, $result)
    {
        $class = $this->logger;
        $event = constant($class . '::' . $event);
        $class::log($event, $message, $result);
    }

    protected function canBeDeleted()
    {
        if (isset($_POST['delete']) && isset($_POST['token']) && $this->isObjectSet()) {
            if (isset($_SESSION['stmUser']['token']) && $_POST['token'] == $_SESSION['stmUser']['token']) {
                return true;
            } else {
                $this->session->setMessageFail(SessionMessage::get('delete-csrf'));
            }
        } else {
            $this->session->setMessageFail(SessionMessage::get('delete-input'));
        }
        return false;
    }

    // used for forms - where if TRUE -> log, if FALSE -> log only if there are no errors
    protected function checkAction($result, $logEvent, $log_message)
    {
        if ($result) {
            $this->log($logEvent, $log_message, true);
        } elseif ($this->formProcessor->isValid()) {
            $this->log($logEvent, $log_message, false);
            $this->formProcessor->addError(FormError::get('db-fail'));
        }
    }

    protected function isObjectSet()
    {
        return is_object($this->updatedObject);
    }

    protected function deleteCacheIfSuccessfulAction($result, $id_competition)
    {
        if ($result) {
            CacheHelper::deleteCache($id_competition);
        }
    }

    protected function deleteActionCacheIfSuccessfulAction($result, $id_competition)
    {
        if ($result) {
            CacheHelper::deleteActionStatsCache($id_competition);
        }
    }
}
