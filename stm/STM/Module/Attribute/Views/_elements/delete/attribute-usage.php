<?php
$url = '?module=attribute&action=delete&type=usage&atr=' . $print_attribute->getId();
$submit_message = stmLang('attribute', 'delete-usage') . ',';
$question = stmLang('attribute', 'delete-usage-msg') . $print_attribute->__toString();
include(STM_ADMIN_TEMPLATE_ROOT . 'form/delete-form.php');
