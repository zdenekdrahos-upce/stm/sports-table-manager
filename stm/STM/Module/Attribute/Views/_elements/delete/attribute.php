<?php
$url = '?module=attribute&action=delete&type=attribute&atr=' . $print_attribute->getId();
$submit_message = stmLang('attribute', 'delete');
$question = stmLang('attribute', 'delete-msg') . $print_attribute->__toString();
include(STM_ADMIN_TEMPLATE_ROOT . 'form/delete-form.php');
