<?php
/**
 * Update name of the attribute  (AttributeController::update_name)
 * $attribute_name        current name of the attribute
 */
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <?php include(STM_MODULES_ROOT . 'Attribute/Views/_elements/form-parts/attribute.php'); ?>

    <input type="submit" name="update" value="<?php echo stmLang('form', 'change'); ?>" />
</form>
