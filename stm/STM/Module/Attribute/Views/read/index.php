<?php
/*
 * List of all attributes
 * $attributes
 */
?>

<?php if (empty($attributes)): ?>
    <p><?php echo stmLang('no-content'); ?></p>
<?php else: ?>
    <table>
        <thead>
            <tr>
                <th><?php echo stmLang('name'); ?></th>
                <th><?php echo stmLang('attribute', 'usage'); ?></th>
                <th><?php echo stmLang('team', 'links'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($attributes as $print_attribute): extract($print_attribute->toArray()); ?>
            <tr id="<?php echo $print_attribute->getId();?>">
                <td><?php echo $print_attribute; ?></td>
                <td><?php echo $usage; ?></td>
                <td>
                    <a href="<?php echo buildUrl(array('action' => 'update', 'type' => 'name', 'atr' => $print_attribute->getId())) ?>"><?php echo stmLang('form', 'change'); ?></a>,
                    <?php include(STM_MODULES_ROOT . 'Attribute/Views/_elements/delete/attribute-usage.php'); ?>
                    <?php include(STM_MODULES_ROOT . 'Attribute/Views/_elements/delete/attribute.php'); ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php endif;
