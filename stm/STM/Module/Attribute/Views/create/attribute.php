<?php
/**
 * Create new attribute
 */
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <?php include(STM_MODULES_ROOT . 'Attribute/Views/_elements/form-parts/attribute.php'); ?>

    <input type="submit" name="create" value="<?php echo stmLang('form', 'create'); ?>" />

</form>
