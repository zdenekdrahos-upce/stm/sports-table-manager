<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\Attribute;

use STM\Module\Controller;
use STM\Web\Message\SessionMessage;
use STM\Attribute\Attribute;

class AttributeController extends Controller
{
    /** @var \STM\Attribute\Attribute */
    private $attribute;
    /** @var \STM\Module\Attribute\AttributeProcessing */
    private $processing;

    protected function __construct()
    {
        parent::__construct();
        parent::setMainPage('index.php?module=attribute');
        $this->loadSelectedAttribute();
        $this->processing = new AttributeProcessing($this->attribute);
    }

    /** index.php?module=attribute */
    public function index()
    {
        parent::setIndexInLeftMenu();
        parent::setContent(array('attributes' => $this->entities->Attribute->findAll()), 'index');
    }

    /** index.php?module=attribute&action=create&type=attribute */
    public function createAttribute()
    {
        if ($this->processing->createAttribute()) {
            parent::redirectToIndexPage();
        } else {
            parent::setIndexInLeftMenu();
            parent::setFormErrors($this->processing);
            parent::setContent();
        }
    }

    /** index.php?module=attribute&action=update&type=name&ATR=ID_ATTRIBUTE */
    public function updateName()
    {
        if ($this->checkSelectedAttribute()) {
            if ($this->processing->updateName()) {
                parent::redirectToIndexPage();
            } else {
                parent::setIndexInLeftMenu();
                parent::setFormErrors($this->processing);
                parent::setContent(array('attribute_name' => $this->attribute->__toString()));
            }
        }
    }

    /** index.php?module=attribute&action=delete&type=attribute&ATR=ID_ATTRIBUTE */
    public function deleteAttribute()
    {
        if ($this->checkSelectedAttribute()) {
            $this->processing->deleteAttribute();
            parent::redirectToIndexPage();
        }
    }

    /** index.php?module=attribute&action=delete&type=usage&ATR=ID_ATTRIBUTE */
    public function deleteUsage()
    {
        if ($this->checkSelectedAttribute()) {
            $this->processing->deleteAttributeUsage();
            parent::redirectToIndexPage();
        }
    }

    private function checkSelectedAttribute()
    {
        if (!($this->attribute instanceof Attribute)) {
            $this->session->setMessageFail(SessionMessage::get('invalid', array(stmLang('person', 'attribute'))));
            parent::redirectToIndexPage();
        }
        return true;
    }

    private function loadSelectedAttribute()
    {
        $this->attribute = isset($_GET['atr']) ? $this->entities->Attribute->findById($_GET['atr']) : false;
    }
}
