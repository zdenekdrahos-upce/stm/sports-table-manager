<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\Attribute;

use STM\Module\ModuleProcessing;
use STM\Web\Message\SessionMessage;
use STM\Attribute\Attribute;
use STM\Attribute\AttributeValidator;
use STM\Person\Attribute\PersonAttribute;

class AttributeProcessing extends ModuleProcessing
{
    /** @var \STM\Attribute\Attribute */
    private $attribute;

    public function __construct($attribute)
    {
        $this->attribute = $attribute instanceof Attribute ? $attribute : false;
        parent::__construct($this->attribute, '\STM\Team\TeamEvent');
        AttributeValidator::init($this->formProcessor);
    }

    public function createAttribute()
    {
        if (isset($_POST['create'])) {
            unset($_POST['create']);
            $this->formProcessor->escapeValues();
            $result = Attribute::create($_POST);
            parent::checkAction($result, 'PERSON_ATTRIBUTE', "New Attribute: {$_POST['attribute']}");
            parent::setSessionMessage($result, $this->getMessage('create', $_POST['attribute']));
            return $result;
        } else {
            $this->formProcessor->initVars(array('attribute'));
        }
        return false;
    }

    public function deleteAttribute()
    {
        if (parent::canBeDeleted()) {
            if ($this->attribute->canBeDeleted()) {
                $delete = $this->attribute->delete();
                parent::setSessionMessage(
                    $delete,
                    $this->getMessage('delete-success'),
                    $this->getMessage('delete-fail')
                );
                parent::log('PERSON_ATTRIBUTE', "Delete Attribute: {$this->attribute->__toString()}", $delete);
            } else {
                $this->session->setMessageFail(SessionMessage::get('attribute-delete'));
            }
        }
    }

    public function deleteAttributeUsage()
    {
        if (parent::canBeDeleted()) {
            if (!$this->attribute->canBeDeleted()) {
                $delete = PersonAttribute::deleteAttributeFromPersons($this->attribute);
                parent::setSessionMessage(
                    $delete,
                    $this->getMessage('delete-success', stmLang('attribute', 'usage')),
                    $this->getMessage('delete-fail', stmLang('attribute', 'usage'))
                );
                parent::log('PERSON_ATTRIBUTE', "Delete Attribute {$this->attribute} from Persons", $delete);
            } else {
                $this->session->setMessageFail(SessionMessage::get('attribute-usage'));
            }
        }
    }

    public function updateName()
    {
        if (parent::isObjectSet() && isset($_POST['update'])) {
            $this->formProcessor->escapeValues();
            $result = $this->attribute->updateAttributeName($_POST['attribute']);
            parent::checkAction(
                $result,
                'PERSON_ATTRIBUTE',
                "Update Attribute Name: {$this->attribute->__toString()} to {$_POST['attribute']}"
            );
            parent::setSessionMessage($result, $this->getMessage('change', $_POST['attribute']));
            return $result;
        } else {
            $_POST['attribute'] = $this->attribute->__toString();
        }
        return false;
    }

    private function getMessage($name, $attribute = false)
    {
        $attribute = $attribute || !parent::isObjectSet() ? $attribute : $this->attribute->__toString();
        return SessionMessage::get($name, array(stmLang('person', 'attribute'), $attribute));
    }
}
