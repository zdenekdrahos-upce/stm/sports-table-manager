<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\Action;

use STM\Module\Controller;
use STM\Web\Message\SessionMessage;
use STM\Match\Action\MatchAction;

class ActionController extends Controller
{
    /** @var \MatchAction */
    private $matchAction;
    /** @var \STM\Module\Action\ActionProcessing */
    private $processing;

    protected function __construct()
    {
        parent::__construct();
        parent::setMainPage('index.php?module=action');
        $this->loadSelectedAction();
        $this->processing = new ActionProcessing($this->matchAction);
    }

    /** index.php?module=action */
    public function index()
    {
        parent::setIndexInLeftMenu();
        parent::setContent(array('actions' => $this->entities->MatchAction->findAll()), 'index');
    }

    /** index.php?module=action&action=create&type=position */
    public function createAction()
    {
        if ($this->processing->createAction()) {
            parent::redirectToIndexPage();
        } else {
            parent::setIndexInLeftMenu();
            parent::setFormErrors($this->processing);
            parent::setContent(array('sports' => $this->entities->Sport->findAll()));
        }
    }

    /** index.php?module=action&action=update&type=action&ma=ID_MATCH_ACTION */
    public function updateAction()
    {
        if ($this->checkSelectedAction()) {
            if ($this->processing->updateAction()) {
                parent::redirectToIndexPage();
            } else {
                parent::setIndexInLeftMenu();
                parent::setFormErrors($this->processing);
                parent::setContent(array('sports' => $this->entities->Sport->findAll()));
            }
        }
    }

    /** index.php?module=action&action=delete&type=action&ma=ID_MATCH_ACTION */
    public function deleteAction()
    {
        if ($this->checkSelectedAction()) {
            $this->processing->deleteAction();
            parent::redirectToIndexPage();
        }
    }

    /** index.php?module=action&action=delete&type=action_team&ma=ID_MATCH_ACTION */
    public function deleteActionTeams()
    {
        if ($this->checkSelectedAction()) {
            $this->processing->deleteActionTeams();
            parent::redirectToIndexPage();
        }
    }

    /** index.php?module=action&action=delete&type=action_players&ma=ID_MATCH_ACTION */
    public function deleteActionPlayers()
    {
        if ($this->checkSelectedAction()) {
            $this->processing->deleteActionPlayers();
            parent::redirectToIndexPage();
        }
    }

    private function checkSelectedAction()
    {
        if (!($this->matchAction instanceof MatchAction)) {
            $this->session->setMessageFail(SessionMessage::get('invalid', array(stmLang('action', 'name'))));
            parent::redirectToIndexPage();
        }
        return true;
    }

    private function loadSelectedAction()
    {
        $this->matchAction = isset($_GET['ma']) ? $this->entities->MatchAction->findById($_GET['ma']) : false;
    }
}
