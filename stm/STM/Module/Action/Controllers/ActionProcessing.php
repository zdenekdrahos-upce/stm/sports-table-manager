<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\Action;

use STM\Module\ModuleProcessing;
use STM\Web\Message\SessionMessage;
use STM\Match\Action\MatchAction;
use STM\Match\Action\MatchActionValidator;

class ActionProcessing extends ModuleProcessing
{
    /** @var \MatchAction */
    private $matchAction;

    public function __construct($matchAction)
    {
        $this->matchAction = $matchAction instanceof MatchAction ? $matchAction : false;
        parent::__construct($this->matchAction, '\STM\Match\MatchEvent');
        MatchActionValidator::init($this->formProcessor);
    }

    public function createAction()
    {
        if (isset($_POST['create'])) {
            unset($_POST['create']);
            $this->formProcessor->escapeValues();
            $_POST['sport'] = isset($_POST['sport']) ? $this->entities->Sport->findById($_POST['sport']) : false;
            $result = MatchAction::create($_POST);
            parent::checkAction($result, 'MATCH_ACTIONS', "New Match Action: {$_POST['match_action']}");
            parent::setSessionMessage($result, $this->getMessage('create', $_POST['match_action']));
            $_POST['sport'] = $_POST['sport'] ? $_POST['sport']->getId() : '';
            return $result;
        } else {
            $this->formProcessor->initVars(array('match_action', 'sport'));
        }
        return false;
    }

    public function updateAction()
    {
        if (parent::isObjectSet() && isset($_POST['update'])) {
            unset($_POST['update']);
            $this->formProcessor->escapeValues();
            $_POST['sport'] = isset($_POST['sport']) ? $this->entities->Sport->findById($_POST['sport']) : false;
            $result = $this->matchAction->update($_POST);
            parent::checkAction(
                $result,
                'MATCH_ACTIONS',
                "Update Match Action {$this->matchAction->__toString()} to {$_POST['match_action']}"
            );
            parent::setSessionMessage($result, $this->getMessage('change'));
            $_POST['sport'] = $_POST['sport'] ? $_POST['sport']->getId() : '';
            return $result;
        } elseif (parent::isObjectSet()) {
            $_POST['match_action'] = $this->matchAction->__toString();
            $_POST['sport'] = $this->matchAction->getIdSport();
        } else {
            $this->formProcessor->initVars(array('match_action', 'sport'));
        }
        return false;
    }

    public function deleteAction()
    {
        if (parent::canBeDeleted()) {
            if ($this->matchAction->canBeDeleted()) {
                $delete = $this->matchAction->delete();
                parent::setSessionMessage(
                    $delete,
                    $this->getMessage('delete-success'),
                    $this->getMessage('delete-fail')
                );
                parent::log('MATCH_ACTIONS', "Delete Match Action: {$this->matchAction->__toString()}", $delete);
            } else {
                $this->session->setMessageFail(SessionMessage::get('action-delete'));
            }
        }
    }

    public function deleteActionTeams()
    {
        $this->deleteActionFromStatistics('teams', 'count_in_teams', '\STM\Match\Action\Team\MatchTeamAction');
    }

    public function deleteActionPlayers()
    {
        $this->deleteActionFromStatistics('players', 'count_in_players', '\STM\Match\Action\Player\MatchPlayerAction');
    }

    private function deleteActionFromStatistics($name, $attribute_name, $class)
    {
        if (parent::canBeDeleted()) {
            $arr = $this->matchAction->toArray();
            if ($arr[$attribute_name] > 0) {
                $name = ucfirst($name);
                $delete = $class::deleteByAction($this->matchAction);
                parent::setSessionMessage(
                    $delete,
                    $this->getMessage('delete-success'),
                    $this->getMessage('delete-fail')
                );
                parent::log(
                    'MATCH_ACTIONS',
                    "Delete Match Action From {$name}: {$this->matchAction->__toString()}",
                    $delete
                );
            } else {
                $this->session->setMessageFail(SessionMessage::get("action-{$name}-delete"));
            }
        }
    }

    private function getMessage($name, $action = false)
    {
        $action = $action || !parent::isObjectSet() ? $action : $this->matchAction->__toString();
        return SessionMessage::get($name, array(stmLang('action', 'name'), $action));
    }
}
