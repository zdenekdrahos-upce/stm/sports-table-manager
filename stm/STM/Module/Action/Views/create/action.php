<?php
/**
 * Create new action
 */
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <?php include(STM_MODULES_ROOT . 'Action/Views/_elements/form-parts/action.php'); ?>

    <input type="submit" name="create" value="<?php echo stmLang('form', 'create'); ?>" />
</form>
