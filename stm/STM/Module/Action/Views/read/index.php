<?php
/*
 * List of all actions
 * $actions
 */
?>

<?php if (empty($actions)): ?>
    <p><?php echo stmLang('no-content'); ?></p>
<?php else: ?>
    <table>
        <thead>
            <tr>
                <td colspan="2">&nbsp;</td>
                <th colspan="2"><?php echo stmLang('action', 'stats'); ?></th>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <th><?php echo stmLang('action', 'name'); ?></th>
                <th><?php echo stmLang('sport', 'sport'); ?></th>
                <th><?php echo stmLang('action', 'count_in_teams'); ?></th>
                <th><?php echo stmLang('action', 'count_in_players'); ?></th>
                <th><?php echo stmLang('team', 'links'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($actions as $action): extract($action->toArray()); ?>
            <tr id="<?php echo $action->getId();?>">
                <td><?php echo $match_action; ?></td>
                <td><a href="?module=sport&action=read&type=sport&spr=<?php echo $action->getIdSport();?>"><?php echo $sport; ?></a></td>
                <td><?php echo $count_in_teams; ?></td>
                <td><?php echo $count_in_players; ?></td>
                <td>
                    <a href="<?php echo buildUrl(array('action' => 'update', 'type' => 'action', 'ma' => $action->getId())) ?>"><?php echo stmLang('form', 'change'); ?></a>,
                    <?php include(STM_MODULES_ROOT . 'Action/Views/_elements/delete/action.php'); ?>
                    <?php include(STM_MODULES_ROOT . 'Action/Views/_elements/delete/action_teams.php'); ?><br />
                    <?php include(STM_MODULES_ROOT . 'Action/Views/_elements/delete/action_players.php'); ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php endif;
