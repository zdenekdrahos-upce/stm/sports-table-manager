    <label for="match_action"><?php echo stmLang('name'); ?>: </label>
    <input type="text" id="match_action" name="match_action" size="40" maxlength="50" value="<?php echo $_POST['match_action']; ?>" />
    <label for="action"><?php echo stmLang('sport', 'sport'); ?>: </label>
    <?php \STM\Web\HTML\Forms::select('sport', \STM\Utils\Arrays::getAssocArray($sports, 'getId', '__toString')); ?>
