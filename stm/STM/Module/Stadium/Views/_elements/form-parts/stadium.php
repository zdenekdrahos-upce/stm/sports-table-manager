    <h2><?php echo stmLang('form', 'required'); ?></h2>
    <label for="name"><?php echo stmLang('name'); ?>: </label>
    <input type="text" id="name" name="name" size="40" maxlength="60" value="<?php echo $_POST['name']; ?>" />
    <label for="name"><?php echo stmLang('stadium', 'capacity'); ?>: </label>
    <input type="text" id="capacity" name="capacity" size="40" maxlength="6" value="<?php echo $_POST['capacity']; ?>" />
    <label for="name"><?php echo stmLang('stadium', 'map-link'); ?>: </label>
    <input type="text" id="map_link" name="map_link" size="40" maxlength="150" value="<?php echo $_POST['map_link']; ?>" />

    <h2><?php echo stmLang('form', 'optional'); ?></h2>
    <label for="name"><?php echo stmLang('stadium', 'field-width'); ?>: </label>
    <input type="text" id="field_width" name="field_width" size="40" maxlength="20" value="<?php echo $_POST['field_width']; ?>" />
    <label for="name"><?php echo stmLang('stadium', 'field-height'); ?>: </label>
    <input type="text" id="field_height" name="field_height" size="40" maxlength="20" value="<?php echo $_POST['field_height']; ?>" />
