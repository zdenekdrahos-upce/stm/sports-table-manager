<?php
$url = '?module=stadium&action=delete&type=stadium&std=' . $stadium->getId();
$submit_message = stmLang('stadium', 'delete');
$question = stmLang('stadium', 'delete-msg') . $stadium->__toString();
include(STM_ADMIN_TEMPLATE_ROOT . 'form/delete-form.php');
