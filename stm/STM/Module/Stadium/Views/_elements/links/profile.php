    <h2><a href="<?php echo buildUrl(array('action' => 'read', 'type' => 'stadium')) ?>"><?php echo $stadium; ?></a></h2>
    <ul>
        <li><a href="<?php echo buildUrl(array('action' => 'update', 'type' => 'stadium')) ?>"><?php echo stmLang('header', 'stadium', 'update', 'stadium'); ?></a></li>
        <li><?php include(STM_MODULES_ROOT . 'Stadium/Views/_elements/delete/stadium.php'); ?></li>
    </ul>
