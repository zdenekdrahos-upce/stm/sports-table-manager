<?php
/*
 * List of all stadiums
 * $stadiums
 */
?>

<?php if (empty($stadiums)): ?>
    <p><?php echo stmLang('no-content'); ?></p>
<?php else: ?>
    <table>
        <tr>
            <th><?php echo stmLang('name'); ?></th>
            <th><?php echo stmLang('team', 'links'); ?></th>
        </tr>
        <?php foreach($stadiums as $stadium): ?>
        <tr>
            <td><?php echo $stadium; ?></td>
            <td>
                <a href="?module=stadium&action=read&type=stadium&std=<?php echo $stadium->getId(); ?>"><?php echo stmLang('header', 'stadium', 'read', 'stadium'); ?></a>,
                <a href="?module=stadium&action=update&type=stadium&std=<?php echo $stadium->getId(); ?>"><?php echo stmLang('form', 'change'); ?></a>,
                <?php include(STM_MODULES_ROOT . 'Stadium/Views/_elements/delete/stadium.php'); ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </table>
<?php endif;
