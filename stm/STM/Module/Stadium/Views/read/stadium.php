<?php
/*
 * Detail of selected stadium
 * $stadium        selected stadium from Stadium::toArray
 */
extract($stadium);
?>

<ul>
    <li><?php echo stmLang('name'); ?>: <strong><?php echo $stadium; ?></strong></li>
    <li><?php echo stmLang('stadium', 'capacity'); ?>: <strong><?php echo $capacity; ?></strong></li>
    <li><?php echo stmLang('stadium', 'map-link'); ?>: <strong><a href="<?php echo is_int(stristr($map_link, 'http://')) ? $map_link : ('http://' . $map_link); ?>" target="_blank"><?php echo stmLang('stadium', 'map'); ?></a></strong></li>
    <li><?php echo stmLang('stadium', 'field-width'); ?>: <strong><?php echo $field_width != '' ? $field_width : stmLang('not-set'); ?></strong></li>
    <li><?php echo stmLang('stadium', 'field-height'); ?>: <strong><?php echo $field_height != '' ? $field_height : stmLang('not-set'); ?></strong></li>
</ul>
