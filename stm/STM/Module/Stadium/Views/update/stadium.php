<?php
/**
 * Update stadium
 */
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <?php include(STM_MODULES_ROOT . 'Stadium/Views/_elements/form-parts/stadium.php'); ?>

    <input type="submit" name="update" value="<?php echo stmLang('form', 'change'); ?>" />

</form>
