<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\Stadium;

use STM\Module\Controller;
use STM\Web\Message\SessionMessage;
use STM\Stadium\Stadium;

class StadiumController extends Controller
{
    /** @var \Stadium */
    private $stadium;
    /** @var \STM\Module\Stadium\StadiumProcessing */
    private $processing;

    protected function __construct()
    {
        parent::__construct();
        parent::setMainPage('index.php?module=stadium');
        $this->loadSelectedStadium();
        $this->processing = new StadiumProcessing($this->stadium);
    }

    /** index.php?module=stadium */
    public function index()
    {
        parent::setIndexInLeftMenu();
        parent::setContent(array('stadiums' => $this->entities->Stadium->findAll()), 'index');
    }

    /** index.php?module=stadium&action=create&type=stadium */
    public function createStadium()
    {
        if ($this->processing->createStadium()) {
            parent::redirectToIndexPage();
        } else {
            parent::setIndexInLeftMenu();
            parent::setFormErrors($this->processing);
            parent::setContent();
        }
    }

    /** index.php?module=stadium&action=read&type=stadium&STD=ID_STADIUM */
    public function readStadium()
    {
        if ($this->checkSelectedStadium()) {
            parent::setProfileInLeftMenu();
            parent::setContent(array('stadium' => $this->stadium->toArray()));
        }
    }

    /** index.php?module=stadium&action=update&type=stadium&STD=ID_STADIUM */
    public function updateStadium()
    {
        if ($this->checkSelectedStadium()) {
            if ($this->processing->updateStadium()) {
                redirect(buildUrl(array('action' => 'read', 'type' => 'stadium')));
            } else {
                parent::setProfileInLeftMenu();
                parent::setFormErrors($this->processing);
                parent::setContent();
            }
        }
    }

    public function deleteStadium()
    {
        if ($this->checkSelectedStadium()) {
            $this->processing->deleteStadium();
            parent::redirectToIndexPage();
        }
    }

    private function checkSelectedStadium()
    {
        if (!($this->stadium instanceof Stadium)) {
            $this->session->setMessageFail(SessionMessage::get('invalid', array(stmLang('club', 'stadium'))));
            parent::redirectToIndexPage();
        }
        return true;
    }

    private function loadSelectedStadium()
    {
        $this->stadium = isset($_GET['std']) ? $this->entities->Stadium->findById($_GET['std']) : false;
    }

    protected function getProfileLinks()
    {
        return array('stadium' => $this->stadium);
    }
}
