<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\Stadium;

use STM\Module\ModuleProcessing;
use STM\Web\Message\SessionMessage;
use STM\Stadium\Stadium;
use STM\Stadium\StadiumValidator;

class StadiumProcessing extends ModuleProcessing
{
    /** @var Stadium */
    private $stadium;

    public function __construct($stadium)
    {
        $this->stadium = $stadium instanceof Stadium ? $stadium : false;
        parent::__construct($this->stadium, '\STM\Team\TeamEvent');
        StadiumValidator::init($this->formProcessor);
    }

    public function createStadium()
    {
        if (isset($_POST['create'])) {
            unset($_POST['create']);
            $this->formProcessor->escapeValues();
            $result = Stadium::create($_POST);
            parent::checkAction($result, 'STADIUM', "New Stadium: {$_POST['name']}");
            parent::setSessionMessage($result, $this->getMessage('create', $_POST['name']));
            return $result;
        } else {
            $this->formProcessor->initVars(array('name', 'capacity', 'map_link', 'field_width', 'field_height'));
        }
        return false;
    }

    public function updateStadium()
    {
        if (isset($_POST['update'])) {
            unset($_POST['update']);
            $this->formProcessor->escapeValues();
            $result = $this->stadium->update($_POST);
            parent::checkAction(
                $result,
                'STADIUM',
                "Update Stadium: {$_POST['name']} ({$this->stadium->__toString()})"
            );
            parent::setSessionMessage($result, $this->getMessage('change'));
            return $result;
        } else {
            $arr = $this->stadium->toArray();
            unset($arr['stadium']);
            foreach ($arr as $key => $value) {
                $_POST[$key] = $value;
            }
            $_POST['name'] = $this->stadium->__toString();
        }
        return false;
    }

    public function deleteStadium()
    {
        if (parent::canBeDeleted()) {
            $delete = $this->stadium->delete();
            parent::setSessionMessage($delete, $this->getMessage('delete-success'), $this->getMessage('delete-fail'));
            parent::log('STADIUM', "Delete Stadium: {$this->stadium->__toString()}", $delete);
        }
    }

    private function getMessage($name, $stadium = false)
    {
        $stadium = $stadium || !parent::isObjectSet() ? $stadium : $this->stadium->__toString();
        return SessionMessage::get($name, array(stmLang('club', 'stadium'), $stadium));
    }
}
