<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\Sport;

use STM\Module\Controller;
use STM\Web\Message\SessionMessage;
use STM\Sport\Sport;

class SportController extends Controller
{
    /** @var \Sport */
    private $sport;
    /** @var \STM\Module\Sport\SportProcessing */
    private $processing;

    protected function __construct()
    {
        parent::__construct();
        parent::setMainPage('index.php?module=sport');
        $this->loadSelectedSport();
        $this->processing = new SportProcessing($this->sport);
    }

    /** index.php?module=sport */
    public function index()
    {
        parent::setIndexInLeftMenu();
        parent::setContent(array('sports' => $this->entities->Sport->findAll()), 'index');
    }

    /** index.php?module=sport&action=create&type=sport */
    public function createSport()
    {
        if ($this->processing->createSport()) {
            parent::redirectToIndexPage();
        } else {
            parent::setIndexInLeftMenu();
            parent::setFormErrors($this->processing);
            parent::setContent(array('sports' => $this->entities->Sport->findAll()));
        }
    }

    /** index.php?module=sport&action=read&type=sport&SPR=ID_SPORT */
    public function readSport()
    {
        if ($this->checkSelectedSport()) {
            parent::setProfileInLeftMenu();
            parent::setContent(
                array(
                    'sport' => $this->sport,
                    'childs' => $this->entities->Sport->findSubcategories($this->sport),
                    'actions' => $this->entities->MatchAction->findBySport($this->sport),
                    'positions' => $this->entities->PersonPosition->findBySport($this->sport),
                )
            );
        }
    }

    /** index.php?module=sport&action=update&type=sport&SPR=ID_SPORT */
    public function updateSport()
    {
        if ($this->checkSelectedSport()) {
            if ($this->processing->updateSport()) {
                redirect(buildUrl(array('action' => 'read', 'type' => 'sport')));
            } else {
                parent::setProfileInLeftMenu();
                parent::setFormErrors($this->processing);
                parent::setContent(array('sports' => $this->entities->Sport->findAll()));
            }
        }
    }

    /** index.php?module=sport&action=delete&type=sport&SPR=ID_SPORT */
    public function deleteSport()
    {
        if ($this->checkSelectedSport()) {
            $this->processing->deleteSport();
            parent::redirectToIndexPage();
        }
    }

    private function checkSelectedSport()
    {
        if (!($this->sport instanceof Sport)) {
            $this->session->setMessageFail(SessionMessage::get('invalid', array(stmLang('sport', 'sport'))));
            parent::redirectToIndexPage();
        }
        return true;
    }

    private function loadSelectedSport()
    {
        $this->sport = isset($_GET['spr']) ? $this->entities->Sport->findById($_GET['spr']) : false;
    }

    protected function getProfileLinks()
    {
        return array('sport_name' => $this->sport->__toString());
    }
}
