<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\Sport;

use STM\Module\ModuleProcessing;
use STM\Web\Message\SessionMessage;
use STM\Sport\Sport;
use STM\Sport\SportValidator;

class SportProcessing extends ModuleProcessing
{
    /** @var Sport */
    private $sport;

    public function __construct($sport)
    {
        $this->sport = $sport instanceof Sport ? $sport : false;
        parent::__construct($this->sport, '\STM\Team\TeamEvent');
        SportValidator::init($this->formProcessor);
    }

    public function createSport()
    {
        if (isset($_POST['create'])) {
            $this->formProcessor->escapeValues();
            $result = Sport::create(
                array(
                    'sport' => $_POST['sport'],
                    'parent_sport' => $this->entities->Sport->findById($_POST['id_parent'])
                )
            );
            parent::checkAction($result, 'SPORT', "New Sport: {$_POST['sport']}");
            parent::setSessionMessage($result, $this->getMessage('create', $_POST['sport']));
            return $result;
        } else {
            $this->formProcessor->initVars(array('sport', 'id_parent'));
        }
        return false;
    }

    public function deleteSport()
    {
        if (parent::canBeDeleted()) {
            if ($this->sport->canBeDeleted()) {
                $delete = $this->sport->delete();
                parent::setSessionMessage(
                    $delete,
                    $this->getMessage('delete-success'),
                    $this->getMessage('delete-fail')
                );
                parent::log('SPORT', "Delete Sport: {$this->sport->__toString()}", $delete);
            } else {
                $this->session->setMessageFail(SessionMessage::get('sport-delete'));
            }
        }
    }

    public function updateSport()
    {
        if (parent::isObjectSet() && isset($_POST['update'])) {
            $this->formProcessor->escapeValues();
            $result = $this->sport->update(
                array(
                    'sport' => $_POST['sport'],
                    'parent_sport' => $this->entities->Sport->findById($_POST['id_parent'])
                )
            );
            parent::checkAction($result, 'SPORT', "Update Sport {$_POST['sport']} ({$this->sport->__toString()})");
            parent::setSessionMessage($result, $this->getMessage('change', $_POST['sport']));
            return $result;
        } else {
            $_POST['sport'] = $this->sport->__toString();
            $_POST['id_parent'] = $this->sport->getIdParent();
        }
        return false;
    }

    private function getMessage($name, $sport = false)
    {
        $sport = $sport || !parent::isObjectSet() ? $sport : $this->sport->__toString();
        return SessionMessage::get($name, array(stmLang('sport', 'sport'), $sport));
    }
}
