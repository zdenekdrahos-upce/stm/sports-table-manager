    <label for="sport"><?php echo stmLang('name'); ?>: </label>
    <input type="text" id="sport" name="sport" size="40" maxlength="50" value="<?php echo $_POST['sport']; ?>" />
    <label for="id_parent"><?php echo stmLang('sport', 'parent'); ?>: </label>
    <?php \STM\Web\HTML\Forms::select('id_parent', \STM\Utils\Arrays::getAssocArray($sports, 'getId', '__toString'), true); ?>
