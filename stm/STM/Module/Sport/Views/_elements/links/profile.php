    <h2><a href="<?php echo buildUrl(array('action' => 'read', 'type' => 'sport')) ?>"><?php echo $sport_name; ?></a></h2>
    <ul>
        <li><a href="<?php echo buildUrl(array('action' => 'update', 'type' => 'sport')) ?>"><?php echo stmLang('header', 'sport', 'update', 'sport'); ?></a></li>
        <li><?php include(STM_MODULES_ROOT . 'Sport/Views/_elements/delete/sport.php'); ?></li>
    </ul>
