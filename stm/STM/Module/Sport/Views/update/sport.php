<?php
/**
 * Update name of the sport
 */
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <?php include(STM_MODULES_ROOT . 'Sport/Views/_elements/form-parts/sport.php'); ?>

    <input type="submit" name="update" value="<?php echo stmLang('form', 'change'); ?>" />
</form>
