<?php
/*
 * Detail of selected sport
 * $sport        selected sport
 * $childs
 * $actions
 * $positions
 */
extract($sport->toArray());
?>

<ul>
    <li><?php echo stmLang('name'); ?>: <strong><?php echo $sport; ?></strong></li>
    <li><?php echo stmLang('category', 'parent'); ?>: <strong><?php echo $parent_sport == false ? stmLang('form', 'empty') : ("<a href=\"" . buildUrl(array('spr' => $sport->getIdParent())) . "\">{$parent_sport}</a>"); ?></strong></li>
    <li><?php echo stmLang('sport', 'count_childs'); ?>: <strong><?php echo $count_childs; ?></strong></li>
    <li><?php echo stmLang('sport', 'count_positions'); ?>: <strong><?php echo $count_positions; ?></strong></li>
    <li><?php echo stmLang('sport', 'count_match_actions'); ?>: <strong><?php echo $count_match_actions; ?></strong></li>
</ul>

<h2><?php echo stmLang('category', 'descendants'); ?></h2>
<?php if (empty($childs)): ?>
    <p><?php echo stmLang('no-content'); ?></p>
<?php else: ?>
    <ul>
        <?php foreach($childs as $child): ?>
        <li><a href="<?php echo buildUrl(array('spr' => $child->getId())); ?>"><?php echo $child; ?></a></li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>

<h2><?php echo stmLang('sport', 'actions'); ?></h2>
<?php if (empty($actions)): ?>
    <p><?php echo stmLang('no-content'); ?></p>
<?php else: ?>
    <ul>
        <?php foreach($actions as $action): ?>
        <li><a href="?module=action#<?php echo $sport->getId(); ?>"><?php echo $action; ?></a></li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>

<h2><?php echo stmLang('sport', 'positions'); ?></h2>
<?php if (empty($positions)): ?>
    <p><?php echo stmLang('no-content'); ?></p>
<?php else: ?>
    <ul>
        <?php foreach($positions as $position): ?>
        <li><a href="?module=position#<?php echo $position->getId(); ?>"><?php echo $position; ?></a></li>
        <?php endforeach; ?>
    </ul>
<?php endif;
