<?php
/*
 * List of all sports
 * $sports
 */
?>

<?php if (empty($sports)): ?>
    <p><?php echo stmLang('no-content'); ?></p>
<?php else: ?>
    <table>
        <tr>
            <th><?php echo stmLang('sport', 'sport'); ?></th>
            <th><?php echo stmLang('category', 'parent'); ?></th>
            <th><?php echo stmLang('sport', 'count_childs'); ?></th>
            <th><?php echo stmLang('sport', 'count_positions'); ?></th>
            <th><?php echo stmLang('sport', 'count_match_actions'); ?></th>
        </tr>
        <?php foreach($sports as $sport): extract($sport->toArray()); ?>
        <tr>
            <td><a href="?module=sport&action=read&type=sport&spr=<?php echo $sport->getId(); ?>"><?php echo $sport; ?></a></td>
            <td><a href="?module=sport&action=read&type=sport&spr=<?php echo $sport->getIdParent(); ?>"><?php echo $parent_sport ? $parent_sport : '-'; ?></a></td>
            <td><?php echo $count_childs; ?></td>
            <td><?php echo $count_positions; ?></td>
            <td><?php echo $count_match_actions; ?></td>
        </tr>
        <?php endforeach; ?>
    </table>
<?php endif;
