<?php
/*
 * List of all positions
 * $positions
 */
?>

<?php if (empty($positions)): ?>
    <p><?php echo stmLang('no-content'); ?></p>
<?php else: ?>
    <table>
        <thead>
            <tr>
                <th><?php echo stmLang('name'); ?></th>
                <th><?php echo stmLang('position', 'abbrevation'); ?></th>
                <th><?php echo stmLang('sport', 'sport'); ?></th>
                <th><?php echo stmLang('team', 'links'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($positions as $print_position): extract($print_position->toArray()); ?>
            <tr id="<?php echo $print_position->getId();?>">
                <td><?php echo $print_position; ?></td>
                <td><?php echo $abbrevation; ?></td>
                <td><a href="?module=sport&action=read&type=sport&spr=<?php echo $print_position->getIdSport();?>"><?php echo $sport; ?></a></td>
                <td>
                    <a href="<?php echo buildUrl(array('action' => 'update', 'type' => 'position', 'pos' => $print_position->getId())) ?>">
                        <?php echo stmLang('form', 'change'); ?>
                    </a>,
                    <?php include(STM_MODULES_ROOT . 'Position/Views/_elements/delete/position.php'); ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php endif;
