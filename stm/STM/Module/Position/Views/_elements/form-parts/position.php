<label for="position"><?php echo stmLang('name'); ?>: </label>
<input type="text" id="position" name="position" size="40" maxlength="30" value="<?php echo $_POST['position']; ?>" />
<label for="abbrevation"><?php echo stmLang('position', 'abbrevation'); ?>: </label>
<input type="text" id="abbrevation" name="abbrevation" size="40" maxlength="5" value="<?php echo $_POST['abbrevation']; ?>" />
<label for="sport"><?php echo stmLang('sport', 'sport'); ?>: </label>
<?php \STM\Web\HTML\Forms::select('sport', \STM\Utils\Arrays::getAssocArray($sports, 'getId', '__toString')); ?>
