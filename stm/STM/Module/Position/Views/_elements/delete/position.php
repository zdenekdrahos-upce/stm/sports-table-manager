<?php
$url = '?module=position&action=delete&type=position&pos=' . $print_position->getId();
$submit_message = stmLang('form', 'delete');
$question = stmLang('position', 'delete-msg') . $print_position->__toString();
include(STM_ADMIN_TEMPLATE_ROOT . 'form/delete-form.php');
