<?php
/**
 * Update position
 */
?>

<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <?php include(STM_MODULES_ROOT . 'Position/Views/_elements/form-parts/position.php'); ?>

    <input type="submit" name="update" value="<?php echo stmLang('form', 'change'); ?>" />

</form>
