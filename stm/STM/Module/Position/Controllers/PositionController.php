<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\Position;

use STM\Module\Controller;
use STM\Web\Message\SessionMessage;
use STM\Person\Position\PersonPosition;

class PositionController extends Controller
{
    /** @var \PersonPosition */
    private $position;
    /** @var \STM\Module\Position\PositionProcessing */
    private $processing;

    protected function __construct()
    {
        parent::__construct();
        parent::setMainPage('index.php?module=position');
        $this->loadSelectedPosition();
        $this->processing = new PositionProcessing($this->position);
    }

    /** index.php?module=position */
    public function index()
    {
        parent::setIndexInLeftMenu();
        parent::setContent(array('positions' => $this->entities->PersonPosition->findAll()), 'index');
    }

    /** index.php?module=position&action=create&type=position */
    public function createPosition()
    {
        if ($this->processing->createPosition()) {
            parent::redirectToIndexPage();
        } else {
            parent::setIndexInLeftMenu();
            parent::setFormErrors($this->processing);
            parent::setContent(array('sports' => $this->entities->Sport->findAll()));
        }
    }

    /** index.php?module=position&action=update&type=position&POS=ID_PERSON_POSITION */
    public function updatePosition()
    {
        if ($this->checkSelectedPosition()) {
            if ($this->processing->updatePosition()) {
                parent::redirectToIndexPage();
            } else {
                parent::setIndexInLeftMenu();
                parent::setFormErrors($this->processing);
                parent::setContent(array('sports' => $this->entities->Sport->findAll()));
            }
        }
    }

    /** index.php?module=position&action=delete&type=position&POS=ID_PERSON_POSITION */
    public function deletePosition()
    {
        if ($this->checkSelectedPosition()) {
            $this->processing->deletePosition();
            parent::redirectToIndexPage();
        }
    }

    private function checkSelectedPosition()
    {
        if (!($this->position instanceof PersonPosition)) {
            $this->session->setMessageFail(SessionMessage::get('invalid', array(stmLang('club', 'position'))));
            parent::redirectToIndexPage();
        }
        return true;
    }

    private function loadSelectedPosition()
    {
        $this->position = isset($_GET['pos']) ? $this->entities->PersonPosition->findById($_GET['pos']) : false;
    }
}
