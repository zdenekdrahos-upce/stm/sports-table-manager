<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Module\Position;

use STM\Module\ModuleProcessing;
use STM\Web\Message\SessionMessage;
use STM\Person\Position\PersonPosition;
use STM\Person\Position\PersonPositionValidator;

class PositionProcessing extends ModuleProcessing
{
    /** @var \PersonPosition */
    private $person_position;

    public function __construct($person_position)
    {
        $this->person_position = $person_position instanceof PersonPosition ? $person_position : false;
        parent::__construct($this->person_position, '\STM\Team\TeamEvent');
        PersonPositionValidator::init($this->formProcessor);
    }

    public function createPosition()
    {
        if (isset($_POST['create'])) {
            unset($_POST['create']);
            $this->formProcessor->escapeValues();
            $_POST['sport'] = isset($_POST['sport']) ? $this->entities->Sport->findById($_POST['sport']) : false;
            $result = PersonPosition::create($_POST);
            parent::checkAction($result, 'PERSON_POSITION', "New Person Position: {$_POST['position']}");
            parent::setSessionMessage($result, $this->getMessage('create', $_POST['position']));
            return $result;
        } else {
            $this->formProcessor->initVars(array('position', 'abbrevation', 'sport'));
        }
        return false;
    }

    public function deletePosition()
    {
        if (parent::canBeDeleted()) {
            if ($this->person_position->canBeDeleted()) {
                $delete = $this->person_position->delete();
                parent::setSessionMessage(
                    $delete,
                    $this->getMessage('delete-success'),
                    $this->getMessage('delete-fail')
                );
                parent::log(
                    'PERSON_POSITION',
                    "Delete Person Position: {$this->person_position->__toString()}",
                    $delete
                );
            } else {
                $this->session->setMessageFail(SessionMessage::get('position-delete'));
            }
        }
    }

    public function updatePosition()
    {
        if (parent::isObjectSet() && isset($_POST['update'])) {
            unset($_POST['update']);
            $this->formProcessor->escapeValues();
            $_POST['sport'] = isset($_POST['sport']) ? $this->entities->Sport->findById($_POST['sport']) : false;
            $result = $this->person_position->update($_POST);
            parent::checkAction(
                $result,
                'PERSON_POSITION',
                "Update Person Position {$this->person_position->__toString()}"
            );
            parent::setSessionMessage($result, $this->getMessage('change'));
            $_POST['sport'] = $_POST['sport']->getId();
            return $result;
        } elseif (parent::isObjectSet()) {
            $_POST['position'] = $this->person_position->__toString();
            $_POST['abbrevation'] = $this->person_position->getAbbrevation();
            $_POST['sport'] = $this->person_position->getIdSport();
        } else {
            $this->formProcessor->initVars(array('position', 'abbrevation', 'sport'));
        }
        return false;
    }

    private function getMessage($name, $position = false)
    {
        $position = $position || !parent::isObjectSet() ? $position : $this->person_position->__toString();
        return SessionMessage::get($name, array(stmLang('club', 'position'), $position));
    }
}
