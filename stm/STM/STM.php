<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM;

use STM\Session;
use STM\Module\Controller;
use STM\Web\View\View;
use STM\Web\View\ViewTemplate;
use STM\Web\Menu\Menu;
use STM\Web\Menu\MenuPrintout;
use STM\Web\Page\Page;
use STM\Web\Page\PageException;
use STM\DB\DatabaseEvent;
use STM\DB\DatabaseException;

class STM
{
    /** @var \STM\Web\Page\Page */
    private $page;
    /** @var \STM\Module\Controller */
    private $controller;
    /** @var string */
    private $viewPath;
    /** @var array */
    private $viewData;

    public function run()
    {
        try {
            $this->loadPage();
            $this->createController();
            $this->loadView();
        } catch (PageException $e) {
            $this->redirectAfterPageException($e);
        } catch (\Exception $e) {
            $this->checkDBException($e);
            $this->loadErrorView($e);
        }
        $this->displayView();
    }

    private function loadPage()
    {
        $this->page = Page::loadFromUrl();
    }

    private function createController()
    {
        $this->controller = Controller::create($this->page);
        call_user_func(array($this->controller, $this->page->getMethodName()));
    }

    private function loadView()
    {
        $this->viewPath = STM_ADMIN_TEMPLATE_ROOT . 'web/page.php';
        $this->viewData = array(
            'page' => $this->page,
            'controller' => $this->controller,
            'session' => Session::getInstance(),
            'menu' => $this->getMenu(),
        );
    }

    private function getMenu()
    {
        $menu = new MenuPrintout();
        $menu->setMenu(Menu::load());
        $menu->setSelectedItem($this->page->getSelectedModuleForMenu());
        return $menu;
    }

    private function redirectAfterPageException($e)
    {
        if ($e->failMessage) {
            $session = Session::getInstance();
            $session->setMessageFail($e->failMessage);
        }
        redirect($e->getMessage());
    }

    private function checkDBException($e)
    {
        if ($e instanceof DatabaseException) {
            DatabaseEvent::log($e);
        }
    }

    private function loadErrorView($e)
    {
        $this->viewPath = STM_ADMIN_TEMPLATE_ROOT . 'web/error.php';
        $this->viewData = array('exception' => $e);
    }

    public function displayView()
    {
        $view = new View();
        $view->setContent(new ViewTemplate($this->viewPath, $this->viewData));
        $view->display();
    }
}
