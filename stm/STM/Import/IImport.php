<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Import;

interface IImport
{
    public function getMaxStep();

    public function getValidKeys();

    public function step1();
}
