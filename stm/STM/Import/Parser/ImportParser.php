<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Import\Parser;

use STM\Utils\Strings;

final class ImportParser
{
    /** @var ImportFormat */
    private $format;

    public function __construct(ImportFormat $format)
    {
        $this->format = $format;
    }

    public function parseString($string)
    {
        $result = array();
        if (Strings::isStringNonEmpty($string)) {
            $position = 0;
            $positionOfLastDelimiter = 0;
            foreach ($this->format->getDelimitersFromFormat() as $position => $delimiter) {
                $nextDelimiterPosition = mb_strpos($string, $delimiter, $positionOfLastDelimiter, 'UTF-8');
                if ($nextDelimiterPosition === false) {
                    $position--;
                    break;
                }
                $content = mb_substr(
                    $string,
                    $positionOfLastDelimiter,
                    $nextDelimiterPosition - $positionOfLastDelimiter,
                    'UTF-8'
                );
                $key = $this->format->getKeyAtPositionInFormat($position);
                if ($key && Strings::isStringNonEmpty($content)) {
                    $this->addNew($result, $key, $content);
                }
                $positionOfLastDelimiter = $nextDelimiterPosition + mb_strlen($delimiter, 'UTF-8');
            }
            if ($positionOfLastDelimiter < mb_strlen($string, 'UTF-8')) {
                $content = mb_substr(
                    $string,
                    $positionOfLastDelimiter,
                    mb_strlen($string, 'UTF-8') - $positionOfLastDelimiter,
                    'UTF-8'
                );
                $key = $this->format->getKeyAtPositionInFormat($position + 1);
                $this->addNew($result, $key, $content);
            }
        }
        return $result;
    }

    private function addNew(&$result, $key, $content)
    {
        if ($key instanceof ImportKey) {
            switch ($key->getValueType()) {
                case ImportValueType::ARRAYS:
                    $result[$key->getName()][] = $content;
                    break;
                case ImportValueType::STRING:
                    if (!array_key_exists($key->getName(), $result)) {
                        $result[$key->getName()] = '';
                    }
                    $result[$key->getName()] .= $content;
                    break;
            }
        }
    }
}
