<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Import\Parser;

use STM\Utils\Classes;
use STM\Utils\Strings;

final class ImportKey
{
    /** @var string */
    private $key;
    /** @var string */
    private $name;
    /** @var ImportValueType constant */
    private $valueType;

    /**
     * @param string $key
     * @param string $name
     * @param ImportValueType constant $valueType
     */
    public function __construct($key, $name, $valueType = ImportValueType::STRING)
    {
        $this->key = $key;
        $this->name = $name;
        $this->valueType = $valueType;
    }

    /** @return boolean */
    public function isValid()
    {
        return $this->isValidKey() && $this->isValidName() && $this->isValidType();
    }

    /** @return string|false */
    public function getKey()
    {
        if ($this->isValidKey()) {
            return '{' . $this->key . '}';
        }
        return false;
    }

    /** @return string|false */
    public function getName()
    {
        if ($this->isValidName()) {
            return $this->name;
        }
        return false;
    }

    /** @return ImportValueType constant|false */
    public function getValueType()
    {
        if ($this->isValidType()) {
            return $this->valueType;
        }
        return false;
    }

    private function isValidKey()
    {
        return Strings::isStringNonEmpty($this->key);
    }

    private function isValidName()
    {
        return Strings::isStringNonEmpty($this->name);
    }

    private function isValidType()
    {
        return Classes::isClassConstant('\STM\Import\Parser\ImportValueType', $this->valueType);
    }
}
