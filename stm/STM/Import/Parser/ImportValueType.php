<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Import\Parser;

final class ImportValueType
{
    const STRING = 'STRING';
    const ARRAYS = 'ARRAYS';
}
