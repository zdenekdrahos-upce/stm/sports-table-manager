<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Import\Parser;

use STM\Utils\Arrays;
use STM\Utils\Strings;

define('STM_IMPORT_SPECIAL_CHAR', '§');

final class ImportFormat
{
    /** @var string */
    private $format;
    /** @var array */
    private $keys;
    /** @var array */
    private $keysInFormat;
    /** @var array */
    private $delimitersInFormat;

    public function __construct()
    {
        $this->format = '';
        $this->keys = array();
        $this->keysInFormat = array();
        $this->delimitersInFormat = array();
    }

    /** @param string $format */
    public function setFormat($format)
    {
        if (Strings::isStringNonEmpty($format)) {
            $this->format = $format;
            $this->updateInstance();
        }
    }

    /** @param ImportKey $key */
    public function addKey(ImportKey $key)
    {
        if ($key->isValid()) {
            $this->keys[$key->getKey()] = $key;
            $this->updateInstance();
        }
    }

    /**
     * @param int $position
     * @return ImportKey|false
     */
    public function getKeyAtPositionInFormat($position)
    {
        if (is_int($position) && $position > 0 && $position <= count($this->keysInFormat)) {
            foreach ($this->keysInFormat as $pos => $key) {
                if ($pos == ($position - 1)) {
                    return $this->keys[$key];
                }
            }
        }
        return false;
    }


    /** @return array */
    public function getDelimitersFromFormat()
    {
        return $this->delimitersInFormat;
    }

    private function updateInstance()
    {
        $this->updateDelimitersInFormat();
        $this->updateKeysInFormat();
    }

    private function updateKeysInFormat()
    {
        $this->keysInFormat = $this->getFormatAsArray($this->getDelimitersFromFormat());
    }

    private function updateDelimitersInFormat()
    {
        $keys = Arrays::getAssocArray($this->keys, 'getKey', 'getKey');
        $this->delimitersInFormat = $this->getFormatAsArray(array_values($keys));
    }

    private function getFormatAsArray($replacedKeys)
    {
        $formatWithReplacedItems = str_replace($replacedKeys, STM_IMPORT_SPECIAL_CHAR, $this->format);
        $explodedFormat = explode(STM_IMPORT_SPECIAL_CHAR, $formatWithReplacedItems);
        return array_filter($explodedFormat, 'strlen');
    }
}
