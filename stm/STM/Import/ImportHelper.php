<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Import;

use STM\Import\Parser\ImportFormat;
use STM\Import\Parser\ImportKey;
use STM\Import\Parser\ImportParser;
use STM\Import\Parser\ImportValueType;

final class ImportHelper
{
    public static function getParser(IImport $import, $format)
    {
        if (is_int(strpos($format, '\t'))) {
            $format = str_replace('\\t', "\t", $format);
        }
        $import_format = new ImportFormat();
        $import_format->setFormat($format);
        foreach ($import->getValidKeys() as $key) {
            $import_format->addKey($key);
        }
        $import_format->addKey(new ImportKey('W', 'waste', ImportValueType::STRING));
        return new ImportParser($import_format);
    }

    public static function parseStringToArray($input_string)
    {
        return preg_split("/(\r\n|\n|\r)/", $input_string);
    }
}
