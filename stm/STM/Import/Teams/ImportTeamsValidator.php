<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Import\Teams;

use STM\Helpers\ObjectValidator;
use STM\Web\Message\FormError;
use STM\Utils\Strings;

final class ImportTeamsValidator
{
    /** @var ObjectValidator */
    private static $validator;

    /** @param \STM\Libs\FormProcessor $formProcessor */
    public static function init($formProcessor = false)
    {
        if (is_null(self::$validator)) {
            self::$validator = new ObjectValidator('\STM\Team\Team');
        }
        self::$validator->setFormProcessor($formProcessor);
    }

    public static function checkFirstStep()
    {
        // check format
        $format = isset($_POST['format']) ? $_POST['format'] : false;
        self::$validator->getFormProcessor()->checkCondition(
            Strings::isStringNonEmpty($format),
            FormError::get('empty-value', array(stmLang('import', 'format')))
        );
        // check input
        self::$validator->getFormProcessor()->checkCondition(
            isset($_POST['input']) && Strings::isStringNonEmpty($_POST['input']),
            FormError::get('empty-value', array(stmLang('import', 'input')))
        );
        return self::$validator->getFormProcessor()->isValid();
    }

    public static function checkSecondStep()
    {
        $_POST['teams'] = isset($_POST['teams']) ? $_POST['teams'] : array();
        $count_selected = 0;
        foreach ($_POST['teams'] as $team) {
            if (array_key_exists('checked', $team)) {
                $count_selected++;
            }
        }
        self::$validator->getFormProcessor()->checkCondition(
            $count_selected > 0,
            FormError::get('nothing-checked')
        );
        return self::$validator->getFormProcessor()->isValid();
    }
}
