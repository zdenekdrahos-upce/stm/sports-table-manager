<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Import\Teams;

use STM\Import\IImport;
use STM\Import\Parser\ImportKey;
use STM\Import\Parser\ImportValueType;
use STM\Import\ImportHelper;
use STM\Libs\FormProcessor;
use STM\Club\Club;
use STM\Club\ClubValidator;
use STM\Team\Team;
use STM\Team\TeamValidator;
use STM\Module\ModuleProcessing;
use STM\StmFactory;

final class ImportTeams extends ModuleProcessing implements IImport
{
    private $clubs;

    public function __construct($object = false)
    {
        parent::__construct($object, '\STM\Team\TeamEvent');
        ImportTeamsValidator::init($this->formProcessor);
        $this->loadClubs();
    }

    public function getMaxStep()
    {
        return 3;
    }

    public function getValidKeys()
    {
        return array(
            new ImportKey('T', 'team', ImportValueType::STRING),
            new ImportKey('C', 'club', ImportValueType::STRING),
        );
    }

    public function step1()
    {
        return array();
    }

    public function step2()
    {
        if (ImportTeamsValidator::checkFirstStep()) {
            return array(
                'teams' => $this->getTeams(),
                'clubs' => $this->clubs
            );
        }
        return false;
    }

    public function step3()
    {
        if (isset($_SESSION['import'])) {
            $info = $_SESSION['import'];
            $_SESSION['import'] = array();
            unset($_SESSION['import']);
            return array('import' => $info);
        } elseif (ImportTeamsValidator::checkSecondStep()) {
            $this->saveToDatabase();
            return true;
        }
        return false;
    }

    private function saveToDatabase()
    {
        $count_clubs = 0;
        $count_created_clubs = 0;
        $count_teams = 0;
        $count_created_teams = 0;
        $all_errors = array();
        $club_formProcessor = new FormProcessor();
        foreach ($_POST['teams'] as $team) {
            if (array_key_exists('checked', $team)) {
                $this->formProcessor->resetErrors();
                $club_formProcessor->resetErrors();

                $temp_teams = $this->getTempArray($team);
                // club create
                if (!is_object($temp_teams['club'])) {
                    $count_clubs++;
                    ClubValidator::init($club_formProcessor);
                    if (Club::create($this->getClubArrayForCreate($temp_teams['club']))) {
                        $count_created_clubs++;
                        $this->loadClubs();
                        $temp_teams = $this->getTempArray($team);
                    }
                }
                // team create
                if ($temp_teams['team']) {
                    TeamValidator::init($this->formProcessor);
                    $count_teams++;
                    if (Team::create(array('name' => $temp_teams['team'], 'club' => $temp_teams['club']))) {
                        $count_created_teams++;
                    }
                }
                // add club errors
                foreach ($club_formProcessor->getErrors() as $error) {
                    $this->formProcessor->addError($error); // because formProcessor is reset in init
                }
                // add errors
                if (!$this->formProcessor->isValid()) {
                    $all_errors[] = array(
                        'text' => $team['original'],
                        'errors' => $this->formProcessor->getErrors()
                    );
                }
            }
        }
        $_SESSION['import'] = array(
            'all_clubs' => $count_clubs,
            'created_clubs' => $count_created_clubs,
            'all_teams' => $count_teams,
            'created_teams' => $count_created_teams,
            'errors' => $all_errors
        );
        parent::log(
            'IMPORT',
            "Clubs: {$count_created_clubs}/{$count_clubs}, Teams: {$count_created_teams}/{$count_teams}",
            true
        );
    }

    private function getTeams()
    {
        $teams = array();
        $club_formProcessor = new FormProcessor();
        $parser = ImportHelper::getParser($this, $_POST['format']);
        $lines = ImportHelper::parseStringToArray($_POST['input']);
        foreach ($lines as $line) {
            $this->formProcessor->resetErrors();
            $parsed_array = $parser->parseString($line);
            if ((isset($parsed_array['team']) || isset($parsed_array['club']))) {
                if (!isset($parsed_array['club'])) {
                    $parsed_array['club'] = $parsed_array['team'];
                }
                $temp_teams = $this->getTempArray($parsed_array);
                if ($temp_teams['team']) {
                    TeamValidator::init($this->formProcessor);
                    if (is_object($temp_teams['club'])) {
                        TeamValidator::checkCreate(
                            array('name' => $temp_teams['team'], 'club' => $temp_teams['club'])
                        );
                    } else {
                        TeamValidator::checkName($temp_teams['team']);
                    }
                }
                if (!is_object($temp_teams['club'])) {
                    ClubValidator::init($club_formProcessor);
                    ClubValidator::checkName($temp_teams['club']);
                    foreach ($club_formProcessor->getErrors() as $error) {
                        $this->formProcessor->addError($error); // because formProcessor is reset in init
                    }
                }
                $teams[] = array(
                    'original' => $line,
                    'teams' => $temp_teams,
                    'errors' => $this->formProcessor->getErrors(),
                );
            }
        }
        return $teams;
    }



    private function getTempArray($input_array)
    {
        return array(
            'team' => isset($input_array['team']) ? $input_array['team'] : '',
            'club' => isset($this->clubs[$input_array['club']]) ?
                $this->clubs[$input_array['club']] : $input_array['club'],
        );
    }

    private function getClubArrayForCreate($club_name)
    {
        $attributes = array();
        foreach (ClubValidator::getAttributes() as $key) {
            $attributes[$key] = '';
        }
        $attributes['name'] = $club_name;
        return $attributes;
    }

    private function loadClubs()
    {
        $this->clubs = array();
        foreach (StmFactory::find()->Club->findAll() as $club) {
            $this->clubs[$club->__toString()] = $club;
        }
    }
}
