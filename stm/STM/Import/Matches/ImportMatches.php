<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Import\Matches;

use STM\Import\IImport;
use STM\Import\ImportHelper;
use STM\Import\Parser\ImportKey;
use STM\Import\Parser\ImportValueType;
use STM\Libs\FormProcessor;
use STM\Competition\Season\Season;
use STM\Match\Match;
use STM\Match\MatchValidator;
use STM\Match\Period\Period;
use STM\Match\Period\PeriodValidator;
use STM\Module\ModuleProcessing;
use STM\Web\Message\FormError;

final class ImportMatches extends ModuleProcessing implements IImport
{
    /** @var Competition|false */
    private $competition;
    /** @var array */
    private $competition_teams;

    public function __construct($object = false)
    {
        parent::__construct($object, '\STM\Competition\CompetitionEvent');
        ImportMatchesValidator::init($this->formProcessor);
        $this->loadSelectedCompetition();
    }

    public function getMaxStep()
    {
        return 3;
    }

    public function getValidKeys()
    {
        return array(
            new ImportKey('H', 'home_team', ImportValueType::STRING),
            new ImportKey('A', 'away_team', ImportValueType::STRING),
            new ImportKey('D', 'datetime', ImportValueType::STRING),
            new ImportKey('R', 'round', ImportValueType::STRING),
            new ImportKey('V', 'score_home', ImportValueType::ARRAYS),
            new ImportKey('O', 'score_away', ImportValueType::ARRAYS),
        );
    }

    public function step1()
    {
        return array('competitions' => $this->entities->Competition->findAll());
    }

    public function step2()
    {
        if (ImportMatchesValidator::checkFirstStep($this->competition)) {
            return array(
                'matches' => $this->getMatches(),
                'is_season' => $this->competition instanceof Season,
                'teams' => $this->competition_teams
            );
        }
        return false;
    }

    public function step3()
    {
        if (isset($_SESSION['import'])) {
            $info = $_SESSION['import'];
            $_SESSION['import'] = array();
            unset($_SESSION['import']);
            return array('import' => $info);
        } elseif (ImportMatchesValidator::checkSecondStep()) {
            $this->saveToDatabase();
            return true;
        }
        return false;
    }

    private function saveToDatabase()
    {
        $count_matches = 0;
        $count_created_matches = 0;
        $count_periods = 0;
        $count_created_periods = 0;
        $all_errors = array();
        $period_formProcessor = new FormProcessor();
        foreach ($_POST['match'] as $match) {
            if (array_key_exists('checked', $match)) {
                $count_matches++;
                MatchValidator::init($this->formProcessor);
                $id_match = Match::create($this->getMatchArrayForCreate($match));
                if ($id_match) {
                    $count_created_matches++;
                    if (isset($match['periods'])) {
                        $matchEntity = $this->entities->Match->findById($id_match);
                        foreach ($match['periods'] as $period) {
                            $count_periods++;
                            $period['match'] = $matchEntity;
                            $period['note'] = '';
                            PeriodValidator::init($period_formProcessor);
                            if (Period::create($period)) {
                                $count_created_periods++;
                            }
                            foreach ($period_formProcessor->getErrors() as $error) {
                                $this->formProcessor->addError($error);
                            }
                        }
                    }
                } else {
                    $all_errors[] = array(
                        'text' => $match['original'],
                        'errors' => $this->formProcessor->getErrors()
                    );
                }
            }
        }
        $_SESSION['import'] = array(
            'id_competition' => $this->competition->getId(),
            'all_matches' => $count_matches,
            'created_matches' => $count_created_matches,
            'all_periods' => $count_periods,
            'created_periods' => $count_created_periods,
            'errors' => $all_errors
        );
        parent::deleteCacheIfSuccessfulAction(true, $this->competition->getId());
        $log = "Matches: {$count_created_matches}/{$count_matches} ";
        $log .= "(Periods: {$count_created_periods}/{$count_periods}) in {$this->competition->__toString()} ";
        $log .= "({$this->competition->getId()})";
        parent::log('IMPORT_MATCHES', $log, true);
    }

    private function getMatches()
    {
        $matches = array();
        $period_formProcessor = new FormProcessor();
        $parser = ImportHelper::getParser($this, $_POST['format']);
        $lines = ImportHelper::parseStringToArray($_POST['input']);
        foreach ($lines as $line) {
            $add = false;
            $this->formProcessor->resetErrors();
            $parsed_array = $parser->parseString($line);
            $this->normalizeParsedString($parsed_array);
            if ((isset($parsed_array['match']['home_team']) && isset($parsed_array['match']['away_team']))) {
                $match = $this->getMatchArrayForCreate($parsed_array['match']);
                MatchValidator::init($this->formProcessor);
                MatchValidator::checkCreate($match);
                if (isset($parsed_array['periods'])) {
                    $match['periods'] = $parsed_array['periods'];
                    foreach ($match['periods'] as $period) {
                        PeriodValidator::init($period_formProcessor);
                        PeriodValidator::checkScores($period);
                        foreach ($period_formProcessor->getErrors() as $error) {
                            $this->formProcessor->addError($error); // because formProcessor is reset in init
                        }
                    }
                }
                $add = true;
            } elseif (!isset($_POST['remove_invalid'])) {
                $this->formProcessor->addError(FormError::get('import-reguired-competition'));
                $match = array(
                    'home_team' => '', 'away_team' => '', 'datetime' => '',
                    'round' => '', 'serie' => ''
                );
                $add = true;
            }
            if ($add) {
                $matches[] = array(
                    'original' => $line,
                    'match' => $match,
                    'errors' => $this->formProcessor->getErrors(),
                );
            }
        }
        return $matches;
    }

    private function getMatchArrayForCreate($input_array)
    {
        return array(
            'competition' => $this->competition,
            'datetime' => isset($input_array['datetime']) ? $input_array['datetime'] : '',
            'round' => isset($input_array['round']) ? $input_array['round'] : false,
            'serie' => isset($input_array['serie']) ? $input_array['serie'] : false,
            'home_team' => isset($this->competition_teams[$input_array['home_team']]) ?
                $this->competition_teams[$input_array['home_team']] : $input_array['home_team'],
            'away_team' => isset($this->competition_teams[$input_array['away_team']]) ?
                $this->competition_teams[$input_array['away_team']] : $input_array['away_team'],
        );
    }

    private function loadSelectedCompetition()
    {
        $this->competition = isset($_SESSION['id_competition']) ?
            $this->entities->Competition->findById($_SESSION['id_competition']) : false;
        $this->competition = isset($_POST['competition']) ?
            $this->entities->Competition->findById($_POST['competition']) : false;
        $this->competition_teams = array();
        if ($this->competition) {
            foreach ($this->competition->getTeams() as $team) {
                $this->competition_teams[$team->__toString()] = $team;
            }
        }
    }

    private function normalizeParsedString(&$result)
    {
        $new = array();
        // match values
        foreach ($result as $key => $value) {
            if (is_string($value)) {
                $new['match'][$key] = $value;
            }
        }
        // periods
        if (isset($result['score_home'])) {
            for ($i = 0; $i < count($result['score_home']); $i++) {
                if (isset($result['score_away'][$i])) {
                    $new['periods'][] = array(
                        'score_home' => $result['score_home'][$i],
                        'score_away' => $result['score_away'][$i],
                    );
                }
            }
        }
        $result = $new;
    }
}
