<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Import\Matches;

use STM\Helpers\ObjectValidator;
use STM\Web\Message\FormError;
use STM\Competition\Competition;
use STM\Competition\Season\Season;
use STM\Utils\Strings;

final class ImportMatchesValidator
{
    /** @var ObjectValidator */
    private static $validator;

    /** @param \STM\Libs\FormProcessor $formProcessor */
    public static function init($formProcessor = false)
    {
        if (is_null(self::$validator)) {
            self::$validator = new ObjectValidator('\STM\Match\Match');
        }
        self::$validator->setFormProcessor($formProcessor);
    }

    public static function checkFirstStep($competition)
    {
        // check competition
        self::$validator->getFormProcessor()->checkCondition(
            $competition instanceof Competition,
            'Invalid Competition'
        );
        self::$validator->getFormProcessor()->checkCondition(
            !($competition instanceof Playoff),
            FormError::get('import-playoff')
        );
        // check format
        $format = isset($_POST['format']) ? $_POST['format'] : false;
        self::$validator->getFormProcessor()->checkCondition(
            Strings::isStringNonEmpty($format),
            FormError::get('empty-value', array(stmLang('import', 'format')))
        );
        if (self::$validator->getFormProcessor()->isValid()) {
            $required_symbols = is_int(strpos($format, '{H}')) && is_int(strpos($format, '{A}'));
            self::$validator->getFormProcessor()->checkCondition(
                $required_symbols,
                FormError::get('import-reguired-competition')
            );
            if ($competition instanceof Season) {
                self::$validator->getFormProcessor()->checkCondition(
                    is_int(strpos($format, '{R}')),
                    FormError::get('import-reguired-season')
                );
            }
        }
        // check input
        self::$validator->getFormProcessor()->checkCondition(
            isset($_POST['input']) && Strings::isStringNonEmpty($_POST['input']),
            FormError::get('empty-value', array(stmLang('import', 'input')))
        );
        return self::$validator->getFormProcessor()->isValid();
    }

    public static function checkSecondStep()
    {
        $_POST['match'] = isset($_POST['match']) ? $_POST['match'] : array();
        $count_selected = 0;
        foreach ($_POST['match'] as $match) {
            if (array_key_exists('checked', $match)) {
                $count_selected++;
            }
        }
        self::$validator->getFormProcessor()->checkCondition(
            $count_selected > 0,
            FormError::get('nothing-checked')
        );
        return self::$validator->getFormProcessor()->isValid();
    }
}
