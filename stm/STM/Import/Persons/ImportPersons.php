<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Import\Persons;

use STM\Import\IImport;
use STM\Import\Parser\ImportKey;
use STM\Import\Parser\ImportValueType;
use STM\Import\ImportHelper;
use STM\Person\Person;
use STM\Person\PersonValidator;
use STM\Module\ModuleProcessing;
use STM\Web\Message\FormError;
use STM\Utils\Arrays;
use STM\Team\Member\TeamMember;
use STM\Team\Member\TeamMemberValidator;
use STM\Libs\FormProcessor;

final class ImportPersons extends ModuleProcessing implements IImport
{
    private $teams;
    private $positions;

    public function __construct($object = false)
    {
        parent::__construct($object, '\STM\Team\TeamEvent');
        ImportPersonsValidator::init($this->formProcessor);
        $this->teams = Arrays::getAssocArray($this->entities->Team->findAll(), '__toString');
        $this->positions = Arrays::getAssocArray($this->entities->PersonPosition->findAll(), '__toString');
    }

    public function getMaxStep()
    {
        return 3;
    }

    public function getValidKeys()
    {
        return array(
            new ImportKey('F', 'forename', ImportValueType::STRING),
            new ImportKey('S', 'surname', ImportValueType::STRING),
            new ImportKey('T', 'team', ImportValueType::STRING),
            new ImportKey('P', 'position', ImportValueType::STRING),
            new ImportKey('B', 'birth_date', ImportValueType::STRING),
        );
    }

    public function step1()
    {
        return array();
    }

    public function step2()
    {
        if (ImportPersonsValidator::checkFirstStep()) {
            return array(
                'persons' => $this->getPersons(),
                'teams' => $this->teams,
                'positions' => $this->positions
            );
        }
        return false;
    }

    public function step3()
    {
        if (isset($_SESSION['import'])) {
            $info = $_SESSION['import'];
            $_SESSION['import'] = array();
            unset($_SESSION['import']);
            return array('import' => $info);
        } elseif (ImportPersonsValidator::checkSecondStep()) {
            $this->saveToDatabase();
            return true;
        }
        return false;
    }

    private function saveToDatabase()
    {
        $countPersons = 0;
        $countCreatedPersons = 0;
        $countTeamMembers = 0;
        $countCreatedTeamMembers = 0;
        $teamFormProcessor = new FormProcessor();
        $all_errors = array();
        foreach ($_POST['person'] as $person) {
            if (array_key_exists('checked', $person)) {
                $countPersons++;
                PersonValidator::init($this->formProcessor);
                $input = $this->getPersonArrayForCreate($person);
                $personArray = $input['person'];
                $idCreatedPerson = Person::create($personArray);
                if ($idCreatedPerson) {
                    $countCreatedPersons++;
                    $teamArray = $input['team'];
                    if ($teamArray['team'] != '' || $teamArray['position'] != '') {
                        $countTeamMembers++;
                        if ($teamArray['team'] == '') {
                            $this->formProcessor->addError(FormError::get('import-persons-team'));
                        } elseif ($teamArray['position'] == '') {
                            $this->formProcessor->addError(FormError::get('import-persons-position'));
                        } else {
                            // create TeamMember
                            $team = array(
                                'team' => $teamArray['team'],
                                'person' => $this->entities->Person->findById($idCreatedPerson),
                                'position' => $teamArray['position'],
                                'date_since' => '',
                                'date_to' => '',
                                'is_player' => true,
                                'dress_number' => ''
                            );
                            TeamMemberValidator::init($teamFormProcessor);
                            if (TeamMember::create($team)) {
                                $countCreatedTeamMembers++;
                            } else {
                                foreach ($teamFormProcessor->getErrors() as $error) {
                                    $this->formProcessor->addError($error);
                                }
                            }
                        }
                        // errors in TeamMember
                        if (!$this->formProcessor->isValid()) {
                            $all_errors[] = array(
                                'text' => $person['original'],
                                'errors' => $this->formProcessor->getErrors()
                            );
                        }
                    }
                } else {
                    $all_errors[] = array(
                        'text' => $person['original'],
                        'errors' => $this->formProcessor->getErrors()
                    );
                }
            }
        }
        $_SESSION['import'] = array(
            'all_persons' => $countPersons,
            'created_persons' => $countCreatedPersons,
            'all_team_members' => $countTeamMembers,
            'created_team_members' => $countCreatedTeamMembers,
            'errors' => $all_errors
        );
        $logMessage = "Persons: {$countCreatedPersons}/{$countPersons}, ";
        $logMessage .= "Team Members: {$countCreatedTeamMembers}/{$countTeamMembers}";
        parent::log('IMPORT_PERSONS', $logMessage, $countCreatedPersons > 0);
    }

    private function getPersons()
    {
        $persons = array();
        $parser = ImportHelper::getParser($this, $_POST['format']);
        $lines = ImportHelper::parseStringToArray($_POST['input']);
        foreach ($lines as $line) {
            $add = false;
            $this->formProcessor->resetErrors();
            $parsed_array = $parser->parseString($line);
            if ((isset($parsed_array['forename']) && isset($parsed_array['surname']))) {
                $person = $this->getPersonArrayForCreate($parsed_array);
                PersonValidator::init($this->formProcessor);
                PersonValidator::checkCreate($person['person']);
                $add = true;
                // new TeamMember (team, position) is not checked in Validator because it requires created person
                if ((isset($parsed_array['team']) && isset($parsed_array['position']))) {
                    $team = $person['team'];
                    if ($team['team'] == '') {
                        $this->formProcessor->addError(FormError::get('import-persons-team'));
                    }
                    if ($team['position'] == '') {
                        $this->formProcessor->addError(FormError::get('import-persons-position'));
                    }
                }
            } elseif (!isset($_POST['remove_invalid'])) {
                $this->formProcessor->addError(FormError::get('import-reguired-person'));
                $person = $this->getPersonArrayForCreate(array());
                $add = true;
            }
            if ($add) {
                $persons[] = array(
                    'original' => $line,
                    'person' => $person,
                    'errors' => $this->formProcessor->getErrors(),
                );
            }
        }
        return $persons;
    }

    private function getPersonArrayForCreate($inputArray)
    {
        return array(
            'person' => array(
                'forename' => isset($inputArray['forename']) ? $inputArray['forename'] : '',
                'surname' => isset($inputArray['surname']) ? $inputArray['surname'] : '',
                'birth_date' => isset($inputArray['birth_date']) ? $inputArray['birth_date'] : '',
                'characteristic' => '',
                'id_country' => ''
            ),
            'team' => array(
                'team' => isset($inputArray['team']) && isset($this->teams[$inputArray['team']]) ?
                    $this->teams[$inputArray['team']] : '',
                'position' => isset($inputArray['position']) && isset($this->positions[$inputArray['position']]) ?
                    $this->positions[$inputArray['position']] : '',
            )
        );
    }
}
