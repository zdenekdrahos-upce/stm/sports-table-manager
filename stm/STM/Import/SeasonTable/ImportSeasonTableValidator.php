<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Import\SeasonTable;

use STM\Helpers\ObjectValidator;
use STM\Web\Message\FormError;
use STM\Competition\Season\Season;
use STM\Utils\Arrays;

final class ImportSeasonTableValidator
{
    /** @var ObjectValidator */
    private static $validator;

    /** @param \STM\Libs\FormProcessor $formProcessor */
    public static function init($formProcessor = false)
    {
        if (is_null(self::$validator)) {
            self::$validator = new ObjectValidator('\STM\Competition\Season\Season');
        }
        self::$validator->setFormProcessor($formProcessor);
    }

    public static function checkFirstStep($season)
    {
        self::$validator->getFormProcessor()->checkCondition($season instanceof Season, 'Invalid Competition');
        return self::$validator->getFormProcessor()->isValid();
    }

    public static function checkSecondStep($season)
    {
        self::$validator->getFormProcessor()->checkCondition($season instanceof Season, 'Invalid Competition');
        if (self::$validator->getFormProcessor()->isValid()) {
            $teams = Arrays::getAssocArray($season->getTeams(), 'getId', 'getId');
            self::checkTable('full', $teams);
            self::checkTable('home', $teams);
            self::checkTable('away', $teams);
        }
        return self::$validator->getFormProcessor()->isValid();
    }

    private static function checkTable($name, $season_teams)
    {
        if (isset($_POST[$name])) {
            $table_name = stmLang('competition', 'table', $name);
            $valid_teams = array();
            $valid_numbers = array();
            $valid_structures = array();
            foreach ($_POST[$name] as $id_team => $rows) {
                $valid_structures[] = Arrays::isArrayValid(
                    $rows,
                    array('win', 'winInOt', 'draw', 'lossInOt', 'loss', 'goalFor', 'goalAgainst')
                );
                if (self::$validator->getFormProcessor()->isValid()) {
                    $valid_teams[] = in_array($id_team, $season_teams);
                    foreach ($rows as $row) {
                        $valid_numbers[] = self::$validator->checkNumber($row, array('min' => 0, 'max' => 10000));
                    }
                }
            }
            self::$validator->getFormProcessor()->resetErrors();
            self::$validator->getFormProcessor()->checkCondition(
                !in_array(false, $valid_structures),
                FormError::get('import-seasontable-structure', array($table_name))
            );
            self::$validator->getFormProcessor()->checkCondition(
                !in_array(false, $valid_teams),
                FormError::get('import-seasontable-teams', array($table_name))
            );
            self::$validator->getFormProcessor()->checkCondition(
                !in_array(false, $valid_numbers),
                FormError::get('import-seasontable-numbers', array($table_name))
            );
        }
    }
}
