<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Import\SeasonTable;

use STM\Import\IImport;
use STM\Competition\CompetitionType;
use STM\Match\Table\ResultTable;
use STM\Match\Table\TablePoints;
use STM\Match\Table\TableRow;
use STM\Match\Table\TableRowHelper;
use STM\Module\ModuleProcessing;
use STM\StmCache;

final class ImportSeasonTable extends ModuleProcessing implements IImport
{
    /** @var \STM\Competition\Season\Season|false */
    private $season;
    /** @var TablePoints */
    private $table_points;
    /** @var array */
    private $teams_with_extra_points;

    public function __construct($object = false)
    {
        parent::__construct($object, '\STM\Competition\CompetitionEvent');
        ImportSeasonTableValidator::init($this->formProcessor);
        $this->loadSelectedCompetition();
    }

    public function getMaxStep()
    {
        return 3;
    }

    public function getValidKeys()
    {
        return array();
    }

    public function step1()
    {
        return array(
            'competitions' => $this->entities->Competition->findByType(CompetitionType::SEASON)
        );
    }

    public function step2()
    {
        if (ImportSeasonTableValidator::checkFirstStep($this->season)) {
            $result_table = StmCache::getSeasonTable($this->season);
            $tables = array('full' => $result_table->getTableFull());
            if (isset($_POST['all_tables'])) {
                $tables['home'] = $result_table->getTableHome();
                $tables['away'] = $result_table->getTableAway();
            }
            return array(
                'season' => $this->season,
                'tables' => $tables,
                'displayWinInOT' => $this->season->getPointWinOvertime() != $this->season->getPointWin(),
                'displayLossInOT' => $this->season->getPointLossOvertime() != $this->season->getPointLoss(),
            );
        }
        return false;
    }

    public function step3()
    {
        if (isset($_SESSION['import'])) {
            $info = $_SESSION['import'];
            $_SESSION['import'] = array();
            unset($_SESSION['import']);
            return array('import' => $info);
        } elseif (ImportSeasonTableValidator::checkSecondStep($this->season)) {
            $this->save();
            return true;
        }
        return false;
    }

    private function save()
    {
        $table = ResultTable::import($this->season, $this->getTables());
        $save = file_put_contents(STM_CACHE_SEASON_TABLES . "{$this->season->getId()}.txt", serialize($table));
        $_SESSION['import'] = array(
            'season_name' => $this->season->__toString(),
            'id_competition' => $this->season->getId(),
        );
        parent::log('IMPORT_SEASON_TABLES', "{$this->season->__toString()} ({$this->season->getId()})", $save);
    }

    private function getTables()
    {
        $full_table = $this->getTable($_POST['full']);
        return array(
            'full' => $full_table,
            'home' => isset($_POST['home']) ? $this->getTable($_POST['home']) : $full_table,
            'away' => isset($_POST['away']) ? $this->getTable($_POST['away']) : $full_table
        );
    }

    private function getTable($rows)
    {
        $table = array();
        foreach ($rows as $id_team => $row) {
            $table[] = $this->getTableRow($id_team, $row);
        }
        usort($table, array('\STM\Match\Table\TableRowHelper', 'compareByPoints'));
        return $table;
    }

    private function getTableRow($id_team, $row)
    {
        $tr = new TableRow();
        $tr->idTeam = (int) $id_team;
        $tr->name = $this->teams_with_extra_points[$id_team]['name'];
        $tr->win = (int) $row['win'];
        $tr->winInOt = (int) $row['winInOt'];
        $tr->draw = (int) $row['draw'];
        $tr->lossInOt = (int) $row['lossInOt'];
        $tr->loss = (int) $row['loss'];
        $tr->goalFor = (int) $row['goalFor'];
        $tr->goalAgainst = (int) $row['goalAgainst'];
        $tr->extraPoints = (int) $this->teams_with_extra_points[$id_team]['extra'];
        $tr->updateMatchCount();
        return TableRowHelper::calculatePointsInRow($tr, $this->table_points);
    }

    private function loadSelectedCompetition()
    {
        $this->season = isset($_SESSION['id_competition']) ?
            $this->entities->Competition->findById($_SESSION['id_competition']) : false;
        $this->season = isset($_POST['competition']) ?
            $this->entities->Competition->findById($_POST['competition']) : false;
        $this->teams_with_extra_points = array();
        if ($this->season) {
            $this->table_points = TablePoints::getSeasonPoints($this->season);
            $this->teams_with_extra_points = $this->season->getExtraPointsForAllTeams();
        }
    }
}
