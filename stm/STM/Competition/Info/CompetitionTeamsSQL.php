<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition\Info;

use STM\Team\Team;
use STM\Competition\Competition;
use STM\DB\Database;
use STM\DB\SQL\SelectQuery;
use STM\DB\SQL\UpdateQuery;
use STM\DB\SQL\DeleteQuery;
use STM\DB\SQL\InsertQuery;
use STM\DB\SQL\BulkInsertQuery;
use STM\DB\SQL\Condition;
use STM\DB\SQL\JoinTable;
use STM\DB\SQL\JoinType;

class CompetitionTeamsSQL
{
    /** @return InsertQuery */
    public static function addTeam(Competition $competition, Team $team)
    {
        $query = new InsertQuery(DT_COMPETITIONS_TEAMS);
        $query->addAttributes(
            array(
                'id_competition' => $competition->getId(),
                'id_team' => $team->getId(),
            )
        );
        return $query;
    }

    public static function insertTeamsFromCompetition(
        Competition $currentCompetition,
        Competition $competition
    ) {
        $select = self::selectTeamFromOtherCompetition($currentCompetition, $competition);
        $insert = new BulkInsertQuery(DT_COMPETITIONS_TEAMS);
        $insert->addColumns(array('id_competition', 'id_team', 'your_team'));
        $insert->setSelect($select);
        return $insert;
    }

    /** @return DeleteQuery */
    public static function deleteTeam(Competition $competition, Team $team)
    {
        $delete = new DeleteQuery(DT_COMPETITIONS_TEAMS);
        $delete->setWhere(self::getTeamCompetitionCondition($competition, $team));
        return $delete;
    }

    /** @var SelectQuery */
    private static function selectTeamFromOtherCompetition(
        Competition $currentCompetition,
        Competition $competition
    ) {
        $current_teams = self::selectTeamsFromTeamsCompetitions($currentCompetition->getId());
        $new_teams = self::selectTeamsFromTeamsCompetitions($competition->getId());

        $query = new SelectQuery("({$current_teams->generate()}) current_teams");
        $query->setColumns(
            "'{$currentCompetition->getId()}' as id_competition, new_teams.id_team, new_teams.your_team"
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::RIGHT,
                "({$new_teams->generate()}) new_teams",
                'current_teams.id_team = new_teams.id_team'
            )
        );
        $query->setWhere(new Condition('current_teams.id_team is null'));
        Database::escapeQuery($query);
        return $query;
    }

    /** @return SelectQuery */
    private static function selectTeamsFromTeamsCompetitions($id_competition)
    {
        $query = new SelectQuery(DT_COMPETITIONS_TEAMS);
        $query->setColumns('id_team, your_team');
        $query->setWhere(new Condition('id_competition = {C}', array('C' => $id_competition)));
        Database::escapeQuery($query);
        return $query;
    }

    /** @return SelectQuery */
    public static function selectYourTeams(Competition $competition)
    {
        $query = new SelectQuery(DT_COMPETITIONS_TEAMS);
        $query->setColumns('ID_TEAM');
        $query->setWhere(
            new Condition(
                'your_team = {T} AND id_competition = {C}',
                array('T' => 'Y', 'C' => $competition->getId())
            )
        );
        return $query;
    }

    /** @return UpdateQuery */
    public static function updateYourTeam(Competition $competition, Team $team, $isYourTeam)
    {
        $value = $isYourTeam ? 'Y' : 'N';
        $query = new UpdateQuery(DT_COMPETITIONS_TEAMS);
        $query->addAttributes(array('your_team' => $value));
        $query->setWhere(self::getTeamCompetitionCondition($competition, $team));
        return $query;
    }

    private static function getTeamCompetitionCondition($competition, $team)
    {
        return new Condition(
            'id_competition = {C} and id_team = {T}',
            array('C' => $competition->getId(), 'T' => $team->getId())
        );
    }
}
