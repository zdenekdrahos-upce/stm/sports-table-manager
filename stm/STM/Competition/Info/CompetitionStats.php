<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition\Info;

use STM\DB\Object\DatabaseObject;
use STM\Competition\Competition;
use STM\Match\MatchSelection;
use STM\Utils\Arrays;
use STM\Utils\Classes;

class CompetitionStats
{
    /** @var DatabaseObject */
    private static $db;

    /** @var STM\Competition\Competition */
    private $competition;
    /** @var array(all_matches, played_matches, upcoming_matches) */
    private $matchCounts;
    /** @var array(team => players count) */
    private $teamPlayersCount;

    public static function setORM(DatabaseObject $dbObject)
    {
        if (is_null(self::$db)) {
            self::$db = $dbObject;
        }
    }

    public function __construct(Competition $competition)
    {
        $this->competition = $competition;
    }

    public function getCountMatches($matchType = MatchSelection::ALL_MATCHES)
    {
        if (is_null($this->matchCounts)) {
            $this->findMatchCounts();
        }
        $isClassConstant = Classes::isClassConstant('\STM\Match\MatchSelection', $matchType);
        $matchType = $isClassConstant ? $matchType : MatchSelection::ALL_MATCHES;
        return $this->matchCounts[$matchType];
    }

    /** Loads number of all/played/upcoming matches for this competition */
    private function findMatchCounts()
    {
        $query = CompetitionStatsSQL::selectCountCompetitionMatches($this->competition);
        $this->matchCounts = self::$db->select($query, 'array', true);
    }

    public function getCountsPlayers()
    {
        if (is_null($this->teamPlayersCount)) {
            $this->findPlayersCount();
        }
        return $this->teamPlayersCount;

    }

    private function findPlayersCount()
    {
        $query = CompetitionStatsSQL::selectCountsOfTeamsPlayersCompetitions($this->competition);
        $result = self::$db->select($query, 'array', false);
        $this->teamPlayersCount = Arrays::getAssocArray($result, 'team', 'players_count');
    }
}
