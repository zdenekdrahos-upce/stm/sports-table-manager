<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition\Info;

use STM\DB\Object\DatabaseObject;
use STM\Competition\Competition;
use STM\Competition\CompetitionValidator;
use STM\Team\Team;
use STM\StmFactory;
use STM\Utils\Arrays;
use STM\Match\Match;
use STM\Match\MatchSelection;

class CompetitionTeams
{
    /** @var DatabaseObject */
    private static $db;

    /** @var STM\Competition\Competition */
    private $competition;
    /** @var array(id of your_teams) */
    private $yourTeams;
    /** @var array(Team) */
    protected $teams;

    public static function setORM(DatabaseObject $dbObject)
    {
        if (is_null(self::$db)) {
            self::$db = $dbObject;
        }
    }

    public function __construct(Competition $competition)
    {
        $this->competition = $competition;
    }

    public function addTeam($team)
    {
        CompetitionValidator::setCompetition($this->competition);
        if (CompetitionValidator::checkAddedTeam($team)) {
            return self::$db->query(CompetitionTeamsSQL::addTeam($this->competition, $team));
        }
        return false;
    }

    public function addTeamsFromCompetition($competition)
    {
        CompetitionValidator::setCompetition($this->competition);
        if (CompetitionValidator::checkCompetitionForAddTeams($competition)) {
            $insert = CompetitionTeamsSQL::insertTeamsFromCompetition($this->competition, $competition);
            return self::$db->query($insert);
        }
        return false;
    }

    public function deleteTeam(Team $team)
    {
        if ($this->isTeamFromCompetition($team)) {
            if (STM_DB_PROCEDURES === true) {
                return self::$db->executeProcedure(
                    'delete_team_from_competition',
                    array($this->competition->getId(), $team->getId())
                );
            } else {
                return $this->deleteTeamWithoutProcedure($team);
            }
        }
        return false;
    }

    private function deleteTeamWithoutProcedure($team)
    {
        // delete matches
        $ms = new MatchSelection();
        $ms->setMatchType(MatchSelection::ALL_MATCHES);
        $ms->setLoadingPeriods(false);
        $ms->setLoadingScores(false);
        $ms->setCompetition($this->competition);
        $ms->setSelectedTeams(array($team));
        Match::deleteMatches($ms);
        // delete team from COMPETITIONS_TEAMS
        return self::$db->query(CompetitionTeamsSQL::deleteTeam($this->competition, $team));
    }

    public function addYourTeam(Team $team)
    {
        if (!$this->isYourTeam($team->getId()) && $this->isTeamFromCompetition($team)) {
            return self::$db->query(CompetitionTeamsSQL::updateYourTeam($this->competition, $team, true));
        }
        return false;
    }

    public function deleteYourTeam(Team $team)
    {
        if ($this->isYourTeam($team->getId())) {
            return self::$db->query(CompetitionTeamsSQL::updateYourTeam($this->competition, $team, false));
        }
        return false;
    }

    public function isTeamFromCompetition($team)
    {
        if ($team instanceof Team) {
            $this->checkTeams();
            foreach ($this->teams as $competitionTeam) {
                if ($team->getId() == $competitionTeam->getId()) {
                    return true;
                }
            }
        }
        return false;
    }

    public function getTeams()
    {
        $this->checkTeams();
        return array_values($this->teams);
    }

    public function isYourTeam($idTeam)
    {
        $this->checkTeams();
        return is_numeric($idTeam) ? in_array($idTeam, $this->yourTeams) : false;
    }

    public function getIdOfYourTeams()
    {
        $this->checkTeams();
        return $this->yourTeams;
    }

    public function getYourTeams()
    {
        $this->checkTeams();
        $yourTeams = array();
        foreach ($this->yourTeams as $teamId) {
            $yourTeams[] = $this->teams[$teamId];
        }
        return $yourTeams;
    }

    public function getCountTeams()
    {
        $this->checkTeams();
        return count($this->teams);
    }

    private function checkTeams()
    {
        if (is_null($this->teams)) {
            $this->loadTeams();
        }
    }

    /** Loads teams from competition */
    private function loadTeams()
    {
        $this->findYourTeams();
        $teams = StmFactory::find()->Team->findByCompetition($this->competition);
        $this->teams = Arrays::getAssocArray($teams, 'getId');
    }

    /** Loads IDs of teams marked as 'your_team' from this competition */
    private function findYourTeams()
    {
        $yourTeams = self::$db->select(CompetitionTeamsSQL::selectYourTeams($this->competition), 'array');
        $this->yourTeams = array();
        foreach ($yourTeams as $team_array) {
            $this->yourTeams[] = $team_array['ID_TEAM'];
        }
        return $this->yourTeams;
    }
}
