<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition\Info;

use STM\Competition\Competition;
use STM\Team\Member\TeamMemberSelection;
use STM\DB\Database;
use STM\DB\SQL\SelectQuery;
use STM\DB\SQL\Condition;
use STM\DB\SQL\JoinTable;
use STM\DB\SQL\JoinType;

class CompetitionStatsSQL
{

    /**
     * Select number of all, played and upcoming matches. Optionally select can be l
     * imited by where condition (only for table matches)
     * @param Condition $where if $where is not valid instance of Condition then no where condition is used
     * @return SelectQuery
     */
    public static function selectCountCompetitionMatches(Competition $competition)
    {
        $query = new SelectQuery(DT_MATCHES . ' matches');
        $query->setColumns(Database::getNullFunction() . '(count(*), 0) as ALL_MATCHES');
        $query->setColumns(
            Database::getNullFunction() .
            '(sum(case when match_periods.id_match is not null THEN 1 ELSE 0 END), 0) as PLAYED_MATCHES'
        );
        $query->setColumns(
            Database::getNullFunction() .
            '(sum(case when match_periods.id_match is null THEN 1 ELSE 0 END), 0) as UPCOMING_MATCHES'
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::LEFT,
                '(select distinct id_match from ' . DT_MATCH_PERIODS . ')match_periods',
                'matches.id_match = match_periods.id_match'
            )
        );
        $where = new Condition('id_competition = ' . $competition->getId());
        $query->setWhere($where);
        return $query;
    }

    /** @return SelectQuery */
    public static function selectCountsOfTeamsPlayersCompetitions(Competition $competition)
    {
        // load players counts
        $subquery = new SelectQuery(DT_COMPETITIONS_TEAMS . ' competitions_teams');
        $subquery->setColumns(
            'competitions_teams.id_team, count(players.id_team_player) as players_count'
        );
        $subquery->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                DT_TEAMS_PERSONS . ' teams_members',
                'teams_members.id_team = competitions_teams.id_team'
            )
        );
        $subquery->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                DT_TEAMS_PLAYERS . ' players',
                'teams_members.id_team_player = players.id_team_player'
            )
        );
        // condition - competition_id and competitions dates must be between person dates in team
        $player_selection = new TeamMemberSelection();
        $player_selection->setCompetition($competition);
        $subquery->setWhere($player_selection->getWhereCondition());
        $subquery->setGroupBy('competitions_teams.id_team');
        // load team names and add players count
        $query = new SelectQuery(DT_COMPETITIONS_TEAMS . ' competitions_teams');
        $query->setColumns(
            'teams.name as team, ' . Database::getNullFunction() . '(counts.players_count, 0) as players_count'
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                DT_TEAMS . ' teams',
                'competitions_teams.id_team = teams.id_team'
            )
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::LEFT,
                "({$subquery->generate()}) counts",
                'teams.id_team = counts.id_team'
            )
        );
        $query->setWhere(
            new Condition(
                'competitions_teams.id_competition = {C}',
                array('C' => $competition->getId())
            )
        );
        return $query;
    }
}
