<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition;

use STM\DB\SQL\SelectQuery;
use STM\DB\SQL\JoinTable;
use STM\DB\SQL\JoinType;

class CompetitionSQL
{

    /**
     * Selects all competition, it also selects name of category from table competition_categories
     * @param Condition $where if $where is not valid instance of Condition then no where condition is used
     * @return SelectQuery
     */
    public static function selectCompetitions()
    {
        $query = new SelectQuery(DT_COMPETITIONS . ' competitions');
        $query->setColumns(
            'competitions.id_competition, competitions.name, competitions.match_periods,
            competitions.date_start, competitions.date_end,
            competitions.id_competition_type, competitions.id_score_type, competitions.id_status,
            competitions.id_category, categories.name as category_name'
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::LEFT,
                DT_COMPETITION_CATEGORIES . ' categories',
                'competitions.id_category = categories.id_category'
            )
        );
        $query->setOrderBy('categories.name, competitions.id_competition_type desc, competitions.name');
        return $query;
    }

    /** @return \STM\DB\SQL\Condition */
    public static function getJoinCompetitionsTeamsTable()
    {
        return new JoinTable(
            JoinType::JOIN,
            DT_COMPETITIONS_TEAMS . ' teams',
            'teams.id_competition = competitions.id_competition'
        );
    }

    /** @return \STM\DB\SQL\Condition */
    public static function getJoinCompetitionsUsersTable()
    {
        return new JoinTable(
            JoinType::JOIN,
            DT_COMPETITIONS_USERS . ' users',
            'users.id_competition = competitions.id_competition'
        );
    }
}
