<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition;

use STM\DB\Object\DatabaseObject;
use STM\Match\Match;
use STM\Match\ScoreType;
use STM\Match\MatchSelection;
use STM\StmDatetime;
use STM\Team\Team;
use STM\Competition\CompetitionStatus;
use STM\Utils\Dates;
use STM\Competition\Info\CompetitionTeams;
use STM\Competition\Info\CompetitionStats;

/**
 * Competition class
 * - only competition without advanced gaming system (season...)
 * - instance: represents row from database table 'COMPETITIONS'
 * - static methods: for finding competition, creating new one
 */
class Competition
{
    /** @var DatabaseObject */
    private static $db;

    // table fields
    /** @var int */
    protected $id_competition;
    /** @var string */
    protected $name;
    /** @var int */
    protected $match_periods;
    /** @var mysql datetime */
    protected $date_start;
    /** @var mysql datetime */
    protected $date_end;
    /** @var char */
    protected $id_competition_type;
    /** @var char */
    protected $id_score_type;
    /** @var int/null */
    protected $id_category;
    /** @var char */
    protected $id_status;
    /** @var string */
    protected $category_name;

    /** @var CompetitionTeams */
    private $teams;
    /** @var CompetitionStats */
    private $stats;

    protected function __construct()
    {
        $this->teams = new CompetitionTeams($this);
        $this->stats = new CompetitionStats($this);
    }

    public static function setORM(DatabaseObject $dbObject)
    {
        if (is_null(self::$db)) {
            self::$db = $dbObject;
            CompetitionTeams::setORM($dbObject);
            CompetitionStats::setORM($dbObject);
        }
    }

    /**
     * Checks if name exists as competition in database. Method is case-INsensitive.
     * It doesn't distinguish wheter competition is e.g. basic competition or season
     * @param string $name
     * @return boolean
     */
    public static function exists($name)
    {
        return self::$db->existsItem('name', $name);
    }

    /**
     * Creates new row in in database table 'competitions'
     * @param array $attributes
     * @return int/boolean
     * Returns id of created competition if creating was successful, otherwise false
     */
    public static function create($attributes)
    {
        if (CompetitionValidator::checkCreate($attributes)) {
            if (self::$db->insertValues(self::processAttributes($attributes), true)) {
                return self::$db->getLastInsertId();
            }
        }
        return false;
    }

    /** Processes array before creating - convert dates to mysql datetime format */
    private static function processAttributes($attributes)
    {
        $attributes['date_start'] = Dates::stringToDatabaseDate($attributes['date_start']);
        $attributes['date_end'] = Dates::stringToDatabaseDate($attributes['date_end']);
        return $attributes;
    }

    /** Used if advanced competition (season or playoff) is not successfully created */
    protected static function deleteCompetition($idCompetition)
    {
        return self::$db->deleteById($idCompetition);
    }

    // INSTANCE METHODS FOR ONE COMPETITION
    public function update($attributes)
    {
        CompetitionValidator::setCompetition($this);
        if (CompetitionValidator::checkUpdate($attributes)) {
            return self::$db->updateById($this->id_competition, self::processAttributes($attributes));
        }
        return false;
    }

    /**
     * Deletes complete competition + competitions's matches and teams
     * @return boolean
     */
    public function delete()
    {
        if ($this->getCountMatches() == 0) {
            return self::deleteCompetition($this->id_competition);
        }
        return false;
    }

    /**
     * Delete matches which belong to this competition
     * @return boolean
     */
    public function deleteMatches()
    {
        if ($this->getCountMatches() > 0) {
            $ms = new MatchSelection();
            $ms->setCompetition($this);
            return Match::deleteMatches($ms);
        }
        return false;
    }

    // TEAMS IN COMPETITION - work with DB table 'TEAMS_COMPETITIONS'

    /**
     * Adds $team to competition if teams is not already in competition
     * @param Team $team
     * @return boolean
     */
    public function addTeam($team)
    {
        return $this->teams->addTeam($team);
    }

    /**
     * Adds teams from $competition that aren't already in competition
     * @param Competition $competition
     * @return boolean
     */
    public function addTeamsFromCompetition($competition)
    {
        return $this->teams->addTeamsFromCompetition($competition);
    }

    /**
     * Deletes team from teams_competitions table + deletes all team matches in this competition
     * @param Team $team
     * @return boolean
     * Returns true if team and team matches were deleted from competition
     */
    public function deleteTeam(Team $team)
    {
        return $this->teams->deleteTeam($team);
    }

    /**
     * Mark $team as normal team if $team is set as 'your_team'
     * @param Team $team
     * @return boolean
     */
    public function deleteYourTeam(Team $team)
    {
        return $this->teams->deleteYourTeam($team);
    }

    /**
     * Mark $team as 'your_team' if $team is not already 'your_team'
     * @param Team $team
     * @return boolean
     */
    public function addYourTeam(Team $team)
    {
        return $this->teams->addYourTeam($team);
    }

    /**
     * @param numeric $idTeam
     * @return boolean
     * Returns true if $team is highlighted as 'your_team' in DB table teams_competitions
     */
    public function isYourTeam($idTeam)
    {
        return $this->teams->isYourTeam($idTeam);
    }

    /** @return array */
    public function getIdOfYourTeams()
    {
        return $this->teams->getIdOfYourTeams();
    }

    // OTHER METHODS
    /**
     * Checks whether $team belongs to this competition
     * @param Team $team
     * @return boolean
     */
    public function isTeamFromCompetition($team)
    {
        return $this->teams->isTeamFromCompetition($team);
    }

    /**
     * Checks if $date is between start/end dates of the competition
     * @param string $date
     * @return boolean
     */
    public function isValidDate($date)
    {
        return Dates::isDateInRange($date, $this->getDateStart(), $this->getEndStart());
    }


    // GETTERS
    /** @return array(Team) teams playing the competition (empty array if no teams in competition) */
    public function getTeams()
    {
        return $this->teams->getTeams();
    }

    /** @return array(Team) your_team(s) playing this competition */
    public function getYourTeams()
    {
        return $this->teams->getYourTeams();
    }

    /** @return int number of teams playing this competition */
    public function getCountTeams()
    {
        return $this->teams->getCountTeams();
    }

    /**
     * @param string $matchType - constant from MatchSelection
     * @return int
     * Returns number of All, Played or Upcoming matches
     */
    public function getCountMatches($matchType = MatchSelection::ALL_MATCHES)
    {
        return $this->stats->getCountMatches($matchType);
    }

    /**
     * @return array
     * Returns number of players in each team from competition
     */
    public function getCountsPlayers()
    {
        return $this->stats->getCountsPlayers();
    }

    /** @return int */
    public function getId()
    {
        return (int) $this->id_competition;
    }

    /** @return char */
    public function getIdCompetitionType()
    {
        return $this->id_competition_type;
    }

    /** @return char */
    public function getStatusId()
    {
        return $this->id_status;
    }

    /** @return char - F, T */
    public function getScoreType()
    {
        return $this->id_score_type;
    }

    /** @return int */
    public function getNumberOfMatchPeriods()
    {
        return (int) $this->match_periods;
    }

    /** @return int/false */
    public function getIdCategory()
    {
        return !is_null($this->id_category) ? (int) $this->id_category : false;
    }

    /** @return string */
    public function getNameCategory()
    {
        return $this->category_name ? $this->category_name : '';
    }

    /** @return StmDatetime */
    public function getDateStart()
    {
        return new StmDatetime($this->date_start);
    }

    /** @return StmDatetime */
    public function getEndStart()
    {
        return new StmDatetime($this->date_end);
    }

    /**
     * Method for templating. Returns competition as an array
     * @return array
     */
    public function toArray()
    {
        return array(
            'name' => $this->name,
            'match_periods' => $this->match_periods,
            'date_start' => $this->getDateStart(),
            'date_end' => $this->getEndStart(),
            'type' => CompetitionType::getCompetitionType($this->id_competition_type),
            'score_type' => ScoreType::getName($this->id_score_type),
            'status' => CompetitionStatus::getStatus($this->id_status),
            'id_category' => $this->id_category,
            'category' => $this->getNameCategory(),
        );
    }

    /** @return string name of the competition */
    public function __toString()
    {
        return $this->name;
    }
}
