<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition\Summary;

use STM\Competition\Competition;
use STM\Competition\Season\Season;
use STM\Competition\Playoff\Playoff;
use STM\StmCache;
use STM\Match\MatchSelection;
use STM\Team\Member\TeamMember;
use \STM\Match\Table\Cross\CrossTableFactory;
use STM\Match\Table\ResultTable;
use STM\Match\Table\TablePoints;
use STM\StmFactory;

final class CompetitionSummaryFactory
{
    /** @var Competition */
    private $competition;
    /** @var array item = Match */
    private $matches;
    /** @var \STM\Match\Stats\Action\MatchPlayerStats */
    private $stats;
    /** @var array */
    private $filter;
    /** @var CompetitionSummary */
    private $summary;

    public function getCompetitionSummary(Competition $competition, $filter_data)
    {
        $this->loadFilterData($filter_data);
        $this->loadCompetition($competition);
        $this->initSummary();
        $this->loadSchedule();
        $this->loadTeams();
        $this->loadCompetitionStatistics();
        $this->loadPlayerStatistics();
        $this->loadTables();
        $this->loadMatches();
        $this->loadWinners();
        $this->disablUncheckedAttributesNeededForLoading();
        return $this->summary;
    }

    private function loadFilterData($filter_data)
    {
        $this->filter = is_array($filter_data) ? $filter_data : array();
    }

    private function loadCompetition($competition)
    {
        $this->competition = $competition;
        $this->matches = StmCache::getAllCompetitionMatches($this->competition);
        $this->stats = $stats = StmCache::getCompetitionPlayerStatistics($this->competition);
    }

    private function initSummary()
    {
        $this->summary = new CompetitionSummary();
        $this->summary->competition = array(
            'name' => $this->competition->__toString(),
            'dateStart' => $this->competition->getDateStart(),
            'dateEnd' => $this->competition->getEndStart(),
        );
    }

    private function loadSchedule()
    {
        if ($this->isAttributeUnchecked('schedule')) {
            return $this->disableAttribute('schedule');
        }
        $this->summary->schedule = array(
            'fieldsCount' => $this->isFieldsCountValid() ? (int) $this->filter['schedule']['fields_count'] : 1,
            'matches' => $this->matches,
        );
    }

    private function isFieldsCountValid()
    {
        if (isset($this->filter['schedule']['fields_count']) && is_numeric($this->filter['schedule']['fields_count'])) {
            $fields_count = $this->filter['schedule']['fields_count'];
            return $fields_count >= 2 && $fields_count <= ($this->competition->getCountTeams() / 2);
        }
        return false;
    }

    private function loadTeams()
    {
        if ($this->isAttributeUnchecked('teams')) {
            return $this->disableAttribute('teams');
        }
        $this->summary->teams = array(
            'count' => isset($this->filter['teams']['count']) ? $this->competition->getCountTeams() : false,
            'teams' => isset($this->filter['teams']['list']) ? $this->competition->getTeams() : false,
        );
    }

    private function loadCompetitionStatistics()
    {
        if ($this->isAttributeUnchecked('statistics')) {
            return $this->disableAttribute('statistics');
        }
        $this->summary->statistics = array(
            'matchesCountAll' => isset($this->filter['statistics']['matches_count_all'])
                ? $this->competition->getCountMatches(MatchSelection::ALL_MATCHES) : false,
            'matchesCountPlayed' => isset($this->filter['statistics']['matches_count_played'])
                ? $this->competition->getCountMatches(MatchSelection::PLAYED_MATCHES) : false,
            'matchesCountUpcoming' => isset($this->filter['statistics']['matches_count_upcoming'])
                ? $this->competition->getCountMatches(MatchSelection::UPCOMING_MATCHES) : false,
            'goalsCount' => isset($this->filter['statistics']['goals_count'])
                ? $this->getGoalsCount() : false,
            'goalsAverage' => false,
        );
        $this->loadGoalsAverage();
    }

    private function getGoalsCount()
    {
        $goals_count = 0;
        foreach ($this->matches as $match) {
            if ($match->hasScore()) {
                $goals_count += $match->getScoreHome() + $match->getScoreAway();
            }
        }
        return $goals_count;
    }

    private function loadGoalsAverage()
    {
        if (isset($this->filter['statistics']['goals_average'])) {
            $average = $this->summary->statistics['matchesCountAll'] == 0 ?
                    0 : $this->summary->statistics['goalsCount'] / $this->summary->statistics['matchesCountAll'];
            $this->summary->statistics['goalsAverage'] = $average;
        }
    }

    private function loadPlayerStatistics()
    {
        if ($this->isAttributeUnchecked('winners') && $this->isAttributeUnchecked('players_actions')) {
            return $this->disableAttribute('playersActions');
        }
        if ($this->stats) {
            $this->initPlayersActions();
            $this->loadPlayersActionsFullList();// full_list must be loaded always
            $this->sortPlayersActionsFullList();
            foreach ($this->stats->getAllMatchActions() as $action) {
                $this->summary->playersActions[$action]['top5'] = $this->getPlayersActionTop5($action);
                $this->summary->playersActions[$action]['teams'] = $this->getPlayersActionTeams($action);
            }
        }
    }

    private function initPlayersActions()
    {
        $this->summary->playersActions = array();
        foreach ($this->stats->getAllMatchActions() as $action) {
            $this->summary->playersActions[$action] = array(
                'top5' => array(),
                'teams' => array(),
                'fullList' => array(
                    'sum' => array(),
                    'count' => array(),
                )
            );
        }
    }

    private function loadPlayersActionsFullList()
    {
        foreach ($this->stats->getStatistics() as $stat) {
            if (!isset($stat['actions'])) {
                $this->loadMemberWithoutActions($stat['member']);
            } else {
                $this->loadMemberWithActions($stat['member'], $stat['actions']);
            }
        }
    }

    private function loadMemberWithoutActions(TeamMember $member)
    {
        foreach ($this->stats->getAllMatchActions() as $action) {
            $this->summary->playersActions[$action]['fullList']['sum'][0][] = $member;
            $this->summary->playersActions[$action]['fullList']['count'][0][] = $member;
        }
    }

    private function loadMemberWithActions(TeamMember $member, $actions)
    {
        foreach ($actions as $action) {
            $this->summary->playersActions[$action->matchAction]['fullList']['sum'][$action->sum][] = $member;
            $this->summary->playersActions[$action->matchAction]['fullList']['count'][$action->count][] = $member;
        }
    }

    private function sortPlayersActionsFullList()
    {
        foreach ($this->stats->getAllMatchActions() as $action) {
            krsort($this->summary->playersActions[$action]['fullList']['sum']);
            krsort($this->summary->playersActions[$action]['fullList']['count']);
        }
    }

    private function getPlayersActionTop5($action)
    {
        if (isset($this->filter['players_actions']['top_5'])) {
            return array(
                'sum' => array_slice($this->summary->playersActions[$action]['fullList']['sum'], 0, 5, true),
                'count' => array_slice($this->summary->playersActions[$action]['fullList']['count'], 0, 5, true)
            );
        }
        return false;
    }

    private function getPlayersActionTeams($action)
    {
        if (isset($this->filter['players_actions']['teams'])) {
            $teams = array();
            foreach ($this->summary->playersActions[$action]['fullList'] as $type => $values) {
                foreach ($values as $value => $players) {
                    foreach ($players as $team_member) {
                        $team = $team_member->getNameTeam();
                        if (!isset($teams[$team][$type][$value])) {
                            $teams[$team][$type][$value] = array();
                        }
                        $teams[$team][$type][$value][] = $team_member;
                    }
                }
            }
            return $teams;
        }
        return false;
    }

    private function loadTables()
    {
        if (($this->isAttributeUnchecked('winners') &&
            $this->isAttributeUnchecked('tables')) || $this->competition instanceof Playoff) {
            return $this->disableAttribute('tables');
        }
        $tables = $this->competition instanceof Season ? $this->getSeasonTables() : $this->getMatchesTables();
        $this->summary->tables = array(
            'full' => isset($this->filter['tables']['full']) ? $tables['classic_table']->getTableFull() : false,
            'home' => isset($this->filter['tables']['home']) ? $tables['classic_table']->getTableHome() : false,
            'away' => isset($this->filter['tables']['away']) ? $tables['classic_table']->getTableAway() : false,
            'cross' => isset($this->filter['tables']['cross']) ? $tables['cross_table']->getTable() : false,
        );
    }

    private function getSeasonTables()
    {
        return array(
            'classic_table' => StmCache::getSeasonTable($this->competition),
            'cross_table' => CrossTableFactory::getSeasonCrossTable($this->competition)
        );
    }

    private function getMatchesTables()
    {
        $ms = array(
            'matchType' => \STM\Match\MatchSelection::PLAYED_MATCHES,
            'loadScores' => false,
            'loadPeriods' => false,
            'competition' => $this->competition,
        );
        return array(
            'classic_table' => ResultTable::getTable(
                $ms,
                new TablePoints(),
                $this->competition->getNumberOfMatchPeriods()
            ),
            'cross_table' => CrossTableFactory::getMatchSelectionCrossTable($ms)
        );
    }

    private function loadWinners()
    {
        if ($this->isAttributeUnchecked('winners') || $this->competition instanceof Playoff) {
            return $this->disableAttribute('winners');
        }
        $this->summary->winners = array(
            'podium' => $this->getWinnersPodium(),
            'topPlayers' => $this->getWinnersTopPlayers(),
        );
    }

    private function getWinnersPodium()
    {
        if (isset($this->filter['winners']['top_3'])) {
            return array(
                1 => $this->getTeamsFromTableOnPosition(1),
                2 => $this->getTeamsFromTableOnPosition(2),
                3 => $this->getTeamsFromTableOnPosition(3),
            );
        }
        return false;
    }

    private function getTeamsFromTableOnPosition($position)
    {
        if (isset($this->summary->tables['full']) && array_key_exists($position - 1, $this->summary->tables['full'])) {
            $row = $this->summary->tables['full'][$position - 1];
            return array($row->name);
        }
        return array();
    }

    private function getWinnersTopPlayers()
    {
        if (isset($this->filter['winners']['players'])) {
            $top_players = array();
            if ($this->stats) {
                foreach ($this->stats->getAllMatchActions() as $action) {
                    $top_players[$action] = array(
                        'sum' =>
                            array_slice($this->summary->playersActions[$action]['fullList']['sum'], 0, 1, true),
                        'count' =>
                            array_slice($this->summary->playersActions[$action]['fullList']['count'], 0, 1, true),
                    );
                }
            }
            return $top_players;
        }
        return false;
    }

    private function loadMatches()
    {
        if ($this->isAttributeUnchecked('matches')) {
            return $this->disableAttribute('matches');
        }
        $this->summary->matches = array();
        foreach ($this->matches as $match) {
            $actions = StmFactory::find()->MatchPlayerAction->findByMatch($match);
            $isAction = !empty($actions) && isset($this->filter['matches']['players_actions']);
            $this->summary->matches[] = array(
                'match' => $match,
                'periods' => isset($this->filter['matches']['periods']) ? $match->getPeriods() : false,
                'playersActions' => $isAction ? $this->categorizeMatchActions($match, $actions) : false
            );
        }
    }

    private function categorizeMatchActions($match, $match_actions)
    {
        $actions = $this->getHomeAwayActionsFromMatch($match, $match_actions);
        return $this->sortActionsByMinute($actions);
    }

    private function getHomeAwayActionsFromMatch($match, $match_actions)
    {
        $actions = array();
        foreach ($match_actions as $action) {//MatchPlayerAction
            $action_array = $action->toArray();
            if (!isset($actions[$action_array['name_action']])) {
                $actions[$action_array['name_action']] = array(
                    'home' => array(),
                    'away' => array(),
                );
            }
            $team = $action->getIdTeam() == $match->getIdTeamHome() ? 'home' : 'away';
            $actions[$action_array['name_action']][$team]['players'][] = $action_array['name_player'];
            $actions[$action_array['name_action']][$team]['minutes'][] = $action_array['minute_action'];
        }
        return $actions;
    }

    private function sortActionsByMinute($actions)
    {
        foreach ($actions as $action => $teams) {
            foreach ($teams as $field => $players_and_minutes) {
                if (!empty($players_and_minutes)) {
                    // sort minutes by minute_action
                    arsort($players_and_minutes['minutes']);
                    // reorganize players array
                    $new_players = array();
                    foreach (array_keys($players_and_minutes['minutes']) as $key) {
                        $new_players[$key] = $players_and_minutes['players'][$key];
                    }
                    // save changes
                    $players_and_minutes['players'] = $new_players;
                    $teams[$field] = $players_and_minutes;
                }
            }
            $actions[$action] = $teams;
        }
        return $actions;
    }

    // statistics and tables must be loaded if winners are loaded
    private function disablUncheckedAttributesNeededForLoading()
    {
        if ($this->isAttributeUnchecked('tables') || $this->competition instanceof Playoff) {
            $this->disableAttribute('tables');
        }
        if ($this->isAttributeUnchecked('players_actions')) {
            $this->disableAttribute('playersActions');
        }
        if (!$this->isAttributeUnchecked('players_actions') && !isset($this->filter['players_actions']['full_list'])) {
            foreach ($this->stats->getAllMatchActions() as $action) {
                $this->summary->playersActions[$action]['fullList'] = false;
            }
        }
    }

    private function isAttributeUnchecked($attribute)
    {
        return !isset($this->filter[$attribute]) || !isset($this->filter[$attribute]['checked']);
    }

    private function disableAttribute($attribute)
    {
        $this->summary->$attribute = false;
    }
}
