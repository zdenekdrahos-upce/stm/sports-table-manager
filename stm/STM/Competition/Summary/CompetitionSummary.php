<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition\Summary;

/**
 * CompetitionSummary loaded via CompetitionSummaryFactory. If any attribute
 * is not loaded then his value is equal to false
 */
class CompetitionSummary
{
    /** @var array */
    public $competition = array(
        'name' => false,
        'dateStart' => false,
        'dateEnd' => false
    );

    /**
     * Keys
     * - fields_count = number of fields
     * - matches = array of matches, where item is instanceof Match
     * @var false|array
     */
    public $schedule = array(
        'fieldsCount' => 1,
        'matches' => array(),
    );

    /**
     * Keys
     * - teams_count = number of teams
     * - teams = array of teams, where item is instanceof \STM\Team\Team
     * @var false|arrays
     */
    public $teams = array(
        'count' => 0,
        'teams' => array(),
    );

    /**
     * Keys
     * - podium = array with keys 1,2,3 where value is array containing names of teams on position
     * - top_players = array of top players where key is name of match action and value is array
     *     where key is value of statistic and values is array of players where item is instanceof TeamMember
     * @var false|array
     */
    public $winners = array(
        'podium' => array(
            1 => array(),
            2 => array(),
            3 => array(),
        ),
        'topPlayers' => array(
            /* 'Goal' => array(
                            'count' => array(25 => array('TeamMember1', ...)),
                            'sum' => array(25 => array('TeamMember1', ...)),
               ),
            */
        ),
    );

    /**
     * Keys: matches_count_all, matches_count_played, matches_count_upcoming, goals_count, goals_average
     * Value is number or false if key is not printed
     * @var false|array
     */
    public $statistics = array(
        'matchesCountAll' => false,
        'matchesCountPlayed' => false,
        'matchesCountUpcoming' => false,
        'goalsCount' => false,
        'goalsAverage' => false,
    );

    /**
     * Array of match actions, where keys is name of match actions and value is array:
     * - top_5 - array, key is name of match action and value is array with value and TeamMembers
     * - teams - array, key name of the team and value is array with value and TeamMember
     * - full_list - array, key is value of match action and value is array with TeamMembers
     * @var false|array
     */
    public $playersActions = array(
        /*
        'Goal' => array(
            'top5' => array(
                'count' => array(25 => array('TeamMember1', ...), ...),
                'sum' => array(25 => array('TeamMember1', ...), ...),
            'teams' => array(
                'TeamName' => array(
                    'count' => array(25 => array('TeamMember1', ...), ...),
                    'sum' => array(25 => array('TeamMember1', ...), ...),
                )
            ),
            'fullList' => array(
                'count' => array(
                    25 => array('TeamMember1', ...),
                    ...
                ),
                'sum' => array(
                    95 => array('TeamMember1', ...),
                    ...
                ),
            )
        ),
         */
    );

    /**
     * Keys
     * - full, home, away - array where items are instanceof TableRow
     * - cross - array where items are instanceof CrossTableRow
     * If value is false then key is not printed
     * @var false|array
     */
    public $tables = array(
        'full' => false,
        'cross' => false,
        'home' => false,
        'away' => false,
    );

    /**
     * Array of matches, where item is array with keys
     * - match - instanceof Match
     * - periods - array where item is instanceof Period
     * - players_actions - array where key is name of match action and value are
     *      arrays of player's names for home and away team
     * @var false|array
     */
    public $matches = array(
        /*
        1 => array(
            'match' => 'instanceof Match',
            'periods' => array('instanceof Period', '...'),
            'playersActions' => array(
                'Goal' => array(
                    'home' => array(
                       'players' => array('name of player', '...'),
                       'minutes' => array('minute_action', '...'),
                    ),
                    'away' => array(
                       'players' => array('name of player', '...'),
                       'minutes' => array('minute_action', '...'),
                    ),
                )
            )
        ),
         */
    );
}
