<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition;

use STM\Helpers\Event;

/**
 * CompetitionEvent class
 * - for saving events related with Competition
 */
class CompetitionEvent
{
    /** Creating new competition */
    const CREATE = 'Create Competition';
    /** Updating competition - update date start, date end, match_periods, name, score_type */
    const UPDATE = 'Update Competition';
    /** Deleting competition - competition + teams in competition + matches which belongs to competition */
    const DELETE = 'Delete Competition';
    /** Deleting all competition matches */
    const DELETE_MATCHES = 'Delete Matches';
    /** Add team to competition */
    const ADD_TEAMS = 'Add Team';
    /** Delete team from competition + team matches in competition (if some exist) */
    const DELETE_TEAM = 'Delete Team';
    /** Events with your_team in competition - create, delete */
    const YOUR_TEAM = 'Your Team';
    /** Events with category - create, delete, update */
    const CATEGORY = 'Category';
    /** Season - generate match schedule of the season */
    const SEASON_SCHEDULE = 'Season Schedule';
    /** Season rounds */
    const SEASON_ROUNDS = 'Season Rounds';
    /** Season table */
    const SEASON_TABLE = 'Season Table';
    /** Playoff - generate round of the playoff */
    const PLAYOFF_ROUND = 'Playoff Round';
    /** Import matches */
    const IMPORT_MATCHES = 'Import Matches';
    /** Import season tables */
    const IMPORT_SEASON_TABLES = 'Import Season Tables';
    /** Import players to all matches */
    const IMPORT_PLAYERS_TO_MATCHES = 'Import Players';


    private function __construct()
    {
    }

    /**
     * Saves events related with Competition
     * @param CompetitionEvent $competitionEvent constant from CompetitionEvent class
     * @param string $message description of event
     * @param boolean $result if event was sucessful or fail
     * @return boolean
     * Returns true if event was successfuly saved, otherwise false (bad arguments
     * or error during file saving)
     */
    public static function log($competitionEvent, $message, $result)
    {
        $event = new Event('\STM\Competition\CompetitionEvent', 'competitions');
        $event->setMessage($competitionEvent, $message, $result);
        return $event->logEvent();
    }
}
