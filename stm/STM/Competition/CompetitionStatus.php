<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition;

use STM\User\UserGroup;
use STM\Utils\Classes;

/**
 * CompetitionStatus class
 * - represents database table 'COMPETITION_STATUS'
 * - used for simpler work with primary keys from table
 */
class CompetitionStatus
{
    /** only admin and manager */
    const HIDDEN = 'Hidden';
    /** everybody, matches cannot be predicted */
    const VISIBLE = 'Visible';
    /** everybody, allowed match predictions */
    const PUBLICS = 'Public';

    private function __construct()
    {
    }

    public static function getStatus($id_type)
    {
        if (is_string($id_type)) {
            switch ($id_type) {
                case 'H':
                    return self::HIDDEN;
                case 'V':
                    return self::VISIBLE;
                case 'P':
                    return self::PUBLICS;
            }
        }
        return 'Invalid Status ID';
    }

    public static function getStatusId($constant)
    {
        if (Classes::isClassConstant('\STM\Competition\CompetitionStatus', $constant)) {
            switch ($constant) {
                case self::VISIBLE:
                    return 'V';
                case self::HIDDEN:
                    return 'H';
                case self::PUBLICS:
                    return 'P';
            }
        }
        return false;
    }

    public static function hasAccessToHiddenCompetition($user_group)
    {
        if (Classes::isClassConstant('\STM\User\UserGroup', $user_group)) {
            $allowed_groups = array(UserGroup::ADMIN, UserGroup::MANAGER);
            return in_array($user_group, $allowed_groups);
        }
        return false;
    }

    public static function hasAccessToCompetition($user_group, Competition $competition)
    {
        if (Classes::isClassConstant('\STM\User\UserGroup', $user_group)) {
            if ($competition->getStatusId() == 'H') {
                return self::hasAccessToHiddenCompetition($user_group);
            }
            return true;
        }
        return false;
    }
}
