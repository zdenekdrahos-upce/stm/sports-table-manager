<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition\Playoff\Serie;

use STM\Competition\Playoff\Playoff;
use STM\Entities\AbstractEntityFactory;

class PlayoffSerieFactory extends AbstractEntityFactory
{
    public function findById($id)
    {
        return $this->entityHelper->findById($id);
    }

    public function findByPlayoff(Playoff $playoff)
    {
        $methods = array('setPlayoff' => $playoff);
        return $this->entityHelper->setAndFindArray($methods);
    }

    public function findByPlayoffRound(Playoff $playoff, $round)
    {
        $methods = array(
            'setPlayoff' => $playoff,
            'setRound' => $round,
        );
        return $this->entityHelper->setAndFindArray($methods);
    }

    protected function getEntitySelection()
    {
         return new PlayoffSerieSelection();
    }

    protected function getEntityClass()
    {
        return 'STM\Competition\Playoff\Serie\PlayoffSerie';
    }
}
