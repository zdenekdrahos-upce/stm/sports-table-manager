<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition\Playoff\Serie;

use STM\Competition\Playoff\Playoff;
use STM\DB\Object\DatabaseObject;

class PlayoffSerie
{
    /** @var DatabaseObject */
    private static $db;

    /** @var int */
    private $id_serie;
    /** @var int */
    private $id_playoff;
    /** @var int */
    private $id_team_1;
    /** @var int */
    private $id_team_2;
    /** @var int */
    private $round;
    /** @var int */
    private $seeded;
    /** @var int */
    private $id_next_serie;

    /** @var int number of wins of the team 1 */
    private $wins_team_1;
    /** @var int number of wins of the team 2 */
    private $wins_team_2;
    /** @var int number of draw */
    private $draws;

    /** @var string */
    private $name_team_1;
    /** @var string */
    private $name_team_2;

    private function __construct()
    {
    }

    public static function setORM(DatabaseObject $dbObject)
    {
        if (is_null(self::$db)) {
            self::$db = $dbObject;
        }
    }

    public static function deletePlayoffRound(Playoff $playoff)
    {
        $last_round = $playoff->getPlayedRounds();
        if ($last_round >= 1) {
            $series = self::findByPlayoffRound($playoff, $last_round);
            foreach ($series as $serie) {
                $serie->delete();
            }
            return true;
        }
    }

    public static function updateSeededType(Playoff $playoff, $type)
    {
        if ($playoff->getPlayedRounds() > 0) {
            $series = self::findByPlayoffRound($playoff, $playoff->getPlayedRounds());
            foreach ($series as $serie) {
                if ($type == 'variable') {
                    if ($serie->isFinished($playoff)) {
                        $serie->updateSeededToVariable($playoff);
                    }
                } else {
                    $serie->updateSeededToFixed($playoff);
                }
            }
            return true;
        }
    }

    private static function findByPlayoffRound(Playoff $playoff, $round)
    {
        $factory = new PlayoffSerieFactory();
        return $factory->findByPlayoffRound($playoff, $round);
    }

    private function updateSeededToFixed($playoff)
    {
        $home_seeded = $playoff->getSeededOfTeam($this->id_team_1);
        $away_seeded = $playoff->getSeededOfTeam($this->id_team_2);
        $this->updateSeeded(min($home_seeded, $away_seeded));
    }

    private function updateSeededToVariable($playoff)
    {
        $winner_seeded = $playoff->getSeededOfTeam($this->getIdWinner());
        $this->updateSeeded($winner_seeded);
    }

    private function updateSeeded($new_seeded)
    {
        if ($new_seeded != $this->seeded) {
            self::$db->updateById($this->id_serie, array('seeded' => $new_seeded));
        }
    }

    private function delete()
    {
        return self::$db->deleteById($this->id_serie);
    }


    # INSTANCE METHODS FOR ONE PLAYOFF SERIE
    public function isFinished($playoff)
    {
        if ($playoff instanceof Playoff && $playoff->getId() == $this->id_playoff) {
            return max($this->wins_team_1, $this->wins_team_2) >= $playoff->getSerieWinMatches();
        }
        return false;
    }

    public function getIdWinner()
    {
        return $this->wins_team_1 > $this->wins_team_2 ? $this->getIdTeam1() : $this->getIdTeam2();
    }

    public function getNameWinner()
    {
        return $this->wins_team_1 > $this->wins_team_2 ? $this->name_team_1 : $this->name_team_2;
    }

    public function getId()
    {
        return (int) $this->id_serie;
    }

    public function getIdPlayoff()
    {
        return (int) $this->id_playoff;
    }

    public function getIdTeam1()
    {
        return (int) $this->id_team_1;
    }

    public function getIdTeam2()
    {
        return (int) $this->id_team_2;
    }

    public function getIdNextSerie()
    {
        return $this->id_next_serie ? (int) $this->id_next_serie : null;
    }

    public function getRound()
    {
        return (int) $this->round;
    }

    public function getSeeded()
    {
        return (int) $this->seeded;
    }

    public function toArray()
    {
        return array(
            'id_serie' => $this->getId(),
            'round' => $this->getRound(),
            'seeded' => $this->getSeeded(),
            'id_team_1' => (int) $this->id_team_1,
            'id_team_2' => (int) $this->id_team_2,
            'team_1' => $this->name_team_1,
            'team_2' => $this->name_team_2,
            'wins_team_1' => (int) $this->wins_team_1,
            'wins_team_2' => (int) $this->wins_team_2,
            'draws' => (int) $this->draws,
        );
    }

    public function __toString()
    {
        return $this->name_team_1 . ':' . $this->name_team_2;
    }
}
