<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition\Playoff\Serie;

use STM\DB\SQL\SelectQuery;
use STM\DB\SQL\JoinTable;
use STM\DB\SQL\JoinType;

class PlayoffSerieSQL
{

    /** @return SelectQuery */
    public static function selectPlayoffSeries()
    {
        $query = new SelectQuery(DT_PLAYOFF_SERIES . ' playoff_series');
        $columns = array(
            0 => 'playoff_series.id_serie, playoff_series.id_playoff, playoff_series.id_team_1,
                  playoff_series.id_team_2, playoff_series.round, playoff_series.seeded,
                  playoff_series.id_next_serie',
            'name_team_1' => 'home.name',
            'name_team_2' => 'away.name',
        );
        $query->setColumns($columns);
        $query->setColumns(
            'sum(case when match_results.score_home = match_results.score_away then 1 else 0 end) as draws,
            sum(case
                 when matches.id_home = playoff_series.id_team_1 AND
                      match_results.score_home > match_results.score_away then 1
                 when matches.id_away = playoff_series.id_team_1 AND
                      match_results.score_home < match_results.score_away then 1
                 else 0
                end) as wins_team_1,
            sum(case
                 when matches.id_home = playoff_series.id_team_2 AND
                      match_results.score_home > match_results.score_away then 1
                 when matches.id_away = playoff_series.id_team_2 AND
                      match_results.score_home < match_results.score_away then 1
                 else 0
                end) as wins_team_2'
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                DT_TEAMS . ' home',
                'playoff_series.id_team_1 = home.id_team'
            )
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                DT_TEAMS . ' away',
                'playoff_series.id_team_2 = away.id_team'
            )
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::LEFT,
                DT_MATCHES . ' matches',
                'playoff_series.id_playoff = matches.id_competition AND playoff_series.id_serie = matches.id_serie'
            )
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::LEFT,
                DT_MATCH_RESULTS . ' match_results',
                'matches.id_match = match_results.id_match'
            )
        );
        $query->setGroupBy(implode(', ', array_values($columns)));
        $query->setOrderBy('playoff_series.round, playoff_series.seeded');
        return $query;
    }
}
