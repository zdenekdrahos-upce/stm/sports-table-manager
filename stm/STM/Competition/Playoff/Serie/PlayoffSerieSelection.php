<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition\Playoff\Serie;

use STM\Competition\Playoff\Playoff;
use STM\Entities\AbstractEntitySelection;

class PlayoffSerieSelection extends AbstractEntitySelection
{

    public function setId($id)
    {
        if (is_numeric($id)) {
            $this->selection->setAttributeWithValue('playoff_series.id_serie', (string)$id);
        }
    }

    public function setPlayoff(Playoff $playoff)
    {
        $this->selection->setAttributeWithValue('playoff_series.id_playoff', (string)$playoff->getId());
    }

    public function setRound($round)
    {
        $this->selection->setAttributeWithValue('playoff_series.round', (string)$round);
    }

    protected function getQuery()
    {
        return PlayoffSerieSQL::selectPlayoffSeries();
    }
}
