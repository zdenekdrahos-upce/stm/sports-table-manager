<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition\Playoff\Team;

use STM\DB\Database;
use STM\DB\SQL\SelectQuery;
use STM\DB\SQL\Condition;
use STM\Competition\Playoff\Playoff;

class PlayoffTeamSQL
{

    /** @return SelectQuery */
    public static function selectPlayoffTeams(Playoff $playoff)
    {
        $query = new SelectQuery(DT_COMPETITIONS_TEAMS);
        $query->setColumns('id_competition as id_playoff, id_team, playoff_seeded');
        $condition = new Condition('id_competition = {P}', array('P' => $playoff->getId()));
        $query->setWhere($condition);
        $query->setOrderBy(Database::getNullFunction() . '(playoff_seeded, 1000) asc');
        return $query;
    }
}
