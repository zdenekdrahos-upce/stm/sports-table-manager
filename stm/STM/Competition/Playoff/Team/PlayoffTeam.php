<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition\Playoff\Team;

use STM\DB\Object\DatabaseObject;
use STM\Competition\Playoff\Playoff;

class PlayoffTeam
{
    /** @var DatabaseObject */
    private static $db;

    /** @var int */
    private $id_team;
    /** @var int */
    private $id_playoff;
    /** @var int */
    private $playoff_seeded;
    /** @var \STM\Team\Team */
    private $team;

    private function __construct()
    {
    }

    public static function setORM(DatabaseObject $dbObject)
    {
        if (is_null(self::$db)) {
            self::$db = $dbObject;
        }
    }

    public static function find(Playoff $playoff)
    {
        $query = PlayoffTeamSQL::selectPlayoffTeams($playoff);
        $playoff_teams = self::$db->select($query);
        return self::attachTeams($playoff_teams, $playoff->getTeams());
    }

    public static function updatePlayoffSeeded(Playoff $playoff, $new_seeded)
    {
        if (is_array($new_seeded)) {
            PlayoffTeamValidator::setPlayoff($playoff);
            if (PlayoffTeamValidator::checkPlayoffSeeded($new_seeded)) {
                self::updatePlayoff($playoff, $new_seeded);
                return true;
            }
        }
        return false;
    }

    public function getIdTeam()
    {
        return (int) $this->id_team;
    }

    public function getSeeded()
    {
        return is_null($this->playoff_seeded) ? null : (int) $this->playoff_seeded;
    }

    /** @var \STM\Team\Team */
    public function getTeam()
    {
        return $this->team;
    }

    public function __toString()
    {
        $seeded = $this->getSeeded() ? $this->getSeeded() : '?';
        return "{$this->team} ({$seeded})";
    }

    private static function attachTeams($result_array, $teams)
    {
        foreach ($result_array as $playoff_team) {
            foreach ($teams as $team) {
                if ($playoff_team->id_team == $team->getId()) {
                    $playoff_team->team = $team;
                    break;
                }
            }
        }
        return $result_array;
    }

    private static function updatePlayoff($playoff, $new_seeded)
    {
        foreach ($playoff->getTeamsWithSeeded() as $playoff_team) {
            $id_team = $playoff_team->getIdTeam();
            if (isset($new_seeded[$id_team])) {
                $playoff_team->updateTeamSeeded($new_seeded[$id_team]);
            }
        }
    }

    private function updateTeamSeeded($seeded)
    {
        if ($seeded != $this->getSeeded()) {
            self::$db->updateById(
                array('playoff' => $this->id_playoff, 'team' => $this->id_team),
                array('playoff_seeded' => $seeded)
            );
        }
    }
}
