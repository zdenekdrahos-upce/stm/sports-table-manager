<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition\Playoff\Team;

use STM\Helpers\ObjectValidator;
use STM\Web\Message\FormError;

/**
 * Class for validating attributes for database table 'COMPETITIONS_TEAMS'
 * - attributes = serie_win_matches
 */
final class PlayoffTeamValidator
{
    /** @var ObjectValidator */
    private static $validator;

    public static function init($formProcessor = false)
    {
        if (is_null(self::$validator)) {
            self::$validator = new ObjectValidator('\STM\Competition\Playoff\Playoff');
        }
        self::$validator->setFormProcessor($formProcessor);
    }

    public static function setPlayoff($playoff)
    {
        self::$validator->setComparedObject($playoff);
    }

    public static function resetPlayoff()
    {
        self::$validator->resetComparedObject();
    }

    public static function checkPlayoffSeeded($seeded_array)
    {
        if (self::$validator->isSetComparedObject()) {
            $change = self::checkSeededArray($seeded_array);
            self::checkChange($change);
            self::checkArraySize($seeded_array);
            self::checkNumberOfPlayedRounds();
        } else {
            self::$validator->getFormProcessor()->addError('Playoff must be set');
        }
        return self::$validator->isValid();
    }

    private static function checkSeededArray($seeded_array)
    {
        $change = false;
        $max = self::getMaxTeamSeeded();
        foreach (self::getTeams() as $playoff_team) {
            if (isset($seeded_array[$playoff_team->getIdTeam()])) {
                $new_seeded = $seeded_array[$playoff_team->getIdTeam()];
                if ($change == false && $new_seeded != $playoff_team->getSeeded()) {
                    $change = true;
                }
                if (is_numeric($new_seeded)) {
                    $seeded_array[$playoff_team->getIdTeam()] = (int) $new_seeded;
                    self::$validator->checkNumber(
                        $new_seeded,
                        array('min' => 1, 'max' => $max),
                        stmLang('competition', 'round', 'seeded')
                    );
                }
            }
        }
        return $change;
    }

    private static function checkChange($change)
    {
        if ($change == false) {
            self::$validator->getFormProcessor()->addError(
                FormError::get('no-change', array(stmLang('competition', 'round', 'seeded')))
            );
        }
    }

    private static function checkArraySize($seeded_array)
    {
        if (count(self::getTeams()) != count(array_unique($seeded_array))) {
            self::$validator->getFormProcessor()->addError(FormError::get('seeded-unique'));
        }
    }

    private static function checkNumberOfPlayedRounds()
    {
        if (self::$validator->getValueFromCompared('getPlayedRounds') > 0) {
            self::$validator->getFormProcessor()->addError(FormError::get('seeded-firstround'));
        }
    }

    private static function getMaxTeamSeeded()
    {
        return min(self::$validator->getValueFromCompared('getCountTeams'), PLAYOFF_MAX_TEAMS);
    }

    private static function getTeams()
    {
        return self::$validator->getValueFromCompared('getTeamsWithSeeded');
    }
}
