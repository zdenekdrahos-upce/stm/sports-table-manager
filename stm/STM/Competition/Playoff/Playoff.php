<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition\Playoff;

use STM\DB\Object\DatabaseObject;
use STM\Competition\Competition;
use STM\Team\Team;
use STM\Competition\Playoff\Team\PlayoffTeam;
use STM\Utils\Arrays;
use \Exception;

/**
 * Playoff class
 * - instance: represents row from database table 'PLAYOFFS' and 'COMPETITIONS'
 * - static methods: for finding playoff, creating new one
 */
final class Playoff extends Competition
{
    /** @var DatabaseObject */
    private static $dbPlayoff;

    /** @var int */
    private $serie_win_matches;

    /** @var int */
    private $played_rounds;
    /** @var array(PlayoffTeam) */
    private $teams_with_seeded;

    protected function __construct()
    {
        parent::__construct();
    }

    public static function setORM(DatabaseObject $dbObject)
    {
        if (is_null(self::$dbPlayoff)) {
            self::$dbPlayoff = $dbObject;
        }
    }

    public static function findById($id)
    {
        if (is_numeric($id)) {
            $query = PlayoffSQL::selectPlayoff($id);
            return self::$dbPlayoff->select($query, 'object', true);
        }
        return false;
    }

    /** @param array $attributes */
    public static function create($attributes)
    {
        if (Arrays::isArrayValid($attributes, array('playoff', 'competition'))) {
            if (PlayoffValidator::checkCreate($attributes['playoff'])) {
                $attributes['competition']['id_competition_type'] = 'P';
                $idCompetition = parent::create($attributes['competition'], 'playoff');
                if ($idCompetition) {
                    $attributes['playoff']['id_playoff'] = $idCompetition;
                    if (self::$dbPlayoff->insertValues($attributes['playoff'])) {
                        return $idCompetition;
                    } else {
                        parent::deleteCompetition($idCompetition);
                    }
                }
            }
        }
        return false;
    }


    # INSTANCE METHODS FOR ONE PLAYOFF
    /** @param \STM\Team\Team $team */
    public function deleteTeam(Team $team)
    {
        if ($this->played_rounds == 0) {
            return parent::deleteTeam($team);
        }
        return false;
    }

    /** @param \STM\Team\Team $team */
    public function addTeam($team)
    {
        if ($this->played_rounds == 0) {
            return parent::addTeam($team);
        }
        return false;
    }

    /** @param Competition $competition */
    public function addTeamsFromCompetition($competition)
    {
        if ($this->played_rounds == 0) {
            return parent::addTeamsFromCompetition($competition);
        }
        return false;
    }

    public function deleteMatches()
    {
        throw new Exception('Not Allowed');
    }

    /** @param int $serie_win_matches */
    public function updatePlayoff($serie_win_matches)
    {
        PlayoffValidator::setPlayoff($this);
        if (PlayoffValidator::checkSerieWinMatches($serie_win_matches)) {
            $attributes = array('serie_win_matches' => $serie_win_matches);
            return self::$dbPlayoff->updateById($this->id_competition, $attributes);
        }
        return false;
    }

    public function getSerieWinMatches()
    {
        return (int) $this->serie_win_matches;
    }

    public function getPlayedRounds()
    {
        return (int) $this->played_rounds;
    }

    public function getMaxRound()
    {
        $team_count = parent::getCountTeams();
        return (int) log($team_count, 2);
    }

    public function getTeamsWithSeeded()
    {
        $this->checkSeeded();
        return $this->teams_with_seeded;
    }

    public function getSeededOfTeam($id_team)
    {
        $this->checkSeeded();
        if (is_numeric($id_team)) {
            foreach ($this->teams_with_seeded as $playoffTeam) {
                if ($playoffTeam->getIdTeam() == $id_team) {
                    return $playoffTeam->getSeeded();
                }
            }
        }
        return false;
    }

    /**
     * Method for templating. Returns playoff as an array
     * @return array
     */
    public function toArray()
    {
        $playoff_array = array('serie_win_matches' => $this->serie_win_matches);
        return array_merge(parent::toArray(), $playoff_array);
    }

    private function checkSeeded()
    {
        if (is_null($this->teams_with_seeded)) {
            $this->teams_with_seeded = PlayoffTeam::find($this);
        }
    }
}
