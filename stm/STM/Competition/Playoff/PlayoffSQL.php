<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition\Playoff;

use STM\DB\SQL\SelectQuery;
use STM\DB\SQL\JoinTable;
use STM\DB\SQL\JoinType;
use STM\DB\SQL\Condition;
use STM\Competition\CompetitionSQL;

class PlayoffSQL
{

    /**
     * Selects all playoffs, it also selects columns from parent table competitions
     * @return SelectQuery
     */
    public static function selectPlayoff($idPlayoff)
    {
        $query = CompetitionSQL::selectCompetitions();
        $query->setColumns('serie_win_matches, count(distinct series.round) as played_rounds');
        $query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                DT_PLAYOFFS . ' playoffs',
                'playoffs.id_playoff = competitions.id_competition'
            )
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::LEFT,
                DT_PLAYOFF_SERIES . ' series',
                'playoffs.id_playoff = series.id_playoff'
            )
        );
        $query->setGroupBy(
            'id_competition, competitions.name, match_periods, date_start, date_end,
             id_competition_type, id_score_type, competitions.id_category, id_status,
             categories.name, serie_win_matches'
        );
        $query->setWhere(
            new Condition(
                "id_competition_type = 'P' AND playoffs.id_playoff = {0}",
                array(0 => $idPlayoff)
            )
        );
        return $query;
    }
}
