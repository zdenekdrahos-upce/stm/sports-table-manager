<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition\Playoff\Round;

use STM\DB\Object\DatabaseObject;
use STM\DB\SQL\Query;
use STM\DB\SQL\InsertQuery;
use STM\DB\SQL\UpdateQuery;
use STM\DB\SQL\Condition;
use \Exception;

/**
 * PlayoffRoundSaver class
 * - for saving generated round of the playoff
 */
final class PlayoffRoundSaver
{

    /** @var DatabaseObject */
    private static $database;

    public static function setORM(DatabaseObject $dbObject)
    {
        if (is_null(self::$database)) {
            self::$database = $dbObject;
        }
    }

    public static function save($data)
    {
        if (PlayoffRoundValidator::checkGeneratedRound($data)) {
            $schedule = new self($data);
            $callback = array($schedule, 'savePlayoffRound');
            return self::$database->executeTransaction($callback);
        }
        return false;
    }

    /** @var InsertQuery */
    private $insertQuery;
    /** @var array */
    private $data;
    /** @var int */
    private $generatedRound;

    private function __construct($data)
    {
        $this->data = $data;
        $this->generatedRound = $this->data['round'];
        $this->insertQuery = new InsertQuery(DT_PLAYOFF_SERIES);
        $this->insertQuery->addAttributes(
            array(
                'id_playoff' => $this->data['playoff']->getId(),
                'round' => $this->generatedRound
            )
        );
    }

    public function savePlayoffRound()
    {
        foreach ($this->data['matches'] as $match) {
            $this->insertSerie($match);
        }
    }

    private function insertSerie($match)
    {
        $row = $this->getSerieAttributes($match);
        $this->insertQuery->addAttributes($row);
        $this->executeQuery($this->insertQuery);
        if ($this->generatedRound >= 2) {
            $this->updateIdNextSerie($row['id_team_1'], $row['id_team_2']);
        }
    }

    private function getSerieAttributes($match)
    {
        if ($this->generatedRound == 1) {
            return $this->getFirstRoundAttributes($match);
        } else {
            return $this->getNextRoundAttributes($match);
        }
    }

    /** @param array $match with keys home, away which are instances with PlayoffTeam */
    private function getFirstRoundAttributes($match)
    {
        return array(
            'seeded' => min($match['home']->team->getSeeded(), $match['away']->team->getSeeded()),
            'id_team_1' => $match['home']->team->getIdTeam(),
            'id_team_2' => $match['away']->team->getIdTeam(),
        );
    }

    /** @param array $match with keys home, away which are instances with PlayoffSerie */
    private function getNextRoundAttributes($match)
    {
        return array(
            'seeded' => min($match['home']->team->getSeeded(), $match['away']->team->getSeeded()),
            'id_team_1' => $match['home']->team->getIdWinner(),
            'id_team_2' => $match['away']->team->getIdWinner(),
        );
    }

    private function updateIdNextSerie($team_1, $team_2)
    {
        $updateQuery = new UpdateQuery(DT_PLAYOFF_SERIES);
        $updateQuery->addAttributes(
            array('id_next_serie' => self::$database->getLastInsertId())
        );
        $updateQuery->setWhere(
            $this->getSeriesFromPreviousRound($team_1, $team_2)
        );
        $this->executeQuery($updateQuery);
    }

    private function getSeriesFromPreviousRound($team_1, $team_2)
    {
        return new Condition(
            'round = {R} AND (id_team_1 in ({T1}, {T2}) OR id_team_2 in ({T1}, {T2}))',
            array('R' => ($this->generatedRound - 1), 'T1' => $team_1, 'T2' => $team_2)
        );
    }

    private function executeQuery(Query $query)
    {
        if (self::$database->query($query) == false) {
            throw new Exception();
        }
    }
}
