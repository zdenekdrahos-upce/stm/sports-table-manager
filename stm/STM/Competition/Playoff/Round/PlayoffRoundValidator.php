<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition\Playoff\Round;

use STM\Helpers\ObjectValidator;
use STM\Web\Message\FormError;
use STM\Competition\Playoff\Playoff;
use STM\Competition\Playoff\Serie\PlayoffSerie;
use STM\Utils\Arrays;
use STM\Libs\Generators\Playoff\PlayoffDuelParticipant;

/**
 * PlayoffRoundValidator class
 * - for veryfing generated round before saving to DB
 */
final class PlayoffRoundValidator
{
    /** @var ObjectValidator */
    private static $validator;

    /** @param \STM\Libs\FormProcessor $formProcessor */
    public static function init($formProcessor = false)
    {
        if (is_null(self::$validator)) {
            self::$validator = new ObjectValidator('\STM\Competition\Playoff\Playoff');
        }
        self::$validator->setFormProcessor($formProcessor);
    }

    public static function checkGeneratedRound($data)
    {
        if (self::$validator->checkCreate($data, array('playoff', 'round', 'matches'))) {
            self::$validator->getFormProcessor()->checkCondition(
                $data['playoff'] instanceof Playoff,
                'Playoff must be instanceof Playoff class'
            );
            self::$validator->checkNumber(
                $data['round'],
                array('min' => 1, 'max' => 7),
                stmLang('match', 'season-round')
            );
            self::$validator->getFormProcessor()->checkCondition(is_array($data['matches']), 'Matches must be array');
            if (self::$validator->isValid()) {
                self::$validator->setComparedObject($data['playoff']);
                self::checkRound($data['round']);
                self::checkTeamsSeeded();
                self::checkMatches($data['matches']);
                if (self::$validator->isValid() && $data['round'] >= 2) {
                    self::checkGeneratedNextRound($data['matches']);
                }
            }
        }
        return self::$validator->isValid();
    }

    private static function checkRound($round_number)
    {
        $last_round = self::$validator->getValueFromCompared('getPlayedRounds');
        self::$validator->getFormProcessor()->checkEquality(
            $last_round + 1,
            $round_number,
            'Cannot generate round n.' . $round_number
        );
        $max_round = self::$validator->getValueFromCompared('getMaxRound');
        self::$validator->getFormProcessor()->checkCondition(
            $round_number <= $max_round,
            'In this playoff max rounds can be ' . $max_round
        );
    }

    private static function checkTeamsSeeded()
    {
        $teams = self::$validator->getValueFromCompared('getTeamsWithSeeded');
        $max_seeded = self::$validator->getValueFromCompared('getCountTeams');
        foreach ($teams as $playoff_team) {
            $seeded = $playoff_team->getSeeded();
            self::$validator->getFormProcessor()->checkCondition(
                !is_null($seeded),
                FormError::get('playoffround-seeded')
            );
            self::$validator->checkNumber(
                $seeded,
                array('min' => 1, 'max' => $max_seeded),
                stmLang('competition', 'round', 'seeded')
            );
            if (!self::$validator->isValid()) {
                return;
            }
        }
    }

    private static function checkMatches($matches)
    {
        foreach ($matches as $match) {
            $error = true;
            if (Arrays::isArrayValid($match, array('home', 'away'))) {
                if ($match['home'] instanceof PlayoffDuelParticipant
                    && $match['away'] instanceof PlayoffDuelParticipant) {
                    $error = false;
                }
            }
            if ($error) {
                self::$validator->getFormProcessor()->addError('Invalid format of matches');
                return;
            }
        }
    }

    private static function checkGeneratedNextRound($matches)
    {
        foreach ($matches as $match) {
            if (!($match['home']->team instanceof PlayoffSerie) || !($match['away']->team instanceof PlayoffSerie)) {
                self::$validator->getFormProcessor()->addError('All series must be instances of PlayoffSerie');
                return;
            }
            $playoff = self::$validator->getComparedObject();
            if (!$match['home']->team->isFinished($playoff) || !$match['away']->team->isFinished($playoff)) {
                self::$validator->getFormProcessor()->addError(FormError::get('playoffround-winner'));
                return;
            }
        }
    }
}
