<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition\Playoff\Tree;

class PlayoffTreeAdapter
{
    /** @var Playoff */
    private $playoff;
    /** @var array */
    private $binary_tree_items;
    /** @var array result array, reorganized binary tree */
    private $rounds;

    public function __construct($tree_builder)
    {
        if (!($tree_builder instanceof PlayoffTreeBuilder)) {
            throw new Exception('Tree must be instance of PlayoffTreeBuilder class.');
        }
        $this->playoff = $tree_builder->getPlayoff();
        $this->binary_tree_items = $tree_builder->getBinaryTree()->getItems();
        $this->rounds = array();
        if ($this->playoff->getPlayedRounds() > 0) {
            $this->organizeTreeToRounds();
        }
    }

    public function getRounds()
    {
        return array_reverse($this->rounds, true);
    }

    private function organizeTreeToRounds()
    {
        $series_in_round = 1;
        for ($i = 1; $i <= $this->playoff->getMaxRound(); $i++) {
            $actual_round = $this->playoff->getMaxRound() - $i + 1;
            for ($j = 0; $j < $series_in_round; $j++) {
                $actual_position = pow(2, $i - 1) + $j;
                $serie = $this->binary_tree_items[$actual_position]->team;
                if ($serie->getRound() == $actual_round) {
                    $this->rounds[$i][] = $serie;
                } else {
                    $this->rounds[$i][] = $this->getFutureSerie($actual_round, $actual_position);
                }
            }
            $series_in_round *= 2;
        }
    }

    private function getFutureSerie($actual_round, $actual_position)
    {
        $text = '?<br />?';
        $left_son = 2 * $actual_position;
        $right_son = 2 * $actual_position + 1;
        if ($right_son <= count($this->binary_tree_items)) {
            $left_serie = $this->binary_tree_items[$left_son]->team;
            $right_serie = $this->binary_tree_items[$right_son]->team;
            if ($left_serie->getRound() == ($actual_round - 1)) {
                $l = $left_serie->isFinished($this->playoff) ? $left_serie->getNameWinner() : '?';
                $r = $right_serie->isFinished($this->playoff) ? $right_serie->getNameWinner() : '?';
                $text = $l . '<br />' . $r;
            }
        }
        return $text;
    }
}
