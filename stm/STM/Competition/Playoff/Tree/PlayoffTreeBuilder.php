<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition\Playoff\Tree;

use STM\Competition\Playoff\Playoff;
use STM\Libs\Generators\Playoff\BinaryTree;
use STM\Libs\Generators\Playoff\PlayoffTree;
use STM\Libs\Generators\Playoff\PlayoffGenerator;
use STM\StmFactory;

class PlayoffTreeBuilder
{
    /** @var Playoff */
    private $playoff;
    /** @var BinaryTree */
    private $binaryTree;

    public function __construct(Playoff $playoff)
    {
        $this->playoff = $playoff;
        $this->buildTree();
    }

    /** @return BinaryTree */
    public function getBinaryTree()
    {
        return $this->binaryTree;
    }

    /** @return Playoff */
    public function getPlayoff()
    {
        return $this->playoff;
    }

    private function buildTree()
    {
        $this->binaryTree = new BinaryTree();
        if ($this->playoff->getPlayedRounds() > 0) {
            $series = $this->getAllPlayoffSeries();
            $this->establishTree($series);
            if ($this->playoff->getPlayedRounds() - 1 > 0) {
                $this->insertExistingSeries($series);
            }
        }
    }

    private function establishTree($series)
    {
        $last_series = $series[$this->playoff->getPlayedRounds()];
        if ($this->playoff->getPlayedRounds() == $this->playoff->getMaxRound()) {
            $this->establishFromCompletePlayoff($last_series);
        } else {
            $this->establishFromUnfinishedPlayoff($last_series);
        }
    }

    private function establishFromCompletePlayoff($last_series)
    {
        $last_serie = array_shift($last_series);
        $this->binaryTree->insertRoot(new PlayoffTreeSerie($last_serie->getSeeded(), $last_serie));
    }

    private function establishFromUnfinishedPlayoff($last_series)
    {
        $next_round = array();
        foreach ($last_series as $serie) {
            $next_round[$serie->getSeeded()] = $serie;
        }
        $playoffgen = PlayoffGenerator::create($next_round, '\STM\Competition\Playoff\Tree\PlayoffTreeSerie');
        $playoff_tree = new PlayoffTree($playoffgen);
        $this->binaryTree = $playoff_tree->getBinaryTree();
    }

    private function insertExistingSeries($series)
    {
        for ($i = $this->playoff->getPlayedRounds() - 1; $i >= 1; $i--) {
            foreach ($series[$i] as $serie) {
                $pserie = new PlayoffTreeSerie($serie->getSeeded(), $serie);
                $this->binaryTree->accessLast($pserie);
                $this->placeSerie($pserie);
            }
        }
    }

    private function placeSerie($pserie)
    {
        if ($pserie->seeded % 2 == 1) {
            $before = $this->binaryTree->getItemCount();
            $this->binaryTree->insertLeftSon($pserie);
            if ($before == $this->binaryTree->getItemCount()) {
                // if variable seeding, there can be situtation where both teams have odd seeded
                $this->binaryTree->insertRightSon($pserie);
            }
        } else {
            $before = $this->binaryTree->getItemCount();
            $this->binaryTree->insertRightSon($pserie);
            if ($before == $this->binaryTree->getItemCount()) {
                $this->binaryTree->insertLeftSon($pserie);
            }
        }
    }

    private function getAllPlayoffSeries()
    {
        $all_series = array();
        $series = StmFactory::find()->PlayoffSerie->findByPlayoff($this->playoff);
        foreach ($series as $serie) {
            $all_series[$serie->getRound()][] = $serie;
        }
        return $all_series;
    }
}
