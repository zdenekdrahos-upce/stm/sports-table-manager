<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition\Playoff\Tree;

use STM\Competition\Playoff\Serie\PlayoffSerie;
use STM\Libs\Generators\Playoff\PlayoffDuelParticipant;

class PlayoffTreeSerie extends PlayoffDuelParticipant
{
    public static function isIdentical($a, $b)
    {
        if ($a->team instanceof PlayoffSerie && $b->team instanceof PlayoffSerie) {
            return in_array($a->team->getIdTeam1(), array($b->team->getIdTeam1(), $b->team->getIdTeam2())) ||
                    in_array($a->team->getIdTeam2(), array($b->team->getIdTeam1(), $b->team->getIdTeam2()));
        }
        return false;
    }
}
