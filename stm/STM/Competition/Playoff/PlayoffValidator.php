<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition\Playoff;

use STM\Helpers\ObjectValidator;
use STM\Web\Message\FormError;

/**
 * Class for validating attributes for database table 'PLAYOFFS'
 * - attributes = serie_win_matches
 */
final class PlayoffValidator
{
    /** @var ObjectValidator */
    private static $validator;

    public static function init($formProcessor = false)
    {
        if (is_null(self::$validator)) {
            self::$validator = new ObjectValidator('\STM\Competition\Playoff\Playoff');
        }
        self::$validator->setFormProcessor($formProcessor);
    }

    public static function setPlayoff($playoff)
    {
        self::$validator->setComparedObject($playoff);
    }

    public static function resetPlayoff()
    {
        self::$validator->resetComparedObject();
    }

    public static function checkCreate($attributes)
    {
        if (self::$validator->checkCreate($attributes, array('serie_win_matches'))) {
            self::checkSerieWinMatches($attributes['serie_win_matches']);
        }
        return self::$validator->isValid();
    }

    public static function checkSerieWinMatches($serie_win_matches)
    {
        self::$validator->checkNumber(
            $serie_win_matches,
            array('min' => 1, 'max' => 50),
            stmLang('competition', 'serie-win-matches')
        );
        if (self::$validator->isValid()) {
            self::$validator->getFormProcessor()->checkCondition(
                $serie_win_matches != self::$validator->getValueFromCompared('getSerieWinMatches'),
                FormError::get('no-change', array(stmLang('competition', 'serie-win-matches')))
            );
        }
        return self::$validator->isValid();
    }
}
