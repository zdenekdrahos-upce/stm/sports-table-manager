<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition\Season\Schedule;

use STM\DB\Object\DatabaseObject;
use STM\DB\SQL\InsertQuery;
use STM\Utils\Dates;
use STM\Competition\Season\Season;

/**
 * SeasonScheduleSaver class
 * - for saving generated schedule of the season
 */
final class SeasonScheduleSaver
{
    /** @var DatabaseObject */
    private static $db;

    public static function setORM(DatabaseObject $dbObject)
    {
        if (is_null(self::$db)) {
            self::$db = $dbObject;
        }
    }

    public static function save($data)
    {
        if (SeasonScheduleValidator::checkGeneratedSchedule($data)) {
            $schedule = new self($data['season']);
            $schedule->loadQueries($data);
            return self::$db->executeQueriesInTransaction($schedule->queries);
        }
        return false;
    }

    /** @var array */
    private $queries;
    /** @var \STM\DB\SQL\InsertQuery */
    private $insertQuery;
    /** @var int */
    private $currentRound;
    /** @var string */
    private $periodStartDate;

    private function __construct(Season $season)
    {
        $this->currentRound = 1;
        $this->queries = array();
        $this->insertQuery = new InsertQuery(DT_MATCHES);
        $this->insertQuery->addAttributes(array('id_competition' => $season->getId()));
    }

    private function loadQueries($data)
    {
        for ($period = 1; $period <= $data['season']->getSeasonPeriods(); $period++) {
            $this->periodStartDate = array_shift($data['dates']);
            foreach ($data['matches'] as $round => $roundMatches) {
                $this->modifyRoundDate($round);
                $this->addMatchesInRound($period, $roundMatches);
            }
        }
    }

    private function modifyRoundDate($round)
    {
        if ($this->periodStartDate) {
            $roundDate = $this->getShiftedRoundDate($round);
            $this->insertQuery->addAttributes(
                array('datetime' => Dates::stringToDatabaseDate($roundDate))
            );
        } else {
            $this->resetRoundDate();
        }
    }

    private function resetRoundDate()
    {
        $this->insertQuery->removeAttribute('datetime');
    }

    private function getShiftedRoundDate($round)
    {
        $round -= 1;
        return $this->periodStartDate . " + {$round} week";
    }

    private function addMatchesInRound($period, $round)
    {
        $this->insertQuery->addAttributes(array('round' => $this->currentRound++));
        foreach ($round as $match) {
            $teams = $this->getTeams($period, $match);
            $this->insertQuery->addAttributes($teams);
            $this->queries[] = clone $this->insertQuery;
        }
    }

    private function getTeams($period, $match)
    {
        $home = $period % 2 == 1 ? $match['h'] : $match['a'];
        $away = $period % 2 == 1 ? $match['a'] : $match['h'];
        return array('id_home' => $home, 'id_away' => $away);
    }
}
