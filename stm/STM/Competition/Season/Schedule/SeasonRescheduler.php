<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition\Season\Schedule;

use STM\DB\Object\DatabaseObject;
use STM\DB\SQL\InsertQuery;
use STM\DB\SQL\UpdateQuery;
use STM\DB\SQL\Condition;
use STM\Utils\Dates;
use STM\StmCache;

/**
 * For updating existing schedule, so existing matches won't be deleted
 * but it will change round.
 */
final class SeasonRescheduler
{
    /** @var DatabaseObject */
    private static $db;

    public static function setORM(DatabaseObject $dbObject)
    {
        if (is_null(self::$db)) {
            self::$db = $dbObject;
        }
    }

    public static function save($data)
    {
        if (SeasonScheduleValidator::checkGeneratedSchedule($data)) {
            $schedule = new SeasonRescheduler($data);
            $schedule->saveScheduleToDb($data);
            return self::$db->executeQueriesInTransaction($schedule->queries);
        }
        return false;
    }

    /** @var array */
    private $queries;
    /** @var Season */
    private $season;
    /** @var int */
    private $currentRound;
    /** @var array */
    private $existingMatches;
    /** @var InsertQuery */
    private $insertQuery;
    /** @var UpdateQuery */
    private $updateQuery;
    /** @var string */
    private $periodStartDate;

    private function __construct($data)
    {
        $this->currentRound = 1;
        $this->queries = array();
        $this->season = $data['season'];
        $this->loadExistingMatches();
        $this->initQueries();
    }

    private function saveScheduleToDb($data)
    {
        for ($period = 1; $period <= $data['season']->getSeasonPeriods(); $period++) {
            $this->periodStartDate = array_shift($data['dates']);
            foreach ($data['matches'] as $round => $roundMatches) {
                $this->modifyRoundDate($round);
                $this->updateRoundInQueries();
                $this->addOrModifyMatchesInRound($period, $roundMatches);
            }
        }
    }

    private function modifyRoundDate($round)
    {
        if ($this->periodStartDate) {
            $roundDate = $this->getShiftedRoundDate($round);
            $datetimeArray = array('datetime' => Dates::stringToDatabaseDate($roundDate));
            $this->updateQuery->addAttributes($datetimeArray);
            $this->insertQuery->addAttributes($datetimeArray);
        } else {
            $this->resetRoundDate();
        }
    }

    private function getShiftedRoundDate($round)
    {
        $round -= 1;
        return $this->periodStartDate . " + {$round} week";
    }

    private function resetRoundDate()
    {
        $this->insertQuery->removeAttribute('datetime');
        $this->updateQuery->addAttributes(array('datetime' => ''));
    }

    private function updateRoundInQueries()
    {
        $this->updateQuery->addAttributes(array('round' => $this->currentRound));
        $this->insertQuery->addAttributes(array('round' => $this->currentRound));
        $this->currentRound++;
    }

    private function addOrModifyMatchesInRound($period, $round)
    {
        foreach ($round as $match) {
            $teams = $this->getTeams($period, $match);
            $idMatch = $this->getExistingMatchId($teams);
            if ($idMatch) {
                $this->updateQuery->setWhere(new Condition('id_match = {I}', array('I' => $idMatch)));
                unset($this->existingMatches[$idMatch]);
                $this->queries[] = clone $this->updateQuery;
            } else {
                $this->insertQuery->addAttributes($teams);
                $this->queries[] = clone $this->insertQuery;
            }
        }
    }

    private function getTeams($period, $match)
    {
        $home = $period % 2 == 1 ? $match['h'] : $match['a'];
        $away = $period % 2 == 1 ? $match['a'] : $match['h'];
        return array('id_home' => $home, 'id_away' => $away);
    }

    private function getExistingMatchId($teams)
    {
        $match = $teams['id_home'] . '-' . $teams['id_away'];
        return array_search($match, $this->existingMatches);
    }

    private function loadExistingMatches()
    {
        $matches = StmCache::getAllCompetitionMatches($this->season);
        $this->existingMatches = array();
        foreach ($matches as $match) {
            $this->existingMatches[$match->getId()] = $match->getIdTeamHome() . '-' . $match->getIdTeamAway();
        }
    }

    private function initQueries()
    {
        $this->insertQuery = new InsertQuery(DT_MATCHES);
        $this->insertQuery->addAttributes(array('id_competition' => $this->season->getId()));
        $this->updateQuery = new UpdateQuery(DT_MATCHES);
    }
}
