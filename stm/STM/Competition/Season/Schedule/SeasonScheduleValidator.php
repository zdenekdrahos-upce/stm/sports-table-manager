<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition\Season\Schedule;

use STM\Helpers\ObjectValidator;
use STM\Competition\Season\Season;
use STM\Utils\Arrays;

/**
 * SeasonScheduleValidator class
 * - for veryfing generated schedule before saving to DB
 */
final class SeasonScheduleValidator
{
    /** @var ObjectValidator */
    private static $validator;

    /** @param \STM\Libs\FormProcessor $formProcessor */
    public static function init($formProcessor = false)
    {
        if (is_null(self::$validator)) {
            self::$validator = new ObjectValidator('\STM\Competition\Season\Season');
        }
        self::$validator->setFormProcessor($formProcessor);
    }

    public static function checkGeneratedSchedule($data)
    {
        if (self::$validator->checkCreate($data, array('season', 'matches', 'dates'))) {
            self::$validator->getFormProcessor()->checkCondition($data['season'] instanceof Season, 'Invalid season');
            self::$validator->getFormProcessor()->checkCondition(is_array($data['matches']), "Matches must be array");
            self::$validator->getFormProcessor()->checkCondition(is_array($data['dates']), "Dates must be array");
            if (self::$validator->isValid()) {
                self::checkNumberOfRounds($data);
                self::checkMatchesInRound($data['matches'], $data['season']->getCountTeams());
                self::checkDates($data['dates'], $data['season']->getSeasonPeriods());
            }
        }
        return self::$validator->isValid();
    }

    private static function checkMatchesInRound($matches, $team_count)
    {
        $matches_in_round = floor($team_count / 2);
        foreach ($matches as $round) {
            if (count($round) != $matches_in_round) {
                self::$validator->getFormProcessor()->addError('Invalid number of matches in round');
                return;
            }
            foreach ($round as $match) {
                if (!Arrays::isArrayValid($match, array('a', 'h'))) {
                    self::$validator->getFormProcessor()->addError('Invalid matches');
                    return;
                }
            }
        }
    }

    private static function checkNumberOfRounds($data)
    {
        $team_count = $data['season']->getCountTeams();
        $round_count = $team_count % 2 == 0 ? $team_count - 1 : $team_count;
        self::$validator->getFormProcessor()->checkCondition(
            $round_count == count($data['matches']),
            'Invalid number of rounds'
        );
    }

    private static function checkDates($dates, $season_periods)
    {
        self::$validator->getFormProcessor()->checkEquality(count($dates), $season_periods, 'Invalid number of dates');
        foreach ($dates as $date) {
            self::$validator->checkOptionalDate($date, stmLang('competition', 'schedule', 'start-date'));
        }
    }
}
