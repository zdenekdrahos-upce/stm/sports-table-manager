<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition\Season;

use STM\DB\SQL\SelectQuery;
use STM\DB\SQL\JoinTable;
use STM\DB\SQL\JoinType;
use STM\DB\SQL\Condition;
use STM\Competition\CompetitionSQL;

class SeasonSQL
{

    /**
     * Selects all seasons, it also selects columns from parent table competitions
     * @return SelectQuery
     */
    public static function selectSeason($idSeason)
    {
        $query = CompetitionSQL::selectCompetitions();
        $query->setColumns('season_periods, point_win, point_win_ot, point_draw, point_loss_ot, point_loss');
        $query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                DT_SEASONS . ' seasons',
                'seasons.id_season = competitions.id_competition'
            )
        );
        $query->setWhere(
            new Condition(
                "id_competition_type = 'S' AND seasons.id_season = {0}",
                array(0 => $idSeason)
            )
        );
        return $query;
    }
}
