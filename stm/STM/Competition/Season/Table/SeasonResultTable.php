<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition\Season\Table;

use STM\Competition\Season\Season;
use STM\Match\Table\ResultTable;
use STM\Match\Table\TablePoints;
use STM\Match\Table\TableRow;
use STM\Match\Table\TableRowHelper;

final class SeasonResultTable extends ResultTable
{
    /** @var Season */
    private static $season;
    /** @var array */
    private static $season_extra_points;
    private $first_run = true;

    /**
     * @param Season $season
     * @param int $max_round
     * @return SeasonResultTable|boolean
     */
    public static function loadTableFromDb($season, $max_round = false)
    {
        if ($season instanceof Season) {
            self::$season = $season;
            self::$season_extra_points = self::$season->getExtraPointsForAllTeams();
            $ms = array(
                'matchType' => \STM\Match\MatchSelection::PLAYED_MATCHES,
                'loadScores' => false,
                'loadPeriods' => false,
                'competition' => $season,
            );
            if (is_int($max_round)) {
                $ms['seasonMaxRound'] = $max_round;
            }
            $arr = $season->toArray();
            return self::getTable($ms, TablePoints::getSeasonPoints($season), $arr['match_periods']);
        }
        return false;
    }

    public static function getTable(array $match_selection, TablePoints $points, $match_periods)
    {
        if (self::canBeInstanceCreated($match_selection, $points, $match_periods)) {
            return new self($match_selection, $points, $match_periods);
        }
        return false;
    }

    protected static function canBeInstanceCreated($match_selection, $points, $match_periods)
    {
        return parent::canBeInstanceCreated($match_selection, $points, $match_periods)
                && self::$season instanceof Season;
    }

    protected function calculatePoints(&$array)
    {
        parent::calculatePoints($array);
        $extra_points = self::$season_extra_points;
        $this->addExtraPointsToExistingsRows($array, $extra_points);
        if ($extra_points) {
            $this->addSeasonTeamsWithoutMatch($array, $extra_points);
        }
        $this->first_run = false;
    }

    private function addExtraPointsToExistingsRows(&$array, &$extra_points)
    {
        foreach ($array as $key => $table_row) {
            if ($this->first_run) {
                $table_row->extraPoints = $extra_points[$table_row->idTeam]['extra'];
                $array[$key] = TableRowHelper::calculatePointsInRow($table_row, $this->points);
            }
            unset($extra_points[$table_row->idTeam]);
        }
    }

    private function addSeasonTeamsWithoutMatch(&$array, &$extra_points)
    {
        foreach ($extra_points as $id_team => $extra) {
            $row = new TableRow();
            $row->idTeam = $id_team;
            $row->name = $extra['name'];
            if ($this->first_run) {
                $row->extraPoints = $extra['extra'];
                $row->points = $extra['extra'];
            }
            $array[] = $row;
        }
    }
}
