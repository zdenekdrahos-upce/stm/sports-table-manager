<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition\Season\Table;

use STM\StmCache;
use STM\Match\Table\TableRow;
use STM\Match\Table\TableRowHelper;
use STM\Match\ExtendedMatch;
use STM\Competition\Season\Season;

/**
 * Rows in full table are ordered by head to head matches
 */
class SeasonTableUpdater
{
    /** @var array */
    private $matches;
    /** @var SeasonTable */
    private $current_table;
    /** @var array */
    private $new_full_table;

    public function __construct(Season $season)
    {
        $this->current_table = StmCache::getSeasonTable($season);
        $this->new_full_table = $this->current_table->getTableFull();
        $this->matches = StmCache::getAllCompetitionMatches($season);
    }

    /**
     * @return SeasonResultTable
     */
    public function getUpdatedResultTable()
    {
        $table = clone $this->current_table;
        $this->orderByHeadToHeadMatches();
        $tables = array(
            'home' => $this->current_table->getTableHome(),
            'away' => $this->current_table->getTableAway(),
            'full' => $this->new_full_table
        );
        $table->unserialize(serialize($tables));
        return $table;
    }

    private function orderByHeadToHeadMatches()
    {
        usort($this->new_full_table, array($this, 'sortRows'));
    }

    private function sortRows(TableRow $a, TableRow $b)
    {
        if ($a->points == $b->points) {
            $points = $this->compareTeams($a->idTeam, $b->idTeam);
            if ($points['points']['A'] != $points['points']['B']) {
                return $points['points']['A'] > $points['points']['B'] ? -1 : 1;
            } elseif ($points['score']['A'] != $points['score']['B']) {
                return $points['score']['A'] > $points['score']['B'] ? -1 : 1;
            }
        }
        return TableRowHelper::compareByPoints($a, $b);
    }

    private function compareTeams($id_team_a, $id_team_b)
    {
        static $points_a = array('W' => 2, 'D' => 1, 'L' => 0, '?' => 0);
        static $points_b = array('W' => 0, 'D' => 1, 'L' => 2, '?' => 0);
        $compare = array(
            'points' => array('A' => 0, 'B' => 0),
            'score' => array('A' => 0, 'B' => 0)
        );
        foreach ($this->matches as $match) {
            if ($this->isHeadToHeadMatch($match, $id_team_a, $id_team_b)) {
                $eMatch = new ExtendedMatch($match, $id_team_a);
                $compare['points']['A'] += $points_a[$eMatch->result];
                $compare['points']['B'] += $points_b[$eMatch->result];
                if ($eMatch->match->getIdTeamHome() == $id_team_a) {
                    $compare['score']['A'] += $eMatch->match->getScoreHome();
                    $compare['score']['B'] += $eMatch->match->getScoreAway();
                } else {
                    $compare['score']['B'] += $eMatch->match->getScoreHome();
                    $compare['score']['A'] += $eMatch->match->getScoreAway();
                }
            }
        }
        return $compare;
    }

    private function isHeadToHeadMatch($match, $id_team_a, $id_team_b)
    {
        $ids = array($match->getIdTeamHome(), $match->getIdTeamAway());
        return in_array($id_team_a, $ids) && in_array($id_team_b, $ids);
    }
}
