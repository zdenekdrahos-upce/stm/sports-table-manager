<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition\Season\Table;

use STM\Competition\Season\Season;
use \Serializable;

final class SeasonTeamsPositions implements Serializable
{
    /** @var Season */
    private $season;
    /** @var array */
    private $teamsPositions;

    public function __construct(Season $season)
    {
        $this->season = $season;
        $this->initTeamsPositions();
        $this->loadTeamsPositions();
        $this->cleanTeamsPositions();
    }

    public function getPositionOfAllTeams()
    {
        return $this->teamsPositions;
    }

    public function getCountTeams()
    {
        return count($this->teamsPositions);
    }

    public function getCountRounds()
    {
        foreach ($this->teamsPositions as $team) {
            return count($team['positions']);
        }
        return 0;
    }

    /**
     * @param numeric $idTeam
     * @return array|false
     * If team is from selected competition then array is returned.
     * Format of array:
     *  'team' => instanceof \STM\Team\Team class
     *  'positions' => array of positions in all season rounds (if no match -> false)
     */
    public function getTeamPositions($idTeam)
    {
        if (is_numeric($idTeam)) {
            foreach ($this->teamsPositions as $id => $team) {
                if ($id == $idTeam) {
                    return $team;
                }
            }
        }
        return array('team' => null, 'positions' => array());
    }

    public function serialize()
    {
        return serialize(array('teamsPositions' => $this->teamsPositions));
    }

    public function unserialize($serialized)
    {
        $content = unserialize($serialized);
        $this->teamsPositions = $content['teamsPositions'];
    }

    private function initTeamsPositions()
    {
        $this->teamsPositions = array();
        foreach ($this->season->getTeams() as $team) {
            $this->teamsPositions[$team->getId()] =
                array('team' => $team, 'positions' => array(), 'last_matches_count' => 0);
        }
    }

    private function loadTeamsPositions()
    {
        for ($i = 1, $max = $this->season->getMaxRound(); $i <= $max; $i++) {
            $result_table = SeasonResultTable::loadTableFromDb($this->season, $i);
            $is_match_played_from_round = $this->isMatchPlayedFromRound($result_table);
            foreach ($result_table->getTableFull() as $position => $row) {
                $this->teamsPositions[$row->idTeam]['positions'][$i] =
                    $is_match_played_from_round ? ($position + 1) : false;
                $this->teamsPositions[$row->idTeam]['last_matches_count'] = $row->matches;
            }
        }
    }

    private function isMatchPlayedFromRound($result_table)
    {
        foreach ($result_table->getTableFull() as $row) {
            if ($row->matches != $this->teamsPositions[$row->idTeam]['last_matches_count']) {
                return true;
            }
        }
        return false;
    }

    private function cleanTeamsPositions()
    {
        foreach ($this->teamsPositions as &$team) {
            unset($team['last_matches_count']);
        }
    }
}
