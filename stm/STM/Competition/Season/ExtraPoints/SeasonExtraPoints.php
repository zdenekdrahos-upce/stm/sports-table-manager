<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition\Season\ExtraPoints;

use STM\DB\Object\DatabaseObject;
use STM\Competition\Season\Season;
use STM\Competition\Season\SeasonValidator;

class SeasonExtraPoints
{
    /** @var DatabaseObject */
    private static $db;

    /** @var Season */
    private $season;

    public static function setORM(DatabaseObject $dbObject)
    {
        if (is_null(self::$db)) {
            self::$db = $dbObject;
        }
    }

    public function __construct(Season $season)
    {
        $this->season = $season;
    }

    public function getTeamExtraPoints($team)
    {
        if ($this->season->isTeamFromCompetition($team)) {
            $query = SeasonExtraPointsSQL::selectTeamExtraPoints($this->season, $team);
            $result = self::$db->select($query, 'array', true);
            return $result['SEASON_EXTRA_POINTS'];
        }
        return false;
    }

    public function getExtraPointsForAllTeams()
    {
        $query = SeasonExtraPointsSQL::selectExtraPointsForTeamsFromSeason($this->season);
        $result = self::$db->select($query, 'array', false);
        $team_extra = array();
        foreach ($result as $values) {
            $team_extra[$values['ID_TEAM']] = array(
                'name' => $values['NAME'],
                'extra' => $values['SEASON_EXTRA_POINTS'],
            );
        }
        return $team_extra;
    }

    public function addExtraPoints($team, $points)
    {
        SeasonValidator::setSeason($this->season);
        if (SeasonValidator::checkExtraPoints($team, $points)) {
            $current_points = $this->getTeamExtraPoints($team);
            $query = SeasonExtraPointsSQL::updateTeamExtraPoints($this->season, $team, $current_points + $points);
            return self::$db->query($query);
        }
        return false;
    }
}
