<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition\Season\ExtraPoints;

use STM\Team\Team;
use STM\Competition\Season\Season;
use STM\DB\SQL\SelectQuery;
use STM\DB\SQL\UpdateQuery;
use STM\DB\SQL\Condition;
use STM\DB\SQL\JoinTable;
use STM\DB\SQL\JoinType;

class SeasonExtraPointsSQL
{
    /** @return SelectQuery */
    public static function selectTeamExtraPoints(Season $season, Team $team)
    {
        $query = new SelectQuery(DT_COMPETITIONS_TEAMS);
        $query->setColumns('SEASON_EXTRA_POINTS');
        $query->setWhere(self::getTeamCompetitionCondition($season, $team));
        return $query;
    }

    /** @return SelectQuery */
    public static function selectExtraPointsForTeamsFromSeason(Season $season)
    {
        $query = new SelectQuery(DT_COMPETITIONS_TEAMS . ' comp_teams');
        $query->setColumns('SEASON_EXTRA_POINTS, teams.ID_TEAM, teams.NAME');
        $query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                DT_TEAMS . ' teams',
                'comp_teams.id_team = teams.id_team'
            )
        );
        $query->setWhere(new Condition('id_competition = {C}', array('C' => $season->getId())));
        return $query;
    }

    /** @return UpdateQuery */
    public static function updateTeamExtraPoints(Season $season, Team $team, $points)
    {
        $query = new UpdateQuery(DT_COMPETITIONS_TEAMS);
        $query->addAttributes(array('season_extra_points' => (int) $points));
        $query->setWhere(self::getTeamCompetitionCondition($season, $team));
        return $query;
    }

    private static function getTeamCompetitionCondition($competition, $team)
    {
        return new Condition(
            'id_competition = {C} and id_team = {T}',
            array('C' => $competition->getId(), 'T' => $team->getId())
        );
    }
}
