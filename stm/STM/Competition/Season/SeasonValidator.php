<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition\Season;

use STM\Helpers\ObjectValidator;
use STM\Web\Message\FormError;

/**
 * Class for validating attributes for database table 'SEASONS'
 * - attributes = season_periods, point_win, point_win_ot, point_draw, point_loss_ot, point_loss
 */
final class SeasonValidator
{
    /** @var ObjectValidator */
    private static $validator;

    /** @param \STM\Libs\FormProcessor $formProcessor */
    public static function init($formProcessor = false)
    {
        if (is_null(self::$validator)) {
            self::$validator = new ObjectValidator('\STM\Competition\Season\Season');
        }
        self::$validator->setFormProcessor($formProcessor);
    }

    /** @param Season $season */
    public static function setSeason($season)
    {
        self::$validator->setComparedObject($season);
    }

    public static function resetSeason()
    {
        self::$validator->resetComparedObject();
    }

    /**
     * @param array $attributes keys: season_periods, point_win, point_win_ot, point_draw, point_loss_ot, point_loss
     * @return boolean
     */
    public static function checkCreate($attributes)
    {
        if (self::$validator->checkCreate($attributes, self::getAttributes())) {
            self::checkFields($attributes);
        }
        return self::$validator->isValid();
    }

    public static function checkUpdate($attributes)
    {
        if (self::$validator->checkUpdate($attributes, self::getAttributes())) {
            self::checkFields($attributes);
            if (self::$validator->isValid()) {
                self::checkChange($attributes);
            }
        }
        return self::$validator->isValid();
    }

    private static function checkFields($attributes)
    {
        self::checkSeasonPeriods($attributes['season_periods']);
        self::checkPointWin($attributes['point_win']);
        self::checkPointWinOt($attributes['point_win_ot']);
        self::checkPointDraw($attributes['point_draw']);
        self::checkPointLossOt($attributes['point_loss_ot']);
        self::checkPointLoss($attributes['point_loss']);
        self::checkPointIntergrity(
            $attributes['point_win'],
            $attributes['point_win_ot'],
            $attributes['point_draw'],
            $attributes['point_loss_ot'],
            $attributes['point_loss']
        );
    }

    public static function checkSeasonPeriods($season_periods)
    {
        return self::$validator->checkNumberAndCompare(
            $season_periods,
            array('min' => 1, 'max' => 20),
            false,
            stmLang('competition', 'periods')
        );
    }

    public static function checkPointWin($point_win)
    {
        return self::checkPoint($point_win, 'win');
    }

    public static function checkPointWinOt($point_win_ot)
    {
        return self::checkPoint($point_win_ot, 'win-ot');
    }

    public static function checkPointDraw($point_draw)
    {
        return self::checkPoint($point_draw, 'draw');
    }

    public static function checkPointLossOt($point_loss_ot)
    {
        return self::checkPoint($point_loss_ot, 'loss-ot');
    }

    public static function checkPointLoss($point_loss)
    {
        return self::checkPoint($point_loss, 'loss');
    }

    public static function checkExtraPoints($team, $points)
    {
        self::$validator->getFormProcessor()->checkCondition(
            self::$validator->isSetComparedObject(),
            'Season is not set'
        );
        $langExtra = stmLang('competition', 'extra', 'added');
        if (self::$validator->checkNumber($points, array('min' => -20, 'max' => 20), $langExtra)) {
            self::$validator->getFormProcessor()->checkCondition(
                $points != 0,
                FormError::get('empty-value', array(stmLang('competition', 'extra', 'added')))
            );
            self::$validator->getFormProcessor()->checkCondition(
                self::$validator->getComparedObject()->isTeamFromCompetition($team),
                'Invalid team or team is not added in season'
            );
            if (self::$validator->isValid()) {
                $current_points = self::$validator->getComparedObject()->getTeamExtraPoints($team);
                self::$validator->getFormProcessor()->checkRange(
                    $points + $current_points,
                    -127,
                    127,
                    FormError::get('number-length', array(stmLang('competition', 'extra', 'current'), -127, 127))
                );
            }
        }
        return self::$validator->isValid();
    }

    private static function checkPoint($point, $name)
    {
        self::$validator->checkNumber($point, array('min' => -20, 'max' => 20), stmLang('competition', $name));
        return self::$validator->isValid();
    }

    private static function checkPointIntergrity($w, $wot, $d, $lot, $l)
    {
        if (!($w >= $wot && $wot >= $d && $d >= $lot && $lot >= $l)) {
            self::$validator->getFormProcessor()->addError(FormError::get('point-integrity'));
        }
    }

    private static function checkChange($attributes)
    {
        $arr = self::$validator->getValueFromCompared('toArray');
        $change = array();
        $change[] = $attributes['season_periods'] != $arr['season_periods'];
        $change[] = $attributes['point_win'] != $arr['points']['win'];
        $change[] = $attributes['point_win_ot'] != $arr['points']['win_overtime'];
        $change[] = $attributes['point_draw'] != $arr['points']['draw'];
        $change[] = $attributes['point_loss_ot'] != $arr['points']['loss_overtime'];
        $change[] = $attributes['point_loss'] != $arr['points']['loss'];
        self::$validator->getFormProcessor()->checkCondition(
            in_array(true, $change),
            FormError::get('no-change', array(stmLang('competition', 'season')))
        );
    }

    private static function getAttributes()
    {
        return array('season_periods', 'point_win', 'point_win_ot', 'point_draw', 'point_loss_ot', 'point_loss');
    }
}
