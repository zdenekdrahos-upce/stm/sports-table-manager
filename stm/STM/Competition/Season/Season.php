<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition\Season;

use STM\DB\Object\DatabaseObject;
use STM\Competition\Competition;
use STM\Utils\Arrays;
use STM\Competition\Season\ExtraPoints\SeasonExtraPoints;

/**
 * Season class
 * - instance: represents row from database table 'SEASONS' and 'COMPETITIONS'
 * - static methods: for finding seasons, creating new one
 */
class Season extends Competition
{
    /** @var DatabaseObject */
    private static $dbSeason;

    // table fields
    /** @var int */
    private $season_periods;
    /** @var int */
    private $point_win;
    /** @var int */
    private $point_win_ot;
    /** @var int */
    private $point_draw;
    /** @var int */
    private $point_loss;
    /** @var int */
    private $point_loss_ot;

    /** @var SeasonExtraPoints */
    private $extraPoints;

    protected function __construct()
    {
        parent::__construct();
        $this->extraPoints = new SeasonExtraPoints($this);
    }

    public static function setORM(DatabaseObject $dbObject)
    {
        if (is_null(self::$dbSeason)) {
            self::$dbSeason = $dbObject;
            SeasonExtraPoints::setORM($dbObject);
        }
    }

    /**
     * Finds Season specified by id.
     * @param int $id
     * @return Season/false
     */
    public static function findById($id)
    {
        if (is_numeric($id)) {
            $query = SeasonSQL::selectSeason($id);
            return self::$dbSeason->select($query, 'object', true);
        }
        return false;
    }

    /**
     * Creates new Season - row in DB table SEASONS and COMPETITIONS
     * @param array $attributes
     * @return Competition
     * If creating was successfull then returns instance of parent class Competition,
     * not instance of Season.
     */
    public static function create($attributes)
    {
        if (Arrays::isArrayValid($attributes, array('season', 'competition'))) {
            if (SeasonValidator::checkCreate($attributes['season'])) {
                $attributes['competition']['id_competition_type'] = 'S';
                $idCompetition = parent::create($attributes['competition'], 'season');
                if ($idCompetition) {
                    $attributes['season']['id_season'] = $idCompetition;
                    if (self::$dbSeason->insertValues($attributes['season'])) {
                        return $idCompetition;
                    } else {
                        parent::deleteCompetition($idCompetition);
                    }
                }
            }
        }
        return false;
    }


    // INSTANCE METHODS FOR ONE SEASON
    public function updateSeason($attributes)
    {
        SeasonValidator::setSeason($this);
        if (SeasonValidator::checkUpdate($attributes)) {
            return self::$dbSeason->updateById($this->id_competition, $attributes);
        }
        return false;
    }

    // EXTRA POINTS - maximum = <-127, 127>
    /**
     * Finds team extra points
     * @param \STM\Team\Team $team
     * @return int/false
     * Returns false if team is not participated is the season.
     */
    public function getTeamExtraPoints($team)
    {
        return $this->extraPoints->getTeamExtraPoints($team);
    }

    public function getExtraPointsForAllTeams()
    {
        return $this->extraPoints->getExtraPointsForAllTeams();
    }

    /**
     * Adds (+,-) extra points (at one moment min -20 and max 20 points) to team in season.
     * @param \STM\Team\Team $team
     * @param int $points
     * @return boolean
     */
    public function addExtraPoints($team, $points)
    {
        return $this->extraPoints->addExtraPoints($team, $points);
    }


    // GETTERS
    /** @return int */
    public function getSeasonPeriods()
    {
        return (int) $this->season_periods;
    }

    /** @return int */
    public function getPointWin()
    {
        return (int) $this->point_win;
    }

    /** @return int */
    public function getPointWinOvertime()
    {
        return (int) $this->point_win_ot;
    }

    /** @return int */
    public function getPointDraw()
    {
        return (int) $this->point_draw;
    }

    /** @return int */
    public function getPointLossOvertime()
    {
        return (int) $this->point_loss_ot;
    }

    /** @return int */
    public function getPointLoss()
    {
        return (int) $this->point_loss;
    }

    /**
     * Finds the highest possible number of round (depends on number of teams
     * in season + number of season periods)
     * @return int
     */
    public function getMaxRound()
    {
        $team_count = $this->getCountTeams();
        $round_count = $team_count % 2 == 0 ? $team_count - 1 : $team_count;
        return $this->getSeasonPeriods() * $round_count;
    }

    /**
     * Finds how many matches must be played in season before the season is played.
     * @return int
     */
    public function getTotalNumberOfMatches()
    {
        $team_count = $this->getCountTeams();
        $matches_in_period = ($team_count * ($team_count - 1)) / 2;
        return $matches_in_period * $this->season_periods;
    }

    /**
     * Method for templating. Returns season as an array
     * @return array
     */
    public function toArray()
    {
        $season_array = array(
            'season_periods' => $this->getSeasonPeriods(),
            'points' => array(
                'win' => $this->getPointWin(),
                'win_overtime' => $this->getPointWinOvertime(),
                'draw' => $this->getPointDraw(),
                'loss_overtime' => $this->getPointLossOvertime(),
                'loss' => $this->getPointLoss(),
            )
        );
        return array_merge(parent::toArray(), $season_array);
    }
}
