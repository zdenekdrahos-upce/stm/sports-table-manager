<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition\Season\Rounds;

use STM\DB\Object\DatabaseObject;
use STM\Competition\Season\Season;
use STM\Match\Match;
use STM\Match\MatchValidator;
use STM\Utils\Dates;

final class SeasonRounds
{
    /** @var DatabaseObject */
    private static $db;

    public static function setORM(DatabaseObject $dbObject)
    {
        if (is_null(self::$db)) {
            self::$db = $dbObject;
        }
    }

    /** @var array */
    private $rounds;
    /** @var Season */
    private $season;

    public function __construct(Season $season)
    {
        $this->rounds = array();
        $this->season = $season;
    }

    public function deleteMatchesFromRound($round)
    {
        $msArray = $this->getMatchSelection($round);
        $builder = new \STM\Match\MatchSelectionBuilder();
        $ms = $builder->build($msArray);
        return Match::deleteMatches($ms);
    }

    public function updateMatchesDateInRound($round, $date)
    {
        if ($this->isValidRound($round) && MatchValidator::checkDate($date, $this->season)) {
            $attributes = array('datetime' => Dates::stringToDatabaseDate($date));
            $condition = SeasonRoundsSQL::getUpdateCondition($this->season, $round);
            return self::$db->updateByCondition($condition, $attributes);
        }
        return false;
    }

    public function getMatchSelection($round)
    {
        if ($this->isValidRound($round)) {
            return array(
                'matchType' => \STM\Match\MatchSelection::ALL_MATCHES,
                'loadScores' => true,
                'loadPeriods' => true,
                'competition' => $this->season,
                'seasonRound' => $round,
            );
        }
        return false;
    }

    private function isValidRound($round)
    {
        return $round > 0 && $round <= $this->season->getMaxRound();
    }

    public function getRounds()
    {
        if (empty($this->rounds)) {
            $this->initRounds();
            $this->loadRounds();
        }
        return $this->rounds;
    }

    private function initRounds()
    {
        for ($i = 1; $i <= $this->season->getMaxRound(); $i++) {
            $row = new SeasonRoundRow();
            $row->round = $i;
            $row->allMatches = 0;
            $row->playedMatches = 0;
            $this->rounds[$i] = $row;
        }
    }

    private function loadRounds()
    {
        $query = SeasonRoundsSQL::selectSeasonRounds($this->season);
        $rounds = self::$db->select($query);
        foreach ($rounds as $round) {
            if (array_key_exists($round->round, $this->rounds)) {
                $this->rounds[$round->round] = $round;
            }
        }
    }
}
