<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition\Season\Rounds;

use STM\DB\SQL\SelectQuery;
use STM\DB\SQL\Condition;
use STM\DB\SQL\JoinTable;
use STM\DB\SQL\JoinType;
use STM\Competition\Season\Season;

class SeasonRoundsSQL
{

    /** @return SelectQuery */
    public static function selectSeasonRounds(Season $season)
    {
        $query = new SelectQuery(DT_MATCHES . ' matches');
        $query->setColumns(
            'matches.round, count(distinct matches.id_match) as allMatches,
             count(distinct match_periods.id_match) as playedMatches'
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::LEFT,
                DT_MATCH_PERIODS . ' match_periods',
                'matches.id_match = match_periods.id_match'
            )
        );
        $query->setGroupBy('matches.round');
        $query->setWhere(new Condition('matches.id_competition = {S}', array('S' => $season->getId())));
        return $query;
    }

    /** @return STM\DB\SQL\Condition */
    public static function getUpdateCondition(Season $season, $round)
    {
        return new Condition(
            'round = {R} AND id_competition = {S}',
            array('R' => $round, 'S' => $season->getId())
        );
    }
}
