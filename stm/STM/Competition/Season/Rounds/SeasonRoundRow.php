<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition\Season\Rounds;

final class SeasonRoundRow
{
    /** @var int */
    public $round;
    /** @var int */
    public $allMatches;
    /** @var int */
    public $playedMatches;

    public function __construct()
    {
        $this->typeAttributes();
    }

    private function typeAttributes()
    {
        foreach (get_object_vars($this) as $key => $value) {
            $this->$key = empty($value) ? 0 : (int) $value;
        }
    }
}
