<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition;

use STM\Utils\Classes;

/**
 * CompetitionType class
 * - represents database table 'COMPETITION_TYPES'
 * - used for simpler work with primary keys (null, S, P) from table
 */
class CompetitionType
{
    /**
     * Basic competition (collection of matches), no dependencies between matches
     */
    const COMPETITION = 'Competition';
    /**
     * Season competition, round robin, in each round number of matches = count(teams)/2
     */
    const SEASON = 'Season';
    /**
     * Playoff, single elimination game system, series
     */
    const PLAYOFF = 'Playoff';

    private function __construct()
    {
    }

    /**
     * Finds name of competition type specified by primary key from DB table Competition_types
     * @param string/null $id_type valid = null, S, P
     * @return string
     */
    public static function getCompetitionType($id_type)
    {
        if (is_string($id_type) || is_null($id_type)) {
            switch ($id_type) {
                case 'X':
                    return self::COMPETITION;
                case 'S':
                    return self::SEASON;
                case 'P':
                    return self::PLAYOFF;
            }
        }
        return 'Invalid Type ID';
    }

    public static function getTypeId($constant)
    {
        if (Classes::isClassConstant('\STM\Competition\CompetitionType', $constant)) {
            switch ($constant) {
                case self::COMPETITION:
                    return 'X';
                case self::SEASON:
                    return 'S';
                case self::PLAYOFF:
                    return 'P';
            }
        }
        return false;
    }
}
