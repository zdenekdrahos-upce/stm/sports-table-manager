<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition;

use STM\Team\Team;
use STM\Competition\Category\CompetitionCategory;
use STM\Competition\CompetitionStatus;
use STM\User\UserGroup;
use STM\User\User;
use STM\Utils\Classes;
use STM\Entities\AbstractEntitySelection;

/**
 * All queries generated from instances of this class loads only basic competition
 * attributes (id_competition, name, match_periods, date_start, date_end, id_score_type,
 * id_competition_type, id_category, id_status, categoryName). Attributes like
 * serie_win_matches for playoff or points for season are NOT loaded.
 */
class CompetitionSelection extends AbstractEntitySelection
{

    public function setId($idCompetition)
    {
        if (is_numeric($idCompetition)) {
            $this->selection->setAttributeWithValue('competitions.id_competition', (string) $idCompetition);
        }
    }

    /**
     * @param int $maxRows
     * @param int $offset
     */
    public function setLimit($maxRows, $offset = 0)
    {
        $this->selection->setRowsLimit($maxRows, $offset);
    }

    /** @param \STM\Competition\CompetitionType $type constant */
    public function setCompetitionType($type)
    {
        if (Classes::isClassConstant('\STM\Competition\CompetitionType', $type)) {
            $this->selection->setAttributeWithValue('id_competition_type', CompetitionType::getTypeId($type));
        }
    }

    /** @param Team $team */
    public function setTeam(Team $team)
    {
        $joinTable = CompetitionSQL::getJoinCompetitionsTeamsTable();
        $this->query->setJoinTables($joinTable);
        $this->selection->setAttributeWithValue('teams.id_team', (string) $team->getId());
    }

    /** @param User $user */
    public function setCompetitionManager(User $user)
    {
        if ($user->getUserGroup() == UserGroup::COMPETITION_MANAGER) {
            $joinTable = CompetitionSQL::getJoinCompetitionsUsersTable();
            $this->query->setJoinTables($joinTable);
            $this->selection->setAttributeWithValue('users.id_user', (string) $user->getId());
        }
    }

    /** @param \STM\Competition\Category\CompetitionCategory $category */
    public function setCategory(CompetitionCategory $category)
    {
        $this->selection->setAttributeWithValue('competitions.id_category', (string) $category->getId());
    }

    /** @param numeric $category_id */
    public function setIdCategory($category_id)
    {
        if (is_numeric($category_id)) {
            $this->selection->setAttributeWithValue('competitions.id_category', (string) $category_id);
        }
    }

    /** @param array $array constant */
    public function setCompetitionStatuses(array $array)
    {
        $ids = array();
        foreach ($array as $status) {
            if (Classes::isClassConstant('\STM\Competition\CompetitionStatus', $status)) {
                $ids[] = CompetitionStatus::getStatusId($status);
            }
        }
        $this->selection->setAttributesInMultipleColumns(array('id_status'), $ids, 'AND');
    }

    public function setDate($date)
    {
        $this->selection->setDateBetweenAttributes($date, 'competitions.date_start', 'competitions.date_end');
    }

    protected function getQuery()
    {
        return CompetitionSQL::selectCompetitions();
    }
}
