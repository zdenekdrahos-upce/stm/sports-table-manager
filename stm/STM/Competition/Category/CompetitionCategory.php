<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition\Category;

use STM\DB\Object\DatabaseObject;

/**
 * CompetitionCategory class
 * - instance: represents row from database table 'COMPETITIONS_CATEGORIES' +
 *             number of child categories and number of competition which are in category
 */
final class CompetitionCategory
{
    /** @var DatabaseObject */
    private static $db;

    /** @var int */
    private $id_category;
    /** @var string */
    private $name;
    /** @var string */
    private $description;
    /** @var int */
    private $id_parent_category;
    /** @var int */
    private $count_childs;
    /** @var int */
    private $count_competitions;

    private function __construct()
    {
    }

    public static function setORM(DatabaseObject $dbObject)
    {
        if (is_null(self::$db)) {
            self::$db = $dbObject;
        }
    }

    /**
     * Creates new row in in database table 'competition_categories'
     * @param array $attributes
     * @return int/boolean
     * Returns true if creating was successful, otherwise false
     */
    public static function create($attributes)
    {
        if (CompetitionCategoryValidator::checkCreate($attributes)) {
            return self::$db->insertValues($attributes, true);
        }
        return false;
    }

    /**
     * Checks if name exists as competition category name in database. Method is case-INsensitive.
     * @param string $name
     * @return boolean
     */
    public static function exists($category_name)
    {
        return self::$db->existsItem('name', $category_name);
    }


    // INSTANCE METHODS FOR ONE COMPETITION CATEGORY
    public function delete()
    {
        return self::$db->deleteById($this->id_category);
    }

    public function update($attributes)
    {
        CompetitionCategoryValidator::setCategory($this);
        if (CompetitionCategoryValidator::checkUpdate($attributes)) {
            return self::$db->updateById($this->id_category, $attributes);
        }
        return false;
    }

    /** @return int */
    public function getId()
    {
        return (int) $this->id_category;
    }

    /** @return string */
    public function getName()
    {
        return $this->name;
    }

    /** @return string */
    public function getDescription()
    {
        return $this->description;
    }

    /** @return int/null */
    public function getIdParent()
    {
        return !is_null($this->id_parent_category) ? (int) $this->id_parent_category : null;
    }

    /** @return int */
    public function getCountChilds()
    {
        return (int) $this->count_childs;
    }

    /** @return int */
    public function getCountCompetitions()
    {
        return (int) $this->count_competitions;
    }

    /**
     * Method for templating. Returns category as an array
     * @return array
     */
    public function toArray()
    {
        return array(
            'name' => $this->name,
            'description' => $this->description,
            'id_parent_category' => $this->getIdParent(),
            'count_childs' => $this->getCountChilds(),
            'count_competitions' => $this->getCountCompetitions()
        );
    }

    /** @return string */
    public function __toString()
    {
        return $this->name;
    }
}
