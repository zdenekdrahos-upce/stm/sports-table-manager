<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition\Category;

use STM\Entities\AbstractEntityFactory;

class CompetitionCategoryFactory extends AbstractEntityFactory
{
    public function findById($id)
    {
        return $this->entityHelper->findById($id);
    }

    public function findSubcategories(CompetitionCategory $category)
    {
        $methods = array('setParentCategory' => $category);
        return $this->entityHelper->setAndFindArray($methods);
    }

    public function findAll()
    {
        return $this->entityHelper->findAll();
    }

    protected function getEntitySelection()
    {
         return new CompetitionCategorySelection();
    }

    protected function getEntityClass()
    {
        return 'STM\Competition\Category\CompetitionCategory';
    }
}
