<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition\Category;

use STM\DB\Database;
use STM\DB\SQL\SelectQuery;
use STM\DB\SQL\JoinTable;
use STM\DB\SQL\JoinType;

class CompetitionCategorySQL
{

    /**
     * Selects base information about competition category. Selected attributes are
     * same as instance attributes in class CompetitionCategory
     * @return SelectQuery
     */
    public static function selectCompetitionCategories()
    {
        $query = new SelectQuery(DT_COMPETITION_CATEGORIES . ' categories');
        $query->setColumns(
            'categories.id_category, categories.name, categories.description,
             categories.id_parent_category, sub_count.count_childs,
             count(competitions.id_competition) as count_competitions'
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                '(' . self::selectCountSubcategories()->generate() . ')sub_count',
                'categories.id_category = sub_count.id_category'
            )
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::LEFT,
                DT_COMPETITIONS . ' competitions',
                'categories.id_category = competitions.id_category'
            )
        );
        $query->setGroupBy(
            'categories.id_category, categories.name, categories.description,
             categories.id_parent_category, sub_count.count_childs'
        );
        $query->setOrderBy('count_childs desc, count(*) desc, categories.name');
        return $query;
    }

    /**
     * Selects id_category and count_childs which is number of child categories
     * for id_category
     * @return SelectQuery
     */
    private static function selectCountSubcategories()
    {
        $query = new SelectQuery(DT_COMPETITION_CATEGORIES . ' categories');
        $query->setColumns('categories.id_category, count(childs.id_category) as count_childs');
        $query->setJoinTables(
            new JoinTable(
                JoinType::LEFT,
                DT_COMPETITION_CATEGORIES . ' childs',
                'categories.id_category = childs.id_parent_category'
            )
        );
        $query->setGroupBy('categories.id_category');
        Database::escapeQuery($query);
        return $query;
    }
}
