<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition\Category;

use STM\Entities\AbstractEntitySelection;

class CompetitionCategorySelection extends AbstractEntitySelection
{

    public function setId($idSport)
    {
        if (is_numeric($idSport)) {
            $this->selection->setAttributeWithValue('categories.id_category', (string) $idSport);
        }
    }

    public function setParentCategory(CompetitionCategory $category)
    {
        $this->selection->setAttributeWithValue('categories.id_parent_category', (string) $category->getId());
    }

    protected function getQuery()
    {
        return CompetitionCategorySQL::selectCompetitionCategories();
    }
}
