<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition\Category;

use STM\Helpers\ObjectValidator;
use STM\Web\Message\FormError;
use STM\StmFactory;

/**
 * Class for validating attributes for database table 'COMPETITIONS_CATEGORIES'
 * - attributes = name, description, id_parent_category
 */
final class CompetitionCategoryValidator
{
    /** @var ObjectValidator */
    private static $validator;

    /**
     * @param \STM\Libs\FormProcessor $formProcessor
     */
    public static function init($formProcessor = false)
    {
        if (is_null(self::$validator)) {
            self::$validator = new ObjectValidator('\STM\Competition\Category\CompetitionCategory');
        }
        self::$validator->setFormProcessor($formProcessor);
    }

    /**
     * Sets instance of the CompetitionCategory class. Instance is used for checking if
     * new value of attribute is different from current value in category
     * @param CompetitionCategory $category
     */
    public static function setCategory($category)
    {
        self::$validator->setComparedObject($category);
    }

    /**
     * Removes CompetitionCategory, so attributes are not compared with competition category attributes
     */
    public static function resetCategory()
    {
        self::$validator->resetComparedObject();
    }

    /**
     * Checks if array $attributes can be used as values for new competition category in database.
     * @param array $attributes keys: name, description, id_parent_category
     * @return boolean
     */
    public static function checkCreate($attributes)
    {
        if (self::$validator->checkCreate($attributes, array('name', 'description', 'id_parent_category'))) {
            self::checkName($attributes['name']);
            self::checkDescription($attributes['description']);
            self::checkParentCategory($attributes['id_parent_category']);
        }
        return self::$validator->isValid();
    }

    public static function checkUpdate($attributes)
    {
        if (self::$validator->checkUpdate($attributes, array('name', 'description', 'id_parent_category'))) {
            if ($attributes['name'] != self::$validator->getValueFromCompared('getName')) {
                self::checkName($attributes['name']);
            }
            self::checkDescription($attributes['description']);
            self::checkParentCategory($attributes['id_parent_category']);
            self::checkChange($attributes);
        }
        return self::$validator->isValid();
    }

    /**
     * Checks if $name is filled and string and length is from interval <3, 50>
     * @param string $name
     * @return boolean
     */
    public static function checkName($name)
    {
        return self::$validator->checkUniqueName($name, array('min' => 3, 'max' => 50), false);
    }

    /**
     * Checks if $description is filled and string and length is from interval <5, 200>
     * @param string $description
     * @return boolean
     */
    public static function checkDescription($description)
    {
        return self::$validator->checkString(
            $description,
            array('min' => 5, 'max' => 200),
            stmLang('category', 'description'),
            false
        );
    }

    /**
     * Checks if id_parent is null (and if it was changed) or if id_parent is
     * id of existing category which is different from updated category
     * @param int/id $id_parent
     * @return boolean
     */
    public static function checkParentCategory($id_parent)
    {
        self::$validator->getFormProcessor()->checkCondition(
            empty($id_parent) || is_numeric($id_parent),
            'Parent category must be empty or id of existing category'
        );
        if (!empty($id_parent)) {
            self::$validator->getFormProcessor()->checkCondition(
                StmFactory::find()->CompetitionCategory->findById($id_parent) instanceof CompetitionCategory,
                'Please insert valid parent category'
            );
        }
        if (self::$validator->isSetComparedObject() && self::$validator->isValid()) {
            self::$validator->getFormProcessor()->checkCondition(
                $id_parent != self::$validator->getValueFromCompared('getId'),
                FormError::get('category-parent')
            );
        }
        return self::$validator->isValid();
    }

    private static function checkChange($attributes)
    {
        $new_name = $attributes['name'] != self::$validator->getValueFromCompared('getName');
        $new_description = $attributes['description'] != self::$validator->getValueFromCompared('getDescription');
        $new_parent = $attributes['id_parent_category'] != self::$validator->getValueFromCompared('getIdParent');
        $is_different = $new_name || $new_description || $new_parent;
        self::$validator->getFormProcessor()->checkCondition(
            $is_different,
            FormError::get('no-change', array(stmLang('category', 'category')))
        );
    }
}
