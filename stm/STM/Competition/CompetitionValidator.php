<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition;

use STM\Helpers\ObjectValidator;
use STM\Web\Message\FormError;
use STM\Team\Team;
use STM\Competition\Category\CompetitionCategory;
use STM\Utils\Dates;
use STM\StmFactory;

/**
 * Class for validating attributes for database table 'COMPETITIONS'
 * - attributes = name, match_periods, date_start, date_end, id_score_type, id_competition_type
 */
final class CompetitionValidator
{
    /** @var ObjectValidator */
    private static $validator;

    /**
     * @param \STM\Libs\FormProcessor $formProcessor
     */
    public static function init($formProcessor = false)
    {
        if (is_null(self::$validator)) {
            self::$validator = new ObjectValidator('\STM\Competition\Competition');
        }
        self::$validator->setFormProcessor($formProcessor);
    }

    /**
     * Sets instance of the Competition class. Instance is used for checking if
     * new value of attribute is different from current value in competition.
     * @param \STM\Competition\Competition $competition
     */
    public static function setCompetition($competition)
    {
        self::$validator->setComparedObject($competition);
    }

    /**
     * Removes competition, so attributes are not compared with competition attributes
     */
    public static function resetCompetition()
    {
        self::$validator->resetComparedObject();
    }

    /**
     * Checks if array $attributes can be used as values for new competition in database.
     * @param array $attributes keys: name, match_periods, date_start, date_end, id_score_type, id_competition_type
     * @return boolean
     */
    public static function checkCreate($attributes)
    {
        if (self::$validator->checkCreate($attributes, self::getAttributes())) {
            self::checkFields($attributes);
        }
        return self::$validator->isValid();
    }

    public static function checkUpdate($attributes)
    {
        if (self::$validator->checkUpdate($attributes, self::getAttributes())) {
            self::checkFields($attributes);
            if (self::$validator->isValid()) {
                self::checkChange($attributes);
            }
        }
        return self::$validator->isValid();
    }

    private static function checkFields($attributes)
    {
        self::checkName($attributes['name']);
        self::checkMatchPeriods($attributes['match_periods']);
        self::checkScoreType($attributes['id_score_type']);
        self::checkCompetitionType($attributes['id_competition_type']);
        self::checkStatus($attributes['id_status']);
        self::checkDateStart($attributes['date_start']);
        self::checkDateEnd($attributes['date_end']);
        self::checkCategory($attributes['id_category']);
        if (self::$validator->isValid()) {
            self::compareDates($attributes['date_start'], $attributes['date_end']);
        }
    }

    /**
     * Checks if $new_score_type is F or T
     * @param string $new_score_type
     * @return boolean
     * Returns true if $new_score_type can be written into the database
     */
    public static function checkScoreType($new_score_type)
    {
        self::$validator->getFormProcessor()->checkCondition(
            in_array($new_score_type, array('F', 'T')),
            'Score type must be Football or Tennis'
        );
        return self::$validator->isValid();
    }

        /**
     * Checks if $id_competition_type is null, S or F
     * @param string $id_status
     * @return boolean
     */
    public static function checkStatus($id_status)
    {
        self::$validator->getFormProcessor()->checkCondition(
            in_array($id_status, array('P', 'V', 'H')),
            'Competition status must be Public, Visible or Hidden'
        );
        return self::$validator->isValid();
    }

    /**
     * Checks if $new_match_periods is number from interval <1, 10>
     * @param numeric $new_match_periods
     * @return boolean
     * Returns true if $new_match_periods can be written into the database
     */
    public static function checkMatchPeriods($new_match_periods)
    {
        $langError = stmLang('competition', 'min-match-periods');
        return self::$validator->checkNumber($new_match_periods, array('min' => 1, 'max' => 10), $langError);
    }

    /**
     * Checks if $new_name is filled and string and length is from interval <4, 50>
     * @param string $new_name
     * @return boolean
     */
    public static function checkName($new_name)
    {
        if (self::$validator->isSetComparedObject()
            && $new_name == self::$validator->getValueFromCompared('__toString')) {
            return true;
        }
        return self::$validator->checkUniqueName($new_name, array('min' => 4, 'max' => 50), false);
    }

    /**
     * Checks if $new_date_start is string containing date
     * @param string $new_date_start
     * @return boolean
     */
    public static function checkDateStart($new_date_start)
    {
        return self::checkDate($new_date_start, 'date-start');
    }

    /**
     * Checks if $new_date_end is string containing date
     * @param string $new_date_end
     * @return boolean
     */
    public static function checkDateEnd($new_date_end)
    {
        return self::checkDate($new_date_end, 'date-end');
    }

    /**
     * Checks if $id_category is existing category (different from current category) or empty value
     * @param int $id_category
     * @return boolean
     */
    public static function checkCategory($id_category)
    {
        if (!empty($id_category)) {
            self::$validator->getFormProcessor()->checkCondition(
                StmFactory::find()->CompetitionCategory->findById($id_category) instanceof CompetitionCategory,
                'Please insert valid category'
            );
        }
        return self::$validator->isValid();
    }

    public static function checkAddedTeam($team)
    {
        self::$validator->getFormProcessor()->checkCondition(
            self::$validator->isSetComparedObject(),
            'Updated competition must be set'
        );
        if (self::$validator->isValid()) {
            $competition = self::$validator->getComparedObject();
            self::$validator->getFormProcessor()->checkCondition($team instanceof Team, "Invalid team");
            self::$validator->getFormProcessor()->checkCondition(
                !$competition->isTeamFromCompetition($team),
                FormError::get('unique-value', array(stmLang('competition', 'team')))
            );
        }
        return self::$validator->isValid();
    }

    public static function checkCompetitionForAddTeams($competition)
    {
        self::$validator->getFormProcessor()->checkCondition(
            self::$validator->isSetComparedObject(),
            'Updated competition must be set'
        );
        if (self::$validator->isValid()) {
            self::$validator->getFormProcessor()->checkCondition(
                $competition instanceof Competition,
                "Invalid competition"
            );
            if (self::$validator->isValid()) {
                self::$validator->getFormProcessor()->checkCondition(
                    $competition->getCountTeams() > 0,
                    FormError::get('no-teams')
                );
                self::$validator->getFormProcessor()->checkCondition(
                    $competition->getId() != self::$validator->getValueFromCompared('getId'),
                    FormError::get('no-change', array(stmLang('competition', 'competition')))
                );
            }
        }
        return self::$validator->isValid();
    }

    /**
     * Checks date (start/end)
     * @param string $new_date
     * @param string $translator_name name in error message
     * @return boolean
     */
    private static function checkDate($new_date, $translator_name = 'Date')
    {
        $name = stmLang('competition', $translator_name);
        self::$validator->checkOptionalDate($new_date, $name);
        if (self::$validator->isValid() && self::$validator->isSetComparedObject()) {
            if ($translator_name == 'date-start') {
                $date_end = self::$validator->getValueFromCompared('getEndStart');
                self::compareDates($new_date, $date_end->__toString());
            } elseif ($translator_name == 'date-end') {
                $date_start = self::$validator->getValueFromCompared('getDateStart');
                self::compareDates($date_start->__toString(), $new_date);
            }
        }
        return self::$validator->isValid();
    }

    /**
     * Checks if $id_competition_type is null, S or F
     * @param string $id_competition_type
     * @return boolean
     */
    private static function checkCompetitionType($id_competition_type)
    {
        self::$validator->getFormProcessor()->checkCondition(
            in_array($id_competition_type, array('X', 'S', 'P')),
            'Competition type must be Season, Playoff or nothing'
        );
        return self::$validator->isValid();
    }

    /**
     * Checks if $start_date is same or smaller than $end_date. Comparision is
     * performed if both dates are non empty
     * @param string $start_date
     * @param string $end_date
     */
    private static function compareDates($start_date, $end_date)
    {
        if (!empty($start_date) && !empty($end_date)) {
            self::$validator->getFormProcessor()->checkCondition(
                Dates::areDatesInOrder($start_date, $end_date),
                FormError::get('date-integrity')
            );
        }
    }

    private static function checkChange($attributes)
    {
        $arr = self::$validator->getValueFromCompared('toArray');
        $is_different = false;
        foreach ($attributes as $key => $value) {
            if ($key == 'id_competition_type') {
                continue;
            } elseif ($key == 'id_score_type') {
                $change = $value != self::$validator->getValueFromCompared('getScoreType');
            } elseif ($key == 'id_status') {
                $change = $value != self::$validator->getValueFromCompared('getStatusId');
            } else {
                $change = $value != $arr[$key];
            }
            if ($change) {
                $is_different = true;
                break;
            }
        }
        self::$validator->getFormProcessor()->checkCondition(
            $is_different,
            FormError::get('no-change', array(stmLang('competition', 'competition')))
        );
    }

    private static function getAttributes()
    {
        return array(
            'name', 'match_periods', 'date_start', 'date_end', 'id_score_type',
            'id_competition_type', 'id_category', 'id_status'
        );
    }
}
