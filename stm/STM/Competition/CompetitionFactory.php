<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Competition;

use STM\Team\Team;
use STM\User\User;
use STM\User\UserGroup;
use STM\Competition\Season\Season;
use STM\Competition\Playoff\Playoff;
use STM\Competition\Category\CompetitionCategory;
use STM\Entities\AbstractEntityFactory;

class CompetitionFactory extends AbstractEntityFactory
{
    public function findById($id)
    {
        $competition = $this->entityHelper->findById($id);
        if ($competition instanceof Competition) {
            if ($competition->getIdCompetitionType() == 'S') {
                return Season::findById($id);
            } elseif ($competition->getIdCompetitionType() == 'P') {
                return Playoff::findById($id);
            } else {
                return $competition;
            }
        }
        return false;
    }

    public function findByTeam(Team $team)
    {
        $methods = array('setTeam' => $team);
        return $this->entityHelper->setAndFindArray($methods);
    }

    public function findByCategory($category)
    {
        $method = $category instanceof CompetitionCategory ? 'setCategory' : 'setIdCategory';
        $methods = array($method => $category);
        return $this->entityHelper->setAndFindArray($methods);
    }

    public function findByType($type)
    {
        $methods = array('setCompetitionType' => $type);
        return $this->entityHelper->setAndFindArray($methods);
    }

    public function findManagedCompetitions(User $user)
    {
        if ($user->getUserGroup() == UserGroup::COMPETITION_MANAGER) {
            $methods = array('setCompetitionManager' => $user);
            return $this->entityHelper->setAndFindArray($methods);
        } else {
            return array();
        }
    }

    public function findAll()
    {
        return $this->entityHelper->findAll();
    }

    public function find(CompetitionSelection $selection)
    {
        $this->entityHelper->setSelection($selection);
        return $this->entityHelper->findArray();
    }

    protected function getEntitySelection()
    {
         return new CompetitionSelection();
    }

    protected function getEntityClass()
    {
        return 'STM\Competition\Competition';
    }
}
