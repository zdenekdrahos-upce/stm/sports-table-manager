<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Helpers;

use STM\DB\Database;
use STM\DB\SQL\SelectQuery;
use STM\DB\SQL\RowsLimit;
use STM\DB\SQL\ColumnOrder;
use STM\DB\SQL\Condition;
use STM\StmDatetime;
use STM\Utils\Dates;
use STM\Utils\Strings;
use \Exception;

final class Selection
{
    private static $date_ids;

    /** @var Condition */
    private $condition;
    /** @var RowsLimit */
    private $limit;
    /** @var array item = ColumnOrder->__toString() */
    private $orderColumns;
    /** @var int */
    private $conditionsCount;

    public function __construct()
    {
        $this->reset();
    }

    public function reset()
    {
        $this->condition = new Condition('');
        $this->conditionsCount = 0;
        $this->limit = false;
        $this->orderColumns = array();
    }

    /** @return SelectQuery */
    public function getSelectQuery($query)
    {
        if ($query instanceof SelectQuery) {
            $query = clone $query;
            if ($this->condition) {
                $query->setWhere($this->condition);
            }
            if ($this->limit) {
                $query->setLimit($this->limit);
            }
            if ($this->orderColumns) {
                $query->setOrderBy(implode(', ', $this->orderColumns));
            }
            return $query;
        }
        return false;
    }

    /** @return Condition */
    public function getEscapedCondition()
    {
        $condition = clone $this->condition;
        $condition->escapeValues(Database::getDB());
        return $condition;
    }

    /** @return RowsLimit/false */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param int $max_rows
     * @param int $offset
     */
    public function setRowsLimit($max_rows, $offset)
    {
        try {
            $this->limit = new RowsLimit();
            $this->limit->set($max_rows, $offset);
        } catch (Exception $e) {
            $this->limit = false;
        }
    }

    /**
     * @param string $name
     * @param boolean $is_ascending
     */
    public function addColumnOrder($name, $is_ascending)
    {
        $order = new ColumnOrder($name, $is_ascending);
        if ($order->isValid()) {
            $this->orderColumns[] = $order->__toString();
        }
    }

    /**
     * @param Condition $condition
     */
    public function addCondition($condition)
    {
        if ($condition instanceof Condition && $condition->isValid()) {
            $condition->escapeValues(Database::getDB());
            $this->condition->addOrReplaceCondition($condition->__toString());
        }
    }

    /**
     * @param string $name
     * @param boolean $is_null
     */
    public function setNullAttribute($name, $is_null)
    {
        if (Strings::isStringNonEmpty($name)) {
            $operator = $is_null ? 'IS NULL' : 'IS NOT NULL';
            $this->condition->addOrReplaceCondition("{$name} {$operator}");
        }
    }

    /**
     * @param string $name
     * @param string $value
     */
    public function setAttributeLikeValue($name, $value)
    {
        $value = is_string($value) ? "%{$value}%" : $value;
        $this->setAttributeWithValue($name, $value, 'LIKE');
    }

    /**
     * @param string $name
     * @param string $value
     */
    public function setAttributeWithValue($name, $value, $operator = '=')
    {
        if (Strings::isStringNonEmpty($name) && Strings::isStringNonEmpty($value)) {
            $id = $this->conditionsCount++;
            $this->condition->addOrReplaceCondition("{$name} {$operator} {{$id}}", array($id => $value));
        }
    }

    /**
     * @param string $name
     * @param string|null $date_min
     * @param string|null $date_max
     */
    public function setAttributeWithDates($name, $date_min, $date_max)
    {
        if (Strings::isStringNonEmpty($name)) {
            $date_min = $this->convertDate($date_min);
            $date_max = $this->convertDate($date_max);
            if ($date_min || $date_max) {
                $min = self::$date_ids['min'];
                $max = self::$date_ids['max'];
                if ($date_min && $date_max) {
                    $condition = "{$name} BETWEEN {$min} AND {$max}";
                    $args = array('DMIN' => $date_min, 'DMAX' => $date_max);
                } elseif (empty($date_min) && $date_max) {
                    $condition = "{$name} <= {$max}";
                    $args = array('DMAX' => $date_max);
                } elseif (empty($date_max) && $date_min) {
                    $condition = "{$name} >= {$min}";
                    $args = array('DMIN' => $date_min);
                }
                $this->condition->addOrReplaceCondition('(' . $condition . ')', $args);
            }
        }
    }

    /**
     * @param string $date
     * @param string $column_date_min
     * @param string $column_date_max
     */
    public function setDateBetweenAttributes($date, $column_date_min, $column_date_max)
    {
        $date = $this->convertDate($date);
        if (Strings::isStringNonEmpty($date)) {
            $db_date = self::$date_ids['date'];
            $condition = "({$db_date} BETWEEN {$column_date_min} AND {$column_date_max})";
            $condition .= " OR ({$column_date_min} IS NULL AND {$column_date_max} >= {$db_date})";
            $condition .= " OR ({$column_date_max} IS NULL AND {$column_date_min} <= {$db_date})";
            $args = array('DATE' => $date);
            $this->condition->addOrReplaceCondition('(' . $condition . ')', $args);
        }
    }

    private function convertDate($date)
    {
        if ($date instanceof StmDatetime) {
            return Dates::datetimeToDatabaseDate($date);
        }
        return Dates::stringToDatabaseDate($date);
    }

    public static function initDateIds()
    {
        $db = Database::getDB();
        self::$date_ids = array(
            'date' => $db::prepareStringForDateComparison('{DATE}'),
            'min' => $db::prepareStringForDateComparison('{DMIN}'),
            'max' => $db::prepareStringForDateComparison('{DMAX}'),
        );
    }

    /**
     * @param array $columns
     * @param array $values
     * @param boolean $and
     */
    public function setAttributesInMultipleColumns($columns, $values, $and)
    {
        if (is_array($values) && is_array($columns)) {
            $values = array_unique($values);
            if ($values) {
                $values_for_escaping = array();
                foreach ($values as $value) {
                    $values_for_escaping[$this->conditionsCount++] = $value;
                }
                $string_for_in = '{' . implode('}, {', array_keys($values_for_escaping)) . '}';
                foreach ($columns as $key => $column_name) {
                    $columns[$key] = "{$column_name} IN ({$string_for_in})";
                }
                $this->set($columns, $values_for_escaping, $and);
            }
        }
    }

    private function set($columns, $values, $and)
    {
        $operator = $and ? 'AND' : 'OR';
        $condition = '(' . implode(" {$operator} ", $columns) . ')';
        $this->condition->addOrReplaceCondition($condition, $values);
    }
}
