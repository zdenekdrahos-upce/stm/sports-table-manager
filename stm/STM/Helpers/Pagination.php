<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Helpers;

final class Pagination
{
    private $totalCount;
    private $perPage;
    private $currentPage;
    private $totalPages;

    public function __construct($total_count, $per_page, $current_page)
    {
        $this->totalCount = $total_count;
        $this->perPage = $per_page;
        $this->totalPages = ceil($total_count / $per_page);
        $this->currentPage = 1;
        $this->setCurrentPage($current_page);
    }

    public function getTotalPages()
    {
        return $this->totalPages;
    }

    public function existsNextPage()
    {
        return $this->currentPage + 1 <= $this->getTotalPages();
    }

    public function existsPreviousPage()
    {
        return $this->currentPage - 1 >= 1;
    }

    public function getNextPage()
    {
        if ($this->existsNextPage()) {
            return $this->currentPage + 1;
        }
    }

    public function getPreviousPage()
    {
        if ($this->existsPreviousPage()) {
            return $this->currentPage - 1;
        }
    }

    public function getOffset()
    {
        return ($this->currentPage - 1) * $this->perPage;
    }

    public function getCurrentPage()
    {
        return $this->currentPage;
    }

    public function getPerPage()
    {
        return $this->perPage;
    }

    public function setCurrentPage($page)
    {
        if ($this->isPageValid($page)) {
            $this->currentPage = $page;
            return true;
        }
        return false;
    }

    public function isPageValid($page)
    {
        return $page >= 1 && $page <= $this->totalPages;
    }
}
