<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Helpers;

use STM\Web\Message\FormError;
use STM\Libs\FormProcessor;
use STM\Utils\Arrays;
use STM\Utils\Dates;

/**
 * Class for advanced validating
 * - comparision to selected object
 * - advanced method for validating: string, number, date
 * - used in TeamValidator, UserValidator, CompetitionValidator, MatchValidator,...
 */
final class ObjectValidator
{
    /** @var \STM\Libs\FormProcessor */
    private $formProcessor;
    /**
     * Name of the class, instances of this class are used for comparing
     * @var string
     */
    private $className;
    /** @var object|false */
    private $comparedObject;

    /**
     * Initializes class with default FormProcessor and if class $className was
     * defined then sets this class as class of compared objects
     * @param string $className
     */
    public function __construct($className)
    {
        // only string check, class_exists not work
        $this->class_name = is_string($className) ? $className : false;
        $this->comparedObject = false;
        $this->formProcessor = new FormProcessor(true);
    }

    /**
     * If $fp is instance of FormProcessor then sets it as attribute and resets
     * all errors, so after setting $fp doesn't have saved any errors, otherwise
     * sets as attributes new instance of FormProcessor
     * @param \STM\Libs\FormProcessor $fp
     */
    public function setFormProcessor($fp)
    {
        if ($fp instanceof FormProcessor) {
            $this->formProcessor = $fp;
            $this->formProcessor->resetErrors();
        } else {
            $this->formProcessor = new FormProcessor(true);
        }
    }

    /**
     * @return \STM\Libs\FormProcessor
     */
    public function getFormProcessor()
    {
        return $this->formProcessor;
    }

    /**
     * @return boolean
     * If $formProcessor contains errors then returns false, otherwise true
     */
    public function isValid()
    {
        return $this->formProcessor->isValid();
    }

    /**
     * Sets instance of the $this->$className class. Instance is used for checking if
     * new value of attribute is different from current value in instance.
     * @param object $instance
     */
    public function setComparedObject($instance)
    {
        if (is_string($this->class_name) && $instance instanceof $this->class_name) {
            $this->comparedObject = $instance;
        }
    }

    /**
     * @return object
     */
    public function getComparedObject()
    {
        return $this->comparedObject;
    }

    /**
     * Removes team, so attributes are not compared with team attributes
     */
    public function resetComparedObject()
    {
        if ($this->isSetComparedObject()) {
            $this->comparedObject = false;
        }
    }

    /**
     * @return boolean
     */
    public function isSetComparedObject()
    {
        return is_object($this->comparedObject);
    }

    /**
     * Checks if value is string and from specified range and different from old one.
     * @param string $string
     * @param array $range e.g. array('min' => 5, 'max' => 10)
     * @param string $error_name
     * @param string $old
     * @return boolean
     */
    public function checkString($string, $range, $error_name = 'Name', $old = false)
    {
        if (Arrays::isArrayValid($range, array('min', 'max')) && is_string($error_name)) {
            $this->formProcessor->checkCondition(
                !empty($string) || is_numeric($string),
                FormError::get('empty-value', array($error_name))
            );
            $this->formProcessor->checkCondition(is_string($string), "{$error_name} must be string.");
            if ($this->formProcessor->isValid()) {
                $rangeError = FormError::get('string-length', array($error_name, $range['min'], $range['max']));
                $this->formProcessor->checkRange(strlen($string), $range['min'], $range['max'], $rangeError);
                $this->formProcessor->checkCondition($string !== $old, FormError::get('no-change', array($error_name)));
            }
        } else {
            $this->formProcessor->addError('Invalid attributes in checkString method (ObjectValidator)');
        }
        return $this->isValid();
    }

    /**
     * Checks if value is numeric and from specified range and different from old one.
     * @param number $number
     * @param array $range e.g. array('min' => 5, 'max' => 10)
     * @param string $error_name
     * @param string $old
     * @return boolean
     */
    public function checkNumber($number, $range, $error_name = 'Score')
    {
        if (Arrays::isArrayValid($range, array('min', 'max')) && is_string($error_name)) {
            $this->formProcessor->checkCondition(
                is_numeric($number),
                FormError::get('invalid-value', array($error_name, stmLang('number')))
            );
            if ($this->formProcessor->isValid()) {
                $rangeError = FormError::get('number-length', array($error_name, $range['min'], $range['max']));
                $this->formProcessor->checkRange($number, $range['min'], $range['max'], $rangeError);
            }
        } else {
            $this->formProcessor->addError('Invalid attributes in checkNumber method (ObjectValidator)');
        }
        return $this->isValid();
    }

    /**
     * @param string $date
     * @param string $error_message
     * @param \STM\Libs\FormProcessor $fp
     * @return boolean
     * Returns true if date is valid or date is empty string
     */
    public function checkOptionalDate($date, $error_message)
    {
        if (is_string($error_message)) {
            if (!is_string($date)) {
                $this->formProcessor->addError('Date must be string (empty or non empty)');
            } elseif ($date != '') {
                $new_date = Dates::stringToDatabaseDate($date);
                $this->formProcessor->checkCondition(
                    !empty($new_date),
                    FormError::get('invalid-date', array($error_message))
                );
            }
        } else {
            $this->formProcessor->addError('Invalid attributes in checkOptionalDate method (ObjectValidator)');
        }
        return $this->isValid();
    }

    /**
     * Check name (string) + compare to old name from compared object + check if
     * name is unique (not already exists)
     * @param string $new_name
     * @param array $range
     * @param string $get_method
     * @param string $exist_method
     * @return boolean
     */
    public function checkUniqueName($new_name, $range, $get_method)
    {
        $this->checkString($new_name, $range, stmLang('name'), $this->getValueFromCompared($get_method));
        if ($this->isValid() && $this->class_name) {
            $class = $this->class_name;
            $this->formProcessor->checkCondition(
                !$class::exists($new_name),
                FormError::get('unique-value', array(stmLang('name')))
            );
        }
        return $this->isValid();
    }

    public function checkNumberAndCompare($number, $range, $getter, $error_name = 'Score')
    {
        $this->checkNumber($number, $range, $error_name);
        if ($this->isValid() && $this->isSetComparedObject()) {
            $this->getFormProcessor()->checkCondition(
                $number != $this->getValueFromCompared($getter),
                FormError::get('no-change', array($error_name))
            );
        }
        return $this->isValid();
    }

    /**
     * Resets compared object and controls if $attributes array contains all keys
     * which are defined in array $structure
     * @param array $attributes
     * @param array $structure
     * @return boolean
     */
    public function checkCreate($attributes, $structure)
    {
        $this->resetComparedObject();
        $this->formProcessor->checkCondition(
            Arrays::isArrayValid($attributes, $structure),
            FormError::get('invalid-input')
        );
        return $this->isValid();
    }

    public function checkUpdate($attributes, $structure)
    {
        $this->formProcessor->checkCondition($this->isSetComparedObject(), 'Updated object is not set');
        $this->formProcessor->checkCondition(
            Arrays::isArrayValid($attributes, $structure),
            FormError::get('invalid-input')
        );
        return $this->isValid();
    }

    /**
     * Gets value from method $method_name in class $this->class_name. It can be
     * used only for methods with no required parameters. Main use for getters.
     * @param string $method_name name of the method
     * @return mixed
     * Returns false if compared object is not set or $method_name doesn't
     * exist as method in class $this->class_name or if real class method returns false
     */
    public function getValueFromCompared($method_name)
    {
        if ($this->isSetComparedObject() && is_string($method_name)) {
            if (method_exists($this->comparedObject, $method_name)) {
                return $this->comparedObject->$method_name();
            }
        }
        return false;
    }
}
