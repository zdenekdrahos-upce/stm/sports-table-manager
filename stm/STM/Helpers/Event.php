<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Helpers;

use STM\Libs\Logger;
use STM\Utils\Classes;

/**
 * Event class
 * - used for save events to logfile
 */
class Event
{
    /** @var string event classname */
    private $constantName;
    /** @var string name of the file in log folder */
    private $logname;
    /** @var timestamp from time() in setMessage() */
    private $time;
    /** @var string from $_SESSION['username'], if session not exists, then 'User' */
    private $author;
    /** @var string from $_SERVER['REMOTE_ADDR'] */
    private $ip;
    /** @var string basic description of the event */
    private $event;
    /** @var boolean specify if event was successful or fail */
    private $result;
    /** @var string message further details of the event */
    private $message;


    /**
     * @param string $constantName classname of the event class ($event must be constant from this class)
     * @param string $logname  name of the file in folder with logs
     */
    public function __construct($constantName, $logname)
    {
        $this->constantName = is_string($constantName) ? $constantName : 'Event';
        $this->logname = is_string($logname) ? $logname : STM_DEFAULT_LOG_NAME;
        $this->event = false;
    }

    /**
     * Sets content of the record which will be saved to logfile
     * @param string $event
     * @param boolean $result
     * @param string $message
     */
    public function setMessage($event, $message, $result)
    {
        if (Classes::isClassConstant($this->constantName, $event)) {
            $this->time = time();
            $this->author = (isset($_SESSION['stmUser']['name'])) ? $_SESSION['stmUser']['name'] : 'User';
            $this->ip = $_SERVER['REMOTE_ADDR'];
            $this->result = $result ? 'Successful' : 'Fail';
            $this->event = $event;
            $this->message = is_string($message) ? $message : 'Unspecified message';
        }
    }

    /**
     * Save instance record to logfile
     * @return boolean
     * Returns true if event was successfuly saved to file, false if event wasn't
     * set, log's folder doesn't exist or missing file permissions to log file.
     */
    public function logEvent()
    {
        if ($this->event) {
            return Logger::logEvent($this, $this->logname);
        }
        return false;
    }

    /**
     * @return string
     * If event was set then returns event as string, otherwise returns time of
     * calling this method and text Unspecified event
     */
    public function __toString()
    {
        if ($this->event) {
            $text = $this->convertTimeToString($this->time) . '"';
            $text .= STM_SEP . '"' . $this->author . '"' . STM_SEP . '"' . $this->ip . '"';
            $text .= STM_SEP . '"' . $this->event . '"' . STM_SEP . '"' . $this->message . '"';
            $text .= STM_SEP . '"' . $this->result . '"';
            return '"' . $text . "\n";
        } else {
            return $this->convertTimeToString(time()) . '"' . STM_SEP . '"Unspecified event"';
        }
    }

    /**
     * Converts timestamp to string (specified in constant STM_TIME_FORMAT in init.php)
     * @param int $timestamp
     * @return string
     */
    private function convertTimeToString($timestamp)
    {
        return strftime(STM_TIME_FORMAT, $timestamp);
    }
}
