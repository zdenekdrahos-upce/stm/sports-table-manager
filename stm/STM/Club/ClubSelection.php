<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Club;

use STM\Entities\AbstractEntitySelection;

class ClubSelection extends AbstractEntitySelection
{

    public function setId($idClub)
    {
        if (is_numeric($idClub)) {
            $this->selection->setAttributeWithValue('clubs.id_club', (string) $idClub);
        }
    }

    protected function getQuery()
    {
        return ClubSQL::selectClubs();
    }
}
