<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Club;

use STM\DB\Object\DatabaseObject;
use STM\StmDatetime;
use STM\Utils\Dates;

final class Club
{
    /** @var DatabaseObject */
    private static $db;

    /** @var int */
    private $id_club;
    /** @var string */
    private $name;
    /** @var string */
    private $city;
    /** @var mysql datetime */
    private $foundation_date;
    /** @var string */
    private $club_info;
    /** @var string */
    private $club_colors;
    /** @var string */
    private $website;
    /** @var string */
    private $address;
    /** @var string */
    private $email_contact;
    /** @var string */
    private $telephone_contact;
    /** @var int */
    private $id_logo;
    /** @var int */
    private $id_stadium;
    /** @var int */
    private $id_country;

    /** @var string */
    private $name_country;
    /** @var string */
    private $name_stadium;

    private function __construct()
    {
    }

    public static function setORM(DatabaseObject $dbObject)
    {
        if (is_null(self::$db)) {
            self::$db = $dbObject;
        }
    }

    public static function create($attributes)
    {
        if (ClubValidator::checkCreate($attributes)) {
            return self::$db->insertValues(self::processAttributes($attributes), true);
        }
        return false;
    }

    public static function exists($club_name)
    {
        return self::$db->existsItem('name', $club_name);
    }

    public function isTeamFromClub($teamName)
    {
        if (is_string($teamName)) {
            $select = ClubSQL::selectIsTeamFromClub($this, $teamName);
            return self::$db->selectCountInQuery($select) > 0;
        }
        return false;
    }

    public function delete()
    {
        if ($this->canBeDeleted()) {
            return self::$db->deleteById($this->id_club);
        }
        return false;
    }

    public function canBeDeleted()
    {
        $query = ClubSQL::selectIncidenceOfClub($this);
        return self::$db->selectCountInQuery($query) == 0;
    }

    public function update($details)
    {
        ClubValidator::setClub($this);
        if (ClubValidator::checkClubUpdate($details)) {
            return self::$db->updateById($this->id_club, self::processAttributes($details));
        }
        return false;
    }

    public function getId()
    {
        return (int) $this->id_club;
    }

    public function getIdCountry()
    {
        return is_numeric($this->id_country) ? (int) $this->id_country : false;
    }

    public function getIdStadium()
    {
        return is_numeric($this->id_stadium) ? (int) $this->id_stadium : false;
    }

    public function toArray()
    {
        return array(
            'name' => $this->name,
            'city' => $this->city,
            'foundation_date' => new StmDatetime($this->foundation_date),
            'club_info' => $this->club_info,
            'club_colors' => $this->club_colors,
            'website' => $this->website,
            'address' => $this->address,
            'email_contact' => $this->email_contact,
            'telephone_contact' => $this->telephone_contact,
            'stadium' => $this->name_stadium,
            'country' => $this->name_country
        );
    }

    public function __toString()
    {
        return $this->name;
    }

    private static function processAttributes($attributes)
    {
        $attributes['foundation_date'] = Dates::stringToDatabaseDate($attributes['foundation_date']);
        return $attributes;
    }
}
