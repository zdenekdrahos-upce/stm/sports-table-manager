<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Club;

use STM\DB\SQL\SelectQuery;
use STM\DB\SQL\JoinType;
use STM\DB\SQL\JoinTable;
use STM\DB\SQL\Condition;
use STM\DB\Query\QueryFactory;

class ClubSQL
{

    /** @return SelectQuery */
    public static function selectClubs()
    {
        $query = new SelectQuery(DT_CLUBS . ' clubs');
        $query->setColumns(
            'id_club, clubs.name, city, foundation_date, club_info, club_colors,
            website, address, email_contact, telephone_contact, id_logo,
            clubs.id_stadium, stadiums.name as name_stadium,
            clubs.id_country, countries.country as name_country'
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::LEFT,
                DT_STADIUMS . ' stadiums',
                'clubs.id_stadium = stadiums.id_stadium'
            )
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::LEFT,
                DT_COUNTRIES . ' countries',
                'clubs.id_country = countries.id_country'
            )
        );
        $query->setOrderBy('clubs.name');
        return $query;
    }

    /** @return SelectQuery */
    public static function selectIncidenceOfClub(Club $club)
    {
        $condition = new Condition('id_club = {I}', array('I' => $club->getId()));
        $tables = array(
            DT_TEAMS => $condition,
            DT_CLUBS_MEMBERS => $condition
        );
        return QueryFactory::selectIncidence($tables);
    }

    /** @return SelectQuery */
    public static function selectIsTeamFromClub(Club $club, $teamName)
    {
        $condition = new Condition(
            'id_club = {C} AND LOWER(name) = {T}',
            array('C' => $club->getId(), 'T' => mb_strtolower($teamName, 'UTF-8'))
        );
        return QueryFactory::selectCount(DT_TEAMS, $condition);
    }
}
