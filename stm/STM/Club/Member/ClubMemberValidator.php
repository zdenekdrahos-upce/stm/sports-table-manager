<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Club\Member;

use STM\Helpers\ObjectValidator;
use STM\Web\Message\FormError;
use STM\Club\Club;
use STM\Person\Person;
use STM\StmFactory;
use STM\Person\Position\PersonPosition;

/**
 * Class for validating attributes for database table 'CLUBS_MEMBERS'
 * - attributes = club, person, position
 */
final class ClubMemberValidator
{
    /** @var ObjectValidator */
    private static $validator;

    /** @param \STM\Libs\FormProcessor $formProcessor */
    public static function init($formProcessor = false)
    {
        if (is_null(self::$validator)) {
            self::$validator = new ObjectValidator('\STM\Club\Member\ClubMember');
        }
        self::$validator->setFormProcessor($formProcessor);
    }

    public static function setClubMember($club_member)
    {
        self::$validator->setComparedObject($club_member);
    }

    public static function resetClubMember()
    {
        self::$validator->resetComparedObject();
    }

    public static function checkCreate($attributes)
    {
        if (self::$validator->checkCreate($attributes, array('club', 'person', 'position'))) {
            self::$validator->getFormProcessor()->checkCondition($attributes['club'] instanceof Club, 'Invalid club');
            self::$validator->getFormProcessor()->checkCondition(
                $attributes['person'] instanceof Person,
                'Invalid person'
            );
            self::checkPosition($attributes['position']);
            self::checkExistingClubMember($attributes['club'], $attributes['person'], $attributes['position']);
        }
        return self::$validator->isValid();
    }

    public static function checkPosition($position)
    {
        self::$validator->getFormProcessor()->checkCondition($position instanceof PersonPosition, 'Invalid position');
        if (self::$validator->isValid() && self::$validator->isSetComparedObject()) {
            $current_id = self::$validator->getValueFromCompared('getIdPosition');
            self::$validator->getFormProcessor()->checkCondition(
                $position->getId() != $current_id,
                FormError::get('no-change', array(stmLang('club', 'position')))
            );
            $club = StmFactory::find()->Club->findById(self::$validator->getValueFromCompared('getIdClub'));
            $person = StmFactory::find()->Person->findById(self::$validator->getValueFromCompared('getIdPerson'));
            self::checkExistingClubMember($club, $person, $position);
        }
        return self::$validator->isValid();
    }

    private static function checkExistingClubMember($club, $person, $position)
    {
        if (self::$validator->isValid()) {
            $member = StmFactory::find()->ClubMember->findById($club, $person, $position);
            self::$validator->getFormProcessor()->checkCondition(
                $member == false,
                FormError::get('unique-value', array(stmLang('club', 'person')))
            );
        }
    }
}
