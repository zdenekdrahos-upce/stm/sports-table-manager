<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Club\Member;

use STM\DB\SQL\SelectQuery;
use STM\DB\SQL\JoinType;
use STM\DB\SQL\JoinTable;

class ClubMemberSQL
{

    /** @return SelectQuery */
    public static function selectClubMembers()
    {
        $query = new SelectQuery(DT_CLUBS_MEMBERS . ' clubs_members');
        $query->setColumns(
            "clubs_members.id_club, clubs_members.id_person, clubs_members.id_position,
            clubs.name as name_club, positions.position as name_position,
            concat(concat(persons.forename, ' '), persons.surname) as name_person"
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                DT_CLUBS . ' clubs',
                'clubs_members.id_club = clubs.id_club'
            )
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                DT_PERSONS . ' persons',
                'clubs_members.id_person = persons.id_person'
            )
        );
        $query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                DT_PERSON_POSITIONS . ' positions',
                'clubs_members.id_position = positions.id_position'
            )
        );
        return $query;
    }
}
