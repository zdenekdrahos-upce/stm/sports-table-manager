<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Club\Member;

use STM\Entities\AbstractEntitySelection;
use STM\Club\Club;
use STM\Person\Person;
use STM\Person\Position\PersonPosition;

class ClubMemberSelection extends AbstractEntitySelection
{

    public function setId(Club $club, Person $person, PersonPosition $position)
    {
        $this->setClub($club);
        $this->setPerson($person);
        $this->setPosition($position);
    }

    public function setClub(Club $club)
    {
        $this->setIdClub($club->getId());
    }

    public function setIdClub($idClub)
    {
        if (is_numeric($idClub)) {
            $this->selection->setAttributeWithValue('clubs_members.id_club', (string)$idClub);
        }
    }

    public function setPerson(Person $person)
    {
        $this->selection->setAttributeWithValue('clubs_members.id_person', (string)$person->getId());
    }

    public function setPosition(PersonPosition $position)
    {
        $this->selection->setAttributeWithValue('clubs_members.id_position', (string)$position->getId());
    }

    protected function getQuery()
    {
        return ClubMemberSQL::selectClubMembers();
    }
}
