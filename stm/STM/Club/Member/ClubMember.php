<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Club\Member;

use STM\DB\Object\DatabaseObject;

/**
 * ClubMember class
 * - instance: represents row from database table 'CLUBS_MEMBERS'
 */
final class ClubMember
{
    /** @var DatabaseObject */
    private static $db;

    private $id_club;
    private $id_person;
    private $id_position;
    private $name_club;
    private $name_person;
    private $name_position;

    private function __construct()
    {
    }

    public static function setORM(DatabaseObject $dbObject)
    {
        if (is_null(self::$db)) {
            self::$db = $dbObject;
        }
    }

    public static function create($attributes)
    {
        if (ClubMemberValidator::checkCreate($attributes)) {
            return self::$db->insertValues(self::processAttributes($attributes));
        }
        return false;
    }

    public function updatePersonPosition($position)
    {
        ClubMemberValidator::setClubMember($this);
        if (ClubMemberValidator::checkPosition($position)) {
            $attributes = array('id_position' => $position->getId());
            return self::$db->updateById($this->getPrimaryKey(), $attributes);
        }
        return false;
    }

    public function delete()
    {
        return self::$db->deleteById($this->getPrimaryKey());
    }

    private function getPrimaryKey()
    {
        return array(
            'club' => $this->id_club,
            'person' => $this->id_person,
            'position' => $this->id_position
        );
    }

    public function getIdClub()
    {
        return (int) $this->id_club;
    }

    public function getIdPerson()
    {
        return (int) $this->id_person;
    }

    public function getIdPosition()
    {
        return (int) $this->id_position;
    }

    public function toArray()
    {
        return array(
            'club' => $this->name_club,
            'person' => $this->name_person,
            'position' => $this->name_position,
        );
    }

    public function __toString()
    {
        return $this->name_person;
    }

    private static function processAttributes($attributes)
    {
        $result = array();
        $result['id_club'] = $attributes['club']->getId();
        $result['id_person'] = $attributes['person']->getId();
        $result['id_position'] = $attributes['position']->getId();
        return $result;
    }
}
