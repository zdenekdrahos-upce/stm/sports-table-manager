<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Club\Member;

use STM\Entities\AbstractEntityFactory;
use STM\Club\Club;
use STM\Person\Person;
use STM\Person\Position\PersonPosition;

class ClubMemberFactory extends AbstractEntityFactory
{
    public function findById(Club $club, Person $person, PersonPosition $position)
    {
        return $this->entityHelper->findById($club, $person, $position);
    }

    public function findByClub(Club $club)
    {
        $methods = array('setClub' => $club);
        return $this->entityHelper->setAndFindArray($methods);
    }

    public function findByIdClub($idClub)
    {
        $methods = array('setIdClub' => $idClub);
        return $this->entityHelper->setAndFindArray($methods);
    }

    public function findByPerson(Person $person)
    {
        $methods = array('setPerson' => $person);
        return $this->entityHelper->setAndFindArray($methods);
    }

    protected function getEntitySelection()
    {
         return new ClubMemberSelection();
    }

    protected function getEntityClass()
    {
        return 'STM\Club\Member\ClubMember';
    }
}
