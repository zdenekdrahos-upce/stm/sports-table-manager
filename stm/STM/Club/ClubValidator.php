<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Club;

use STM\Helpers\ObjectValidator;
use STM\Web\Message\FormError;
use STM\Country\Country;
use STM\Stadium\Stadium;
use STM\StmFactory;

/**
 * Class for validating attributes for database table 'CLUBS'
 * - attributes = name, city, foundation_date, club_info, club_colors, website
  address, email_contact, telephone_contact, id_stadium, id_country
 */
final class ClubValidator
{
    /** @var ObjectValidator */
    private static $validator;

    /** @param \STM\Libs\FormProcessor $formProcessor */
    public static function init($formProcessor = false)
    {
        if (is_null(self::$validator)) {
            self::$validator = new ObjectValidator('\STM\Club\Club');
        }
        self::$validator->setFormProcessor($formProcessor);
    }

    public static function setClub($club)
    {
        self::$validator->setComparedObject($club);
    }

    public static function resetClub()
    {
        self::$validator->resetComparedObject();
    }

    public static function checkCreate($attributes)
    {
        if (self::$validator->checkCreate($attributes, self::getAttributes())) {
            self::checkName($attributes['name']);
            self::checkOptionalFields($attributes);
        }
        return self::$validator->isValid();
    }

    public static function checkClubUpdate($details)
    {
        if (self::$validator->checkUpdate($details, self::getAttributes())) {
            if ($details['name'] != self::$validator->getValueFromCompared('__toString')) {
                self::checkName($details['name']);
            }
            self::checkOptionalFields($details);
            self::checkChangeInClubDetails($details);
        }
        return self::$validator->isValid();
    }

    private static function checkOptionalFields($attributes)
    {
        self::checkCity($attributes['city']);
        self::checkFoundationDate($attributes['foundation_date']);
        self::checkClubInfo($attributes['club_info']);
        self::checkClubColors($attributes['club_colors']);
        self::checkWebsite($attributes['website']);
        self::checkAddress($attributes['address']);
        self::checkEmail($attributes['email_contact']);
        self::checkTelephone($attributes['telephone_contact']);
        self::checkStadium($attributes['id_stadium']);
        self::checkCountry($attributes['id_country']);
    }

    public static function checkName($new_name)
    {
        return self::$validator->checkUniqueName($new_name, array('min' => 2, 'max' => 50), '__toString');
    }

    private static function checkCity($city)
    {
        self::checkOptionalString($city, array('min' => 2, 'max' => 50), stmLang('club', 'city'));
    }

    private static function checkFoundationDate($foundation_date)
    {
        self::$validator->checkOptionalDate($foundation_date, stmLang('club', 'foundation'));
    }

    private static function checkClubInfo($club_info)
    {
        self::checkOptionalString($club_info, array('min' => 5, 'max' => 500), stmLang('club', 'info'));
    }

    private static function checkClubColors($club_colors)
    {
        self::checkOptionalString($club_colors, array('min' => 2, 'max' => 60), stmLang('club', 'colors'));
    }

    private static function checkWebsite($website)
    {
        self::checkOptionalString($website, array('min' => 4, 'max' => 60), stmLang('club', 'website'));
        if (!empty($website) && self::$validator->isValid()) {
            self::$validator->getFormProcessor()->checkUrlAddress(
                $website,
                FormError::get('invalid-website', array(stmLang('club', 'website')))
            );
        }
    }

    private static function checkAddress($address)
    {
        self::checkOptionalString($address, array('min' => 2, 'max' => 100), stmLang('club', 'address'));
    }

    private static function checkEmail($email)
    {
        self::checkOptionalString($email, array('min' => 2, 'max' => 60), stmLang('club', 'email'));
        if (!empty($email) && self::$validator->isValid()) {
            self::$validator->getFormProcessor()->checkEmail(
                $email,
                FormError::get('invalid-email', array(stmLang('club', 'email')))
            );
        }
    }

    private static function checkTelephone($telephone)
    {
        self::checkOptionalString($telephone, array('min' => 6, 'max' => 20), stmLang('club', 'telephone'));
    }

    private static function checkStadium($id_stadium)
    {
        if (!empty($id_stadium)) {
            self::$validator->getFormProcessor()->checkCondition(
                StmFactory::find()->Stadium->findById($id_stadium) instanceof Stadium,
                'Invalid stadium'
            );
        }
    }

    private static function checkCountry($id_country)
    {
        if (!empty($id_country)) {
            self::$validator->getFormProcessor()->checkCondition(
                StmFactory::find()->Country->findById($id_country) instanceof Country,
                'Country must be existing country'
            );
        }
    }

    private static function checkOptionalString($string, $range, $error_name)
    {
        if ($string != '') {
            self::$validator->checkString($string, $range, $error_name);
        }
    }

    private static function checkChangeInClubDetails($details)
    {
        $change = false;
        $current = self::getCurrentNonObjectAttributes();
        foreach ($current as $key => $value) {
            if ($value != $details[$key]) {
                $change = true;
                break;
            }
        }
        $change = self::checkChangeInObject($change, $details['id_country'], 'getIdCountry');
        $change = self::checkChangeInObject($change, $details['id_stadium'], 'getIdStadium');
        self::$validator->getFormProcessor()->checkCondition(
            $change,
            FormError::get('no-change', array(stmLang('team', 'club')))
        );
    }

    private static function getCurrentNonObjectAttributes()
    {
        $current = self::$validator->getValueFromCompared('toArray');
        unset($current['stadium']);
        unset($current['country']);
        return $current;
    }

    private static function checkChangeInObject($change, $new_value, $getter)
    {
        if ($change == false) {
            if (!empty($new_value)) {
                $change = $new_value != self::$validator->getValueFromCompared($getter);
            } else {
                $change = is_int(self::$validator->getValueFromCompared($getter));
            }
        }
        return $change;
    }

    public static function getAttributes()
    {
        return array(
            'name', 'city', 'foundation_date', 'club_info', 'club_colors',
            'website', 'address', 'email_contact', 'telephone_contact',
            'id_stadium', 'id_country'
        );
    }
}
