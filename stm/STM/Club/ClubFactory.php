<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Club;

use STM\Entities\AbstractEntityFactory;

class ClubFactory extends AbstractEntityFactory
{
    public function findById($id)
    {
        return $this->entityHelper->findById($id);
    }

    public function findAll()
    {
        return $this->entityHelper->findAll();
    }

    protected function getEntitySelection()
    {
         return new ClubSelection();
    }

    protected function getEntityClass()
    {
        return 'STM\Club\Club';
    }
}
