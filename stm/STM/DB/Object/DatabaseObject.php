<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\DB\Object;

use STM\DB\IDatabase;
use STM\DB\SQL\Query;
use STM\DB\SQL\SelectQuery;
use STM\DB\SQL\Condition;
use STM\DB\Object\Helpers\FetchRows;
use STM\DB\Object\Helpers\TableQuery;
use STM\Utils\Arrays;
use STM\Utils\Strings;
use \Exception;

/**
 * DatabaseObject class
 * - used for basic CRUD operations in database table
 */
final class DatabaseObject
{
    /** @var IDatabase */
    private $database;
    /** @var FetchRows */
    private $fetchRows;
    /** @var TableQuery */
    private $tableQuery;

    /**
     * @param string $table Name of the table in database
     * @param array $primaryKey attributes which are in primary key
     */
    public function __construct($table, array $primaryKey)
    {
        if (Strings::isStringNonEmpty($table)) {
            $this->fetchRows = new FetchRows();
            $this->tableQuery = new TableQuery($table, $primaryKey);
        } else {
            throw new Exception('Bad arguments in DatabaseObject::__construct');
        }
    }

    /**
     * Sets ORM mapped PHP class to table
     * @param string $class Name of the class that is returned by method fetch
     */
    public function setClass($class)
    {
        $this->fetchRows->setClass($class);
    }

    public function setDatabase(IDatabase $db)
    {
        $this->database = $db;
    }

    /**
     * Performs query to database
     * @param Query $query
     * @return mixed
     * For SELECT returns a resource on success, or FALSE on error. For INSERT,
     * UPDATE, DELETE, etc returns TRUE on success or FALSE on error.
     * Returns false if $sql is not string
     */
    public function query(Query $query)
    {
        $query->escapeValues($this->database);
        return $this->database->query($query->generate());
    }

    /**
     * Insert values into table
     * @param array $attributes e.g. array('id' => 5, 'name' = 'zdenda')
     * @param boolean $cleanArray if remove empty fields from array
     * @return boolean
     * Returns true if inserting operation was successfull.
     */
    public function insertValues($attributes, $cleanArray = false)
    {
        if (is_array($attributes) && !empty($attributes)) {
            $attributes = $cleanArray ? Arrays::cleanArray($attributes) : $attributes;
            $insert = $this->tableQuery->getEmptyInsertQuery();
            $insert->addAttributes($attributes);
            return (bool) $this->query($insert);
        }
        return false;
    }

    public function getLastInsertId()
    {
        return $this->database->insertId();
    }

    /**
     * Updates selected row in table
     * @param numeric $id
     * @param array $attributes
     * @return boolean
     */
    public function updateById($id, $attributes)
    {
        if ($this->isValidPK($id)) {
            $values = $this->getValuesPK($id);
            $condition = $this->tableQuery->getConditionPK($values);
            return $this->updateByCondition($condition, $attributes);
        }
        return false;
    }

    /**
     * Updates row in table WHERE ...
     * @param \STM\DB\SQL\Condition $condition
     * @param array $attributes
     * @return boolean
     */
    public function updateByCondition(Condition $condition, array $attributes)
    {
        if (!empty($attributes)) {
            $update = $this->tableQuery->getEmptyUpdateQuery();
            $update->addAttributes($attributes);
            $update->setWhere($condition);
            return $this->query($update);
        }
        return false;
    }

    /**
     * Deletes row from table
     * @param scalar $id
     * @return boolean
     */
    public function deleteById($id)
    {
        if ($this->isValidPK($id)) {
            $values = $this->getValuesPK($id);
            $condition = $this->tableQuery->getConditionPK($values);
            return $this->deleteByCondition($condition);
        }
        return false;
    }

    /**
     * Deletes from table WHERE ...
     * @param \STM\DB\SQL\Condition $condition
     * @return boolean
     */
    public function deleteByCondition(Condition $condition)
    {
        $delete = $this->tableQuery->getEmptyDeleteQuery();
        $delete->setWhere($condition);
        return $this->query($delete);
    }

    /**
     * Finds if $value is already in table. Method is case-INsensitive.
     * @param string $attribute name of the table column
     * @param string $value searched value
     * @return boolean
     * Returns true if $value exists in table column $attribute.
     */
    public function existsItem($attribute, $value)
    {
        if (Strings::isStringNonEmpty($attribute) && Strings::isStringNonEmpty($value)) {
            return $this->isUnique(array($attribute), array($value));
        }
        return false;
    }

    public function isUnique(array $attributes, array $values)
    {
        $query = $this->tableQuery->existsRow($attributes, $values);
        $result = $this->select($query, 'array', true);
        return !empty($result);
    }

    /**
     * Method for perform select query.
     * @param SelectQuery $sql SQL Select query
     * @param string $type object - row is transformed into object
     *                      array - row is transformed into associative array
     * @param boolean $checkArray determine if result e.g. array with 1 item return array or 1 item
     * @return object/array/false
     * If $checkArray is false then method can return false or array (content depends on $type).
     * If $checkArray is true then method can return false, object or array (content depends on $type).
     * It returns false only if $sql is not instanceof SelectQuery or if this
     * instance is not valid (SelectQuery::isValid)
     */
    public function select(SelectQuery $sql, $type = 'object', $checkArray = false)
    {
        if ($sql instanceof SelectQuery && $sql->isValid()) {
            $result_array = $this->selectBySql($sql, $type);
            return $checkArray ? Arrays::checkArray($result_array) : $result_array;
        }
        return false;
    }

    /**
     * Selects all rows from table where $this->id_name is equal to $id from argument
     * @return object/array/false
     * If exists row with $id from argument then returns this row as object.
     * If exists more than 1 row with this $is then returns array of objects.
     * Otherwise returns false
     */
    public function selectById($id)
    {
        if ($this->isValidPK($id)) {
            $values = $this->getValuesPK($id);
            $query = $this->tableQuery->selectByPK($values);
            return $this->select($query, 'object', true);
        }
        return false;
    }

    /**
     * Selects number of rows from $table. Optionally select can be limited by
     * where condition
     * @param Condition|null $where
     * @return int
     */
    public function selectCount(Condition $where = null)
    {
        $query = $this->tableQuery->countRows($where);
        return $this->selectCountInQuery($query);
    }

    /**
     * Selects COUNT attribute from query
     * @param SelectQuery $query
     * @return int
     */
    public function selectCountInQuery(SelectQuery $query)
    {
        $count = $this->select($query, 'array', true);
        return $count ? ((int) $count['COUNT']) : 0;
    }

    /** @return boolean */
    private function isValidPK($id)
    {
        return is_scalar($id) || is_array($id);
    }

    /** @return array */
    private function getValuesPK($id)
    {
        if (is_array($id)) {
            return $id;
        } else {
            return array($id);
        }
    }

    /**
     * Performs database select query and transforms result of the query to array
     * which contains results (arrays or objects - depends on $fetch_type)
     * @param SelectQuery $sql SQL query (arrays or objects)
     * @param string $fetchType options: object, assoc
     * @return array
     */
    private function selectBySql($sql, $fetchType)
    {
        if ($fetchType == 'object') {
            $this->fetchRows->setObjectAsResult($this->database);
        } else {
            $this->fetchRows->setArrayAsResult($this->database);
        }
        $queryResult = $this->query($sql);
        return $this->fetchRows->fetch($queryResult);
    }

    /**
     * Executes procedure
     * @param string $name name of the procedure
     * @param array $parameters procedure parameteres
     * @return boolean
     * Returns boolean to inform about result of query.
     */
    public function executeProcedure($name, $parameters = array())
    {
        return $this->database->executeProcedure($name, $parameters);
    }

    /** @return boolean */
    public function executeQueriesInTransaction(array $queries)
    {
        $callback = array($this, 'executeQueryGroup');
        return $this->executeTransaction($callback, $queries);
    }

    /** @return boolean */
    public function executeTransaction($callback, array $args = array())
    {
        try {
            $this->database->startTransaction();
            call_user_func($callback, $args);
            $this->database->commitTransaction();
            return true;
        } catch (Exception $e) {
            $this->database->rollbackTransaction();
        }
        return false;
    }

    private function executeQueryGroup($queries)
    {
        foreach ($queries as $query) {
            if ($query instanceof Query) {
                $result = $this->query($query);
                if ($result == false) {
                    throw new Exception();
                }
            }
        }
    }
}
