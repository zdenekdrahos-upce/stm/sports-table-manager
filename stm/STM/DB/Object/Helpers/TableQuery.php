<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\DB\Object\Helpers;

use \InvalidArgumentException;
use STM\DB\SQL\Condition;
use STM\DB\SQL\SelectQuery;
use STM\DB\SQL\InsertQuery;
use STM\DB\SQL\DeleteQuery;
use STM\DB\SQL\UpdateQuery;
use STM\DB\Query\QueryFactory;

class TableQuery
{
    /** @var string */
    private $table;
    /** @var int */
    private $keysCount;
    /** @var array */
    private $primaryKeys;
    /** @var string */
    private $condition;

    public function __construct($table, array $primaryKeys)
    {
        $this->table = $table;
        $this->primaryKeys = $primaryKeys;
        $this->keysCount = count($primaryKeys);
        $this->loadCondition();
    }

    /** @return \STM\DB\SQL\InsertQuery */
    public function getEmptyInsertQuery()
    {
        return new InsertQuery($this->table);
    }

    /** @return \STM\DB\SQL\UpdateQuery */
    public function getEmptyUpdateQuery()
    {
        return new UpdateQuery($this->table);
    }

    /** @return \STM\DB\SQL\DeleteQuery */
    public function getEmptyDeleteQuery()
    {
        return new DeleteQuery($this->table);
    }

    /** @return \STM\DB\SQL\Condition */
    public function getConditionPK(array $values)
    {
        if (count($values) != $this->keysCount) {
            throw new InvalidArgumentException('Number of values and attributes in PK does not match');
        }
        return new Condition($this->condition, array_values($values));
    }

    /**
     * @param array $values values for attributes in primary key
     * @return \STM\DB\SQL\SelectQuery
     */
    public function selectByPK(array $values)
    {
        $condition = $this->getConditionPK($values);
        return $this->select($condition);
    }

    /**
     * Selects rows from table. Optionally select can be limited by
     * where condition and sorted by $order.
     * @param Condition|null $where
     * @param string $order
     * @return \STM\DB\SQL\SelectQuery
     * Returns alway array (even if rows don't exist in table)
     */
    public function select(Condition $where = null, $order = '')
    {
        return QueryFactory::selectAll($this->table, $where, $order);
    }

    /**
     * Selects number of rows from $table. Optionally select can be limited by
     * where condition
     * @param Condition|null $where
     * @return \STM\DB\SQL\SelectQuery
     */
    public function countRows(Condition $where = null)
    {
        if (is_null($where)) {
            return $this->countRowsInTable();
        } else {
            return QueryFactory::selectCount($this->table, $where);
        }
    }

    /** @return \STM\DB\SQL\SelectQuery */
    public function countRowsInTable()
    {
        return QueryFactory::selectCount($this->table);
    }

    /**
     * Finds if $values in unique, if it's not already in table.
     * Method is case-INsensitive.
     * @param array $attributes table columns
     * @param array $values searched values in columns
     * @return \STM\DB\SQL\SelectQuery
     */
    public function existsRow(array $attributes, array $values)
    {
        $condition = $this->getExistsCondition($attributes, $values);
        $query = new SelectQuery($this->table);
        $query->setColumns('1');
        $query->setWhere($condition);
        return $query;
    }

    private function getExistsCondition($attributes, $values)
    {
        $conditions = array();
        $conditionArray = array();
        for ($i = 0; $i < count($attributes); $i++) {
            $conditions[] = "LOWER({$attributes[$i]}) = {{$i}}";
            $conditionArray[] = mb_strtolower($values[$i], 'UTF-8');
        }
        return new Condition(
            implode(' AND ', $conditions),
            array_values($conditionArray)
        );
    }

    private function loadCondition()
    {
        $conditions = array();
        for ($i = 0; $i < $this->keysCount; $i++) {
            $conditions[] = "{$this->primaryKeys[$i]} = {{$i}}";
        }
        $this->condition = implode(' AND ', $conditions);
    }
}
