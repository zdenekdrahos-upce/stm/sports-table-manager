<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\DB\Object\Helpers;

use STM\DB\IDatabase;
use STM\Utils\Strings;

class FetchRows
{

    /** @var string */
    private $className = '';
    /** @var string name of fetch method in IDatabase class */
    private $fetchMethod;
    /** @var callable */
    private $callback;
    /** @var array */
    private $callbackArgs;

    public function setClass($class)
    {
        if (Strings::isStringNonEmpty($class)) {
            $this->className = $class;
        }
    }

    public function isSetClass()
    {
        return $this->className !== '';
    }

    final public function setArrayAsResult(IDatabase $db)
    {
        if ($this->fetchMethod != 'fetchAssoc') {
            $this->fetchMethod = 'fetchAssoc';
            $this->setCallback($db);
        }
    }

    public function setObjectAsResult(IDatabase $db)
    {
        if ($this->fetchMethod != 'fetchObject' && $this->isSetClass()) {
            $this->fetchMethod = 'fetchObject';
            $this->setCallback($db);
        }
    }

    private function setCallback($db)
    {
        $this->callback = array($db, $this->fetchMethod);
        $this->callbackArgs = array('result');
        if ($this->isSetClass()) {
            $this->callbackArgs[] = $this->className;
        }
    }

    /**
     * Performs database select query and transforms result of the query to array
     * which contains results (arrays or objects - depends on selected result)
     * @param resource $queryResult result returned by IDatabase::query
     * @param \STM\DB\IDatabase $db
     * @return array
     */
    public function fetch($queryResult)
    {
        $this->addQueryResult($queryResult);
        return $this->fetchRows();
    }

    private function addQueryResult($queryResult)
    {
        $this->callbackArgs[0] = $queryResult; // add result as first parameter
    }

    private function fetchRows()
    {
        $resultArray = array();
        while ($row = call_user_func_array($this->callback, $this->callbackArgs)) {
            $resultArray[] = $row;
        }
        return $resultArray;
    }
}
