<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\DB\Object;

use STM\DB\Database;
use STM\DB\DatabaseException;
use STM\DB\ORM\ClassMapping;
use STM\DB\ORM\TablePrimaryKeys;

class DbObjectsLoader
{
    /** @var \STM\DB\IDatabase */
    private static $db;
    /** @var boolean */
    private static $connectionError;

    public static function initDb()
    {
        if (is_null(self::$db)) {
            self::$db = Database::getDB();
            try {
                self::$db->openConnection();
                self::$connectionError = false;
            } catch (DatabaseException $e) {
                self::$connectionError = true;
                throw $e;
            }
        }
    }

    public static function loadAllObjects()
    {
        $classes = array_keys(ClassMapping::$mapping);
        foreach ($classes as $class) {
            self::loadClass($class);
        }
    }

    public static function loadClass($class)
    {
        if (self::$connectionError) {
            throw self::$db->getLastException();
        }
        $ormObjects = ClassMapping::$mapping[$class];
        foreach ($ormObjects as $object => $table) {
            $pk = TablePrimaryKeys::$KEYS[$table];
            if ($object == 'default') {
                $object = $class;
            } elseif ($object == 'none') {
                $object = false;
            }
            $dbObject = new DatabaseObject($table, $pk);
            $dbObject->setDatabase(self::$db);
            if ($object) {
                $dbObject->setClass($object);
            }
            $class::setORM($dbObject);
        }
    }
}
