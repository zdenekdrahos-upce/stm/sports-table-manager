<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\DB\RDBMS;

use STM\DB\IDatabase;
use STM\DB\DatabaseEvent;
use STM\DB\DatabaseException;
use STM\Utils\Strings;
use STM\DB\SQL\RowsLimit;

/**
 * MySQLDatabase class
 * - for basic operations with MySQL Database
 */
class MySQLDatabase implements IDatabase
{
    /** @var string */
    private $server;
    /** @var string */
    private $user;
    /** @var string */
    private $password;
    /** @var string */
    private $dbName;

    /** @var type MySQL link identifier on success or FALSE on failure. */
    private $connection;
    /** @var string Last performed SQL query */
    private $lastQuery;
    /** @var DatabaseException */
    private $lastException;

    public function __construct($server, $user, $password, $dbName)
    {
        $this->server = $server;
        $this->user = $user;
        $this->password = $password;
        $this->dbName = $dbName;
    }

    public function __destruct()
    {
        $this->closeConnection();
    }

    public function openConnection()
    {
        $this->connection = @mysql_connect($this->server, $this->user, $this->password);
        if (!$this->connection) {
            $this->showError(DatabaseEvent::FAIL_CONNECTION);
        } else {
            mysql_query("SET NAMES 'UTF8'");
            $db_select = mysql_select_db($this->dbName, $this->connection);
            if (!$db_select) {
                $this->showError(DatabaseEvent::FAIL_SELECTION);
            }
        }
    }

    public function closeConnection()
    {
        if ($this->connection) {
            mysql_close($this->connection);
        }
    }

    public function query($sqlQuery)
    {
        $this->lastQuery = $sqlQuery;
        $result = mysql_query($sqlQuery, $this->connection);
        if (!$result) {
            $this->showError(DatabaseEvent::FAIL_QUERY);
        }
        return $result;
    }

    public function executeProcedure($name, $parameters = array())
    {
        if (Strings::isStringNonEmpty($name) && is_array($parameters)) {
            $parameters = implode(',', $parameters);
            return $this->query("CALL {$name}({$parameters});");
        }
        return false;
    }

    public function escapeValue($value)
    {
        return mysql_real_escape_string($value);
    }

    public function fetchAssoc($resultSet)
    {
        return mysql_fetch_assoc($resultSet);
    }

    public function fetchObject($resultSet, $className)
    {
        return mysql_fetch_object($resultSet, $className);
    }

    public function numRows($resultSet)
    {
        return mysql_num_rows($resultSet);
    }

    public function insertId()
    {
        return mysql_insert_id($this->connection);
    }

    # TRANSACTIONS
    public function startTransaction()
    {
        $this->query('START TRANSACTION;');
    }

    public function commitTransaction()
    {
        $this->query('COMMIT;');
    }

    public function rollbackTransaction()
    {
        $this->query('ROLLBACK;');
    }

    /**
     * @param Database $errorEvent constant from DatabaseEvent class
     * @param boolean $last_query
     */
    private function showError($errorEvent)
    {
        $error = trim(mb_convert_encoding(mysql_error(), 'UTF-8'));
        $output = $errorEvent . ' failed: ' . $error;
        if ($errorEvent == DatabaseEvent::FAIL_QUERY) {
            $error .= ' - last query: ' . $this->lastQuery;
        }
        $this->lastException = new DatabaseException($output, mysql_errno(), $errorEvent, $error);
        throw $this->lastException;
    }

    public static function addLimitToQuery(&$sqlQuery, RowsLimit $rowsLimit)
    {
        if (is_string($sqlQuery)) {
            $sqlQuery .= " LIMIT {$rowsLimit->getOffset()},{$rowsLimit->getMaxRows()}";
        }
    }

    public static function prepareStringForDateComparison($string_date)
    {
        return $string_date;
        // string can be compared with Datetime without conversion because it has MySQL date format
    }

    public function getLastException()
    {
        return $this->lastException;
    }
}
