<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\DB;

use \Exception;

final class DatabaseException extends Exception
{
    private $errorEvent;
    private $logMessage;

    public function __construct($message, $code, $errorEvent, $logMessage = '')
    {
        parent::__construct($message, $code);
        $this->errorEvent = $errorEvent;
        $this->logMessage = $logMessage;
    }

    public function getErrorEvent()
    {
        return $this->errorEvent;
    }

    public function getLogMessage()
    {
        return $this->logMessage;
    }
}
