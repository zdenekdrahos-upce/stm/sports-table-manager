<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\DB;

use STM\DB\SQL\Query;

/**
 * Database class
 * - name of the used database is defined in stm/config/database.php.
 * - methods: get selected database, get database functions
 */
final class Database
{
    /** @var Database */
    private static $instance;

    public static function getNullFunction()
    {
        return self::$instance->functions['null'];
    }

    public static function getCurrentDateFunction()
    {
        return self::$instance->functions['date'];
    }

    public static function getIsNumeric($field)
    {
        return '(' .  str_replace('{FIELD}', $field, self::$instance->functions['isNumeric']) . ')';
    }

    public static function escapeQuery(Query &$query)
    {
        $query->escapeValues(self::getDB());
    }

    /**
     * @return IDatabase
     * Returns instance of selected database
     */
    public static function getDB()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance->database;
    }

    /** @var IDatabase */
    private $database;
    /** @var array */
    private $functions;

    private function __construct()
    {
        $this->loadDatabase();
        $this->loadFunctionsNames();
    }

    private function loadDatabase()
    {
        $this->database = new RDBMS\MySQLDatabase(STM_DB_SERVER, STM_DB_USER, STM_DB_PASS, STM_DB_NAME);
    }

    private function loadFunctionsNames()
    {
        $this->functions = array(
            'null' => 'ifnull',
            'date' => 'now()',
            'isNumeric' =>
                "{FIELD} REGEXP '^([[+-]?((([[:digit:]]+[[.period.]]?[[:digit:]]*))([eE][+-]?[[:digit:]]+)?))$'",
        );
    }
}
