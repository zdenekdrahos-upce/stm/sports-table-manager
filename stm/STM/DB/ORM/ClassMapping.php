<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\DB\ORM;

final class ClassMapping
{
    public static $mapping = array(
        // classes mapped directly to tables (default == same as parent class)
        'STM\Competition\Competition' => array(
            'default' => DT_COMPETITIONS,
        ),
        'STM\Competition\Category\CompetitionCategory' => array(
            'default' => DT_COMPETITION_CATEGORIES,
        ),
        'STM\Competition\Season\Season' => array(
            'default' => DT_SEASONS,
        ),
        'STM\Competition\Playoff\Playoff' => array(
            'default' => DT_PLAYOFFS,
        ),
        'STM\Competition\Playoff\Team\PlayoffTeam' => array(
            'default' => DT_COMPETITIONS_TEAMS,
        ),
        'STM\Competition\Playoff\Serie\PlayoffSerie' => array(
            'default' => DT_PLAYOFF_SERIES,
        ),
        'STM\Match\Match' => array(
            'default' => DT_MATCHES,
        ),
        'STM\Match\Period\Period' => array(
            'default' => DT_MATCH_PERIODS,
        ),
        'STM\Match\Detail\MatchDetail' => array(
            'default' => DT_MATCH_DETAILS,
        ),
        'STM\Match\Referee\MatchReferee' => array(
            'default' => DT_MATCH_REFEREES,
        ),
        'STM\Match\Lineup\MatchPlayer' => array(
            'default' => DT_MATCH_PLAYERS,
        ),
        'STM\Match\Action\MatchAction' => array(
            'default' => DT_MATCH_ACTIONS,
        ),
        'STM\Match\Action\Team\MatchTeamAction' => array(
            'default' => DT_MATCH_TEAM_ACTIONS,
        ),
        'STM\Match\Action\Player\MatchPlayerAction' => array(
            'default' => DT_MATCH_PLAYER_ACTIONS,
        ),
        'STM\Prediction\Prediction' => array(
            'default' => DT_MATCHES_PREDICTIONS,
        ),
        'STM\Team\Team' => array(
            'default' => DT_TEAMS,
        ),
        'STM\Team\Member\TeamMember' => array(
            'default' => DT_TEAMS_PERSONS,
        ),
        'STM\Club\Club' => array(
            'default' => DT_CLUBS,
        ),
        'STM\Country\Country' => array(
            'default' => DT_COUNTRIES,
        ),
        'STM\Stadium\Stadium' => array(
            'default' => DT_STADIUMS,
        ),
        'STM\Club\Member\ClubMember' => array(
            'default' => DT_CLUBS_MEMBERS,
        ),
        'STM\Person\Person' => array(
            'default' => DT_PERSONS,
        ),
        'STM\Attribute\Attribute' => array(
            'default' => DT_ATTRIBUTES,
        ),
        'STM\Person\Attribute\PersonAttribute' => array(
            'default' => DT_PERSONS_ATTRIBUTES,
        ),
        'STM\Person\Position\PersonPosition' => array(
            'default' => DT_PERSON_POSITIONS,
        ),
        'STM\Sport\Sport' => array(
            'default' => DT_SPORTS,
        ),
        'STM\User\User' => array(
            'default' => DT_USERS,
        ),

        // classes which contains other objects which are mapped to tables
        'STM\Match\MatchFactory' => array( // TODO: MatchFactory without DB
            'STM\Match\Match' => DT_MATCHES,
        ),
        'STM\Competition\Season\Rounds\SeasonRounds' => array(
            'STM\Competition\Season\Rounds\SeasonRoundRow' => DT_MATCHES
        ),
        'STM\Match\Table\ResultTable' => array(
            'STM\Match\Table\TableRow' => DT_MATCHES
        ),
        'STM\Prediction\Stats\PredictionStats' => array(
            'STM\Prediction\Stats\PredictionStats' => DT_MATCHES_PREDICTIONS
        ),
        'STM\Prediction\Chart\PredictionChart' => array(
            'STM\Prediction\Chart\ChartRow' => DT_MATCHES_PREDICTIONS
        ),
        'STM\Match\Stats\Action\MatchPlayerStats' => array(
            'STM\Match\Stats\Action\PlayerActionStatistic' => DT_MATCH_PLAYER_ACTIONS,
            'STM\Match\Stats\Action\PlayerGeneralStatistic' => DT_MATCH_PLAYERS
        ),
        'STM\UserGroupAuthorization' => array(
            'stdClass' => DT_USER_GROUPS_AUTHORIZATIONS
        ),
        'STM\Team\Member\TeamPlayerHelper' => array(
            'stdClass' => DT_TEAMS_PLAYERS,
        ),

        // Classes which don't use mapping to php objects
        'STM\Competition\Playoff\Round\PlayoffRoundSaver' => array(
            'none' => DT_PLAYOFF_SERIES
        ),
        'STM\Competition\Season\Schedule\SeasonScheduleSaver' => array(
            'none' => DT_MATCHES
        ),
        'STM\Competition\Season\Schedule\SeasonRescheduler' => array(
            'none' => DT_MATCHES
        ),
        'STM\Match\Lineup\LineupImportPlayers' => array(
            'none' => DT_MATCH_PLAYERS
        )
    );
}
