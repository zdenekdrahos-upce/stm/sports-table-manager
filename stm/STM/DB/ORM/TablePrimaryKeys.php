<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\DB\ORM;

class TablePrimaryKeys
{
    public static $KEYS = array(
        DT_COMPETITIONS => array('id_competition'),
        DT_COMPETITION_CATEGORIES => array('id_category'),
        DT_SEASONS => array('id_season'),
        DT_PLAYOFFS => array('id_playoff'),
        DT_COMPETITIONS_TEAMS => array('id_competition', 'id_team'),
        DT_PLAYOFF_SERIES => array('id_serie'),
        DT_MATCHES => array('id_match'),
        DT_MATCH_PERIODS => array('id_match', 'id_period'),
        DT_MATCH_DETAILS => array('id_match'),
        DT_MATCH_REFEREES => array('id_match', 'id_referee'),
        DT_MATCH_PLAYERS => array('id_match_player'),
        DT_MATCH_ACTIONS => array('id_match_action'),
        DT_MATCH_TEAM_ACTIONS => array('id_match', 'id_match_statistic'),
        DT_MATCH_PLAYER_ACTIONS => array('id_match_player', 'id_match_action', 'minute_action'),
        DT_MATCHES_PREDICTIONS => array('id_match', 'id_user'),
        DT_TEAMS => array('id_team'),
        DT_TEAMS_PERSONS => array('id_team_player'),
        DT_TEAMS_PLAYERS => array('id_team_player'),
        DT_CLUBS => array('id_club'),
        DT_COUNTRIES => array('id_country'),
        DT_STADIUMS => array('id_stadium'),
        DT_CLUBS_MEMBERS => array('id_club', 'id_person', 'id_position'),
        DT_PERSONS => array('id_person'),
        DT_ATTRIBUTES => array('id_attribute'),
        DT_PERSONS_ATTRIBUTES => array('id_person', 'id_attribute'),
        DT_PERSON_POSITIONS => array('id_position'),
        DT_SPORTS => array('id_sport'),
        DT_USERS => array('id_user'),
        DT_USER_GROUPS_AUTHORIZATIONS => array('id_user_group', 'module_name')
    );
}
