<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\DB;

use STM\Helpers\Event;

/**
 * DatabaseEvent class
 * - for saving events (errors) related with Database
 */
class DatabaseEvent
{
    /** Database connection failed */
    const FAIL_CONNECTION = 'Database Connection';
    /** Database selection failed */
    const FAIL_SELECTION = 'Database Selection';
    /** Database query failed */
    const FAIL_QUERY = 'Database Query';
    /** Database transaction failed */
    const FAIL_TRANSACTION = 'Database Transaction';

    private function __construct()
    {
    }

    /**
     * Saves events related with errors in Database
     * @param DatabaseException $exception
     * @return boolean
     * Returns true if event was successfuly saved, otherwise false (bad arguments
     * or error during file saving)
     */
    public static function log(DatabaseException $exception)
    {
        $event = new Event('\STM\DB\DatabaseEvent', 'database');
        $event->setMessage(
            $exception->getErrorEvent(),
            $exception->getCode() . '"' . STM_SEP . '"' . $exception->getLogMessage(),
            false
        );
        return $event->logEvent();
    }
}
