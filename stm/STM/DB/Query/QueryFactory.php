<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\DB\Query;

use STM\Utils\Strings;
use STM\DB\Database;
use STM\DB\SQL\SelectQuery;
use STM\DB\SQL\Condition;
use STM\DB\SQL\JoinTable;
use STM\DB\SQL\JoinType;

/**
 * QueryFactory class
 * - for generating the base of the most used SQL SELECT queries
 * - all methods return instance of class SelectQuery
 */
class QueryFactory
{

    /**
     * Selects all columns (*) from $table. Optionally select can be limited by
     * where condition and sorted by $order. Checking if argument is valid string
     * is performed in SelectQuery class
     * @param string $table name of the table in Database
     * @param Condition $where if $where is not valid instance of Condition then no where condition is used
     * @param string $order e.g. 'name desc'
     * @return SelectQuery
     */
    public static function selectAll($table, $where = '', $order = '')
    {
        $query = new SelectQuery($table);
        $query->setColumns('*');
        $query->setWhere($where);
        $query->setOrderBy($order);
        return $query;
    }

    /**
     * Selects all columns (*) from $table where $atribute is equal to $value.
     * Optionally operator can be changed using argument $operator
     * @param string $table
     * @param string $attribute
     * @param scalar $value string, float, int or boolean
     * @param string $operator
     * @return SelectQuery/false
     * Returns instance of SelectQuery. If $attributes is not string, $values is
     * not scalar or $operator is not string then return false.
     */
    public static function selectByAttribute($table, $attribute, $value, $operator = '=')
    {
        if (Strings::isStringNonEmpty($attribute) &&
            Strings::isStringNonEmpty($operator) && is_scalar($value)
        ) {
            $query = new SelectQuery($table);
            $query->setColumns('*');
            $query->setWhere(new Condition("{$attribute} {$operator} {1}", array(1 => $value)));
            return $query;
        }
        return false;
    }

    /**
     * Selects number of rows from $table. Optionally select can be limited by
     * where condition
     * @param string $table
     * @param Condition $where if $where is not valid instance of Condition then no where condition is used
     * @return SelectQuery
     */
    public static function selectCount($table, $where = '')
    {
        $query = new SelectQuery($table);
        $query->setWhere($where); // check $where in SelectQuery
        return self::selectCountInQuery($query);
    }

    /**
     * @param SelectQuery $query
     * @return \SelectQuery|boolean
     */
    public static function selectCountInQuery($query)
    {
        if ($query instanceof SelectQuery) {
            Database::escapeQuery($query);
            $table = '( ' . $query->generate() . ')countedTable';
            $select = new SelectQuery($table);
            $select->setColumns(Database::getNullFunction() . '(count(*), 0) as COUNT');
            return $select;
        }
        return false;
    }

    public static function getMatchNameQuery()
    {
        $match_query = new SelectQuery(DT_MATCHES . ' matches');
        $match_query->setColumns(
            "matches.id_match, matches.id_competition,
             concat(concat(home.name, ' : '), away.name) as name_match"
        );
        $match_query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                DT_TEAMS . ' home',
                'matches.id_home = home.id_team'
            )
        );
        $match_query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                DT_TEAMS . ' away',
                'matches.id_away = away.id_team'
            )
        );
        Database::escapeQuery($match_query);
        return $match_query;
    }

    public static function getTeamMemberNameQuery()
    {
        $persons_query = new SelectQuery(DT_TEAMS_PERSONS . ' teams_members');
        $persons_query->setColumns(
            "teams_members.id_team_player, teams_members.id_person, teams_members.id_team,
            teams.name as name_team, concat(concat(persons.forename, ' '), persons.surname) as name_person"
        );
        $persons_query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                DT_TEAMS . ' teams',
                'teams_members.id_team = teams.id_team'
            )
        );
        $persons_query->setJoinTables(
            new JoinTable(
                JoinType::JOIN,
                DT_PERSONS . ' persons',
                'teams_members.id_person = persons.id_person'
            )
        );
        Database::escapeQuery($persons_query);
        return $persons_query;
    }

    /** @return SelectQuery */
    public static function selectIncidence($tables)
    {
        $query_tables = array();
        foreach ($tables as $table_name => $condition) {
            $table = QueryFactory::selectCount($table_name, $condition);
            Database::escapeQuery($table);
            $query_tables[] = $table->generate();
        }
        $table_count = 1;
        $query = new SelectQuery('(' . array_shift($query_tables) . ") t{$table_count}");
        foreach ($query_tables as $table) {
            $table_count++;
            $query->setJoinTables(new JoinTable(JoinType::CROSS, "({$table}) t{$table_count}"));
        }
        $query->setColumns(self::getIncidenceColumns($table_count));
        return $query;
    }

    private static function getIncidenceColumns($table_count)
    {
        $columns = '';
        for ($i = 1; $i <= $table_count; $i++) {
            $columns .= "t{$i}.COUNT";
            if ($i != $table_count) {
                $columns .= ' + ';
            }
        }
        $columns .= ' as COUNT';
        return $columns;
    }
}
