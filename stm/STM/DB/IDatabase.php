<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\DB;

use STM\DB\SQL\RowsLimit;

interface IDatabase
{
    /**
     * Open a connection to a Database server
     */
    public function openConnection();

    /**
     * Close Database connection
     */
    public function closeConnection();

    /**
     * Send a Database query
     * @param string $sql SQL query
     * @return mixed
     * For SELECT returns a resource on success, or FALSE on error. For INSERT,
     * UPDATE, DELETE, etc returns TRUE on success or FALSE on error
     */
    public function query($sqlQuery);

    /**
     * Executes procedure
     * @param string $name name of the procedure
     * @param array $parameters procedure parameteres
     * @return boolean
     * Returns boolean to inform about result of query.
     */
    public function executeProcedure($name, $parameters = array());

    /**
     * Escapes special characters in a string for use in an SQL statement
     * @param string $value the string that is to be escaped.
     * @return returns the escaped string, or FALSE on error.
     */
    public function escapeValue($value);

    /**
     * Fetch a result row as an associative array
     * @param $resultSet the result resource that is being evaluated. this result comes from a call to query().
     * @return array
     * Returns an array of strings that corresponds to the fetched row, or FALSE if there are no more rows.
     */
    public function fetchAssoc($resultSet);

    /**
     * Fetch a result row as an object
     * @param $resultSet the result resource that is being evaluated. this result comes from a call to query().
     * @return array
     * Returns an array of strings that corresponds to the fetched row, or FALSE if there are no more rows.
     */
    public function fetchObject($resultSet, $className);

    /**
     * Get number of rows in result
     * @param $resultSet the result resource that is being evaluated. this result comes from a call to query().
     * @return array
     * The number of rows in a result set on success or FALSE on failure.
     */
    public function numRows($resultSet);

    /**
     * Get the ID generated in the last query
     * @return mixed
     * The ID generated for an AUTO_INCREMENT column by the previous query on success,
     * 0 if the previous query does not generate an AUTO_INCREMENT value,
     * or FALSE if no MySQL connection was established.
     */
    public function insertId();

    # TRANSACTIONS
    /** Starts new transaction */
    public function startTransaction();

    /** Commits the outstanding database transaction */
    public function commitTransaction();

    /** Rolls back the outstanding database transaction */
    public function rollbackTransaction();

    /**
     * @param string $sql_query
     * @param RowsLimit $rows_limit
     */
    public static function addLimitToQuery(&$sql_query, RowsLimit $rowsLimit);

    /**
     * @param string $string_date string has MySQL date format 'YYYY-MM-DD HH24:MI:SS'
     */
    public static function prepareStringForDateComparison($string_date);

    /** @return DatabaseException */
    public function getLastException();
}
