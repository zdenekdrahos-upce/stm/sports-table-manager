<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\DB\SQL;

use STM\DB\IDatabase;

class MultipleInsertQuery extends Query
{
    /** @var array */
    private $attributes;
    /** @var array */
    private $escapedAttributes;

    public function __construct($table)
    {
        $this->attributes = array();
        $this->escapedAttributes = array();
        parent::__construct($table);
    }

    public function isValid()
    {
        return !empty($this->attributes) && parent::isValid();
    }

    public function generate()
    {
        if ($this->isValid()) {
            $sql = "INSERT INTO " . $this->table . " (";
            $sql .= implode(", ", $this->getTableColumns());
            $sql .= ") VALUES ";
            $count = count($this->attributes) - 1;
            foreach ($this->getQueryAttributes() as $id => $attributes) {
                $sql .= "('";
                $sql .= implode("', '", array_values($attributes));
                $sql .=  "')";
                if ($id < $count) {
                    $sql .= ', ';
                }
            }
            return $sql;
        }
        return false;
    }
    public function addAnotherAttributes($attributes)
    {
        $this->attributes[] = $attributes;
    }
    
    protected function executeEscaping(IDatabase $database)
    {
        foreach ($this->attributes as $id => $attributes) {
            foreach ($attributes as $key => $value) {
                $this->escapedAttributes[$id][$key] = $database->escapeValue($value);
            }
        }
    }

    private function getTableColumns()
    {
        return array_keys($this->attributes[0]);
    }

    /**
     * @return array
     * If query wasn't escaped then returns original unsecaped data from user.
     */
    private function getQueryAttributes()
    {
        return !empty($this->escapedAttributes) ? $this->escapedAttributes : $this->attributes;
    }
}
