<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\DB\SQL;

final class ColumnOrder
{
    private $name;
    private $is_ascending;

    public function __construct($name, $is_ascending = false)
    {
        $this->name = is_string($name) ? $name : '';
        $this->is_ascending = $is_ascending ? true : false;
    }

    public function isValid()
    {
        return !empty($this->name);
    }

    public function __toString()
    {
        if ($this->isValid()) {
            return "{$this->name} {$this->getOrder()}";
        }
        return '';
    }

    private function getOrder()
    {
        return $this->is_ascending ? 'asc' : 'desc';
    }
}
