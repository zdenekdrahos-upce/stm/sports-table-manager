<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\DB\SQL;

use STM\Utils\Classes;
use STM\Utils\Strings;

/**
 * JoinTable class
 * - used for joining tables in SQL
 * - with condition ON, no USING
 */
class JoinTable
{
    private $type;
    private $table;
    private $condition;

    public function __construct($type, $table, $condition = '')
    {
        $this->type = $type;
        $this->table = $table;
        $this->condition = $condition;
    }

    public function isValid()
    {
        return $this->isValidJoinType() &&
                $this->isValidTable() &&
                $this->isValidCondition();
    }

    public function getTable()
    {
        return $this->isValid() ? $this->table : false;
    }

    public function __toString()
    {
        if ($this->isValid()) {
            $output = "{$this->type} {$this->table} ";
            if ($this->type != JoinType::CROSS) {
                $output .= "ON {$this->condition}";
            }
            return $output;
        }
        return '';
    }

    private function isValidJoinType()
    {
        return Classes::isClassConstant('\STM\DB\SQL\JoinType', $this->type);
    }

    private function isValidTable()
    {
        return Strings::isStringNonEmpty($this->table);
    }

    private function isValidCondition()
    {
        if ($this->type != JoinType::CROSS) {
            return Strings::isStringNonEmpty($this->condition);
        }
        return true;
    }
}
