<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\DB\SQL;

use STM\DB\IDatabase;

class InsertQuery extends Query
{
    /** @var array */
    private $attributes;
    /** @var array */
    private $escapedAttributes;

    public function __construct($table)
    {
        $this->attributes = array();
        $this->escapedAttributes = array();
        parent::__construct($table);
    }

    public function isValid()
    {
        return !empty($this->attributes) && parent::isValid();
    }

    public function generate()
    {
        if ($this->isValid()) {
            $sql = "INSERT INTO " . $this->table . " (";
            $sql .= implode(", ", array_keys($this->getQueryAttributes()));
            $sql .= ") VALUES ('";
            $sql .= implode("', '", array_values($this->getQueryAttributes()));
            $sql .= "')";
            return $sql;
        }
        return false;
    }

    /**
     * Escapes attributes from VALUES of the insert query. Every attribute is escaped
     * once, even if this method is called a several times
     * @param IDatabase $database
     */
    protected function executeEscaping(IDatabase $database)
    {
        foreach ($this->attributes as $key => $value) {
            $this->escapedAttributes[$key] = $database->escapeValue($value);
        }
    }

    /**
     * Adds only scalar values (integer, float, string or boolean; null will be ignored).
     * If attribute already exists, then it will be overwritten value of the attribute.
     * @param array $attributes keys = names of attributes, values = values of attributes
     */
    public function addAttributes($attributes)
    {
        $this->attributes = parent::sanitizeAttributes($this->attributes, $attributes);
    }

    public function removeAttribute($attribute)
    {
        if (array_key_exists($attribute, $this->attributes)) {
            unset($this->attributes[$attribute]);
            unset($this->escapedAttributes[$attribute]);
        }
    }

    /**
     * @return array
     * If query wasn't escaped then returns original unsecaped data from user.
     */
    private function getQueryAttributes()
    {
        return !empty($this->escapedAttributes) ? $this->escapedAttributes : $this->attributes;
    }
}
