<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\DB\SQL;

use \Exception;

/**
 * RowsLimit class
 * - limit for select queries
 * - default values: max = 100; offset = 0
 */
class RowsLimit
{
    private $max_rows;
    private $offset;

    public function __construct()
    {
        $this->offset = 0;
        $this->max_rows = 100;
    }

    public function set($maxRows, $offset = 0)
    {
        $this->checkArguments($maxRows, $offset);
        $this->max_rows = $maxRows;
        $this->offset = $offset;
    }

    public function getMaxRows()
    {
        return $this->max_rows;
    }

    public function getOffset()
    {
        return $this->offset;
    }

    private function checkArguments($max_rows, $offset)
    {
        if (!$this->checkIntegerValue($max_rows, 1)) {
            throw new Exception('RowsLimit - max_rows must be >= 1');
        }
        if (!$this->checkIntegerValue($offset, 0)) {
            throw new Exception('RowsLimit - offset must be >= 0');
        }
    }

    private function checkIntegerValue($value, $minimum)
    {
        return is_int($value) && $value >= $minimum;
    }
}
