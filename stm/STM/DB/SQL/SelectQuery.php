<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\DB\SQL;

use STM\Utils\Strings;
use STM\DB\Database;
use STM\DB\IDatabase;

/**
 * SelectQuery class
 * - used for simpler work with SQL SELECT queries
 */
final class SelectQuery extends Query
{
    /** @var boolean */
    private $isDistinct;
    /** @var array */
    private $columns;
    /** @var array */
    private $joinTables;
    /** @var Condition */
    private $where;
    /** @var string */
    private $groupBy;
    /** @var Condition */
    private $having;
    /** @var string */
    private $order_by;
    /** @var Limit */
    private $limit;

    public function generate()
    {
        if ($this->isValid()) {
            $sql = 'SELECT ';
            if ($this->isDistinct) {
                $sql .= 'DISTINCT ';
            }
            $sql .= $this->assembleColumns() . ' ';
            $sql .= 'FROM ' . $this->table . ' ';
            if (is_array($this->joinTables)) {
                foreach ($this->joinTables as $joinTable) {
                    $sql .= $joinTable . ' ';
                }
            }
            $sql .= ! empty($this->where) ? ' WHERE ' . $this->where : ' ';
            $sql .= ! empty($this->groupBy) ? ' GROUP BY ' . $this->groupBy : ' ';
            $sql .= ! empty($this->having) ? ' HAVING ' . $this->having : ' ';
            $sql .= ! empty($this->order_by) ? ' ORDER BY ' . $this->order_by : ' ';
            if (!is_null($this->limit)) {
                $db = Database::getDB();
                $db::addLimitToQuery($sql, $this->limit);
            }
            return $sql;
        }
        return false;
    }

    public function setDistinct()
    {
        $this->isDistinct = true;
    }

    /**
     * Set selected columns from table. Default value is *
     * If columns already exist then attach new columns to the end
     * @param array/string $columns
     *          E.g.  $columns = 'id',
     *                $columns = 'id as id, count(*) as count'
     *                $columns = array('id'),
     *                $columns = array('id as id, count(*) as count')
     *                $columns = array('id' => 'id', 'count' => 'count(*)')
     */
    public function setColumns($columns)
    {
        if (is_array($columns)) {
            foreach ($columns as $alias => $column) {
                if (is_string($column)) {
                    if (is_string($alias)) {
                        $this->columns[$alias] = $column;
                    } else {
                        $this->columns[] = $column;
                    }
                }
            }
        } elseif (is_string($columns)) {
            $this->setColumns(array($columns));
        }
    }

    /**
     * Method for clear all columns
     */
    public function resetColumns()
    {
        $this->columns = array();
    }

    /**
     * Sets one JOIN table in SQL query (appends it to the end of all join tables
     * if table is not already joined)
     * @param JoinTable $join_table
     */
    public function setJoinTables(JoinTable $join_table)
    {
        if ($this->isValidJoinTable($join_table)) {
            $this->joinTables[] = $join_table;
        }
    }

    /**
     * Checks if $join_table is not already joined
     * @param JoinTable $join_table
     * @return boolean
     */
    private function isValidJoinTable(JoinTable $join_table)
    {
        if ($join_table->isValid()) {
            if (!is_null($this->joinTables)) {
                foreach ($this->joinTables as $current_table) {
                    if ($current_table->getTable() == $join_table->getTable()) {
                        return false;
                    }
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Sets WHERE content in SQL query. If condition exists then is replaced by new condition
     * @param Condition $condition
     */
    public function setWhere($condition)
    {
        if ($condition instanceof Condition) {
            $this->where = ($condition->isValid()) ? $condition : false;
        }
    }

    /**
     * Sets GROUP BY content in SQL query.
     * @param string $group_by
     */
    public function setGroupBy($group_by)
    {
        if (Strings::isStringNonEmpty($group_by)) {
            $this->groupBy = $group_by;
        }
    }

    /**
     * Sets HAVING content in SQL query. If condition exists then is replaced by new condition
     * @param type $having
     */
    public function setHaving($having)
    {
        if ($having instanceof Condition) {
            $this->having = ($having->isValid()) ? $having : false;
        }
    }

    /**
     * Sets ORDER BY content in SQL query.
     * @param string $order_by
     */
    public function setOrderBy($order_by)
    {
        if (Strings::isStringNonEmpty($order_by)) {
            $this->order_by = $order_by;
        }
    }

    /** @param RowsLimit $rows_limit */
    public function setLimit(RowsLimit $rows_limit)
    {
        $this->limit = $rows_limit;
    }

    /**
     * Clears all object attributes. After calling this method get_sql_squery() returns false
     */
    public function clearQuery()
    {
        foreach (get_object_vars($this) as $attribute => $value) {
            if (!is_null($value)) {
                $this->$attribute = null;
            }
        }
    }

    /**
     * Escapes values in WHERE/HAVING conditions
     * @param IDatabase $database
     */
    protected function executeEscaping(IDatabase $database)
    {
        if ($this->where) {
            $this->where->escapeValues($database);
        }
        if ($this->having) {
            $this->having->escapeValues($database);
        }
    }

    /**
     * @return string
     * Returns columns separated by comma, if no selected columns exist then return *
     */
    private function assembleColumns()
    {
        $string = '';
        if (is_array($this->columns)) {
            foreach ($this->columns as $alias => $column) {
                $string .= $column . (is_string($alias) ? " as {$alias}" : '');
                $string .= $column != end($this->columns) ? ', ' : '';
            }
        }
        return empty($string) ? '*' : $string;
    }
}
