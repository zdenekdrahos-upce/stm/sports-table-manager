<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\DB\SQL;

use STM\DB\IDatabase;

class BulkInsertQuery extends Query
{

    /** @var array */
    private $columns;
    /** @var array */
    private $escapedColumns;
    /** @var SelectQuery */
    private $selectQuery;

    public function isValid()
    {
        return $this->selectQuery != null && parent::isValid();
    }

    public function generate()
    {
        if ($this->isValid()) {
            $sql = "INSERT INTO " . $this->table . " (";
            $sql .= implode(", ", $this->getQueryColumns()) . ") ";
            $sql .= $this->selectQuery->generate();
            return $sql;
        }
        return false;
    }

    public function addColumns(array $columns)
    {
        $this->columns = parent::sanitizeAttributes($this->columns, array_values($columns));
    }

    public function setSelect(SelectQuery $query)
    {
        $this->selectQuery = $query;
    }

    protected function executeEscaping(IDatabase $database)
    {
        foreach ($this->columns as $key => $value) {
            $this->escapedColumns[$key] = $database->escapeValue($value);
        }
        $this->selectQuery->escapeValues($database);
    }

    private function getQueryColumns()
    {
        return !empty($this->escapedColumns) ? $this->escapedColumns : $this->columns;
    }
}
