<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\DB\SQL;

use STM\Utils\Strings;
use STM\DB\IDatabase;

/**
 * Query class
 * - base class for all SQL queries
 */
abstract class Query
{
    /** @var string */
    protected $table;

    /** @param string $table */
    public function __construct($table)
    {
        $this->table = Strings::isStringNonEmpty($table) ? $table : null;
    }

    /**
     * Checks if SQL query is valid and if it can be used as argument in e.g. mysql_query()
     * @return type
     */
    public function isValid()
    {
        return !is_null($this->table);
    }

    /**
     * Generates SQL query used as argument in database method query($sql)
     * @return string/false
     * Returns string if query is valid, otherwise returns false
     */
    abstract public function generate();

    /** @param IDatabase $database */
    public function escapeValues(IDatabase $database)
    {
        $this->executeEscaping($database);
    }

    /** @param IDatabase $database */
    abstract protected function executeEscaping(IDatabase $database);

    /**
     * Preserves only scalar keys & values (integer, float, string or boolean; null will be ignored).
     * If attribute already exists, then it will be overwritten value of the attribute
     * in $currentAttributes
     * @param array $currentAttributes sanitized values are saved to this array
     * @param array $newAttributes keys = names of attributes, values = values of attributes
     * @return array
     */
    protected function sanitizeAttributes($currentAttributes, $newAttributes)
    {
        if (is_array($newAttributes)) {
            foreach ($newAttributes as $attribute => $value) {
                if (is_scalar($attribute) && is_scalar($value)) {
                    $currentAttributes[$attribute] = $value;
                }
            }
        }
        return $currentAttributes;
    }

    /**
     * @return string
     * Returns SQL query with semicolon at the end if SQL query is valid, otherwise empty string.
     */
    public function __toString()
    {
        return $this->isValid() ? ($this->generate() . ';') : '';
    }
}
