<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\DB\SQL;

class JoinType
{
    const JOIN = 'JOIN';
    const LEFT = 'LEFT JOIN';
    const RIGHT = 'RIGHT JOIN';
    const FULL = 'FULL JOIN';
    const CROSS = 'CROSS JOIN';

    private function __construct()
    {
    }
}
