<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\DB\SQL;

use STM\DB\IDatabase;

// delete only if WHERE condition is set
class DeleteQuery extends Query
{
    /** @var Condition */
    private $where;

    public function isValid()
    {
        return parent::isValid() && !is_null($this->where);
    }

    public function generate()
    {
        if ($this->isValid()) {
            $sql = 'DELETE FROM ' . $this->table . ' ';
            $sql .= 'WHERE ' . $this->where;
            return $sql;
        }
        return false;
    }

    public function setWhere($condition)
    {
        if ($condition instanceof Condition && $condition->isValid()) {
            $this->where = $condition;
        }
    }

    protected function executeEscaping(IDatabase $database)
    {
        if ($this->isValid()) {
            $this->where->escapeValues($database);
        }
    }
}
