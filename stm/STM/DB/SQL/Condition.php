<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\DB\SQL;

use STM\DB\IDatabase;

/**
 * Condition class
 * - used for defining WHERE/HAVING conditions in SQL queries
 */
class Condition
{
    /** @var string */
    private $condition;
    /** @var array */
    private $values;
    /** @var array */
    private $escapedValues;

    /**
     * Where/Having condition,
     * @param string $full_condition    e.g. 'id > {1} and name like {2}'
     * @param array $values             e.g. array(1 => 20, 2 => 'S%');
     */
    public function __construct($full_condition, $values = false)
    {
        $this->condition = is_string($full_condition) ? $full_condition : '';
        $this->values = array();
        $this->escapedValues = array();
        $this->loadValuesArray($values);
    }

    /**
     * It joins $condition to current condition (use AND).
     * @param string $condition    e.g. 'id > {1} and name like {2}'
     * @param array $values        e.g. array(1 => 20, 2 => 'S%');
     */
    public function addCondition($condition, $values = false)
    {
        if (is_string($condition)) {
            if ($this->isValid()) {
                $this->condition .= ' AND ';
            }
            $this->condition .= $condition;
            $this->loadValuesArray($values);
        }
    }

    /**
     *
     * @param string $condition
     * @param array $values
     */
    public function addOrReplaceCondition($condition, $values = false)
    {
        if (is_int(strpos($this->condition, $condition))) {
            if (is_array($values)) {
                foreach ($values as $key => $value) {
                    $this->values[$key] = $value;
                }
            }
        } else {
            $this->addCondition($condition, $values);
        }
    }

    /**
     * Removes condition (if exists) from instance
     * @param string $condition
     */
    public function removeCondition($condition)
    {
        if (is_int(strpos($this->condition, $condition))) {
            if (strlen($this->condition) == strlen($condition)) {
                $this->condition = '';
            } else {
                $search = array($condition . ' and', $condition . ' or');
                $this->condition = str_ireplace($search, '', $this->condition);
            }
        }
    }

    /**
     * Adds value only if key is not already defined and if condition (attribute)
     * in this class is valid (non empty)
     * @param array $array
     */
    private function loadValuesArray($array)
    {
        if (is_array($array) && $this->isValid()) {
            foreach ($array as $key => $value) {
                if (!array_key_exists($key, $this->values) && is_scalar($value)) {
                    $this->values[$key] = $value;
                }
            }
        }
    }

    /**
     * Checks if condition is valid and it can be used in SQL query
     * @return boolean
     */
    public function isValid()
    {
        return !empty($this->condition);
    }

    /**
     * Escapes all values in array $values. If $database does not implement
     * interface IDatabase then program die
     * @param IDatabase $database
     */
    public function escapeValues(IDatabase $database)
    {
        foreach ($this->values as $key => $value) {
            $this->escapedValues[$key] = $database->escapeValue($value);
        }
    }

    /**
     * @return string
     */
    public function __toString()
    {
        if ($this->isValid()) {
            if (empty($this->values)) {
                return $this->condition;
            } else {
                $output = $this->condition;
                foreach ($this->getQueryAttributes() as $key => $value) {
                    $output = str_replace("{{$key}}", "'{$value}'", $output);
                }
                return $output;
            }
        }
        return '';
    }

    private function getQueryAttributes()
    {
        return !empty($this->escapedValues) ? $this->escapedValues : $this->values;
    }
}
