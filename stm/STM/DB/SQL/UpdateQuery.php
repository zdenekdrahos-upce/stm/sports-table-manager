<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\DB\SQL;

use STM\DB\IDatabase;

class UpdateQuery extends Query
{
    /** @var array */
    private $setAttributes;
    /** @var array */
    private $escapedSetAttributes;
    /** @var Condition */
    private $where;

    public function __construct($table)
    {
        $this->setAttributes = array();
        $this->escapedSetAttributes = array();
        $this->where = null;
        parent::__construct($table);
    }

    public function isValid()
    {
        return parent::isValid() && !empty($this->setAttributes) && $this->isValidWhere();
    }

    public function generate()
    {
        if ($this->isValid()) {
            $sql = 'UPDATE ' . $this->table . ' ';
            $sql .= 'SET ' . implode(", ", $this->createPairs()) . ' ';
            $sql .= 'WHERE ' . $this->where;
            return $sql;
        }
        return false;
    }

    protected function executeEscaping(IDatabase $database)
    {
        if ($this->isValidWhere()) {
            $this->where->escapeValues($database);
        }
        foreach ($this->setAttributes as $key => $value) {
            $this->escapedSetAttributes[$key] = $database->escapeValue($value);
        }
    }

    /** @param array $attributes for SET part of UPDATE query */
    public function addAttributes($attributes)
    {
        $this->setAttributes = parent::sanitizeAttributes($this->setAttributes, $attributes);
    }

    public function setWhere($condition)
    {
        if ($condition instanceof Condition && $condition->isValid()) {
            $this->where = $condition;
        }
    }

    private function isValidWhere()
    {
        return !is_null($this->where) && $this->where->isValid();
    }

    private function createPairs()
    {
        $attributePairs = array();
        foreach ($this->getQueryAttributes() as $key => $value) {
            $value = (!is_numeric($value) && empty($value) ? 'null' : "'{$value}'");
            $attributePairs[] = "{$key} = {$value}";
        }
        return $attributePairs;
    }

    private function getQueryAttributes()
    {
        return !empty($this->escaped_attributes) ? $this->escaped_attributes : $this->setAttributes;
    }
}
