<?php
/**
 * If content of a constant is non empty string then the content of the constant will
 * be displayed to all visitors who come to STM (even admin cannot access administration)
 * You can use it e.g. during update of STM
 */
define('STM_OUT_OF_ORDER', '');

/**
 * Allowed UP during out of order. Semi-colon (;) for separating IPs
 * Examples:
 *  ''                      no ips
 *  '127.0.0.1'             only IP 127.0.0.1 can access STM during update
 *  '127.0.0.1;127.0.0.0'   two ip adresses can access STM during update
 */
define('STM_ALLOWED_IP_DURING_SERVICE', '');


/** @return boolean */
function isStmOutOfOrder()
{
    $is_out = is_string(STM_OUT_OF_ORDER) && strlen(STM_OUT_OF_ORDER) > 0;
    if (isset($_SERVER['REMOTE_ADDR'])) {
        $allowed_ip = in_array($_SERVER['REMOTE_ADDR'], explode(';', STM_ALLOWED_IP_DURING_SERVICE));
        return $is_out && !$allowed_ip;
    } else {
        return false;
    }
}
