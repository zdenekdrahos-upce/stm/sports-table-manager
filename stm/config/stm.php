<?php
// BASIC SETTINGS
/** Define if user registration is allowed */
define('STM_ALLOW_USER_REGISTRATION', true);
/** Define if unregistered visitor has access to application (only read competitions) */
define('STM_ALLOW_VISITOR_ACCESS', true);
/** Define if match score prediction is allowed */
define('STM_ALLOW_PREDICTIONS', true);
/** Available languages: english, czech */
define('STM_DEFAULT_LANGUAGE', 'czech');
/** Default datetime format ('Y-m-d H:i:s' == YEAR-MONTH-DAY HOUR-MINUTE-SECOND) 
 * http://php.net/manual/en/datetime.formats.php */
define('STM_DEFAULT_DATETIME_FORMAT', 'd.m.Y H:i');
/** Url to website used in Back to website */
define('STM_CLUB_WEBSITE', 'http://www.sports-table-manager.php5.cz');

// DEFAULT VALUES
# Season points
define('STM_POINT_WIN', 3);
define('STM_POINT_WIN_OT', 3);
define('STM_POINT_DRAW', 1);
define('STM_POINT_LOSS_OT', 0);
define('STM_POINT_LOSS', 0);
define('STM_MATCH_PERIODS', 2);
# Filtering in Matches
define('STM_MAX_MATCHES_FOR_TABLE', 30);
define('STM_MAX_MATCHES_FOR_LISTING', 100);
define('STM_MATCHES_ON_ONE_PAGE', 15);
# Statistics
define('STM_STATISTIC_CALCULATION', 'COUNT');
# Match players
define('STM_MINUTE_IN', '0');
define('STM_MINUTE_OUT', '90');

// Cache
/** Id of seasons (separated by comma) for which season table is not deleted and updated (e.g. 4,8) */
define('STM_NOT_DELETED_SEASONS_TABLE', '');
