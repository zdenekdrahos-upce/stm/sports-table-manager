<?php
/** The hostname of the Database server */
define('STM_DB_SERVER', 'localhost');
/** Your username for the server / database */
define('STM_DB_USER', 'root');
/** Your password for the server/database */
define('STM_DB_PASS', 'rootroot');
/** The name of your database - set only if you're using MySQL */
define('STM_DB_NAME', 'sports_table_manager');
/** If procedures can be used in database */
define('STM_DB_PROCEDURES', true);
/** If triggers can be used in database */
define('STM_DB_TRIGGERS', true);
