<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

/**
 * Redirects to page from argument (if $page is string)
 * @param string $page
 */
function redirect($page)
{
    if (is_string($page)) {
        header('Location: ' . $page);
        exit;
    }
}

/**
 * Function for building links. It appends the array from arguments to the current
 * link ($_GET). It only adds item from array if $key is string and $value is
 * scalar (integer, float, string or boolean)
 * @param array $array e.g. array('id' => 1, 'action' => 'create')
 * @return string
 * Returns new link as string. If argument is not array then returns the URI
 * which was given in order to access this page ($_SERVER['REQUEST_URI'])
 */
function buildUrl($array)
{
    if (is_array($array)) {
        $parameters = $_GET;
        foreach ($array as $parameter => $value) {
            if (is_string($parameter) && is_scalar($value)) {
                $parameters[$parameter] = htmlspecialchars(urlencode($value));
            } elseif (is_null($value)) {
                unset($parameters[$parameter]);
            }
        }
        $query = http_build_query($parameters, '', '&');
        return $_SERVER['PHP_SELF'] . (empty($query) ? '' : '?' . $query);
    } else {
        return $_SERVER['REQUEST_URI'];
    }
}

/**
 * Get translations from LanguageSwitch class
 * @return string
 */
function stmLang()
{
    $args = func_get_args();
    $callback = '\STM\Libs\LanguageSwitch\Translator::get';
    return call_user_func_array($callback, $args);
}

function stmLangf()
{
    $args = func_get_args();
    $callback = '\STM\Libs\LanguageSwitch\Translator::getFormattedString';
    return call_user_func_array($callback, $args);
}

/**
 * STM PSR-0 autoload function
 * @param string $class
 */
function stmAutoload($class)
{
    $isStm = strpos($class, 'STM\\') === 0;
    if ($isStm) {
        $modulePosition = strpos($class, '\Module\\');
        if (is_int($modulePosition)) {
            $nextSeparator = strpos($class, '\\', $modulePosition + 8);
            $modulePre = substr($class, 0, $nextSeparator);
            $modulePost = substr($class, $nextSeparator + 1);
            $class = "{$modulePre}\Controllers\\{$modulePost}";
        }
        $classPath = str_replace('\\', DIRECTORY_SEPARATOR, $class);
        require_once(STM_CLASS_ROOT . $classPath . '.php');
        // load DB object
        if (array_key_exists($class, STM\DB\ORM\ClassMapping::$mapping)) {
            STM\DB\Object\DbObjectsLoader::loadClass($class);
        }
    }
}
