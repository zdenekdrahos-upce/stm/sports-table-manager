<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

// Configuration
require_once(STM_CONFIG . 'database.php');
require_once(STM_CONFIG . 'stm.php');

// Classes, functions, MVC, DB
require_once(STM_CLASS_ROOT . 'functions.php');
require_once(STM_CLASS_ROOT . 'STM/Session.php');
require_once(STM_CLASS_ROOT . 'STM/Module/Controller.php');
require_once(STM_CLASS_ROOT . 'STM/Module/ModuleController.php');
require_once(STM_CLASS_ROOT . 'STM/Module/ModuleProcessing.php');
require_once(STM_CLASS_ROOT . 'STM/DB/ORM/ClassMapping.php');
require_once(STM_CLASS_ROOT . 'STM/DB/ORM/TablePrimaryKeys.php');
require_once(STM_CLASS_ROOT . 'STM/DB/Object/DbObjectsLoader.php');
