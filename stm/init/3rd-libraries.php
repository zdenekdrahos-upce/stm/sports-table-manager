<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

// Logger
define('STM_LOG_ROOT', STM_SITE_ROOT . 'files/logs/');
define('STM_DEFAULT_LOG_NAME', 'log');
define('STM_EXT', '.csv');
define('STM_SEP', ';');
define('STM_TIME_FORMAT', '%Y-%m-%d %H:%M:%S');

// Language switch
define('STM_COOKIE_NAME', 'sports_table_manager');
define('STM_COOKIE_EXPIRE', time() + 60 * 60 * 24 * 365);
define('STM_LANG_EXT', '.php');
if (!defined('STM_LANG_ROOT')) {
    define('STM_LANG_ROOT', STM_SITE_ROOT . 'stm/langs/');
}

// Sports generators
define('SEASON_MIN_PERIODS', 1);
define('SEASON_MAX_PERIODS', 20);
define('SEASON_MAX_TEAMS', 100);
define('PLAYOFF_MAX_TEAMS', 128);
