<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

// Autoload
spl_autoload_register('stmAutoload');

// Init entity factories (mapping is static variable, so somebody could overwrite it)
\STM\StmFactory::find();

// Init session
\STM\Session::getInstance();

// Language
\STM\Libs\LanguageSwitch\Language::init(STM_LANG_ROOT);

// DB Objects
try {
    \STM\DB\Object\DbObjectsLoader::initDb();
    \STM\Helpers\Selection::initDateIds();
} catch (\STM\DB\DatabaseException $e) {
}
