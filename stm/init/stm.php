<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

// STM
define('STM_VERSION', '2.0');
define('STM_WEBSITE', 'http://www.sports-table-manager.php5.cz/');
define('STM_SITE_ROOT', __DIR__ . '/../../');
define('STM_CONFIG', STM_SITE_ROOT . '/stm/config/');
define('STM_CLASS_ROOT', STM_SITE_ROOT . 'stm/');
define('STM_ADMIN_TEMPLATE_ROOT', STM_SITE_ROOT . 'stm/admin-templates/');
define('STM_MODULES_ROOT', STM_SITE_ROOT . 'stm/STM/Module/');

// Cache
define('STM_CACHE', STM_SITE_ROOT . 'files/cache/');
define('STM_CACHE_SEASON_TABLES', STM_CACHE . 'season-tables/');
define('STM_CACHE_SEASON_TEAM_POSITIONS', STM_CACHE . 'season-team-positions/');
define('STM_CACHE_COMPETITION_MATCHES', STM_CACHE . 'competition-matches/');
define('STM_CACHE_COMPETITION_PLAYERS_STATS', STM_CACHE . 'competition-players-stats/');
define('STM_CACHE_TEAM_MATCHES_STATS', STM_CACHE . 'team-match-stats/');
