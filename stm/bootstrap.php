<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport/sports-table-manager)
 * @license New BSD License
 * @author Zdenek Drahos
 */

/* Is STM out of order? If so then end the script */
require_once(__DIR__ . '/config/out-of-order.php');
if (isStmOutOfOrder()) {
    die(STM_OUT_OF_ORDER);
}

/** Defines STM constants like version, paths, cache. */
require_once(__DIR__ . '/init/stm.php');

/** Defines constants for names of tables in DB */
require_once(__DIR__ . '/init/db-tables.php');

/** Loads required files needed for configuration and important classes. */
require_once(__DIR__ . '/init/required-files.php');

/** Init 3rd party libraries - Logger, Language Switch, Sports Generators. */
require_once(__DIR__ . '/init/3rd-libraries.php');

/** Object/class initialization and autoload registration. */
require_once(__DIR__ . '/init/objects.php');
